# @(#) runapp.sh 
# @(#) Mynor Ramirez
# @(#) Marzo 201       
# @(#) Shell utilizado para ejecutar el aplicatuvo 

# Asignando valores
DIR=`pwd`
export DIRBASE=/sistemas/apl/bod 

# @(#) Variables del Desarollador (Genero)
. /opt/fourjs/grt/envgenero

# @(#) 2. Variables de Base de datos (Informix)
. /opt/informix/ids12/ifmx.sh

# @(#) 3. Variables de Aplicacion 
. $DIRBASE/cmd/enva.sh 
cd $DIRBASE/src/bin

# Ejecutando modulo
fglrun mainmenu.42r

cd $DIR 
