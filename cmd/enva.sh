# @(#) enva.sh 
# @(#) Mynor Ramirez
# @(#) Diciembre 2010 
# @(#) Shell utilizado para cargar las variables de aplicacion 

# Asignando valores
export TERM=vt100
export DIRCMD=/sistemas/cmd 
export BASEDIR=/sistemas/apl/bod
export FGLDBPATH=$BASEDIR/sch 
export FGLLDPATH=$BASEDIR/lib:.
export DBPATH=$BASEDIR/lib:.
export FGLSOURCEPATH=$BASEDIR/lib:$BASEDIR/std:. 
export FGLRESOURCEPATH=$BASEDIR/lib:$BASEDIR/std:.
export FGLIMAGEPATH=$BASEDIR/img:$BASEDIR/pic:.
export SPOOLDIR=$HOME/spl
export PATH=$DIRCMD:$BASEDIR/cmd:$PATH
export USER=`logname` 

# Creando directorios de reportes
[ ! -d $SPOOLDIR ] && mkdir $SPOOLDIR 
[ ! -d $DIRWWW ] && mkdir $DIRWWW/$HOME 

# Mascara de creacion 
umask 0000 
