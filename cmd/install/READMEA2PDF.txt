NAME
    a2pdf - converts ASCII text to PDF format, with optional line/page
    numbering and Perl syntax highlighting

SYNOPSIS
     a2pdf [options] [filename]

  Options
    a2pdf recognises the following command line options:

    --help
        Displays usage message and exits.

    --doc
        Displays full documentation and exits.

    --version
        Prints the version number and exits

    --output-file | -o
        Specifies the filename for the PDF file. If this option is not set,
        a2pdf will output to STDOUT.

    --title
        Sets the title to be included in the page header. If unspecified,
        the title will default to the name of the file which is being
        converted, or to 'STDIN' if a2pdf is processing from standard input.

    --timestamp
        Boolean option - if set, the timestamp of the file to be converted
        will be included in the page header. This option is turned off by
        default.

    --icon
        Path to an image file which will be included as part of the header
        in the top left of each page.

        Image files may be in any format supported by PDF::API2.

    --icon-scale
        Scaling value for icon images, default is 0.25.

    --header | --noheader | --notitle
        Prints a header consististing of the page title, and optionally the
        timestamp and an image icon at the top of each page. This option is
        enabled by default, use "--notitle" or "--noheader" to disable.

    --footer | --nofooter | --page-numbers | --nopage-numbers
        Adds the current page number to the bottom of each page. This is
        enabled by default, use "--nofooter" or "--nopage-numbers" to
        disable.

    --line-numbers | --noline-numbers
        By default, line numbers will be included in the output PDF file. To
        disable this behaviour, use the "--noline-numbers" option.

    --perl-syntax | --noperl-syntax
        Enables or disables (default is enabled) Perl syntax highlighting.
        This feature requires that the Perl::Tidy module is installed.

    --page-width
    --page-height
        Page width and height in points. Default page size is 595 x 842
        (A4).

    --margins
    --left-margin
    --right-margin
    --top-margin
    --bottom-margin
        Specifies the non-printable area of the page. The "--margin" option
        will set all margins to the same value, however individual margins
        may be altered with the appropriate options. Values must be given in
        points. The default value for the left and right margins is 48
        points, and for the top and bottom margins is 60 points.

    --font-face
        Sets the font to use for the PDF file - currently this must be one
        of the PDF core fonts. The default font face is Courier.

    --font-size
        Font size in points, default value is 10.

    --line-spacing
        Line spacing in points, default value is the font size + 2.

    --noformfeed
        By default, any formfeed characters in the input stream will be
        processed and will act as expected, i.e. a new page will be started
        in the output PDF file. This can be disabled with the "--noformfeed"
        option which will cause all formfeed characters to be ignored.

    Options may be given in any format recognised by the Getopt::Long Perl
    module, e.g. "--name=value" or "--name value". Option names may be
    abbreviated to their shortest unique value.

    If the input filename is not given, then a2pdf will expect to receive
    input from STDIN.

DEPENDENCIES
    a2pdf requires the PDF::API2 Perl module (tested with PDF::API2 version
    0.60).

    Perl syntax highlighting requires the Perl::Tidy module (tested with
    Perl::Tidy version 20070504).

    To include images in the page header, the modules File::Type and
    Image::Size must be installed.

BUGS / ISSUES
    *   If the Perl syntax highlighting feature is used and the input Perl
        code uses source filter modules, then depending on the changes made
        by the source filter the syntax highlighting may not be performed
        correctly.

    *   When running under Red Hat 9, the LANG environment variable must be
        set to 'C'.

SEE ALSO
    a2pdf homepage - <http://perl.jonallen.info/projects/a2pdf>

    PDF::API2 - <http://search.cpan.org/dist/PDF-API2>

    Perl::Tidy - <http://search.cpan.org/dist/Perl-Tidy>

    File::Type - <http://search.cpan.org/dist/File-Type>

    Image::Size - <http://search.cpan.org/dist/Image-Size>

AUTHOR
    Written by Jon Allen (JJ), <jj@jonallen.info> /
    <http://perl.jonallen.info>

COPYRIGHT and LICENCE
    Copyright (C) 2007 Jon Allen (JJ) <jj@jonallen.info>

    This software is licensed under the terms of the Artistic License
    version 2.0.

    For full license details, please read the file 'artistic-2_0.txt'
    included with this distribution, or see
    http://www.perlfoundation.org/legal/licenses/artistic-2_0.html

