DATABASE storepos

#########################################################################
## Function  : GLOBALS
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Declaracion de funciones globales
#########################################################################
GLOBALS 
--DEFINE gUsuario   RECORD LIKE usuario.*
DEFINE gUsuario   RECORD LIKE glb_usuarios.*
CONSTANT cRaiz = -1
CONSTANT cErr = "Error. La operaci�n no fue procesada."
CONSTANT cAddOK = "Registro agregado exitosamente"
CONSTANT cDelOK = "Registro eliminado"
CONSTANT cUpdOK = "Registro actualizado"
CONSTANT cPerOK = "Permisos actualizados"
END GLOBALS 
