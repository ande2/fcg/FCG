GLOBALS "menuGlobals.4gl"

#########################################################################
## Function  : MAIN
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Funcion principal del programa
#########################################################################
MAIN
   DEFINE aui  om.DomNode
   DEFINE sm   om.DomNode
   DEFINE ok   SMALLINT
   DEFINE tit  STRING
   
   OPTIONS
      INPUT NO WRAP,
      ON CLOSE APPLICATION STOP
   WHENEVER ERROR CONTINUE

CALL ui.Interface.setName("mainmenu")
      CALL ui.Interface.setType("container")
   
   CALL ui.Interface.loadActionDefaults("actiondefaultsLogin")
   CALL ui.Interface.loadStyles("style")
   --CALL ui.Interface.loadActionDefaults("actiondefaults")
   --CALL ui.Interface.loadStyles("styles")
   LET ok = login()
   IF (ok) THEN
      --LET tit = "Bienvenido al Sistema :: ", gUsuario.usuNombre CLIPPED, " (", gUsuario.usuLogin CLIPPED, ")"
      LET tit = "Bienvenido al Sistema :: ", gUsuario.nomUsr CLIPPED, " (", gUsuario.userId CLIPPED, ")"
      
      OPEN WINDOW menuBlank AT 1,1 WITH FORM "menuBlank" ATTRIBUTES (TEXT = tit)

          -- Cargando estilos del menu    
         CALL ui.Interface.setText(tit)
         LET aui = ui.Interface.getRootNode()
         LET sm = aui.createChild("StartMenu")
         CALL sm.setAttribute("text","Menu Principal")
         CALL men(cRaiz, sm)
         MENU ""
            COMMAND "Salir"
                -- Verificando si existe algun programa abierto 
                IF ui.Interface.getChildCount()>0 THEN
                   CALL fgl_winmessage(
                   "Atencion:",
                   "Deben cerrarse todas las ventanas antes de salir del sistema.",
                   "exclamation")
                ELSE
                   EXIT PROGRAM
                END IF
                
           -- COMMAND KEY(INTERRUPT)
             --  EXIT MENU
         END MENU
      CLOSE WINDOW menuBlank
   END IF
END MAIN

#########################################################################
## Function  : men()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Arma la estructura de menu
#########################################################################
FUNCTION men(p,sm)
   DEFINE arr    DYNAMIC ARRAY OF
      RECORD
         menId    LIKE menu.menId,
         menNombre   LIKE menu.menNombre,
         menTipo  LIKE menu.menTipo,
         menCmd   LIKE menu.menCmd,
         node     om.DomNode
      END RECORD
   DEFINE p       LIKE menu.menId
   DEFINE i, n, cnt  SMALLINT
   DEFINE qry     STRING
   DEFINE nom     LIKE menu.menNombre
   DEFINE sm      om.DomNode
   DEFINE smg     om.DomNode
   DEFINE smc     om.DomNode
   --LET qry = "SELECT a.menId, a.menNombre, a.menTipo, a.menCmd FROM menu a, permiso b WHERE a.menId <> ", cRaiz, " AND a.menPadre = ", p, " AND b.perMenId = a.menId AND b.perGrpId = ", gUsuario.usuGrpId
   LET qry = "SELECT a.menId, a.menNombre, a.menTipo, a.menCmd FROM menu a, permiso b WHERE a.menId <> ", cRaiz, " AND a.menPadre = ", p, " AND b.perMenId = a.menId AND b.perGrpId = ", gUsuario.roleId
   PREPARE prpM1 FROM qry
   DECLARE curM1 CURSOR FOR prpM1
   LET i = 1
   FOREACH curM1 INTO arr[i].menId, arr[i].menNombre, arr[i].menTipo, arr[i].menCmd
      LET i = i + 1
   END FOREACH
   LET i = i - 1
   IF i > 0 THEN
      FOR n = 1 TO i
         IF arr[n].menTipo = 0 THEN    -- Crea un submenu
            LET arr[n].node = createStartMenuGroup(sm,arr[n].menNombre)
         ELSE                          -- Crea un comando
            LET arr[n].node = createStartMenuCommand(sm,arr[n].menNombre CLIPPED,arr[n].menCmd CLIPPED, "circle.png")
         END IF
         CALL men(arr[n].menId,arr[n].node)
      END FOR
   END IF
END FUNCTION

#########################################################################
## Function  : login()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Firma del sistema
#########################################################################
FUNCTION login()
   DEFINE ok,c,s  SMALLINT
   DEFINE rec
      RECORD
         userId LIKE glb_usuarios.userId,
         usuPwd   LIKE glb_usuarios.usuPwd
      END RECORD
   DEFINE qry     STRING
   LET ok = FALSE
   LET s = TRUE
   OPEN WINDOW menLogin AT 1,1 WITH FORM "menLogin" ATTRIBUTES (TEXT = "Inicio de sesion...")
   CALL lib_cleanTinyScreen()
   CALL ui.Interface.setText("Inicio de sesion...")
   WHILE s
      LET int_flag = FALSE
      INPUT BY NAME rec.*

         AFTER INPUT
            IF (NOT int_flag) THEN
               LET c = 0
               LET qry = "SELECT COUNT(*) FROM glb_usuarios WHERE userId = ? AND usuPwd = ?"
               PREPARE prpL1 FROM qry
               EXECUTE prpL1 USING rec.* INTO c
               IF STATUS = 0 THEN
                  IF c = 1 THEN
                     LET qry = "SELECT * FROM glb_usuarios WHERE userId = ? AND usuPwd = ?"
                     PREPARE prpL2 FROM qry
                     EXECUTE prpL2 USING rec.* INTO gUsuario.*
                     IF STATUS = 0 THEN
                        LET s = FALSE
                        LET ok = TRUE
                     END IF
                  ELSE
                     CALL msg("Usuario/Password incorrecto")
                     NEXT FIELD userId
                  END IF
               END IF
            ELSE
               LET int_flag = TRUE
               LET s = FALSE
            END IF
      END INPUT
   END WHILE
   CLOSE WINDOW menLogin
   RETURN ok
END FUNCTION

#########################################################################
## Function  : nm()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Obtiene el nombre del menu
#########################################################################
FUNCTION nm(id)
   DEFINE id   LIKE menu.menId
   DEFINE nomb LIKE menu.menNombre
   DEFINE qry  STRING
   LET qry = "SELECT menNombre FROM menu WHERE menId = ?"
   PREPARE prpD1 FROM qry
   EXECUTE prpD1 USING id INTO nomb
   RETURN nomb
END FUNCTION
