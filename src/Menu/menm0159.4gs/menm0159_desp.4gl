################################################################################
# Funcion     : %M%
# Descripcion : Funcion para consulta de catalogo 
# Funciones   : cat_busca_init() 
#               busca_cat()
#               fetch_cat(fetch_flag) 
#               cat_desp()
#               limpiar() 
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Nestor Pineda
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################


GLOBALS "menm0159_glob.4gl"

FUNCTION cat_busca_init()
DEFINE
	select_stmt3			VARCHAR(1000)
   LET select_stmt3 =" SELECT m.proc_id, m.proc_desc, m.est_id ",
                     " FROM   bproc m ",
                     " WHERE  m.proc_id = ? "
PREPARE ex_stmt3 FROM select_stmt3
END FUNCTION

FUNCTION busca_cat()
DEFINE
	cat_query		VARCHAR(1000),
	where_clause	VARCHAR(1500),
	cat_cnt        SMALLINT,
   condicion      CHAR(300),
	cat_count		CHAR(300),
   mensaje        CHAR(100),
   respuesta      CHAR(6),
   lint_flag      SMALLINT ,
	lcondicion     STRING 

   CLEAR FORM

   --CALL encabezado()

   LET gtit_enc="BUSQUEDA REGISTRO"
   INITIALIZE gr_reg.*  TO NULL
   CALL tree_arr.clear()


   DISPLAY BY NAME gtit_enc                                                           
   LET int_flag = FALSE

   CONSTRUCT  where_clause 
      ON      bproc.proc_id, bproc.proc_desc
      FROM    proc_id, proc_desc

      BEFORE CONSTRUCT
         CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")	
         CALL fgl_dialog_setkeylabel("CONTROL-B", "Buscar")
         CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")	

       ON KEY (CONTROL-B)
            CASE
               WHEN INFIELD(proc_id)
                  LET int_flag = FALSE
                  LET lcondicion =  " est_id = 13 "
                  CALL librut002_picklist('Busqueda de Procesos','C�digo','Descripci�n',
                                  'proc_id','proc_desc',
                                  'bproc',lcondicion,1,0)
                   RETURNING gr_reg.proc_id, gr_reg.proc_desc, INT_FLAG
                   DISPLAY BY NAME gr_reg.*
            END CASE

         
      AFTER CONSTRUCT
         IF int_flag THEN
            --CALL box_inter() RETURNING int_flag
            LET INT_FLAG =  box_pregunta("Esta seguro de cancelar el proceso?")
            IF int_flag = 1 THEN
               EXIT CONSTRUCT
            ELSE
               CONTINUE CONSTRUCT
            END IF
         END IF
   END CONSTRUCT

   IF int_flag = TRUE THEN
      LET int_flag = FALSE
      CLEAR FORM
      --CALL encabezado()
      ERROR "B�squeda Abortada."
      RETURN FALSE, 0
   END IF

   LET where_report = where_clause

   LET cat_query =" SELECT proc_id ",
                  " FROM bproc WHERE ", where_clause CLIPPED,
                  " ORDER BY 1 "
   LET cat_count = "SELECT count(*) FROM bproc WHERE ", where_clause CLIPPED

   PREPARE ex_stmt FROM cat_query

   PREPARE ex_stmt2 FROM cat_count

   DECLARE cat_ptr SCROLL CURSOR WITH HOLD FOR ex_stmt

   OPEN cat_ptr

   FETCH FIRST cat_ptr INTO gr_reg.proc_id

   IF SQLCA.SQLCODE = 100 THEN
      CALL box_valdato("No existen datos con estas condiciones.")
      CLOSE cat_ptr
      RETURN FALSE,0
   ELSE
      DECLARE cat_all CURSOR FOR ex_stmt3	
      OPEN cat_all USING gr_reg.proc_id

      FETCH cat_all INTO gr_reg.*

      IF SQLCA.SQLCODE = NOTFOUND THEN
         CALL box_valdato("No existen datos con estas Condiciones.")
         CLOSE cat_ptr
         RETURN FALSE,0
      ELSE
         DECLARE contador_ptr CURSOR FOR ex_stmt2
         OPEN contador_ptr
         FETCH contador_ptr INTO cat_cnt
         CLOSE contador_ptr
         CALL cat_desp15()
         RETURN TRUE, cat_cnt
      END IF				
   END IF				
END FUNCTION

FUNCTION fetch_cat(fetch_flag)
DEFINE
	fetch_flag		SMALLINT

   FETCH RELATIVE fetch_flag cat_ptr INTO gr_reg.proc_id

   IF SQLCA.SQLCODE = NOTFOUND THEN
      IF fetch_flag = 1 THEN
         CALL box_valdato("Est� posicionado en el final de la lista.")
      ELSE
         CALL box_valdato("Est� posicionado en el principio de la lista.")
      END IF
   ELSE
      OPEN cat_all USING gr_reg.proc_id

      FETCH cat_all INTO gr_reg.*
      IF SQLCA.SQLCODE = NOTFOUND THEN
         CLEAR FORM
         CALL cat_desp15()
         CALL box_valdato("El registro fue eliminado desde la �ltima Consulta.")
      ELSE
         CALL cat_desp15()
      END IF
      CLOSE cat_all
   END IF
END FUNCTION

FUNCTION cat_desp15()
DEFINE idx INT
   CALL  CargarTablas (gr_reg.proc_id)
	DISPLAY BY NAME gr_reg.*
   DISPLAY ARRAY tree_arr TO sTree.* 
      BEFORE DISPLAY
         EXIT DISPLAY
   END DISPLAY
   CALL CargarMovimiento ( gr_reg.proc_id )
END FUNCTION



FUNCTION limpiar() 
WHENEVER ERROR CONTINUE
--CLOSE cat_ptr
WHENEVER ERROR STOP
CALL tree_arr.clear()
CALL gr_det.clear()
CLEAR FORM
   DISPLAY ARRAY tree_arr TO sTree.* 
      BEFORE DISPLAY
         EXIT DISPLAY
   END DISPLAY
   DISPLAY ARRAY gr_det TO sDet.*
      BEFORE DISPLAY
         EXIT DISPLAY
   END DISPLAY
END FUNCTION