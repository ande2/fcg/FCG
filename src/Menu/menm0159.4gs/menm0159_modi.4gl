###############################################################################
# Funcion     : %M%
# ccliente : Modulo para la modificacion del catalogo 
# Funciones   : modi_init()
#               cat_modi()
#               cat_anul()
#               valida_modi()
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : ea
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        ccliente de la modificacion
################################################################################

GLOBALS "menm0159_glob.4gl"

FUNCTION modi_init()
DEFINE prepvar STRING 

   DECLARE lockcat CURSOR FOR
	SELECT proc_id
   FROM   bproc
	WHERE  bproc.proc_id = gr_reg.proc_id
	FOR UPDATE

   LET prepvar =  " UPDATE bproc SET ",
                  " proc_desc = ? ",
                  " WHERE CURRENT OF lockcat"

   PREPARE upd_cat FROM prepvar

   LET prepvar =  " UPDATE bproc SET ",
                  " est_id = ? ",
                  " WHERE CURRENT OF lockcat"

PREPARE anula_cat FROM prepvar
END FUNCTION

FUNCTION cat_modi()
END FUNCTION

FUNCTION valida_modi()
RETURN TRUE
END FUNCTION

FUNCTION HabilitaBitProc(lhabilita)
DEFINE lhabilita BOOLEAN 
DEFINE mensaje STRING 
   BEGIN WORK
   OPEN lockcat

   WHENEVER ERROR CONTINUE

   FETCH lockcat INTO gr_reg.proc_id

   WHENEVER ERROR STOP

   IF SQLCA.SQLCODE < 0 OR SQLCA.SQLCODE = NOTFOUND THEN
      ROLLBACK WORK
      CALL box_valdato("No es posible bloquear el registro para Modificarlo.")
   ELSE
      LET ur_reg.* = gr_reg.*
      LET int_flag = FALSE
      IF lhabilita THEN 
         LET gr_reg.est_id = 13
         IF NOT CreaTrigger() THEN 
            RETURN FALSE
         END IF 
      ELSE 
         LET gr_reg.est_id = 14
         IF NOT EliminaTrigger() THEN 
            RETURN FALSE 
         END IF 
      END IF 
      WHENEVER ERROR CONTINUE
      EXECUTE anula_cat USING  gr_reg.est_id
      WHENEVER ERROR STOP
      IF SQLCA.SQLCODE < 0 THEN
         LET mensaje = "El error numero ", SQLCA.SQLCODE USING "-<<<<<", " ha ocurrido."
         ROLLBACK WORK
         LET gr_reg.* = ur_reg.*
         CALL cat_desp15()
         CALL box_valdato(mensaje)
         RETURN FALSE
      END IF 
      COMMIT WORK
      CALL cat_desp15()
      CALL box_valdato("Registro fue Habilitado/Deshabilitado satisfactoriamente")
      RETURN TRUE
END IF 
CLOSE lockcat
RETURN FALSE

END FUNCTION 