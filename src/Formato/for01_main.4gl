GLOBALS "for01_glob.4gl"

MAIN

   LET g_fele.documlk = arg_val(1)

   WHENEVER ERROR CONTINUE
      SELECT fac_id INTO g_fele.fac_id FROM facturafel_e WHERE documlk = g_fele.documlk AND estado_doc = "ORIGINAL" AND estatus = "C"
   WHENEVER ERROR STOP

#TODO: Agregar anulado en el reporte
{SELECT estado_doc INTO vestado_doc
      FROM facturafel_e
      WHERE documlk = prfelg.documlk
      AND estado_doc = "ANULADO"
      AND estatus = "C"

      IF STATUS = NOTFOUND THEN
         LET vestado_doc = ""
      ELSE
         LET vestado_doc = "*** DOCUMENTO ANULADO ***"
      END IF
      Colocar anulado despues del www.nicol.com
      }


                     
   IF sqlca.sqlcode <> 0 THEN
      DISPLAY SFMT("Documento #%1 a procesar no existe", g_fele.documlk)
      RETURN 
   END IF 

   CALL input_report() 

END MAIN

FUNCTION input_report()
   DEFINE handler om.SaxDocumentHandler,
          rnombre, rformato, cmd STRING,
          rdestino INTEGER

   CLOSE WINDOW SCREEN

   LET rnombre  = "fel_form01"
   LET rformato = "PDF"
   LET rdestino = 1
   
   IF fgl_report_loadCurrentSettings(rnombre||".4rp") THEN
      CALL fgl_report_selectDevice(rformato)
      CALL fgl_report_selectPreview(rdestino)

      CALL fgl_report_setOutputFileName("/tmp/"||rnombre CLIPPED||
                      "."||rformato)

      LET handler = fgl_report_commitCurrentSettings()
   ELSE
      EXIT PROGRAM
   END IF

   IF handler IS NOT NULL THEN
      CALL run_report1_to_handler(handler)
   END IF
            
            
END FUNCTION

