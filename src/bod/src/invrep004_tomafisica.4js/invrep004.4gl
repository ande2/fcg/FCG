{ 
Programo : invrep004.4gl 
Objetivo : Reporte de toma fisico de inventario
}

DATABASE storepos 

{ Definicion de variables globales }

DEFINE w_mae_bod RECORD LIKE inv_mbodegas.*,
       w_mae_emp RECORD LIKE glb_empresas.*,
       w_mae_suc RECORD LIKE glb_sucsxemp.*,
       w_datos   RECORD
        codbod   LIKE inv_mtransac.codbod,
        codemp   LIKE inv_mtransac.codemp,
        codsuc   LIKE inv_mtransac.codbod,
        codcat   LIKE glb_categors.codcat,
        subcat   LIKE glb_subcateg.subcat,
        ordrep   SMALLINT 
       END RECORD,
       existe    SMALLINT,
       pagina    SMALLINT,
       filename  CHAR(100),
       fcodcat   STRING, 
       fsubcat   STRING 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar7")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rtomafisico")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep004_tomafisica()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep004_tomafisica()
 DEFINE w_pro_bod         RECORD LIKE inv_proenbod.*,
        w_mae_pro         RECORD LIKE inv_products.*,
        imp1              RECORD
         codemp           LIKE inv_proenbod.codemp, 
         codsuc           LIKE inv_proenbod.codsuc, 
         codbod           LIKE inv_proenbod.codbod,
         codcat           LIKE inv_products.codcat,
         nomcat           LIKE glb_categors.nomcat,
         subcat           LIKE inv_products.subcat,
         nomsub           LIKE glb_subcateg.nomsub, 
         cditem           LIKE inv_proenbod.cditem, 
         codabr           CHAR(20),
         despro           CHAR(70),
         nommed           CHAR(15),
         presug           LIKE inv_products.presug 
        END RECORD,
        wpais             VARCHAR(255),
        pipeline          VARCHAR(100), 
        qrytxt            STRING,
        strcodcat         STRING,
        strsubcat         STRING,
        strcditem         STRING,
        strordenr         STRING,
        loop              SMALLINT

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep004a AT 5,2
  WITH FORM "invrep004a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invrep004",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/TomaFisico.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de bodegas x usuario
  CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   CLEAR FORM
   LET w_datos.ordrep = 1 
   LET pagina = FALSE 

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codbod,
                 w_datos.codcat,
                 w_datos.subcat,
                 w_datos.ordrep 
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     -- Verificando datos 
     IF w_datos.codbod IS NULL THEN 
        ERROR "Error: deben completarse los filtos de seleccion." 
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf3" 
     --LET pipeline = "pdf" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     -- Verificando datos 
     IF w_datos.codbod IS NULL THEN
        ERROR "Error: debe seleccionarse la bodega ..." 
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON CHANGE codbod
     -- Obteniendo datos de la bodega
     CALL librut003_bbodega(w_datos.codbod)
     RETURNING w_mae_bod.*,existe

     -- Asignando datos de empresa y sucursal de la bodega
     LET w_datos.codemp = w_mae_bod.codemp
     LET w_datos.codsuc = w_mae_bod.codsuc

     -- Obteniendo datos de la empresa
     CALL librut003_bempresa(w_mae_bod.codemp)
     RETURNING w_mae_emp.*,existe
     -- Obteniendo datos de la sucursal
     CALL librut003_bsucursal(w_mae_bod.codsuc)
     RETURNING w_mae_suc.*,existe

     -- Desplegando datos de la empresa y sucursal
     DISPLAY BY NAME w_datos.codemp,w_datos.codsuc,w_mae_emp.nomemp,w_mae_suc.nomsuc

     -- Limpiando combos de categorias y subcategorias
     LET w_datos.codcat = NULL
     LET w_datos.subcat = NULL
     CLEAR codcat,subcat

    ON CHANGE codcat
     -- Limpiando combos
     LET w_datos.subcat = NULL
     CLEAR subcat

     -- Llenando combox de subcategorias
     IF w_datos.codcat IS NOT NULL THEN 
        CALL librut003_cbxsubcategorias(w_datos.codcat)
     END IF 

    AFTER FIELD codbod
     -- Verificando bodega 
     IF w_datos.codbod IS NULL THEN
        ERROR "Error: debe de seleccionarse la bodega a listar."
        NEXT FIELD codbod
     END IF

    AFTER INPUT
     -- Verificando datos
     IF w_datos.codbod IS NULL OR
        pipeline IS NULL THEN
        NEXT FIELD codbod
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Verificando seleccion de categoria
   LET strcodcat = NULL
   IF w_datos.codcat IS NOT NULL THEN
      LET strcodcat = "AND d.codcat = ",w_datos.codcat
   END IF

   -- Verificando condicion de subcategoria
   LET strsubcat = NULL
   IF w_datos.subcat IS NOT NULL THEN
      LET strsubcat = "AND e.subcat = ",w_datos.subcat
   END IF

   -- Verificando orden del reporte
   LET strordenr = NULL
   CASE (w_datos.ordrep)
    WHEN 1 -- Alfabetico
     LET strordenr = " ORDER BY 1,2,3,4,6,10"
    WHEN 2 -- Codigo
     LET strordenr = " ORDER BY 1,2,3,4,6,9"
   END CASE

   -- Construyendo seleccion 
   LET qrytxt = "SELECT a.codemp,a.codsuc,a.codbod,b.codcat,d.nomcat,",
                       "b.subcat,e.nomsub,a.cditem,b.codabr,b.despro,",
                       "c.nommed,b.presug ", 
                  "FROM inv_proenbod a,inv_products b,inv_unimedid c,",
                       "glb_categors d,glb_subcateg e ",
                  "WHERE a.codemp = ",w_datos.codemp,
                   " AND a.codsuc = ",w_datos.codsuc,
                   " AND a.codbod = ",w_datos.codbod,
                   " AND b.estado = 1 ",
                   " AND b.cditem = a.cditem ",
                   " AND c.unimed = b.unimed ",
                   " AND d.codcat = b.codcat ",
                   " AND e.codcat = b.codcat ",
                   " AND e.subcat = b.subcat ",
                   strcodcat CLIPPED," ",
                   strsubcat CLIPPED," ",
                   strordenr CLIPPED

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep004 FROM qrytxt 
   DECLARE c_crep004 CURSOR FOR c_rep004
   LET existe = FALSE
   FOREACH c_crep004 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE
       START REPORT invrep004_invtomafisica TO filename
    END IF 

    -- Llenando reporte
    OUTPUT TO REPORT invrep004_invtomafisica(imp1.*)
   END FOREACH
   CLOSE c_crep004 
   FREE  c_crep004 

   IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT invrep004_invtomafisica 

    -- Enviando reporte al destino seleccionado
    CALL librut001_sendreport
    (filename,pipeline,"Toma de Inventario Fisico",
    "--noline-numbers "||
    "--nofooter "||
    "--font-size 8 "||
    "--page-width 842 --page-height 595 "||
    "--left-margin 55 --right-margin 25 "||
    "--top-margin 35 --bottom-margin 45 "||
    "--title Inventarios")

    ERROR "" 
    CALL fgl_winmessage(
    " Atencion","Reporte Emitido ...","information") 
   ELSE
    ERROR "" 
    CALL fgl_winmessage(
    " Atencion","No existen datos con el filtro seleccionado.","stop")
   END IF 
  END WHILE
 CLOSE WINDOW wrep004a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT invrep004_invtomafisica(imp1)
 DEFINE imp1              RECORD
         codemp           LIKE inv_proenbod.codemp, 
         codsuc           LIKE inv_proenbod.codsuc, 
         codbod           LIKE inv_proenbod.codbod,
         codcat           LIKE inv_products.codcat,
         nomcat           LIKE glb_categors.nomcat,
         subcat           LIKE inv_products.subcat,
         nomsub           LIKE glb_subcateg.nomsub,
         cditem           LIKE inv_proenbod.cditem, 
         codabr           CHAR(20),
         despro           CHAR(70),
         nommed           CHAR(15),
         presug           LIKE inv_products.presug 
        END RECORD,
        linea             CHAR(154)

  OUTPUT PAGE   LENGTH 50 
         LEFT   MARGIN 0
         TOP    MARGIN 0
         BOTTOM MARGIN 0 

  FORMAT 
   PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________",
                "____"
   
    -- Imprimiendo Encabezado
    PRINT COLUMN   1,"Inventarios",
	  COLUMN 134,PAGENO USING "Pagina: <<<<"
    PRINT COLUMN   1,"Invrep004",
          COLUMN  53,"T O M A  D E  I N V E N T A R I O  F I S I C O",
          COLUMN 134,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN 134,"Hora  : ",TIME 
    SKIP 1 LINES 
    PRINT COLUMN   1,"INVENTARIO REALIZADO EL ____________________" 
    PRINT linea 
    PRINT "Codigo                Descripcion del Producto                            ",
          "                    Unidad                               E X I S T E N C I A"
    PRINT "Producto                                                                  ",
          "                    Medida                               F I S I C A" 
    PRINT linea

   BEFORE GROUP OF imp1.codbod
    SKIP 1 LINES

   BEFORE GROUP OF imp1.codcat
    -- Imprimiendo datos de la categoria
    IF NOT pagina THEN 
       LET pagina = TRUE 
    ELSE 
       SKIP TO TOP OF PAGE 
    END IF 

    -- Imprimiendo datos de la bodega 
    PRINT "EMPRESA   : ",imp1.codemp USING "<<<"," ",w_mae_emp.nomemp CLIPPED
    PRINT "SUCURSAL  : ",imp1.codsuc USING "<<<"," ",w_mae_suc.nomsuc CLIPPED
    PRINT "BODEGA    : ",imp1.codbod USING "<<<"," ",w_mae_bod.nombod CLIPPED
    PRINT "CATEGORIA : ",imp1.codcat USING "<<<"," ",imp1.nomcat CLIPPED

   BEFORE GROUP OF imp1.subcat
    -- Imprimiendo datos de la sub-categoria
    SKIP 1 LINES
    PRINT "[ SUB-CATEGORIA : ",imp1.subcat USING "<<<"," ",imp1.nomsub CLIPPED," ]"
    SKIP 1 LINES 

   ON EVERY ROW
    -- Imprimiendo datos
    PRINT COLUMN   1,imp1.codabr                                 ,
          COLUMN  23,imp1.despro                                 ,2  SPACES,
                     imp1.nommed                                 ,12 SPACES,
                     "______________________________"

    SKIP 1 LINES 

   AFTER GROUP OF imp1.codcat
    -- Imprimiendo datos de la categoria
    SKIP 1 LINES
    PRINT COLUMN   1,"TOTAL ",GROUP COUNT(*) USING "<<<,<<&",
                     " PRODUCTO[S] en CATEGORIA ",imp1.nomcat CLIPPED
END REPORT 
