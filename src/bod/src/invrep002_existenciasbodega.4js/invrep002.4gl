{ 
Fecha    : Enero 2011
Programo : invrep002.4gl 
Objetivo : Reporte de existencias en bodega. 
}

DATABASE storepos 

{ Definicion de variables globales }

CONSTANT progname = "invrep002" 

DEFINE w_mae_bod RECORD LIKE inv_mbodegas.*,
       w_mae_emp RECORD LIKE glb_empresas.*,
       w_mae_suc RECORD LIKE glb_sucsxemp.*,
       w_datos   RECORD
        codbod   LIKE inv_mtransac.codbod,
        codemp   LIKE inv_mtransac.codemp,
        codsuc   LIKE inv_mtransac.codbod,
        fecexi   DATE, 
        codcat   LIKE glb_categors.codcat,
        subcat   LIKE glb_subcateg.subcat,
        codmar   LIKE inv_products.codmar,
        exiact   SMALLINT,
        eximin   SMALLINT, 
        eximax   SMALLINT
       END RECORD,
       fnt       RECORD
        cmp      CHAR(12),
        nrm      CHAR(12),
        tbl      CHAR(12),
        fbl,t88  CHAR(12),
        t66,p12  CHAR(12),
        p10,srp  CHAR(12),
        twd      CHAR(12),
        fwd      CHAR(12),
        tda,fda  CHAR(12),
        ini      CHAR(12)
       END RECORD,
       existe    SMALLINT,
       filename  STRING,
       fcodcat   STRING,
       fsubcat   STRING,
       fexiact   STRING,
       feximin   STRING,
       feximax   STRING, 
       fcodmar   STRING

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar7")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rexistencias")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep002_existencias()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep002_existencias()
 DEFINE w_pro_bod         RECORD LIKE inv_proenbod.*,
        w_mae_pro         RECORD LIKE inv_products.*,
        imp1              RECORD
         codemp           LIKE inv_proenbod.codemp, 
         codsuc           LIKE inv_proenbod.codsuc, 
         codbod           LIKE inv_proenbod.codbod,
         codcat           LIKE glb_categors.codcat,
         nomcat           LIKE glb_categors.nomcat,
         subcat           LIKE glb_subcateg.subcat,
         nomsub           LIKE glb_subcateg.nomsub,
         cditem           LIKE inv_proenbod.cditem, 
         codabr           CHAR(20),
         dsitem           CHAR(42),
         nommed           CHAR(20),
         preuni           LIKE inv_proenbod.cospro, 
         eximin           LIKE inv_proenbod.eximin,
         eximax           LIKE inv_proenbod.eximax,
         exican           LIKE inv_proenbod.exican,
         fulent           LIKE inv_proenbod.fulent,
         fulsal           LIKE inv_proenbod.fulsal 
        END RECORD,
        wsalval           DEC(14,2), 
        wpais             VARCHAR(255),
        pipeline,qrytxt   STRING,
        strcodcat         STRING,
        strsubcat         STRING,
        streximin         STRING,
        streximax         STRING,
        strexiact         STRING,
        strcodmar         STRING,
        loop              SMALLINT

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep002a AT 5,2
  WITH FORM "invrep002a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/Existencias.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de bodegas x usuario
  CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()
  -- Llenando combox de marcas
  CALL librut003_CbxMarcas()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   LET w_datos.exiact = 3 
   LET w_datos.eximin = 3 
   LET w_datos.eximax = 3 
   LET w_datos.fecexi = TODAY 
   CLEAR FORM

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codbod,
                 w_datos.fecexi,
                 w_datos.codcat,
                 w_datos.subcat,
                 w_datos.codmar,
                 w_datos.exiact,
                 w_datos.eximax, 
                 w_datos.eximin 
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)
     LET fcodmar = GET_FLDBUF(w_datos.codmar)

     -- Verificando datos 
     IF w_datos.codbod IS NULL THEN
        CALL fgl_winmessage(
        "Atencion","Deben completarse los filtros de seleccion, Verifica.","stop")
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)
     LET fcodmar = GET_FLDBUF(w_datos.codmar)

     -- Verificando datos 
     IF w_datos.codbod IS NULL THEN
        CALL fgl_winmessage(
        "Atencion","Deben completarse los filtros de seleccion, Verifica.","stop")
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON CHANGE codbod
     -- Obteniendo datos de la bodega
     CALL librut003_bbodega(w_datos.codbod)
     RETURNING w_mae_bod.*,existe

     -- Asignando datos de empresa y sucursal de la bodega
     LET w_datos.codemp = w_mae_bod.codemp
     LET w_datos.codsuc = w_mae_bod.codsuc

     -- Obteniendo datos de la empresa
     CALL librut003_bempresa(w_mae_bod.codemp)
     RETURNING w_mae_emp.*,existe
     -- Obteniendo datos de la sucursal
     CALL librut003_bsucursal(w_mae_bod.codsuc)
     RETURNING w_mae_suc.*,existe

     -- Desplegando datos de la empresa y sucursal
     DISPLAY BY NAME w_datos.codemp,w_datos.codsuc,w_mae_emp.nomemp,w_mae_suc.nomsuc

     -- Limpiando combos de categorias y subcategorias
     LET w_datos.codcat = NULL
     LET w_datos.subcat = NULL
     CLEAR codcat,subcat

    AFTER FIELD codbod
     -- Verificando bodega 
     IF w_datos.codbod IS NULL THEN
        CALL fgl_winmessage(
        "Atencion","Deben completarse los filtros de seleccion, Verifica.","stop")
        NEXT FIELD codbod
     END IF

    AFTER FIELD fecexi
     -- Verificando fecha
     IF w_datos.fecexi IS NULL THEN
        LET w_datos.fecexi = TODAY
        DISPLAY BY NAME w_datos.fecexi
     END IF 

    ON CHANGE codcat
     -- Limpiando combos
     LET w_datos.subcat = NULL
     CLEAR subcat

     -- Llenando combox de subcategorias
     IF w_datos.codcat IS NOT NULL THEN 
        CALL librut003_cbxsubcategorias(w_datos.codcat)
     END IF 

    AFTER INPUT 
     -- Verificando datos
     IF w_datos.codbod IS NULL OR
        pipeline IS NULL THEN
        NEXT FIELD codbod
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Verificando seleccion de categoria
   LET strcodcat = NULL
   IF w_datos.codcat IS NOT NULL THEN
      LET strcodcat = "AND d.codcat = ",w_datos.codcat
   END IF

   -- Verificando condicion de subcategoria
   LET strsubcat = NULL
   IF w_datos.subcat IS NOT NULL THEN
      LET strsubcat = "AND e.subcat = ",w_datos.subcat
   END IF

   -- Verificando condicion de marca
   LET strcodmar = NULL
   IF w_datos.codmar IS NOT NULL THEN
      LET strcodmar = "AND b.codmar = ",w_datos.codmar 
   END IF

   -- Verificando condicion de existencia actual
   LET strexiact = NULL
   CASE (w_datos.exiact)
    WHEN 1    LET strexiact = "AND a.exican >0"
              LET fexiact   = ">0" 
    WHEN 2    LET strexiact = "AND a.exican =0"
              LET fexiact   = "=0" 
    OTHERWISE LET strexiact = NULL
              LET fexiact   = "Todas" 
   END CASE 

   -- Verificando condicion de existencia minima 
   LET streximin = NULL
   CASE (w_datos.eximin)
    WHEN 1    LET streximin = "AND a.eximin >0"
              LET feximin   = ">0" 
    WHEN 2    LET streximin = "AND a.eximin =0"
              LET feximin   = "=0" 
    OTHERWISE LET streximin = NULL
              LET feximin   = "Todas" 
   END CASE 

   -- Verificando condicion de existencia maxima
   LET streximax = NULL
   CASE (w_datos.eximax)
    WHEN 1    LET streximax = "AND a.eximax >0"
              LET feximax   = ">0" 
    WHEN 2    LET streximax = "AND a.eximax =0"
              LET feximax   = "=0" 
    OTHERWISE LET streximax = NULL
              LET feximax   = "Todas" 
   END CASE 

   -- Construyendo seleccion 
   LET qrytxt = 
     "SELECT a.codemp,a.codsuc,a.codbod,b.codcat,d.nomcat,b.subcat,",
            "e.nomsub,a.cditem,b.codabr,b.despro,c.nommed,a.cospro,",
            "a.eximin,a.eximax,a.exican,a.fulent,a.fulsal ",
      "FROM inv_proenbod a,inv_products b,inv_unimedid c,glb_categors d,",
            "glb_subcateg e ",
      "WHERE a.codemp = ",w_datos.codemp,
       " AND a.codsuc = ",w_datos.codsuc,
       " AND a.codbod = ",w_datos.codbod,
       " AND b.cditem = a.cditem ",
       strcodmar CLIPPED," ",
       " AND b.estado = 1 ", 
       " AND c.unimed = b.unimed ",
       " AND d.codcat = b.codcat ",
       " AND e.codcat = b.codcat ",
       " AND e.subcat = b.subcat ",
       strcodcat CLIPPED," ",
       strsubcat CLIPPED," ",
       strexiact CLIPPED," ",
       streximin CLIPPED," ",
       streximax CLIPPED," ",
       " ORDER BY 1,2,3,4,6"

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep002 FROM qrytxt 
   DECLARE c_crep002 CURSOR FOR c_rep002
   LET existe = FALSE
   FOREACH c_crep002 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       -- Seleccionando fonts para impresora 
       CALL librut001_fontsprn(pipeline,"pdf")
       RETURNING fnt.*

       LET existe = TRUE
       LET existe = TRUE
       START REPORT invrep002_impinvbod TO filename
    END IF 

    -- Verificando si existencia es anterior al dia
    IF w_datos.fecexi<TODAY THEN
       -- Calculando existencia del producto 
       CALL librut003_saldoantxpro(imp1.codemp,
                                   imp1.codsuc,
                                   imp1.codbod,
                                   imp1.cditem,
                                   (w_datos.fecexi+1))
       RETURNING imp1.exican,wsalval 
    END IF 

    -- Verificando fechas
    IF (imp1.fulent>w_datos.fecexi) THEN
       LET imp1.fulent = NULL
    END IF 
    IF (imp1.fulsal>w_datos.fecexi) THEN
       LET imp1.fulsal = NULL
    END IF 

    -- Llenando el reporte
    OUTPUT TO REPORT invrep002_impinvbod(imp1.*)
   END FOREACH
   CLOSE c_crep002 
   FREE  c_crep002 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT invrep002_impinvbod 

      -- Enviando reporte al destino seleccionado
      CALL librut001_sendreport
      (filename,pipeline,"Existencias en Bodega",
      "--noline-numbers "||
      "--nofooter "||
      "--font-size 8 "||
      "--page-width 842 --page-height 595 "||
      "--left-margin 55 --right-margin 25 "||
      "--top-margin 35 --bottom-margin 45 "||
      "--title Inventarios")

      ERROR "" 
      CALL fgl_winmessage(
      "Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      "Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE
 CLOSE WINDOW wrep002a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT invrep002_impinvbod(imp1)
 DEFINE imp1      RECORD
         codemp   LIKE inv_proenbod.codemp, 
         codsuc   LIKE inv_proenbod.codsuc, 
         codbod   LIKE inv_proenbod.codbod,
         codcat   LIKE glb_categors.codcat,
         nomcat   LIKE glb_categors.nomcat,
         subcat   LIKE glb_subcateg.subcat,
         nomsub   LIKE glb_subcateg.nomsub,
         cditem   LIKE inv_proenbod.cditem, 
         codabr   CHAR(20),
         dsitem   CHAR(42),
         nommed   CHAR(20),
         preuni   LIKE inv_proenbod.cospro, 
         eximin   LIKE inv_proenbod.eximin,
         eximax   LIKE inv_proenbod.eximax,
         exican   LIKE inv_proenbod.exican,
         fulent   LIKE inv_proenbod.fulent,
         fulsal   LIKE inv_proenbod.fulsal 
        END RECORD,
        linea     CHAR(158),
        exis      SMALLINT

  OUTPUT LEFT MARGIN 0 
         PAGE LENGTH 50 
         TOP MARGIN 0 
         BOTTOM MARGIN 0 

  FORMAT 
   PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________",
                "________"


    -- Imprimiendo Encabezado
    PRINT COLUMN   1,"Inventarios",
	  COLUMN 137,PAGENO USING "Pagina: <<<<"
    PRINT COLUMN   1,"Invrep002",
          COLUMN  69,"Existencias en Bodega",
          COLUMN 137,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN  73,"AL ",w_datos.fecexi, 
          COLUMN 137,"Hora  : ",TIME 
    PRINT linea 
    PRINT "Codigo                Descripcion del Producto                      Precio  ",
          "  Existencia   Existencia    Existencia         Precio  Fec/Ultima  Fec/Ultima" 
    PRINT "Producto                                                            Unitario",
          "  Minima       Maxima        Actual             TOTAL   Entrada     Salida" 
    PRINT linea

   BEFORE GROUP OF imp1.codbod 
    -- Imprimiendo datos de la bodega
    PRINT "EMPRESA  : ",imp1.codemp USING "<<<"," ",w_mae_emp.nomemp CLIPPED
    PRINT "SUCURSAL : ",imp1.codsuc USING "<<<"," ",w_mae_suc.nomsuc CLIPPED
    PRINT "BODEGA   : ",imp1.codbod USING "<<<"," ",w_mae_bod.nombod CLIPPED
    SKIP 1 LINES

   BEFORE GROUP OF imp1.codcat
    -- Imprimiendo datos de la categoria
    SKIP 1 LINES 
    PRINT "[ CATEGORIA : ",imp1.codcat USING "<<<"," ",imp1.nomcat CLIPPED," ]"

   BEFORE GROUP OF imp1.subcat 
    -- Imprimiendo datos de la sub-categoria
    SKIP 1 LINES 
    PRINT "* SUB-CATEGORIA : ",imp1.subcat USING "<<<"," ",imp1.nomsub CLIPPED," *"
 
   ON EVERY ROW
    -- Imprimiendo productos
    PRINT COLUMN   1,imp1.codabr                                 ,2 SPACES,
                     imp1.dsitem                                 ,2 SPACES,
                     imp1.preuni          USING  "---,--&.&&"    ,2 SPACES,
                     imp1.eximin          USING  "---,--&.&&"    ,2 SPACES,
                     imp1.eximax          USING  "---,--&.&&"    ,2 SPACES,
                     imp1.exican          USING  "--,---,--&.&&" ,2 SPACES,
                     (imp1.preuni*
                     imp1.exican)         USING  "--,---,--&.&&" ,2 SPACES,
                     imp1.fulent          USING  "dd/mm/yyyy"    ,2 SPACES,
                     imp1.fulsal          USING  "dd/mm/yyyy" 

   AFTER GROUP OF imp1.subcat 
    -- Imprimiendo datos de la sub-categoria
    PRINT COLUMN   1,"TOTAL ",GROUP COUNT(*) USING "<<<,<<&",
                     " PRODUCTO[S] EN SUB-CATEGORIA ",imp1.nomsub CLIPPED, 
          COLUMN 118,GROUP SUM(imp1.preuni*
                     imp1.exican)         USING  "--,---,--&.&&" 

   AFTER GROUP OF imp1.codcat
    -- Imprimiendo datos de la categoria
    SKIP 1 LINES 
    PRINT COLUMN   1,"TOTAL ",GROUP COUNT(*) USING "<<<,<<&",
                     " PRODUCTO[S] EN CATEGORIA ",imp1.nomcat CLIPPED,
          COLUMN 118,GROUP SUM(imp1.preuni*
                     imp1.exican)         USING  "--,---,--&.&&" 
 
   AFTER GROUP OF imp1.codbod
    -- Totalizando por bodega
    SKIP 1 LINES
    PRINT COLUMN   1,"TOTAL ",GROUP COUNT(*) USING "<<<,<<&",
                     " PRODUCTO[S] EN BODEGA",
          COLUMN 118,GROUP SUM(imp1.preuni*
                     imp1.exican)         USING  "--,---,--&.&&" 
    SKIP 1 LINES 

   ON LAST ROW
    -- Imprimiendo filtros
    IF (LENGTH(fcodcat)>0) THEN
       PRINT "CATEGORIA         : ",fcodcat
    END IF
    IF (LENGTH(fsubcat)>0) THEN
       PRINT "SUBCATEGORIA      : ",fsubcat
    END IF
    IF (LENGTH(fcodmar)>0) THEN
       PRINT "MARCA             : ",fcodmar 
    END IF
    PRINT "EXISTENCIA ACTUAL : ",fexiact 
    PRINT "EXISTENCIA MINIMA : ",feximin 
    PRINT "EXISTENCIA MAXIMA : ",feximax 
END REPORT 
