{ 
Fecha    : Diciiembre 2011        
Programo : Mynor Ramirez
Objetivo : Programa de criterios de seleccion para:
           Consulta ingreso de inventario fisicos
}

-- Definicion de variables globales 
GLOBALS "invglb002.4gl" 
DEFINE xcoddes LIKE inv_mtransac.coddes, 
       xcodori LIKE inv_mtransac.codori

-- Subrutina para consultar ingresos de inventario fisico

FUNCTION invqbx002_fisicos()
 DEFINE qrytext,qrypart        STRING, 
        loop,existe,opc        SMALLINT,
        qryres,msg             CHAR(80),
        ultfechaabs            DATE 

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Obteniendo parametro de codigo de proveedor para nota de absorcion positiva 
 CALL librut003_parametros(13,14)
 RETURNING existe,xcodori 
 IF NOT existe THEN
    CALL fgl_winmessage(
    "Atencion:",
    "Parametro de codigo de proveedor para nota absorcion positiva no definido.\n"||
    "Debe definirse antes de poder hacer absorciones.",
    "stop")
    RETURN
 END IF

 -- Obteniendo parametro de codigo de cliente para nota de absorcion negativa
 CALL librut003_parametros(13,15)
 RETURNING existe,xcoddes 
 IF NOT existe THEN
    CALL fgl_winmessage(
    "Atencion:",
    "Parametro de codigo de cliente para nota absorcion negativa no definido.\n"||
    "Debe definirse antes de poder hacer absorciones.",
    "stop")
    RETURN
 END IF

 -- Buscando datos 
 LET loop = TRUE 
 WHILE loop 
  -- Inicializando las variables 
  INITIALIZE qrytext,qrypart TO NULL
  LET int_flag = 0 
  CALL inving002_inival(1)

  -- Construyendo busqueda 
  CONSTRUCT BY NAME qrytext 
                 ON a.codbod, 
                    a.fecinv 

   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT CONSTRUCT

   ON ACTION calculator 
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
      CALL fgl_winmessage(
      "Atencion","Calculadora no disponible.","stop") 
    END IF

  END CONSTRUCT
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Preparando la busqueda 
  ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

  -- Creando la busqueda 
  LET qrypart = "SELECT UNIQUE a.codemp,a.codsuc,a.codbod,a.fecinv ",
                 "FROM  inv_tofisico a ",
                 "WHERE EXISTS (SELECT b.userid ",
                                "FROM inv_permxbod b ",
                                "WHERE b.userid = '",  username CLIPPED, "'", --LOGNAMEUSER 
                                " AND b.codbod = a.codbod) ",
                  " AND ", qrytext CLIPPED,
                  " ORDER BY a.codemp,a.codsuc,a.codbod,a.fecinv DESC" 

  -- Declarando el cursor 
  PREPARE estqbe002 FROM qrypart
  DECLARE c_fisico SCROLL CURSOR WITH HOLD FOR estqbe002  
  OPEN c_fisico 
  FETCH FIRST c_fisico INTO w_mae_fis.codemp,w_mae_fis.codsuc,w_mae_fis.codbod,w_mae_fis.fecinv
  IF (status = NOTFOUND) THEN
     INITIALIZE w_mae_fis.* TO NULL
     ERROR ""
     CALL fgl_winmessage(
     "Atencion",
     "No existen inventarios fisicos con el criterio seleccionado.",
     "stop") 
  ELSE
     ERROR "" 

     -- Desplegando datos 
     CALL invqbx002_datos()

     -- Fetchando inventarios fisicos
     MENU "Consultar" 
      BEFORE MENU
       -- Verificando permiso de opcion de Absorcion 
       IF NOT seclib001_accesos(progname,4,username) THEN
          HIDE OPTION "absorcion"
       END IF
      COMMAND "Siguiente"
       "Visualiza el siguiente inventario fisico en la lista."
       FETCH NEXT c_fisico INTO w_mae_fis.codemp,w_mae_fis.codsuc,
                                w_mae_fis.codbod,w_mae_fis.fecinv

        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas inventarios fisicos siguientes en lista.", 
           "information")
           FETCH LAST c_fisico INTO w_mae_fis.codemp,w_mae_fis.codsuc,
                                    w_mae_fis.codbod,w_mae_fis.fecinv
       END IF 

       -- Desplegando datos 
       CALL invqbx002_datos()

      COMMAND "Anterior"
       "Visualiza el inventario fisico anterior en la lista."
       FETCH PREVIOUS c_fisico INTO w_mae_fis.codemp,w_mae_fis.codsuc,
                                    w_mae_fis.codbod,w_mae_fis.fecinv

        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas inventarios fisicos anteriores en lista.", 
           "information")
           FETCH FIRST c_fisico INTO w_mae_fis.codemp,w_mae_fis.codsuc,
                                     w_mae_fis.codbod,w_mae_fis.fecinv
        END IF

        -- Desplegando datos 
        CALL invqbx002_datos()

      COMMAND "Primero" 
       "Visualiza el primer inventario fisico en la lista."
       FETCH FIRST c_fisico INTO w_mae_fis.codemp,w_mae_fis.codsuc,
                                 w_mae_fis.codbod,w_mae_fis.fecinv

        -- Desplegando datos 
        CALL invqbx002_datos()

      COMMAND "Ultimo" 
       "Visualiza el ultimo inventario fisico en la lista."
       FETCH LAST c_fisico INTO w_mae_fis.codemp,w_mae_fis.codsuc,
                                w_mae_fis.codbod,w_mae_fis.fecinv

        -- Desplegando datos
        CALL invqbx002_datos()

      COMMAND "Productos"
       "Permite visualizar el detalle completo de los productos del inventario fisico."
       CALL invqbx002_verproductos()

      ON ACTION absorcion
       -- Proceso de absorcion 

       -- Verificando su ya existe absorcion
       IF (inving002_ExisteAbsorcion()>0) THEN 
        CALL fgl_winmessage(
        "Atencion",
        "Inventario fisico ya con absorciones realizadas.",
        "stop")
        CONTINUE MENU  
       END IF 

       -- Mostrando mensaje de advertencia 
       CALL fgl_winmessage(
       "Absorcion de Invetario Fisico",
       "Este proceso ajusta saldos del inventario en base a las diferencias\n"||
       "registradas, por lo que debe estar seguro de ejecutar este proceso.\n"||
       "Presione Aceptar para continuar.",
       "warning") 

       -- Inicializando vectores de datos
       CALL inving002_inivec()

       -- Recargando productos y recalculando existencias 
       CALL inving002_CargaProductosBodega()

       -- Desplegando datos y preparandose para la absorcion 
       DISPLAY ARRAY v_products TO s_products.*
        ON ACTION cancel
         -- Salida sin absorcion 
         EXIT DISPLAY
        ON ACTION accept
         -- Procesando absorcion 
         -- Confirmacion absorcion 
         LET msg = "Esta SEGURO de ejecutar ABSORCION ? "
         LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

         -- Verificando operacion
         IF (opc=1) THEN 
            -- Creando absorcion positiva 
            CALL invqbx002_TransaccionAbsorcion(1) 
            -- Creando absorcion negativa  
            CALL invqbx002_TransaccionAbsorcion(0) 

            CALL fgl_winmessage(
            "Atencion",
            "Proceso de absorcion completado.",
            "information")
            
            EXIT DISPLAY 
         END IF 
       END DISPLAY

      ON ACTION eliminar
       -- Verificando si existe absrocion que eliminar
       IF (inving002_ExisteAbsorcion()=0) THEN
          CALL fgl_winmessage(
          "Atencion",
          "No existe absorcion para la fecha de inventario, VERIFICA.",
          "stop")
          CONTINUE MENU 
       END IF

       -- Verificando que sea ultima absorcion 
       LET ultfechaabs = inving002_UltimaFechaAbsorcion() 
       IF ultfechaabs!=w_mae_fis.fecinv THEN 
          CALL fgl_winmessage(
          "Atencion",
          "Solo puede eliminarse ultima absorcion realizada, VERIFICA.",
          "stop")
          CONTINUE MENU 
       END IF

       -- Eliminar absorcion 
       LET msg = "Esta SEGURO de eliminar ABSORCION ? "
       LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)
       IF (opc=1) THEN
          BEGIN WORK

           -- Eliminando absorcion
           DELETE FROM inv_mtransac
           WHERE inv_mtransac.codemp = w_mae_fis.codemp
             AND inv_mtransac.codsuc = w_mae_fis.codsuc
             AND inv_mtransac.codbod = w_mae_fis.codbod
             AND inv_mtransac.tipmov IN (2,3)
             AND inv_mtransac.aniotr = w_mae_fis.aniotr
             AND inv_mtransac.mestra = w_mae_fis.mestra
             AND inv_mtransac.fecemi = w_mae_fis.fecinv

         COMMIT WORK

         CALL fgl_winmessage(
         "Atencion",
         "Eliminacion de absorcion completada.",
         "information")
       END IF 

      ON ACTION cancel    
       EXIT MENU

      COMMAND KEY(F4,CONTROL-E)
       EXIT MENU
     END MENU
   END IF     
  CLOSE c_fisico
 END WHILE

 -- Inicializando datos 
 CALL inving002_inival(1)
END FUNCTION 

-- Subrutina para desplegar los datos del inventarrio 

FUNCTION invqbx002_datos()
 DEFINE existe SMALLINT

 -- Desplegando datos 
 CLEAR FORM

 -- Obteniendo datos de la empresa
 CALL librut003_bempresa(w_mae_fis.codemp)
 RETURNING w_mae_emp.*,existe

 -- Obteniendo datos de la sucursal
 CALL librut003_bsucursal(w_mae_fis.codsuc)
 RETURNING w_mae_suc.*,existe

 -- Desplegando datos del inventario 
 CLEAR FORM 
 DISPLAY BY NAME w_mae_fis.codbod,w_mae_fis.fecinv, 
                 w_mae_emp.nomemp,w_mae_suc.nomsuc

 -- Seleccionando detalle del inventario 
 CALL invqbx002_detalle()
END FUNCTION 

-- Subrutina para seleccionar datos del detalle del inventario

FUNCTION invqbx002_detalle()
 DEFINE existe  SMALLINT,
        msg     STRING 

 -- Asignando datos de fecha y anio
 LET w_mae_fis.aniotr = YEAR(w_mae_fis.fecinv)
 LET w_mae_fis.mestra = MONTH(w_mae_fis.fecinv)

 -- Inicializando vector de productos
 CALL inving002_inivec()

 DECLARE cdet CURSOR FOR
 SELECT x.cditem, 
        x.codabr,
        y.despro,
        z.nommed, 
        x.cancon, 
        x.canepq, 
        x.totepq,
        x.canhhd, 
        x.cansue,
        x.canuni,
        x.canexi,
        (x.canuni-x.canexi),
        "" 
  FROM  inv_tofisico x, inv_products y,inv_unimedid z
  WHERE x.codemp = w_mae_fis.codemp
    AND x.codsuc = w_mae_fis.codsuc
    AND x.codbod = w_mae_fis.codbod
    AND x.aniotr = w_mae_fis.aniotr
    AND x.mestra = w_mae_fis.mestra
    AND x.fecinv = w_mae_fis.fecinv
    AND y.cditem = x.cditem 
    AND z.unimed = y.unimed 
  ORDER BY 3 

  LET totlin = 1 
  FOREACH cdet INTO v_products[totlin].*
   -- Incrementando contador
   LET totlin = (totlin+1) 
  END FOREACH
  CLOSE cdet
  FREE  cdet
  LET totlin = (totlin-1)  

 -- Desplegando datos
 DISPLAY ARRAY v_products TO s_products.* 
  BEFORE DISPLAY
   EXIT DISPLAY 
 END DISPLAY 

 -- Desplegando totales
 LET msg = "Total Productos (",totlin USING "<<<,<<<",")"
 CALL f.setElementText("labelt",msg)
END FUNCTION 

-- Subrutina para ver el detalle de productos

FUNCTION invqbx002_verproductos()
 -- Desplegando productos
 DISPLAY ARRAY v_products TO s_products.*
  ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

  ON ACTION cancel
   EXIT DISPLAY

  ON ACTION calculator
   -- Cargando calculadora
   IF NOT winshellexec("calc") THEN
      CALL fgl_winmessage(
      "Atencion","Calculadora no disponible.","stop") 
   END IF

  BEFORE ROW
   -- Verificando control del total de lineas
   IF (ARR_CURR()>totlin) THEN
      CALL FGL_SET_ARR_CURR(1)
   END IF
 END DISPLAY
END FUNCTION 

-- Subrutina para insertar una transaccion de absorcion 

FUNCTION invqbx002_TransaccionAbsorcion(xtipope) 
 DEFINE w_mae_tra RECORD LIKE inv_mtransac.*,
        xtipmov   LIKE inv_mtransac.tipmov,
        xnomori   LIKE inv_mtransac.nomori,
        xnomdes   LIKE inv_mtransac.nomdes,
        xtipope   LIKE inv_mtransac.tipope,
        xobserv   LIKE inv_mtransac.observ,
        strsql    STRING 

 -- Verificando tipo de movimiento
 CASE (xtipope)
  WHEN 1 -- Cargo
   LET xtipmov = 2
   LET xcodori = 3 
   LET xcoddes = 0 
   LET xnomdes = NULL 
   LET xobserv = "NOTA DE CARGO POR ABSORCION INVENTARIO FISICO DEL "||w_mae_fis.fecinv 

   -- Obteniendo nombre del proveedor 
   SELECT a.nomprv INTO xnomori FROM inv_provedrs a WHERE a.codprv = xcodori  
  WHEN 0 -- Abono 
   LET xtipmov = 3 
   LET xcodori = 0
   LET xnomori = NULL  
   LET xcoddes = 6 
   LET xobserv = "NOTA DE ABONO POR ABSORCION INVENTARIO FISICO DEL "||w_mae_fis.fecinv 

   -- Obteniendo nombre del cliente
   SELECT a.nomcli INTO xnomdes FROM fac_clientes a WHERE a.codcli = xcoddes  
 END CASE 

 -- Asignando datos del encabezado 
 LET w_mae_tra.lnktra = 0  
 LET w_mae_tra.codemp = w_mae_fis.codemp 
 LET w_mae_tra.codsuc = w_mae_fis.codsuc 
 LET w_mae_tra.codbod = w_mae_fis.codbod 
 LET w_mae_tra.tipmov = xtipmov
 LET w_mae_tra.codori = xcodori 
 LET w_mae_tra.nomori = xnomori
 LET w_mae_tra.coddes = xcoddes
 LET w_mae_tra.nomdes = xnomdes 
 LET w_mae_tra.numrf1 = 1
 LET w_mae_tra.numrf2 = NULL
 LET w_mae_tra.numord = 1
 LET w_mae_tra.aniotr = w_mae_fis.aniotr 
 LET w_mae_tra.mestra = w_mae_fis.mestra
 LET w_mae_tra.fecemi = w_mae_fis.fecinv 
 LET w_mae_tra.tipope = xtipope 
 LET w_mae_tra.observ = xobserv 
 LET w_mae_tra.estado = "V"
 LET w_mae_tra.lnkcon = 0
 LET w_mae_tra.conaut = 0
 LET w_mae_tra.motanl = NULL
 LET w_mae_tra.fecanl = NULL
 LET w_mae_tra.horanl = NULL
 LET w_mae_tra.usranl = NULL
 LET w_mae_tra.impres = "S" 
 LET w_mae_tra.userid = username 
 LET w_mae_tra.fecsis = CURRENT
 LET w_mae_tra.horsis = "22:00:00" 

 -- Iniciando transaccion
 BEGIN WORK 

  -- 1. Creando encabezado
  SET LOCK MODE TO WAIT
  INSERT INTO inv_mtransac 
  VALUES (w_mae_tra.*) 
  LET w_mae_tra.lnktra = SQLCA.SQLERRD[2] 

  -- 2. Creando detalle
  CASE (xtipope) 
   WHEN 1 -- Cargo 
    LET strsql = "INSERT INTO inv_dtransac ",
                 "SELECT ",w_mae_tra.lnktra,",",
                           w_mae_tra.codemp,",", 
                           w_mae_tra.codsuc,",", 
                           w_mae_tra.codbod,",", 
                           w_mae_tra.tipmov,",", 
                           w_mae_tra.codori,",", 
                           w_mae_tra.coddes,",'", 
                           w_mae_tra.fecemi,"',", 
                           w_mae_tra.aniotr,",", 
                           w_mae_tra.mestra,",", 
                          "x.cditem,",
                          "x.codabr,",
                          "'',0,",
                          "(x.canuni-x.canexi),",
                          "(x.canuni-x.canexi),",
                          "0,0,0,0,'',",
                           w_mae_tra.tipope,",",
                          "'V',0,", 
                          "(x.canuni-x.canexi),",
                          "1,0,0,user,current,'22:00:00' ",
                 " FROM  inv_tofisico x ",
                 " WHERE x.codemp = ",w_mae_tra.codemp,
                 "   AND x.codsuc = ",w_mae_tra.codsuc,
                 "   AND x.codbod = ",w_mae_tra.codbod,
                 "   AND x.fecinv = '",w_mae_tra.fecemi,"'", 
                 "   AND (x.canuni-x.canexi)>0" 

   WHEN 0 -- Abono 
    LET strsql = "INSERT INTO inv_dtransac ",
                 "SELECT ",w_mae_tra.lnktra,",",
                           w_mae_tra.codemp,",", 
                           w_mae_tra.codsuc,",", 
                           w_mae_tra.codbod,",", 
                           w_mae_tra.tipmov,",", 
                           w_mae_tra.codori,",", 
                           w_mae_tra.coddes,",'", 
                           w_mae_tra.fecemi,"',", 
                           w_mae_tra.aniotr,",", 
                           w_mae_tra.mestra,",", 
                          "x.cditem,",
                          "x.codabr,",
                          "'',0,",
                          "(x.canexi-x.canuni),",
                          "(x.canexi-x.canuni),",
                          "0,0,0,0,'',",
                           w_mae_tra.tipope,",",
                          "'V',0,", 
                          "((x.canexi-x.canuni)*-1),",
                          "1,0,0,user,current,'22:00:00' ",
                 " FROM  inv_tofisico x ",
                 " WHERE x.codemp = ",w_mae_tra.codemp,
                 "   AND x.codsuc = ",w_mae_tra.codsuc,
                 "   AND x.codbod = ",w_mae_tra.codbod,
                 "   AND x.fecinv = '",w_mae_tra.fecemi,"'", 
                 "   AND (x.canuni-x.canexi)<0" 
  END CASE 
           
  -- Ejecutando procedimiento de absorcion 
  DISPLAY strsql 
  PREPARE p_absorcion FROM strsql
  EXECUTE p_absorcion 

 -- Finalizando transaccion
 COMMIT WORK
END FUNCTION 
