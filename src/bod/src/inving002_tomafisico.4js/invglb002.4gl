{ 
Fecha    : diciembre 2006 
Programo : Mynor Ramirez 
Objetivo : Programa de variables globales ingreso de inventario fisico 
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "inving002"
DEFINE w_mae_pro   RECORD LIKE inv_products.*,
       w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_suc   RECORD LIKE glb_sucsxemp.*,
       w_mae_bod   RECORD LIKE inv_mbodegas.*,
       w_mae_fis   RECORD 
        codemp     LIKE inv_tofisico.codemp, 
        codsuc     LIKE inv_tofisico.codsuc, 
        codbod     LIKE inv_tofisico.codbod, 
        aniotr     LIKE inv_tofisico.aniotr, 
        mestra     LIKE inv_tofisico.mestra, 
        fecinv     LIKE inv_tofisico.fecinv, 
        codcat     LIKE inv_products.codcat, 
        subcat     LIKE inv_products.subcat 
       END RECORD,
       v_products  DYNAMIC ARRAY OF RECORD
        codpro     LIKE inv_products.cditem, 
        cditem     LIKE inv_products.codabr, 
        dsitem     CHAR(50), 
        nommed     CHAR(30),
        cancon     LIKE inv_tofisico.canuni,
        canepq     LIKE inv_tofisico.canuni,
        totepq     LIKE inv_tofisico.canuni,
        canhhd     LIKE inv_tofisico.canuni, 
        cansue     LIKE inv_tofisico.canuni,
        canuni     LIKE inv_tofisico.canuni,
        canexi     LIKE inv_dtransac.canuni,
        candif     LIKE inv_dtransac.canuni,
        endrec     CHAR(1) 
       END RECORD,
       username    VARCHAR(15), 
       totlin      SMALLINT,
       b           ui.ComboBox,
       cba         ui.ComboBox,
       w           ui.Window,
       f           ui.Form
END GLOBALS
