{
Fecha    : Diciembre 2010 
Programo : Mynor Ramirez
Objetivo : Programa de ingreso del inventario fisico 
}

-- Definicion de variables globales
GLOBALS "invglb002.4gl"
DEFINE regreso      SMALLINT,
       diamax       SMALLINT,
       diamin       SMALLINT,
       existe       SMALLINT,
       absorciones  SMALLINT,
       msg          STRING 

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarFisico")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("tomafisico")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario
 LET username = FGL_GETENV("LOGNAME") 

 -- Menu de opciones
 CALL inving002_menu()
END MAIN

-- Subrutina para el menu de ingreso del inventario fisico

FUNCTION inving002_menu()
 DEFINE regreso    SMALLINT, 
        wpais      VARCHAR(255), 
        titulo     STRING 

 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing002a AT 5,2  
  WITH FORM "inving002a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ 

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header(progname,wpais,1)

  -- Inicializando datos 
  CALL inving002_inival(1)

  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()

  -- Menu de opciones
  MENU "Inventario Fisico" 
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Consultar"
    END IF
    -- Registrar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Registrar"
    END IF
   COMMAND "Consultar" 
    "Consulta de inventarios fisicos existentes."
    CALL invqbx002_fisicos()
   COMMAND "Registrar" 
    "Registro o modificacion de inventarios fisicos."
    CALL inving002_fisicos() 
   COMMAND "Salir"
    "Abandona el menu." 
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wing002a
END FUNCTION

-- Subrutina para el ingreso de los datos generales del ingreso del inventario

FUNCTION inving002_fisicos()
 DEFINE loop,existe,retroceso SMALLINT

 -- Obteniendo dias anteriores para ingreso de fecha del inventario
 CALL librut003_parametros(5,1)
 RETURNING existe,diamin
 IF NOT existe THEN
    CALL fgl_winmessage(
    "Atencion",
    "Parametro de maximo dias anteriores no existe registrado.\n"||
    "Definir parametro antes de ingresar el inventario.",
    "stop")
    RETURN 
 END IF

 -- Obteniendo dias posteriores para ingreso de fecha del inventario
 CALL librut003_parametros(5,2)
 RETURNING existe,diamax 
 IF NOT existe THEN
    CALL fgl_winmessage(
    "Atencion",
    "Parametro de maximo dias posteriores no existe registrado.\n"||
    "Definir parametro antes de ingresar el inventario.",
    "stop")
    RETURN 
 END IF

 -- Inicio del loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop   

  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos 
     CALL inving002_inival(1) 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_fis.codbod,
                w_mae_fis.codcat,
                w_mae_fis.subcat,
                w_mae_fis.fecinv WITHOUT DEFAULTS
    ATTRIBUTES(UNBUFFERED) 

   ON ACTION cancel 
    -- Salida 
    IF INFIELD(codbod) THEN
       LET loop = FALSE
       CALL inving002_inival(1)
       EXIT INPUT
    ELSE
       CALL inving002_inival(1)
       NEXT FIELD codbod
    END IF 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       CALL fgl_winmessage(
       "Atencion","Calculadora no disponible, VERIFICA.","stop") 
    END IF

   ON CHANGE codbod 
    -- Obteniendo datos de la bodega 
    CALL librut003_bbodega(w_mae_fis.codbod) 
    RETURNING w_mae_bod.*,existe 

    -- Asignando datos de empresa y sucursal de la bodega
    LET w_mae_fis.codemp = w_mae_bod.codemp 
    LET w_mae_fis.codsuc = w_mae_bod.codsuc 

    -- Obteniendo datos de la empresa 
    CALL librut003_bempresa(w_mae_bod.codemp)
    RETURNING w_mae_emp.*,existe 
    -- Obteniendo datos de la sucursal 
    CALL librut003_bsucursal(w_mae_bod.codsuc)
    RETURNING w_mae_suc.*,existe 
   
    -- Desplegando datos de la empresa y sucursal 
    DISPLAY BY NAME w_mae_emp.nomemp,w_mae_suc.nomsuc

    -- Limpiando combos de categorias y subcategorias
    LET w_mae_fis.codcat = NULL
    LET w_mae_fis.subcat = NULL
    CLEAR codcat,subcat

   ON CHANGE codcat
    -- Limpiando combos
    LET w_mae_fis.subcat = NULL
    CLEAR subcat

    -- Llenando combox de subcategorias
    IF w_mae_fis.codcat IS NOT NULL THEN
       CALL librut003_cbxsubcategorias(w_mae_fis.codcat)
    END IF

   AFTER FIELD codbod
    -- Verificando bodega
    IF w_mae_fis.codbod IS NULL THEN
       CALL fgl_winmessage(
       "Atencion","Bodega invalida, VERIFICA.","stop") 
       NEXT FIELD codbod 
    END IF 

   AFTER FIELD fecinv
    -- Verificando fecha de emision
    IF w_mae_fis.fecinv IS NULL OR 
       (w_mae_fis.fecinv >(TODAY+diamax)) OR 
       (w_mae_fis.fecinv <(TODAY-diamax)) THEN 
       CALL fgl_winmessage(
       "Atencion","Fecha del inventario invalida, VERIFICA.","stop") 
       NEXT FIELD fecinv
    END IF

    -- Asignando datos de fecha y anio 
    LET w_mae_fis.aniotr = YEAR(w_mae_fis.fecinv) 
    LET w_mae_fis.mestra = MONTH(w_mae_fis.fecinv) 
   AFTER INPUT
    -- Verificando datos
    IF w_mae_fis.codbod IS NULL THEN
       NEXT FIELD codbod
    END IF
    IF w_mae_fis.fecinv IS NULL THEN
       NEXT FIELD fecinv 
    END IF

    -- Asignando datos de fecha y anio 
    LET w_mae_fis.aniotr = YEAR(w_mae_fis.fecinv) 
    LET w_mae_fis.mestra = MONTH(w_mae_fis.fecinv) 
  END INPUT 
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Verificando si ya se hizo nota de absorcion para el fisico
  LET absorciones = inving002_ExisteAbsorcion()

  -- Cargando productos de la bodega para la toma del fisico 
  CALL inving002_CargaProductosBodega()
  IF (totlin>0) THEN
     IF absorciones>0 THEN
        CALL fgl_winmessage(
        "Atencion",
        "Inventario fisico ya con absorciones realizadas.\nSolo podra visualizarse.",
        "stop")

        DISPLAY ARRAY v_products TO s_products.*
         ON ACTION cancel
          EXIT DISPLAY 
        END DISPLAY

        LET retroceso = FALSE
        CONTINUE WHILE 
     END IF 

     -- Registrando productos 
     LET retroceso = inving002_RegistroProductos()
  ELSE
     CALL fgl_winmessage(
     "Atencion",
     "No existen productos registrados en la bodega seleccionada que inventariar.\n"||
     "Verifica que existan productos en la bodega seleccionada.",
     "stop")
  END IF 
 END WHILE

 -- Inicializando datos 
 CALL inving002_inival(1) 
END FUNCTION

-- Subutina para registrar el detalle de productos del inventario 

FUNCTION inving002_RegistroProductos() 
 DEFINE loop       SMALLINT,
        opc        SMALLINT, 
        retroceso  SMALLINT 

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop
  -- Ingresando productos
  INPUT ARRAY v_products WITHOUT DEFAULTS FROM s_products.*
       ATTRIBUTE(MAXCOUNT=totlin,INSERT ROW=FALSE,
                 APPEND ROW=FALSE,DELETE ROW=FALSE,
                 ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)

   ON ACTION ACCEPT
    -- Aceptar
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET loop = FALSE
    EXIT INPUT

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON CHANGE cancon 
    -- Calculando cantidad total en unidades totales
    CALL inving002_TotalCantidades(ARR_CURR(),SCR_LINE())

   AFTER FIELD cancon 
    -- Verificando cantidad contada
    IF v_products[ARR_CURR()].cancon IS NULL OR
       (v_products[ARR_CURR()].cancon <0) THEN 
       CALL fgl_winmessage(
       "Atencion","Cantidad invalida, VERIFICA.","stop") 
       NEXT FIELD cancon 
    END IF 

    -- Calculando cantidad total en unidades totales
    CALL inving002_TotalCantidades(ARR_CURR(),SCR_LINE())

   ON CHANGE cansue 
    -- Calculando cantidad total en unidades totales
    CALL inving002_TotalCantidades(ARR_CURR(),SCR_LINE())

   AFTER FIELD cansue 
    -- Verificando cantidad suelta 
    IF v_products[ARR_CURR()].cansue IS NULL OR
       (v_products[ARR_CURR()].cansue <0) THEN 
       CALL fgl_winmessage(
       "Atencion","Cantidad invalida, VERIFICA.","stop") 
       NEXT FIELD cansue 
    END IF 

    -- Calculando cantidad total en unidades totales
    CALL inving002_TotalCantidades(ARR_CURR(),SCR_LINE())

  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop      = FALSE
    LET retroceso = FALSE
   WHEN 1 -- Grabando
    LET loop      = FALSE
    LET retroceso = FALSE

    -- Grabando inventario 
    CALL inving002_grabar()

   WHEN 2 -- Modificando
    LET loop = TRUE 
  END CASE
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subrutina para cargar el producto de la bodega seleccionada 

FUNCTION inving002_CargaProductosBodega() 
 DEFINE wsalval    LIKE inv_dtransac.totpro, 
        strcodcat  STRING,
        strsubcat  STRING,
        qrytext    STRING

 -- Verificando seleccion de categoria
 LET strcodcat = NULL
 IF w_mae_fis.codcat IS NOT NULL THEN
    LET strcodcat = "AND d.codcat = ",w_mae_fis.codcat
 END IF

 -- Verificando condicion de subcategoria
 LET strsubcat = NULL
 IF w_mae_fis.subcat IS NOT NULL THEN
    LET strsubcat = "AND e.subcat = ",w_mae_fis.subcat
 END IF

 -- Preparando seleccion de productos
 LET qrytext =
   "SELECT x.cditem,y.codabr,y.dsitem,y.canepq,z.nommed,x.exican ",
    "FROM  inv_proenbod x,inv_products y,glb_categors d,glb_subcateg e,inv_unimedid z ",
    "WHERE x.codemp = ",w_mae_fis.codemp, 
     " AND x.codsuc = ",w_mae_fis.codsuc, 
     " AND x.codbod = ",w_mae_fis.codbod, 
     " AND x.cditem = y.cditem ",
     " AND y.estado = 1",
     " AND d.codcat = y.codcat ",
     " AND e.codcat = y.codcat ",
     " AND e.subcat = y.subcat ",
     " AND z.unimed = y.unimed ",
     strcodcat CLIPPED," ",
     strsubcat CLIPPED," ",
     "ORDER BY 3"  

 -- Seleccionando productos
 PREPARE cpinv FROM qrytext 
 DECLARE cprod CURSOR FOR cpinv 
 LET totlin = 1
 FOREACH cprod INTO v_products[totlin].codpro,
                    v_products[totlin].cditem,
                    v_products[totlin].dsitem, 
                    v_products[totlin].canepq, 
                    v_products[totlin].nommed, 
                    v_products[totlin].canexi 

  -- Cargando inventario si ya existe 
  SELECT NVL(a.cancon,0),
         NVL(a.canhhd,0), 
         NVL(a.cansue,0)
   INTO  v_products[totlin].cancon,
         v_products[totlin].canhhd,
         v_products[totlin].cansue 
   FROM  inv_tofisico a
   WHERE a.codemp = w_mae_fis.codemp
     AND a.codsuc = w_mae_fis.codsuc
     AND a.codbod = w_mae_fis.codbod
     AND a.aniotr = w_mae_fis.aniotr 
     AND a.mestra = w_mae_fis.mestra
     AND a.fecinv = w_mae_fis.fecinv
     AND a.cditem = v_products[totlin].codpro 

  -- Calculando saldo anterior del producto
  CALL librut003_saldoantxpro(w_mae_fis.codemp,
                              w_mae_fis.codsuc,
                              w_mae_fis.codbod,
                              v_products[totlin].codpro,
                              (w_mae_fis.fecinv+1))
  RETURNING v_products[totlin].canexi,wsalval 

  IF v_products[totlin].cancon IS NULL THEN LET v_products[totlin].cancon = 0 END IF 
  IF v_products[totlin].canhhd IS NULL THEN LET v_products[totlin].canhhd = 0 END IF 
  IF v_products[totlin].cansue IS NULL THEN LET v_products[totlin].cansue = 0 END IF 
  IF v_products[totlin].canexi IS NULL THEN LET v_products[totlin].canexi = 0 END IF 

  -- Caculando totales
  CALL inving002_TotalCantidades(totlin,0) 

  -- Incrementando contador 
  LET totlin = (totlin+1)
 END FOREACH
 CLOSE cprod
 FREE  cprod
 LET totlin = (totlin-1)

 -- Desplegando totales
 LET msg = "Total Productos (",totlin USING "<<<,<<<",")"
 CALL f.setElementText("labelt",msg) 
END FUNCTION 

-- Subrutina para calcular las undiades totales (unidades ingresdas * cantidad empaque)

FUNCTION inving002_TotalCantidades(arr,scr) 
 DEFINE wequival LIKE inv_epqsxpro.cantid,  
        arr,scr  SMALLINT

 -- Calculando total unidades x empaque 
 IF (v_products[arr].canepq=0) THEN
    LET v_products[arr].totepq = v_products[arr].cancon
 ELSE 
    LET v_products[arr].totepq = (v_products[arr].cancon*v_products[arr].canepq)
 END IF 

 -- Calculando unidades totales 
 -- (cantidad contada segun empaque + cantidad hand held + camtidad suelta) 
 LET v_products[arr].canuni = (v_products[arr].totepq+
                               v_products[arr].canhhd+ 
                               v_products[arr].cansue) 

 -- Caculando diferencia
 LET v_products[arr].candif = (v_products[arr].canuni-v_products[arr].canexi)

 -- Desplegando datos
 IF (scr>0) THEN 
    DISPLAY v_products[arr].candif TO s_products[scr].candif 
 END IF 
END FUNCTION 

-- Subrutina para verificar si ya existe la absorcion de un inventario fisico

FUNCTION inving002_ExisteAbsorcion()
 DEFINE absorcion INTEGER 

 -- Verificando si ya se hizo nota de absorcion para el fisico
 SELECT COUNT(*)
  INTO  absorcion  
  FROM  inv_mtransac a
  WHERE a.codemp = w_mae_fis.codemp
    AND a.codsuc = w_mae_fis.codsuc
    AND a.codbod = w_mae_fis.codbod
    AND a.tipmov IN (2,3)
    AND a.aniotr = w_mae_fis.aniotr
    AND a.mestra = w_mae_fis.mestra
    AND a.fecemi = w_mae_fis.fecinv

 RETURN absorcion 
END FUNCTION 

-- Subrutina para verificar la ultima fecha de absorcion 

FUNCTION inving002_UltimaFechaAbsorcion()
 DEFINE maxfecabs DATE

 -- Buscando ultima fecha de absorcion
 SELECT MAX(a.fecemi) 
  INTO  maxfecabs 
  FROM  inv_mtransac a
  WHERE a.codemp = w_mae_fis.codemp
    AND a.codsuc = w_mae_fis.codsuc
    AND a.codbod = w_mae_fis.codbod
    AND a.tipmov IN (2,3)
    AND a.aniotr = w_mae_fis.aniotr
    AND a.mestra = w_mae_fis.mestra
    AND a.fecemi = w_mae_fis.fecinv

 RETURN maxfecabs
END FUNCTION 

-- Subrutina para grabar el movimiento 

FUNCTION inving002_grabar()
 DEFINE i,finmes SMALLINT

 -- Grabando transaccion
 ERROR " Registrando Inventario Fisico ..." ATTRIBUTE(CYAN)

 -- Verificando si fecha es de ultimo dia del mes
 IF (librut001_diasmes(w_mae_fis.mestra,w_mae_fis.aniotr)=DAY(w_mae_fis.fecinv)) THEN 
    LET finmes = 1
 ELSE
    LET finmes = 0
 END IF 

 -- Iniciando la transaccion
 BEGIN WORK

   -- 1. Grabando inventario fisico
   FOR i = 1 TO totlin 
    IF v_products[i].cditem IS NULL OR 
       v_products[i].canuni IS NULL THEN 
       CONTINUE FOR 
    END IF 

    -- Eliminando registro antes de insertar
    SET LOCK MODE TO WAIT
    DELETE FROM inv_tofisico 
    WHERE inv_tofisico.codemp = w_mae_fis.codemp
      AND inv_tofisico.codsuc = w_mae_fis.codsuc
      AND inv_tofisico.codbod = w_mae_fis.codbod
      AND inv_tofisico.aniotr = w_mae_fis.aniotr 
      AND inv_tofisico.mestra = w_mae_fis.mestra
      AND inv_tofisico.fecinv = w_mae_fis.fecinv
      AND inv_tofisico.cditem = v_products[i].codpro 

    -- Grabando
    SET LOCK MODE TO WAIT
    INSERT INTO inv_tofisico 
    VALUES (w_mae_fis.codemp       , -- empresa 
            w_mae_fis.codsuc       , -- sucursal 
            w_mae_fis.codbod       , -- bodega   
            w_mae_fis.aniotr       , -- anio del inventario
            w_mae_fis.mestra       , -- mes del inventario
            w_mae_fis.fecinv       , -- fecha del inventario 
            v_products[i].codpro   , -- codigo del producto 
            v_products[i].cditem   , -- codigo del producto abreviado
            v_products[i].cancon   , -- cantidad fisica contada x empaque 
            v_products[i].canepq   , -- cantidad en empaque 
            v_products[i].totepq   , -- total cantidad empaque (cancon*canepq)
            v_products[i].canhhd   , -- cantidad fisica tomada con hand held
            v_products[i].cansue   , -- cantidad fisica contada suelta
            v_products[i].canuni   , -- total cantidad fisica 
            0                      , -- total en valores del inventario 
            v_products[i].canexi   , -- existencia teorica del producto al ingreso 
            finmes                 , -- Inventario es fin de mes
            USER                   , -- Usuario que registro el inventario 
            CURRENT                , -- Fecha de registro del inventario 
            CURRENT HOUR TO SECOND)  -- Hora de registro del inventario 
   END FOR 

 -- Finalizando la transaccion
 COMMIT WORK

 -- Desplegando mensaje
 CALL fgl_winmessage("Atencion"," Inventario fisico registrado.","information")

 ERROR "" 
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION inving002_inival(i)
 DEFINE i SMALLINT
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_fis.* TO NULL 
   CLEAR FORM 
 END CASE 

 -- Inicializando datos
 LET totlin = 0 

 -- Lllenando combo de bodegas
 CALL librut003_cbxbodegasxusuario(username) 

 -- Inicializando vectores de datos
 CALL inving002_inivec() 

 -- Desplegando total de unidades del inventario 
 DISPLAY BY NAME diamin,diamax ATTRIBUTE(REVERSE,YELLOW) 
 CALL f.setElementText("labelt","Total Productos (0)")
END FUNCTION

-- Subrutina para inicializar y limpiar vector de trabajo 

FUNCTION inving002_inivec()
 DEFINE i SMALLINT

 -- Inicializando vectores
 CALL v_products.clear()
 LET totlin = 0 

 -- Limpiando detalle 
 DISPLAY ARRAY v_products TO s_products.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION 
