{ 
glbglo015.4gl
Mynor Ramirez
Mantenimiento de categorias 
Octubre 2011 
-- invmae0012a.per 
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae015"
DEFINE w_mae_pro   RECORD LIKE glb_categors.*,
       v_categpro  DYNAMIC ARRAY OF RECORD
        tcodcat    LIKE glb_categors.codcat,
        tnomcat    LIKE glb_categors.nomcat,
        ttellen    CHAR(1)
       END RECORD,
       username    VARCHAR(15)
END GLOBALS
