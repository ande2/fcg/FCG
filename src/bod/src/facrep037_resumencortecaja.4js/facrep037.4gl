{ 
Programo : facrep037.4gl 
Objetivo : Reporte de resumen de corte de caja  
}

DATABASE storepos 

-- Definicion de variables globales 

DEFINE w_datos       RECORD
        numpos       LIKE fac_mtransac.numpos,
        fecini       DATE,
        fecfin       DATE
       END RECORD,
       xnompos       LIKE fac_puntovta.nompos, 
       lg,nlines     SMALLINT, 
       existe        SMALLINT,
       tituloreporte STRING,
       filename      STRING,
       pipeline      STRING,
       periodo       STRING 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes2")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rresumencortecaja")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL facrep037_resumencortecaja()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION facrep037_resumencortecaja()
 DEFINE wpais      VARCHAR(255),
        loop,res   SMALLINT,
        w          ui.Window

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep001a AT 5,2
  WITH FORM "facrep037a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("facrep037",wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/ResumenCorteCaja.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox
  CALL librut003_CbxPuntosVenta()

  -- Inicializando datos
  INITIALIZE w_datos.* TO NULL
  LET w_datos.fecini = TODAY 
  LET w_datos.fecfin = TODAY 
  LET w_datos.numpos = 1 
  CLEAR FORM

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline TO NULL

   -- Construyendo busqueda
   INPUT BY NAME w_datos.numpos,
                 w_datos.fecini, 
                 w_datos.fecfin 
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Verificando si filtros estan completos
     IF NOT facrep037_FiltrosCompletos() THEN
        NEXT FIELD numpos 
     END IF
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 

     -- Verificando si filtros estan completos
     IF NOT facrep037_FiltrosCompletos() THEN
        NEXT FIELD numpos 
     END IF
     EXIT INPUT 

    AFTER FIELD numpos
     -- Verificando punto de venta 
     IF w_datos.numpos IS NULL THEN
        CALL fgl_winmessage("Atencion","Punto de venta invalido, VERIFICA.","stop") 
        NEXT FIELD numpos
     END IF

    AFTER INPUT 
     -- Verificando datos
     IF w_datos.numpos IS NULL OR 
        w_datos.fecini IS NULL OR 
        w_datos.fecfin IS NULL OR 
        pipeline       IS NULL THEN
        NEXT FIELD numpos
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Generando el reporte
   CALL facrep037_gencorte()
  END WHILE
 CLOSE WINDOW wrep001a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION facrep037_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE
 IF w_datos.numpos IS NULL OR
    w_datos.fecini IS NULL OR
    w_datos.fecfin IS NULL THEN
    CALL fgl_winmessage(
    "Atencion",
    "Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte de resumen corte de caja   

FUNCTION facrep037_gencorte()
 DEFINE w_mae_fac RECORD LIKE vis_resumencortecaja.*,
        xnomusr   LIKE glb_usuarios.nomusr, 
        s_query   STRING, 
        strfeccor STRING

 -- Definiendo parametros
 LET lg     = 156 
 LET nlines = 50 

 -- Seleccionando nombre del punto de venta 
 SELECT NVL(a.nompos,"")
  INTO  xnompos  
  FROM  fac_puntovta a
  WHERE a.numpos = w_datos.numpos 

 -- Verificando condicion de fecha de corte
 LET strfeccor = " AND x.feccor >= '",w_datos.fecini,"'",
                 " AND x.feccor <= '",w_datos.fecfin,"' "
 
 -- Preparando condiciones del reporte 
 LET s_query = 
  "SELECT x.* ",
   "FROM  vis_resumencortecaja x ",
   "WHERE x.numpos = ",w_datos.numpos, 
   strfeccor CLIPPED,
   " ORDER BY x.feccor" 

 -- Seleccionando datos del reporte
 ERROR "Atencion: seleccionando datos ... por favor espere ..."

 PREPARE s_corte FROM s_query
 DECLARE c_corte CURSOR FOR s_corte
 LET existe    = FALSE
 FOREACH c_corte INTO w_mae_fac.*
  -- Iniciando reporte
  IF NOT existe THEN
     -- Iniciando la impresion
     START REPORT facrep037_impcorvta TO filename
     LET existe = TRUE
  END IF

  -- Verificando sobrante o faltantes
  LET w_mae_fac.totdif = w_mae_fac.totdif*(-1)

  -- Llenando el reporte
  OUTPUT TO REPORT facrep037_impcorvta(w_mae_fac.*) 
 END FOREACH
 CLOSE c_corte
 FREE  c_corte

 IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT facrep037_impcorvta   

    -- Enviando reporte al destino seleccionado
    CALL librut001_sendreport
    (filename,pipeline,tituloreporte, 
     "--noline-numbers "||
     "--nofooter "||
     "--font-size 8 "||
     "--page-width 842 --page-height 595 "||
     "--left-margin 55 --right-margin 25 "||
     "--top-margin 35 --bottom-margin 45 "||
     "--title Facturacion")

    ERROR "" 
    CALL fgl_winmessage("Atencion","Reporte Emitido.","information") 
 ELSE
    ERROR "" 
    CALL fgl_winmessage("Atencion","No existen datos con el filtro seleccionado.","stop") 
 END IF 
END FUNCTION 

-- Subrutina para imprimir el reporte 

REPORT facrep037_impcorvta(imp1)
 DEFINE imp1          RECORD LIKE vis_resumencortecaja.*,
        exis,i,col    SMALLINT, 
        linea         CHAR(156), 
        wdsitem       CHAR(40),
        wnomrec       CHAR(40),
        xnomcli       CHAR(44), 
        nestado       CHAR(3)

  OUTPUT LEFT   MARGIN 0 
         TOP    MARGIN 0 
         BOTTOM MARGIN 0 
         PAGE   LENGTH nlines 

  FORMAT 
   PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________",
                "______"

    -- Imprimiendo encabezado 
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Facturacion",
          COLUMN col,tituloreporte CLIPPED,            
	  COLUMN (lg-20),PAGENO USING "Pagina: <<"

    LET periodo = "PERIODO DEL ",w_datos.fecini," AL ",w_datos.fecfin 
    LET col = librut001_centrado(periodo,lg) 
    PRINT COLUMN   1,"Facrep001",
          COLUMN col,periodo CLIPPED,
          COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET col = librut001_centrado(xnompos,lg) 
    PRINT COLUMN col,xnompos CLIPPED, 
          COLUMN (lg-20),"Hora  : ",TIME 

    PRINT linea 
    PRINT "Fecha           Fondo           Total      Total            Total       ",
          "Total           Total    Total          Total             Total  Fecha      Hora"
    PRINT "Corte           Inicial         Ventas     Egresos          Corte       ",
          "Efectivo        Tarjeta  EfectivoCorte  EfectivoCierre    Falta- Cierre     Cierre"
    PRINT "                                                                        ",
          "                         Sistema        Cajero            Sobra+"
    PRINT linea

   ON EVERY ROW
    -- Imprimiendo datos 
    PRINT imp1.feccor                         USING "dd/mm/yyyy"   ,3 SPACES,
          imp1.fonini                         USING "###,##&.&&"   ,2 SPACES,
          imp1.totvta                         USING "##,###,##&.&&",2 SPACES,
          imp1.totgas                         USING "###,##&.&&"   ,2 SPACES,
          imp1.totcor                         USING "##,###,##&.&&",2 SPACES,
          imp1.totefe                         USING "##,###,##&.&&",2 SPACES,
          imp1.tottar                         USING "##,###,##&.&&",2 SPACES, 
          imp1.toecor                         USING "##,###,##&.&&",2 SPACES, 
          imp1.toecie                         USING "##,###,##&.&&",2 SPACES, 
          imp1.totdif                         USING "--,--&.&&"    ,1 SPACES,
          imp1.feccie                                              ,1 SPACES,
          imp1.horcie                                              ,1 SPACES

--  lnkcor, numpos, feccor, tipcor, observ, totvta, fonini, totgas, totcor, totefe,
--  tottar, totcon, totcre, totcos, totdon, totisv, totdes, toecie, toecor, totdif,
--  ttacie, todita, userid, fecsis, horsis, cierre, usrcie, feccie, horcie

   ON LAST ROW
    -- Totalizando
    SKIP 1 LINES 
    PRINT COLUMN   1,"TOTAL -->",
          COLUMN  14,SUM(imp1.fonini)         USING "###,##&.&&"   ,2 SPACES,
                     SUM(imp1.totvta)         USING "##,###,##&.&&",2 SPACES,
                     SUM(imp1.totgas)         USING "###,##&.&&"   ,2 SPACES,
                     SUM(imp1.totcor)         USING "##,###,##&.&&",2 SPACES,
                     SUM(imp1.totefe)         USING "##,###,##&.&&",2 SPACES,
                     SUM(imp1.tottar)         USING "##,###,##&.&&",2 SPACES, 
                     SUM(imp1.toecor)         USING "##,###,##&.&&",2 SPACES, 
                     SUM(imp1.toecie)         USING "##,###,##&.&&",2 SPACES, 
                     SUM(imp1.totdif)         USING "--,--&.&&"    ,1 SPACES 
END REPORT
