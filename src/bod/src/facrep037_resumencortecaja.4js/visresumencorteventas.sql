  drop view vis_resumencortecaja;

create view vis_resumencortecaja
(
  lnkcor, numpos, feccor, tipcor, observ, totvta, fonini, totgas, totcor, totefe,
  tottar, totcon, totcre, totcos, totdon, totisv, totdes, toecie, toecor, totdif,
  ttacie, todita, userid, fecsis, horsis, cierre, usrcie, feccie, horcie
 )
as

select a.lnkcor,a.numpos,a.feccor,a.tipcor,a.observ,a.totvta,a.fonini,a.totgas,a.totcor,
       a.totefe,a.tottar,a.totcon,a.totcre,a.totcos,a.totdon,a.totisv,a.totdes,a.toecie,
       a.toecor,a.totdif,a.ttacie,a.todita,a.userid,a.fecsis,a.horsis,a.cierre,a.usrcie,
       a.feccie,a.horcie
from   fac_vicortes a
where  a.cierre = 1;

grant select on vis_resumencortecaja to public;
