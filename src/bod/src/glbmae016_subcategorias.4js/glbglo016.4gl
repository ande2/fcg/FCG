{ 
glbglo016.4gl
Mynor Ramirez
Mantenimiento de subcategorias
Octubre 2011 
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT  progname = "glbmae016"
DEFINE w_mae_pro   RECORD LIKE glb_subcateg.*,
       v_subcats   DYNAMIC ARRAY OF RECORD
        tcodcat    LIKE glb_subcateg.codcat,
        tnomcat    LIKE glb_categors.nomcat, 
        tsubcat    LIKE glb_subcateg.subcat,
        tnomsub    LIKE glb_subcateg.nomsub 
       END RECORD,
       username    VARCHAR(15)
END GLOBALS
