{
Mantenimiento de subcategorias 
glbmae016.4gl 
MRS 
Octubre 2011 
}

-- Definicion de variables globales 

GLOBALS "glbglo016.4gl"

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("subcategorias")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Obteniendo nombre del usuario 
 LET username = FGL_GETENV("LOGNAME")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL glbmae016_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION glbmae016_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        existe   SMALLINT,
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "glbmae016a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("glbmae016",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Subcategorias"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(progname,4,username) THEN 
       HIDE OPTION "Buscar"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de subcategorias."
    CALL glbqbe016_subcategorias(1) 
   COMMAND "Nuevo"
    " Ingreso de una nueva subcategoria."
    LET savedata = glbmae016_subcategorias(1) 
   COMMAND "Modificar"
    " Modificacion de una subcategoria existente."
    CALL glbqbe016_subcategorias(2) 
   COMMAND "Borrar"
    " Eliminacion de una subcategoria existente."
    CALL glbqbe016_subcategorias(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION glbmae016_subcategorias(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL glbqbe016_EstadoMenu(4,"")
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL glbmae016_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.codcat,
                w_mae_pro.nomsub
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE FIELD codcat
    -- Cargando combobox de categorias
    CALL librut003_cbxcategorias() 

   AFTER FIELD codcat 
    --Verificando catagoria
    IF w_mae_pro.codcat IS NULL THEN
       NEXT FIELD codcat
    END IF

   AFTER FIELD nomsub  
    --Verificando nombre de lia subcategoria
    IF (LENGTH(w_mae_pro.nomsub)=0) THEN
       ERROR "Error: nombre de la subcategoria invalido, VERIFICA."
       LET w_mae_pro.nomsub = NULL
       NEXT FIELD nomsub  
    END IF

    -- Verificando que no exista otra subcategoria con el mismo nombre
    SELECT UNIQUE (a.subcat)
     FROM  glb_subcateg a
     WHERE (a.codcat  = w_mae_pro.codcat)
       AND (a.subcat != w_mae_pro.subcat) 
       AND (a.nomsub  = w_mae_pro.nomsub) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion",
        " Existe otra subcategoria con el mismo nombre, VERIFICA.",
        "information")
        NEXT FIELD nomsub
     END IF 

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.codcat IS NULL THEN
       NEXT FIELD codcat 
    END IF
    IF w_mae_pro.nomsub IS NULL THEN 
       NEXT FIELD nomsub
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL glbmae016_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL glbmae016_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso 
 IF (operacion=1) THEN
    CALL glbqbe016_EstadoMenu(0,"")
    CALL glbmae016_inival(1) 
 END IF 

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar una subcategoria

FUNCTION glbmae016_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando subcategoria ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.subcat),0)
    INTO  w_mae_pro.subcat 
    FROM  glb_subcateg a
    IF (w_mae_pro.subcat IS NULL) THEN
       LET w_mae_pro.subcat = 1
    ELSE
       LET w_mae_pro.subcat = w_mae_pro.subcat+1
    END IF

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO glb_subcateg   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.subcat 

   --Asignando el mensaje 
   LET msg = "Subcategoria (",w_mae_pro.subcat USING "<<<<<<",") registrada."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE glb_subcateg
   SET    glb_subcateg.*      = w_mae_pro.*
   WHERE  glb_subcateg.subcat = w_mae_pro.subcat 

   --Asignando el mensaje 
   LET msg = "Subcategoria (",w_mae_pro.subcat USING "<<<<<<",") actualizada."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando subcategorias
   DELETE FROM glb_subcateg 
   WHERE  glb_subcateg.subcat = w_mae_pro.subcat 

   --Asignando el mensaje 
   LET msg = "Subcategoria (",w_mae_pro.subcat USING "<<<<<<",") borrada."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL glbmae016_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION glbmae016_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.subcat = 0 
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codcat
 DISPLAY BY NAME w_mae_pro.subcat,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION
