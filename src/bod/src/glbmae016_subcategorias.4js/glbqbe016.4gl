{
glbqbe016.4gl 
Mynor Ramirez
Mantenimiento de subcategorias 
}

{ Definicion de variables globales }

GLOBALS "glbglo016.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe016_subcategorias(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        titmenu             STRING,
        qry                 STRING,
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Llenando combonox de categorias
  CALL librut003_cbxcategorias() 

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL glbqbe016_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae016_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codcat,a.subcat,a.nomsub,
                                a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel  
     -- Salida
     CALL glbmae016_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codcat,b.nomcat,a.subcat,a.nomsub ",
                 " FROM glb_subcateg a,glb_categors b ",
                 " WHERE b.codcat = a.codcat AND ",qrytext CLIPPED,
                 " ORDER BY 2,3,4 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_subcategorias SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_subcats.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_subcategorias INTO v_subcats[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_subcategorias
   FREE  c_subcategorias
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL glbqbe016_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_subcats TO s_subcategorias.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel
      -- Salida
      CALL glbmae016_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- buscar
      CALL glbmae016_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      LET res = glbmae016_subcategorias(2) 
      -- Desplegando datos
      CALL glbqbe016_datos(v_subcats[ARR_CURR()].tcodcat,v_subcats[ARR_CURR()].tsubcat)

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN
       -- Modificando 
       LET res = glbmae016_subcategorias(2)
       -- Desplegando datos
       CALL glbqbe016_datos(v_subcats[ARR_CURR()].tcodcat,v_subcats[ARR_CURR()].tsubcat)
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF glbqbe016_integridad() THEN
       LET msg = " Esta subcategoria ya tiene registros. \n Subcategoria no puede borrarse."
       CALL fgl_winmessage(
       " Atencion",msg,"stop")
       CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar esta subcategoria ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                  msg,
                                  "Si",
                                  "No",
                                  NULL,
                                  NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL glbmae016_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Categoria"
      LET arrcols[2] = "Subcategoria"
      LET arrcols[3] = "Nombre Subcategoria" 
      LET arrcols[4] = "Usuario Registro"
      LET arrcols[5] = "Fecha Registro"
      LET arrcols[6] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT b.nomcat,a.subcat,a.nomsub,",
                              "a.userid,a.fecsis,a.horsis ",
                       " FROM glb_subcateg a,glb_categors b ",
                       " WHERE b.codcat = a.codcat AND ",qrytext CLIPPED,
                       " ORDER BY 1,2 "

      -- Ejecutando el reporte
      LET res        = librut002_excelreport("Subcategorias",qry,6,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Desplegando datos 
      IF (ARR_CURR()<=totlin) THEN 
         CALL glbqbe016_datos(v_subcats[ARR_CURR()].tcodcat,
                              v_subcats[ARR_CURR()].tsubcat)
      END IF 
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen subcategorias con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Asignando estado del menu 
  CALL glbqbe016_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION glbqbe016_datos(wcodcat,wsubcat)
 DEFINE wcodcat LIKE glb_subcateg.codcat,
        wsubcat LIKE glb_subcateg.subcat,
        existe  SMALLINT,
        qryres  STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  glb_subcateg a "||
              "WHERE a.subcat = "||wsubcat||
              " ORDER BY 1 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_subcategoriast SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_subcategoriast INTO w_mae_pro.*
 END FOREACH 
 CLOSE c_subcategoriast
 FREE  c_subcategoriast

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomsub
 DISPLAY BY NAME w_mae_pro.codcat,w_mae_pro.subcat,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para verificar si la subcategoria ya tiene registros

FUNCTION glbqbe016_integridad()
 DEFINE conteo INTEGER  

 -- Verificando productos 
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_products a 
  WHERE (a.subcat = w_mae_pro.subcat)
  IF (conteo>0) THEN
     RETURN TRUE 
  ELSE
     RETURN FALSE 
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION glbqbe016_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Subcategorias - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Subcategorias - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Subcategorias - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Subcategorias - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Subcategorias - NUEVO")
 END CASE
END FUNCTION
