{ 
Mynor Ramirez
Mantenimiento de grupos de tipos de movimiento de inventario 
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae040"
DEFINE w_mae_pro   RECORD LIKE inv_grupomov.*,
       v_grupos    DYNAMIC ARRAY OF RECORD
        tcodgru    LIKE inv_grupomov.codgru,
        tnomgru    LIKE inv_grupomov.nomgru,
        tendrec    CHAR(1)
       END RECORD,
       username    VARCHAR(15)
END GLOBALS
