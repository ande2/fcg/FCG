{
glbqbe040.4gl 
Mynor Ramirez
Mantenimiento de grupos de tipos de movimiento de inventario 
}

{ Definicion de variables globales }

GLOBALS "glbglo040.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe040_grupos(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),    
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,   
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL glbqbe040_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae040_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codgru,a.nomgru,a.tipope,a.numord,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel
     -- Salida
     CALL glbmae040_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codgru,a.nomgru,'' ",
                 " FROM inv_grupomov a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_grupos SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_grupos.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_grupos INTO v_grupos[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_grupos
   FREE  c_grupos
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL glbqbe040_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_grupos TO s_grupos.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL glbmae040_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL glbmae040_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      LET res = glbmae040_grupos(2) 
      -- Desplegando datos
      CALL glbqbe040_datos(v_grupos[ARR_CURR()].tcodgru)

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN 
         -- Modificando 
         LET res = glbmae040_grupos(2) 
         -- Desplegando datos
         CALL glbqbe040_datos(v_grupos[ARR_CURR()].tcodgru)
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF glbqbe040_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este grupo ya tiene registros.\n Grupo no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar este grupo ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL glbmae040_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte 
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Grupo" 
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Tipo Operacion"
      LET arrcols[4] = "Orden Reportes" 
      LET arrcols[5] = "Usuario Registro"
      LET arrcols[6] = "Fecha Registro"
      LET arrcols[7] = "Hora Registro" 

      -- Se aplica el mismo query de la seleccion para enviar al reporte 
      LET qry        = 
         "SELECT a.codgru,a.nomgru,",
                "CASE (a.tipope) WHEN 1 THEN 'INGRESO' WHEN 0 THEN 'SALIDA' END CASE,",
                "a.numord,a.userid,a.fecsis,a.horsis",
          " FROM inv_grupomov a ",
          " WHERE ",qrytext CLIPPED,
          " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res    = librut002_excelreport("Grupos",qry,7,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL glbqbe040_datos(v_grupos[ARR_CURR()].tcodgru)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen grupos con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Asignando estado del menu 
  CALL glbqbe040_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION glbqbe040_datos(wcodgru)
 DEFINE wcodgru LIKE inv_grupomov.codgru,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  inv_grupomov a "||
              "WHERE a.codgru = "||wcodgru||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_grupost SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_grupost INTO w_mae_pro.*
 END FOREACH
 CLOSE c_grupost
 FREE  c_grupost

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomgru THRU w_mae_pro.numord 
 DISPLAY BY NAME w_mae_pro.codgru,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para verificar si la grupo tiene registros

FUNCTION glbqbe040_integridad()
 DEFINE conteo INTEGER  

 -- Verificando tipos de movimiento 
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_tipomovs a 
  WHERE (a.codgru = w_mae_pro.codgru) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION glbqbe040_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Grupos - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Grupos - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Grupos - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Grupos - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Grupos - NUEVO")
 END CASE
END FUNCTION
