{ 
Programo : facrep032.4gl 
Objetivo : Reporte de ventas x horas    
}

DATABASE storepos 

-- Definicion de variables globales 

TYPE   datosreporte RECORD
        horini      DATETIME HOUR TO MINUTE,
        horfin      DATETIME HOUR TO MINUTE,
        totdoc      INTEGER,                     
        totvta      DEC(14,2) 
       END RECORD 

DEFINE w_datos       RECORD
        numpos       LIKE pos_mtransac.numpos,
        fecini       DATE,
        fecfin       DATE
       END RECORD,
       xnompos       LIKE fac_puntovta.nompos, 
       lg,nlines     SMALLINT, 
       existe        SMALLINT,
       xcajero       STRING, 
       tituloreporte STRING,
       filename      STRING,
       pipeline      STRING,
       periodo       STRING 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes2")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rventasxhoras")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL facrep032_ventasxhoras() 
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION facrep032_ventasxhoras() 
 DEFINE wpais      VARCHAR(255),
        loop,res   SMALLINT,
        w          ui.Window

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep001a AT 5,2
  WITH FORM "facrep032a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("facrep032",wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/VentasxHoras.spl" 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox
  CALL librut003_CbxPuntosVenta()

  -- Inicializando datos
  INITIALIZE w_datos.* TO NULL
  LET w_datos.fecini = TODAY 
  LET w_datos.fecfin = TODAY 
  LET w_datos.numpos = 1 
  CLEAR FORM

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline TO NULL

   -- Construyendo busqueda
   INPUT BY NAME w_datos.numpos,
                 w_datos.fecini, 
                 w_datos.fecfin  
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Verificando si filtros estan completos
     IF NOT facrep003_FiltrosCompletos() THEN
        NEXT FIELD numpos 
     END IF
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 

     -- Verificando si filtros estan completos
     IF NOT facrep003_FiltrosCompletos() THEN
        NEXT FIELD numpos 
     END IF
     EXIT INPUT 

    AFTER FIELD numpos
     -- Verificando punto de venta 
     IF w_datos.numpos IS NULL THEN
        ERROR "Error: debe de seleccionarse el punto de venta listar."
        NEXT FIELD numpos
     END IF

    AFTER INPUT 
     -- Verificando datos
     IF w_datos.numpos IS NULL OR 
        w_datos.fecini IS NULL OR 
        w_datos.fecfin IS NULL OR 
        pipeline       IS NULL THEN
        NEXT FIELD numpos
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Generando el reporte
   CALL facrep032_GeneraReporte()
  END WHILE
 CLOSE WINDOW wrep001a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION facrep003_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE
 IF w_datos.numpos IS NULL OR
    w_datos.fecini IS NULL OR
    w_datos.fecfin IS NULL THEN
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte de ventas x horas   

FUNCTION facrep032_GeneraReporte() 
 DEFINE imp1      datosreporte,                
        s_query   STRING, 
        strfeccor STRING

 -- Definiendo parametros
 LET lg     = 100 
 LET nlines = 36 

 -- Seleccionando nombre del punto de venta 
 SELECT NVL(a.nompos,"")
  INTO  xnompos  
  FROM  fac_puntovta a
  WHERE a.numpos = w_datos.numpos 

 -- Verificando condicion de fecha de corte
 LET strfeccor = " AND x.feccor >= '",w_datos.fecini,"'",
                 " AND x.feccor <= '",w_datos.fecfin,"' "
 
 -- Preparando condiciones del reporte 
 LET s_query = 
  "SELECT x.horini,x.horfin,SUM(x.totdoc),SUM(x.totvta) ",
   "FROM  vis_ventasxhoras x ",
   "WHERE x.numpos = ",w_datos.numpos, 
   strfeccor CLIPPED,
   " GROUP BY 1,2 ORDER BY x.horini" 

 -- Seleccionando datos del reporte
 ERROR "Atencion: seleccionando datos ... por favor espere ..."

 PREPARE s_ventas FROM s_query
 DECLARE c_ventas CURSOR FOR s_ventas
 LET existe    = FALSE
 FOREACH c_ventas INTO imp1.* 
  -- Iniciando reporte
  IF NOT existe THEN
     -- Iniciando la impresion
     START REPORT facrep032_ImprimeReporte TO filename
     LET existe = TRUE
  END IF

  -- Llenado el reporte
  OUTPUT TO REPORT facrep032_ImprimeReporte(imp1.*) 
 END FOREACH
 CLOSE c_ventas
 FREE  c_ventas

 IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT facrep032_ImprimeReporte   

    -- Enviando reporte al destino seleccionado
    CALL librut001_sendreport
    (filename,pipeline,tituloreporte, 
     "--noline-numbers "||
     "--nofooter "||
     "--font-size 10 "||
     "--page-width 842 --page-height 595 "||
     "--left-margin 55 --right-margin 25 "||
     "--top-margin 35 --bottom-margin 45 "||
     "--title Facturacion")

    ERROR "" 
    CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
 ELSE
    ERROR "" 
    CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
 END IF 
END FUNCTION 

-- Subrutina para imprimir el reporte 

REPORT facrep032_ImprimeReporte(imp1)
 DEFINE imp1          datosreporte,
        col           SMALLINT, 
        linea         CHAR(100) 

  OUTPUT LEFT   MARGIN 0 
         TOP    MARGIN 0 
         BOTTOM MARGIN 0 
         PAGE   LENGTH nlines 

  FORMAT 
   PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________"

    -- Imprimiendo encabezado 
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Facturacion",
          COLUMN col,tituloreporte CLIPPED,            
	  COLUMN (lg-20),PAGENO USING "Pagina: <<"

    LET periodo = "PERIODO DEL ",w_datos.fecini," AL ",w_datos.fecfin 
    LET col = librut001_centrado(periodo,lg) 
    PRINT COLUMN   1,"Facrep032",
          COLUMN col,periodo CLIPPED,
          COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    PRINT COLUMN   1,xnompos CLIPPED, 
          COLUMN (lg-20),"Hora  : ",TIME 

    PRINT linea 
    PRINT "Hora Inicial      Hora Final         Numero                    Total "
    PRINT "                                     Ventas                    Ventas"
    PRINT linea

   ON EVERY ROW
    -- Imprimiendo datos 
    PRINT imp1.horini                                              ,13 SPACES, 
          imp1.horfin                                              ,13 SPACES, 
          imp1.totdoc                         USING "###,##&"      ,13 SPACES, 
          imp1.totvta                         USING "##,###,##&.&&"

   ON LAST ROW
    -- Totalizando
    SKIP 1 LINES 
    PRINT COLUMN   1,"TOTAL VENTA X HORAS -->", 
          COLUMN  37,SUM(imp1.totdoc)         USING "###,##&"      ,13 SPACES,
                     SUM(imp1.totvta)         USING "##,###,##&.&&"
END REPORT
