

  drop view vis_ventasxhoras;

create view vis_ventasxhoras
(numpos,feccor,horini,horfin,totdoc,totvta) as

select a.numpos,a.feccor,"00:00","00:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "00:00" and "00:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"07:00","07:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "07:00" and "07:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"08:00","08:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "08:00" and "08:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"09:00","09:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "09:00" and "09:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"10:00","10:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "10:00" and "10:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"11:00","11:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "11:00" and "11:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"12:00","12:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "12:00" and "12:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"13:00","13:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "13:00" and "13:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"14:00","14:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "14:00" and "14:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"15:00","15:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "15:00" and "15:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"16:00","16:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "16:00" and "16:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"17:00","17:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "17:00" and "17:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"18:00","18:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "18:00" and "18:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"19:00","19:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "19:00" and "19:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"20:00","20:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "20:00" and "20:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"21:00","21:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "21:00" and "21:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4
union all
select a.numpos,a.feccor,"22:00","22:59",count(*),sum(a.totdoc)
from   fac_mtransac a
where  extend(a.horsis,hour to minute) between "22:00" and "22:59"
and    a.estado = "V"
and    a.haycor = 1 
group by 1,2,3,4;

grant select on vis_ventasxhoras to public;
