{ 
Programo : facrep035.4gl 
Objetivo : Reporte de otros cobros facturacion 
Ejecución: Desde el menu sin argumentos
           Desde GST se le coloca 1 argumento
}

DATABASE storepos 

-- Definicion de variables globales 

TYPE datosreporte RECORD LIKE pos_mtransac.*

DEFINE w_datos       RECORD
        numpos       LIKE pos_mtransac.numpos,
        tipdoc       LIKE pos_mtransac.tipdoc,
        codepl       LIKE pos_mtransac.codepl,
        fecini       DATE,
        fecfin       DATE
       END RECORD,
       existe        SMALLINT,
       lg,nlines     SMALLINT, 
       tituloreporte STRING, 
       filename      STRING,
       pipeline      STRING,
       xnompos       CHAR(40), 
       np            CHAR(3), 
       s,d           CHAR(1)

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS() = 0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rotroscobrosfac") 
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL facrep035_OtrosCobrosFac()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION facrep035_OtrosCobrosFac()
 DEFINE wpais      VARCHAR(255),
        loop,res   SMALLINT, 
        w          ui.Window

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep035a AT 5,2
  WITH FORM "facrep035a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("facrep035",wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/OtrosCobrosFac.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox
  CALL librut003_CbxPuntosVenta() 

  -- Inicializando datos
  INITIALIZE w_datos.* TO NULL
  CLEAR FORM

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline TO NULL
   LET s = 1 SPACES
   LET d = "~"

   -- Construyendo busqueda
   INPUT BY NAME w_datos.numpos,
                 w_datos.tipdoc, 
                 w_datos.codepl, 
                 w_datos.fecini,
                 w_datos.fecfin 
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Verificando si filtros estan completos
     IF NOT facrep035_FiltrosCompletos() THEN 
        NEXT FIELD numpos
     END IF 
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 

     -- Verificando si filtros estan completos
     IF NOT facrep035_FiltrosCompletos() THEN 
        NEXT FIELD numpos 
     END IF 
     EXIT INPUT 

    ON ACTION excel
     -- Asignando dispositivo
     LET pipeline = "excel"
     LET s        = ASCII(9)
     LET d        = ASCII(9)

     -- Verificando si filtros estan completos
     IF NOT facrep035_FiltrosCompletos() THEN 
        NEXT FIELD numpos 
     END IF 
     EXIT INPUT 

    BEFORE FIELD tipdoc
     -- Llenando combo de tipos de documento x punto de venta
     CALL librut003_CbxTiposDocumentoOtrosCobros() 

    BEFORE FIELD codepl 
     -- Llenando combo de empleados
     CALL librut003_CbxEmpleadosTodos() 

    AFTER INPUT 
     -- Verificando datos
     IF w_datos.numpos IS NULL OR 
        w_datos.fecini IS NULL OR 
        w_datos.fecfin IS NULL OR 
        pipeline IS NULL THEN
        NEXT FIELD numpos
     END IF
    
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Generando el reporte
   CALL facrep035_GeneraReporte()
  END WHILE
 CLOSE WINDOW wrep035a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION facrep035_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE
 IF w_datos.numpos IS NULL OR
    w_datos.fecini IS NULL OR
    w_datos.fecfin IS NULL THEN
    CALL fgl_winmessage(
    "Atencion",
    "Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte de libro de ventas 

FUNCTION facrep035_GeneraReporte()
 DEFINE imp1      datosreporte,
        strfeccor STRING,
        strcodepl STRING,
        strtipdoc STRING,
        s_query   STRING 

 -- Definiendo parametros del reporte
 LET lg     = 130 
 LET nlines = 72
 LET np     = "8" 

 -- Seleccionando nombre del punto de venta 
 SELECT NVL(a.nompos,"")
  INTO  xnompos  
  FROM  fac_puntovta a
  WHERE a.numpos = w_datos.numpos 

 -- Verificando condicion de fechas
 LET strfeccor = " AND x.feccor >= '",w_datos.fecini,"'",
                 " AND x.feccor <= '",w_datos.fecfin,"' "

 -- Verificando condicion de tipo de documento
 LET strtipdoc = " AND x.tipdoc = "||w_datos.tipdoc

 -- Verificando condicion de empleado
 LET strcodepl = " AND x.codepl = "||w_datos.codepl 

 -- Preparando condiciones del reporte 
 LET s_query = 
  "SELECT x.* ",
   "FROM  pos_mtransac x ",
   "WHERE x.numpos = ",w_datos.numpos, 
   " AND  x.haycor = 0 ", 
   strtipdoc CLIPPED,
   strcodepl CLIPPED,
   strfeccor CLIPPED,
   " ORDER BY x.tipdoc,x.codepl,x.feccor,x.nserie,x.numdoc" 

 -- Seleccionando datos del reporte
 ERROR "Atencion: seleccionando datos ... por favor espere ..."

 PREPARE s_otcob FROM s_query
 DECLARE c_otcob CURSOR FOR s_otcob
 LET existe  = FALSE
 FOREACH c_otcob INTO imp1.*
  -- Iniciando reporte
  IF NOT existe THEN
     -- Iniciando la impresion
     START REPORT facrep035_ImprimeReporte TO filename
     LET existe = TRUE
  END IF

  -- Llenado el reporte
  OUTPUT TO REPORT facrep035_ImprimeReporte(imp1.*) 
 END FOREACH
 CLOSE c_otcob
 FREE  c_otcob

 IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT facrep035_ImprimeReporte

     -- Transfiriendo reporte a excel
    IF pipeline = "excel" THEN
      CALL librut005_excel2(filename)
    ELSE
    -- Enviando reporte al destino seleccionado
    CALL librut001_sendreport
    (filename,pipeline,tituloreporte,
     "--noline-numbers "||
     "--nofooter "||
     "--font-size 8 "||
     "--page-width 842 --page-height 595 "||
     "--left-margin 55 --right-margin 25 "||
     "--top-margin 35 --bottom-margin 45 "||
     "--title Facturacion")
    END IF 

    ERROR "" 
    CALL fgl_winmessage(" Atencion","Reporte Emitido.","information") 
 ELSE
    ERROR "" 
    CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
 END IF 
END FUNCTION 

-- Subrutina para imprimir el reporte 

REPORT facrep035_ImprimeReporte(imp1)
 DEFINE imp1          datosreporte,
        wdocumentos   CHAR(32),
        linea         CHAR(130), 
        exis,i,col    SMALLINT,
        lfn           SMALLINT,
        xnomcli       CHAR(40),
        xnomdoc       CHAR(30),
        xnomemp       CHAR(40),
        periodo       STRING 

  OUTPUT LEFT   MARGIN 2 
         PAGE   LENGTH nlines 
         TOP    MARGIN 2
         BOTTOM MARGIN 2 

  FORMAT 
   PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "______________________________"

    -- Ajustando columna 
    LET lfn = lg-20  

    -- Periodo de fechas
    LET periodo = "PERIODO DEL ",w_datos.fecini," AL ",w_datos.fecfin

    -- Imprimiendo encabezado 
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Facturacion",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
	  COLUMN lfn,PAGENO USING "Pagina: <<"

    LET col = librut001_centrado(periodo,lg) 
    PRINT COLUMN   1,"Facrep035",
          COLUMN col,periodo CLIPPED, 
          COLUMN lfn,"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET col = librut001_centrado(xnompos,lg) 
    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,xnompos CLIPPED, 
          COLUMN lfn,"Hora  : ",TIME 

    PRINT linea 
    PRINT "Fecha                                             Serie          ",
          "Numero         T O T A L     Cajero           Hora" 
    PRINT "Emision                                           Numero         ",
          "Documento      DOCUMENTO     Opero            Operacion"
    PRINT linea

   BEFORE GROUP OF imp1.numpos 
    IF (pipeline="excel") THEN
       PRINT UPSHIFT(tituloreporte) CLIPPED,s
       PRINT periodo CLIPPED,s
       PRINT xnompos CLIPPED,s
       PRINT s
       PRINT "Fecha",s,
             "Empleado",s,
             "Serie #",s,
             "Documento #",s,
             "Total Documento",s,
             "Cajero Opero",s,
             "Hora Operacion",s
       PRINT s
    END IF

   BEFORE GROUP OF imp1.tipdoc 
    -- Imprimiendo tipo de documento
    INITIALIZE xnomdoc TO NULL 
    SELECT a.nomdoc INTO xnomdoc FROM fac_tipodocs a WHERE a.tipdoc = imp1.tipdoc
    PRINT xnomdoc,s
    PRINT s      

   BEFORE GROUP OF imp1.codepl 
    -- Imprimiendo empleado
    INITIALIZE xnomemp TO NULL 
    SELECT a.nomemp INTO xnomemp FROM pla_empleads a WHERE a.codepl = imp1.codepl
    PRINT xnomemp,s   
    
   ON EVERY ROW
    -- Imprimiendo datos 
    LET xnomcli = imp1.nomcli 

    IF (pipeline!="excel") THEN 
     PRINT imp1.feccor             USING "dd/mm/yyyy"   ,3 SPACES,
           xnomcli                                      ,2 SPACES, 
           imp1.nserie                                  ,
           imp1.numdoc             USING "&&&&&&&&"     ,3 SPACES,
           imp1.totdoc             USING "##,###,##&.&&",5 SPACES,
           imp1.usrope                                  ,2 SPACES, 
           imp1.horsis                                              
    ELSE 
     PRINT imp1.feccor             USING "dd/mm/yyyy"   ,s,
           xnomcli                                      ,s,
           imp1.nserie                                  ,s,"'",
           imp1.numdoc             USING "&&&&&&&&"     ,s,
           imp1.totdoc             USING "##,###,##&.&&",s,
           imp1.usrope                                  ,s,
           imp1.horsis                                              
    END IF 

   AFTER GROUP OF imp1.codepl
    PRINT COLUMN   1,"Total Empleado  [ ",xnomemp CLIPPED," ]",s,s,s,s,
          COLUMN  77,GROUP SUM(imp1.totdoc)  USING "##,###,##&.&&"
    PRINT s 

   AFTER GROUP OF imp1.tipdoc 
    -- Totalizando
    PRINT COLUMN   1,"Total Documento [ ",xnomdoc CLIPPED," ]",s,s,s,s,
          COLUMN  77,GROUP SUM(imp1.totdoc)  USING "##,###,##&.&&"
    PRINT s

   ON LAST ROW
    -- Totalizando reporte
    PRINT COLUMN   1,"Total Reporte --> "                   ,28 SPACES,s,s,s,s,
          COLUMN  77,SUM(imp1.totdoc)        USING "##,###,##&.&&"
END REPORT
