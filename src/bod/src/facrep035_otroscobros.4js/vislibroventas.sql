
  drop view vis_libroventas;

create view vis_libroventas
(numpos,nompos,tipdoc,nomdoc,lnktdc,nserie,fecemi,numdoc,totgra,totexe,totisv,totdoc) as

select a.numpos,c.nompos,a.tipdoc,b.nomdoc,a.lnktdc,a.nserie,a.fecemi,a.numdoc,

(0),

(select nvl(sum(b.totrec),0) from pos_dtransac b
where b.lnktra = a.lnktra),

(0),

(select nvl(sum(totrec),0) from pos_dtransac b
where b.lnktra = a.lnktra)

from pos_mtransac a,fac_tipodocs b,fac_puntovta c
where a.estado = 1
  and c.numpos = a.numpos
  and b.tipdoc = a.tipdoc

union all

select a.numpos,c.nompos,a.tipdoc,b.nomdoc,a.lnktdc,a.nserie,a.fecemi,a.numdoc,0,0,0,0
from pos_mtransac a,fac_tipodocs b,fac_puntovta c
where a.estado = 0
  and c.numpos = a.numpos
  and b.tipdoc = a.tipdoc;

grant select on vis_libroventas to public;
