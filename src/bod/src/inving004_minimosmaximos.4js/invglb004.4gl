{ 
Fecha    : diciembre 2006 
Programo : Mynor Ramirez 
Objetivo : Programa de variables globales mantenimiento de existencias minimas y maximas 
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
DEFINE w_mae_pro   RECORD LIKE inv_products.*,
       w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_suc   RECORD LIKE glb_sucsxemp.*,
       w_mae_bod   RECORD LIKE inv_mbodegas.*,
       w_mae_exi   RECORD 
        codemp     LIKE inv_proenbod.codemp, 
        codsuc     LIKE inv_proenbod.codsuc, 
        codbod     LIKE inv_proenbod.codbod, 
        codcat     LIKE inv_products.codcat, 
        subcat     LIKE inv_products.subcat 
       END RECORD,
       v_products  DYNAMIC ARRAY OF RECORD
        codpro     LIKE inv_products.cditem, 
        cditem     LIKE inv_products.codabr, 
        dsitem     CHAR(50), 
        nommed     CHAR(30),
        eximin     LIKE inv_proenbod.eximin,
        eximax     LIKE inv_proenbod.eximax,
        endrec     CHAR(1) 
       END RECORD,
       totlin      SMALLINT,
       b           ui.ComboBox,
       cba         ui.ComboBox,
       w           ui.Window,
       f           ui.Form
END GLOBALS
