{ 
Fecha    : Diciiembre 2011        
Programo : Mynor Ramirez
Objetivo : Programa de criterios de seleccion para:
           Mantenimiento de existencias minimas y maximas 
}

-- Definicion de variables globales 
GLOBALS "invglb004.4gl" 

-- Subrutina para consultar las existencias minimas y maximas 

FUNCTION invqbx004_DatosExistencias()
 DEFINE qrytext,qrypart     CHAR(500),
        loop,existe         SMALLINT,
        qryres,msg          CHAR(80)

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Buscando datos 
 LET loop = TRUE 
 WHILE loop 
  -- Inicializando las variables 
  INITIALIZE qrytext,qrypart TO NULL
  LET int_flag = 0 
  CALL inving004_InicializaVariables(1)

  -- Construyendo busqueda 
  CONSTRUCT BY NAME qrytext 
                 ON a.codbod 

   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT CONSTRUCT

   ON ACTION calculator 
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

  END CONSTRUCT
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Preparando la busqueda 
  ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

  -- Creando la busqueda 
  LET qrypart = "SELECT UNIQUE a.codemp,a.codsuc,a.codbod ",
                 "FROM  inv_proenbod a ",
                 "WHERE EXISTS (SELECT b.userid ",
                                "FROM inv_permxbod b ",
                                "WHERE b.userid = USER AND b.codbod = a.codbod) ",
                  " AND ", qrytext CLIPPED,
                  " ORDER BY a.codemp,a.codsuc,a.codbod" 

  -- Declarando el cursor 
  PREPARE curpre FROM qrypart
  DECLARE c_existencias SCROLL CURSOR WITH HOLD FOR curpre 
  OPEN c_existencias 
  FETCH FIRST c_existencias INTO w_mae_exi.* 
  IF (status = NOTFOUND) THEN
     INITIALIZE w_mae_exi.* TO NULL
     ERROR ""
     CALL fgl_winmessage(
     "Atencion",
     "No existen bodegas con el criterio seleccionado.",
     "stop") 
  ELSE
     ERROR "" 

     -- Desplegando datos 
     CALL invqbx004_datos()

     -- Fetchando bodegas 
     MENU "Consultar" 
      COMMAND "Siguiente"
       "Visualiza la siguienre bodega en la lista."
       FETCH NEXT c_existencias INTO w_mae_exi.* 
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas bodegas siguientes en lista.", 
           "information")
           FETCH LAST c_existencias INTO w_mae_exi.* 
       END IF 

       -- Desplegando datos 
       CALL invqbx004_datos()

      COMMAND "Anterior"
       "Visualiza la bodega anterior en la lista."
       FETCH PREVIOUS c_existencias INTO w_mae_exi.*
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas bodegas anteriores en lista.", 
           "information")
           FETCH FIRST c_existencias INTO w_mae_exi.* 
        END IF

        -- Desplegando datos 
        CALL invqbx004_datos()

      COMMAND "Primero" 
       "Visualiza la primer bodega en la lista."
       FETCH FIRST c_existencias INTO w_mae_exi.*
        -- Desplegando datos 
        CALL invqbx004_datos()

      COMMAND "Ultimo" 
       "Visualiza la ultima bodega en la lista."
       FETCH LAST c_existencias INTO w_mae_exi.*
        -- Desplegando datos
        CALL invqbx004_datos()

      COMMAND "Productos"
       "Permite visualizar el detalle completo de los productos de la bodega."
       CALL invqbx004_verproductos()

      ON ACTION cancel    
       -- Salida 
       EXIT MENU

      COMMAND KEY(F4,CONTROL-E)
       EXIT MENU
     END MENU
   END IF     
  CLOSE c_existencias
 END WHILE

 -- Inicializando datos 
 CALL inving004_InicializaVariables(1)
END FUNCTION 

-- Subrutina para desplegar los datos de las existencias 

FUNCTION invqbx004_datos()
 DEFINE existe SMALLINT

 -- Desplegando datos 
 CLEAR FORM

 -- Obteniendo datos de la empresa
 CALL librut003_bempresa(w_mae_exi.codemp)
 RETURNING w_mae_emp.*,existe

 -- Obteniendo datos de la sucursal
 CALL librut003_bsucursal(w_mae_exi.codsuc)
 RETURNING w_mae_suc.*,existe

 -- Desplegando datos de la bodega
 CLEAR FORM 
 DISPLAY BY NAME w_mae_exi.codbod,w_mae_emp.nomemp,w_mae_suc.nomsuc

 -- Seleccionando detalle de productos 
 CALL invqbx004_DetalleProductos() 
END FUNCTION 

-- Subrutina para seleccionar el detalle de productos 

FUNCTION invqbx004_DetalleProductos()
 DEFINE existe  SMALLINT,
        msg     STRING 

 -- Inicializando vector de productos
 CALL inving004_InicializaArray()
 
 -- Seleccionando productos 
 DECLARE cdet CURSOR FOR
 SELECT x.cditem, 
        x.codabr,
        y.despro,
        z.nommed, 
        x.eximin,
        x.eximax,
        "" 
  FROM  inv_proenbod x, inv_products y,inv_unimedid z
  WHERE x.codemp = w_mae_exi.codemp
    AND x.codsuc = w_mae_exi.codsuc
    AND x.codbod = w_mae_exi.codbod
    AND y.cditem = x.cditem 
    AND z.unimed = y.unimed 
  ORDER BY 3 

  LET totlin = 1 
  FOREACH cdet INTO v_products[totlin].*
   -- Incrementando contador
   LET totlin = (totlin+1) 
  END FOREACH
  CLOSE cdet
  FREE  cdet
  LET totlin = (totlin-1)  

 -- Desplegando datos
 DISPLAY ARRAY v_products TO s_products.* 
  BEFORE DISPLAY
   EXIT DISPLAY 
 END DISPLAY 

 -- Desplegando totales
 LET msg = "Total Productos (",totlin USING "<<<,<<<",")"
 CALL f.setElementText("labelt",msg)
END FUNCTION 

-- Subrutina para ver el detalle de productos

FUNCTION invqbx004_verproductos()
 -- Desplegando productos
 DISPLAY ARRAY v_products TO s_products.*
  ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

  ON ACTION cancel
   -- Salida
   EXIT DISPLAY

  ON ACTION calculator
   -- Cargando calculadora
   IF NOT winshellexec("calc") THEN
      ERROR "Atencion: calculadora no disponible."
   END IF

  BEFORE ROW
   -- Verificando control del total de lineas
   IF (ARR_CURR()>totlin) THEN
      CALL FGL_SET_ARR_CURR(1)
   END IF
 END DISPLAY
END FUNCTION 
