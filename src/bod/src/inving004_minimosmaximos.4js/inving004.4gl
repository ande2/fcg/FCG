{
Programo : Mynor Ramirez
Objetivo : Programa de mantenimiento de existencias minimas y maximas
}

-- Definicion de variables globales
GLOBALS "invglb004.4gl"
CONSTANT progname = "inving004"
DEFINE username   VARCHAR(15),
       regreso    SMALLINT,
       existe     SMALLINT,
       msg        STRING 

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/ToolBarFisico")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("minimosmaximos")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario
 LET username = FGL_GETENV("LOGNAME") 

 -- Menu de opciones
 CALL inving004_Menu()
END MAIN

-- Subrutina para el menu de registro de existencias minimas y maximas 

FUNCTION inving004_Menu()
 DEFINE regreso    SMALLINT, 
        wpais      VARCHAR(255), 
        titulo     STRING 

 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing002a AT 5,2  
  WITH FORM "inving004a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ 

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header(progname,wpais,1)

  -- Inicializando datos 
  CALL inving004_InicializaVariables(1)

  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()

  -- Menu de opciones
  MENU "Existencias Minimas y Maximas" 
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Consultar"
    END IF
    -- Registrar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Registrar"
    END IF
   COMMAND "Consultar" 
    "Consulta de existencias minimas y maximas."
    CALL invqbx004_DatosExistencias()
   COMMAND "Registrar" 
    "Registro o modificacion de existencias minimas y maximas."
    CALL inving004_DatosExistencias() 
   COMMAND "Salir"
    "Abandona el menu." 
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wing002a
END FUNCTION

-- Subrutina para la seleccion de la bodega y productos para el registro de existencias 

FUNCTION inving004_DatosExistencias()
 DEFINE loop,existe,retroceso SMALLINT

 -- Inicio del loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop   

  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos 
     CALL inving004_InicializaVariables(1) 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_exi.codbod,
                w_mae_exi.codcat,
                w_mae_exi.subcat WITHOUT DEFAULTS
    ATTRIBUTES(UNBUFFERED) 

   ON ACTION cancel 
    -- Salida 
    IF INFIELD(codbod) THEN
       LET loop = FALSE
       CALL inving004_InicializaVariables(1)
       EXIT INPUT
    ELSE
       CALL inving004_InicializaVariables(1)
       NEXT FIELD codbod
    END IF 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON CHANGE codbod 
    -- Obteniendo datos de la bodega 
    CALL librut003_bbodega(w_mae_exi.codbod) 
    RETURNING w_mae_bod.*,existe 

    -- Asignando datos de empresa y sucursal de la bodega
    LET w_mae_exi.codemp = w_mae_bod.codemp 
    LET w_mae_exi.codsuc = w_mae_bod.codsuc 

    -- Obteniendo datos de la empresa 
    CALL librut003_bempresa(w_mae_bod.codemp)
    RETURNING w_mae_emp.*,existe 
    -- Obteniendo datos de la sucursal 
    CALL librut003_bsucursal(w_mae_bod.codsuc)
    RETURNING w_mae_suc.*,existe 
   
    -- Desplegando datos de la empresa y sucursal 
    DISPLAY BY NAME w_mae_emp.nomemp,w_mae_suc.nomsuc

    -- Limpiando combos de categorias y subcategorias
    LET w_mae_exi.codcat = NULL
    LET w_mae_exi.subcat = NULL
    CLEAR codcat,subcat

   ON CHANGE codcat
    -- Limpiando combos
    LET w_mae_exi.subcat = NULL
    CLEAR subcat

    -- Llenando combox de subcategorias
    IF w_mae_exi.codcat IS NOT NULL THEN
       CALL librut003_cbxsubcategorias(w_mae_exi.codcat)
    END IF

   AFTER FIELD codbod
    -- Verificando bodega
    IF w_mae_exi.codbod IS NULL THEN
       ERROR "Error: bodega invalida, VERIFICA."
       NEXT FIELD codbod 
    END IF 

   AFTER INPUT
    -- Verificando datos
    IF w_mae_exi.codbod IS NULL THEN
       NEXT FIELD codbod
    END IF
  END INPUT 
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Cargando productos de la bodega 
  CALL inving004_CargaProductosBodega()
  IF (totlin>0) THEN
     -- Registrando existencias 
     LET retroceso = inving004_RegistroExistencias()
  ELSE
     CALL fgl_winmessage(
     "Atencion",
     "No existen productos registrados en la bodega seleccionada. \n"||
     "Verifica que existan productos en la bodega seleccionada.",
     "stop")
  END IF 
 END WHILE

 -- Inicializando datos 
 CALL inving004_InicializaVariables(1) 
END FUNCTION

-- Subutina para registrar las existencias minimas y maximas 

FUNCTION inving004_RegistroExistencias() 
 DEFINE loop       SMALLINT,
        opc        SMALLINT, 
        retroceso  SMALLINT 

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop
  -- Ingresando productos
  INPUT ARRAY v_products WITHOUT DEFAULTS FROM s_products.*
       ATTRIBUTE(MAXCOUNT=totlin,INSERT ROW=FALSE,
                 APPEND ROW=FALSE,DELETE ROW=FALSE,
                 ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)

   ON ACTION ACCEPT
    -- Aceptar
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET loop = FALSE
    EXIT INPUT

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop      = FALSE
    LET retroceso = FALSE
   WHEN 1 -- Grabando
    LET loop      = FALSE
    LET retroceso = FALSE

    -- Grabando existencias
    CALL inving004_GrabarExistencias() 

   WHEN 2 -- Modificando
    LET loop = TRUE 
  END CASE
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subrutina para cargar el producto de la bodega seleccionada 

FUNCTION inving004_CargaProductosBodega() 
 DEFINE wsalval    LIKE inv_dtransac.totpro, 
        strcodcat  STRING,
        strsubcat  STRING,
        qrytext    STRING

 -- Verificando seleccion de categoria
 LET strcodcat = NULL
 IF w_mae_exi.codcat IS NOT NULL THEN
    LET strcodcat = "AND d.codcat = ",w_mae_exi.codcat
 END IF

 -- Verificando condicion de subcategoria
 LET strsubcat = NULL
 IF w_mae_exi.subcat IS NOT NULL THEN
    LET strsubcat = "AND e.subcat = ",w_mae_exi.subcat
 END IF

 -- Preparando seleccion de productos
 LET qrytext =
   "SELECT x.cditem,y.codabr,y.dsitem,z.nommed,x.eximin,x.eximax ",
    "FROM  inv_proenbod x,inv_products y,glb_categors d,glb_subcateg e,inv_unimedid z ",
    "WHERE x.codemp = ",w_mae_exi.codemp, 
     " AND x.codsuc = ",w_mae_exi.codsuc, 
     " AND x.codbod = ",w_mae_exi.codbod, 
     " AND x.cditem = y.cditem ",
     " AND y.estado = 1",
     " AND d.codcat = y.codcat ",
     " AND e.codcat = y.codcat ",
     " AND e.subcat = y.subcat ",
     " AND z.unimed = y.unimed ",
     strcodcat CLIPPED," ",
     strsubcat CLIPPED," ",
     "ORDER BY 3"  

 -- Seleccionando productos
 PREPARE cpexi FROM qrytext 
 DECLARE cprod CURSOR FOR cpexi 
 LET totlin = 1
 FOREACH cprod INTO v_products[totlin].* 
  -- Incrementando contador 
  LET totlin = (totlin+1)
 END FOREACH
 CLOSE cprod
 FREE  cprod
 LET totlin = (totlin-1)

 -- Desplegando totales
 LET msg = "Total Productos (",totlin USING "<<<,<<<",")"
 CALL f.setElementText("labelt",msg) 
END FUNCTION 

-- Subrutina para grabar las existencias

FUNCTION inving004_GrabarExistencias() 
 DEFINE i,finmes SMALLINT

 -- Grabando transaccion
 ERROR " Registrando Exitencias ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

   -- 1. Grabando existencias
   FOR i = 1 TO totlin 
    IF v_products[i].cditem IS NULL OR 
       v_products[i].eximin IS NULL OR
       v_products[i].eximax IS NULL THEN 
       CONTINUE FOR 
    END IF 

    -- Eliminando registro antes de insertar
    SET LOCK MODE TO WAIT
    UPDATE inv_proenbod 
       SET inv_proenbod.eximin = v_products[i].eximin, 
           inv_proenbod.eximax = v_products[i].eximax 
     WHERE inv_proenbod.codemp = w_mae_exi.codemp
       AND inv_proenbod.codsuc = w_mae_exi.codsuc
       AND inv_proenbod.codbod = w_mae_exi.codbod
       AND inv_proenbod.cditem = v_products[i].codpro 
   END FOR 

 -- Finalizando la transaccion
 COMMIT WORK

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion"," Existencias Registradas.","information")

 ERROR "" 
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION inving004_InicializaVariables(i)
 DEFINE i SMALLINT
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_exi.* TO NULL 
   CLEAR FORM 
 END CASE 

 -- Inicializando datos
 LET totlin = 0 

 -- Lllenando combo de bodegas
 CALL librut003_cbxbodegasxusuario(username) 

 -- Inicializando vectores de datos
 CALL inving004_InicializaArray() 

 -- Desplegando total de unidades del inventario 
 CALL f.setElementText("labelt","Total Productos (0)")
END FUNCTION

-- Subrutina para inicializar y limpiar vector de trabajo 

FUNCTION inving004_InicializaArray()
 DEFINE i SMALLINT

 -- Inicializando vectores
 CALL v_products.clear()
 LET totlin = 0 

 -- Limpiando detalle 
 DISPLAY ARRAY v_products TO s_products.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION 
