  drop view vis_estadisticaventas;

create view vis_estadisticaventas
(numpos, codemp, nserie, numdoc, fecemi, feccor,
 codcat, nomcat, cditem, dsitem, codabr, cantid,
 prevta, totrec, totisv, totdes)
as
select a.numpos,
       a.codemp,
       a.nserie,
       a.numdoc,
       a.fecemi,
       a.feccor,
       c.codcat,
       e.nomcat,
       b.cditem,
       c.dsitem,
       b.codabr,
       b.cantid,
       b.preuni,
       b.totpro,
       b.totisv,
       b.descto
from   fac_mtransac a,fac_dtransac b,inv_products c,glb_categors e
where  a.lnktra = b.lnktra
  and  a.estado = "V"
  and  a.haycor = 1
  and  c.cditem = b.cditem
  and  e.codcat = c.codcat;

grant select on vis_estadisticaventas to public;
