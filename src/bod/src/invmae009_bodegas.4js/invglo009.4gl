{ 
invglo009.4gl
Mynor Ramirez
Mantenimiento de bodegas
}

DATABASE storepos 

{ Definicion de variables globales }

GLOBALS
CONSTANT progname = "invmae009"
DEFINE w_mae_pro   RECORD LIKE inv_mbodegas.*,
       v_mbodegas  DYNAMIC ARRAY OF RECORD
        tcodemp    LIKE inv_mbodegas.codemp,
        tnomemp    LIKE glb_empresas.nomemp, 
        tcodbod    LIKE inv_mbodegas.codbod,
        tnombod    LIKE inv_mbodegas.nombod 
       END RECORD,
       v_permxusr  DYNAMIC ARRAY OF RECORD
        tcheckb    SMALLINT,
        tuserid    LIKE inv_permxbod.userid,
        tnomusr    VARCHAR(50),
        tusuaid    LIKE inv_permxbod.usuaid,
        tfecsis    LIKE inv_permxbod.fecsis,
        thorsis    LIKE inv_permxbod.horsis,
        tendrec    CHAR(1)
       END RECORD,
       username    VARCHAR(15) 
END GLOBALS
