{
invqbe009.4gl 
Mynor Ramirez
Mantenimiento de bodegas 
}

{ Definicion de variables globales }

GLOBALS "invglo009.4gl" 
DEFINE totlin,totusr INT, 
       w             ui.Window,
       f             ui.Form

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe009_bodegas(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL invqbe009_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae009_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codemp,a.codbod,a.nombod,a.nomabr,
                                a.chkexi,a.hayfac,a.userid,a.fecsis,
                                a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL invmae009_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progres ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codemp,b.nomemp,a.codbod,a.nombod,'' ",
                 " FROM inv_mbodegas a,glb_empresas b ",
                 " WHERE b.codemp = a.codemp AND ",qrytext CLIPPED,
                 " ORDER BY 2,3,4 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_bodegas SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_mbodegas.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_bodegas INTO v_mbodegas[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_bodegas
   FREE  c_bodegas
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL invqbe009_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_mbodegas TO s_bodegas.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel  
      -- Salida
      CALL invmae009_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar
      CALL invmae009_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF invmae009_bodegas(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL invqbe009_datos(v_mbodegas[ARR_CURR()].tcodbod)
         CALL invqbe009_permxusr(v_mbodegas[ARR_CURR()].tcodbod,1)
      END IF 

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN
       IF invmae009_bodegas(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL invqbe009_datos(v_mbodegas[ARR_CURR()].tcodbod)
          CALL invqbe009_permxusr(v_mbodegas[ARR_CURR()].tcodbod,1)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF invqbe009_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Esta bodega ya tiene registros. \n Bodega no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar esta bodega ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                   msg,
                                   "Si",
                                   "No",
                                   NULL,
                                   NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL invmae009_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION accesos
      IF (totusr=0) THEN
         ERROR "Atencion: no existen usuarios con accesos autorizados."
      END IF

      -- Desplegando accesos x usuario
      CALL invqbe009_detpermxusr(v_mbodegas[ARR_CURR()].tcodbod)

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Empresa"
      LET arrcols[2] = "Bodega"
      LET arrcols[3] = "Nombre Bodega" 
      LET arrcols[4] = "Nombre Abreviado" 
      LET arrcols[5] = "Chequea Existencia"
      LET arrcols[6] = "Bodega Factura"
      LET arrcols[7] = "Usuario Registro"
      LET arrcols[8] = "Fecha Registro"
      LET arrcols[9] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry    = "SELECT b.nomemp,a.codbod,a.nombod,a.nomabr,",
                          "CASE (a.chkexi) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END,",
                          "CASE (a.hayfac) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END,",
                          " a.userid,a.fecsis,a.horsis ", 
                   " FROM inv_mbodegas a,glb_empresas b ",
                   " WHERE b.codemp = a.codemp AND ",qrytext CLIPPED,
                   " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res    = librut002_excelreport("Bodegas",qry,9,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a opcion de accesos
         IF NOT seclib001_accesos(progname,6,username) THEN
            CALL DIALOG.setActionActive("accesos",FALSE)
         ELSE
            CALL DIALOG.setActionActive("accesos",TRUE)
         END IF

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
	 CALL DIALOG.setActionActive("accesos",FALSE)
	 CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
	 CALL DIALOG.setActionActive("accesos",FALSE)
	 CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

      -- Escondiendo campo de seleccion de accesos de usuario
      CALL f.setFieldHidden("tcheckb",1)

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL invqbe009_datos(v_mbodegas[ARR_CURR()].tcodbod)
         CALL invqbe009_permxusr(v_mbodegas[ARR_CURR()].tcodbod,1)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen bodegas con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Desplegando estado del menu 
  CALL invqbe009_EstadoMenu(0," ") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION invqbe009_datos(wcodbod)
 DEFINE wcodbod LIKE inv_mbodegas.codbod,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  inv_mbodegas a "||
              "WHERE a.codbod = "||wcodbod||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_bodegast SCROLL CURSOR WITH HOLD FOR cprodt

 -- Lllenando combobox
 CALL librut003_cbxempresas() 

 -- Llenando vector de seleccion
 FOREACH c_bodegast INTO w_mae_pro.*
 END FOREACH
 CLOSE c_bodegast
 FREE  c_bodegast

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nombod THRU w_mae_pro.hayfac  
 DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.codbod,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para buscar los accesos a usuarios

FUNCTION invqbe009_permxusr(wcodbod,opcion)
 DEFINE wcodbod   LIKE inv_mbodegas.codbod,
        i,opcion  SMALLINT,
        qrytext   STRING

 -- Inicializando vector de datos
 CALL v_permxusr.clear()

 -- Seleccionando datos
 CASE (opcion)
  WHEN 1 -- Consulta de usuarios con acceso
   LET qrytext = "SELECT 1,a.userid,b.nomusr,a.usuaid,a.fecsis,a.horsis "||
                  "FROM  inv_permxbod a, glb_usuarios b " ||
                  "WHERE a.codbod = "||wcodbod||
                   " AND b.userid = a.userid "||
                   " ORDER BY a.fecsis DESC "

  WHEN 2 -- Cargando usuarios para seleccionar acceso
   LET qrytext = "SELECT 0,a.userid,a.nomusr,USER,CURRENT,CURRENT HOUR TO SECOND "||
                  "FROM  glb_usuarios a " ||
                  "WHERE NOT exists (SELECT b.userid FROM inv_permxbod b "||
                                     "WHERE b.codbod = "||wcodbod||
                                      " AND b.userid = a.userid) "||
                   "ORDER BY 2"
 END CASE

 -- Seleccionando datos
 PREPARE cp1 FROM qrytext
 DECLARE cperm CURSOR FOR cp1
 LET totusr = 1
 FOREACH cperm INTO v_permxusr[totusr].*
  LET totusr = (totusr+1)
 END FOREACH
 CLOSE cperm
 FREE  cperm
 LET totusr = (totusr-1)

 -- Desplegando datos
 DISPLAY ARRAY v_permxusr TO s_permxusr.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION

-- Subrutina para visualizar el detalle de accesos a usuarios

FUNCTION invqbe009_detpermxusr(wcodbod)
 DEFINE wcodbod    LIKE inv_mbodegas.codbod,
        loop,opc,i SMALLINT

 -- Desplegando usuarios
 LET loop = TRUE
 WHILE loop
  DISPLAY ARRAY v_permxusr TO s_permxusr.*
   ATTRIBUTE(COUNT=totusr,ACCEPT=FALSE,CANCEL=FALSE)
   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT DISPLAY

   ON ACTION agregarusers
    -- Agregando usuarios
    CALL invqbe009_permxusr(wcodbod,2)
    IF (totusr=0) THEN
        CALL fgl_winmessage(
        " Atencion",
        " No existen mas usuarios para agregar.\n Todos los usuarios ya tienen acceso.",
        "stop")

       -- Cargando usuarios con accesos
       CALL invqbe009_permxusr(wcodbod,1)
       EXIT DISPLAY
    ELSE
      -- Mostrando campo de seleccion de accesos de usuario
      CALL f.setFieldHidden("tcheckb",0)

      -- Seleccionando usuarios
      INPUT ARRAY v_permxusr WITHOUT DEFAULTS FROM s_permxusr.*
       ATTRIBUTE(MAXCOUNT=totusr,INSERT ROW=FALSE,
                 APPEND ROW=FALSE,DELETE ROW=FALSE,
                 ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)

       ON ACTION cancel
        -- Salida
        EXIT INPUT

       ON ACTION accept
        -- Verificando si existen usuarios que agregar
        IF NOT invqbe009_hayusuarios(1) THEN 
           ERROR "Atencion: no existen usuarios marcados que agregar."
           CONTINUE INPUT
        END IF

        -- Grabando usuarios seleccionados
        LET opc = librut001_menugraba("Confirmacion de Accesos",
                                      "Que desea hacer ?",
                                      "Guardar",
                                      "Modificar",
                                      "Cancelar",
                                      NULL)

        CASE (opc)
         WHEN 0 -- Cancelando
          EXIT INPUT
         WHEN 1 -- Grabando
          -- Iniciando la transaccion
          BEGIN WORK

          -- Guardando accesos
          FOR i = 1 TO totusr
           IF (v_permxusr[i].tcheckb=0) OR
              (v_permxusr[i].tcheckb IS NULL) THEN
              CONTINUE FOR
           END IF

           -- Grabando
           SET LOCK MODE TO WAIT
           INSERT INTO inv_permxbod
           VALUES (wcodbod,
                   v_permxusr[i].tuserid,
                   v_permxusr[i].tusuaid,
                   v_permxusr[i].tfecsis,
                   v_permxusr[i].thorsis)
          END FOR

          -- Terminando la transaccion
          COMMIT WORK

          EXIT INPUT
         WHEN 2 -- Modificando
          CONTINUE INPUT
        END CASE

       BEFORE ROW
        -- Verificando control del total de lineas
        IF (ARR_CURR()>totusr) THEN
           CALL FGL_SET_ARR_CURR(1)
        END IF
      END INPUT

      -- Escondiendo campo de seleccion de accesos de usuario
      CALL f.setFieldHidden("tcheckb",1)

      -- Cargando usuarios con accesos
      CALL invqbe009_permxusr(wcodbod,1)
      EXIT DISPLAY
    END IF

   ON ACTION eliminarusers
    -- Verificando si existen accesos 
    IF (totusr=0) THEN
       ERROR "Atencion: no existen usuarios con accesos autorizados que borrar."
       EXIT DISPLAY
    END IF 

    -- Eliminando usuarios
    -- Mostrando campo de seleccion de accesos de usuario
    CALL f.setFieldHidden("tcheckb",0)

    -- Seleccionando usuarios
    INPUT ARRAY v_permxusr WITHOUT DEFAULTS FROM s_permxusr.*
     ATTRIBUTE(MAXCOUNT=totusr,INSERT ROW=FALSE,
               APPEND ROW=FALSE,DELETE ROW=FALSE,
               ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
     ON ACTION cancel
      -- Salida
      EXIT INPUT

     ON ACTION accept
      -- Verificando si existen usuarios que borrar  
      IF NOT invqbe009_hayusuarios(0) THEN 
         ERROR "Atencion: no existen usuarios desmarcados que borrar."
         CONTINUE INPUT
      END IF

      -- Grabando usuarios seleccionados
      LET opc = librut001_menugraba("Confirmacion de Accesos",
                                      "Que desea hacer ?",
                                      "Guardar",
                                      "Modificar",
                                      "Cancelar",
                                      NULL)

      CASE (opc)
       WHEN 0 -- Cancelando
        EXIT INPUT
       WHEN 1 -- Grabando
        -- Iniciando la transaccion
        BEGIN WORK

        -- Guardando accesos
        FOR i = 1 TO totusr
         IF (v_permxusr[i].tcheckb=1) OR
            (v_permxusr[i].tcheckb IS NULL) THEN
            CONTINUE FOR
         END IF

         -- Eliminando
         SET LOCK MODE TO WAIT
         DELETE FROM inv_permxbod
         WHERE inv_permxbod.codbod = wcodbod
           AND inv_permxbod.userid = v_permxusr[i].tuserid
        END FOR

        -- Terminando la transaccion
        COMMIT WORK

        EXIT INPUT
       WHEN 2 -- Modificando
        CONTINUE INPUT
      END CASE

     BEFORE ROW
      -- Verificando control del total de lineas
      IF (ARR_CURR()>totusr) THEN
         CALL FGL_SET_ARR_CURR(1)
      END IF
    END INPUT

    -- Escondiendo campo de seleccion de accesos de usuario
    CALL f.setFieldHidden("tcheckb",1)

    -- Cargando usuarios con accesos
    CALL invqbe009_permxusr(wcodbod,1)
    EXIT DISPLAY

   BEFORE ROW
    -- Verificando control del total de lineas
    IF (ARR_CURR()>totusr) THEN
       CALL FGL_SET_ARR_CURR(1)
    END IF
  END DISPLAY
 END WHILE
END FUNCTION

-- Subrutina para verificar si existen usuarios seleccionados que agregar o que borrar

FUNCTION invqbe009_hayusuarios(estado)
 DEFINE i,estado,hayusr SMALLINT

 -- Chequeando estado
 LET hayusr = 0
 FOR i = 1 TO totusr
  -- Verificando estado
  IF (v_permxusr[i].tcheckb=estado) THEN
     LET hayusr = 1
     EXIT FOR
  END IF
 END FOR

 RETURN hayusr
END FUNCTION

-- Subrutina para verificar si la bodega ya tiene registros 

FUNCTION invqbe009_integridad()
 DEFINE conteo INTEGER  

 -- Verificando movimientos   
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_mtransac a
  WHERE (a.codbod = w_mae_pro.codbod) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION invqbe009_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Bodegas - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Bodegas - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Bodegas - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Bodegas - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Bodegas - NUEVO")
 END CASE
END FUNCTION
