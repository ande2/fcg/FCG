{ 
Programo : facrep009.4gl 
Objetivo : Reporte de estadistica de ventas
}

DATABASE storepos 

-- Definicion de variables globales 
TYPE   datosreporte  RECORD 
        coddiv       LIKE sre_mrecetas.coddiv,
        nomdiv       CHAR(40), 
        lnkrec       LIKE sre_mrecetas.lnkrec,
        numrec       LIKE sre_mrecetas.numrec,
        nomrec       CHAR(50),
        cantid       DEC(12,2),
        totrec       DEC(14,3)
       END RECORD
DEFINE w_datos       RECORD
        numpos       LIKE pos_mtransac.numpos,
        coddiv       LIKE sre_mrecetas.coddiv,
        fecini       DATE,
        fecfin       DATE
       END RECORD,
       totalreporte  DEC(14,2), 
       totalporcen   DEC(5,2),  
       existe,lg     SMALLINT,
       nlines        SMALLINT,
       primeravez    SMALLINT, 
       np            CHAR(3), 
       tituloreporte STRING,
       filename      STRING,
       pipeline      STRING,
       s,d           CHAR(1) 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("restadisticaventas")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL facrep009_EstadisticaVentas()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION facrep009_EstadisticaVentas()
 DEFINE wpais      VARCHAR(255),
        w          ui.Window,
        loop,res   SMALLINT

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep001a AT 5,2
  WITH FORM "facrep009a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("facrep009",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/EstadisticaVentas.spl"

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET tituloreporte = w.getText()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox
  CALL librut003_Cbxpuntosventa()
  CALL librut003_CbxDivisionesRecetas()

  -- Inicializando datos
  INITIALIZE w_datos.* TO NULL
  LET w_datos.numpos = 1 
  LET w_datos.fecini = TODAY 
  LET w_datos.fecfin = TODAY 
  CLEAR FORM

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline TO NULL
   LET primeravez = TRUE 
   LET s = 1 SPACES
   LET d = "~"

   -- Construyendo busqueda
   INPUT BY NAME w_datos.numpos,
                 w_datos.coddiv,
                 w_datos.fecini, 
                 w_datos.fecfin 
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Verificando si filtros estan completos
     IF NOT facrep009_FiltrosCompletos() THEN 
        NEXT FIELD numpos 
     END IF 
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 
     
     -- Verificando si filtros estan completos
     IF NOT facrep009_FiltrosCompletos() THEN 
        NEXT FIELD numpos 
     END IF 
     EXIT INPUT

    ON ACTION excel
     -- Asignando dispositivo
     LET pipeline = "excel"
     LET s        = ASCII(9)
     LET d        = ASCII(9)

     -- Verificando si filtros estan completos
     IF NOT facrep009_FiltrosCompletos() THEN 
        NEXT FIELD numpos 
     END IF 
     EXIT INPUT 

    AFTER INPUT 
     -- Verificando datos
     IF w_datos.fecini IS NULL OR 
        w_datos.fecfin IS NULL OR 
        pipeline       IS NULL THEN
        NEXT FIELD numpos
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Generando el reporte
   CALL facrep009_GeneraEstadisticaVentas()
  END WHILE
 CLOSE WINDOW wrep001a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION facrep009_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE
 IF w_datos.fecini IS NULL OR
    w_datos.fecfin IS NULL THEN
    CALL fgl_winmessage(
    "Atencion",
    "Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte de estadistica de ventas

FUNCTION facrep009_GeneraEstadisticaVentas()
 DEFINE w_mae_est datosreporte,
        s_query   STRING, 
        s_total   STRING, 
        strnumpos STRING,
        strcodemp STRING,
        strfeccor STRING,
        strcoddiv STRING    

 -- Parametros reporte
 LET lg     = 150 
 LET nlines = 50 

 -- Verificando condicion de punto de venta 
 LET strnumpos = NULL
 IF w_datos.numpos IS NOT NULL THEN
    LET strnumpos = " WHERE x.numpos = ",w_datos.numpos 
 ELSE 
    LET strnumpos = " WHERE x.numpos IS NOT NULL "
 END IF 

 -- Verificando condicion de empresa
 LET strcodemp = " AND x.codemp IS NOT NULL "

 -- Verificando fecha de corte
 LET strfeccor = " AND x.feccor >= '",w_datos.fecini,"'",
                 " AND x.feccor <= '",w_datos.fecfin,"' "

 -- Verificando condicion de division de platos 
 LET strcoddiv = NULL
 IF w_datos.coddiv IS NOT NULL THEN
    LET strcoddiv = " AND x.coddiv = ",w_datos.coddiv 
 END IF 

 -- Preparando total del reporte 
 LET totalporcen = 0 
 LET s_total = 
  "SELECT NVL(SUM(x.totrec),0) ",
   "FROM  vis_estadisticaventas x ",
   strnumpos CLIPPED, 
   strcodemp CLIPPED, 
   strfeccor CLIPPED, 
   strcoddiv CLIPPED 
 PREPARE s_total FROM s_total 
 DECLARE c_total CURSOR FOR s_total
 FOREACH c_total INTO totalreporte 
 END FOREACH
 CLOSE c_total
 FREE  c_total 

 -- Preparando condiciones del reporte 
 LET s_query = 
  "SELECT x.coddiv,x.nomdiv,x.lnkrec,x.numrec,x.nomrec,SUM(x.cantid),SUM(x.totrec) ",
   "FROM  vis_estadisticaventas x ",
   strnumpos CLIPPED, 
   strcodemp CLIPPED, 
   strfeccor CLIPPED, 
   strcoddiv CLIPPED, 
   " GROUP BY 1,2,3,4,5",
   " ORDER BY 2,4 " 

 -- Seleccionando datos del reporte
 ERROR "Atencion: seleccionando datos ... por favor espere ..."

 PREPARE s_estad FROM s_query
 DECLARE c_estad CURSOR FOR s_estad
 LET existe    = FALSE
 FOREACH c_estad INTO w_mae_est.*
  -- Iniciando reporte
  IF NOT existe THEN
     -- Iniciando la impresion
     START REPORT facrep009_GeneraReporte TO filename
     LET existe = TRUE
  END IF

  -- Llenado el reporte
  OUTPUT TO REPORT facrep009_GeneraReporte(w_mae_est.*) 
 END FOREACH
 CLOSE c_estad
 FREE  c_estad

 IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT facrep009_GeneraReporte 

     -- Transfiriendo reporte a excel
    IF pipeline = "excel" THEN
      CALL librut005_excel(filename)
    ELSE
    -- Enviando reporte al destino seleccionado
    CALL librut001_sendreport
    (filename,pipeline,tituloreporte,
     "--noline-numbers "||
     "--nofooter "||
     "--font-size 8 "||
     "--page-width 842 --page-height 595 "||
     "--left-margin 55 --right-margin 25 "||
     "--top-margin 35 --bottom-margin 45 "||
     "--title Facturacion")
    END IF
 
    ERROR "" 
    CALL fgl_winmessage(" Atencion","Reporte Emitido.","information") 
 ELSE
    ERROR "" 
    CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
 END IF 
END FUNCTION 

-- Subrutina para imprimir el reporte 

REPORT facrep009_GeneraReporte(imp1)
 DEFINE imp1          datosreporte, 
        porcentaje    DEC(5,2), 
        wnompos       CHAR(40), 
        linea         CHAR(161), 
        exis,i,col    SMALLINT,
        lfn           SMALLINT,
        periodo       STRING

  OUTPUT LEFT   MARGIN 0 
         TOP    MARGIN 0 
         BOTTOM MARGIN 0 
         PAGE   LENGTH nlines 

  FORMAT 
   PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________"

    -- Verificando si reporte es por punto de venta
    IF w_datos.numpos IS NOT NULL THEN
      -- Seleccionando nombre del punto de venta
      SELECT NVL(a.nompos,"")
       INTO  wnompos
       FROM  fac_puntovta a 
       WHERE a.numpos = w_datos.numpos 
    ELSE
       LET wnompos = "PUNTO DE VENTA TODOS" 
    END IF

    -- Imprimiendo encabezado 
    LET lfn = (lg-20) 
    LET col = librut001_centrado(tituloreporte,lg)
    PRINT COLUMN   1,"Facturacion",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,
	  COLUMN lfn,PAGENO USING "Pagina: <<"

    LET periodo = "PERIODO DEL ",w_datos.fecini," AL ",w_datos.fecfin 
    LET col = librut001_centrado(periodo,lg)
    PRINT COLUMN   1,"Facrep009",
          COLUMN col,periodo CLIPPED,
          COLUMN lfn,"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    PRINT COLUMN   1,wnompos CLIPPED,
          COLUMN lfn,"Hora  : ",TIME 

    PRINT linea 
    PRINT "Plato       Descripcion del Plato                                ",
          "     Total           Precio             Precio      Porcentaje" 
    PRINT "                                                                 ",
          "     Cantidad        Promedio           Total    Participacion"
    PRINT linea

   BEFORE GROUP OF imp1.coddiv 
    IF (pipeline="excel") AND primeravez THEN
       PRINT UPSHIFT(tituloreporte),s
       PRINT wnompos CLIPPED,s
       PRINT periodo CLIPPED,s
       PRINT s
       PRINT "Producto",s,"Descripcion del Producto",s,"Total",s,"Precio",s,"Precio",s,
             "Porcentaje"
       PRINT s,s,"Cantidad",s,"Promedio",s,"Total",s,"Participacion",s 
       PRINT s
       LET primeravez = FALSE 
    END IF
   
    -- Imprimiendo division  
    PRINT "DIVISION DE PLATOS: [ ",imp1.nomdiv CLIPPED," ]",s 

   ON EVERY ROW
    -- Imprimiendo datos 
    IF (pipeline!="excel") THEN 
     PRINT imp1.numrec                                               ,1 SPACES,s, 
           imp1.nomrec                                               ,1 SPACES,s, 
           imp1.cantid                         USING "###,###,##&.&&",2 SPACES,s,
           (imp1.totrec/imp1.cantid)           USING  "##,###,##&.&&",2 SPACES,s,
           imp1.totrec                         USING "###,###,##&.&&",s
    ELSE 
     PRINT imp1.numrec                                               ,s, 
           imp1.nomrec                                               ,s, 
           imp1.cantid                         USING "###,###,##&.&&",s,
           (imp1.totrec/imp1.cantid)           USING  "##,###,##&.&&",s,
           imp1.totrec                         USING "###,###,##&.&&",s
    END IF 

   AFTER GROUP OF imp1.coddiv
    -- Calculando porcentaje 
    LET porcentaje  = ((GROUP SUM(imp1.totrec)/totalreporte)*100) 
    LET totalporcen = (totalporcen+porcentaje) 

    -- Totalizando por division 
    IF (pipeline!="excel") THEN 
     PRINT COLUMN   1,"TOTAL DIVISION [ ",imp1.nomdiv CLIPPED," ]",s,s,
           COLUMN  65,GROUP SUM(imp1.cantid)    USING "###,###,##&.&&",2 SPACES,s,s,
           COLUMN  98,GROUP SUM(imp1.totrec)    USING "###,###,##&.&&",8 SPACES,s,
                      porcentaje                USING "##&.&&","%" 
    ELSE
     PRINT COLUMN   1,"TOTAL DIVISION [ ",imp1.nomdiv CLIPPED," ]",s,s,
           COLUMN  65,GROUP SUM(imp1.cantid)    USING "###,###,##&.&&",s,s,
           COLUMN  98,GROUP SUM(imp1.totrec)    USING "###,###,##&.&&",s,
                      porcentaje                USING "##&.&&"
    END IF 
    PRINT s

   ON LAST ROW
    -- Totalizando
    IF (pipeline!="excel") THEN 
     PRINT COLUMN   1,"TOTAL VENTA -->",s,s,s,s,
           COLUMN  98,SUM(imp1.totrec)          USING "###,###,##&.&&",8 SPACES,s,
                      totalporcen               USING "##&.&&","%" 
    ELSE 
     PRINT COLUMN   1,"TOTAL VENTA -->",s,s,s,s,
           COLUMN  98,SUM(imp1.totrec)          USING "###,###,##&.&&",s,
                      totalporcen               USING "##&.&&" 
    END IF 
END REPORT
