drop view vis_estadisticaventas;

create view vis_estadisticaventas
(numpos, codemp, nserie, numdoc, fecemi, feccor,
 coddiv, nomdiv, lnkrec, nomrec, numrec, cantid,
 prevta, totrec, totisv, totdes, norden)
as
select a.numpos,
       a.codemp,
       a.nserie,
       a.numdoc,
       a.fecemi,
       a.feccor,
       c.coddiv,
       e.nomdiv,
       b.lnkrec,
       c.nomrec,
       b.numrec,
       b.cantid,
       b.prevta,
       b.totrec,
       b.totisv,
       b.totdes,
       e.norden 
from   pos_mtransac a,pos_dtransac b,sre_mrecetas c,sre_divrecet e
where  a.lnktra = b.lnktra
  and  a.estado = 1
  and  a.haycor = 1  
  and  c.lnkrec = b.lnkrec
  and  e.coddiv = c.coddiv;

grant select on vis_estadisticaventas to public;
