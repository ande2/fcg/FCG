
update statistics high for table inv_mtransac (lnktra,estado,tipope)
resolution 0.50000;

{
create view "sistemas".vis_integramovtos (codemp,codsuc,codbod,nomemp,fecemi,codgru,nomgru,codcat,cditem,codabr,dsitem,nomuni,canuni) as
  select x0.codemp ,x0.codsuc ,x0.codbod ,x3.nomemp ,x0.fecemi
    ,x5.codgru ,x2.nomgru ,x4.codcat ,x1.cditem ,x1.codabr ,x4.despro
    ,x6.nomabr ,x1.canuni from "sistemas".inv_mtransac x0 ,"sistemas"
    .inv_dtransac x1 ,"sistemas".inv_grupomov x2 ,"sistemas".glb_empresas
    x3 ,"sistemas".inv_products x4 ,"sistemas".inv_tipomovs x5
    ,"sistemas".inv_unimedid x6 where ((((((((x0.lnktra =  x1.lnktra
    ) AND (x0.estado = 'V' ) ) AND (x0.tipope >= 0 ) ) AND (x4.cditem
    = x1.cditem ) ) AND (x3.codemp = x0.codemp ) ) AND (x5.tipmov
    = x0.tipmov ) ) AND (x2.codgru = x5.codgru ) ) AND (x6.unimed
    = x4.unimed ) ) ;
} 
