
      SELECT UNIQUE b.codgru,b.nomgru,b.numord
     FROM  vis_productobodega a,inv_grupomov b
     WHERE a.codemp = 1
       AND a.codsuc = 1
       AND a.codbod = 1
       AND b.codgru = a.codgru
      ORDER BY b.numord
