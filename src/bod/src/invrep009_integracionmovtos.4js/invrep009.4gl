{ 
invrep009.4gl 
Mynor Ramirez 
Reporte integracion de movimientos
}

DATABASE storepos 

--  Definicion de variables globales 

CONSTANT progname = "invrep009" 
TYPE     datosreporte    RECORD 
          codemp         LIKE inv_mtransac.codemp, 
          nomemp         CHAR(30),
          codsuc         LIKE inv_mtransac.codbod,
          nomsuc         CHAR(30),
          codbod         LIKE inv_mtransac.codbod,
          nombod         CHAR(30),
          codcat         LIKE inv_products.codcat,
          nomcat         CHAR(30),
          cditem         LIKE inv_dtransac.cditem,
          codabr         CHAR(20), 
          dsitem         CHAR(40),
          nomuni         CHAR(4) 
         END RECORD
DEFINE   w_datos         RECORD
          codbod         LIKE inv_mtransac.codbod, 
          codemp         LIKE inv_mtransac.codemp,
          codsuc         LIKE inv_mtransac.codsuc,
          codcat         LIKE inv_products.codcat, 
          fecini         DATE,
          fecfin         DATE
         END RECORD
DEFINE   v_grupomovto    DYNAMIC ARRAY OF RECORD
          codgru         LIKE inv_tipomovs.codgru, 
          nomgru         CHAR(13) 
         END RECORD
DEFINE   w_mae_bod       RECORD LIKE inv_mbodegas.*
DEFINE   w_mae_emp       RECORD LIKE glb_empresas.*
DEFINE   w_mae_suc       RECORD LIKE glb_sucsxemp.*
DEFINE   xnomcat         VARCHAR(40) 
DEFINE   subtotcan       DEC(14,2)
DEFINE   totcantid       DEC(14,2)
DEFINE   existe          SMALLINT
DEFINE   totgru          SMALLINT
DEFINE   titulo1,titulo2 STRING
DEFINE   tituloreporte   STRING
DEFINE   filename        STRING
DEFINE   pipeline        STRING
DEFINE   strcodcat       STRING
DEFINE   s,d             CHAR(1)

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes1") 

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rintegramovtos")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep009_IntegracionMovimientos()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep009_IntegracionMovimientos()
 DEFINE imp1              datosreporte, 
        wpais             VARCHAR(255),
        qrytxt,hilera     STRING,
        loop              SMALLINT,
        w                 ui.Window 

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep009a AT 5,2
  WITH FORM "invrep009a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/IntegracionMovimientos.spl"

  -- Llenando combobox de bodegas x usuario
  CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
  -- Llenando combobox de empresas   
  CALL librut003_CbxEmpresas() 

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   LET s = 1 SPACES
   LET d = "-" 
   CLEAR FORM

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codemp,
                 w_datos.codsuc, 
                 w_datos.fecini, 
                 w_datos.fecfin 
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Verificando si filtros estan completos
     IF NOT invrep009_FiltrosCompletos() THEN
        NEXT FIELD codemp 
     END IF
     EXIT INPUT

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 

     -- Verificando si filtros estan completos
     IF NOT invrep009_FiltrosCompletos() THEN
        NEXT FIELD codemp 
     END IF
     EXIT INPUT

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline = "excel" 
     LET s        = ASCII(9) 
     LET d        = ASCII(9) 

     -- Verificando si filtros estan completos
     IF NOT invrep009_FiltrosCompletos() THEN
        NEXT FIELD codemp 
     END IF
     EXIT INPUT

    AFTER FIELD codemp  
     -- Verificando empresa 
     IF w_datos.codemp IS NULL THEN
        ERROR "Error: debe de seleccionarse la empresa a listar."
        NEXT FIELD codemp 
     END IF

    BEFORE FIELD codsuc 
     -- Cargando sucursales x empresa
     CALL librut003_CbxSucursales(w_datos.codemp) 

    AFTER FIELD codsuc  
     -- Verificando sucursal
     IF w_datos.codsuc IS NULL THEN
        ERROR "Error: debe de seleccionarse la sucursal a listar."
        NEXT FIELD codsuc 
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Obteniendo nombre de la categoria
   INITIALIZE xnomcat TO NULL
   IF w_datos.codcat IS NOT NULL THEN 
    SELECT a.nomcat 
     INTO  xnomcat
     FROM  glb_categors a
     WHERE a.codcat = w_datos.codcat
   END IF 

   -- Cargando grupo de tipos de movimiento
   CALL v_grupomovto.clear() 
   LET qrytxt = 
    "SELECT UNIQUE a.codgru,b.nomgru,b.numord ",
     "FROM  vis_integramovtos a,inv_grupomov b ",
     "WHERE a.codemp = ",w_datos.codemp,
     "  AND a.codsuc = ",w_datos.codsuc,
     "  AND a.codbod IS NOT NULL ",
     "  AND b.codgru = a.codgru ", 
     "  AND a.fecemi >= '",w_datos.fecini,"' ", 
     "  AND a.fecemi <= '",w_datos.fecfin,"' ", 
     strcodcat CLIPPED,
     " ORDER BY b.numord "

    display qrytxt 
    
    PREPARE p_grupomov FROM qrytxt 
    DECLARE c_grupomov CURSOR FOR p_grupomov 
    LET totgru  = 1
    LET titulo1 = NULL
    LET titulo2 = NULL
    FOREACH c_grupomov INTO v_grupomovto[totgru].*
     -- Creando encabezados
     LET hilera = librut001_JustificaDerecha(v_grupomovto[totgru].nomgru,13)
     LET v_grupomovto[totgru].nomgru = hilera 
     LET titulo1 = titulo1.trimright(),v_grupomovto[totgru].nomgru,d
     LET totgru = totgru+1 
    END FOREACH
    CLOSE c_grupomov
    FREE  c_grupomov
    LET totgru = totgru-1

    -- Adicionando totales   
    LET titulo1 = titulo1.trimright(),"  SALDO FINAL",s
    LET titulo1 = librut001_replace(titulo1,"-"," ",100)

   -- Creando condicion de categoria de productos
   LET strcodcat = "AND a.codcat = "||w_datos.codcat 

   -- Construyendo seleccion 
   LET qrytxt = 
    "SELECT UNIQUE a.codemp,a.nomemp,a.codsuc,a.nomsuc,a.codbod,a.nombod,", 
                  "a.codcat,a.nomcat,a.cditem,a.codabr,a.dsitem,a.nomuni ",
           "FROM  vis_productobodega a ",
           "WHERE a.codemp = ",w_datos.codemp,
           "  AND a.codsuc = ",w_datos.codsuc,
           strcodcat CLIPPED,
           " ORDER BY 1,3,5,7,11" 

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep009 FROM qrytxt 
   DECLARE c_crep009 SCROLL CURSOR WITH HOLD FOR c_rep009
   LET existe = FALSE
   FOREACH c_crep009 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE
       START REPORT invrep009_GeneraReporte TO filename
    END IF 

    -- Llenando el reporte
    OUTPUT TO REPORT invrep009_GeneraReporte(imp1.*)
   END FOREACH
   CLOSE c_crep009 
   FREE  c_crep009 

   IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT invrep009_GeneraReporte   

    -- Transfiriendo reporte a excel 
    IF pipeline = "excel" THEN 
      CALL librut005_excel(filename) 
    ELSE 
      -- Enviando reporte al destino seleccionado
      CALL librut001_sendreport
      (filename,pipeline,tituloreporte,
      "--noline-numbers "||
      "--nofooter "||
      "--font-size 8 "||
      "--page-width 842 --page-height 595 "||
      "--left-margin 55 --right-margin 25 "||
      "--top-margin 35 --bottom-margin 45 "||
      "--title Inventarios")
    END IF 

    ERROR "" 
    CALL fgl_winmessage(
    " Atencion","Reporte Emitido ...","information") 
   ELSE
    ERROR "" 
    CALL fgl_winmessage(
    " Atencion","No existen datos con el filtro seleccionado.","stop")
   END IF 
  END WHILE
  OPTIONS INPUT NO WRAP 
 CLOSE WINDOW wrep009a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION invrep009_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE
 IF w_datos.codemp IS NULL OR
    w_datos.fecini IS NULL OR
    w_datos.fecfin IS NULL THEN
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT invrep009_GeneraReporte(imp1)
 DEFINE imp1      datosreporte,
        xtipope   LIKE inv_grupomov.tipope,
        linea     VARCHAR(500),
        xcanant   DEC(14,2),
        xvalant   DEC(14,2),
        xcantid   DEC(14,2),
        hilera    CHAR(14),
        col,i,lg  SMALLINT,
        lfn       SMALLINT,
        qrytxt    STRING,
        periodo   STRING 

  OUTPUT LEFT MARGIN 0
         PAGE LENGTH 50 
         TOP MARGIN 0 
         BOTTOM MARGIN 0 

  FORMAT 
   PAGE HEADER
    -- Definiendo linea 
    LET hilera = "______________"
    LET linea  = "___________________________________________________________________"
    LET lg     = 14+(67+(totgru*14)+14)
    LET lfn    = (lg-20) 

    -- Creando linea 
    FOR i = 1 TO totgru+2 
     LET linea = linea,hilera CLIPPED 
    END FOR

    -- Periodo de fechas
    LET periodo = "PERIODO DEL ",w_datos.fecini," AL ",w_datos.fecfin

    -- Imprimiendo Encabezado
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Inventarios",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
	  COLUMN lfn,PAGENO USING "Pagina: <<<<"

    LET col = librut001_centrado(periodo,lg) 
    PRINT COLUMN   1,"Invrep009",
          COLUMN col,periodo CLIPPED, 
          COLUMN lfn,"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET col = librut001_centrado(xnomcat,lg) 
    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,xnomcat CLIPPED, 
          COLUMN lfn,"Hora  : ",TIME 
    PRINT linea 
   
    -- Verificando si tipo de reporte no es a EXCEL
    IF (pipeline!="excel") THEN 
       PRINT "Producto            ",s,"Descripcion                             ",s,"U/M ",
             s," SALDO INICIAL",
             s,titulo1.trimright()
    ELSE
       SKIP 1 LINES 
    END IF 

    PRINT linea

   BEFORE GROUP OF imp1.codemp
    -- Verificando si tipo de reporte es a EXCEL
    IF (pipeline="excel") THEN 
       -- Imprimiendo datos de la bodega
       PRINT UPSHIFT(tituloreporte)          CLIPPED,s
       PRINT "PERIODO DEL ",w_datos.fecini," AL ",w_datos.fecfin,s 
       PRINT "EMPRESA:",s,imp1.nomemp        CLIPPED,s
       PRINT "SUCURSAL:",s,w_mae_suc.nomsuc  CLIPPED,s
       PRINT "BODEGA:",s,imp1.nombod         CLIPPED,s 
       PRINT s
       PRINT "Producto",s,"Descripcion",s,"U/M",s,"Saldo Anterior",s,titulo1.trimright()
       PRINT s 
    ELSE 
       -- Imprimiendo datos de la bodega
       PRINT w_mae_emp.nomemp         CLIPPED
       PRINT "SUCURSAL [",imp1.codsuc USING "<<<<","] ",
             w_mae_suc.nomsuc         CLIPPED
    END IF 


   BEFORE GROUP OF imp1.codbod 
    PRINT s 
    PRINT "BODEGA:",s,imp1.nombod CLIPPED,s 

   BEFORE GROUP OF imp1.codcat
    PRINT s
    PRINT "Categoria:",s,imp1.nomcat CLIPPED   
    PRINT s 

   ON EVERY ROW
    -- Calculando saldo anterior del producto
    CALL librut003_saldoantxpro(imp1.codemp,
                                imp1.codsuc,
                                imp1.codbod, 
                                imp1.cditem,
                                w_datos.fecini)
    RETURNING xcanant,xvalant                         

    -- Imprimiendo pesaje 
    PRINT imp1.codabr,s,imp1.dsitem,s,imp1.nomuni,s,xcanant USING "---,---,--&.&&",s;

    -- Imprimiendo detalle x destino 
    LET subtotcan = xcanant 
    FOR i = 1 TO totgru
     -- Obteniendo tipo de operacion del grupo
     SELECT a.tipope
      INTO  xtipope
      FROM  inv_grupomov a
      WHERE a.codgru = v_grupomovto[i].codgru 

     -- Totalizando por empresa, fecha, grupo y producto 
     LET qrytxt = "SELECT NVL(SUM(a.canuni),0) ",
                   "FROM  vis_integramovtos a ",
                   "WHERE a.codemp = ",imp1.codemp,
                    " AND a.codsuc = ",imp1.codsuc,
                    " AND a.codbod = ",imp1.codbod,
                    " AND a.fecemi >= '",w_datos.fecini,"' ", 
                    " AND a.fecemi <= '",w_datos.fecfin,"' ", 
                    " AND a.codgru = ",v_grupomovto[i].codgru,
                    " AND a.cditem = ",imp1.cditem,
                    strcodcat CLIPPED

     PREPARE cmovtos FROM qrytxt 
     DECLARE cintmov CURSOR FOR cmovtos
     FOREACH cintmov INTO xcantid 
     END FOREACH
     
     -- Imprimiendo 
     PRINT xcantid USING "--,---,--&.&&",s;

     -- Calculando saldo 
     CASE (xtipope) 
      WHEN 1 LET subtotcan = (subtotcan+xcantid) -- CARGO
      WHEN 0 LET subtotcan = (subtotcan-xcantid) -- ABONO 
     END CASE 
    END FOR 

    -- Imprimiendo totales x fecha 
    PRINT subtotcan USING "--,---,--&.&&",s

   AFTER GROUP OF imp1.codcat  
    -- Totalizando por categoria 
    PRINT s 
    PRINT "Total Categoria:",s,GROUP COUNT(*) USING "<<<<<<<<<<"," Producto[s]",s

   AFTER GROUP OF imp1.codbod 
    PRINT s

   AFTER GROUP OF imp1.codsuc 
    -- Totalizando por sucursal 
    -- PRINT "Total Reporte:",s,GROUP COUNT(*) USING "<<<<<<<<<<",s
END REPORT 
