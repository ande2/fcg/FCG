
DBSCHEMA Schema Utility       INFORMIX-SQL Version 12.10.FC6WE







CREATE FUNCTION  "sistemas".fxsaldofisico(wcodemp SMALLINT,wcodsuc SMALLINT,wcodbod SMALLINT,waniotr SMALLINT,
                               wmestra SMALLINT,wcditem VARCHAR(20))
 RETURNING DEC(12,2)

 DEFINE spcanuni LIKE inv_saldopro.canuni;

 -- Funcion para obtener el saldo de inventario fisico de un producto
 -- Mynor Ramirez
 -- Diciembre 2010

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;
 LET  spcanuni = 0; 

 -- Obteniendo inventario fisico del producto
 IF spcanuni IS NULL THEN LET spcanuni = 0; END IF;

 RETURN spcanuni;
END FUNCTION;

CREATE FUNCTION  "sistemas".fxsaldomovimientos(wcodemp SMALLINT,wcodsuc SMALLINT,wcodbod SMALLINT,
                                    waniotr SMALLINT,wmestra SMALLINT,wcditem VARCHAR(20))
 RETURNING DEC(16,2),DEC(14,6),DEC(16,2),DATE,DATE

 DEFINE spcanuni LIKE inv_proenbod.exican;
 DEFINE spcospro LIKE inv_proenbod.cospro;
 DEFINE sptotpro LIKE inv_proenbod.exival;
 DEFINE spopeuni LIKE inv_proenbod.exican;
 DEFINE spopeval LIKE inv_proenbod.exival;
 DEFINE xcanuni  LIKE inv_proenbod.exican;
 DEFINE xcospro  LIKE inv_proenbod.cospro;
 DEFINE xtotpro  LIKE inv_proenbod.exival;
 DEFINE xfulent  LIKE inv_proenbod.fulent;
 DEFINE xfulsal  LIKE inv_proenbod.fulsal;
 DEFINE aniant   SMALLINT;
 DEFINE mesant   SMALLINT;
 DEFINE conteo   INTEGER;

 -- Funcion para calcular el saldo de un producto por sus movimientos
 -- Los saldos son por empresa, sucursal, bodega y producto
 -- Mynor Ramirez
 -- Diciembre 2010

 -- Definiendo nivel de aislamiento
 SET ISOLATION COMMITTED READ;

 -- Calculando saldo de movimientos
 SELECT NVL(SUM(a.opeuni),0),
        NVL(SUM(a.opeval),0)
  INTO  spopeuni,
        spopeval
  FROM  inv_dtransac a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.cditem = wcditem
    AND a.tipope >= 0 
    AND a.estado = "V" 
    AND a.actexi = 1;

 -- Calculando fecha de ultima entrada
 SELECT MAX(a.fecemi)
  INTO  xfulent
  FROM  inv_dtransac a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.cditem = wcditem
    AND a.tipope = 1
    AND a.estado = "V"
    AND a.actexi = 1;

 -- Calculando fecha de ultima salida
 SELECT MAX(a.fecemi)
  INTO  xfulsal
  FROM  inv_dtransac a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.cditem = wcditem
    AND a.tipope = 0
    AND a.estado = "V"
    AND a.actexi = 1;

 -- Asignando saldos
 LET xcanuni = (spopeuni);
 LET xtotpro = (spopeval);

 -- Calculando costo promedio
 LET xcospro = 0;
 IF (xcanuni>0) THEN
    LET xcospro = (xtotpro/xcanuni);
 END IF

 -- Verificando nulos
 IF xcanuni IS NULL THEN LET xcanuni = 0; END IF;
 IF xcospro IS NULL THEN LET xcospro = 0; END IF;
 IF xtotpro IS NULL THEN LET xtotpro = 0; END IF;

 RETURN xcanuni,xcospro,xtotpro,xfulent,xfulsal;
END FUNCTION;


CREATE PROCEDURE "sistemas".sptempaquedefault(wcditem INT)
 -- Procedimiento para crear el empaque default de un producto despues de grabarlo en la tabla de empaques por producto
 -- Mynor Ramirez
 -- Diciembre 2010

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Borrando empaque antes de grabar
 SET LOCK MODE TO WAIT;
 DELETE FROM inv_epqsxpro
 WHERE inv_epqsxpro.cditem = wcditem;

 -- Creando empaque default
 INSERT INTO inv_epqsxpro
 SELECT 0,a.cditem,0,b.nommed,1,USER,CURRENT,CURRENT HOUR TO SECOND
  FROM  inv_products a,inv_unimedid b
  WHERE b.unimed = a.unimed
    AND a.cditem = wcditem;

END PROCEDURE;

CREATE PROCEDURE "sistemas".sptcrearaccesosxusuario(xroleid INT,xuserid VARCHAR(15))
 -- Procedimiento para crear los accesos x usuario de acuerdo al perfil
 -- Mynor Ramirez
 -- Octubre 2011

 DEFINE splnkpro LIKE glb_permxrol.lnkpro;
 DEFINE spcodpro LIKE glb_permxrol.codpro;
 DEFINE spprogid LIKE glb_permxrol.progid;
 DEFINE conteo   INT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Insertando los accesos por perfil que no existan
 FOREACH SELECT a.lnkpro,
                a.codpro,
                a.progid
          INTO  splnkpro,
                spcodpro,
                spprogid
          FROM  glb_permxrol a
          WHERE a.roleid = xroleid
            AND a.progid >0
          ORDER BY a.lnkpro

    -- Verificando si permiso ya existe
    SELECT COUNT(*)
     INTO  conteo
     FROM  glb_permxusr a
     WHERE a.roleid = xroleid
       AND a.lnkpro = splnkpro
       AND a.userid = xuserid;

    -- Si existe no lo grabar para mantener datos actualzados desde el usuario
    IF (conteo>0) THEN
       CONTINUE FOREACH;
    END IF;

    -- Insertando accesos
    SET LOCK MODE TO WAIT;
    INSERT INTO glb_permxusr
    VALUES (0,                           -- id unico del permiso
            xroleid,                     -- id del perfil
            splnkpro,                    -- id del acceso
            xuserid,                     -- usuario del perfil
            spcodpro,                    -- codigo de la opcion
            spprogid,                    -- id de la opcion
            1,                           -- marcando perfil de activo
            NULL,                        -- sin password
            NULL,                        -- sin fecha de inicio
            NULL,                        -- sin fecha de finalizacion
            USER,                        -- usuario registro
            CURRENT,                     -- fecha de registro
            CURRENT HOUR TO SECOND);     -- ghora de registro
 END FOREACH;

 -- Elimina todos aquellos permisos que no son del usuario por si fue cambio de perfil
 SET LOCK MODE TO WAIT;
 DELETE FROM glb_permxusr
 WHERE glb_permxusr.roleid != xroleid
   AND glb_permxusr.userid  = xuserid;
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptcreaprogramabase(xcodpro VARCHAR(15),xnompro VARCHAR(50),
                                                xordpro SMALLINT)
 -- Procedimiento para crear el programa base o raiz en la tabla de detalle
 -- de programas. Este programa es el nodo raiz para los accesos por programa
 -- Mynor Ramirez
 -- Octubre 2011

 DEFINE xcodopc LIKE glb_dprogram.codopc;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Creando codigo de la opcion
 SELECT TRIM(a.codpro)||"-0"
  INTO  xcodopc
  FROM  glb_programs a
  WHERE a.codpro = xcodpro;

 -- Insertando programa
 SET LOCK MODE TO WAIT;
 INSERT INTO glb_dprogram
 VALUES (0,                         -- id unico de la opcion
         xcodpro,                   -- codigo del programa padre
         0,                         -- id unico de la opcion por programa padre
         xcodopc,                   -- codigo de la opcion (codigo programa-id opcion)
         xnompro,                   -- nombre de la opcion
         xordpro,                   -- orden de la opcion
         0,                         -- opcion no requerie password
         USER,                      -- usuario registro la opcion
         CURRENT,                   -- fecha de registro de la opcion
         CURRENT HOUR TO SECOND);   -- hora de registro de la opcion
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptsaldosxproducto(wcodemp SMALLINT,wcodsuc SMALLINT,wcodbod SMALLINT,
                                    wtipmov SMALLINT,waniotr SMALLINT,wmestra SMALLINT,
                                    wcditem INT,wtipope SMALLINT,wcodabr VARCHAR(20),wpreuni DEC(14,6))

 -- Procedimiento para calcular y registrar el saldo x producto
 -- Los saldos son por empresa, sucursal, bodega, y producto
 -- Mynor Ramirez
 -- Diciembre 2010

 DEFINE xcanuni  LIKE inv_proenbod.exican;
 DEFINE xcospro  LIKE inv_proenbod.cospro;
 DEFINE xtotpro  LIKE inv_proenbod.exival;
 DEFINE xcodemp  LIKE inv_proenbod.codemp;
 DEFINE xfulent  LIKE inv_proenbod.fulent;
 DEFINE xfulsal  LIKE inv_proenbod.fulsal;
 DEFINE whayval  LIKE inv_tipomovs.hayval;

-- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Calculando saldo en base a movimientos
 CALL fxsaldomovimientos(wcodemp,wcodsuc,wcodbod,waniotr,wmestra,wcditem)
 RETURNING xcanuni,xcospro,xtotpro,xfulent,xfulsal;

 -- Verificando si producto ya existe en saldos x bodega
 LET xcodemp = NULL;
 SELECT UNIQUE a.codemp
  INTO  xcodemp
  FROM  inv_proenbod a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.cditem = wcditem;

 -- Si ya existe el saldo del producto lo actualiza, sino lo inserta
 IF xcodemp IS NOT NULL THEN
    -- Actualizando
    SET LOCK MODE TO WAIT;
    UPDATE inv_proenbod
    SET    inv_proenbod.fulent = xfulent,
           inv_proenbod.fulsal = xfulsal,
           inv_proenbod.exican = xcanuni,
           inv_proenbod.exival = xtotpro,
           inv_proenbod.cospro = xcospro
    WHERE  inv_proenbod.codemp = wcodemp
      AND  inv_proenbod.codsuc = wcodsuc
      AND  inv_proenbod.codbod = wcodbod
      AND  inv_proenbod.cditem = wcditem;
 ELSE
    -- Insertando
    SET LOCK MODE TO WAIT;
    INSERT INTO inv_proenbod
    VALUES (wcodemp,
            wcodsuc,
            wcodbod,
            wcditem,
            wcodabr,
            0,
            0,
            xfulent,
            xfulsal,
            xcanuni,
            xtotpro,
            xcospro,
            USER,
            CURRENT,
            CURRENT HOUR TO SECOND);
 END IF

 -- Actualizando existencia total del producto tomando en cuenta todas las
 -- bodegas
 -- Totalizando existencia
 SELECT NVL(SUM(a.exican),0)
  INTO  xcanuni
  FROM  inv_proenbod a
  WHERE (a.cditem = wcditem);

 -- Actualizando ultimo precio de compra si el tipo de movimiento esta parametrizado
 LET whayval = 0;
 SELECT NVL(a.hayval,0)
  INTO  whayval
  FROM  inv_tipomovs a
  WHERE a.tipmov = wtipmov;

 -- Verificando si tipo de movimiento actualiza valores
 IF (whayval=1) THEN
    -- Actualizando producto
    SET LOCK MODE TO WAIT;
    UPDATE inv_products
    SET    inv_products.pulcom = wpreuni,
           inv_products.exiact = xcanuni
    WHERE  inv_products.cditem = wcditem;
 ELSE
    -- Actualizando existencia
    SET LOCK MODE TO WAIT;
    UPDATE inv_products
    SET    inv_products.exiact = xcanuni
    WHERE  inv_products.cditem = wcditem;
 END IF
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptingresosxpeso(wcodemp SMALLINT,
                                             wcodsuc SMALLINT,
                                             wcodbod SMALLINT,
                                             wfecemi DATE,
                                             wtipope SMALLINT,
                                             wcodori SMALLINT,
                                             wnomori VARCHAR(100),
                                             wcditem INT,
                                             wcodabr VARCHAR(20),
                                             wnumetq VARCHAR(20),
                                             wcanuni DEC(12,2),
                                             wcxpeso DEC(12,2))

 -- Procedimiento para insertar un movimiento de ingreso x peso automaticamente
 -- Mynor Ramirez
 -- Agosto 2012
 -- Si la cantidad x peso (wcxpeso=0) toma wcanuni=cantidad peso, sino toma
 -- wcxpeso=cantidad unidades 

 DEFINE wtipmov  LIKE inv_mtransac.tipmov;
 DEFINE waniotr  LIKE inv_mtransac.aniotr;
 DEFINE wmestra  LIKE inv_mtransac.mestra;
 DEFINE wobserv  LIKE inv_mtransac.observ;
 DEFINE westado  LIKE inv_mtransac.estado;
 DEFINE wchkinv  LIKE fac_puntovta.chkinv;
 DEFINE wlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xcodabr  LIKE inv_dtransac.codabr;
 DEFINE xnserie  LIKE inv_dtransac.nserie;
 DEFINE xcodepq  LIKE inv_dtransac.codepq;
 DEFINE xcanepq  LIKE inv_dtransac.canepq;
 DEFINE xpreuni  LIKE inv_dtransac.preuni;
 DEFINE xtotpro  LIKE inv_dtransac.totpro;
 DEFINE xcorrel  LIKE inv_dtransac.correl;
 DEFINE xopeval  LIKE inv_dtransac.opeval;
 DEFINE xopeuni  LIKE inv_dtransac.opeuni;
 DEFINE xtipmov  LIKE inv_dtransac.tipmov;
 DEFINE xnumdoc  LIKE inv_mtransac.numrf1;
 DEFINE xtipope  LIKE inv_dtransac.tipope;
 DEFINE xcanuni  DEC(14,6);
 DEFINE weximov  SMALLINT;
 DEFINE conteo   INT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Verificando si tipo de movimiento es cargo
 IF (wtipope=1) THEN

   -- Obteniendo tipo de movimiento default de ingresos por peso
   SELECT NVL(a.valchr,0)
    INTO  wtipmov
    FROM  glb_paramtrs a
    WHERE a.numpar = 13
      AND a.tippar = 8;

   -- Seleccionando tipo de movimiento
   SELECT NVL(a.tipmov,0),NVL(a.tipope,0)
    INTO  xtipmov,xtipope
    FROM  inv_tipomovs a
    WHERE a.tipmov = wtipmov;

   -- Verificando si existen datos
   IF (xtipmov>0) THEN

    -- Verificando si existe un movimiento de ingreso por peso de la misma fecha
    SELECT UNIQUE a.lnktra
     INTO  xlnktra
     FROM  inv_mtransac a
     WHERE a.codemp = wcodemp
       AND a.codsuc = wcodsuc
       AND a.codbod = wcodbod
       AND a.tipmov = xtipmov
       AND a.fecemi = wfecemi;

    IF (xlnktra=0) OR xlnktra IS NULL THEN
     -- Creando encabezado
     -- Asignando datos
     LET wlnktra = 0;
     LET xnumdoc = "";
     LET wobserv = "INGRESOS POR PESO";
     LET wmestra = MONTH(wfecemi);
     LET waniotr = YEAR(wfecemi);
     LET xcorrel = 1;
     LET westado = "V";
     LET xnserie = "";
     LET xpreuni = 0;

     -- Insertando encabezado
     SET LOCK MODE TO WAIT;
     INSERT INTO inv_mtransac
     VALUES (0                     ,  -- Id unico
             wcodemp               ,  -- Empresa
             wcodsuc               ,  -- Sucursal
             wcodbod               ,  -- Bodega
             xtipmov               ,  -- Tipo de movimiento
             wcodori               ,  -- Origen
             wnomori               ,  -- Nombre del origen
             0                     ,  -- Destino
             ""                    ,  -- Nombre del Destino
             wlnktra               ,  -- Numero id de transaccion que origino el consumo
             xnumdoc               ,  -- Numero de documento
             0                     ,  -- Movimiento es externo
             waniotr               ,  -- Anio
             wmestra               ,  -- Mes
             wfecemi               ,  -- Fecha de emision
             xtipope               ,  -- Tipo de operacion del movimiento
             wobserv               ,  -- Observaciones
             westado               ,  -- Estado V=igente
             wlnktra               ,  -- Numero id de transaccion que origino el consumo
             0                     ,  -- Consumo automatico
             NULL                  ,  -- Motivo de la anulacion
             NULL                  ,  -- Fecha de anulacion
             NULL                  ,  -- Hora de la anulacion
             NULL                  ,  -- Usuario anulo
             "S"                   ,  -- Documento impreso
             USER                  ,  -- Usuario registro
             CURRENT               ,  -- Fecha de registro
             CURRENT HOUR TO SECOND); -- Hora de registro

     -- Obteniendo serial de la tabla encabezado
     SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
      INTO  xlnktra
      FROM  inv_mtransac;
    ELSE
     -- Asignando datos
     LET wmestra = MONTH(wfecemi);
     LET waniotr = YEAR(wfecemi);
     LET westado = "V";
     LET xnserie = "";
     LET xpreuni = 0;

     -- Obteniendo correlativo de producto de la transaccion
     SELECT (NVL(MAX(a.correl),0)+1)
      INTO  xcorrel
      FROM  inv_dtransac a
      WHERE a.lnktra = xlnktra;
    END IF;

    --- Creando detalle
    -- Calculando cantidad y valor para calculo de saldo

    -- Si cantidad x peso (wcxpeso=0) se toma peso en libras que viene en (wcanuni),
    -- de lo contrario se toma la cantidad en unidades que viene en (wcxpeso) 
    IF (wcxpeso=0) THEN 
       LET xcanuni = wcanuni;
    ELSE
       LET xcanuni = wcxpeso;
    END IF;

    LET xcanepq = xcanuni;
    LET xtotpro = 0;
    LET xopeuni = xcanuni;
    LET xopeval = xtotpro;

    -- Insertando detalle
    SET LOCK MODE TO WAIT;
    INSERT INTO inv_dtransac
    VALUES (xlnktra               ,  -- id transaccion
            wcodemp               ,  -- empresa
            wcodsuc               ,  -- sucursal
            wcodbod               ,  -- bodega
            xtipmov               ,  -- tipo de movimiento
            wcodori               ,  -- origen
            0                     ,  -- destino
            wfecemi               ,  -- fecha de emision
            waniotr               ,  -- anio
            wmestra               ,  -- mes
            wcditem               ,  -- codigo de producto
            wcodabr               ,  -- codigo abreviado
            xnserie               ,  -- numero de serie
            0                     ,  -- codigo de empaque
            xcanepq               ,  -- cantidad de empaque
            xcanuni               ,  -- cantidad
            xpreuni               ,  -- precio unitario
            xtotpro               ,  -- total del producto
            0                     ,  -- precio promedio
            xcorrel               ,  -- correlativo
            wnumetq               ,  -- Numero de etiqueta
            xtipope               ,  -- tipo de operacion del movimniento
            westado               ,  -- estado
            xopeval               ,  -- total
            xopeuni               ,  -- cantidad
            1                     ,  -- Actualiza existencia automaticamente 
            USER                  ,  -- Usuario registro
            CURRENT               ,  -- Fecha de registro
            CURRENT HOUR TO SECOND); -- Hora de registro
  END IF;

 END IF;
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptrebajaconsumos(wlnktra INTEGER,
                                              wcodemp SMALLINT,
                                              wcodsuc SMALLINT,
                                              wcodbod SMALLINT,
                                              wfecemi DATE,
                                              wtipope SMALLINT)

 -- Procedimiento para insertar los consumos de producto automaticos en el inventario
 -- Mynor Ramirez
 -- Juio 2012

 DEFINE wtipmov  LIKE inv_mtransac.tipmov;
 DEFINE waniotr  LIKE inv_mtransac.aniotr;
 DEFINE wmestra  LIKE inv_mtransac.mestra;
 DEFINE wobserv  LIKE inv_mtransac.observ;
 DEFINE westado  LIKE inv_mtransac.estado;
 DEFINE wchkinv  LIKE fac_puntovta.chkinv;
 DEFINE xlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xcodabr  LIKE inv_dtransac.codabr;
 DEFINE xnserie  LIKE inv_dtransac.nserie;
 DEFINE xcodepq  LIKE inv_dtransac.codepq;
 DEFINE xcanepq  LIKE inv_dtransac.canepq;
 DEFINE xpreuni  LIKE inv_dtransac.preuni;
 DEFINE xtotpro  LIKE inv_dtransac.totpro;
 DEFINE xcorrel  LIKE inv_dtransac.correl;
 DEFINE xbarcod  LIKE inv_dtransac.barcod;
 DEFINE xopeval  LIKE inv_dtransac.opeval;
 DEFINE xopeuni  LIKE inv_dtransac.opeuni;
 DEFINE xtipmov  LIKE inv_dtransac.tipmov;
 DEFINE xnumdoc  LIKE inv_mtransac.numrf1;
 DEFINE xtipope  LIKE inv_dtransac.tipope;
 DEFINE xcditem  LIKE inv_dtransac.cditem;
 DEFINE xcoddes  LIKE inv_mtransac.coddes;
 DEFINE xnomdes  LIKE inv_mtransac.nomdes;
 DEFINE wcditem  LIKE inv_dtransac.cditem;
 DEFINE wcanuni  LIKE inv_dtransac.canuni;
 DEFINE xcanuni  DEC(14,6);
 DEFINE weximov  SMALLINT;
 DEFINE conteo   INT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Verificando si tipo de movimiento es cargo 
 IF (wtipope=1) THEN

   -- Eliminando movimiento de consumo
   SET LOCK MODE TO WAIT;
   DELETE FROM inv_mtransac
   WHERE inv_mtransac.lnkcon = wlnktra;

   -- Obteniendo tipo de movimiento default de consumos automaticos
   SELECT NVL(a.valchr,0)
    INTO  wtipmov
    FROM  glb_paramtrs a
    WHERE a.numpar = 13
      AND a.tippar = 2;

   -- Seleccionando tipo de movimiento
   SELECT NVL(a.tipmov,0),NVL(a.tipope,0)
    INTO  xtipmov,xtipope
    FROM  inv_tipomovs a
    WHERE a.tipmov = wtipmov;

   -- Verificando si existen datos
   IF (xtipmov>0) THEN

    -- Verificando si movimiento tiene productos que contengan subproductos
    SELECT COUNT(*)
     INTO  conteo
     FROM  inv_dtransac d ,inv_dproduct s
     WHERE d.lnktra = wlnktra
       AND s.cditem = d.cditem
       AND s.cantid >0;

    IF (conteo>0) THEN
     -- Creando encabezado
     -- Seleccionando datos
     SELECT NVL(a.numrf1,"0"),NVL(a.coddes,0),a.nomdes
      INTO  xnumdoc,xcoddes,xnomdes
      FROM  inv_mtransac a
      WHERE a.lnktra = wlnktra;

     -- Asignando datos
     LET wmestra = MONTH(wfecemi);
     LET waniotr = YEAR(wfecemi);
     LET wobserv = "CONSUMO AUTOMATICO";
     LET xbarcod = "";
     LET westado = "V";
     LET xnserie = "";
     LET xpreuni = 0;

     -- Seleccionando datos
     SET LOCK MODE TO WAIT;
     INSERT INTO inv_mtransac
     VALUES (0                     ,  -- Id unico
             wcodemp               ,  -- Empresa
             wcodsuc               ,  -- Sucursal
             wcodbod               ,  -- Bodega
             xtipmov               ,  -- Tipo de movimiento
             0                     ,  -- Origen
             ""                    ,  -- Nombre del origen
             xcoddes               ,  -- Destino
             xnomdes               ,  -- Nombre del Destino
             wlnktra               ,  -- ID Movimiento
             xnumdoc               ,  -- Numero de documento
             0                     ,  -- Movimiento es externo
             waniotr               ,  -- Anio
             wmestra               ,  -- Mes
             wfecemi               ,  -- Fecha de emision
             xtipope               ,  -- Tipo de operacion del movimiento
             wobserv               ,  -- Observaciones
             westado               ,  -- Estado V=igente
             wlnktra               ,  -- Numero id de transaccion que origino el consumo
             0                     ,  -- Consumo automatico
             NULL                  ,  -- Motivo de la anulacion
             NULL                  ,  -- Fecha de anulacion
             NULL                  ,  -- Hora de la anulacion
             NULL                  ,  -- Usuario anulo
             "S"                   ,  -- Documento impreso
             USER                  ,  -- Usuario registro
             CURRENT               ,  -- Fecha de registro
             CURRENT HOUR TO SECOND); -- Hora de registro

     -- Obteniendo serial de la tabla encabezado
     SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
      INTO  xlnktra
      FROM  inv_mtransac;

    -- Seleccionando productos que tiene subproductos
    -- Verificando si movimiento tiene productos que contengan subproductos
    FOREACH SELECT a.cditem,
                   a.canuni,
                   b.citems,
                   b.codabr,
                   b.cantid
             INTO  wcditem,
                   wcanuni,
                   xcditem,
                   xcodabr,
                   xcanuni
             FROM  inv_dtransac a,inv_dproduct b
             WHERE a.lnktra = wlnktra
               AND b.cditem = a.cditem
               AND b.cantid >0

      -- Obteniendo correlativo de producto de la transaccion
      SELECT (NVL(MAX(a.correl),0)+1)
       INTO  xcorrel
       FROM  inv_dtransac a
       WHERE a.lnktra = xlnktra;

      -- Calculando cantidad y valor para calculo de saldo
      LET xcanuni = (xcanuni*wcanuni);
      LET xcanepq = xcanuni;
      LET xtotpro = 0;

      IF (xtipope=0) THEN
         LET xopeuni = (xcanuni*(-1));
         LET xopeval = (xtotpro*(-1));
      ELSE
         LET xopeuni = xcanuni;
         LET xopeval = xtotpro;
      END IF;

      -- Insertando detalle
      SET LOCK MODE TO WAIT;
      INSERT INTO inv_dtransac
      VALUES (xlnktra               ,  -- id transaccion
              wcodemp               ,  -- empresa
              wcodsuc               ,  -- sucursal
              wcodbod               ,  -- bodega
              xtipmov               ,  -- tipo de movimiento
              0                     ,  -- origen
              xcoddes               ,  -- destino
              wfecemi               ,  -- fecha de emision
              waniotr               ,  -- anio
              wmestra               ,  -- mes
              xcditem               ,  -- codigo de producto
              xcodabr               ,  -- codigo abreviado
              xnserie               ,  -- numero de serie
              0                     ,  -- codigo de empaque
              xcanepq               ,  -- cantidad de empaque
              xcanuni               ,  -- cantidad
              xpreuni               ,  -- precio unitario
              xtotpro               ,  -- total del producto
              0                     ,  -- precio promedio
              xcorrel               ,  -- correlativo
              xbarcod               ,  -- codigo de barra
              xtipope               ,  -- tipo de operacion del movimniento
              westado               ,  -- estado
              xopeval               ,  -- total
              xopeuni               ,  -- cantidad
              1                     ,  -- actualiza existencia automaticamente 
              USER                  ,  -- Usuario registro
              CURRENT               ,  -- Fecha de registro
              CURRENT HOUR TO SECOND); -- Hora de registro

    END FOREACH;

   END IF;

  END IF;

 END IF;
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptsalidasxpeso(wcodemp SMALLINT,
                                            wcodsuc SMALLINT,
                                            wcodbod SMALLINT,
                                            wfecemi DATE,
                                            wtipope SMALLINT,
                                            wcoddes SMALLINT,
                                            wnomdes VARCHAR(100),
                                            wcditem INT,
                                            wcodabr VARCHAR(20),
                                            wnumetq VARCHAR(20),
                                            wcanuni DEC(12,2))

 -- Procedimiento para insertar un movimiento de salida x peso automaticamente
 -- Mynor Ramirez
 -- Octubre 2012

 DEFINE wtipmov  LIKE inv_mtransac.tipmov;
 DEFINE waniotr  LIKE inv_mtransac.aniotr;
 DEFINE wmestra  LIKE inv_mtransac.mestra;
 DEFINE wobserv  LIKE inv_mtransac.observ;
 DEFINE westado  LIKE inv_mtransac.estado;
 DEFINE wchkinv  LIKE fac_puntovta.chkinv;
 DEFINE wlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xcodabr  LIKE inv_dtransac.codabr;
 DEFINE xnserie  LIKE inv_dtransac.nserie;
 DEFINE xcodepq  LIKE inv_dtransac.codepq;
 DEFINE xcanepq  LIKE inv_dtransac.canepq;
 DEFINE xpreuni  LIKE inv_dtransac.preuni;
 DEFINE xtotpro  LIKE inv_dtransac.totpro;
 DEFINE xcorrel  LIKE inv_dtransac.correl;
 DEFINE xopeval  LIKE inv_dtransac.opeval;
 DEFINE xopeuni  LIKE inv_dtransac.opeuni;
 DEFINE xtipmov  LIKE inv_dtransac.tipmov;
 DEFINE xnumdoc  LIKE inv_mtransac.numrf1;
 DEFINE xtipope  LIKE inv_dtransac.tipope;
 DEFINE xcanuni  DEC(14,6);
 DEFINE weximov  SMALLINT;
 DEFINE conteo   INT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Verificando si tipo de movimiento es abono
 IF (wtipope=0) AND 
    wcditem IS NOT NULL AND 
    wcodabr IS NOT NULL THEN 

   -- Obteniendo tipo de movimiento default de salidas por peso
   SELECT NVL(a.valchr,0)
    INTO  wtipmov
    FROM  glb_paramtrs a
    WHERE a.numpar = 13
      AND a.tippar = 9;

   -- Seleccionando tipo de movimiento
   SELECT NVL(a.tipmov,0),NVL(a.tipope,0)
    INTO  xtipmov,xtipope
    FROM  inv_tipomovs a
    WHERE a.tipmov = wtipmov;

   -- Verificando si existen datos
   IF (xtipmov>0) THEN

    -- Verificando si existe un movimiento de salida por peso de la misma fecha
    SELECT UNIQUE a.lnktra
     INTO  xlnktra
     FROM  inv_mtransac a
     WHERE a.codemp = wcodemp
       AND a.codsuc = wcodsuc
       AND a.codbod = wcodbod
       AND a.tipmov = xtipmov
       AND a.fecemi = wfecemi;

    IF (xlnktra=0) OR xlnktra IS NULL THEN
     -- Creando encabezado
     -- Asignando datos
     LET wlnktra = 0;
     LET xnumdoc = "";
     LET wobserv = "SALIDAS POR PESO";
     LET wmestra = MONTH(wfecemi);
     LET waniotr = YEAR(wfecemi);
     LET xcorrel = 1;
     LET westado = "V";
     LET xnserie = "";
     LET xpreuni = 0;

     -- Insertando encabezado
     SET LOCK MODE TO WAIT;
     INSERT INTO inv_mtransac
     VALUES (0                     ,  -- Id unico
             wcodemp               ,  -- Empresa
             wcodsuc               ,  -- Sucursal
             wcodbod               ,  -- Bodega
             xtipmov               ,  -- Tipo de movimiento
             0                     ,  -- Origen
             ""                    ,  -- Nombre del origen
             wcoddes               ,  -- Destino
             wnomdes               ,  -- Nombre del Destino
             wlnktra               ,  -- Numero id de transaccion que origino el consumo
             xnumdoc               ,  -- Numero de documento
             0                     ,  -- Movimiento es externo
             waniotr               ,  -- Anio
             wmestra               ,  -- Mes
             wfecemi               ,  -- Fecha de emision
             xtipope               ,  -- Tipo de operacion del movimiento
             wobserv               ,  -- Observaciones
             westado               ,  -- Estado V=igente
             wlnktra               ,  -- Numero id de transaccion que origino el consumo
             0                     ,  -- Consumo automatico
             NULL                  ,  -- Motivo de la anulacion
             NULL                  ,  -- Fecha de anulacion
             NULL                  ,  -- Hora de la anulacion
             NULL                  ,  -- Usuario anulo
             "S"                   ,  -- Documento impreso
             USER                  ,  -- Usuario registro
             CURRENT               ,  -- Fecha de registro
             CURRENT HOUR TO SECOND); -- Hora de registro

     -- Obteniendo serial de la tabla encabezado
     SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
      INTO  xlnktra
      FROM  inv_mtransac;
    ELSE
     -- Asignando datos
     LET wmestra = MONTH(wfecemi);
     LET waniotr = YEAR(wfecemi);
     LET westado = "V";
     LET xnserie = "";
     LET xpreuni = 0;

     -- Obteniendo correlativo de producto de la transaccion
     SELECT (NVL(MAX(a.correl),0)+1)
      INTO  xcorrel
      FROM  inv_dtransac a
      WHERE a.lnktra = xlnktra;
    END IF;

    --- Creando detalle
    -- Calculando cantidad y valor para calculo de saldo
    LET xcanuni = wcanuni;
    LET xcanepq = xcanuni;
    LET xtotpro = 0;
    LET xopeuni = (xcanuni*(-1));
    LET xopeval = (xtotpro*(-1));

    -- Insertando detalle
    SET LOCK MODE TO WAIT;
    INSERT INTO inv_dtransac
    VALUES (xlnktra               ,  -- id transaccion
            wcodemp               ,  -- empresa
            wcodsuc               ,  -- sucursal
            wcodbod               ,  -- bodega
            xtipmov               ,  -- tipo de movimiento
            0                     ,  -- origen
            wcoddes               ,  -- destino
            wfecemi               ,  -- fecha de emision
            waniotr               ,  -- anio
            wmestra               ,  -- mes
            wcditem               ,  -- codigo de producto
            wcodabr               ,  -- codigo abreviado
            xnserie               ,  -- numero de serie
            0                     ,  -- codigo de empaque
            xcanepq               ,  -- cantidad de empaque
            xcanuni               ,  -- cantidad
            xpreuni               ,  -- precio unitario
            xtotpro               ,  -- total del producto
            0                     ,  -- precio promedio
            xcorrel               ,  -- correlativo
            wnumetq               ,  -- Numero de etiqueta
            xtipope               ,  -- tipo de operacion del movimniento
            westado               ,  -- estado
            xopeval               ,  -- total
            xopeuni               ,  -- cantidad
            1                     ,  -- Actualiza existencia automaticamente
            USER                  ,  -- Usuario registro
            CURRENT               ,  -- Fecha de registro
            CURRENT HOUR TO SECOND); -- Hora de registro
  END IF;

 END IF;
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptsumafisico(wcodemp SMALLINT,wcodsuc SMALLINT,wcodbod SMALLINT,
                                          wfecinv DATE,wcditem INT,wcodabr CHAR(20))

 -- Procedimiento para totalizar lo registrado en el inventario fisico
 -- Mynor Ramirez
 -- Mayo 2010

 DEFINE xtotuni DEC(12,2);
 DEFINE wmestra LIKE inv_tofisico.mestra;
 DEFINE waniotr LIKE inv_tofisico.aniotr;
 DEFINE wlnktra LIKE inv_mtransac.lnktra;
 DEFINE wtipmov LIKE inv_mtransac.tipmov;
 DEFINE conteo  INT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;
 LET waniotr = YEAR(wfecinv);
 LET wmestra = MONTH(wfecinv);

 -- Totalizando conteo fisico
 SELECT NVL(SUM(a.canuni),0)
  INTO  xtotuni
  FROM  inv_scfisico a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.fecinv = wfecinv
    AND a.cditem = wcditem;

 -- Verificando si producto ya existe regstrado en inventario fisico
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_tofisico a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.fecinv = wfecinv
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem;
  IF (conteo>0) THEN
     -- Si ya existe borra el inventario
     SET LOCK MODE TO WAIT;
     DELETE FROM inv_tofisico
     WHERE inv_tofisico.codemp = wcodemp
       AND inv_tofisico.codsuc = wcodsuc
       AND inv_tofisico.codbod = wcodbod
       AND inv_tofisico.fecinv = wfecinv
       AND inv_tofisico.aniotr = waniotr
       AND inv_tofisico.mestra = wmestra
       AND inv_tofisico.cditem = wcditem;
  END IF

  -- Insertando ivnentario
  SET LOCK MODE TO WAIT;
  INSERT INTO inv_tofisico
  VALUES (wcodemp,
          wcodsuc,
          wcodbod,
          waniotr,
          wmestra,
          wfecinv,
          wcditem,
          wcodabr,
          0,
          0,
          0,
          xtotuni,
          0,
          xtotuni,
          0,
          0,
          0,
          USER,
          CURRENT,
          CURRENT HOUR TO SECOND);
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptinsertfaccturasinv(wlnktra INTEGER,
                                       wnumpos SMALLINT,
                                       wcodemp SMALLINT,
                                       wnserie CHAR(10),
                                       wnumdoc INTEGER,
                                       wfecemi DATE,
                                       wcodcli INTEGER,
                                       wnomcli CHAR(100))

 -- Procedimiento para insertar los documentos de facturacion en el inventario
 -- Mynor Ramirez
 -- Dic 2017

 DEFINE wtipmov  LIKE inv_mtransac.tipmov;
 DEFINE wcodsuc  LIKE inv_mtransac.codsuc;
 DEFINE wcodbod  LIKE inv_mtransac.codbod;
 DEFINE waniotr  LIKE inv_mtransac.aniotr;
 DEFINE wmestra  LIKE inv_mtransac.mestra;
 DEFINE wobserv  LIKE inv_mtransac.observ;
 DEFINE westado  LIKE inv_mtransac.estado;
 DEFINE wtipope  LIKE inv_mtransac.tipope;
 DEFINE wafeinv  LIKE fac_puntovta.chkinv;
 DEFINE xlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xcditem  LIKE inv_dtransac.cditem;
 DEFINE xcodabr  LIKE inv_dtransac.codabr;
 DEFINE xnserie  LIKE inv_dtransac.nserie;
 DEFINE xcodepq  LIKE inv_dtransac.codepq;
 DEFINE xcanepq  LIKE inv_dtransac.canepq;
 DEFINE xcanuni  LIKE inv_dtransac.canuni;
 DEFINE xpreuni  LIKE inv_dtransac.preuni;
 DEFINE xtotpro  LIKE inv_dtransac.totpro;
 DEFINE xcorrel  LIKE inv_dtransac.correl;
 DEFINE xbarcod  LIKE inv_dtransac.barcod;
 DEFINE xopeval  LIKE inv_dtransac.opeval;
 DEFINE xopeuni  LIKE inv_dtransac.opeuni;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Seleccionando datos del punto de venta
 SELECT NVL(a.codsuc,0),NVL(a.codbod,0),1
  INTO  wcodsuc,wcodbod,wafeinv
  FROM  fac_puntovta a
  WHERE a.numpos = wnumpos;

 -- Verificando si existen datos
 IF (wcodsuc>0) THEN

  -- Verificando si punto de venta afecta inventario
  IF (wafeinv>0) THEN

   -- Obteniendo tipo de movimiento default de para facturacion
   SELECT NVL(a.valchr,0)
    INTO  wtipmov
    FROM  glb_paramtrs a
    WHERE a.numpar = 13
      AND a.tippar = 1;

   -- Seleccionando tipo de movimiento
   SELECT NVL(a.tipmov,0),NVL(a.tipope,0)
    INTO  wtipmov,wtipope
    FROM  inv_tipomovs a
    WHERE a.tipmov = wtipmov;

   -- Verificando si existen datos
   IF (wtipmov>0) THEN

    -- Asignando datos
    LET wmestra = MONTH(wfecemi);
    LET waniotr = YEAR(wfecemi);
    LET wobserv = "FACTURACION DOCUMENTO # "||wnserie||"-"||wnumdoc;
    LET westado = "V";

    -- Seleccionando datos
    INSERT INTO inv_mtransac
    VALUES (0                     ,  -- Id unico
            wcodemp               ,  -- Empresa
            wcodsuc               ,  -- Sucursal
            wcodbod               ,  -- Bodega
            wtipmov               ,  -- Tipo de movimiento
            0                     ,  -- Origen
            ""                    ,  -- Nombre origen
            wcodcli               ,  -- Destino
            wnomcli               ,  -- Nombre destino
            wlnktra               ,  -- ID factura
            wnumdoc               ,  -- Numero de documento de facturacion
            0                     ,  -- Movimiento es externo Movimiento es externo
            waniotr               ,  -- Anio
            wmestra               ,  -- Mes
            wfecemi               ,  -- Fecha de emision
            wtipope               ,  -- Tipo de operacion del movimiento
            wobserv               ,  -- Observaciones
            westado               ,  -- Estado V=igente
            0                     ,  -- ID numero de consumo
            0                     ,  -- 1=Consumo automatico,0=No consumo automatico
            NULL                  ,  -- Motivo de la anulacion
            NULL                  ,  -- Fecha de anulacion
            NULL                  ,  -- Hora de la anulacion
            NULL                  ,  -- Usuario anulo
            "S"                   ,  -- Documento impreso
            USER                  ,  -- Usuario registro
            CURRENT               ,  -- Fecha de registro
            CURRENT HOUR TO SECOND); -- Hora de registro

    -- Obteniendo serial de la tabla encabezado
    SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
     INTO  xlnktra
     FROM  inv_mtransac;

    -- Creando detalle de la transaccion
    LET xnserie = "";
    FOREACH SELECT y.cditem,
                   y.codabr,
                   0,
                   (y.canuni*x.cantid),
                   (y.canuni*x.cantid),
                   0,   -- precio
                   0,   -- total
                   "0",
                   x.correl
             INTO  xcditem,
                   xcodabr,
                   xcodepq,
                   xcanepq,
                   xcanuni,
                   xpreuni,
                   xtotpro,
                   xbarcod,
                   xcorrel
             FROM  pos_dtransac x,sre_drecetas y
             WHERE x.lnktra = wlnktra
               AND y.lnkrec = x.lnkrec

     -- Asignando precio de costo
     LET xpreuni = (SELECT NVL(a.cospro,0) FROM inv_proenbod a 
                     WHERE a.codemp = wcodemp
                       AND a.codsuc = wcodsuc
                       AND a.codbod = wcodbod
                       AND a.cditem = xcditem);

    -- Calculando total
    LET xtotpro = (xcanuni*xpreuni);

     -- Calculando cantdad y valor para calculo de saldo
     IF (wtipope=0) THEN
        LET xopeuni = (xcanuni*(-1));
        LET xopeval = (xtotpro*(-1));
     ELSE
        LET xopeuni = xcanuni;
        LET xopeval = xtotpro;
     END IF;

     -- Insertando detalle
     INSERT INTO inv_dtransac
     VALUES (xlnktra               ,  -- id transaccion
             wcodemp               ,  -- empresa
             wcodsuc               ,  -- sucursal
             wcodbod               ,  -- bodega
             wtipmov               ,  -- tipo de movimiento
             0                     ,  -- origen
             wcodcli               ,  -- destino
             wfecemi               ,  -- fecha de emision
             waniotr               ,  -- anio
             wmestra               ,  -- mes
             xcditem               ,  -- codigo de producto
             xcodabr               ,  -- codigo abreviado
             xnserie               ,  -- numero de serie
             0                     ,  -- codigo de empaque
             xcanepq               ,  -- cantidad de empaque
             xcanuni               ,  -- cantidad
             xpreuni               ,  -- precio unitario
             xtotpro               ,  -- total del producto
             0                     ,  -- precio promedio
             xcorrel               ,  -- correlativo
             xbarcod               ,  -- codigo de barra
             wtipope               ,  -- tipo de operacion del movimniento
             westado               ,  -- estado
             xopeval               ,  -- total
             xopeuni               ,  -- cantidad
             1                     ,  -- Actualiza existencia
             0                     ,  -- porcentaje isv
             0                     ,  -- total isv
             USER                  ,  -- Usuario registro
             CURRENT               ,  -- Fecha de registro
             CURRENT HOUR TO SECOND); -- Hora de registro

    END FOREACH;

   END IF;

  END IF;

 END IF;
END PROCEDURE;


