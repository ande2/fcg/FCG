--create index idxfacmtransac_1 on fac_mtransac (numpos,feccor,estado,credit);

update statistics high for table fac_mtransac 
(lnktra) resolution 0.50000;
update statistics high for table fac_mtransac 
(numpos,feccor,estado,credit) resolution 0.50000;
update statistics high for table fac_tdocxpos 
(lnktdc) resolution 0.50000;
