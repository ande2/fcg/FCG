  drop view vis_estadiscorte;

create view vis_estadiscorte
(numpos, codemp, lnktdc, nomdoc, nserie, numdoc, fecemi, feccor,
 codcat, nomcat, cditem, dsitem, codabr, cantid,
 preuni, totpro, totisv, totdes)
as
select a.numpos,
       a.codemp,
       a.lnktdc,
       f.nomdoc,
       a.nserie,
       a.numdoc,
       a.fecemi,
       a.feccor,
       c.codcat,
       e.nomcat,
       b.cditem,
       c.dsitem,
       b.codabr,
       b.cantid,
       b.preuni,
       b.totpro,
       0,
       b.descto
from   fac_mtransac a,fac_dtransac b,inv_products c,
       glb_categors e,fac_tipodocs f,fac_tdocxpos z
where  a.lnktra = b.lnktra
  and  a.estado = "V"
  and  c.cditem = b.cditem
  and  e.codcat = c.codcat
  and  f.tipdoc = a.tipdoc
  and  z.lnktdc = a.lnktdc
  and  z.haycor = 1;

grant select on vis_estadiscorte to public;
