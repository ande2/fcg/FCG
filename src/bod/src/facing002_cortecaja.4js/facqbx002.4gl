{
facqbx002.4gl 
Mynor Ramirez
Corte de Caja   
}

-- Definicion de variables globales 

GLOBALS "facglb002.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION facqbx002_CortesCaja(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),    
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        conteo              INTEGER, 
        qryres,qry          STRING,   
        strfeccor           STRING, 
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Verificando acceso a ver todos los cortes
  LET strfeccor = NULL 
  IF NOT seclib001_accesos(progname,6,username) THEN
     LET strfeccor = " AND a.feccor >= TODAY-1 " 
  END IF

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL facqbx002_EstadoMenu(operacion," ")
   LET int_flag = 0

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = 
    "SELECT UNIQUE a.lnkcor,a.feccor,a.fonini,a.totvta,",
                  "a.totgas,a.totcor,a.cierre,'' ",
     "FROM  fac_vicortes a,fac_puntovta b ",
     "WHERE a.numpos = ",xnumpos, 
      " AND b.numpos = a.numpos ",
      strfeccor CLIPPED,
     " ORDER BY a.feccor DESC" 

   -- Declarando el cursor
   PREPARE cdatos FROM qrypart
   DECLARE c_datos SCROLL CURSOR WITH HOLD FOR cdatos

   -- Inicializando vector de seleccion
   CALL v_cortes.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_datos INTO v_cortes[totlin].*
    -- Verificando cierre
    IF v_cortes[totlin].tcierre=1 THEN
       LET v_cortes[totlin].tcierre = "mars_cerrar.png"
    ELSE 
       LET v_cortes[totlin].tcierre = ""
    END IF 

    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_datos
   FREE  c_datos
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL facqbx002_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_cortes TO s_cortes.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL facing002_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION consultar 
      -- Buscando
      CALL facing002_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar 
      -- Unicamente pueden modificarse cortes no cerrados 
      IF w_mae_pro.cierre=1 THEN
         CALL fgl_winmessage(
         "Atencion",
         "Corte de caja ya fue cerrado, no puede modificarse.",
         "stop") 
         CONTINUE DISPLAY 
      END IF 

      -- Cargando detalle de egresos del corte                       
      CALL facqbx002_CargaEgresos()

      -- Modificando
      IF facing002_CortesCaja(2) THEN 
         EXIT DISPLAY
      ELSE   
         -- Desplegando datos
         CALL facqbx002_datos(v_cortes[ARR_CURR()].tlnkcor)
      END IF 

     ON ACTION egresos
      -- Cargando detalle de egresos del corte                       
      CALL facqbx002_CargaEgresos()
      -- Totalizando ventas de la fecha de corte
      CALL facing002_TotalVentas()
      -- Totalizando corte
      CALL facing002_TotalCorte()
      -- Mostrando egresos
      CALL facing002_EgresosCorte()
      -- Totalizando egresos
      CALL facing002_TotalEgresos()
      DISPLAY BY NAME w_mae_pro.totgas

     ON ACTION eliminar 
      -- Cargando detalle de egresos del corte                       
      CALL facqbx002_CargaEgresos()
      -- Totalizando ventas de la fecha de corte
      CALL facing002_TotalVentas()
      -- Totalizando corte
      CALL facing002_TotalCorte()

      -- Eiminando 
      -- Unicamente pueden eliminarse cortes no cerrados 
      IF w_mae_pro.cierre=1 THEN
         CALL fgl_winmessage(
         "Atencion",
         "Corte de caja ya fue cerrado, no puede eliminarse.",
         "stop") 
         CONTINUE DISPLAY 
      END IF 

      -- Verificando si corte ya tiene ventas
      SELECT NVL(COUNT(*),0)
       INTO  conteo
       FROM  fac_mtransac x,fac_tdocxpos z 
       WHERE x.numpos = w_mae_pro.numpos
         AND x.feccor = w_mae_pro.feccor
         AND x.estado IS NOT NULL 
         AND x.credit IS NOT NULL 
         AND z.lnktdc = x.lnktdc
         AND z.haycor = 1
      IF (conteo>0) THEN
         CALL fgl_winmessage(
         "Atencion",
         "Fecha del corte de caja ya tiene registros, VERIFICA."||
         "\nCorte de caja no puede eliminarse.", 
         "stop") 
         CONTINUE DISPLAY 
      END IF 

      -- Confirmacion de la accion a ejecutar 
      LET msg = "Esta SEGURO de ELIMINAR el corte de caja ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         -- Eliminando 
         CALL facing002_grabar(3)
         EXIT DISPLAY
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION imprimir 
      -- Cargando detalle de egresos del corte                       
      CALL facqbx002_CargaEgresos()
      -- Totalizando ventas de la fecha de corte
      CALL facing002_TotalVentas()
      -- Totalizando corte
      CALL facing002_TotalCorte()

      -- Verificando total de fondo inicial 
      IF w_mae_pro.fonini=0 THEN
         CALL fgl_winmessage(
         "Atencion",
         "Fondo inicial no ingresado, VERIFICA.\n",
         "stop") 
         CONTINUE DISPLAY 
      END IF 

      -- Verificando total de efectivo cierre
      IF w_mae_pro.toecie=0 THEN
         CALL fgl_winmessage(
         "Atencion",
         "Total Efectivo Cierre no ingresado, VERIFICA.\n", 
         "stop") 
         CONTINUE DISPLAY 
      END IF 

      -- Verificando si cierre ya fue cerrado
      IF w_mae_pro.cierre=1 THEN
         CALL fgl_winmessage(
         "Atencion",
         "Corte de caja ya cerrado, VERIFICA.\n", 
         "stop") 
         CONTINUE DISPLAY 
      END IF 

      -- Imprimir Corte
      -- Confirmacion de la accion a ejecutar 
      LET msg = "Esta SEGURO de IMPRIMIR el corte de caja ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)
      IF (opc=1) THEN 
         -- Cerrando corte 
         CALL facing002_grabar(4)

         -- Imprimiendo corte
         CALL facrpt002_GeneraCorte(0,"local") 
         EXIT DISPLAY
      END IF 

     ON ACTION pdf 
      -- Cargando detalle de egresos del corte                       
      CALL facqbx002_CargaEgresos()
      -- Totalizando ventas de la fecha de corte
      CALL facing002_TotalVentas()
      -- Totalizando corte
      CALL facing002_TotalCorte()

      -- Verificando si corte no esta cerrado
      IF w_mae_pro.cierre=0 THEN
         CALL fgl_winmessage(
         "Atencion",
         "Corte de caja aun no cerrado, VERIFICA.\n", 
         "stop") 
         CONTINUE DISPLAY 
      END IF 

      -- Enviar corte a pdf 
      -- Confirmacion de la accion a ejecutar 
      CALL facrpt002_GeneraCorte(0,"pdf") 

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL facqbx002_datos(v_cortes[1].tlnkcor)
      ELSE 
         CALL facqbx002_datos(v_cortes[ARR_CURR()].tlnkcor)
      END IF 

      -- Cargando detalle de egresos del corte                       
      CALL facqbx002_CargaEgresos()
      -- Totalizando ventas de la fecha de corte
      CALL facing002_TotalVentas()
      -- Totalizando corte
      CALL facing002_TotalCorte()
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    "Atencion",
    "No existen cortes de caja con el criterio seleccionado.",
    "stop")
    LET loop = FALSE
   END IF 
  END WHILE

  -- Desplegando estado del menu
  CALL facqbx002_EstadoMenu(0,"")
END FUNCTION

-- Subrutina para desplegar los datos del corte

FUNCTION facqbx002_datos(wlnkcor)
 DEFINE wlnkcor LIKE fac_vicortes.lnkcor,
        qryres  STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  fac_vicortes a "||
              "WHERE a.lnkcor = "||wlnkcor

 -- Declarando el cursor
 PREPARE ccorte FROM qryres
 DECLARE c_corte SCROLL CURSOR WITH HOLD FOR ccorte  

 -- Llenando vector de seleccion
 FOREACH c_corte INTO w_mae_pro.*
 END FOREACH
 CLOSE c_corte
 FREE  c_corte

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.numpos,w_mae_pro.feccor,w_mae_pro.fonini,
                 w_mae_pro.totvta,w_mae_pro.totgas,w_mae_pro.totcor,
                 w_mae_pro.totefe,w_mae_pro.tottar,w_mae_pro.totcon,
                 w_mae_pro.totcre,w_mae_pro.totcos,w_mae_pro.totdon,
                 w_mae_pro.totdes,w_mae_pro.totisv,w_mae_pro.toecie, 
                 w_mae_pro.toecor,w_mae_pro.totdif,w_mae_pro.observ,
                 w_mae_pro.userid,w_mae_pro.fecsis,w_mae_pro.horsis,
                 w_mae_pro.ttacie,w_mae_pro.todita
 DISPLAY w_mae_pro.tottar TO ttacor

 IF (w_mae_pro.totdif>0) THEN
    CALL librut001_dpelement("lg05c","Total Faltante:")
 ELSE
    IF (w_mae_pro.totdif<0) THEN
       CALL librut001_dpelement("lg05c","Total Sobrante:")
    ELSE
       CALL librut001_dpelement("lg05c","Total Diferencia:")
    END IF
 END IF

 -- Diferencia Tarjeta corte
 IF (w_mae_pro.todita>0) THEN
    CALL librut001_dpelement("lg06c","Total Faltante:")
 ELSE
    IF (w_mae_pro.todita<0) THEN
       CALL librut001_dpelement("lg06c","Total Sobrante:")
    ELSE
       CALL librut001_dpelement("lg06c","Total Diferencia:")
    END IF
 END IF

 -- Cargando detalle de egresos del corte                       
 CALL facqbx002_CargaEgresos()
END FUNCTION 

-- Subrutina para cargar los egresos del corte 

FUNCTION facqbx002_CargaEgresos() 
 -- Inicializando
 CALL v_doctos.clear()
 LET totdoc = 1 

 -- Cargando egresos                    
 DECLARE c1 CURSOR FOR
 SELECT x.fecemi,
        x.numdoc,
        x.totval,
        x.codaut, 
        x.observ,
        x.userid,
        x.horsis, 
        '',
        x.correl 
  FROM  fac_dvicorte x
  WHERE x.numpos = w_mae_pro.numpos 
    AND x.fecemi = w_mae_pro.feccor 
  ORDER BY x.correl 
  FOREACH c1 INTO v_doctos[totdoc].*
   LET totdoc = totdoc+1 
  END FOREACH 
  CLOSE c1
  FREE  c1
  LET totdoc = totdoc-1 

  -- Totalizando
  CALL facing002_TotalEgresos()
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION facqbx002_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       CHAR(80) 

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement(
         "labelx","Lista de Cortes de Caja")
  WHEN 1 CALL librut001_dpelement(
         "labelx","Lista de Cortes de Caja - CONSULTAR"||msg CLIPPED)
  WHEN 2 CALL librut001_dpelement(
         "labelx","Lista de Cortes de Caja - MODIFICAR"||msg CLIPPED)
  WHEN 3 CALL librut001_dpelement(
         "labelx","Lista de Cortes de Caja - BORRAR"||msg CLIPPED)
  WHEN 4 CALL librut001_dpelement(
         "labelx","Lista de Cortes de Caja - INGRESAR")
 END CASE
END FUNCTION
