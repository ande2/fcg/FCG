alter table fac_vicortes
add (
    observ char(200) before totvta, 
    toecie decimal(12,2) before userid,
    cbi500 smallint before userid,
    cbi100 smallint before userid,
    cbi050 smallint before userid,
    cbi020 smallint before userid,
    cbi010 smallint before userid,
    cbi005 smallint before userid,
    cbi002 smallint before userid,
    cbi001 smallint before userid,
    cmo050 smallint before userid,
    cmo020 smallint before userid,
    cmo010 smallint before userid,
    cmo005 smallint before userid,
    toecor decimal(12,2) before userid,
    totdif decimal(12,2) before userid
  );
