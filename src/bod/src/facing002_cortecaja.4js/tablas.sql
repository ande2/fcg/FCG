create table "sistemas".fac_vicortes 
  (
    lnkcor serial not null ,
    numpos smallint not null ,
    feccor date not null ,
    tipcor smallint not null ,
    observ char(200),
    totvta decimal(12,2) not null ,
    fonini decimal(12,2) not null ,
    totgas decimal(12,2) not null ,
    totcor decimal(12,2) not null ,
    totefe decimal(12,2) not null ,
    tottar decimal(12,2) not null ,
    totcon decimal(12,2) not null ,
    totcre decimal(12,2) not null ,
    totcos decimal(12,2) not null ,
    totdon decimal(12,2) not null ,
    totisv decimal(12,2),
    totdes decimal(16),
    bil500 smallint not null ,
    bil100 smallint not null ,
    bil050 smallint not null ,
    bil020 smallint not null ,
    bil010 smallint not null ,
    bil005 smallint not null ,
    bil002 smallint not null ,
    bil001 smallint not null ,
    mon050 smallint not null ,
    mon020 smallint not null ,
    mon010 smallint not null ,
    mon005 smallint not null ,
    toecie decimal(12,2),
    cbi500 smallint,
    cbi100 smallint,
    cbi050 smallint,
    cbi020 smallint,
    cbi010 smallint,
    cbi005 smallint,
    cbi002 smallint,
    cbi001 smallint,
    cmo050 smallint,
    cmo020 smallint,
    cmo010 smallint,
    cmo005 smallint,
    toecor decimal(12,2),
    totdif decimal(12,2),
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    cierre smallint not null ,
    usrcie varchar(15,1),
    feccie date,
    horcie datetime hour to second,
    
    check (feccor >= DATE ('01/06/2016' ) ) constraint "sistemas".ckfacvicortes1,
    primary key (lnkcor)  constraint "sistemas".pkfacvicortes
  );

create table "sistemas".fac_dvicorte 
  (
    lnkcor integer not null ,
    fecemi date not null ,
    numdoc char(20) not null ,
    totval decimal(12,2) not null ,
    codaut smallint not null ,
    observ char(100) not null ,
    correl smallint not null 
  );


alter table "sistemas".fac_dvicorte add constraint (foreign key 
    (lnkcor) references "sistemas".fac_vicortes  on delete cascade 
    constraint "sistemas".fkfacvicortes1);


