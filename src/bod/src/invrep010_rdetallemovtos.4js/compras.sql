
 unload to comprashuevo.cs
 SELECT extend(a.fecemi,year to month),
          i.codabr[1,5],
          i.despro[1,10],
          SUM(d.canuni),
          trunc((SUM(d.canuni*d.preuni)/1.20),2),
          trunc(SUM(d.canuni*d.preuni),2)
   FROM  inv_mtransac a,inv_dtransac d,inv_tipomovs t,
          inv_products i,inv_unimedid u,glb_categors r,
          glb_subcateg e,outer inv_empaques g,
          outer fac_clientes c,outer inv_provedrs p
    WHERE a.lnktra  = d.lnktra
      AND a.estado  = 'V'
      AND a.observ  NOT MATCHES "*JUSTO*"
      AND d.codemp  = 2
      AND d.codbod  = 17
      AND d.tipmov  = 112
      AND d.fecemi >= '010714'
      AND d.fecemi <= '311214'
      AND t.tipmov  = d.tipmov
      AND i.cditem  = d.cditem
      AND r.codcat  = i.codcat
      AND r.codcat  = 2
      AND e.codcat  = i.codcat
      AND e.subcat  = i.subcat
      AND g.codepq  = d.codepq
      AND u.unimed  = i.unimed
      AND c.codcli  = a.coddes
      AND p.codprv  = a.codori
    GROUP BY 1,2,3
    ORDER BY 1,2,3
