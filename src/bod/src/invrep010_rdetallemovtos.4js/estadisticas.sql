unload to ventasmayoristas.csv delimiter ","
select extend(a.fecemi,year to month),nomcli[1,30],sum(a.totpag),
       (sUM(b.totpro)/SUM(b.cantid)*30)
from fac_mtransac a,fac_dtransac b
WHERE a.lnktra = b.lnktra
and a.feCEMI >"010413" AND a.ESTADO = "V"
and a.totpag >5000
and a.codcli != 5
and b.codabr[1,2] = "21"
GROUP BY 1,2
ORDER BY 1,2
