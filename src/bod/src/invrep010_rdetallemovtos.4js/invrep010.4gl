{ 
Fecha    : Enero 2011 
Programo : invrep010.4gl 
Objetivo : Reporte de detalle de movimientos de inventario 
}

DATABASE storepos 

{ Definicion de variables globales }

TYPE   datosreporte RECORD
        codemp   LIKE inv_proenbod.codemp, 
        codsuc   LIKE inv_proenbod.codsuc, 
        codbod   LIKE inv_proenbod.codbod,
        tipmov   LIKE inv_dtransac.tipmov,
        orides   SMALLINT,
        nombre   CHAR(25), 
        fecemi   LIKE inv_mtransac.fecemi,
        cditem   LIKE inv_proenbod.cditem, 
        codabr   CHAR(20),
        dsitem   CHAR(36),
        nommed   CHAR(20),
        lnktra   LIKE inv_mtransac.lnktra,
        numrf1   CHAR(20),
        numrf2   CHAR(20), 
        nommov   LIKE inv_tipomovs.nommov,
        tipope   LIKE inv_dtransac.tipope,
        canepq   LIKE inv_dtransac.canepq,
        canuni   LIKE inv_dtransac.canuni,
        preuni   LIKE inv_dtransac.preuni, 
        totpro   LIKE inv_dtransac.totpro, 
        nomuni   CHAR(6),
        nomepq   CHAR(20) 
       END RECORD
DEFINE w_mae_bod RECORD LIKE inv_mbodegas.*,
       w_mae_emp RECORD LIKE glb_empresas.*,
       w_mae_suc RECORD LIKE glb_sucsxemp.*,
       w_tip_mov RECORD LIKE inv_tipomovs.*,
       w_datos   RECORD
        codbod   LIKE inv_mtransac.codbod,
        codemp   LIKE inv_mtransac.codemp,
        codsuc   LIKE inv_mtransac.codbod,
        tipmov   LIKE inv_mtransac.tipmov, 
        codprv   LIKE inv_provedrs.codprv, 
        codcli   LIKE fac_clientes.codcli, 
        codcat   LIKE glb_categors.codcat,
        subcat   LIKE glb_subcateg.subcat,
        cditem   LIKE inv_dtransac.cditem,
        fecini   DATE,
        fecfin   DATE
       END RECORD,
       existe    SMALLINT,
       filename  STRING 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar7")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rdetallemovtos")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep010_DetalleMovimientos()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep010_DetalleMovimientos()
 DEFINE imp1              datosreporte,
        wpais             VARCHAR(255),
        loop              SMALLINT,
        pipeline          STRING, 
        qrytxt            STRING,
        strcodcat         STRING,
        strsubcat         STRING,
        strcditem         STRING,
        strtipmov         STRING,
        strcodori         STRING 

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep010a AT 5,2
  WITH FORM "invrep010a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invrep010",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/DetalleMovimientos.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando combobox de bodegas x usuario
  CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
  -- Llenando combobox de tipos de movimiento
  CALL librut003_CbxTiposMovimiento()
  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()
  -- Llenando combox de proveedores
  CALL librut003_cbxproveedores()
  --Llenando combox de clientes
  --CALL librut003_CbxClientes()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   CLEAR FORM

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codbod,
                 w_datos.tipmov, 
                 w_datos.codprv, 
                 w_datos.codcli, 
                 w_datos.codcat,
                 w_datos.subcat,
                 w_datos.cditem,
                 w_datos.fecini,
                 w_datos.fecfin
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Verificando datos 
     IF w_datos.codbod IS NULL OR
        w_datos.tipmov IS NULL OR
        w_datos.fecini IS NULL OR
        w_datos.fecfin IS NULL THEN
        ERROR "Error: deben completarse los filtos de seleccion." 
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 

     -- Verificando datos 
     IF w_datos.codbod IS NULL OR
        w_datos.tipmov IS NULL OR
        w_datos.fecini IS NULL OR
        w_datos.fecfin IS NULL THEN
        ERROR "Error: deben completarse los filtos de seleccion." 
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON CHANGE codbod
     -- Obteniendo datos de la bodega
     CALL librut003_bbodega(w_datos.codbod)
     RETURNING w_mae_bod.*,existe

     -- Asignando datos de empresa y sucursal de la bodega
     LET w_datos.codemp = w_mae_bod.codemp
     LET w_datos.codsuc = w_mae_bod.codsuc

     -- Obteniendo datos de la empresa
     CALL librut003_bempresa(w_mae_bod.codemp)
     RETURNING w_mae_emp.*,existe
     -- Obteniendo datos de la sucursal
     CALL librut003_bsucursal(w_mae_bod.codsuc)
     RETURNING w_mae_suc.*,existe

     -- Desplegando datos de la empresa y sucursal
     DISPLAY BY NAME w_datos.codemp,w_datos.codsuc,
                     w_mae_emp.nomemp,w_mae_suc.nomsuc

     -- Limpiando combos de categorias y subcategorias
     LET w_datos.codcat = NULL
     LET w_datos.subcat = NULL
     CLEAR codcat,subcat

    ON CHANGE codcat
     -- Limpiando combos
     LET w_datos.subcat = NULL
     LET w_datos.cditem = NULL
     CLEAR subcat,cditem

     -- Llenando combox de subcategorias
     IF w_datos.codcat IS NOT NULL THEN 
        CALL librut003_cbxsubcategorias(w_datos.codcat)

        -- Llenando combox de productos
        CALL librut003_cbxproductosdes(w_datos.codcat,w_datos.subcat) 
     END IF 

    ON CHANGE subcat
     -- Limpiando combos
     LET w_datos.cditem = NULL
     CLEAR cditem

     -- Llenando combobox de productos 
     IF w_datos.subcat IS NOT NULL THEN 
        -- Llenando combox de productos
        CALL librut003_cbxproductosdes(w_datos.codcat,w_datos.subcat) 
     END IF 

    AFTER FIELD codbod
     -- Verificando bodega 
     IF w_datos.codbod IS NULL THEN
        ERROR "Error: debe de seleccionarse la bodega a listar."
        NEXT FIELD codbod
     END IF

    AFTER FIELD tipmov 
     -- Verificando tipo de movimiento 
     IF w_datos.codbod IS NULL THEN 
        ERROR "Error: debe de seleccionarse el tipo de movimiento a listar."
        NEXT FIELD tipmov 
     END IF

     -- Obteniendo datos del tipo de movimiento
     INITIALIZE w_tip_mov.* TO NULL
     CALL librut003_BTipoMovimiento(w_datos.tipmov)
     RETURNING w_tip_mov.*,existe 

     -- Verificando tipo de operacion del movimiento
     CASE (w_tip_mov.tipope) 
      WHEN 1 -- Entradas
       CALL DIALOG.SetFieldActive("codcli",0) 
       CALL DIALOG.SetFieldActive("codprv",1) 
       LET w_datos.codcli = NULL
       DISPLAY BY NAME w_datos.codcli 
      WHEN 0 -- Salidas
       CALL DIALOG.SetFieldActive("codcli",1) 
       CALL DIALOG.SetFieldActive("codprv",0) 
       LET w_datos.codprv = NULL
       DISPLAY BY NAME w_datos.codprv 
     END CASE 

    AFTER FIELD fecini
     -- Verificando fecha inicial
     IF w_datos.fecini IS NULL THEN
        ERROR "Error: debe ingresarse la fecha inicial a listar."
        NEXT FIELD fecini 
     END IF

    AFTER FIELD fecfin
     -- Verificando fecha final
     IF w_datos.fecfin IS NULL THEN
        ERROR "Error: debe ingresarse la fecha final a listar."
        NEXT FIELD fecfin 
     END IF

    AFTER INPUT
     -- Verificando datos
     IF w_datos.codbod IS NULL OR
        w_datos.tipmov IS NULL OR
        w_datos.fecini IS NULL OR
        w_datos.fecfin IS NULL OR
        pipeline IS NULL THEN
        NEXT FIELD codbod
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Verificando seleccion de categoria
   LET strcodcat = NULL
   IF w_datos.codcat IS NOT NULL THEN
      LET strcodcat = " AND i.codcat = ",w_datos.codcat
   END IF

   -- Verificando condicion de subcategoria
   LET strsubcat = NULL
   IF w_datos.subcat IS NOT NULL THEN
      LET strsubcat = " AND i.subcat = ",w_datos.subcat
   END IF

   -- Verificando condicion de producto
   LET strcditem = NULL
   IF w_datos.cditem IS NOT NULL THEN
      LET strcditem = " AND d.cditem = ",w_datos.cditem 
   END IF

   -- Verificando tipo de operacion del movimiento
   IF (w_tip_mov.tipope=1) THEN -- Ingresos
      LET strtipmov = "a.codori,p.nomprv,"
       
      LET strcodori = NULL 
      IF w_datos.codprv IS NOT NULL THEN 
         LET strcodori = " AND a.codori = ",w_datos.codprv
      END IF 
   ELSE
      LET strtipmov = "a.coddes,c.nomcli,"

      LET strcodori = NULL 
      IF w_datos.codcli IS NOT NULL THEN 
         LET strcodori = " AND a.coddes = ",w_datos.codcli 
      END IF 
   END IF 

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   LET qrytxt = 
   "SELECT d.codemp,d.codsuc,d.codbod,d.tipmov,",strtipmov CLIPPED, 
          "d.fecemi,d.cditem,i.codabr,i.despro,u.nommed,a.lnktra,a.numrf1,",
          "a.numrf2,t.nommov,d.tipope,d.canepq,d.canuni,d.preuni,d.totpro,",
          "u.nomabr,NVL(g.nomepq,u.nommed) ",
    "FROM  inv_mtransac a,inv_dtransac d,inv_tipomovs t,",
          "inv_products i,inv_unimedid u,glb_categors r,",
          "glb_subcateg e,outer inv_empaques g,",
          "outer fac_clientes c,outer inv_provedrs p ",
    "WHERE a.lnktra  = d.lnktra ",
      "AND a.estado  = 'V' ",
      strcodori CLIPPED, 
      "AND d.codemp  = ",w_datos.codemp," ",
      "AND d.codsuc  = ",w_datos.codsuc," ",
      "AND d.codbod  = ",w_datos.codbod," ",
      "AND d.tipmov  = ",w_datos.tipmov," ",
      "AND d.fecemi >= '",w_datos.fecini,"' ",
      "AND d.fecemi <= '",w_datos.fecfin,"' ",
      strcditem CLIPPED,
      "AND t.tipmov  = d.tipmov ",
      "AND i.cditem  = d.cditem ",
      strcodcat CLIPPED,
      strsubcat CLIPPED,
      "AND r.codcat  = i.codcat ",
      "AND e.codcat  = i.codcat ",
      "AND e.subcat  = i.subcat ",
      "AND g.codepq  = d.codepq ",
      "AND u.unimed  = i.unimed ",
      "AND c.codcli  = a.coddes ",
      "AND p.codprv  = a.codori ",
    "ORDER BY 4,6,7" 

   LET existe = FALSE
   PREPARE c_rep010 FROM qrytxt 
   DECLARE c_crep010 CURSOR FOR c_rep010
   FOREACH c_crep010 INTO imp1.*
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE
       START REPORT invrep010_GeneraReporte TO filename
    END IF 

    -- Llenando reporte
    OUTPUT TO REPORT invrep010_GeneraReporte(imp1.*)
   END FOREACH
   CLOSE c_crep010 
   FREE  c_crep010 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT invrep010_GeneraReporte 

      -- Enviando reporte al destino seleccionado
      CALL librut001_sendreport
      (filename,pipeline,"Detalle de Movimientos",
      "--noline-numbers "||
      "--nofooter "||
      "--font-size 7 "||
      "--page-width 842 --page-height 595 "||
      "--left-margin 55 --right-margin 25 "||
      "--top-margin 35 --bottom-margin 45 "||
      "--title Inventarios")

      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE
 CLOSE WINDOW wrep010a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT invrep010_GeneraReporte(imp1)
 DEFINE xnomcat           LIKE glb_categors.nomcat, 
        xnomsub           LIKE glb_subcateg.nomsub, 
        xdsitem           LIKE inv_products.dsitem,  
        imp1              datosreporte, 
        lg,i              SMALLINT, 
        linea             CHAR(176),
        wdocto            CHAR(20)

  OUTPUT PAGE   LENGTH 50 
         LEFT   MARGIN 0
         TOP    MARGIN 0
         BOTTOM MARGIN 0 

  FORMAT
   PAGE HEADER
    LET linea = "-"
    LET lg = 176 
    FOR i = 1 TO lg 
     LET linea = linea CLIPPED,"-"
    END FOR 

    -- Imprimiendo encabezado 
    PRINT COLUMN   1,"Inventarios",
          COLUMN (lg-20),PAGENO USING "Pagina: <<<<"
    PRINT COLUMN   1,"Invrep010",
          COLUMN  66,"DETALLE DE MOVIMIENTOS",
          COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy"
    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED, 
          COLUMN  58,"Periodo Del ",w_datos.fecini USING "dd/mmm/yyyy",
           	     " Al ",        w_datos.fecfin USING "dd/mmm/yyyy", 
          COLUMN (lg-20),"Hora  : ",TIME
    PRINT linea	       
    PRINT "Fecha de     Numero               Proveedor/Destino        ",
          "  Producto                              Uni      Cantidad Nombre                 ",
          "Cantidad    Precio            Valor"
    PRINT "Emision      Documento                                     ",
          "                                        Med      Empaque  Empaque                ",
          "Total       Unitario          Total"
    PRINT linea

  BEFORE GROUP OF imp1.codbod 
   -- Imprimiendo datos de la bodega 
   PRINT w_mae_emp.nomemp         CLIPPED
   PRINT "SUCURSAL [",imp1.codsuc USING "<<<<","] ",
         w_mae_suc.nomsuc         CLIPPED
   PRINT "BODEGA [",imp1.codbod   USING "<<<<","] ",
         w_mae_bod.nombod         CLIPPED
   SKIP 1 LINES 

  BEFORE GROUP OF imp1.tipmov 
   -- Imprimiendo datos del tipo de movimiento 
   PRINT imp1.nommov CLIPPED 
   SKIP 1 LINES 

  BEFORE GROUP OF imp1.orides
   -- Imprimiendo datos del proveedor/cliente
   PRINT imp1.nombre CLIPPED

  ON EVERY ROW
   -- Imprimiendo movimientos  
   -- Asignando numeros de documento 
   IF (LENGTH(imp1.numrf2)>0) THEN
      LET wdocto = imp1.lnktra USING "<<<<<<<<<<","/",
                   imp1.numrf1 CLIPPED,"/",
                   imp1.numrf2 CLIPPED  
   ELSE
      LET wdocto = imp1.lnktra USING "<<<<<<<<<<","/",
                   imp1.numrf1 CLIPPED
   END IF 

   -- Imprimiendo 
   PRINT imp1.fecemi               USING "dd/mmm/yyyy"   ,2 SPACES,
         wdocto                                          ,1 SPACES,
         imp1.nombre                                     ,2 SPACES,
         imp1.dsitem                                     ,2 SPACES, 
         imp1.nomuni                                     ,1 SPACES, 
         imp1.canepq               USING "###,##&.&&"    ,1 SPACES,
         imp1.nomepq                                     ,1 SPACES,
         imp1.canuni               USING "###,##&.&&"    ,1 SPACES,
         imp1.preuni               USING "##,##&.&&&&"   ,1 SPACES,
         imp1.totpro               USING "###,###,##&.&&"

  AFTER GROUP OF imp1.orides
   -- Imprimiendo datos del proveedor/cliente
   PRINT COLUMN   1,"TOTAL ",imp1.nombre CLIPPED," -->",
         COLUMN 135,GROUP SUM(imp1.canuni)     USING "###,###,##&.&&" ,13 SPACES,
                    GROUP SUM(imp1.totpro)     USING "###,###,##&.&&"
   SKIP 1 LINES 

  AFTER GROUP OF imp1.tipmov 
   -- Totalizando tipo de movimiento 
   SKIP 1 LINES 
   PRINT COLUMN   1,"TOTAL ",imp1.nommov CLIPPED," -->",
         COLUMN 135,GROUP SUM(imp1.canuni)     USING "###,###,##&.&&" ,13 SPACES,
                    GROUP SUM(imp1.totpro)     USING "###,###,##&.&&"
   SKIP 1 LINES 

  AFTER GROUP OF imp1.codbod 
    -- Totalizando bodega 
   PRINT COLUMN   1,"TOTAL REPORTE -->",
         COLUMN 135,GROUP SUM(imp1.canuni)     USING "###,###,##&.&&" ,13 SPACES,
                    GROUP SUM(imp1.totpro)     USING "###,###,##&.&&"

  ON LAST ROW
   -- Imprimiendo filtros
   SKIP 1 LINES
   PRINT "FILTROS"
   IF (w_datos.codcat IS NOT NULL) THEN
      INITIALIZE xnomcat TO NULL 
      SELECT a.nomcat INTO xnomcat FROM glb_categors a 
       WHERE a.codcat = w_datos.codcat 
      PRINT "Categoria    : ",xnomcat 
   END IF
   IF (w_datos.subcat IS NOT NULL) THEN
      INITIALIZE xnomsub TO NULL 
      SELECT a.nomsub INTO xnomsub FROM glb_subcateg a 
       WHERE a.codcat = w_datos.codcat AND a.subcat = w_datos.subcat 
      PRINT "Subcategoria : ",xnomsub 
   END IF
   IF (w_datos.cditem IS NOT NULL) THEN
      INITIALIZE xdsitem TO NULL 
      SELECT a.dsitem INTO xdsitem FROM inv_products a 
       WHERE a.cditem = w_datos.cditem 
      PRINT "Producto     : ",xdsitem
   END IF
END REPORT
