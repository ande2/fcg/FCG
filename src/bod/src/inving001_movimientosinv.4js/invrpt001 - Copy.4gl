{
Fecha    : Diciembre 2010 
Programo : Mynor Ramirez 
Objetivo : Impresion de movimientos.
}

-- Definicion de variables globales  

GLOBALS "invglb001.4gl" 
DEFINE w_mae_usr    RECORD LIKE glb_usuarios.*,
       fnt          RECORD
        cmp         CHAR(12),
        nrm         CHAR(12),
        tbl         CHAR(12),
        fbl,t88     CHAR(12),
        t66,p12     CHAR(12),
        p10,srp     CHAR(12),
        twd         CHAR(12),
        fwd         CHAR(12),
        tda,fda     CHAR(12),
        ini         CHAR(12)
       END RECORD,
       existe,x,y   SMALLINT, 
       cambio       SMALLINT, 
       w_flag       SMALLINT,
       filename     CHAR(90),
       pipeline     CHAR(90),
       titulo       CHAR(50),
       i            INT

-- Subrutina para imprimir un movimiento de producto

FUNCTION invrpt001_reportemov()
 CALL fgl_winmessage(" Atencion","Presione [ENTER] para imprimir el movimiento.","information")

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/Movimiento.spl"

 -- Obteniendo datos del tipo de movimiento
 INITIALIZE w_mae_tip.* TO NULL
 CALL librut003_btipomovimiento(w_mae_tra.tipmov)
 RETURNING w_mae_tip.*,existe

 -- Asignando valores
 LET pipeline = "pdf2"   

 -- Seleccionando fonts para impresora epson
 CALL librut001_fontsprn(pipeline,pipeline) 
 RETURNING fnt.* 

 -- Iniciando reporte
 START REPORT invrpt001_printmov TO filename 
  OUTPUT TO REPORT invrpt001_printmov()
 FINISH REPORT invrpt001_printmov 

 -- Enviando reporte al destino seleccionado
 CALL librut001_sendreport
 (filename,pipeline,"",
  "--noline-numbers "||
  "--nofooter "||
  "--font-size 8 "||
  "--page-width 842 --page-height 595 "||
  "--left-margin 55 --right-margin 25 "||
  "--top-margin 35 --bottom-margin 45 "||
  "--title Inventarios")
END FUNCTION 

-- Subrutinar para generar la impresion del movimiento 

REPORT invrpt001_printmov() 
 DEFINE linea     CHAR(121),
        exis      SMALLINT, 
        lh,lv,si  CHAR(1),
        sd,ii,id  CHAR(1),
        x         CHAR(1),
        w_tipcob  CHAR(7),
        w_tipcar  CHAR(7)

  OUTPUT LEFT   MARGIN 3 
         PAGE   LENGTH 92
         TOP    MARGIN 3
         BOTTOM MARGIN 3

 FORMAT 
  PAGE HEADER

   -- Definiendo linea de impresion 
   LET linea = "_____________________________________________________________",
               "_____________________________________________________________",
               "_____________________________________________________________",
               "_____________"


   -- Inicializando impresora
   PRINT fnt.ini CLIPPED,fnt.t66 CLIPPED,fnt.cmp CLIPPED,fnt.p12 CLIPPED
   -- Imprimiendo Encabezado
   PRINT COLUMN   1,"Inventario",
         COLUMN 100,PAGENO USING  "Pagina: <<"
   PRINT COLUMN   1,"invrpt001",
         COLUMN  48,"MOVIMIENTOS DE INVENTARIO",
         COLUMN 100,"Fecha : ",TODAY USING "dd/mmm/yyyy"
   PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
         COLUMN 100,"Hora  : ",TIME

   PRINT linea
   PRINT COLUMN   1,fnt.twd CLIPPED,w_mae_tip.nommov CLIPPED,
                    fnt.fwd CLIPPED
   PRINT COLUMN   1,fnt.twd CLIPPED,"Movimiento Numero [",w_mae_tra.lnktra USING "<<<<<<<<<<<","]",
                    fnt.fwd CLIPPED

   PRINT COLUMN   1,fnt.tbl CLIPPED,"Empresa           : ",fnt.fbl CLIPPED,
                    w_mae_tra.codemp USING "<<<<<<"," ",
                    w_mae_emp.nomemp CLIPPED,
         COLUMN  85,fnt.tbl CLIPPED,"Fecha de Registro : ",fnt.tbl CLIPPED,
                    w_mae_tra.fecsis USING "dd/mm/yyyy"

   PRINT COLUMN   1,fnt.tbl CLIPPED,"Sucursal          : ",fnt.fbl CLIPPED,
                    w_mae_tra.codsuc USING "<<<<<<"," ",
                    w_mae_suc.nomsuc CLIPPED,
         COLUMN  85,fnt.tbl CLIPPED,"Hora de Registro  : ",fnt.tbl CLIPPED,
                    w_mae_tra.horsis 

   PRINT COLUMN   1,fnt.tbl CLIPPED,"Bodega            : ",fnt.fbl CLIPPED,
                    w_mae_tra.codbod USING "<<<<<<"," ",
                    w_mae_bod.nombod CLIPPED,
         COLUMN  85,fnt.tbl CLIPPED,"Operador          : ",fnt.tbl CLIPPED,
                    w_mae_tra.userid 

   PRINT COLUMN   1,fnt.tbl CLIPPED,"Fecha Movimiento  : ",fnt.fbl CLIPPED,
                    w_mae_tra.fecsis USING "dd/mm/yyyy",
         COLUMN  85,fnt.tbl CLIPPED,"Estado            : ",fnt.tbl CLIPPED,
                    nomest CLIPPED 

   -- Verificando tipo de operacion del movimiento
   IF (w_mae_tra.tipope=1) THEN 
      PRINT COLUMN   1,fnt.tbl CLIPPED,"Origen            : ",fnt.fbl CLIPPED,
                       w_mae_tra.codori USING "<<<<<<", " ",w_mae_tra.nomori CLIPPED 
   ELSE
      PRINT COLUMN   1,fnt.tbl CLIPPED,"Destino           : ",fnt.fbl CLIPPED,
                       w_mae_tra.coddes USING "<<<<<<", " ",w_mae_tra.nomdes CLIPPED 
   END IF 

   PRINT COLUMN   1,fnt.tbl CLIPPED,"Docto Referencia 1: ",fnt.fbl CLIPPED,
                    w_mae_tra.numrf1
   PRINT COLUMN   1,fnt.tbl CLIPPED,"Docto Referencia 2: ",fnt.fbl CLIPPED,
                    w_mae_tra.numrf2

   PRINT COLUMN   1,fnt.tbl CLIPPED,"Observaciones     : ",fnt.fbl CLIPPED,
                    w_mae_tra.observ[1,100]

   -- Verificando segunda linea de observaciones
   IF LENGTH(w_mae_tra.observ[101,200])>0 THEN 
      PRINT COLUMN  25,w_mae_tra.observ[101,200]
   ELSE 
      PRINT 1 SPACES
   END IF 

   -- Imprimiendo rotulo del detalle de productos
   PRINT COLUMN 1,fnt.twd CLIPPED,"DETALLE DEL MOVIMIENTO",fnt.fwd CLIPPED 
   PRINT COLUMN 1,"Codigo               D E S C R I P C I O N                                 Cantidad    Cantidad    ",
                  "Precio      Total"   
   PRINT COLUMN 1,"Producto                                                                   Empaque     TOTAL       ",
                  "Unitario    Producto"  
   PRINT linea
   PRINT 1 SPACES

  ON EVERY ROW
   -- Imprimiendo detalle de productos
   FOR i = 1 TO totlin
    PRINT COLUMN   1,v_products[i].cditem                              ,
          COLUMN  22,v_products[i].dsitem                              , 2 SPACES,
                     v_products[i].canepq       USING "##,###,###"     , 2 SPACES,
                     v_products[i].canuni       USING "##,###,###"     , 2 SPACES,
                     v_products[i].preuni       USING "###,##&.&&"     , 2 SPACES,
                     v_products[i].totpro       USING "###,##&.&&" 
   END FOR

  ON LAST ROW
   -- Imprimiendo totales y pie de pagina 
   PRINT linea
   PRINT COLUMN   1,"TOTALES --->"                                     , 73 SPACES,
                    totuni                      USING "##,###,###"     , 14 SPACES,
                    totval                      USING "###,##&.&&" 
   SKIP 1 LINES
   PRINT "Productos ",totlin USING "<<<"

   -- Verificando si hay motivo de anulacion 
   IF w_mae_tra.estado="A" AND LENGTH(w_mae_tra.motanl)>0 THEN
      SKIP 1 LINES 
      PRINT fnt.tbl CLIPPED,"Motivo Anulacion  : ",fnt.fbl CLIPPED,
            w_mae_tra.motanl 
      PRINT "Anulada Por [ ",w_mae_tra.usranl CLIPPED," ] el ",
            w_mae_tra.fecanl," A Las ",w_mae_tra.horanl  
   END IF

   -- Imprimiendo responsables  
   -- Verificando si tipo de movimiento lleva responsables
   IF w_mae_tip.hayres THEN 
      SKIP 3 LINES
      PRINT COLUMN 20,"f:______________________________",
            COLUMN 80,"f:______________________________" 
      PRINT COLUMN 20,"         [ Entregado Por ]",
            COLUMN 80,"         [ Recibido Por  ]"
   END IF 
   
   -- Verificando si es reimpresion
   SKIP 1 LINES
   IF reimpresion THEN
      PRINT "**REIMPRESION**" 
   ELSE
      PRINT "**ORIGINAL**" 
   END IF 
END REPORT 
