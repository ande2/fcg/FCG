{ 
Fecha    : Diciembre 2011        
Programo : Mynor Ramirez
Objetivo : Programa de criterios de seleccion para:
           Consulta/Anulacion/Eliminacion/Impresion de movimientos de inventario
}

-- Definicion de variables globales 
GLOBALS "invglb001.4gl" 

-- Subrutina para consultar/anular/imprimir movimientos 

FUNCTION invqbx001_movimientos(operacion)
 DEFINE qrytext,qrypart     CHAR(500),
        cndanl              CHAR(20),
        loop,existe         SMALLINT,
        operacion           SMALLINT,
        totreg              INTEGER, 
        qryres,msg,titqry   CHAR(80)

 -- Verificando operacion
 CASE (operacion)
  WHEN 1 LET cndanl = NULL 
  WHEN 2 LET cndanl = " AND a.numord = 1 "
 END CASE

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Mostrando campo de motivo de anulacion 
 CALL f.setFieldHidden("formonly.motanl",0)
 CALL f.setElementHidden("labelb",0)

 -- Cargando combobox de tipo de movimiento
 CALL librut003_CbxTiposMovimiento()
 -- Cargando origenes
 CALL librut003_CbxOrigenesMovtos()
 -- Cargando destinos
 CALL librut003_CbxDestinosMovtos()

 -- Buscando datos 
 LET loop = TRUE 
 WHILE loop 
  -- Inicializando las variables 
  CALL invqbx001_EstadoMenu(operacion," ")
  INITIALIZE qrytext,qrypart TO NULL
  LET int_flag = 0 
  CLEAR FORM

  -- Construyendo busqueda 
  CONSTRUCT BY NAME qrytext 
                 ON a.lnktra,
                    a.codbod,
                    a.tipmov,
                    a.fecemi,
                    a.codori,
                    a.coddes,
                    a.numrf1,
                    a.numrf2,
                    a.observ,
                    a.motanl, 
                    a.fecsis,
                    a.horsis,
                    a.userid,
                    a.estado,
                    a.aniotr,
                    a.mestra 
   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT CONSTRUCT
  END CONSTRUCT
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Preparando la busqueda 
  ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

  -- Creando la busqueda 
  LET qrypart = "SELECT a.lnktra,a.codemp,a.codsuc,a.codbod,a.fecemi ",
                 "FROM  inv_mtransac a ",
                 "WHERE EXISTS (SELECT b.userid ",	
                                " FROM inv_permxbod b ",
                                " WHERE b.userid = '", username CLIPPED, "'", -- USER 
                                " AND b.codbod = a.codbod) ",
                  " AND ",qrytext CLIPPED,
                  cndanl CLIPPED,
                  " ORDER BY a.codemp,a.codsuc,a.codbod,a.fecemi DESC,a.lnktra DESC" 

  -- Contando registros
  LET totreg = librut003_NumeroRegistros(qrypart)

  -- Declarando el cursor 
  PREPARE estqbe001 FROM qrypart
  DECLARE c_movtos SCROLL CURSOR WITH HOLD FOR estqbe001  
  OPEN c_movtos 
  FETCH FIRST c_movtos INTO w_mae_tra.lnktra
  IF (status = NOTFOUND) THEN
     INITIALIZE w_mae_tra.* TO NULL
     ERROR ""
     CALL fgl_winmessage(
     "Atencion",
     "No existen movimientos con el criterio seleccionado.",
     "stop") 
  ELSE
     ERROR "" 

     -- Desplegando datos 
     LET msg = " - Registros [ ",totreg||" ]"
     CALL invqbx001_EstadoMenu(operacion,msg)
     CALL invqbx001_datos()

     -- Fetchando movimientos 
     MENU "Movimientos" 
      BEFORE MENU 
       -- Verificando tipo de operacion 
       CASE (operacion)
        WHEN 1 HIDE OPTION "Anular"

               -- Verificando permiso para modificar 
               IF NOT seclib001_accesos(progname,4,username) THEN
                  HIDE OPTION "Modificar"
               END IF

        WHEN 2 HIDE OPTION "Imprimir" 
               HIDE OPTION "Modificar"
       END CASE 
      COMMAND "Siguiente"
       "Visualiza el siguiente movimiento en la lista."
       FETCH NEXT c_movtos INTO w_mae_tra.lnktra 
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas movimientos siguientes en lista.", 
           "information")
           FETCH LAST c_movtos INTO w_mae_tra.lnktra
       END IF 

       -- Desplegando datos 
       CALL invqbx001_datos()

      COMMAND "Anterior"
       "Visualiza el movimientos anterior en la lista."
       FETCH PREVIOUS c_movtos INTO w_mae_tra.lnktra
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas movimientos anteriores en lista.", 
           "information")
           FETCH FIRST c_movtos INTO w_mae_tra.lnktra
        END IF

        -- Desplegando datos 
        CALL invqbx001_datos()

      COMMAND "Primero" 
       "Visualiza el primer movimiento en la lista."
       FETCH FIRST c_movtos INTO w_mae_tra.lnktra
        -- Desplegando datos 
        CALL invqbx001_datos()

      COMMAND "Ultimo" 
       "Visualiza el ultimo movimiento en la lista."
       FETCH LAST c_movtos INTO w_mae_tra.lnktra
        -- Desplegando datos
        CALL invqbx001_datos()

      COMMAND "Anular"
       "Permite anular el movimiento actual en pantalla."
       -- Verificando si movimiento ya fue anulado
       IF (w_mae_tra.estado="A") THEN
          CALL fgl_winmessage(
          "Atencion",
          "Movimiento ya fue anulado.",
          "stop")
          CONTINUE MENU 
       END IF 

       -- Anulando movimiento
       IF invqbx001_anular() THEN
          CONTINUE MENU 
       END IF 

       -- Desplegando datos
       CALL invqbx001_datos()

      COMMAND "Modificar"
       "Permitie modificar el movimiento actual en pantalla."
       -- Verificando si movimiento ya fue anulado
       IF (w_mae_tra.estado="A") THEN
          CALL fgl_winmessage(
          "Atencion",
          "Movimiento ya fue anulado. \n No puede modificarse.",
          "stop")
          CONTINUE MENU 
       END IF 

       -- Verificando si movimiento no fue originado desde el ingreso de movimientos
       IF (w_mae_tra.numord=0) THEN
        CALL fgl_winmessage(
        "Atencion",
        "Movimiento no puede modificarse. \nNo fue originado desde el ingreso de movimientos.",
        "stop")
        CONTINUE MENU 
       END IF 

       -- Verificando fecha del movimiento
       IF (w_mae_tra.fecemi<(TODAY-maxdiamod)) THEN
        CALL fgl_winmessage(
        "Atencion",
        "Movimiento no puede modificarse, fecha de emision con mas de ("||
        maxdiamod||") hacia atras.", 
        "stop")
        CONTINUE MENU 
       END IF 

       -- Recargando productos
       CALL invqbx001_detalle(1)

       -- Modificando movimiento 
       CALL inving001_movimientos(2)

       -- Desplegando datos
       CALL invqbx001_datos()

      ON ACTION delete 

       -- Verificando fecha de eliminacion 
       IF (w_mae_tra.fecemi<(TODAY-maxdiaeli)) THEN
          CALL fgl_winmessage(
          "Atencion",
          "Maximo de "||maxdiaeli||" dias para eliminar excedido, VERIFICA. \n"||
          "Movimiento no puede eliminarse.",
          "stop")
          CONTINUE MENU
       END IF

       -- Verificando si movimiento no fue originado desde el ingreso de movimientos
       IF (w_mae_tra.numord=0) THEN
        CALL fgl_winmessage(
        "Atencion",
        "Movimiento no puede eliminarse. \nNo fue originado desde el ingreso de movimientos.",
        "stop")
        CONTINUE MENU 
       END IF 

       -- Eliminando movimiento 
       IF invqbx001_EliminarMovimiento() THEN
          EXIT MENU
       END IF

      COMMAND "Productos"
       "Permite visualizar el detalle completo de productos del movimiento."
       CALL invqbx001_detalle(0)

      COMMAND "Imprimir" 
       "Permite imprimir el movimiento actual en pantalla." 
       -- Verificando si movimiento ya fue anulado
       IF (w_mae_tra.estado="A") THEN
          CALL fgl_winmessage(
          "Atencion",
          "Movimiento ya fue anulado. \nSera impreso como ANULADO.",
          "stop")
       END IF

       -- Imprimiendo reporte
       ERROR "Imprimiendo reporte ... por favor espere ..."
       LET reimpresion = TRUE 
       CALL invrpt001_reportemov()
       ERROR "" 

      COMMAND "Consultar" 
       "Regresa a la pantalla de seleccion."
       EXIT MENU

      ON ACTION cancel 
       LET loop = FALSE 
       EXIT MENU 

      COMMAND KEY(F4,CONTROL-E)
       LET loop = FALSE 
       EXIT MENU
     END MENU
   END IF     
  CLOSE c_movtos
 END WHILE

 -- Inicializando datos 
 CALL inving001_inival(1)

 -- Desplegando estado del menu 
 CALL invqbx001_EstadoMenu(0,"")
END FUNCTION 

-- Subrutina para desplegar los datos del registro 

FUNCTION invqbx001_datos()
 DEFINE existe SMALLINT

 -- Desplegando datos 
 CLEAR FORM

 -- Obteniendo datos del movimiento
 CALL librut003_bmovimiento(w_mae_tra.lnktra)
 RETURNING w_mae_tra.*,existe

 -- Obteniendo datos de la empresa
 CALL librut003_bempresa(w_mae_tra.codemp)
 RETURNING w_mae_emp.*,existe

 -- Obteniendo datos de la sucursal
 CALL librut003_bsucursal(w_mae_tra.codsuc)
 RETURNING w_mae_suc.*,existe

 -- Obteniendo datos de la bodega
 CALL librut003_bbodega(w_mae_tra.codbod)
 RETURNING w_mae_bod.*,existe

 -- Obteniendo datos del tipo de movimiento
 CALL librut003_btipomovimiento(w_mae_tra.tipmov)
 RETURNING w_mae_tip.*,existe 

 -- Obtiendo nombre del estado
 LET nomest = librut001_valores(w_mae_tra.estado)

 -- Desplegando datos del movimiento 
 CLEAR FORM 
 DISPLAY BY NAME w_mae_tra.codbod,w_mae_tra.tipmov, 
                 w_mae_tra.numrf1,w_mae_tra.numrf2, 
                 w_mae_tra.aniotr,w_mae_tra.mestra,
                 w_mae_tra.observ,w_mae_tra.estado,
                 w_mae_tra.fecemi,w_mae_tra.fecsis,
                 w_mae_tra.horsis,w_mae_tra.userid,
                 w_mae_emp.nomemp,w_mae_suc.nomsuc,
                 nomest          ,w_mae_tra.motanl,
                 w_mae_tra.lnktra

 -- Escondiendo campos de acuerdo al tipo de movimiento 
 CASE (w_mae_tra.tipope) 
  WHEN 1 DISPLAY BY NAME w_mae_tra.codori
  WHEN 0 DISPLAY BY NAME w_mae_tra.coddes
 END CASE 

 -- Seleccionando detalle del movimiento 
 CALL invqbx001_detalle(1)
END FUNCTION 

-- Subrutina para seleccionar datos del detalle 

FUNCTION invqbx001_detalle(dsp)
 DEFINE existe,dsp SMALLINT

 -- Inicializando vector de productos
 CALL inving001_inivec()
 
 -- Seleccionando detalle del movimiento 
 DECLARE cdet CURSOR FOR
 SELECT x.codabr,	
        y.despro,
        x.codepq,
        z.nomepq,
        x.canepq, 
        x.canuni,
        x.preuni,
        x.porisv,
        x.totisv, 
        x.totpro,
        "", 
        x.correl,
        y.cditem
  FROM  inv_dtransac x, inv_products y, outer inv_epqsxpro z
  WHERE (x.lnktra = w_mae_tra.lnktra)
    AND (y.cditem = x.cditem)
    AND (z.cditem = x.cditem)
    AND (z.codepq = x.codepq) 
  ORDER BY x.correl 

  LET totlin = 1 
  FOREACH cdet INTO v_products[totlin].*
   -- Acumulando contador
   LET totlin = (totlin+1) 
  END FOREACH
  CLOSE cdet
  FREE  cdet
  LET totlin = (totlin-1)  

  -- Despelgando datos del detalle
  DISPLAY ARRAY v_products TO s_products.* 
   ATTRIBUTE(ACCEPT=FALSE) 
   BEFORE DISPLAY
    -- Verificando si es solo display
    IF dsp THEN 
       EXIT DISPLAY
    END IF 
  END DISPLAY 

  -- Desplegando totales
  CALL inving001_totdet() 
END FUNCTION 

-- Subrutina para anular el movimiento 

FUNCTION invqbx001_anular()
 DEFINE loop,regreso SMALLINT,
        msg          STRING 

 -- Ingresando motivo de la anulacion
 LET loop = TRUE
 WHILE loop
  LET regreso = FALSE
  INPUT BY NAME w_mae_tra.motanl  
   ATTRIBUTES (UNBUFFERED) 
   ON ACTION cancel
    -- Salida
    LET regreso = TRUE
    LET loop    = FALSE
    LET w_mae_tra.motanl = NULL
    CLEAR motanl 
    EXIT INPUT 
   AFTER FIELD motanl
    -- Verificando motivo
    IF (LENGTH(w_mae_tra.motanl)<=0) THEN
       ERROR "Error: debe ingresarse el motivo de la anulacion, VERIFICA."
       NEXT FIELD motanl
    END IF 
  END INPUT 
  IF regreso THEN     
     EXIT WHILE
  END IF 

  -- Verificando anulacion
  IF NOT librut001_yesornot("Confirmacion",
                            "Desea Anular el Movimiento ?",
                            "Si",
                            "No",
                            "question") THEN
     LET regreso = TRUE 
     EXIT WHILE
  END IF 

  -- Anulando movimiento 
  LET loop = FALSE 
  ERROR " Anulando Movimiento ..." ATTRIBUTE(CYAN)

  -- Iniciando Transaccion
  BEGIN WORK

   -- Marcando maestro del movimiento como anulado 
   SET LOCK MODE TO WAIT 
   DECLARE c_anul CURSOR FOR
   SELECT a.*
    FROM  inv_mtransac a
    WHERE (a.lnktra = w_mae_tra.lnktra) 
    FOR UPDATE 
    FOREACH c_anul 
     UPDATE inv_mtransac
     SET    inv_mtransac.estado = "A",
            inv_mtransac.motanl = w_mae_tra.motanl, 
            inv_mtransac.usranl = USER,
            inv_mtransac.fecanl = CURRENT,
            inv_mtransac.horanl = CURRENT
     WHERE CURRENT OF c_anul
    END FOREACH
    CLOSE c_anul
    FREE  c_anul

   -- Finalizando Transaccion
   COMMIT WORK

   ERROR ""
 END WHILE 

 RETURN regreso 
END FUNCTION

-- Subrutina para eliminar el movimiento 

FUNCTION invqbx001_EliminarMovimiento()
 DEFINE opc,elimina SMALLINT,
        msg         STRING

 -- Verificando operacion
 LET msg = "Desea Eliminar el Movimiento ?"
 IF librut001_yesornot("Confirmacion",msg,"Si","No","question") THEN
  LET elimina = TRUE

  -- Iniciando Transaccion
  BEGIN WORK

   -- Eliminando movimiento 
   SET LOCK MODE TO WAIT
   DELETE FROM inv_mtransac
   WHERE (inv_mtransac.lnktra = w_mae_tra.lnktra)

  -- Finalizando Transaccion
  COMMIT WORK

  LET msg = "Movimiento Eliminado."
 ELSE
  LET elimina = FALSE
  LET msg = "Eliminacion Cancelada."
 END IF

 -- Desplegando mensaje de accion
 CALL fgl_winmessage("Atencion",msg,"information")

 RETURN elimina
END FUNCTION

-- Subrutina para desplegar los estados del menu

FUNCTION invqbx001_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("group1","Datos del Movimiento - MENU")
  WHEN 1 CALL librut001_dpelement("group1","Datos del Movimiento - CONSULTAR"||msg)
  WHEN 2 CALL librut001_dpelement("group1","Datos del Movimiento - ANULAR"||msg)
  WHEN 3 CALL librut001_dpelement("group1","Datos del Movimiento - MODIFICAR")
  WHEN 4 CALL librut001_dpelement("group1","Datos del Movimiento - EMITIR")
 END CASE
END FUNCTION
