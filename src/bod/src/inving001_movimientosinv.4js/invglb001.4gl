{ 
Fecha    : diciembre 2006 
Programo : Mynor Ramirez 
Objetivo : Programa de variables globales movimientos de inventario
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname  = "inving001"
DEFINE w_mae_pro   RECORD LIKE inv_products.*,
       w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_suc   RECORD LIKE glb_sucsxemp.*,
       w_mae_tra   RECORD LIKE inv_mtransac.*,
       w_mae_act   RECORD LIKE inv_mtransac.*,
       w_mae_tip   RECORD LIKE inv_tipomovs.*,
       w_mae_bod   RECORD LIKE inv_mbodegas.*,
       w_mae_prv   RECORD LIKE inv_provedrs.*,
       w_mae_cli   RECORD LIKE fac_clientes.*,
       v_products  DYNAMIC ARRAY OF RECORD
        cditem     LIKE inv_products.codabr, 
        dsitem     CHAR(50), 
        codepq     LIKE inv_dtransac.codepq, 
        nomepq     VARCHAR(40,1), 
        canepq     LIKE inv_dtransac.canepq,
        canuni     LIKE inv_dtransac.canuni,
        preuni     LIKE inv_dtransac.preuni,
        porisv     LIKE inv_dtransac.porisv,
        totisv     LIKE inv_dtransac.totisv, 
        totpro     LIKE inv_dtransac.totpro,
        rellen     CHAR(1) 
       END RECORD, 
       totuni      DEC(14,2),
       totval      DEC(14,2),
       totimp      DEC(14,2), 
       totpdt      SMALLINT, 
       reimpresion SMALLINT,
       totlin      SMALLINT,
       nomprv      STRING,   
       nomest      STRING,   
       nomcli      STRING, 
       b           ui.ComboBox,
       cba         ui.ComboBox,
       w_lnktra    INT,
       w           ui.Window,
       f           ui.Form,
       maxdiamod   SMALLINT, 
       maxdiaeli   SMALLINT, 
       tipmovcmp   INTEGER, 
       username    VARCHAR(15)
END GLOBALS
