{
Programo : Mynor Ramirez
Objetivo : Programa de ingreso de movimientos inventario.
}

-- Definicion de variables globales

GLOBALS "invglb001.4gl"
CONSTANT PermiteRepetidos = FALSE 
CONSTANT TipoCosteo=1 
DEFINE fechasmesactual SMALLINT,
       grabaeimprime   SMALLINT, 
       regreso         SMALLINT,
       existe          SMALLINT,
       msg             STRING,
       fechainicioinv  DATE, 
       result          INT 

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarMovimientosInv")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN 
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("movimientos")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de opciones
 CALL inving001_menu()
END MAIN

-- Subutina para el menu de movimientos de inventario 

FUNCTION inving001_menu()
 DEFINE regreso    SMALLINT, 
        wpais      VARCHAR(255), 
        titulo     STRING 

 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing001a AT 5,2  
  WITH FORM "inving001a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ 

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header(progname,wpais,1)
  CALL f.setElementText("labela","Observaciones:")
  CALL f.setElementText("labelb","Motivo de la Anulacion")
  CALL f.setElementText("labeld","Numero de Movimiento")

  -- Escondiendo motivo de anulacion 
  CALL f.setFieldHidden("formonly.motanl",1)
  CALL f.setElementHidden("labelb",1)

  -- Inicializando datos 
  CALL inving001_inival(1)

  -- Obteniendo parametro de movimiento de compras 
  CALL librut003_parametros(13,5)
  RETURNING existe,tipmovcmp 
  IF NOT existe THEN
    CALL fgl_winmessage(
    "Atencion:",
    "Parametro de tipo de movimiento de compras de inventario no definido.\n"||
    "Debe definirse antes de poder registrar movimientos.",
    "stop")
    RETURN
  END IF 

  -- Obteniendo parametro si el ingreso valida fechas solo del mes
  CALL librut003_parametros(13,10)
  RETURNING existe,fechasmesactual
  IF NOT existe THEN LET fechasmesactual = 0 END IF 

  -- Obteniendo parametro si se desea impresion despues de grabar 
  CALL librut003_parametros(13,11)
  RETURNING existe,grabaeimprime 
  IF NOT existe THEN LET grabaeimprime = 1 END IF 

  -- Obteniendo parametro de maximo numero de dias para modificar un movimiento
  CALL librut003_parametros(13,12)
  RETURNING existe,maxdiamod
  IF NOT existe THEN
   CALL fgl_winmessage(
   "Atencion:",
   "Parametro maximo de dias para modificar movimientos inventario no definido.\n"||
   "Debe definirse antes de poder modificar movinientos.",
   "stop")
   RETURN
  END IF

  -- Obteniendo parametro de maximo numero de dias para eliminar un movimiento
  CALL librut003_parametros(13,13)
  RETURNING existe,maxdiaeli
  IF NOT existe THEN
   CALL fgl_winmessage(
   "Atencion:",
   "Parametro maximo de dias para eliminar movimientos inventario no definido.\n"||
   "Debe definirse antes de poder eliminar movimientos.",
   "stop")
   RETURN
  END IF 

  -- Menu de opciones
  MENU "Opciones" 
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Consultar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Anular
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Anular"
    END IF
   COMMAND "Consultar" 
    " Consulta de movimientos existentes."
    CALL invqbx001_movimientos(1)
   COMMAND "Ingresar" 
    " Ingreso de nuevos movimientos."
    CALL inving001_movimientos(1) 
   COMMAND "Anular"
    " Anulacion de movimientos existentes."
    CALL invqbx001_movimientos(2)
   COMMAND "Salir"
    " Abandona el menu de movimientos." 
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU

 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso de los datos generales del movimiento (encabezado) 

FUNCTION inving001_movimientos(operacion)
 DEFINE w_mae_art         RECORD LIKE inv_products.*,
        retroceso         SMALLINT,
        operacion         SMALLINT,
        loop,existe,i     SMALLINT,
        conteo            INTEGER,  
        w_lnktra          INTEGER,
        fecultabs         DATE 

 -- Escondiendo motivo de anulacion 
 CALL f.setFieldHidden("formonly.motanl",1)
 CALL f.setElementHidden("labelb",1)

 -- Llenando combobox de tipo de movimiento 
 CALL librut003_cbxtipomovxusuario(username) 

 -- Inicio del loop
 IF (operacion=1) THEN
    CALL invqbx001_EstadoMenu(4,"")
    LET retroceso = FALSE
 ELSE
    CALL invqbx001_EstadoMenu(3,"") 
    LET retroceso = TRUE
 END IF 

 LET loop  = TRUE
 WHILE loop   

  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos 
     CALL inving001_inival(1) 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_tra.codbod,
                w_mae_tra.tipmov,
                w_mae_tra.fecemi, 
                w_mae_tra.codori, 
                w_mae_tra.coddes, 
                w_mae_tra.numrf1, 
                w_mae_tra.numrf2, 
                w_mae_tra.observ WITHOUT DEFAULTS
    ATTRIBUTES(UNBUFFERED) 

   ON ACTION cancel 
    -- Salida 
    IF (operacion=1) THEN  -- Ingreso 
     IF INFIELD(codbod) THEN
        LET loop = FALSE
        CALL inving001_inival(1)
        EXIT INPUT
     ELSE
        CALL Dialog.SetFieldActive("codbod",TRUE) 
        CALL Dialog.SetFieldActive("tipmov",TRUE) 
        CALL inving001_inival(1)
        NEXT FIELD codbod
     END IF 
    ELSE                   -- Modifidacion 
     LET loop = FALSE 
     EXIT INPUT 
    END IF 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON CHANGE codbod 
    -- Obteniendo datos de la bodega 
    CALL librut003_bbodega(w_mae_tra.codbod) 
    RETURNING w_mae_bod.*,existe 

    -- Asignando datos de empresa y sucursal de la bodega
    LET w_mae_tra.codemp = w_mae_bod.codemp 
    LET w_mae_tra.codsuc = w_mae_bod.codsuc 

    -- Obteniendo datos de la empresa 
    CALL librut003_bempresa(w_mae_bod.codemp)
    RETURNING w_mae_emp.*,existe 
    -- Obteniendo datos de la sucursal 
    CALL librut003_bsucursal(w_mae_bod.codsuc)
    RETURNING w_mae_suc.*,existe 
   
    -- Desplegando datos de la empresa y sucursal 
    DISPLAY BY NAME w_mae_emp.nomemp,w_mae_suc.nomsuc

   ON CHANGE tipmov 
    -- Obteniendo datos del tipo de movimiento
    CALL librut003_btipomovimiento(w_mae_tra.tipmov)
    RETURNING w_mae_tip.*,existe 
  
    -- Habilitando y deshabilitando campos dependiendo de la operacion del tipo de
    -- movimiento
    CASE (w_mae_tip.tipope)
     WHEN 1 CALL Dialog.SetFieldActive("codori",TRUE)    -- Cargos
            CALL Dialog.SetFieldActive("coddes",FALSE) 
            LET w_mae_tra.coddes = NULL
            LET w_mae_tra.nomdes = NULL
            CLEAR coddes
     WHEN 0 CALL Dialog.SetFieldActive("coddes",TRUE)    -- Abonos 
            CALL Dialog.SetFieldActive("codori",FALSE) 
            LET w_mae_tra.codori = NULL
            LET w_mae_tra.nomori = NULL
            CLEAR codori
    END CASE 

   ON CHANGE fecemi
    -- Asignando datos al anio y al mes
    LET w_mae_tra.aniotr = YEAR(w_mae_tra.fecemi) 
    LET w_mae_tra.mestra = MONTH(w_mae_tra.fecemi) 
    DISPLAY BY NAME w_mae_tra.aniotr,w_mae_tra.mestra 

   BEFORE FIELD codbod 
    -- Verificando si es regreso se mueve al campo de fecha ya que los campos 
    -- de bodega y tipo de movimiento no se pueden modificar si ya se ingresaron 
    -- otros datos
    IF retroceso THEN
       CALL Dialog.SetFieldActive("codbod",FALSE) 
       CALL Dialog.SetFieldActive("tipmov",FALSE) 
       LET retroceso = FALSE 
    END IF 

    -- Verificando tipo de operacion
    CASE (operacion)
     WHEN 1 -- Ingreso 

      -- Deshabilitando campos 
      CALL Dialog.SetFieldActive("codori",FALSE) 
      CALL Dialog.SetFieldActive("coddes",FALSE) 

     WHEN 2 -- Modificacion 

      -- Habilitando y deshabilitando campos dependiendo de la operacion del tipo de
      -- movimiento
      CASE (w_mae_tip.tipope)
       WHEN 1 CALL Dialog.SetFieldActive("codori",TRUE)    -- Cargos
              CALL Dialog.SetFieldActive("coddes",FALSE)
       WHEN 0 CALL Dialog.SetFieldActive("coddes",TRUE)    -- Abonos
              CALL Dialog.SetFieldActive("codori",FALSE)
      END CASE
    END CASE 

   AFTER FIELD codbod
    -- Verificando bodega
    IF w_mae_tra.codbod IS NULL THEN
       ERROR "Error: bodega invalida, VERIFICA."
       NEXT FIELD codbod 
    END IF 

   AFTER FIELD tipmov
    -- Verificando tipo de movimiento
    IF w_mae_tra.tipmov IS NULL THEN
       ERROR "Error: tipo de movimiento invalido, VERIFICA." 
       NEXT FIELD tipmov
    END IF 

    -- Obteniendo datos del tipo de movimiento
    INITIALIZE w_mae_tip.* TO NULL 
    CALL librut003_btipomovimiento(w_mae_tra.tipmov)
    RETURNING w_mae_tip.*,existe 

   BEFORE FIELD codori
    -- Cargando origenes
    CALL librut003_CbxOrigenesMovtos() 

   ON CHANGE codori  
    -- Obteniendo datos del origen 
    INITIALIZE w_mae_prv.* TO NULL
    CALL librut003_bproveedor(w_mae_tra.codori)
    RETURNING w_mae_prv.*,existe 
    LET w_mae_tra.nomori = w_mae_prv.nomprv

   AFTER FIELD codori
    -- Verificando si tipo de movimiento es un cargo
    IF (w_mae_tip.tipope=1) THEN
       IF w_mae_tra.codori IS NULL THEN
          ERROR "Error: origen invalido, VERIFICA."
          NEXT FIELD codori 
       END IF 
    END IF 

    -- Obteniendo datos del origen 
    INITIALIZE w_mae_prv.* TO NULL
    CALL librut003_bproveedor(w_mae_tra.codori)
    RETURNING w_mae_prv.*,existe 
    LET w_mae_tra.nomori = w_mae_prv.nomprv

   BEFORE FIELD coddes
    -- Cargando destinos
    CALL librut003_CbxDestinosMovtos() 

   ON CHANGE coddes
    -- Obteniendo datos del destino
    INITIALIZE w_mae_cli.* TO NULL
    CALL librut003_bcliente(w_mae_tra.coddes)
    RETURNING w_mae_cli.*,existe 
    LET w_mae_tra.nomdes = w_mae_cli.nomcli 

   AFTER FIELD coddes
    -- Verificando si tipo de movimiento es un abono 
    IF (w_mae_tip.tipope=0) THEN
       IF w_mae_tra.coddes IS NULL THEN
          ERROR "Error: destino invalido, VERIFICA."
          NEXT FIELD coddes 
       END IF 
    END IF 

    -- Obteniendo datos del destino
    INITIALIZE w_mae_cli.* TO NULL
    CALL librut003_bcliente(w_mae_tra.coddes)
    RETURNING w_mae_cli.*,existe 
    LET w_mae_tra.nomdes = w_mae_cli.nomcli 
    
   AFTER FIELD fecemi
    -- Verificando fecha de emision 
    IF w_mae_tra.fecemi IS NULL OR 
       (w_mae_tra.fecemi >TODAY) THEN
       LET w_mae_tra.fecemi = TODAY 
       DISPLAY BY NAME w_mae_tra.fecemi

       -- Asignando datos al anio y al mes
       LET w_mae_tra.aniotr = YEAR(w_mae_tra.fecemi) 
       LET w_mae_tra.mestra = MONTH(w_mae_tra.fecemi) 
       DISPLAY BY NAME w_mae_tra.aniotr,w_mae_tra.mestra 
    END IF

    -- Verificando que fecha de movimiento no se menor a ultima fecha de absorcion
   { LET fecultabs = librut003_FechaUltimaAbsorcion(w_mae_tra.codemp,
                                                   w_mae_tra.codsuc,
                                                   w_mae_tra.codbod)
    IF w_mae_tra.fecemi<=fecultabs THEN
       CALL fgl_winmessage(
       "Atencion:",
       "Fecha de movimiento debe ser mayor a ultima fecha absorcion.\n"||
       "Fecha de Ultima Absorcion [ "||fecultabs||" ].",
       "stop")
       NEXT FIELD fecemi
    END IF} 

    -- Verificando parametro para aceptacion de fechas solo del mes actual 
    IF fechasmesactual THEN
     -- Verificando que fecha de emision sea solo del mes
     IF EXTEND(w_mae_tra.fecemi,YEAR TO MONTH)!=EXTEND(CURRENT,YEAR TO MONTH) THEN
      CALL fgl_winmessage(
      "Atencion:",
      "Solo pueden trabajarse movimientos del mes actual.",
      "stop")
      NEXT FIELD fecemi
     END IF 
    END IF 

   AFTER FIELD numrf1
    -- Verificando documento de referencia
    IF (LENGTH(w_mae_tra.numrf1)<=0) THEN 
       ERROR "Error: numero de documento de referencia invalido, VERIFICA."
       NEXT FIELD numrf1 
    END IF 

    -- Verificando si tipo de movimiento es un cargo
    IF (w_mae_tip.tipope=1) THEN
     -- Verificando si tipo de documento es unico 
     IF w_mae_tip.tipmov=tipmovcmp THEN
      -- Verificando que no exista otro documento con el mismo numero
      IF w_mae_tra.lnktra IS NULL THEN 
         LET w_lnktra = 0 
      ELSE 
         LET w_lnktra = w_mae_tra.lnktra
      END IF 

      SELECT COUNT(*)
       INTO  conteo
       FROM  inv_mtransac a  
       WHERE a.lnktra != w_lnktra 
         AND a.codemp  = w_mae_tra.codemp 
         AND a.codsuc  = w_mae_tra.codsuc 
         AND a.codbod  = w_mae_tra.codbod 
         AND a.tipmov  = w_mae_tra.tipmov
         AND a.codori  = w_mae_tra.codori 
         AND a.numrf1  = w_mae_tra.numrf1 
	 AND a.estado  = "V" 
       IF conteo>0 THEN
          CALL fgl_winmessage(
          " Atencion:",
          " Numero de documento "||w_mae_tra.numrf1||
          " ya existe registrado para "||w_mae_tra.nomori||". \n"||
          " VERIFICA que el documento no exista para poder registrarlo.",
          "stop")
          NEXT FIELD numrf1 
       END IF 
     END IF 
    END IF 

   AFTER INPUT
    -- Verificando nulos
    IF w_mae_tra.codbod IS NULL THEN
      NEXT FIELD codbod
    END IF
    IF w_mae_tra.tipmov IS NULL THEN
      NEXT FIELD tipmov
    END IF
    IF w_mae_tra.fecemi IS NULL THEN
      NEXT FIELD fecemi
    END IF
    
    CASE (w_mae_tip.tipope) 
     WHEN 1 -- Cargo    
      IF w_mae_tra.codori IS NULL THEN
         CALL Dialog.SetFieldActive("codori",TRUE) 
         NEXT FIELD codori
      END IF 
     WHEN 0 -- Abono    
      IF w_mae_tra.coddes IS NULL THEN
         CALL Dialog.SetFieldActive("coddes",TRUE) 
         NEXT FIELD coddes
      END IF 
    END CASE  

    IF (LENGTH(w_mae_tra.numrf1)<0) THEN 
       NEXT FIELD numrf1 
    END IF 
  END INPUT 
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Ingresando detalle del movimiento 
  LET retroceso = inving001_detingreso(0,operacion)

  -- Verificando operacion
  IF (operacion=2) THEN
     EXIT WHILE 
  END IF 
 END WHILE

 -- Inicializando datos 
 IF (operacion=1) THEN 
    CALL invqbx001_EstadoMenu(0,"")
    CALL inving001_inival(1) 
 ELSE 
    CALL invqbx001_EstadoMenu(1," ")
 END IF 
END FUNCTION

-- Subutina para el ingreso del detalle de productos del movimiento (detalle) 

FUNCTION inving001_detingreso(modifica,operacion)
 DEFINE w_mae_art    RECORD LIKE inv_products.*,
        canexi       LIKE inv_proenbod.exican,
        canval       LIKE inv_proenbod.exival, 
        wcanepq      LIKE inv_epqsxpro.cantid, 
        loop,arr,scr SMALLINT,
        opc,repetido SMALLINT, 
        retroceso    SMALLINT, 
        operacion    SMALLINT, 
        modifica,i   SMALLINT,
        conteo       INTEGER, 
        lastkey      INTEGER, 
        xcditem      STRING

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop
  -- Ingresando productos
  INPUT ARRAY v_products WITHOUT DEFAULTS FROM s_products.*
   ATTRIBUTE(INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,
             UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION accept
    -- Salida
    EXIT INPUT 

   ON ACTION cancel
    -- Regreso
    LET loop      = FALSE
    LET retroceso = TRUE
    EXIT INPUT  

   ON ACTION listaproducto
    -- Asignando posicion actual
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Seleccionado lista de productos
    CALL librut002_formlist("Consulta de Productos",
                            "Producto",
                            "Descripcion del Producto",
                            "Precio Unitario",
                            "codabr",
                            "despro",
                            "presug",
                            "inv_products",
                            "estado=1 and status=1",
                            2,
                            1,
                            1)
    RETURNING v_products[arr].cditem,v_products[arr].dsitem,regreso
    IF regreso THEN
       NEXT FIELD cditem
    ELSE
       -- Asignando descripcion
       DISPLAY v_products[arr].cditem TO s_products[scr].cditem
       DISPLAY v_products[arr].dsitem TO s_products[scr].dsitem
    END IF

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   BEFORE INPUT
    -- Desabilitando botones de append 
    CALL DIALOG.setActionHidden("append",TRUE)

    -- Verificando si movimiento no es valorizado
    IF NOT w_mae_tip.hayval THEN
       CALL DIALOG.setFieldActive("preuni",FALSE)
    ELSE 
       CALL DIALOG.setFieldActive("preuni",TRUE)
    END IF

   BEFORE ROW
    -- Asignando total de lineas
    LET totlin = ARR_COUNT()

   BEFORE FIELD cditem
    -- Habilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",TRUE)

   AFTER FIELD cditem
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) THEN
       IF v_products[arr].cditem IS NULL THEN
          NEXT FIELD cditem
       END IF 
    END IF

   BEFORE FIELD canepq 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Obteniendo lista de productos
    IF v_products[arr].cditem IS NULL THEN 
     -- Seleccionado lista de productos
     CALL librut002_formlist("Consulta de Productos",
                             "Producto",
                             "Descripcion del Producto",
                             "Precio Unitario",
                             "codabr",
                             "despro",
                             "presug",
                             "inv_products",
                             "estado=1 and status=1",
                             2,
                             1,
                             1)
     RETURNING v_products[arr].cditem,v_products[arr].dsitem,regreso
     IF regreso THEN
        NEXT FIELD cditem
     ELSE 
        -- Asignando descripcion
        DISPLAY v_products[arr].cditem TO s_products[scr].cditem 
        DISPLAY v_products[arr].dsitem TO s_products[scr].dsitem 
     END IF 
    END IF 

    -- Verificando producto
    IF (v_products[arr].cditem <=0) THEN 
       CALL fgl_winmessage(
       " Atencion:",
       " Producto invalido VERIFICA.",
       "stop")
       INITIALIZE v_products[arr].cditem THRU v_products[arr].totpro TO NULL
       CLEAR s_products[scr].cditem,s_products[scr].dsitem,
             s_products[scr].nomepq,s_products[scr].canepq,
             s_products[scr].canuni,s_products[scr].preuni,
             s_products[scr].totpro 
       NEXT FIELD cditem 
    END IF 

    -- Verificando si el producto existe
    INITIALIZE w_mae_art.* TO NULL
    CALL librut003_bproductoabr(v_products[arr].cditem)
    RETURNING w_mae_art.*,existe
    IF NOT existe THEN
       CALL fgl_winmessage(
       " Atencion:",
       " Producto no registrado VERIFICA.",
       "stop")
       INITIALIZE v_products[arr].cditem THRU v_products[arr].totpro TO NULL
       CLEAR s_products[scr].cditem,s_products[scr].dsitem,
             s_products[scr].nomepq,s_products[scr].canepq,
             s_products[scr].canuni,s_products[scr].preuni,
             s_products[scr].totpro 
       NEXT FIELD cditem 
    END IF 

    -- Asignando descripcion
    LET v_products[arr].dsitem = w_mae_art.despro 
    DISPLAY v_products[arr].dsitem TO s_products[scr].dsitem 

    -- Verificando si movimiento es compra asigna porcentaje de impuesto 
    -- si el producto paga impuesto 
    LET v_products[arr].porisv = 0 
    IF w_mae_tra.tipmov=tipmovcmp THEN
       LET v_products[arr].porisv = w_mae_art.porisv 
    END IF 

    -- Verificando productos repetidos
    IF NOT PermiteRepetidos THEN 
     LET repetido = FALSE
     FOR i = 1 TO totlin
      IF v_products[i].cditem IS NULL THEN
         CONTINUE FOR
      END IF

      -- Verificando producto
      IF (i!=arr) THEN 
       IF (v_products[i].cditem = v_products[arr].cditem) THEN
          LET repetido = TRUE
          EXIT FOR
       END IF 
      END IF
     END FOR

     -- Si hay repetido
     IF repetido THEN
        LET msg = "Producto ya registrado en el detalle. "||
                  "Linea (",i USING "<<<",") \nVERIFICA." 
        CALL fgl_winmessage(" Atencion",msg,"stop")
        INITIALIZE v_products[arr].cditem THRU v_products[arr].totpro TO NULL
        CLEAR s_products[scr].cditem,s_products[scr].dsitem,
              s_products[scr].nomepq,s_products[scr].canepq,
              s_products[scr].canuni,s_products[scr].preuni,
              s_products[scr].totpro 
        NEXT FIELD cditem 
     END IF
    END IF 

    -- Verificando estado del producto
    IF NOT w_mae_art.estado  THEN
       CALL fgl_winmessage(
       " Atencion",
       " Producto de BAJA no puede utilizarse, VERIFICA.",
       "stop")
       INITIALIZE v_products[arr].cditem THRU v_products[arr].totpro TO NULL
       CLEAR s_products[scr].cditem,s_products[scr].dsitem,
             s_products[scr].nomepq,s_products[scr].canepq,
             s_products[scr].canuni,s_products[scr].preuni,
             s_products[scr].totpro 
       NEXT FIELD cditem 
    END IF 

    -- Verificando estatus del producto
    IF NOT w_mae_art.status THEN
       CALL fgl_winmessage(
       " Atencion",
       " Producto BLOQUEADO no puede utilizarse, VERIFICA.",
       "stop")
       INITIALIZE v_products[arr].cditem THRU v_products[arr].totpro TO NULL
       CLEAR s_products[scr].cditem,s_products[scr].dsitem,
             s_products[scr].nomepq,s_products[scr].canepq,
             s_products[scr].canuni,s_products[scr].preuni,
             s_products[scr].totpro 
       NEXT FIELD cditem 
    END IF 

    -- Cargando empaques del producto 
    -- Verificando si producto tiene mas de un empaque
    -- Verificando si es entrada 
    IF w_mae_tip.tipope=1 THEN 
     SELECT COUNT(*)
      INTO  conteo
      FROM  inv_epqsxpro a
      WHERE (a.cditem = w_mae_art.cditem) 
      IF (conteo=1) THEN
        INITIALIZE v_products[arr].codepq,v_products[arr].nomepq TO NULL 
        SELECT a.codepq,a.nomepq 
         INTO  v_products[arr].codepq,v_products[arr].nomepq
         FROM  inv_epqsxpro a
         WHERE a.cditem = w_mae_art.cditem
         IF (status=NOTFOUND) THEN
            CALL fgl_winmessage(
            " Atencion:",
            " Producto sin empaques definidos, VERIFICA.",
            "stop") 
            INITIALIZE v_products[arr].cditem THRU v_products[arr].totpro TO NULL
            CLEAR s_products[scr].cditem,s_products[scr].dsitem,
                  s_products[scr].nomepq,s_products[scr].canepq,
                  s_products[scr].canuni,s_products[scr].preuni,
                  s_products[scr].totpro 
            NEXT FIELD cditem 
         END IF 
      ELSE 
       LET msg = "Empaques del Producto (",w_mae_art.despro CLIPPED,")"
       CALL librut002_selectlist(msg,
                                 "Codigo",
                                 "Descripcion",
                                 "a.codepq",
                                 "a.nomepq",
                                 "inv_epqsxpro a",
                                 "a.cditem = "||w_mae_art.cditem,
                                 "a.codepq",
                                 TRUE)
       RETURNING v_products[arr].codepq,v_products[arr].nomepq,regreso
       IF regreso THEN
          NEXT FIELD cditem
       ELSE 
          -- Asignando descripcion
          DISPLAY v_products[arr].nomepq TO s_products[scr].nomepq 
       END IF 
      END IF 
    ELSE 
     -- Si es salida 
     INITIALIZE v_products[arr].codepq,v_products[arr].nomepq TO NULL 
     SELECT a.codepq,a.nomepq 
      INTO  v_products[arr].codepq,v_products[arr].nomepq
      FROM  inv_epqsxpro a
      WHERE a.cditem = w_mae_art.cditem
        AND a.codepq = 0 
      IF (status=NOTFOUND) THEN
         CALL fgl_winmessage(
         " Atencion:",
         " Producto sin empaques definidos, VERIFICA.",
         "stop") 
         INITIALIZE v_products[arr].cditem THRU v_products[arr].totpro TO NULL
         CLEAR s_products[scr].cditem,s_products[scr].dsitem,
               s_products[scr].nomepq,s_products[scr].canepq,
               s_products[scr].canuni,s_products[scr].preuni,
               s_products[scr].totpro 
         NEXT FIELD cditem 
      END IF 
    END IF 

    -- Calculando unidades totales
    LET wcanepq = librut003_canempaque(w_mae_art.cditem,v_products[arr].codepq)
    LET v_products[arr].canuni = (v_products[arr].canepq*wcanepq)
    DISPLAY v_products[arr].canuni TO s_products[scr].canuni

    -- Obteniendo ultimo precio de los movimientos
    IF v_products[arr].preuni IS NULL THEN 
     LET v_products[arr].preuni = 
       inving001_UltimoPrecioCompraEpq(w_mae_tra.codemp,
                                       w_mae_tra.codsuc,
                                       w_mae_tra.codbod,
                                       tipmovcmp,
                                       w_mae_tra.codori, 
                                       w_mae_tra.fecemi,
                                       w_mae_art.cditem,
                                       v_products[arr].codepq)
    END IF 
    DISPLAY v_products[arr].preuni TO s_products[scr].preuni

    -- Totalizando productos 
    CALL inving001_totdet()

    -- Deshabilitando la opcion de aceptar
    CALL DIALOG.SetActionActive("accept",FALSE) 

   AFTER FIELD canepq
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR
       (lastkey = FGL_KEYVAL("up")) THEN
       NEXT FIELD canepq 
    END IF

    -- Verificando cantidad de empaque
    IF (v_products[arr].canepq IS NULL) OR
       (v_products[arr].canepq<=0) THEN
       CALL fgl_winmessage(
       " Atencion",
       " Cantidad de empaque del producto invalida, VERIFICA.",
       "stop")
       INITIALIZE v_products[arr].canepq THRU v_products[arr].totpro TO NULL
       CLEAR s_products[scr].canepq,s_products[scr].canuni,
             s_products[scr].preuni,s_products[scr].totpro 
       NEXT FIELD canepq 
    END IF

    -- Calculando unidades totales
    LET wcanepq = librut003_canempaque(w_mae_art.cditem,v_products[arr].codepq)
    LET v_products[arr].canuni = (v_products[arr].canepq*wcanepq)
    DISPLAY v_products[arr].canuni TO s_products[scr].canuni

    -- Obteniendo ultimo precio de los movimientos
    IF v_products[arr].preuni IS NULL THEN 
     LET v_products[arr].preuni = 
       inving001_UltimoPrecioCompraEpq(w_mae_tra.codemp,
                                       w_mae_tra.codsuc,
                                       w_mae_tra.codbod,
                                       tipmovcmp, 
                                       w_mae_tra.codori, 
                                       w_mae_tra.fecemi,
                                       w_mae_art.cditem,
                                       v_products[arr].codepq)
    END IF 
    DISPLAY v_products[arr].preuni TO s_products[scr].preuni

    -- Totalizando productos 
    CALL inving001_totdet()

    -- Chequeando existencia 
    -- Verificando si tipo de operacion del movimiento es de salida
    IF (w_mae_tip.tipope=0) THEN 
     -- Verificando si bodega tiene chequeo de existencia
     IF w_mae_bod.chkexi THEN
      -- Chequeando existencia
      IF (operacion=1) THEN -- Ingreso
       -- Verificando flag de chequea existencia del producto
       IF w_mae_art.chkexi THEN 
        LET canexi = librut003_chkexistencia(w_mae_tra.codemp,w_mae_tra.codsuc,
                                             w_mae_tra.codbod,w_mae_art.cditem)
        IF (v_products[arr].canuni>canexi) THEN 
          LET msg = " Existencia del producto insuficiente, VERIFICA.\n", 
                    " Existencia Actual ( ",canexi USING "<<,<<<,<<&"," )" 

          CALL fgl_winmessage(" Atencion",msg,"stop")
          NEXT FIELD canepq
        END IF 
       END IF 
      END IF 
     END IF 
    END IF 

   AFTER FIELD preuni
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR
       (lastkey = FGL_KEYVAL("up")) THEN
       NEXT FIELD preuni 
    END IF

    -- Verificando cantidad de empaque
    IF (v_products[arr].preuni IS NULL) OR
       (v_products[arr].preuni<=0) THEN
       CALL fgl_winmessage(
       " Atencion",
       " Precio unitario del producto invalida, VERIFICA.",
       "stop")
       INITIALIZE v_products[arr].preuni THRU v_products[arr].totpro TO NULL
       CLEAR s_products[scr].preuni,s_products[scr].totpro 
       NEXT FIELD preuni 
    END IF

    -- Totalizando productos 
    CALL inving001_totdet()

   AFTER INPUT 
    -- Totalizando productos 
    CALL inving001_totdet()

   AFTER ROW,INSERT,DELETE 
    LET totlin = ARR_COUNT()

    -- Totalizando unidades
    CALL inving001_totdet()

   ON ACTION delete 
    -- Eliminando linea seleccionada
    LET arr = ARR_CURR()
    CALL v_products.deleteElement(arr)
    LET totlin = ARR_COUNT()

    -- Totalizando unidades
    CALL inving001_totdet()
    NEXT FIELD cditem

  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Verificando que se ingrese al menos un producto
  IF (totuni<=0) THEN
     CALL fgl_winmessage(
     " Atencion",
     "Debe ingresarse al menos un producto en el detalle del movimiento.",
     "stop")
     CONTINUE WHILE
  END IF

  -- Verificando existencia de productos por cualquier cambio antes de grabar
  -- Verificando si tipo de operacion del movimiento es de salida
  IF (w_mae_tip.tipope=0) THEN
   -- Verificando si bodega tiene chequeo de existencia
   IF w_mae_bod.chkexi THEN
    -- Chequeando existencia
    IF (operacion=1) THEN -- Ingreso
     CALL inving001_existencia() 
     RETURNING xcditem,canexi 
     IF (LENGTH(xcditem)>0) THEN 
      LET msg = " Producto "||
                xcditem CLIPPED||" sin existencia suficiente, VERIFICA.\n",
                " Existencia Actual ( ",canexi USING "<<,<<<,<<&"," )"

      CALL fgl_winmessage(" Atencion",msg,"stop")
      CONTINUE WHILE
     END IF 
    END IF 
   END IF 
  END IF 

  -- Menu de opciones
  LET opc = librut001_menugraba(
          "Confirmacion","Que desea hacer?","Guardar","Modificar","Cancelar","")

  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop      = FALSE
    LET retroceso = FALSE
   WHEN 1 -- Grabando
    LET loop      = FALSE
    LET retroceso = FALSE

    -- Grabando movimiento
    CALL inving001_grabar(operacion)

   WHEN 2 -- Modificando
    LET loop = TRUE 
  END CASE
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subrutina para totalizar el detalle de productos 

FUNCTION inving001_totdet()
 DEFINE i INT

 -- Totalilzando detalle
 LET totuni = 0
 LET totval = 0 
 LET totimp = 0 
 LET totpdt = 0 
 FOR i=1 TO totlin
  IF v_products[i].cditem IS NULL OR
     v_products[i].canepq IS NULL THEN
     CONTINUE FOR
  END IF 

  -- Verificando si producto se ingreso sin empaque o con empaque
  --IF (w_mae_tip.tipope=1) THEN 
   --IF (v_products[i].codepq=0) THEN
    LET v_products[i].totpro = (v_products[i].canuni*v_products[i].preuni) 
   --ELSE
    {IF NOT w_mae_tip.hayval THEN 
     LET v_products[i].totpro = (v_products[i].canuni*v_products[i].preuni) 
    ELSE
     LET v_products[i].totpro = (v_products[i].canepq*v_products[i].preuni) 
    END IF 
  END IF } 

  -- Asignando porcentaje de impuesto 
  LET v_products[i].totisv = 0 

  -- Sumando impuesto al total
  LET v_products[i].totpro = (v_products[i].totpro+v_products[i].totisv) 
  
  -- Acumulando totales 
  LET totuni = (totuni+v_products[i].canuni)
  LET totval = (totval+v_products[i].totpro) 
  LET totimp = (totimp+v_products[i].totisv) 
  LET totpdt = (totpdt+1) 
 END FOR

 -- Desplegando totales 
 DISPLAY BY NAME totval,totimp,totpdt 
END FUNCTION 

-- Subrutina para verificar la existencia de los productos antes de grabar

FUNCTION inving001_existencia()
 DEFINE w_mae_art  RECORD LIKE inv_products.*,
        xcanexi    LIKE inv_proenbod.exican,
        xcditem    STRING, 
        i          SMALLINT

 -- Chequeando existencia
 LET xcditem = NULL
 LET xcanexi = 0 
 FOR i = 1 TO totlin
  IF v_products[i].cditem IS NULL OR  
     v_products[i].canuni IS NULL THEN
     CONTINUE FOR
  END IF 

  -- Obteniendo datos del producto
  INITIALIZE w_mae_art.* TO NULL
  CALL librut003_bproductoabr(v_products[i].cditem)
  RETURNING w_mae_art.*,existe

  -- Chequeando existencia
  -- Verificando si producto chequea existencia
  IF w_mae_art.chkexi THEN 
     LET xcanexi = librut003_chkexistencia(w_mae_tra.codemp,w_mae_tra.codsuc,
                                           w_mae_tra.codbod,w_mae_art.cditem)

     IF (v_products[i].canuni>xcanexi) THEN
        LET xcditem = v_products[i].cditem 
        EXIT FOR   
     END IF 
  END IF
 END FOR 

 RETURN xcditem,xcanexi 
END FUNCTION 

-- Subrutina para grabar el movimiento 

FUNCTION inving001_grabar(operacion)
 DEFINE w_mae_art    RECORD LIKE inv_products.*, 
        wtipmov      LIKE inv_mtransac.tipmov,
        wopeuni      LIKE inv_dtransac.opeuni, 
        wopeval      LIKE inv_dtransac.opeval, 
        wactexi      LIKE inv_dtransac.actexi,
        ultimoprecio LIKE inv_dtransac.prepro,
        totgra       LIKE inv_dtransac.totpro, 
        totexe       LIKE inv_dtransac.totpro, 
        operacion    SMALLINT,
        conteo       INTEGER,    
        i,correl     SMALLINT,
        strsql       STRING 

 -- Grabando transaccion
 ERROR " Registrando Movimiento ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

   -- 1. Grabando datos generales del movimiento (encabezado) 
   -- Asignando datos
   IF (operacion=1) THEN -- Ingreso 
      LET w_mae_tra.lnktra = 0
      LET w_mae_tra.tipope = w_mae_tip.tipope 
   
      -- Verificando origen
      IF w_mae_tra.codori IS NULL THEN
         LET w_mae_tra.codori = 0
      END IF 

      -- Verificando destino
      IF w_mae_tra.coddes IS NULL THEN
         LET w_mae_tra.coddes = 0
      END IF 

      SET LOCK MODE TO WAIT
      INSERT INTO inv_mtransac
      VALUES (w_mae_tra.*)
      LET w_mae_tra.lnktra = SQLCA.SQLERRD[2]

   ELSE                  -- Modificacion 

      -- Asignando datos   
      LET w_mae_tra.userid = username 
      LET w_mae_tra.fecsis = CURRENT 
      LET w_mae_tra.horsis = CURRENT HOUR TO SECOND 

      -- Actualizando datos del encabezado 
      SET LOCK MODE TO WAIT
      UPDATE inv_mtransac
      SET    inv_mtransac.*      = w_mae_tra.*
      WHERE  inv_mtransac.lnktra = w_mae_tra.lnktra  

      -- Eliminando datos del detalle antes de grabar 
      SET LOCK MODE TO WAIT
      DELETE FROM inv_dtransac 
      WHERE inv_dtransac.lnktra = w_mae_tra.lnktra
   END IF 

   -- 2. Grabando detalle de productos del movimiento (detalle)
   LET correl = 0 
   FOR i = 1 TO totlin 
    IF v_products[i].cditem IS NULL OR 
       v_products[i].canuni IS NULL THEN 
       CONTINUE FOR 
    END IF 
    LET correl = (correl+1) 

    -- Verificando el tipo de operacion del movimiento 
    INITIALIZE wopeuni,wopeval TO NULL
    CASE (w_mae_tra.tipope)
      WHEN 0
        LET wopeuni = (v_products[i].canuni*(-1))
        LET wopeval = (v_products[i].totpro*(-1))
      WHEN 1
        LET wopeuni = v_products[i].canuni
        LET wopeval = v_products[i].totpro
    END CASE

    -- Obteniendo datos del productos
    INITIALIZE w_mae_art.* TO NULL
    CALL librut003_bproductoabr(v_products[i].cditem)
    RETURNING w_mae_art.*,existe

    -- Verificando si el producto actualiza existencia cuando el movimiento es una 
    -- compra 
    LET wactexi = 1
    IF (w_mae_tra.tipmov=tipmovcmp) THEN 
     IF w_mae_art.ixpeso THEN 
        LET wactexi = 0 
     END IF 
    END IF 

    -- Verificando fecha inicio de inventario
    IF (w_mae_tra.fecemi<fechainicioinv) THEN
       LET wactexi = 0 
    END IF 

    -- Verificando si documento es valorizado
    LET ultimoprecio = 0  
    IF w_mae_tip.hayval THEN
       IF v_products[i].canuni>0 THEN
          LET ultimoprecio = v_products[i].totpro/v_products[i].canuni 
       END IF 
    END IF 

    -- Grabando
    SET LOCK MODE TO WAIT
    INSERT INTO inv_dtransac
    VALUES (w_mae_tra.lnktra     , -- id del movimiento 
            w_mae_tra.codemp     , -- empresa 
            w_mae_tra.codsuc     , -- sucursal 
            w_mae_tra.codbod     , -- bodega   
            w_mae_tra.tipmov     , -- tipo de movimiento
            w_mae_tra.codori     , -- origen del movimiento 
            w_mae_tra.coddes     , -- destino del movimiento 
            w_mae_tra.fecemi     , -- fecha del movimiento 
            w_mae_tra.aniotr     , -- anio del movimiento 
            w_mae_tra.mestra     , -- mes del movimiento
            w_mae_art.cditem     , -- codigo del producto 
            v_products[i].cditem , -- codigo del producto abreviado
            ""                   , -- numero de serie del producto - no usado 
            v_products[i].codepq , -- codigo empaque
            v_products[i].canepq , -- cantidad ingresada
            v_products[i].canuni , -- cantidad empaque * 
                                   -- cantidad unds contenida en el empaque 
            v_products[i].preuni , -- precio unitario            
            v_products[i].totpro , -- precio total               
            ultimoprecio         , -- ultimo precio 
            correl               , -- numero correlativo de producto 
            ""                   , -- barcod, numero de codigo de barras
            w_mae_tra.tipope     , -- tipo de operacion del movimiento 
            w_mae_tra.estado     , -- estado del movimiento 
            wopeval              , -- valor dependiendo tipo operacion es + o -
            wopeuni              , -- cantidad unds depende tipo operacion es + o -
            wactexi              , -- el producto actualiza existencia (1=si 0=no) 
            v_products[i].porisv , -- porcentaje de impuesto actual
            v_products[i].totisv , -- total de impuesto del producto 
            w_mae_tra.userid     , -- usuario que registro el movimiento 
            w_mae_tra.fecsis     , -- fecha en que se registro el movimiento 
            w_mae_tra.horsis)      -- hora en la que registro el movimiento 
   END FOR 

 -- Finalizando la transaccion
 COMMIT WORK
 DISPLAY BY NAME w_mae_tra.lnktra 

 -- Desplegando numero de movimiento 
 CASE (operacion)
  WHEN 1 LET msg="Movimiento ("||w_mae_tra.lnktra||") registrado." 
  WHEN 2 LET msg="Movimiento ("||w_mae_tra.lnktra||") actualizado." 
 END CASE
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Imprimiendo movimiento si opcion de grabar e imprimir esta habilitada
 IF grabaeimprime THEN
    ERROR "Imprimiendo movimiento ... por favor esperar ...."
    LET reimpresion = FALSE
    CALL invrpt001_reportemov()
    ERROR "" 
 END IF 
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION inving001_inival(i)
 DEFINE i SMALLINT
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_tra.*  TO NULL 
   CLEAR FORM 
 END CASE 

 -- Inicializando datos
 LET w_mae_tra.impres = "N"
 LET w_mae_tra.estado = "V" 
 LET w_mae_tra.numord = 1 
 LET w_mae_tra.conaut = 0 
 LET w_mae_tra.lnkcon = 0 
 LET w_mae_tra.userid = username 
 LET w_mae_tra.fecsis = CURRENT 
 LET w_mae_tra.horsis = CURRENT HOUR TO SECOND
 LET totlin           = 0 
 LET totpdt           = 0 
 LET totuni           = 0
 LET totval           = 0
 LET fechainicioinv   = "30/11/2015" 

 -- Lllenando combo de bodegas
 CALL librut003_cbxbodegasxusuario(w_mae_tra.userid) 

 -- Inicializando vectores de datos
 CALL inving001_inivec() 

 -- Obtiendo nombre del estado
 LET nomest = librut001_valores(w_mae_tra.estado) 

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_tra.userid,w_mae_tra.fecsis,w_mae_tra.horsis,
                 w_mae_tra.estado,w_mae_tra.lnktra,totval,totpdt,nomest 
END FUNCTION

-- Subrutina para inicializar y limpiar los vectores de trabajo 

FUNCTION inving001_inivec()
 DEFINE i SMALLINT

 -- Inicializando vectores
 CALL v_products.clear()

 LET totlin = 0 
 FOR i = 1 TO 8
  -- Limpiando vector       
  CLEAR s_products[i].*
 END FOR 
END FUNCTION 

-- Subrutina para obtener el ultimo precio de compra de un producto x su empaque

FUNCTION inving001_UltimoPrecioCompraEpq(wcodemp,wcodsuc,wcodbod,wtipmov,
                                         wcodori,wfecemi,wcditem,wcodepq)
 DEFINE wcodemp LIKE inv_dtransac.codemp, 
        wcodsuc LIKE inv_dtransac.codsuc, 
        wcodbod LIKE inv_dtransac.codbod, 
        wtipmov LIKE inv_dtransac.tipmov, 
        wcodori LIKE inv_dtransac.codori,
        wfecemi LIKE inv_dtransac.fecemi, 
        wcditem LIKE inv_dtransac.cditem,
        wcodepq LIKE inv_dtransac.codepq,
        wpreuni LIKE inv_dtransac.prepro  
 
 -- Obteniendo precio
 -- Verificando si tipo de movimiento es valorizado 
 IF w_mae_tip.hayval THEN 
   SELECT NVL(MAX(a.preuni),0)
    INTO  wpreuni
    FROM  inv_dtransac a
    WHERE a.lnktra = (SELECT MAX(x.lnktra)
                       FROM  inv_dtransac x
                       WHERE x.codemp = wcodemp
                         AND x.codsuc = wcodsuc
                         AND x.codbod = wcodbod
                         AND x.tipmov = wtipmov
                         AND x.codori = wcodori 
                         AND x.fecemi <= wfecemi 
                         AND x.cditem = wcditem
                         AND x.codepq = wcodepq)
      AND a.codemp = wcodemp
      AND a.codsuc = wcodsuc
      AND a.codbod = wcodbod
      AND a.tipmov = wtipmov
      AND a.codori = wcodori 
      AND a.fecemi <= wfecemi 
      AND a.cditem = wcditem
      AND a.codepq = wcodepq 

    IF wpreuni=0 OR wpreuni IS NULL THEN
       LET wpreuni = 0
    END IF 
 ELSE 
  -- Si es ultimo precio de compra 
  IF TipoCosteo=2 THEN 
   SELECT NVL(MAX(a.preuni),0)
    INTO  wpreuni
    FROM  inv_dtransac a
    WHERE a.lnktra = (SELECT MAX(x.lnktra)
                      FROM  inv_dtransac x
                      WHERE x.codemp = wcodemp
                        AND x.codsuc = wcodsuc
                        AND x.codbod = wcodbod
                        AND x.tipmov = wtipmov
                        AND x.codori IS NOT NULL 
                        AND x.fecemi <= wfecemi 
                        AND x.cditem = wcditem
                        AND x.codepq = wcodepq)
      AND a.codemp = wcodemp
      AND a.codsuc = wcodsuc
      AND a.codbod = wcodbod
      AND a.tipmov = wtipmov
      AND a.codori IS NOT NULL 
      AND a.fecemi <= wfecemi 
      AND a.cditem = wcditem
      AND a.codepq = wcodepq 

   IF wpreuni=0 OR wpreuni IS NULL THEN
      LET wpreuni = 0
   END IF
 ELSE
  -- Costeo ponderado 
  SELECT NVL(a.cospro,0) 
   INTO  wpreuni 
   FROM  inv_proenbod a
   WHERE a.codemp = wcodemp
     AND a.codsuc = wcodsuc
     AND a.codbod = wcodbod
     AND a.cditem = wcditem
   IF wpreuni=0 OR wpreuni IS NULL THEN
      LET wpreuni = 0
   END IF
  END IF 
 END IF

 RETURN wpreuni
END FUNCTION 
