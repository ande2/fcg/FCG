{ 
Programo : srerep001.4gl 
Objetivo : Reporte de recetario de restaurante
}

DATABASE storepos 

-- Definicion de variables globales 
TYPE   datosreporte  RECORD 
        numrec       LIKE sre_mrecetas.numrec,
        nomrec       CHAR(50),
        coddiv       LIKE sre_divrecet.coddiv,
        nomdiv       CHAR(40),
        cospre       LIKE sre_mrecetas.cospre,
        prevta       LIKE sre_mrecetas.prevta,  
        codabr       CHAR(20),
        dsitem       CHAR(40),
        unimed       LIKE inv_unimedid.unimed,
        nommed       CHAR(6), 
        canuni       LIKE sre_drecetas.canuni
       END RECORD
DEFINE w_datos       RECORD
        coddiv       LIKE sre_divrecet.coddiv,
        numrec       LIKE sre_mrecetas.numrec
       END RECORD,
       existe        SMALLINT,
       primeravez    SMALLINT, 
       tituloreporte STRING,
       filename      STRING,
       pipeline      STRING,
       s,d           CHAR(1) 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("restadisticaventas")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL srerep001_Recetas()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION srerep001_Recetas()
 DEFINE wpais      VARCHAR(255),
        w          ui.Window,
        loop,res   SMALLINT

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep001a AT 5,2
  WITH FORM "srerep001a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("srerep001",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/Recetas.spl"

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET tituloreporte = w.getText()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox
  CALL librut003_cbxdivisionesrecetas()
  CALL librut003_cbxrecetas()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   LET primeravez = TRUE 
   CLEAR FORM
   LET s = 1 SPACES
   LET d = "~"

   -- Construyendo busqueda
   INPUT BY NAME w_datos.coddiv,
                 w_datos.numrec
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 
     EXIT INPUT

    ON ACTION excel
     -- Asignando dispositivo
     LET pipeline = "excel"
     LET s        = ASCII(9)
     LET d        = ASCII(9)
     EXIT INPUT 

    AFTER INPUT 
     -- Verificando datos
     IF w_datos.coddiv IS NULL OR
        w_datos.numrec IS NULL OR
        pipeline       IS NULL THEN
        NEXT FIELD coddiv
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Generando el reporte
   CALL srerep001_GeneraRecetas()
  END WHILE
 CLOSE WINDOW wrep001a   
END FUNCTION 

-- Subrutina para generar el reporte de estadistica de ventas

FUNCTION srerep001_GeneraRecetas()
 DEFINE w_mae_fac  datosreporte,
        s_query    STRING,
        strcoddiv  STRING,
        strnumrec  STRING       

 --Verificando condicion de division
 LET strcoddiv = NULL
  IF w_datos.coddiv IS NOT NULL THEN
    LET strcoddiv = " WHERE x.coddiv = ",w_datos.coddiv
 ELSE
    LET strcoddiv = " WHERE x.coddiv IS NOT NULL "
 END IF

  -- Verificando condicion de receta
 LET strnumrec = NULL
 IF w_datos.numrec IS NOT NULL THEN
    LET strnumrec = " AND x.numrec = '",w_datos.numrec CLIPPED,"'" 
 END IF

 -- Preparando condiciones del reporte 
 LET s_query = "SELECT x.numrec,x.nomrec,x.coddiv,x.nomdiv,x.cospre,x.prevta,",
               "x.codabr,x.dsitem,x.unimed,x.nommed,x.canuni ",
               "FROM  vis_reporterecetas x ",
               strcoddiv CLIPPED,
               strnumrec CLIPPED,
               " ORDER BY x.nomdiv,x.nomdiv,x.numrec" 
DISPLAY s_query

 -- Seleccionando datos del reporte
 ERROR "Atencion: seleccionando datos ... por favor espere ..."

 PREPARE s_estad FROM s_query
 DECLARE c_estad CURSOR FOR s_estad
 LET existe    = FALSE
 FOREACH c_estad INTO w_mae_fac.*
  -- Iniciando reporte
  IF NOT existe THEN
     -- Iniciando la impresion
     START REPORT srerep001_GeneraReporte TO filename
     LET existe = TRUE
  END IF

  -- Llenado el reporte
  OUTPUT TO REPORT srerep001_GeneraReporte(w_mae_fac.*) 
 END FOREACH
 CLOSE c_estad
 FREE  c_estad

 IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT srerep001_GeneraReporte 

     -- Transfiriendo reporte a excel
    IF pipeline = "excel" THEN
      CALL librut005_excel(filename)
    ELSE
    -- Enviando reporte al destino seleccionado
    CALL librut001_sendreport
    (filename,pipeline,tituloreporte,
     "--noline-numbers "||
     "--nofooter "||
     "--font-size 8 "||
     "--page-width 842 --page-height 595 "||
     "--left-margin 55 --right-margin 25 "||
     "--top-margin 35 --bottom-margin 45 "||
     "--title Facturacion")
    END IF
 
    ERROR "" 
    CALL fgl_winmessage(" Atencion","Reporte Emitido.","information") 
 ELSE
    ERROR "" 
    CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
 END IF 
END FUNCTION 

-- Subrutina para imprimir el reporte 

REPORT srerep001_GeneraReporte(imp1)
 DEFINE imp1          datosreporte, 
        wnomdiv       CHAR(40),
        wnomrec       CHAR(40),
        linea         CHAR(132), 
        exis,i,col    SMALLINT,
        lg,lfn        SMALLINT,
        totrec        INTEGER, 
        periodo       STRING,
        xrecetas      STRING

  OUTPUT LEFT   MARGIN 3 
         PAGE   LENGTH 70 
         TOP    MARGIN 3 
         BOTTOM MARGIN 3 

  FORMAT 
   PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________"
     
     -- Verificando si reporte es por divisiones
    IF w_datos.coddiv IS NOT NULL THEN
      -- Seleccionando nombre de la division
      SELECT NVL(a.nomdiv,"")
       INTO  wnomdiv
       FROM  sre_divrecet a
       WHERE a.coddiv = w_datos.coddiv
    ELSE
       LET wnomdiv = "DIVISIONES: TODAS" 
    END IF

    -- Verificando si reporte es por receta
    IF w_datos.numrec IS NOT NULL THEN
      -- Seleccionando nombre de la receta
      SELECT NVL(a.nomrec,"")
       INTO  wnomrec
       FROM  sre_mrecetas a
       WHERE a.numrec = w_datos.numrec
       LET xrecetas = "RECETA ",wnomrec CLIPPED
    ELSE
       LET xrecetas = "RECETAS: TOTAS"    
    END IF

    -- Ajustando largo del reporte 
    LET lg = 132 
    LET lfn = lg-20 

    -- Imprimiendo encabezado 
    LET col = librut001_centrado(tituloreporte,lg)
    PRINT COLUMN   1,"Recetas",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,
	  COLUMN lfn,PAGENO USING "Pagina: <<"

    LET col = librut001_centrado(periodo,lg)
    PRINT COLUMN   1,"Srerep001",
          COLUMN col,periodo CLIPPED,
          COLUMN lfn,"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET col = librut001_centrado(imp1.nomrec,lg)
    PRINT COLUMN   1,wnomdiv CLIPPED,
          COLUMN col,xrecetas CLIPPED, 
          COLUMN lfn,"Hora  : ",TIME 

    PRINT linea 
    PRINT "Producto              Descripcion del Producto                     Unidad",
          "                 Cantidad"
    PRINT "                                                                   Medida"
    PRINT linea
   
   BEFORE GROUP OF imp1.nomdiv
    PRINT imp1.nomdiv CLIPPED
    PRINT s                  

   BEFORE GROUP OF imp1.numrec 
    IF (pipeline="excel") AND primeravez THEN
     PRINT UPSHIFT(tituloreporte),s
     PRINT wnomdiv CLIPPED,s
     PRINT xrecetas CLIPPED,s
     PRINT periodo CLIPPED,s
     PRINT s
     PRINT "Producto",s,"Descripcion del Producto",s,"Unidad",s,"Cantidad",s
     PRINT s,s,"Medida",s,"Unidades",s
     PRINT s
     LET primeravez = FALSE 
    END IF
   
    -- Imprimiendo receta
    LET totrec = 0 
    PRINT "RECETA: [ ",imp1.numrec CLIPPED," - ",imp1.nomrec CLIPPED," ]",s 
    PRINT "----------------------------------------------------------", 
          "----------------------------------------------------------" 

   ON EVERY ROW
    -- Imprimiendo datos 
    LET totrec = totrec+1
    PRINT imp1.codabr                                               ,1 SPACES,s, 
          imp1.dsitem                                               ,4 SPACES,s, 
          imp1.nommed                                               ,11 SPACES,s,
          imp1.canuni                         USING "##,##&.&&&&&&" ,6 SPACES,s

   AFTER GROUP OF imp1.numrec
    -- Totalizando por receta
    IF (totrec=0) THEN 
       PRINT "SIN DETALLE DE PRODUCTOS" 
       display "Recetas no ",totrec 
    ELSE 
       display "Recetas ",totrec 
    END IF 
    LET totrec = 0 

    PRINT "----------------------------------------------------------", 
          "----------------------------------------------------------" 
    PRINT s

   AFTER GROUP OF imp1.nomdiv
    SKIP 1 LINES 
END REPORT
