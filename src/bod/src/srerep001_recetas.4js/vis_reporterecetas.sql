drop view vis_reporterecetas;

CREATE VIEW vis_reporterecetas(numrec,nomrec,coddiv,nomdiv,cospre,prevta,
                               codabr,dsitem,unimed,nommed,canuni) AS

SELECT a.numrec,a.nomrec,e.coddiv,e.nomdiv,a.cospre,a.prevta,
       c.codabr,c.dsitem,d.unimed,d.nommed,b.canuni
 FROM  sre_mrecetas a,
       sre_divrecet e,
       outer (sre_drecetas b,inv_products c,inv_unimedid d)
WHERE  b.lnkrec = a.lnkrec
AND    c.cditem = b.cditem
AND    d.unimed = b.unimed
AND    e.coddiv = a.coddiv;

grant select on vis_reporterecetas to public;
