{
Programo : Mynor Ramirez 
Objetivo : Impresion de etiquetas de codigo de barras
}

-- Definicion de variables globales  

GLOBALS "invglo010.4gl" 
CONSTANT NombreEmpresa = "COSTA'S BURGER & CHICKEN"
TYPE     barcode       RECORD
          cditem       LIKE inv_products.cditem,
          codabr       LIKE inv_products.codabr,
          despro       CHAR(38),
          nomcat       CHAR(29),
          canepq       LIKE inv_products.canepq, 
          numetq       VARCHAR(20) 
         END RECORD 
DEFINE   numerocopias  SMALLINT,
         existe        SMALLINT, 
         filename      CHAR(90),
         pipeline      CHAR(90),
         i             INT

-- Subrutina para generar etiquetas unicas de codigo de barras para identificacion
-- de producto 

FUNCTION invrpt010_EtiquetasIdentificacion(wcditem)
 DEFINE w_mae_pes RECORD LIKE inv_pesajpro.*,
        wcditem   LIKE inv_products.cditem, 
        w_mae_bar barcode,
        i         SMALLINT 

 -- Preparando impresion
 LET numerocopias = fgl_winprompt(5,5,"# Copias a Imprimir Por Etiqueta",1,3,1)
 IF numerocopias IS NULL OR 
    numerocopias <=0 THEN 
    RETURN
 END IF 

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/EtiquetasUnicasBarCode.spl"

 -- Asignando destino de impresion
 LET pipeline = "local"   

 -- Seleccionando datos del producto
 SELECT a.cditem,a.codabr,a.despro,b.nomcat,a.canepq,0 
  INTO  w_mae_bar.* 
  FROM  inv_products a,glb_categors b,glb_subcateg c 
  WHERE a.cditem = wcditem
    AND b.codcat = a.codcat  
    AND c.codcat = a.codcat 
    AND c.subcat = a.subcat 

 -- Imprimiendo copias
 FOR i = 1 TO numerocopias 

  -- Registrando etiqueta unica 
  -- Asignando datos
  LET w_mae_pes.lnketq = 0
  LET w_mae_pes.cditem = w_mae_bar.cditem
  LET w_mae_pes.codabr = w_mae_bar.codabr
  LET w_mae_pes.numetq = librut006_CorrelativoEtiquetas()
  LET w_mae_bar.numetq = "01",w_mae_pes.numetq USING "&&&&&&&&"
  LET w_mae_pes.numetq = w_mae_bar.numetq 
  LET w_mae_pes.fecpro = CURRENT
  LET w_mae_pes.fecven = CURRENT
  LET w_mae_pes.diavig = 0
  LET w_mae_pes.codtar = 0
  LET w_mae_pes.pbruto = 0
  LET w_mae_pes.pstara = 0
  LET w_mae_pes.canpes = 0 
  LET w_mae_pes.ixpeso = 0 
  LET w_mae_pes.canepq = w_mae_bar.canepq 
  LET w_mae_pes.cxpeso = 0 
  LET w_mae_pes.cditma = NULL
  LET w_mae_pes.codaba = NULL
  LET w_mae_pes.unipes = "NA"
  LET w_mae_pes.userid = username
  LET w_mae_pes.fecsis = CURRENT
  LET w_mae_pes.horsis = CURRENT HOUR TO SECOND
 
  -- Grabando etiqueta 
  INSERT INTO inv_pesajpro
  VALUES (w_mae_pes.*)
 
  -- Iniciando reporte
  START REPORT invrpt010_GeneraEtiquetasIdentificacion TO filename 
   -- Llenando reporte
   OUTPUT TO REPORT invrpt010_GeneraEtiquetasIdentificacion(w_mae_bar.*)
  -- Finalizando reporte
  FINISH REPORT invrpt010_GeneraEtiquetasIdentificacion 
 
  -- Imprimiendo el reporte
  CALL librut001_enviareporte(filename,pipeline,"") 
 END FOR 
END FUNCTION 

-- Subrutinar para generar las etiquetas de identificacion 

REPORT invrpt010_GeneraEtiquetasIdentificacion(imp1)
 DEFINE imp1 barcode 

 OUTPUT LEFT   MARGIN 0
        PAGE   LENGTH 10
        TOP    MARGIN 0 
        BOTTOM MARGIN 0

 FORMAT 
  ON EVERY ROW
   -- Imprimiendo etiqueta
   -- Largo y ancho de etiqueta PRINT "Q110,0.04"
   PRINT "N" 
   PRINT "OEPL1"  
   PRINT "D12" 
   PRINT "A001,60,0,3,2,1,N,\"",NombreEmpresa,"\"" 
   PRINT "A001,100,0,3,1,1,N,\"",imp1.despro CLIPPED,"\"" 
   PRINT "A001,140,0,3,1,1,N,\"",imp1.nomcat CLIPPED,"\"" 

   PRINT "A001,180,0,3,1,1,N,\"",imp1.cditem USING "<<<<<<<<<<",' - ',
                                 imp1.codabr CLIPPED,' - ',
                                 TODAY USING "dd/mm/yyyy",' ',
                                 TIME,"\"" 

   PRINT "A260,220,0,2,2,2,N,\"Empaque (",imp1.canepq USING "<<<<<<",") \""
   PRINT "B001,220,0,1,2,2,100,B,\"",imp1.numetq CLIPPED,"\"" 
   PRINT "P1"
END REPORT 

-- Subrutina para generar las etiquetas de codigo de barras 
-- pesaje de producto

FUNCTION invrpt010_EtiquetasPesajeProducto(w_mae_pes)
 DEFINE w_mae_pes   RECORD LIKE inv_pesajpro.*

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/EtiquetasPesaje.spl"

 -- Asignando destino de impresion
 LET pipeline = "local"   

 -- Asignando numero de copias 
 LET numerocopias = 1  

 -- Iniciando reporte
 START REPORT invrpt010_GeneraPesajeProducto TO filename 
  -- Llenando reporte
  OUTPUT TO REPORT invrpt010_GeneraPesajeProducto(w_mae_pes.*)
 -- Finalizando reporte
 FINISH REPORT invrpt010_GeneraPesajeProducto 

 -- Imprimiendo el reporte
 CALL librut001_enviareporte(filename,pipeline,"") 
END FUNCTION 

-- Subrutinar para generar las etiquetas de codigo de barras

REPORT invrpt010_GeneraPesajeProducto(imp1)
 DEFINE imp1 RECORD LIKE inv_pesajpro.* 

  OUTPUT LEFT   MARGIN 0
         PAGE   LENGTH 10 
         TOP    MARGIN 0 
         BOTTOM MARGIN 0

 FORMAT 
  on EVERY ROW
   -- Imprimiendo datos de la etiqueta
   -- Largo y ancho de etiqueta PRINT "Q110,0.04"
   PRINT "N" 
   PRINT "OEPL1"  
   PRINT "D12" 
   PRINT "A001,20,0,3,2,1,N,\"",NombreEmpresa,"\"" 
   PRINT "A001,60,0,3,1,1,N,\"",w_mae_pro.despro CLIPPED,"\"" 

   IF (imp1.diavig>0) THEN
      PRINT "A001,100,0,3,1,1,N,\"","Fecha Produccion: ",imp1.fecpro," ",imp1.horsis,"\"" 
      PRINT "A001,140,0,3,1,1,N,\"","Fecha Vence:      ",imp1.fecven,"\"" 
   ELSE
      PRINT "A001,100,0,3,1,1,N,\"","Fecha Pesaje: ",imp1.fecsis," ",imp1.horsis,"\"" 
   END IF 

   PRINT "A420,200,0,3,2,2,N,\"",imp1.canpes USING "<,<<&.&&"," ",imp1.unipes CLIPPED,"\"" 
   PRINT "B001,180,0,1,2,2,100,B,\"",imp1.numetq CLIPPED,"\"" 
   PRINT "P",numerocopias USING "<<<"
END REPORT 
