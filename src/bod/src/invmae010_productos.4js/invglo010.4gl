{ 
invglo010.4gl
Mynor Ramirez
Mantenimiento de productos 
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname  = "invmae010"
DEFINE w_mae_pro   RECORD LIKE inv_products.*,
       w_mae_sub   RECORD LIKE glb_subcateg.*,
       v_products  DYNAMIC ARRAY OF RECORD
        tcditem    LIKE inv_products.cditem,
        tcodabr    LIKE inv_products.codabr,
        tdsitem    LIKE inv_products.dsitem, 
        tnomcat    LIKE glb_categors.nomcat, 
        texiact    LIKE inv_products.exiact, 
        testado    CHAR(20),
        tendrec    CHAR(1) 
       END RECORD, 
       v_epqsxpro  DYNAMIC ARRAY OF RECORD
        tcheckb    SMALLINT,
        tcodepq    LIKE inv_epqsxpro.codepq,
        tnomepq    LIKE inv_epqsxpro.nomepq,
        tcantid    LIKE inv_epqsxpro.cantid,
        relleno    CHAR(1) 
       END RECORD,
       v_existpro  DYNAMIC ARRAY OF RECORD
        tcodemp    VARCHAR(40),
        tcodsuc    VARCHAR(40),
        tcodbod    VARCHAR(40),
        tcantid    LIKE inv_proenbod.exican,
        tcospro    LIKE inv_proenbod.cospro, 
        tfulent    LIKE inv_proenbod.fulent, 
        tfulsal    LIKE inv_proenbod.fulsal,
        trellen    CHAR(1) 
       END RECORD, 
       v_movtos    DYNAMIC ARRAY OF RECORD
        mfecemi    LIKE inv_dtransac.fecemi,
        mnommov    CHAR(30), 
        morides    CHAR(30),
        mnumdoc    CHAR(20),
        mnomuni    CHAR(20),
        mcanepq    LIKE inv_dtransac.canepq,
        mnomepq    LIKE inv_empaques.nomepq, 
        mcargos    LIKE inv_dtransac.canuni,
        mabonos    LIKE inv_dtransac.canuni,
        muserid    LIKE inv_dtransac.userid,
        mfecsis    LIKE inv_dtransac.fecsis,
        mhorsis    LIKE inv_dtransac.horsis,
        trellen    CHAR(1) 
       END RECORD, 
       v_kardex    DYNAMIC ARRAY OF RECORD
        kfecemi    LIKE inv_dtransac.fecemi,
        knommov    CHAR(30), 
        korides    CHAR(30),
        knumdoc    CHAR(20),
        knomuni    CHAR(20),
        kcanepq    LIKE inv_dtransac.canepq,
        knomepq    LIKE inv_empaques.nomepq, 
        kcargos    LIKE inv_dtransac.canuni,
        kabonos    LIKE inv_dtransac.canuni,
        ksalact    DEC(14,2), 
        kuserid    LIKE inv_dtransac.userid,
        kfecsis    LIKE inv_dtransac.fecsis,
        khorsis    LIKE inv_dtransac.horsis,
        krellen    CHAR(1) 
       END RECORD, 
       v_compras   DYNAMIC ARRAY OF RECORD
        cfecemi    LIKE inv_dtransac.fecemi,
        cnomprv    CHAR(30),
        cnumdoc    CHAR(20),
        cnomuni    CHAR(20),
        ccanepq    LIKE inv_dtransac.canepq,
        cnomepq    LIKE inv_empaques.nomepq, 
        ccantid    LIKE inv_dtransac.canuni,
        cpreuni    LIKE inv_dtransac.preuni,
        ctotpro    LIKE inv_dtransac.totpro,
        cuserid    LIKE inv_dtransac.userid,
        cfecsis    LIKE inv_dtransac.fecsis,
        chorsis    LIKE inv_dtransac.horsis,
        crellen    CHAR(1) 
       END RECORD, 
       v_subprods  DYNAMIC ARRAY OF RECORD
        scditem    LIKE inv_dproduct.cditem,
        scitems    LIKE inv_dproduct.citems,
        scodabr    LIKE inv_dproduct.codabr,
        sdespro    LIKE inv_products.despro, 
        scantid    DEC(14,6), 
        srellen    CHAR(1)
       END RECORD, 
       v_ingxpeso  DYNAMIC ARRAY OF RECORD
        ifecpro    LIKE inv_pesajpro.fecpro,
        lnumetq    LIKE inv_pesajpro.numetq, 
        idiavig    LIKE inv_pesajpro.diavig,
        ifecven    LIKE inv_pesajpro.fecven,
        ipbruto    LIKE inv_pesajpro.pbruto,  
        ipstara    LIKE inv_pesajpro.pstara,  
        ipsneto    LIKE inv_pesajpro.canpes,  
        iuserid    LIKE inv_pesajpro.userid,
        ifecsis    LIKE inv_pesajpro.fecsis,
        ihorsis    LIKE inv_pesajpro.horsis,
        irellen    CHAR(1) 
       END RECORD, 
       username    VARCHAR(15),
       w           ui.Window,
       f           ui.Form
END GLOBALS
