{
invmae010.4gl 
Mynor Ramirez
Mantenimiento de productos
}

IMPORT os 

-- Definicion de variables globales 

GLOBALS "invglo010.4gl"
CONSTANT DirectorioPictures = "/sistemas/apl/bod/pic/"
CONSTANT DirectorioBase     = "c:\\\\"
CONSTANT ExtensionImagenes  = "*.jpg *.gif *.ico *.bmp *.png" 
DEFINE existe               SMALLINT,
       filename             STRING

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarProductos") 

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("productos")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo nombre del usuario 
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL invmae010_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae010_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a --AT 5,2
  WITH FORM "invmae010a" -- ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Obtebiendo datos de la ventana actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Menu de opciones
  MENU "Productos"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(progname,4,username) THEN 
       HIDE OPTION "Buscar"
    END IF
    -- Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    "Consulta de productos existentes."
    CALL invqbe010_productos(1) 
   COMMAND "Nuevo"
    "Ingreso de un nuevo producto."
    LET savedata = invmae010_productos(1) 
   COMMAND "Borrar"
    "Eliminacion de un producto existente."
    CALL invqbe010_productos(3) 
   COMMAND "Salir"
    "Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae010_productos(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata,idx      SMALLINT,
        conteo            INTEGER, 
        msg               CHAR(80),
        qrytext           STRING,
        ExtensionArchivo  STRING, 
        result            INT

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL invqbe010_EstadoMenu(5,"") 
    CALL invmae010_inival(1)
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae010_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.codcat,
                w_mae_pro.subcat,
                w_mae_pro.codprv,
                w_mae_pro.unimed,
                w_mae_pro.dsitem,
                w_mae_pro.chkexi, 
                w_mae_pro.canepq, 
                w_mae_pro.porisv,
                w_mae_pro.estado,
                w_mae_pro.status,
                w_mae_pro.pulcom, 
                w_mae_pro.presug,
                w_mae_pro.canva1,
                w_mae_pro.preva1,
                w_mae_pro.canva2,
                w_mae_pro.preva2,
                w_mae_pro.canva3,
                w_mae_pro.preva3,
                w_mae_pro.codabr 
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON ACTION addimagen    
    -- Agregando imagen del producto 
    -- Seleccionando archivo de imagen
    LET filename = winopenfile(DirectorioBase,
                   "Imagenes",
                   ExtensionImagenes,
                   "Seleccion de la Imagen del Producto")

    IF (LENGTH(filename)>0) THEN 
     -- Abriendo archivo 
     LET ExtensionArchivo = os.Path.basename(filename) 
     LET idx              = ExtensionArchivo.GetIndexOf(".",1) 
     LET ExtensionArchivo = ExtensionArchivo.substring(idx,ExtensionArchivo.GetLength()) 
     LET w_mae_pro.urlimg = DirectorioPictures CLIPPED,"IMG",
                            w_mae_pro.cditem USING "<<<<<<<<<<",
                            ExtensionArchivo CLIPPED

     -- Limpiando imagen 
     CALL f.setElementImage("imgpro","")

     -- Copiando el archivo del frontend al server 
     CALL librut001_getfile(filename,w_mae_pro.urlimg) 

     -- Desplegando imagen
     CALL f.setElementImage("imgpro",w_mae_pro.urlimg CLIPPED)

     -- Actualizando url de la imagen
     SET LOCK MODE TO WAIT 
     UPDATE inv_products
     SET    inv_products.urlimg = w_mae_pro.urlimg
     WHERE  inv_products.cditem = w_mae_pro.cditem 
    END IF 

   ON ACTION delimagen    
    -- Agregando imagen del producto 
    LET w_mae_pro.urlimg = NULL

    -- Desplegando imagen
    CALL f.setElementImage("imgpro",w_mae_pro.urlimg CLIPPED)

    -- Actualizando url de la imagen
    SET LOCK MODE TO WAIT 
    UPDATE inv_products
    SET    inv_products.urlimg = w_mae_pro.urlimg
    WHERE  inv_products.cditem = w_mae_pro.cditem 

   BEFORE INPUT 
    -- Verificando integridad
    -- Si un producto ya tiene movimiento no se puede modificar varios campos
    IF (operacion=2) THEN -- Si es modificacion
     IF invqbe010_integridad() THEN
        -- Verificando si el producto tiene empaques 
        SELECT COUNT(*)
         INTO  conteo
         FROM  inv_epqsxpro a
         WHERE a.cditem = w_mae_pro.cditem
           AND a.codepq >0 
         IF (conteo>0) THEN
            CALL Dialog.SetFieldActive("unimed",FALSE)
         ELSE 
            CALL Dialog.SetFieldActive("unimed",TRUE)
         END IF 
     END IF

     -- Deshabilitando opciones de imagen
     CALL Dialog.SetActionActive("addimagen",1) 
     CALL Dialog.SetActionActive("delimagen",1) 
    ELSE
     -- Deshabilitando opciones de imagen
     CALL Dialog.SetActionActive("addimagen",0) 
     CALL Dialog.SetActionActive("delimagen",0) 
    END IF

   BEFORE FIELD codcat
    -- Cargando combobox de categorias
    CALL librut003_cbxcategorias()

   ON CHANGE codcat
    -- Cargando combobox de subcategorias
    CALL librut003_cbxsubcategorias(w_mae_pro.codcat) 
    LET w_mae_pro.subcat = NULL
    CLEAR subcat 

   AFTER FIELD codcat
    -- Verificando categoria 
    IF w_mae_pro.codcat IS NULL THEN
       ERROR "Error: categoria invalida, VERIFICA."
       NEXT FIELD codcat
    END IF 

   BEFORE FIELD subcat 
    -- Cargando combobox de subcategorias
    CALL librut003_cbxsubcategorias(w_mae_pro.codcat) 

   AFTER FIELD subcat
    -- Verificando subcategoria 
    IF w_mae_pro.subcat IS NULL THEN
       ERROR "Error: categoria invalida, VERIFICA."
       NEXT FIELD codcat 
    END IF 

   BEFORE FIELD codprv 
    -- Cargando combobox de proveedores
    CALL librut003_cbxproveedores() 

   AFTER FIELD codprv 
    -- Verificando proveedor
    IF w_mae_pro.codprv IS NULL THEN
       NEXT FIELD codprv
    END IF 

   BEFORE FIELD unimed
    -- Cargando combobox de unidades de medida 
    CALL librut003_cbxunidadesmedida("unimed")

   AFTER FIELD unimed 
    -- Verificando unidad de medida
    IF w_mae_pro.unimed IS NULL THEN
       NEXT FIELD unimed 
    END IF 

   AFTER FIELD dsitem  
    --Verificando nombre del producto
    IF (LENGTH(w_mae_pro.dsitem)=0) THEN
       ERROR "Error: nombre del producto invalida, VERIFICA."
       LET w_mae_pro.dsitem = NULL
       NEXT FIELD dsitem  
    END IF

    -- Verificando que no exista otro producto con el mismo nombre
    SELECT UNIQUE (a.cditem)
     FROM  inv_products a
     WHERE (a.cditem != w_mae_pro.cditem) 
       AND (a.dsitem  = w_mae_pro.dsitem) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion",
        " Existe otro producto con el mismo nombre, VERIFICA.",
        "stop")
        NEXT FIELD dsitem
     END IF 

   AFTER FIELD chkexi 
    -- Verificando si se chequea existencia
    IF w_mae_pro.chkexi IS NULL THEN
       ERROR "Error: debe indicarse si se chequea existencia, VERIFICA." 
       NEXT FIELD chkexi 
    END IF 

   AFTER FIELD canepq 
    -- Verificando cantidad de empaque 
    IF w_mae_pro.canepq IS NULL THEN 
       LET w_mae_pro.canepq = 0
       DISPLAY BY NAME w_mae_pro.canepq  
    END IF 

   AFTER FIELD porisv
    -- Verificando dias de vigencia
    IF w_mae_pro.porisv IS NULL OR 
       w_mae_pro.porisv >40 THEN 
       LET w_mae_pro.porisv = 0
       DISPLAY BY NAME w_mae_pro.porisv  
    END IF 

   AFTER FIELD estado 
    -- Verificando estado
    IF w_mae_pro.estado IS NULL THEN
       ERROR "Error: estado invalido, VERIFICA."
       NEXT FIELD estado 
    END IF 

   AFTER FIELD status 
    -- Verificando estatus 
    IF w_mae_pro.status IS NULL THEN
       ERROR "Error: estatus invalido, VERIFICA."
       NEXT FIELD status 
    END IF 

   AFTER FIELD pulcom 
    -- Verificando precio de compra
    IF w_mae_pro.pulcom IS NULL OR
       w_mae_pro.pulcom <0 THEN 
       LET w_mae_pro.pulcom = 0
       DISPLAY BY NAME w_mae_pro.pulcom  
    END IF 

   AFTER FIELD presug 
    -- Verificando precio base 
    IF w_mae_pro.presug IS NULL OR
       w_mae_pro.presug <0 THEN 
       LET w_mae_pro.presug = 0
       DISPLAY BY NAME w_mae_pro.presug  
    END IF 

    IF w_mae_pro.presug<w_mae_pro.pulcom THEN
       CALL fgl_winmessage(
       "Atencion","Precio base debe ser mayor o igual a precio costo.","stop") 
       NEXT FIELD pulcom 
    END IF 

   AFTER FIELD canva1 
    -- Verificando cantidad 1
    IF w_mae_pro.canva1 IS NULL OR
       w_mae_pro.canva1 <= 0 THEN 
       LET w_mae_pro.canva1 = 0
       LET w_mae_pro.preva1 = 0
       DISPLAY BY NAME w_mae_pro.canva1,w_mae_pro.preva1 
       NEXT FIELD canva2 
    END IF 

   AFTER FIELD preva1 
    -- Verificando precio 1
    IF w_mae_pro.preva1 IS NULL OR
       w_mae_pro.preva1 <0 THEN 
       LET w_mae_pro.preva1 = 0
       DISPLAY BY NAME w_mae_pro.preva1  
    END IF 

   AFTER FIELD canva2 
    -- Verificando cantidad 2
    IF w_mae_pro.canva2 IS NULL OR
       w_mae_pro.canva2 <= 0 THEN 
       LET w_mae_pro.canva2 = 0
       LET w_mae_pro.preva2 = 0
       DISPLAY BY NAME w_mae_pro.canva2,w_mae_pro.preva2 
       NEXT FIELD canva3 
    END IF 

   AFTER FIELD preva2 
    -- Verificando precio 2
    IF w_mae_pro.preva2 IS NULL OR 
       w_mae_pro.preva2 <0 THEN 
       LET w_mae_pro.preva2 = 0
       DISPLAY BY NAME w_mae_pro.preva2  
    END IF 

   AFTER FIELD canva3 
    -- Verificando cantidad 3
    IF w_mae_pro.canva3 IS NULL OR 
       w_mae_pro.canva3 <= 0 THEN 
       LET w_mae_pro.canva3 = 0
       LET w_mae_pro.preva3 = 0
       DISPLAY BY NAME w_mae_pro.canva3,w_mae_pro.preva3 
       NEXT FIELD codabr 
    END IF 

   AFTER FIELD preva3 
    -- Verificando precio 3
    IF w_mae_pro.preva3 IS NULL OR 
       w_mae_pro.preva2 <0 THEN 
       LET w_mae_pro.preva3 = 0
       DISPLAY BY NAME w_mae_pro.preva3  
    END IF 

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.codcat IS NULL THEN 
       NEXT FIELD codcat
    END IF
    IF w_mae_pro.subcat IS NULL THEN
       NEXT FIELD codcat 
    END IF 
    IF w_mae_pro.codprv IS NULL THEN 
       NEXT FIELD codprv
    END IF
    IF w_mae_pro.unimed IS NULL THEN 
       NEXT FIELD unimed
    END IF
    IF w_mae_pro.dsitem IS NULL THEN 
       NEXT FIELD dsitem
    END IF
    IF w_mae_pro.chkexi IS NULL THEN 
       NEXT FIELD chkexi
    END IF
    IF w_mae_pro.canepq IS NULL THEN 
       NEXT FIELD canepq
    END IF
    IF w_mae_pro.porisv IS NULL THEN 
       NEXT FIELD porisv
    END IF
    IF w_mae_pro.estado IS NULL THEN
       NEXT FIELD estado 
    END IF 
    IF w_mae_pro.status IS NULL THEN
       NEXT FIELD status 
    END IF 
    IF w_mae_pro.pulcom IS NULL THEN 
       NEXT FIELD pulcom 
    END IF
    IF w_mae_pro.presug IS NULL THEN 
       NEXT FIELD presug 
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae010_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL invmae010_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL invqbe010_EstadoMenu(0,"") 
    CALL invmae010_inival(1)
 END IF 

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un producto

FUNCTION invmae010_grabar(operacion)
 DEFINE operacion     SMALLINT,
        correl        INTEGER, 
        msg           CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos 
   LET w_mae_pro.haypre = 0
   LET w_mae_pro.esserv = 0
   LET w_mae_pro.escomb = 0 
   LET w_mae_pro.estado = 1
   LET w_mae_pro.status = 1
   LET w_mae_pro.despro = w_mae_pro.dsitem 

   -- Obteniendo codigo de barras correlativo por categoria de producto 
   IF LENGTH(w_mae_pro.codabr)<=0 THEN 
    LET w_mae_pro.codabr = "XX" 
   END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO inv_products   
   VALUES (w_mae_pro.*)
   LET w_mae_pro.cditem = SQLCA.SQLERRD[2] 

   IF w_mae_pro.codabr="XX" THEN
      -- Actualizando
      SET LOCK MODE TO WAIT
      UPDATE inv_products
      SET    inv_products.codabr = w_mae_pro.cditem 
      WHERE  inv_products.cditem = w_mae_pro.cditem 
   END IF 

   --Asignando el mensaje 
   DISPLAY BY NAME w_mae_pro.cditem ,w_mae_pro.codabr
   LET msg = "Producto (",w_mae_pro.cditem USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Asignando datos
   LET w_mae_pro.fulcam = TODAY
   LET w_mae_pro.despro = w_mae_pro.dsitem 

   -- Actualizando
   SET LOCK MODE TO WAIT
   UPDATE inv_products
   SET    inv_products.*      = w_mae_pro.*
   WHERE  inv_products.cditem = w_mae_pro.cditem 

   --Asignando el mensaje 
   DISPLAY BY NAME w_mae_pro.cditem ,w_mae_pro.codabr
   LET msg = "Producto (",w_mae_pro.cditem USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando
   DELETE FROM inv_products 
   WHERE (inv_products.cditem = w_mae_pro.cditem)

   --Asignando el mensaje 
   LET msg = "Producto (",w_mae_pro.cditem USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae010_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae010_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.cditem = 0 
   LET w_mae_pro.codmar = 0 
   LET w_mae_pro.descto = 0 
   LET w_mae_pro.rebaja = 0 
   LET w_mae_pro.chkexi = 1 
   LET w_mae_pro.essubp = 0 
   LET w_mae_pro.diavig = 0 
   LET w_mae_pro.canepq = 1 
   LET w_mae_pro.estado = 1 
   LET w_mae_pro.status = 1 
   LET w_mae_pro.exiact = 0 
   LET w_mae_pro.pesado = 0 
   LET w_mae_pro.ixpeso = 0 
   LET w_mae_pro.cxpeso = 0 
   LET w_mae_pro.premin = 0 
   LET w_mae_pro.pulcom = 0
   LET w_mae_pro.presug = 0 
   LET w_mae_pro.canva1 = 0
   LET w_mae_pro.canva2 = 0
   LET w_mae_pro.canva3 = 0
   LET w_mae_pro.preva1 = 0
   LET w_mae_pro.preva2 = 0
   LET w_mae_pro.preva3 = 0
   LET w_mae_pro.prereb = 0 
   LET w_mae_pro.porisv = 0 
   LET w_mae_pro.tipdes = 0 
   LET w_mae_pro.tipepq = 0 
   LET w_mae_pro.userid = username  
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.cditem ATTRIBUTE(REVERSE) 
 DISPLAY BY NAME w_mae_pro.codcat,w_mae_pro.subcat,w_mae_pro.codprv, 
                 w_mae_pro.unimed,w_mae_pro.dsitem,w_mae_pro.chkexi,
                 w_mae_pro.codabr,w_mae_pro.canepq,w_mae_pro.pulcom,
                 w_mae_pro.porisv,w_mae_pro.estado,w_mae_pro.status,
                 w_mae_pro.exiact,w_mae_pro.presug,w_mae_pro.canva1,
                 w_mae_pro.canva2,w_mae_pro.canva3,w_mae_pro.preva1,
                 w_mae_pro.preva2,w_mae_pro.preva3
 DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis 

 -- Limpiando imagen del producto
 CALL f.setElementImage("imgpro","")
END FUNCTION
