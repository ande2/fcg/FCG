{  
invqbe010.4gl 
Mynor Ramirez
Mantenimiento de productos 
}

{ Definicion de variables globales }

GLOBALS "invglo010.4gl" 
CONSTANT ImprimeEtiquetaPeso = 0 
DEFINE w                    ui.Window,
       f                    ui.Form,
       archivodestino       STRING,
       pesobascula          STRING,
       msg                  STRING,
       totlin,totepq,totexi INT,
       totpre,totsub,result INT,
       totkov,totkrx,totcmp INT,
       totmov,toting        INT 

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe010_productos(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        westado             LIKE inv_products.estado,
        wstatus             LIKE inv_products.status,
        qrytext,qrypart     STRING,
        loop,existe,opc,res SMALLINT,
        operacion,scr,arr   SMALLINT,
        qry                 STRING,
        msg                 STRING

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL invqbe010_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae010_inival(1)
   LET int_flag = 0

   -- Cargando comboboxes
   CALL librut003_CbxCategorias()
   CALL librut003_CbxProveedores()
   CALL librut003_CbxMarcas()
   CALL librut003_CbxUnidadesmedida("unimed")
   CALL librut003_CbxDestinos()

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.cditem,a.codabr,a.codcat,a.subcat,a.codprv,
                                a.unimed,a.dsitem,a.chkexi,a.canepq,a.porisv,
                                a.estado,a.status,a.pulcom,a.exiact,a.userid,
                                a.fecsis,a.horsis   
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL invmae010_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

    AFTER FIELD codcat
     -- Cargando combobox de subcategorias
     IF (LENGTH(GET_FLDBUF(a.codcat))>0) THEN 
        CALL librut003_cbxsubcategorias(GET_FLDBUF(a.codcat)) 
     ELSE
        CLEAR subcat 
     END IF 
   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Verificando operacion
   IF (operacion<4) THEN 
    -- Creando la busqueda
    LET qrypart = "SELECT UNIQUE a.cditem,a.codabr,a.dsitem,b.nomcat,a.exiact,a.estado,'' ",
                  " FROM inv_products a,glb_categors b ",
                  " WHERE b.codcat = a.codcat AND ",qrytext CLIPPED, 
                  " ORDER BY 2 "

    -- Declarando el cursor
    PREPARE cprod FROM qrypart
    DECLARE c_products SCROLL CURSOR WITH HOLD FOR cprod

    -- Inicializando vector de seleccion
    CALL v_products.clear() 
   
    --Inicializando contador
    LET totlin = 1

    -- Llenando vector de seleccion 
    FOREACH c_products INTO v_products[totlin].tcditem THRU 
                            v_products[totlin].texiact,
                            westado
     -- Verficando estado
     IF (westado=0) THEN
        LET v_products[totlin].testado = "mars_cancelar.png"
     ELSE
        LET v_products[totlin].testado = "mars_cheque.png"
     END IF

     -- Incrementando contador
     LET totlin = (totlin+1) 
    END FOREACH 
    CLOSE c_products
    FREE  c_products
    LET totlin = (totlin-1) 
    ERROR "" 

    -- Verificando si hubieron datos   
    IF (totlin>0) THEN 
     LET msg = " - Registros [ ",totlin||" ]"
     CALL invqbe010_EstadoMenu(operacion,msg)

     -- Desplegando vector de seleccion 
     DISPLAY ARRAY v_products TO s_products.*
      ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)
 
      ON ACTION cancel 
       -- Salida
       CALL invmae010_inival(1)
       LET loop = FALSE
       EXIT DISPLAY 
 
      ON ACTION buscar
       -- Buscar
       CALL invmae010_inival(1)
       EXIT DISPLAY 

      ON ACTION calculator
       -- Cargando calculadora
       IF NOT winshellexec("calc") THEN
          ERROR "Atencion: calculadora no disponible."
       END IF

      ON ACTION modificar
       -- Modificando 
       LET res = invmae010_productos(2) 
       -- Datos generales
       CALL invqbe010_datos(v_products[ARR_CURR()].tcditem)
       -- Empaques
       CALL invqbe010_epqsxpro(v_products[ARR_CURR()].tcditem,1)
       -- Existencias
       CALL invqbe010_existpro(v_products[ARR_CURR()].tcditem)
 
      ON KEY (CONTROL-M) 
       -- Modificando 
       IF (operacion=2) THEN 
          -- Modificando 
          LET res = invmae010_productos(2)
          -- Datos generales
          CALL invqbe010_datos(v_products[ARR_CURR()].tcditem)
          -- Empaques
          CALL invqbe010_epqsxpro(v_products[ARR_CURR()].tcditem,1)
          -- Existencias
          CALL invqbe010_existpro(v_products[ARR_CURR()].tcditem)
       END IF 
 
      ON ACTION borrar
       -- Borrando
       -- Verificando integridad 
       IF invqbe010_integridad() THEN
          CALL fgl_winmessage(
          "Atencion",
          "Este producto ya tiene movimientos, no puede borrarse.",
          "stop")
          CONTINUE DISPLAY 
       END IF 
 
       -- Comfirmacion de la accion a ejecutar 
       LET msg = "Esta SEGURO de Borrar este producto ? "
       LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)
 
       -- Verificando operacion
       CASE (opc) 
        WHEN 1
          IF (operacion=3) THEN
              --  Eliminando
              CALL invmae010_grabar(3)
             EXIT DISPLAY
          END IF 
        WHEN 2
          CONTINUE DISPLAY
       END CASE 

      ON ACTION barcode
       -- Generando etiquetas del codigo de barras
       CALL invrpt010_EtiquetasIdentificacion(w_mae_pro.cditem)

      ON ACTION empaques
       -- Verificando si existen empaques
       IF (totepq=0) THEN
          CALL fgl_winmessage(
          "Atencion",
          "Producto sin empaques registrados.",
          "warning")
          CONTINUE DISPLAY 
       END IF

       -- Desplegando empaques x producto
       CALL invqbe010_detepqsxpro(v_products[ARR_CURR()].tcditem)

      ON ACTION subprods
       -- Registrando subproductos del producto
       CALL invqbe010_IngresarSubproductos() 

       -- Desplegando subproductos 
       CALL invqbe010_DetalleSubproductos() 

      ON ACTION movtos   
       -- Desplegando detalle de movimientos del producto
       CALL invqbe010_DetalleMovimientos() 

      ON ACTION kardex   
       -- Desplegando detalle de kardex del producto
       CALL invqbe010_DetalleKardex() 

      ON ACTION compras  
       -- Desplegando detalle de compras del producto
       CALL invqbe010_DetalleCompras() 

      ON ACTION ingresospeso
       -- Desplegando detalle de ingresos por peso
       CALL invqbe010_IngresosPeso() 

      ON ACTION reporte
       -- Reporte de datos seleccionados a excel
       -- Asignando nombre de columnas
       LET arrcols[1] = "Producto"
       LET arrcols[2] = "Codigo Barras"
       LET arrcols[3] = "Nombre Producto"
       LET arrcols[4] = "Categoria"
       LET arrcols[5] = "Subcategoria"
       LET arrcols[6] = "Proveedor"
       LET arrcols[7] = "Unidad de Medida" 
       LET arrcols[8] = "Chequea Existencia" 
       LET arrcols[9] = "Cantidad Empaque" 
       LET arrcols[10]= "Precio Ultima Compra"
       LET arrcols[11]= "Estado" 
       LET arrcols[12]= "Estatus" 
       LET arrcols[13]= "Usuario Registro"
       LET arrcols[14]= "Fecha Registro"
       LET arrcols[15]= "Hora Registro"
 
       -- Se aplica el mismo query de la seleccion para enviar al reporte
       LET qry = 
        "SELECT a.cditem,a.codabr,a.dsitem,c.nomcat,h.nomsub,",
               "d.nomprv,b.nommed,",
               "CASE (a.chkexi) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END,",
               "a.canepq,a.pulcom,", 
               "CASE (a.estado) WHEN 1 THEN 'Alta' WHEN 0 THEN 'Baja' END,",
               "CASE (a.status) WHEN 1 THEN 'Disponible' WHEN 0 THEN 'Bloqueado' END,",
               "a.userid,a.fecsis,a.horsis ",
        " FROM inv_products a,inv_unimedid b,glb_categors c,",
              "inv_provedrs d,glb_subcateg h ", 
        " WHERE c.codcat = a.codcat AND ",
               "h.codcat = a.codcat AND ",
               "h.subcat = a.subcat AND ",
               "d.codprv = a.codprv AND ",
               "b.unimed = a.unimed AND ",
               qrytext CLIPPED,
        " ORDER BY 2,3 "

       -- Ejecutando el reporte
       LET res = librut002_excelreport("Productos",qry,15,0,arrcols)

      BEFORE ROW 
       -- Verificando control del total de lineas 
       IF (ARR_CURR()<=totlin) THEN
          -- Datos generales
          CALL invqbe010_datos(v_products[ARR_CURR()].tcditem)
          -- Empaques 
          CALL invqbe010_epqsxpro(v_products[ARR_CURR()].tcditem,1)
          -- Existencias 
          CALL invqbe010_existpro(v_products[ARR_CURR()].tcditem)
          -- Subproductos
          CALL invqbe010_DetalleSubproductos() 
       END IF 
 
      BEFORE DISPLAY
       -- Desabilitando y habilitando opciones segun operacion
       CASE (operacion) 
        WHEN 1 -- Consultar 
 	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a modificar
         IF NOT seclib001_accesos(progname,2,username) THEN
            CALL DIALOG.setActionActive("modificar",FALSE)
         ELSE
            CALL DIALOG.setActionActive("modificar",TRUE)
         END IF

         -- Verificando acceso a etiquetas
         IF NOT seclib001_accesos(progname,9,username) THEN
            CALL DIALOG.setActionActive("barcode",FALSE)
         ELSE
            CALL DIALOG.setActionActive("barcode",TRUE)
         END IF

         -- Verificando acceso a empaques
         IF NOT seclib001_accesos(progname,7,username) THEN
            CALL DIALOG.setActionActive("empaques",FALSE)
         ELSE
            CALL DIALOG.setActionActive("empaques",TRUE)
         END IF

         -- Verificando acceso a subproductos
         IF NOT seclib001_accesos(progname,8,username) THEN
            CALL DIALOG.setActionActive("subprods",FALSE)
         ELSE
            CALL DIALOG.setActionActive("subprods",TRUE)
         END IF

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

        WHEN 3 -- Eliminar        
 	 CALL DIALOG.setActionActive("borrar",TRUE)
 	 CALL DIALOG.setActionActive("modificar",FALSE)
 	 CALL DIALOG.setActionActive("barcode",FALSE)
 	 CALL DIALOG.setActionActive("empaques",FALSE)
 	 CALL DIALOG.setActionActive("subprods",FALSE)
 	 CALL DIALOG.setActionActive("reporte",FALSE)
       END CASE
 
       -- Escondiendo campo de seleccion de empaques x producto
       CALL f.setFieldHidden("tcheckb",1)
 
     END DISPLAY 
    ELSE 
      CALL fgl_winmessage(
      "Atencion",
      "No existen productos con el criterio seleccionado.",
      "stop")
    END IF 
   END IF 
  END WHILE

  -- Desplegando estado del menu 
  CALL invqbe010_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION invqbe010_datos(wcditem)
 DEFINE wcditem   LIKE inv_products.cditem,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  inv_products a "||
              "WHERE a.cditem = "||wcditem||
              " ORDER BY a.dsitem "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_productst SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_productst INTO w_mae_pro.*
 END FOREACH
 CLOSE c_productst
 FREE  c_productst

 -- Cargando combobox de subcategorias
 CALL librut003_CbxSubcategorias(w_mae_pro.codcat)

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.cditem ATTRIBUTE(REVERSE)
 DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis 
 DISPLAY BY NAME w_mae_pro.codcat,w_mae_pro.subcat,w_mae_pro.codprv,
                 w_mae_pro.unimed,w_mae_pro.dsitem,w_mae_pro.chkexi,
                 w_mae_pro.codabr,w_mae_pro.canepq,w_mae_pro.pulcom,
                 w_mae_pro.porisv,w_mae_pro.estado,w_mae_pro.status,
                 w_mae_pro.exiact,w_mae_pro.presug 

 -- Desplegando imagen 
 CALL f.setElementImage("imgpro",w_mae_pro.urlimg CLIPPED)

 -- Desplegando titulos
 CALL librut001_dpelement("labels","Sub-Productos del Producto "||w_mae_pro.despro CLIPPED) 
 CALL librut001_dpelement("labelm","Movimientos del Producto "||w_mae_pro.despro CLIPPED) 
 CALL librut001_dpelement("labelk","Kadex del Producto "||w_mae_pro.despro CLIPPED) 
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION invqbe010_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx",
         "Lista de Productos - MENU")
  WHEN 1 CALL librut001_dpelement("labelx",
         "Lista de Productos - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx",
         "Lista de Productos - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx",
         "Lista de Productos - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx",
         "")
  WHEN 5 CALL librut001_dpelement("labelx",
         "Lista de Productos - NUEVO")
 END CASE
END FUNCTION
                                                         
-- Subrutina para buscar los empaques del producto 

FUNCTION invqbe010_epqsxpro(wcditem,opcion)
 DEFINE wcditem   LIKE inv_products.cditem,
        i,opcion  SMALLINT,
        qrytext   STRING 

 -- Inicializando vector de datos
 CALL v_epqsxpro.clear()
 FOR i = 1 TO 4
  DISPLAY v_epqsxpro[i].* TO s_epqsxpro[i].* 
 END FOR

 -- Seleccionando datos
 CASE (opcion)
  WHEN 1 -- Consulta de empaques                 
   LET qrytext = "SELECT 1,a.codepq,a.nomepq,a.cantid,a.lnkepq,'' "||
                  "FROM  inv_epqsxpro a " ||
                  "WHERE a.cditem = "||wcditem||
                   " ORDER BY 5 DESC " 

  WHEN 2 -- Cargando empaques                             
   LET qrytext = "SELECT 0,a.codepq,a.nomepq,a.cantid,'' "||
                  "FROM  inv_empaques a " ||
                  "WHERE NOT exists (SELECT b.codepq FROM inv_epqsxpro b "||
                                     "WHERE b.cditem = "||wcditem||
                                      " AND b.codepq = a.codepq) "||
                    "AND a.unimed = "||w_mae_pro.unimed|| 
                   " ORDER BY 3" 
 END CASE 

 -- Seleccionando datos
 PREPARE cp1 FROM qrytext 
 DECLARE cepqs CURSOR FOR cp1 
 LET totepq = 1
 FOREACH cepqs INTO v_epqsxpro[totepq].* 
  -- Desplegando datos
  IF (totepq<=4) THEN 
     DISPLAY v_epqsxpro[totepq].* TO s_epqsxpro[totepq].* 
  END IF 
  LET totepq = (totepq+1) 
 END FOREACH
 CLOSE cepqs
 FREE  cepqs
 LET totepq = (totepq-1) 
END FUNCTION 

-- Subrutina para visualizar el detalle de empaques x producto 

FUNCTION invqbe010_detepqsxpro(wcditem)
 DEFINE wcditem    LIKE inv_products.cditem,
        loop,opc,i SMALLINT

 -- Desplegando empaques 
 LET loop = TRUE
 WHILE loop
  DISPLAY ARRAY v_epqsxpro TO s_epqsxpro.*
   ATTRIBUTE(COUNT=totepq,ACCEPT=FALSE,CANCEL=FALSE)
   ON ACTION cancel  
    -- Salida
    LET loop = FALSE
    EXIT DISPLAY 

   ON ACTION agregarepqs  
    -- Agregando empaque
    CALL invqbe010_epqsxpro(wcditem,2) 
    IF (totepq=0) THEN
        CALL fgl_winmessage(
        "Atencion",
        "No existen mas empaques para agregar.\n Todos los empaques ya existen.", 
        "stop")

       -- Cargando empaques del producto 
       CALL invqbe010_epqsxpro(wcditem,1) 
       EXIT DISPLAY 
    ELSE  
      -- Mostrando campo de seleccion de empaques
      CALL f.setFieldHidden("tcheckb",0)

      -- Ingresando empaques
      INPUT ARRAY v_epqsxpro WITHOUT DEFAULTS FROM s_epqsxpro.*
       ATTRIBUTE(MAXCOUNT=totepq,INSERT ROW=FALSE,
                 APPEND ROW=FALSE,DELETE ROW=FALSE,
                 ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
       ON ACTION cancel
        -- Salida
        EXIT INPUT 

       ON ACTION accept
        -- Verificando si existen empaques que agregar
        IF NOT invqbe010_hayepqs(1) THEN
           ERROR "Atencion: no existen empaques marcados que agregar."
           CONTINUE INPUT
        END IF

        -- Grabando empaques seleccionados 
        LET opc = librut001_menugraba("Confirmacion de Empaques",
                                      "Que desea hacer ?",
                                      "Guardar",
                                      "Modificar",
                                      "Cancelar",
                                      NULL)

        CASE (opc)
         WHEN 0 -- Cancelando
          EXIT INPUT 
         WHEN 1 -- Grabando
          -- Iniciando la transaccion
          BEGIN WORK 

          -- Guardando accesos
          FOR i = 1 TO totepq
           IF (v_epqsxpro[i].tcheckb=0) OR
              (v_epqsxpro[i].tcheckb IS NULL) THEN 
              CONTINUE FOR
           END IF 

           -- Grabando  
           SET LOCK MODE TO WAIT
           INSERT INTO inv_epqsxpro 
           VALUES (0,
                   wcditem,
                   v_epqsxpro[i].tcodepq, 
                   v_epqsxpro[i].tnomepq, 
                   v_epqsxpro[i].tcantid, 
                   USER, 
                   CURRENT, 
                   CURRENT HOUR TO SECOND)
          END FOR

          -- Terminando la transaccion
          COMMIT WORK 

          EXIT INPUT 
         WHEN 2 -- Modificando
          CONTINUE INPUT 
        END CASE

       BEFORE ROW
        -- Verificando control del total de lineas 
        IF (ARR_CURR()>totepq) THEN
           CALL FGL_SET_ARR_CURR(1)
        END IF 
      END INPUT 

      -- Escondiendo campo de seleccion de empaques 
      CALL f.setFieldHidden("tcheckb",1)

      -- Cargando empaques del producto 
      CALL invqbe010_epqsxpro(wcditem,1) 
      EXIT DISPLAY 
    END IF 

   ON ACTION eliminarepqs  
    -- Eliminando empaques
    -- Mostrando campo de seleccion de empaques
    CALL f.setFieldHidden("tcheckb",0)

    -- Seleccionando empaques
    INPUT ARRAY v_epqsxpro WITHOUT DEFAULTS FROM s_epqsxpro.*
     ATTRIBUTE(MAXCOUNT=totepq,INSERT ROW=FALSE,
               APPEND ROW=FALSE,DELETE ROW=FALSE,
               ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
     ON ACTION cancel
      -- Salida
      EXIT INPUT 

     ON ACTION accept
      -- Verificando si existen empaques que eliminar
      IF NOT invqbe010_hayepqs(0) THEN
         ERROR "Atencion: no existen empaques desmarcados que borrar."
         CONTINUE INPUT 
      END IF

      -- Eliminando empaques seleccionados
      LET opc = librut001_menugraba("Confirmacion de Empaques",
                                    "Que desea hacer ?",
                                    "Borrar",
                                    "Modificar",
                                    "Cancelar",
                                    NULL)

      CASE (opc)
       WHEN 0 -- Cancelando
        EXIT INPUT 
       WHEN 1 -- Grabando
        -- Iniciando la transaccion
        BEGIN WORK 

        -- Guardando accesos
        FOR i = 1 TO totepq
         IF (v_epqsxpro[i].tcheckb=1) OR 
            (v_epqsxpro[i].tcheckb IS NULL) THEN 
            CONTINUE FOR
         END IF 

         -- Eliminando  
         SET LOCK MODE TO WAIT
         DELETE FROM inv_epqsxpro 
         WHERE inv_epqsxpro.cditem = wcditem 
           AND inv_epqsxpro.codepq = v_epqsxpro[i].tcodepq 
        END FOR

        -- Terminando la transaccion
        COMMIT WORK 

        EXIT INPUT 
       WHEN 2 -- Modificando
        CONTINUE INPUT 
      END CASE

     BEFORE ROW
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totepq) THEN
         CALL FGL_SET_ARR_CURR(1)
      END IF 

     ON CHANGE tcheckb 
      -- Verificando eliminacion
      IF (v_epqsxpro[ARR_CURR()].tcodepq=0) THEN
         IF (v_epqsxpro[ARR_CURR()].tcheckb=0) THEN
            LET v_epqsxpro[ARR_CURR()].tcheckb = 1 
            DISPLAY v_epqsxpro[ARR_CURR()].tcheckb TO s_epqsxpro[SCR_LINE()].tcheckb 
            ERROR "Atencion: empaque default no puede eliminarse."
            NEXT FIELD tcheckb 
         END IF 
      END IF 
    END INPUT 

    -- Escondiendo campo de seleccion de empaques
    CALL f.setFieldHidden("tcheckb",1)

    -- Cargando empaques del producto
    CALL invqbe010_epqsxpro(wcditem,1) 
    EXIT DISPLAY 

   BEFORE ROW 
    -- Verificando control del total de lineas 
    IF (ARR_CURR()>totepq) THEN
       CALL FGL_SET_ARR_CURR(1)
    END IF 
  END DISPLAY 
 END WHILE 
END FUNCTION 

-- Subrutina para verificar si existen empaques seleccionados que agregar o que borrar

FUNCTION invqbe010_hayepqs(estado) 
 DEFINE i,estado,hayepq SMALLINT

 -- Chequeando estado
 LET hayepq = 0
 FOR i = 1 TO totepq
  -- Verificando estado
  IF (v_epqsxpro[i].tcheckb=estado) THEN
     LET hayepq = 1
     EXIT FOR
  END IF 
 END FOR
 
 RETURN hayepq
END FUNCTION 

-- Subrutina para buscar las existencias del producto

FUNCTION invqbe010_existpro(wcditem)
 DEFINE wcditem   LIKE inv_products.cditem,
        qrytext   STRING

 -- Inicializando vector de datos
 CALL v_existpro.clear()
 LET totexi = 1

 -- Seleccionando datos
 LET qrytext = "SELECT b.nomemp,c.nomsuc,d.nombod,NVL(a.exican,0),", 
                      "NVL(a.cospro,0),a.fulent,a.fulsal,'' "||
                "FROM  inv_proenbod a,glb_empresas b,glb_sucsxemp c,inv_mbodegas d "||
                "WHERE a.cditem = "||wcditem||
                "  AND b.codemp = a.codemp "||
                "  AND c.codemp = a.codemp "||
                "  AND c.codsuc = a.codsuc "||
                "  AND d.codemp = a.codemp "||
                "  AND d.codsuc = a.codsuc "||
                "  AND d.codbod = a.codbod "||
                " ORDER BY 4 DESC" 

 -- Seleccionando datos
 PREPARE cp2 FROM qrytext
 DECLARE cexist CURSOR FOR cp2
 FOREACH cexist INTO v_existpro[totexi].*
  LET totexi = (totexi+1)
 END FOREACH
 CLOSE cexist
 FREE  cexist
 LET totexi = (totexi-1)

 -- Desplegando existencias
 DISPLAY ARRAY v_existpro TO s_existpro.*
  BEFORE DISPLAY 
   EXIT DISPLAY 
 END DISPLAY 
END FUNCTION 

-- Subrutina para verificar si el producto ya tiene registros 

FUNCTION invqbe010_integridad()
 DEFINE conteo INTEGER  

 -- Verificando inventarios 
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_dtransac a 
  WHERE (a.cditem = w_mae_pro.cditem) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION 

-- Subrutina para cargar el detalle de subproductos

FUNCTION invqbe010_DetalleSubproductos() 
 DEFINE i SMALLINT 

 -- Cargando subproductos
 CALL v_subprods.clear()
 LET totsub = 1

 DECLARE csub CURSOR FOR
 SELECT a.cditem,
        a.citems, 
        a.codabr,
        x.despro, 
        a.cantid,
        "",
        a.correl 
  FROM  inv_dproduct a,inv_products x
  WHERE a.cditem = w_mae_pro.cditem
    AND x.codabr = a.codabr 
  ORDER BY a.correl 
 FOREACH csub INTO v_subprods[totsub].*
  -- Incrementando contador 
  LET totsub = (totsub+1)
 END FOREACH
 CLOSE csub
 FREE  csub
 LET totsub = (totsub-1)

 -- Desplegando subproductos 
 DISPLAY ARRAY v_subprods TO s_subprods.* 
  BEFORE DISPLAY
   EXIT DISPLAY 
 END DISPLAY
END FUNCTION 

-- Subrutina para desplegar el detalle de subproductos

FUNCTION invqbe010_VerSubproductos()
 -- Desplegando subproductos 
 DISPLAY ARRAY v_subprods TO s_productos.* 
   ATTRIBUTE(COUNT=totsub,ACCEPT=FALSE) 
  ON ACTION cancel
   EXIT DISPLAY 
 END DISPLAY
END FUNCTION 

-- Subrutina para ingresar el detalle de subproductos

FUNCTION invqbe010_IngresarSubproductos()
 DEFINE w_mae_art       RECORD LIKE inv_products.*,
        i,j,opc,correl  SMALLINT, 
        arr,scr,existe  SMALLINT,
        regreso         SMALLINT, 
        repetido        SMALLINT,
        msg             STRING, 
        lastkey         INT 

 -- Ingresando subproductos 
 INPUT ARRAY v_subprods WITHOUT DEFAULTS FROM s_subprods.*
  ATTRIBUTE(COUNT=totsub,INSERT ROW=FALSE,ACCEPT=FALSE,
            CANCEL=FALSE,UNBUFFERED=TRUE,
            FIELD ORDER FORM)

  ON ACTION cancel
   -- Salir
   CALL v_subprods.clear() 
   EXIT INPUT

  ON ACTION accept
   -- Chequeando si hay productos
   LET j = 0 
   FOR i = 1 TO totsub 
    IF v_subprods[i].scodabr IS NOT NULL THEN
       LET j=j+1 
    END IF 
   END FOR 
   IF j=0 THEN
      CALL fgl_winmessage(
      "Atencion:",
      "Debe ingresarse al menos un producto, VERIFICA.",
      "stop")
      CONTINUE INPUT 
   END IF 

   -- Chequeando lineas incompletas
   LET j = 0
   FOR i = 1 TO totsub 
    IF v_subprods[i].scodabr IS NOT NULL THEN
     IF v_subprods[i].scantid IS NULL THEN 
        LET j = i
        EXIT FOR
     END IF 
    END IF 
   END FOR 
   IF j>0 THEN
      CALL fgl_winmessage(
      "Atencion:",
      "Producto incompleto en linea "||j||".",
      "stop")
      CONTINUE INPUT 
   END IF 

   -- Grabar
   LET opc = librut001_menugraba("Confirmacion",
                                 "Que desea hacer?",
                                 "Guardar",
                                 "Modificar",
                                 "Cancelar",
                                 "")
  
   -- Verificando opcion  
   CASE (opc)
    WHEN 0 -- Salir sin guardar
           CALL v_subprods.clear() 
           EXIT INPUT 
    WHEN 2 -- Modificar 
           CONTINUE INPUT
   END CASE 

   -- Iniciando transaccion
   BEGIN WORK

    -- Borrando antes de grabar
    SET LOCK MODE TO WAIT
    DELETE FROM inv_dproduct
    WHERE (inv_dproduct.cditem = w_mae_pro.cditem)

    -- Grabando subproductos
    LET correl = 0 
    FOR i = 1 TO totsub 
     IF v_subprods[i].scodabr IS NULL THEN
        CONTINUE FOR
     END IF
     LET correl = (correl+1) 
    
     -- Grabando 
     SET LOCK MODE TO WAIT
     INSERT INTO inv_dproduct
     VALUES (w_mae_pro.cditem,      -- Codigo del producto 
             v_subprods[i].scitems, -- Codigo del subproducto 
             v_subprods[i].scodabr, -- Codigo de barras
             v_subprods[i].scantid, -- Cantidad del producto 
             correl,                -- Correlativo de linea
             USER,                  -- Usuario registro kilometraje
             CURRENT,               -- Fecha registro kilometraje
             CURRENT)               -- Hora registro kilometraje} 
    END FOR 

   -- Finalizando transaccion
   COMMIT WORK 

   EXIT INPUT 

  BEFORE INPUT
   -- Deshabilitando tecla de append
   CALL Dialog.SetActionHidden("append",1) 

  ON ACTION listproducto
   LET arr = ARR_CURR()
   LET scr = SCR_LINE()

   -- Seleccionado lista de productos
   CALL librut002_formlist("Consulta de Productos",
                           "Producto",
                           "Descripcion del Producto",
                           "Existencia",
                           "codabr",
                           "despro",
                           "exiact",
                           "inv_products",
                           "estado=1 and status=1",
                           2,
                           1,
                           1)
   RETURNING v_subprods[arr].scodabr,v_subprods[arr].sdespro,regreso
   IF regreso THEN
      NEXT FIELD scodabr
   ELSE
      -- Desplegando datos
      DISPLAY v_subprods[arr].scodabr TO s_subprods[scr].scodabr 
      DISPLAY v_subprods[arr].sdespro TO s_subprods[scr].sdespro
   END IF

  BEFORE FIELD scodabr 
   -- Habilitando tecla de grabar
   CALL Dialog.SetActionActive("accept",1) 

  AFTER FIELD scodabr 
   LET arr = ARR_CURR() 
   LET scr = SCR_LINE() 

   -- Verificando ultima tecla presionada
   LET lastkey = FGL_LASTKEY()
   IF (lastkey = FGL_KEYVAL("down")) THEN
      NEXT FIELD scodabr 
   END IF

  BEFORE FIELD scantid
   LET arr = ARR_CURR() 
   LET scr = SCR_LINE() 

   -- Verificando producto
   IF v_subprods[arr].scodabr IS NULL THEN
      ERROR "Error: producto invalido, VERIFICA." 
      NEXT FIELD scodabr
   END IF 

   -- Verificando si el producto existe
   INITIALIZE w_mae_art.* TO NULL
   CALL librut003_BProductoAbr(v_subprods[arr].scodabr) 
   RETURNING w_mae_art.*,existe
   IF NOT existe THEN 
      CALL fgl_winmessage(
      "Atencion:",
      "Producto no existe registrado, VERIFICA.",
      "STOP") 
      INITIALIZE v_subprods[arr].* TO NULL 
      CLEAR s_subprods[scr].*  
      NEXT FIELD scodabr
   END IF  
   LET v_subprods[arr].scitems = w_mae_art.cditem
   LET v_subprods[arr].sdespro = w_mae_art.despro 
   DISPLAY v_subprods[arr].sdespro TO s_subprods[scr].sdespro 

   -- Verificando que el producro no sea el mismo que contiene los subproductos
   IF (v_subprods[arr].scodabr = w_mae_pro.codabr) THEN
      CALL fgl_winmessage(
      "Atencion:",
      "Producto "||w_mae_art.despro CLIPPED||" no puede seleccionarse, VERIFICA.",
      "STOP") 
      INITIALIZE v_subprods[arr].* TO NULL 
      CLEAR s_subprods[scr].*  
      NEXT FIELD scodabr
   END IF  

   -- Chequeando productos repetidos
   LET repetido = FALSE
   FOR i = 1 TO totlin
    IF v_subprods[i].scodabr IS NULL THEN
       CONTINUE FOR
    END IF

    -- Verificando producto
    IF (i!=arr) THEN
     IF (v_subprods[i].scodabr = v_subprods[arr].scodabr) THEN
        LET repetido = TRUE
        EXIT FOR
     END IF
    END IF
   END FOR

   -- Si hay repetido
   IF repetido THEN
      LET msg = "Producto ya registrado en el detalle. "||
                "Linea (",i USING "<<<",") \nVERIFICA."
      CALL fgl_winmessage("Atencion",msg,"stop")
      INITIALIZE v_subprods[arr].* TO NULL 
      CLEAR s_subprods[scr].*  
      NEXT FIELD scodabr
   END IF

   -- Verificando si el producto es servicio o combo 
   IF w_mae_art.esserv=1 OR
      w_mae_art.escomb=1 THEN
      CALL fgl_winmessage(
      "Atencion:",
      "Producto es servicio o combo, VERIFICA.",
      "STOP") 
      INITIALIZE v_subprods[arr].* TO NULL 
      CLEAR s_subprods[scr].*  
      NEXT FIELD scodabr
   END IF  
   
   -- Desabilitando tecla de grabar
   CALL Dialog.SetActionActive("accept",0) 

  AFTER FIELD scantid 
   LET arr = ARR_CURR() 
   LET scr = SCR_LINE() 

   -- Verificando ultima tecla presionada
   LET lastkey = FGL_LASTKEY()
   IF (lastkey = FGL_KEYVAL("down")) THEN
      NEXT FIELD scantid 
   END IF

   -- Verificando cantidad
   IF v_subprods[arr].scantid IS NULL OR 
      v_subprods[scr].scantid <0 THEN
      ERROR "Error: cantidad invalida, VERIFICA."
      NEXT FIELD scantid 
   END IF 

   -- Verificando si el producto sera tomado para pesaje
   IF v_subprods[arr].scantid =0 THEN
      CALL fgl_winmessage(
      "Atencion: ",
      "Cantidad del producto sera rebajada por medio de peso de bascula. \n"||
      "Tomar nota.",
      "exclamation")
   END IF 

  BEFORE ROW 
   -- Asignando total de filas
   LET totsub = ARR_COUNT() 

  AFTER ROW,INSERT,DELETE
   -- Asignando total de filas
   LET totsub = ARR_COUNT() 

  AFTER INPUT 
   -- Asignando total de filas
   LET totsub = ARR_COUNT() 
 END INPUT 
END FUNCTION 

-- Subrutina para cargar el detalle de movimientos 

FUNCTION invqbe010_DetalleMovimientos() 
 DEFINE xtipope LIKE inv_dtransac.tipope,
        xnomcli LIKE fac_clientes.nomcli, 
        xnomprv LIKE inv_provedrs.nomprv,
        xcanmov DEC(14,2),
        mtotcar DEC(14,2),
        mtotabo DEC(14,2), 
        fx      ui.Form

 -- Cargando movimientos
 CALL v_movtos.clear()
 LET totmov = 1

 DECLARE cmov CURSOR FOR
 SELECT a.fecemi,
        x.nommov,
        "", 
        y.lnktra||" / "||y.numrf1,
        d.nommed,
        a.canepq,
        NVL(g.nomepq,d.nommed), 
        0,
        0,
        a.userid,
        a.fecsis,
        a.horsis,
        "",
        a.tipope,
        a.canuni, 
        y.nomori,
        y.nomdes 
  FROM  inv_dtransac a,inv_mtransac y,inv_tipomovs x,
        inv_unimedid d,inv_products e,outer inv_empaques g
  WHERE y.lnktra = a.lnktra 
    AND y.estado = "V" 
    AND a.cditem = w_mae_pro.cditem
    AND a.actexi = 1 
    AND x.tipmov = a.tipmov 
    AND e.cditem = a.cditem 
    AND g.codepq = a.codepq 
    AND d.unimed = e.unimed 
  ORDER BY a.fecemi 

 LET mtotcar = 0 
 LET mtotabo = 0 
 FOREACH cmov INTO v_movtos[totmov].*,xtipope,xcanmov,xnomprv,xnomcli 
  -- Verificando tipo de operacion del movimiento
  CASE (xtipope)
   WHEN 1 LET v_movtos[totmov].morides = xnomprv -- Cargos
          LET mtotcar                  = mtotcar+xcanmov
          LET v_movtos[totmov].mcargos = xcanmov 
          LET v_movtos[totmov].mabonos = NULL
   WHEN 0 LET v_movtos[totmov].morides = xnomcli -- Abonos
          LET mtotabo                  = mtotabo+xcanmov
          LET v_movtos[totmov].mcargos = NULL
          LET v_movtos[totmov].mabonos = xcanmov 
  END CASE

  -- Incrementando contador 
  LET totmov = (totmov+1)
 END FOREACH
 CLOSE cmov
 FREE  cmov
 LET totmov = (totmov-1)

 -- Verificando si hay movimientos 
 IF (totmov>0) THEN
    -- Desplegando movimientos
    DISPLAY ARRAY v_movtos TO s_movtos.* 
      ATTRIBUTE(COUNT=totmov,ACCEPT=FALSE) 

     ON ACTION cancel
      -- Regresando a datos generales 
      LET fx = DIALOG.getForm()
      CALL fx.ensureFieldVisible("formonly.cditem")

      EXIT DISPLAY 
    END DISPLAY
 ELSE
    CALL fgl_winmessage(
    "Atencion",
    "Producto sin movimientos registrados.",
    "warning")
 END IF 

 -- limpiando movimientos
 CALL v_movtos.clear()
END FUNCTION 

-- Subrutina para cargar el detalle de compras

FUNCTION invqbe010_DetalleCompras() 
 DEFINE xtipope LIKE inv_dtransac.tipope,
        xnomprv LIKE inv_provedrs.nomprv,
        xcanmov DEC(14,2),
        ctotcom DEC(14,2),
        fx      ui.Form

 -- Cargando compras
 CALL v_compras.clear()
 LET totcmp = 1

 DECLARE ccmp CURSOR FOR
 SELECT a.fecemi,
        y.nomori, 
        y.lnktra||" / "||y.numrf1,
        d.nommed,
        a.canepq,
        NVL(g.nomepq,d.nommed), 
        a.canuni,
        a.preuni,
        a.totpro, 
        a.userid,
        a.fecsis,
        a.horsis,
        ""
  FROM  inv_dtransac a,inv_mtransac y,inv_tipomovs x,
        inv_unimedid d,inv_products e,outer inv_empaques g
  WHERE y.lnktra = a.lnktra 
    AND y.tipmov IN (SELECT t.valchr FROM glb_paramtrs t 
                      WHERE t.numpar = 13 
                        AND t.tippar = 5 
                        AND t.valchr = y.tipmov) 
    AND y.estado = "V" 
    AND a.cditem = w_mae_pro.cditem
    AND a.actexi >= 0 
    AND x.tipmov = a.tipmov 
    AND e.cditem = a.cditem 
    AND g.codepq = a.codepq 
    AND d.unimed = e.unimed 
  ORDER BY a.fecemi 

 LET ctotcom = 0 
 FOREACH ccmp INTO v_compras[totcmp].*
  -- Totalizando
  LET ctotcom = (ctotcom+v_compras[totcmp].ctotpro)
  LET totcmp  = (totcmp+1)
 END FOREACH
 CLOSE ccmp
 FREE  ccmp
 LET totcmp = (totcmp-1)
 DISPLAY BY NAME ctotcom 

 -- Verificando si hay compras
 IF (totcmp>0) THEN
    -- Desplegando compras
    DISPLAY ARRAY v_compras TO s_compras.* 
      ATTRIBUTE(COUNT=totcmp,ACCEPT=FALSE) 

     ON ACTION cancel
      -- Regresando a datos generales 
      LET fx = DIALOG.getForm()
      CALL fx.ensureFieldVisible("formonly.cditem")

      EXIT DISPLAY 
    END DISPLAY
 ELSE
    CALL fgl_winmessage(
    "Atencion",
    "Producto sin compras registrados.",
    "warning")
 END IF 

 -- Limpiando compras 
 CALL v_compras.clear()
END FUNCTION 

-- Subrutina para cargar el detalle de kardex

FUNCTION invqbe010_DetalleKardex() 
 DEFINE xtipope LIKE inv_dtransac.tipope,
        xnomcli LIKE fac_clientes.nomcli, 
        xnomprv LIKE inv_provedrs.nomprv,
        xcanmov DEC(14,2),
        ktotcar DEC(14,2),
        ktotabo DEC(14,2),
        ksalpro DEC(14,2),
        saldo   DEC(14,2),
        fx      ui.Form

 -- Cargando kardex
 CALL v_kardex.clear()
 LET totkrx = 1

 DECLARE ckrx CURSOR FOR
 SELECT a.fecemi,
        x.nommov,
        "", 
        y.lnktra||" / "||y.numrf1,
        d.nommed,
        a.canepq,
        NVL(g.nomepq,d.nommed), 
        0,
        0, 
        0, 
        a.userid,
        a.fecsis,
        a.horsis,
        "",
        a.tipope,
        a.canuni, 
        y.nomori,
        y.nomdes,
        a.lnktra 
  FROM  inv_dtransac a,inv_mtransac y,inv_tipomovs x,
        inv_unimedid d,inv_products e,outer inv_empaques g
  WHERE y.lnktra = a.lnktra 
    AND y.estado = "V" 
    AND a.cditem = w_mae_pro.cditem
    AND a.actexi = 1 
    AND x.tipmov = a.tipmov 
    AND e.cditem = a.cditem 
    AND g.codepq = a.codepq 
    AND d.unimed = e.unimed 
  ORDER BY a.fecemi,a.lnktra 

 LET saldo   = 0
 LET ktotcar = 0 
 LET ktotabo = 0 
 FOREACH ckrx INTO v_kardex[totkrx].*,xtipope,xcanmov,xnomprv,xnomcli 
  -- Verificando tipo de operacion del movimiento
  CASE (xtipope)
   WHEN 1 LET v_kardex[totkrx].korides = xnomprv -- Cargos
          LET v_kardex[totkrx].kcargos = xcanmov 
          LET v_kardex[totkrx].kabonos = NULL
          LET saldo                    = saldo+xcanmov
          LET ktotcar                  = ktotcar+xcanmov
   WHEN 0 LET v_kardex[totkrx].korides = xnomcli -- Abonos
          LET v_kardex[totkrx].kcargos = NULL
          LET v_kardex[totkrx].kabonos = xcanmov 
          LET saldo                    = saldo-xcanmov 
          LET ktotabo                  = ktotabo+xcanmov
  END CASE

  -- Asignando saldo 
  LET v_kardex[totkrx].ksalact = saldo 

  -- Incrementando contador 
  LET totkrx = (totkrx+1)
 END FOREACH
 CLOSE ckrx
 FREE  ckrx
 LET totkrx = (totkrx-1)

 -- Desplegando totales
 LET ksalpro = ktotcar-ktotabo
 DISPLAY BY NAME ktotcar,ktotabo,ksalpro 

 -- Verificando si hay movimientos de kardex  
 IF (totkrx>0) THEN
    -- Desplegando movimientos
    DISPLAY ARRAY v_kardex TO s_kardex.* 
      ATTRIBUTE(COUNT=totkrx,ACCEPT=FALSE) 

     ON ACTION cancel
      -- Regresando a datos generales 
      LET fx = DIALOG.getForm()
      CALL fx.ensureFieldVisible("formonly.cditem")

      EXIT DISPLAY
    END DISPLAY
 ELSE
    CALL fgl_winmessage(
    "Atencion",
    "Producto sin movimientos de kardex registrados.",
    "warning")
 END IF 

 -- Limpiando movimientos
 CALL v_kardex.clear()
END FUNCTION 

-- Subrutina para cargar el detalle de los ingresos x peso

FUNCTION invqbe010_IngresosPeso() 
 DEFINE fx              ui.Form,
        totpes          LIKE inv_dtransac.canuni, 
        loop            SMALLINT, 
        ifecini,ifecfin DATE

 -- Asignando periodo default
 LET ifecini = TODAY
 LET ifecfin = TODAY 

 -- Inicio del loop 
 LET loop = TRUE 
 WHILE loop
  -- Cargando ingresos
  CALL v_ingxpeso.clear()
  LET toting = 1
  LET totpes = 0 

  -- Seleccionando ingresos
  DECLARE curpsj CURSOR FOR
  SELECT a.fecpro,
         a.numetq,
         a.diavig,
         a.fecven,
         a.pbruto,
         a.pstara,
         a.canpes,
         a.userid,
         a.fecsis,
         a.horsis 
   FROM  inv_pesajpro a 
   WHERE a.cditem = w_mae_pro.cditem
     AND a.fecpro BETWEEN ifecini AND ifecfin 
     AND a.ixpeso = 1 
   ORDER BY a.fecpro,a.numetq 

  FOREACH curpsj INTO v_ingxpeso[toting].*
   -- Totalizando peso
   LET totpes = (totpes+v_ingxpeso[toting].ipsneto) 

   -- Incrementando contador 
   LET toting = (toting+1)
  END FOREACH
  CLOSE curpsj
  FREE  curpsj
  LET toting = (toting-1)

  -- Desplegnando mensaje si no hay imgresos
  IF (toting<=0) THEN
   ERROR "Atencion: producto sin ingresos por peso registrados en el periodo."
  END IF 

  -- Viendo ingresos
  DISPLAY BY NAME ifecini,ifecfin,totpes 
  DISPLAY toting TO numpes 
  DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

     -- Desplegando ingresos 
     DISPLAY ARRAY v_ingxpeso TO s_ingxpeso.* 
      ATTRIBUTE(COUNT=toting) 
    
      BEFORE DISPLAY 
       CALL DIALOG.setActionHidden("close",TRUE)
     END DISPLAY
 
     -- Seleccionando parametros
     INPUT BY NAME ifecini,ifecfin 
      ATTRIBUTE(WITHOUT DEFAULTS=TRUE) 
     END INPUT 

     ON ACTION cancel
      -- Salida 
      -- Regresando a datos generales 
      LET fx = DIALOG.getForm()
      CALL fx.ensureFieldVisible("formonly.cditem")
      LET loop = FALSE 
      EXIT DIALOG 
  END DIALOG 
 END WHILE 

 -- limpiando inresos 
 CALL v_ingxpeso.clear()
END FUNCTION 
