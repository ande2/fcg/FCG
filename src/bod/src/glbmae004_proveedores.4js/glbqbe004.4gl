{
glbqbe004.4gl 
MRS 
Mantenimiento de proveedores de producto 
}

{ Definicion de variables globales }

GLOBALS "glbglo004.4gl" 
DEFINE totlin,totpag     INT
DEFINE filename,pipeline STRING 

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe004_proveedores(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        titmenu             STRING,
        qry                 STRING,
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de paises
  CALL librut003_cbxpaises() 

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL glbqbe004_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae004_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codprv,a.nomprv,a.numnit,a.numtel,
                                a.dirprv,a.nomcon,a.bemail,a.tipprv,
                                a.observ,a.userid,a.fecsis,a.horsis
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel       
     -- Salida
     CALL glbmae004_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codprv,a.nomprv,a.numnit ",
                 " FROM inv_provedrs a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_provedrs SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_provedrs.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_provedrs INTO v_provedrs[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_provedrs
   FREE  c_provedrs
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL glbqbe004_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_provedrs TO s_provedrs.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL glbmae004_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL glbmae004_inival(1)
      EXIT DISPLAY

     ON ACTION modificar
      -- Modificando 
      IF glbmae004_proveedores(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL glbqbe004_datos(v_provedrs[ARR_CURR()].tcodprv)
      END IF 

     ON KEY (CONTROL-M)
      -- Modificando 
      IF (operacion=2) THEN
       IF glbmae004_proveedores(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL glbqbe004_datos(v_provedrs[ARR_CURR()].tcodprv)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF glbqbe004_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este proveedor ya tiene registros. \n Proveedor no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar este proveedor ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL glbmae004_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1]  = "Proveedor"
      LET arrcols[2]  = "Nombre"
      LET arrcols[3]  = "Numero de NIT"
      LET arrcols[4]  = "Numero Telefono"
      LET arrcols[5]  = "Direccion"
      LET arrcols[6]  = "Nombre Contacto"
      LET arrcols[7]  = "Direccion Email"
      LET arrcols[8]  = "Tipo Proveedor"
      LET arrcols[9]  = "Usuario Registro"
      LET arrcols[10] = "Fecha Registro"
      LET arrcols[11] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = 
        "SELECT a.codprv,TRIM(a.nomprv),",
               "\"'\"||a.numnit,a.numtel,",
               "TRIM(a.dirprv),a.nomcon,a.bemail,",
               "CASE (a.tipprv) WHEN 1 THEN 'LOCAL' WHEN 2 THEN 'EXTRANJERO' END,",
               "a.userid,a.fecsis,a.horsis",
        " FROM inv_provedrs a ",
        " WHERE ",qrytext CLIPPED,
        " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res = librut002_excelreport("Proveedores",qry,12,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL glbqbe004_datos(v_provedrs[ARR_CURR()].tcodprv)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen proveedores con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Desplegando estado del menu
  CALL glbqbe004_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION glbqbe004_datos(wcodprv)
 DEFINE wcodprv LIKE inv_provedrs.codprv,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  inv_provedrs a "||
              "WHERE a.codprv = "||wcodprv||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_provedrst SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_provedrst INTO w_mae_pro.*
 END FOREACH
 CLOSE c_provedrst
 FREE  c_provedrst

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomprv THRU w_mae_pro.observ 
 DISPLAY BY NAME w_mae_pro.codprv,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para verificar si el proveedor ya tiene registros

FUNCTION glbqbe004_integridad()
 DEFINE conteo SMALLINT

 -- Verificando productos
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_products a
  WHERE (a.codprv = w_mae_pro.codprv) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     SELECT COUNT(*)
      INTO  conteo
      FROM  inv_mtransac a
      WHERE (a.codori = w_mae_pro.codprv) 
      IF (conteo>0) THEN
         RETURN TRUE
      ELSE
         RETURN FALSE
      END IF 
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION glbqbe004_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Proveedores - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Proveedores - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Proveedores - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Proveedores - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Proveedores - NUEVO")
 END CASE
END FUNCTION
