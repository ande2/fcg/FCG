{ 
glbglo004.4gl
Mantenimiento de proveedores de producto 
MRS 
Enero 2011 
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae004"
DEFINE w_mae_pro   RECORD LIKE inv_provedrs.*,
       v_provedrs  DYNAMIC ARRAY OF RECORD
        tcodprv    LIKE inv_provedrs.codprv,
        tnomprv    LIKE inv_provedrs.nomprv, 
        tnumnit    LIKE inv_provedrs.numnit, 
        tendrec    CHAR(1)
       END RECORD, 
       username    VARCHAR(15)
END GLOBALS

