{
Programo : Mynor Ramirez 
Objetivo : Programa de mantenimiento de platos de comida. 
}

-- Definicion de variables globales

GLOBALS "sreglb001.4gl"
CONSTANT PermiteRepetidos = FALSE 
CONSTANT MaxPorIsv        = 30 
DEFINE   regreso         SMALLINT,
         existe          SMALLINT,
         msg             STRING

-- Subrutina principal
MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/ToolBarPlatosComida")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("platoscomida")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de opciones
 CALL sreing001_menu()
END MAIN

-- Subutina para el menu de platos de comida

FUNCTION sreing001_menu()
 DEFINE regreso    SMALLINT, 
        wpais      VARCHAR(255), 
        titulo     STRING 

 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing001a AT 5,2  
  WITH FORM "sreing001a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ 

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header(progname,wpais,1)

  -- Inicializando datos 
  CALL sreing001_inival(1)

  -- Cargando combobox de puntos de venta
  CALL librut003_cbxpuntosventa()
  -- Cargando combobox de divisiones de comidas 
  CALL librut003_cbxdivrecetas()

  -- Menu de opciones
  MENU "Opciones" 
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Consultar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Ingresar"
    END IF
   COMMAND "Consultar" 
    "Consulta de platos existentes."
    CALL sreqbx001_platos(1)
   COMMAND "Ingresar" 
    "Ingreso de nuevos platos."
    CALL sreing001_platos(1) 
   COMMAND "Salir"
    "Salir al menu." 
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU

 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso de los datos generales del plato (encabezado) 

FUNCTION sreing001_platos(operacion)
 DEFINE retroceso         SMALLINT,
        operacion,opc     SMALLINT,
        loop,existe,i     SMALLINT,
        conteo            SMALLINT

 -- Verificando topo de operacion 
 IF (operacion=1) THEN
    CALL sreqbx001_EstadoMenu(4,"")
    LET retroceso = FALSE
 ELSE
    CALL sreqbx001_EstadoMenu(2," ")
    LET retroceso = TRUE
 END IF 

 -- Inicio del loop
 LET loop  = TRUE
 WHILE loop   

  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos 
     CALL sreing001_inival(1) 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_tra.coddiv,
                w_mae_tra.nomrec, 
                w_mae_tra.cospre, 
                w_mae_tra.prevta, 
                w_mae_tra.porisv, 
                w_mae_tra.numpos,
                w_mae_tra.enmenu,  
                w_mae_tra.combeb,
                w_mae_tra.bebref, 
                w_mae_tra.receta WITHOUT DEFAULTS
    ATTRIBUTES(UNBUFFERED) 

   ON ACTION cancel 
    -- Salida 
    IF (operacion=1) THEN  -- Ingreso 
     IF INFIELD(coddiv) THEN
        LET loop = FALSE
        CALL sreing001_inival(1)
        EXIT INPUT
     ELSE
        CALL Dialog.SetFieldActive("coddiv",TRUE) 
        CALL sreing001_inival(1)
        NEXT FIELD coddiv 
     END IF 
    ELSE                   -- Modifidacion 
     LET loop = FALSE 
     EXIT INPUT 
    END IF 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   AFTER FIELD coddiv
    -- Verificando division de comida 
    IF w_mae_tra.coddiv IS NULL THEN
       ERROR "Error: division de comida invalida, VERIFICA." 
       NEXT FIELD coddiv
    END IF 

   AFTER FIELD nomrec
    -- Verificando nombre del plato 
    IF (LENGTH(w_mae_tra.nomrec)<=0)THEN 
       IF w_mae_tra.nomrec IS NULL THEN
          ERROR "Error: nombre del plato invalido, VERIFICA."
          NEXT FIELD nomrec 
       END IF 
    END IF

   AFTER FIELD cospre
    -- Verificando costo de preparacion
    IF (w_mae_tra.cospre<0) OR
       w_mae_tra.cospre IS NULL THEN
       ERROR "Error: costo de preparacion invalido, VERIFICA."
       NEXT FIELD cospre 
    END IF   
 
   AFTER FIELD prevta
    -- Verificando precio de venta
    IF (w_mae_tra.prevta<0) OR
       w_mae_tra.prevta IS NULL THEN
       ERROR "Error: precio de venta invalido, VERIFICA."
       NEXT FIELD prevta 
    END IF 

   AFTER FIELD porisv 
    -- Verificando porcentaje de isv 
    IF (w_mae_tra.porisv<0) OR
       (w_mae_tra.porisv>MaxPorIsv) OR
       w_mae_tra.porisv IS NULL THEN
       ERROR "Error: porcentaje de impuesto sobre venta invalido, VERIFICA."
       NEXT FIELD porisv 
    END IF 

   AFTER FIELD enmenu
    -- Verificando producto en menu
    IF w_mae_tra.enmenu IS NULL THEN
       ERROR "Error: plato en menu invalido, VERIFICA."
       NEXT FIELD enmenu
    END IF 

   AFTER FIELD combeb 
    -- Verificando si plato combo se marca con complemento
    IF w_mae_tra.combeb IS NULL THEN
       ERROR "Error: plato combo con complemento invalido, VERIFICA."
       NEXT FIELD combeb 
    END IF 

   AFTER FIELD bebref 
    -- Verificando si plato es complemento de otros          
    IF w_mae_tra.bebref IS NULL THEN
       ERROR "Error: plato complemento invalido, VERIFICA."
       NEXT FIELD bebref
    END IF 

   AFTER FIELD numpos 
    -- Verificando orden del plato en menu
    IF w_mae_tra.numpos IS NULL THEN
       ERROR "Error: orden del plato invalido, VERIFICA."
       NEXT FIELD numpos 
    END IF 

   AFTER FIELD receta
    -- Verificando plato tiene receta 
    IF w_mae_tra.receta IS NULL THEN
       ERROR "Error: plato tiene receta invalido, VERIFICA."
       NEXT FIELD receta 
    END IF 
   
   AFTER INPUT 
    -- Verificando precio de costo versus precio de venta
    IF (w_mae_tra.prevta<w_mae_tra.cospre) THEN
       CALL fgl_winmessage(
       "Atencion:",
       "Precio de venta no puede ser menor al costo de preparacion.",
       "Stop")
       NEXT FIELD cospre
    END IF 
  END INPUT 
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Ingresando detalle de la receta del plato de comida 
  -- Verificando si plato tiene receta 
  IF w_mae_tra.receta THEN 
     LET retroceso = sreing001_detingreso(0,operacion)
  ELSE 
     -- Si no tiene receta grabar el plato 
     -- Menu de opciones
     LET opc = librut001_menugraba("Confirmacion",
                                   "Que desea hacer?",
                                   "Guardar",
                                   "Modificar",
                                   "Cancelar",
                                   "")

     -- Verificando opcion 
     CASE (opc)
      WHEN 0 -- Cancelando
       LET retroceso = FALSE
      WHEN 1 -- Grabando
       LET retroceso = FALSE

       -- Grabando plato de comida 
       CALL sreing001_grabar(operacion)

      WHEN 2 -- Modificando
       LET retroceso = TRUE 
     END CASE
  END IF

  -- Verificando operacion
  IF (operacion=2) THEN
     EXIT WHILE 
  END IF 
 END WHILE

 -- Inicializando datos 
 IF (operacion=1) THEN 
    CALL sreqbx001_EstadoMenu(0,"")
    CALL sreing001_inival(1) 
 ELSE 
    CALL sreqbx001_EstadoMenu(0," ")
 END IF 
END FUNCTION

-- Subutina para el ingreso del detalle de productos de la receta del plato (detalle) 

FUNCTION sreing001_detingreso(modifica,operacion)
 DEFINE w_mae_art    RECORD LIKE inv_products.*,
        w_mae_uni    RECORD LIKE inv_unimedid.*,
        loop,arr,scr SMALLINT,
        opc,repetido SMALLINT, 
        retroceso    SMALLINT, 
        operacion    SMALLINT, 
        modifica,i   SMALLINT,
        conteo       SMALLINT,
        lastkey      INTEGER 

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop
  -- Ingresando productos
  INPUT ARRAY v_platos WITHOUT DEFAULTS FROM s_platos.*
   ATTRIBUTE(COUNT=totlin,INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,
             UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION accept
    -- Salida
    EXIT INPUT 

   ON ACTION cancel
    -- Regreso
    LET loop      = FALSE
    LET retroceso = TRUE
    EXIT INPUT  

   ON ACTION listaproducto
    -- Asignando posicion actual
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Seleccionado lista de productos
    CALL librut002_formlist("Consulta de Productos",
                            "Producto",
                            "Descripcion del Producto",
                            "Precio Unitario",
                            "codabr",
                            "despro",
                            "presug",
                            "inv_products",
                            "estado=1 and status=1",
                            2,
                            1,
                            1)
    RETURNING v_platos[arr].codabr,v_platos[arr].dsitem,regreso
    IF regreso THEN
       NEXT FIELD codabr
    ELSE
       -- Asignando descripcion
       DISPLAY v_platos[arr].codabr TO s_platos[scr].codabr
       DISPLAY v_platos[arr].dsitem TO s_platos[scr].dsitem
    END IF

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   BEFORE INPUT
    -- Desabilitando botones de append 
    CALL DIALOG.setActionHidden("append",TRUE)

   BEFORE ROW
    -- Asignando total de lineas
    LET totlin = ARR_COUNT()

   BEFORE FIELD codabr
    -- Habilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",TRUE)

   AFTER FIELD codabr
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) THEN
       NEXT FIELD codabr
    END IF

   AFTER FIELD unimed
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) THEN
       NEXT FIELD unimed
    END IF

   BEFORE FIELD canuni 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Obteniendo lista de productos
    IF v_platos[arr].codabr IS NULL THEN 
     -- Seleccionado lista de productos
     CALL librut002_formlist("Consulta de Productos",
                             "Producto",
                             "Descripcion del Producto",
                             "Precio Unitario",
                             "codabr",
                             "despro",
                             "presug",
                             "inv_products",
                             "estado=1 and status=1",
                             2,
                             1,
                             1)
     RETURNING v_platos[arr].codabr,v_platos[arr].dsitem,regreso
     IF regreso THEN
        NEXT FIELD codabr
     ELSE 
        -- Asignando descripcion
        DISPLAY v_platos[arr].codabr TO s_platos[scr].codabr 
        DISPLAY v_platos[arr].dsitem TO s_platos[scr].dsitem 
     END IF 
    END IF 

    -- Verificando producto
    IF (v_platos[arr].codabr <=0) THEN 
       CALL fgl_winmessage(
       " Atencion:",
       " Producto invalido VERIFICA.",
       "stop")
       INITIALIZE v_platos[arr].codabr THRU v_platos[arr].canuni TO NULL
        CLEAR s_platos[scr].codabr,s_platos[scr].dsitem,
              s_platos[scr].nomuni,s_platos[scr].canuni
        NEXT FIELD codabr 
    END IF 

    -- Verificando si el producto existe
    INITIALIZE w_mae_art.* TO NULL
    CALL librut003_bproductoabr(v_platos[arr].codabr)
    RETURNING w_mae_art.*,existe
    IF NOT existe THEN
       CALL fgl_winmessage(
       " Atencion:",
       " Producto no registrado VERIFICA.",
       "stop")
       INITIALIZE v_platos[arr].codabr THRU v_platos[arr].canuni TO NULL
        CLEAR s_platos[scr].codabr,s_platos[scr].dsitem,
              s_platos[scr].nomuni,s_platos[scr].canuni
        NEXT FIELD codabr 
    END IF 

    -- Asignando descripcion
    LET v_platos[arr].dsitem = w_mae_art.despro 
    LET v_platos[arr].unimed = w_mae_art.unimed 
    DISPLAY v_platos[arr].dsitem TO s_platos[scr].dsitem 

    -- Obteniendo datos de la unidad de medida
    INITIALIZE w_uni_med.* TO NULL
    CALL librut003_bumedida(w_mae_art.unimed)
    RETURNING w_uni_med.*,existe

    -- Asignando unidad de medida
    LET v_platos[arr].nomuni = w_uni_med.nommed
    DISPLAY v_platos[arr].nomuni TO s_platos[scr].nomuni
   
    -- Verificando productos repetidos
    IF NOT PermiteRepetidos THEN 
     LET repetido = FALSE
     FOR i = 1 TO totlin
      IF v_platos[i].codabr IS NULL THEN
         CONTINUE FOR
      END IF

      -- Verificando producto
      IF (i!=arr) THEN 
       IF (v_platos[i].codabr = v_platos[arr].codabr) THEN
          LET repetido = TRUE
          EXIT FOR
       END IF 
      END IF
     END FOR

     -- Si hay repetido
     IF repetido THEN
        LET msg = "Producto ya registrado en el detalle. "||
                  "Linea (",i USING "<<<",") \nVERIFICA." 
        CALL fgl_winmessage(" Atencion",msg,"stop")
        INITIALIZE v_platos[arr].codabr THRU v_platos[arr].canuni TO NULL
        CLEAR s_platos[scr].codabr,s_platos[scr].dsitem,
              s_platos[scr].nomuni,s_platos[scr].canuni
        NEXT FIELD codabr 
     END IF
    END IF 

    -- Verificando estado del producto
    IF NOT w_mae_art.estado  THEN
       CALL fgl_winmessage(
       " Atencion",
       " Producto de BAJA no puede utilizarse, VERIFICA.",
       "stop")
       INITIALIZE v_platos[arr].codabr THRU v_platos[arr].canuni TO NULL
        CLEAR s_platos[scr].codabr,s_platos[scr].dsitem,
              s_platos[scr].nomuni,s_platos[scr].canuni
        NEXT FIELD codabr 
    END IF 

    -- Verificando estatus del producto
    IF NOT w_mae_art.status THEN
       CALL fgl_winmessage(
       " Atencion",
       " Producto BLOQUEADO no puede utilizarse, VERIFICA.",
       "stop")
       INITIALIZE v_platos[arr].codabr THRU v_platos[arr].canuni TO NULL
        CLEAR s_platos[scr].codabr,s_platos[scr].dsitem,
              s_platos[scr].nomuni,s_platos[scr].canuni
        NEXT FIELD codabr 
    END IF 

    -- Totalizando productos 
    CALL sreing001_totdet()

    -- Deshabilitando la opcion de aceptar
    CALL DIALOG.SetActionActive("accept",FALSE) 

   AFTER FIELD canuni  
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("up")) OR 
       (lastkey = FGL_KEYVAL("down")) THEN
       NEXT FIELD canuni 
    END IF
   
    -- Verificano cantida
    IF v_platos[arr].canuni IS NULL OR 
       v_platos[arr].canuni <=0 THEN   
       ERROR "Error: cantidad invalida, VERIFICA."
       NEXT FIELD canuni 
    END IF 

   AFTER FIELD entrga
    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) THEN
       NEXT FIELD entrga 
    END IF

   AFTER INPUT 
    -- Totalizando productos 
    CALL sreing001_totdet()

   AFTER ROW,INSERT,DELETE 
    LET totlin = ARR_COUNT()

    -- Totalizando unidades
    CALL sreing001_totdet()

   ON ACTION delete 
    -- Eliminando linea seleccionada
    LET arr = ARR_CURR()
    CALL v_platos.deleteElement(arr)
    LET totlin = ARR_COUNT()

    -- Totalizando unidades
    CALL sreing001_totdet()
    NEXT FIELD codabr

  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Verificando que se ingrese al menos un producto
  IF (totuni<=0) THEN
     CALL fgl_winmessage(
     "Atencion",
     "Debe ingresarse al menos un producto en el detalle de la receta.",
     "stop")
     CONTINUE WHILE
  END IF

  -- Menu de opciones
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop      = FALSE
    LET retroceso = FALSE
   WHEN 1 -- Grabando
    LET loop      = FALSE
    LET retroceso = FALSE

    -- Grabando plato de comida 
    CALL sreing001_grabar(operacion)

   WHEN 2 -- Modificando
    LET loop = TRUE 
  END CASE
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subrutina para totalizar el detalle de productos 

FUNCTION sreing001_totdet()
 DEFINE i INT

 -- Totalilzando detalle
 LET totuni = 0
 FOR i=1 TO totlin
  IF v_platos[i].codabr IS NULL OR
     v_platos[i].canuni IS NULL THEN
     CONTINUE FOR
  END IF 
  LET totuni = (totuni+1)
 END FOR

 -- Desplegando totales
 LET msg = "Total Productos en Receta ("||totuni||")" 
 CALL librut001_DPElement("totalproductos",msg) 
END FUNCTION 

-- Subrutina para grabar el plato de comida 

FUNCTION sreing001_grabar(operacion)
 DEFINE w_mae_art    RECORD LIKE inv_products.*, 
        operacion    SMALLINT,
        conteo       SMALLINT,
        i,correl     SMALLINT,
        xnumrec      INT 

 -- Grabando transaccion
 ERROR " Registrando Plato de Comida ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

  -- 1. Grabando datos generales del plato (encabezado) 
  -- Asignando datos
  IF (operacion=1) THEN -- ingreso
     LET w_mae_tra.lnkrec = 0
 
     -- Obteniendo numero maximo de receta 
     SELECT NVL(MAX(a.lnkrec),0)+1
      INTO  xnumrec
      FROM  sre_mrecetas a               
      LET w_mae_tra.numrec = "M-",xnumrec USING "&&&&&"

     SET LOCK MODE TO WAIT
     INSERT INTO sre_mrecetas
     VALUES (w_mae_tra.*)
     LET w_mae_tra.lnkrec = SQLCA.SQLERRD[2]
  ELSE
     -- Actualizando datos generale del plato (encabezado) 
     SET LOCK MODE TO WAIT
     UPDATE sre_mrecetas
     SET    sre_mrecetas.*      = w_mae_tra.*
     WHERE  sre_mrecetas.lnkrec = w_mae_tra.lnkrec

     -- Eliminando datos del detalle antes de grabar
     SET LOCK MODE TO WAIT
     DELETE FROM sre_drecetas
     WHERE sre_drecetas.lnkrec = w_mae_tra.lnkrec
  END IF

  -- 2. Grabando detalle de productos de la receta del plato de comida (detalle)
  LET correl = 0 
  FOR i = 1 TO totlin 
   IF v_platos[i].codabr IS NULL OR 
      v_platos[i].canuni IS NULL THEN 
      CONTINUE FOR 
   END IF 
   LET correl = (correl+1) 

   -- Obteniendo datos del producto
   INITIALIZE w_mae_art.* TO NULL
   CALL librut003_bproductoabr(v_platos[i].codabr)
   RETURNING w_mae_art.*,existe

   -- Grabando
   SET LOCK MODE TO WAIT
   INSERT INTO sre_drecetas
   VALUES (w_mae_tra.lnkrec   , -- id de la receta 
           "P"                , -- tingre 
           w_mae_art.cditem   , -- codigo del producto 
           v_platos[i].codabr , -- codigo del producto
           v_platos[i].unimed , -- codigo de la unidad de medida
           v_platos[i].canuni , -- cantidad unidad 
           correl             , -- correlativo 
           v_platos[i].entrga)  -- tipo de entrega
  END FOR 

  -- Desplegando datos de la operacion 
  CASE (operacion)
   WHEN 1 LET msg = "Plato de Comida Registrado [ENTER] para continuar" 
   WHEN 2 LET msg = "Plato de Comida Actualizado [ENTER] para continuar" 
  END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 DISPLAY BY NAME w_mae_tra.lnkrec,w_mae_tra.numrec 
 CALL fgl_winmessage(" Atencion",msg,"information")
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION sreing001_inival(i)
 DEFINE i SMALLINT
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_tra.*  TO NULL 
   CLEAR FORM 
 END CASE 

 -- Inicializando datos
 LET totuni           = 0 
 LET w_mae_tra.cospre = 0
 LET w_mae_tra.prevta = 0
 LET w_mae_tra.porisv = 0 
 LET w_mae_tra.enmenu = 1
 LET w_mae_tra.combeb = 0 
 LET w_mae_tra.bebref = 0 
 LET w_mae_tra.receta = 0 
 LET w_mae_tra.numpos = 100
 LET w_mae_tra.userid = username 
 LET w_mae_tra.fecsis = CURRENT 
 LET w_mae_tra.horsis = CURRENT HOUR TO SECOND

 -- Inicializando vectores de datos
 CALL sreing001_inivec() 

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_tra.userid,w_mae_tra.fecsis,w_mae_tra.horsis,w_mae_tra.numrec 

 -- Desplegando totales
 LET msg = "Total Productos en Receta ("||totuni||")" 
 CALL librut001_DPElement("totalproductos",msg) 
END FUNCTION

-- Subrutina para inicializar y limpiar los vectores de trabajo 

FUNCTION sreing001_inivec()
 DEFINE i SMALLINT

 -- Inicializando vectores
 CALL v_platos.clear()

 LET totlin = 0 
 FOR i = 1 TO 5
  -- Limpiando vector       
  CLEAR s_platos[i].*
 END FOR 
END FUNCTION 
