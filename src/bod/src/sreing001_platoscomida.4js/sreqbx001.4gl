{ 
Programo : Fernando Lontero
Objetivo : Programa de criterios de seleccion para:
           Consulta/Eliminacion de platos de comida 
}

-- Definicion de variables globales 
GLOBALS "sreglb001.4gl" 

-- Subrutina para consultar/eliminar platos 

FUNCTION sreqbx001_platos(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     STRING,
        qry                 STRING,
        loop,existe,res     SMALLINT,
        operacion           SMALLINT,
        qryres,msg          CHAR(80),
        numeroregistros     INTEGER 

 -- Verificando operacion
 -- Desplegando estado de menu
 CASE (operacion)
  WHEN 1 CALL sreqbx001_EstadoMenu(1," ")
 END CASE

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Cargando combobox de divisiones de platos
 CALL librut003_cbxdivrecetas() 

 -- Buscando datos 
 LET loop = TRUE 
 WHILE loop 
  -- Inicializando las variables 
  INITIALIZE qrytext,qrypart TO NULL
  LET int_flag = 0 
  CLEAR FORM

  -- Construyendo busqueda 
  CONSTRUCT BY NAME qrytext 
                 ON a.numrec,
                    a.coddiv,
                    a.nomrec,
                    a.cospre,
                    a.prevta,
                    a.porisv, 
                    a.numpos,
                    a.enmenu,
                    a.combeb, 
                    a.bebref, 
                    a.receta,
                    a.lnkrec,
                    a.userid,
                    a.fecsis,
                    a.horsis
   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT CONSTRUCT
  END CONSTRUCT
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Preparando la busqueda 
  ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

  -- Creando la busqueda 
  LET qrypart = "SELECT a.lnkrec ",
                 "FROM  sre_mrecetas a ",
                 "WHERE ",qrytext CLIPPED,
                 " ORDER BY 1" 

  -- Obteniendo numero de registros 
  LET numeroregistros = librut003_NumeroRegistros(qrypart)

  -- Declarando el cursor 
  PREPARE estqbe001 FROM qrypart
  DECLARE c_platos SCROLL CURSOR WITH HOLD FOR estqbe001  
  OPEN c_platos 
  FETCH FIRST c_platos INTO w_mae_tra.lnkrec
  IF (status = NOTFOUND) THEN
     INITIALIZE w_mae_tra.* TO NULL
     ERROR ""
     CALL fgl_winmessage(
     "Atencion",
     "No existen platos con el criterio seleccionado.",
     "stop") 
  ELSE
     ERROR "" 

     -- Desplegando datos 
     LET msg = " - Registros [ ",numeroregistros||" ]"
     CALL sreqbx001_EstadoMenu(operacion,msg)
     CALL sreqbx001_datos()

     -- Fetchando platos 
     MENU "Platos de Comida" 
      BEFORE MENU 
       -- Verificando permiso de la anulacion 
       -- Modificar  
       IF NOT seclib001_accesos(progname,2,username) THEN
          HIDE OPTION "Modificar" 
       END IF
       -- Eiminar    
       IF NOT seclib001_accesos(progname,3,username) THEN
          HIDE OPTION "delete"         
       END IF

      COMMAND "Siguiente"
       "Visualiza el siguiente plato en la lista."

       FETCH NEXT c_platos INTO w_mae_tra.lnkrec 
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas platos siguientes en lista.", 
           "information")
           FETCH LAST c_platos INTO w_mae_tra.lnkrec
       END IF 

       -- Desplegando datos 
       CALL sreqbx001_datos()

      COMMAND "Anterior"
       "Visualiza el plato anterior en la lista."
       FETCH PREVIOUS c_platos INTO w_mae_tra.lnkrec
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas platos anteriores en lista.", 
           "information")
           FETCH FIRST c_platos INTO w_mae_tra.lnkrec
        END IF

        -- Desplegando datos 
        CALL sreqbx001_datos()

      COMMAND "Primero" 
       "Visualiza el primer plato en la lista."
       FETCH FIRST c_platos INTO w_mae_tra.lnkrec
        -- Desplegando datos 
        CALL sreqbx001_datos()

      COMMAND "Ultimo" 
       "Visualiza el ultimo plato en la lista."
       FETCH LAST c_platos INTO w_mae_tra.lnkrec

       -- Desplegando datos
       CALL sreqbx001_datos()

      COMMAND "Modificar"
       "Permite modificar el plato actual en pantalla."
       -- Desplegando datos
       CALL sreqbx001_datos()

       -- Modificando plato de comida 
       CALL sreing001_platos(2)

       -- Desplegando datos
       CALL sreqbx001_datos()

      ON ACTION delete
       -- Eliminando plato de comida 
       IF sreqbx001_EliminarPlatoComida() THEN
          EXIT MENU
       END IF

      COMMAND "Receta"
       "Permite visualizar detalle de productos de receta del plato en pantalla."

       -- Verificando si hay producto en la receta
       IF (totlin>0) THEN 
          CALL sreqbx001_DetalleRecetas(0)
       ELSE
          CALL fgl_winmessage(
          "Atencion:","Plato de comida si receta de productos.","information")
       END IF 

      ON ACTION reporte
       -- Reporte de datos seleccionados a excel
       -- Asignando nombre de columnas
       LET arrcols[1] = "ID Plato"
       LET arrcols[2] = "Numero Plato"
       LET arrcols[3] = "Dvision Plato"
       LET arrcols[4] = "Nombre del Plato" 
       LET arrcols[5] = "Costo Preparacion"
       LET arrcols[6] = "Precio Venta"
       LET arrcols[7] = "Porcentaje ISV"
       LET arrcols[8] = "Posicion en Menu"
       LET arrcols[9] = "Plato en Menu" 
       LET arrcols[10]= "Plato es Combo" 
       LET arrcols[11]= "Plato Complemento" 
       LET arrcols[12]= "Plato Receta"
 
       -- Se aplica el mismo query de la seleccion para enviar al reporte
       LET qry = 
           "SELECT a.lnkrec,a.numrec,d.nomdiv,a.nomrec,a.cospre,a.prevta,",
                  "a.porisv,a.numpos,a.enmenu,",
                  "a.combeb,",
                  "a.bebref,",
                  "a.receta ",
            "FROM  sre_mrecetas a,sre_divrecet d ",
            "WHERE d.coddiv = a.coddiv AND ",qrytext CLIPPED,
            " ORDER BY 3,4" 

       -- Ejecutando el reporte
       LET res = librut002_excelreport("Platos de Comida",qry,12,0,arrcols)

      COMMAND "Consultar" 
       "Regresa a la pantalla de seleccion."
       EXIT MENU

      ON ACTION cancel 
       LET loop = FALSE 
       EXIT MENU 

      COMMAND KEY(F4,CONTROL-E)
       LET loop = FALSE 
       EXIT MENU
     END MENU
   END IF     
  CLOSE c_platos
 END WHILE

 -- Inicializando datos 
 CALL sreing001_inival(1)

 -- Desplegando estado de menu 
 CALL sreqbx001_EstadoMenu(0,msg)
END FUNCTION 

-- Subrutina para desplegar los datos del plato de comida 

FUNCTION sreqbx001_datos()
 DEFINE existe SMALLINT

 -- Obteniendo datos del plato de comida 
 CALL sreqbx001_BPlatosComida(w_mae_tra.lnkrec)
 RETURNING w_mae_tra.*,existe

 -- Desplegando datos 
 CLEAR FORM 
 DISPLAY BY NAME w_mae_tra.numrec,w_mae_tra.coddiv,
                 w_mae_tra.nomrec,w_mae_tra.cospre,
                 w_mae_tra.prevta,w_mae_tra.porisv,
                 w_mae_tra.enmenu,w_mae_tra.combeb,
                 w_mae_tra.bebref,w_mae_tra.receta,
                 w_mae_tra.userid,w_mae_tra.fecsis,
                 w_mae_tra.horsis,w_mae_tra.lnkrec,
                 w_mae_tra.numpos 

 -- Seleccionando detalle de la receta 
 CALL sreqbx001_DetalleRecetas(1)
END FUNCTION 

-- Subrutina para seleccionar los datos de la receta del plato 

FUNCTION sreqbx001_DetalleRecetas(dsp)
 DEFINE existe,dsp SMALLINT

 -- Inicializando vector de productos
 CALL sreing001_inivec()
 
 -- Seleccionando detalle de receta 
 DECLARE cdet CURSOR FOR
 SELECT x.codabr,	
        y.despro,
        y.unimed,
        z.nommed,
        x.canuni,
        x.entrga, 
        "",
        x.correl 
  FROM  sre_drecetas x,inv_products y,inv_unimedid z
  WHERE (x.lnkrec = w_mae_tra.lnkrec)
    AND (y.cditem = x.cditem)
    AND (y.unimed = z.unimed)
  ORDER BY x.correl
  
  LET totlin = 1 
  FOREACH cdet INTO v_platos[totlin].*
   -- Acumulando contador
   LET totlin = (totlin+1) 
  END FOREACH
  CLOSE cdet
  FREE  cdet
  LET totlin = (totlin-1)  

  -- Despelgando datos de detalle
  DISPLAY ARRAY v_platos TO s_platos.* 
   ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE) 
   BEFORE DISPLAY
    -- Verificando si es solo display
    IF dsp THEN 
       EXIT DISPLAY
    END IF 
  END DISPLAY 

  -- Desplegando totales
  CALL sreing001_totdet() 
END FUNCTION 

-- Subrutina para eliminar un plato de comida 

FUNCTION sreqbx001_EliminarPlatoComida()
 DEFINE opc,elimina SMALLINT,
        msg         STRING,
        conteo      INTEGER 

 -- Verificando integridad 
 SELECT COUNT(*) 
  INTO  conteo 
  FROM  sre_dordenes a
  WHERE a.lnkrec = w_mae_tra.lnkrec 
  IF (conteo>0) THEN 
     CALL fgl_winmessage(
     "Atencion:",
     "Plato de comida ya tiene registros, plato no puede eliminarse.",
     "stop") 
     RETURN FALSE   
  END IF 

 -- Verificando operacion
 LET msg = "Desea Eliminar el Plato de Comida?"
 IF librut001_yesornot("Confirmacion",msg,"Si","No","question") THEN
  LET elimina = TRUE

  -- Iniciando Transaccion
  BEGIN WORK

   -- Eliminando receta 
   SET LOCK MODE TO WAIT
   DELETE FROM sre_mrecetas
   WHERE (sre_mrecetas.lnkrec = w_mae_tra.lnkrec)

  -- Finalizando Transaccion
  COMMIT WORK

  LET msg = "Movimiento Eliminado."
 ELSE
  LET elimina = FALSE
  LET msg = "Eliminacion Cancelada."
 END IF

 -- Desplegando mensaje de accion
 CALL fgl_winmessage("Atencion",msg,"information")

 RETURN elimina
END FUNCTION

-- Subrutina para buscar los datos de un plato de comida (sre_mrecetas)

FUNCTION sreqbx001_BPlatosComida(wlnkrec)
 DEFINE w_mae_reg RECORD LIKE sre_mrecetas.*,
        wlnkrec   LIKE sre_mrecetas.lnkrec

 -- Buscando plato de comida 
 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  sre_mrecetas a
  WHERE (a.lnkrec= wlnkrec)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION sreqbx001_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("group1","Datos Generales del Plato - MENU")
  WHEN 1 CALL librut001_dpelement("group1","Datos Generales del Plato - CONSULTAR"||msg.trim())
  WHEN 2 CALL librut001_dpelement("group1","Datos Generales del Plato - MODIFICAR"||msg.trim())
  WHEN 3 CALL librut001_dpelement("group1","Datos Generales del Plato - ELIMINAR"||msg.trim())
  WHEN 4 CALL librut001_dpelement("group1","Datos Generales del Plato - INGRESAR")
 END CASE
END FUNCTION
