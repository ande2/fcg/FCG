{ 
Programo : Fernando Lontero
Objetivo : Programa de variables globales platos de comida 
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname  = "sreing001"
DEFINE w_mae_tra   RECORD LIKE sre_mrecetas.*,
       w_mae_act   RECORD LIKE sre_mrecetas.*,
       w_mae_reg   RECORD LIKE fac_puntovta.*,
       w_uni_med   RECORD LIKE inv_unimedid.*,
       v_platos    DYNAMIC ARRAY OF RECORD
        codabr     LIKE sre_drecetas.codabr, 
        dsitem     CHAR(50), 
        unimed     SMALLINT,
        nomuni     CHAR(50), 
        canuni     LIKE sre_drecetas.canuni,
        entrga     SMALLINT, 
        rellen     CHAR(1) 
       END RECORD, 
       totuni      SMALLINT,  
       reimpresion SMALLINT,
       totlin      SMALLINT,
       b           ui.ComboBox,
       cba         ui.ComboBox,
       w           ui.Window,
       f           ui.Form,
       maxdiamod   SMALLINT, 
       maxdiaeli   SMALLINT, 
       username    VARCHAR(15)
END GLOBALS
