{
Objetivo : Impresion de facturacion 
Programo : Mynor Ramirez 
Fecha    : Diciembre 2011 
}

-- Definicion de variables globales  

GLOBALS "facglb001.4gl" 
CONSTANT imprimevendedor = 0            
DEFINE w_mae_usr       RECORD LIKE glb_usuarios.*,
       fnt             RECORD
        cmp            CHAR(12),
        nrm            CHAR(12),
        tbl            CHAR(12),
        fbl,t88        CHAR(12),
        t66,p12        CHAR(12),
        p10,srp        CHAR(12),
        twd            CHAR(12),
        fwd            CHAR(12),
        tda,fda        CHAR(12),
        ini            CHAR(12)
       END RECorD,
       existe          SMALLINT, 
       filename        CHAR(90),
       pipeline        CHAR(90),
       i               INT

-- Subrutina para imprimir un movimiento de producto

FUNCTION facrpt001_facturacion(reimpresion)
 DEFINE reimpresion SMALLINT 

 IF EnterParaImprimir THEN
  CALL fgl_winmessage(" Atencion","Presione [ENTER] para imprimir el documento.","information")
 END IF 

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/facrpt001.spl"

 -- Asignando valores
 LET pipeline = "local"   

 -- Seleccionando fonts para impresora epson
 CALL librut001_fontsprn(pipeline,"epson") 
 RETURNING fnt.* 

 -- Iniciando reporte
 START REPORT facrpt001_printfac TO filename 
  OUTPUT TO REPORT facrpt001_printfac(1,reimpresion)
 FINISH REPORT facrpt001_printfac 

 -- Imprimiendo el reporte
 CALL librut001_sendreport(filename,pipeline,"","")
END FUNCTION 

-- Subrutinar para generar la impresion de la facturacion 

REPORT facrpt001_printfac(numero,reimpresion) 
 DEFINE imp1        RECORD LIKE fac_formaimp.*,
        xnomcaj     VARCHAR(40), 
        xnomven     VARCHAR(40), 
        exis,i,col  SMALLINT, 
        reimpresion SMALLINT,
        lh,lv,si    CHAR(1),
        sd,ii,id    CHAR(1),
        x           CHAR(1),
        xnumdoc, xnumdoc2     CHAR(40), 
        wcanletras  STRING,
        cortepapel  STRING, 
        abrircaja   STRING, 
        numero      INT 

   DEFINE reg_e RECORD
      estatus        LIKE facturafel_e.estatus,
      serie_e        LIKE facturafel_e.serie_e,
      numdoc_e       LIKE facturafel_e.numdoc_e,
      autorizacion   LIKE facturafel_e.autorizacion,
      fecha_em       LIKE facturafel_e.fecha_em
   END RECORD

  OUTPUT LEFT   MARGIN 0 
         PAGE   LENGTH 5 
         TOP    MARGIN 0 
         BOTTOM MARGIN 0

 FORMAT 
  BEFORE GROUP OF numero 
   -- Imprimiendo encabezado
   LET wcanletras = librut001_numtolet(w_mae_tra.totdoc) 

   -- Inicializando impresora
   SKIP 17 LINES 
   LET cortepapel = ASCII 29,ASCII 86,"0" 
   LET abrircaja  = ASCII 27,ASCII 112,"0" 

   -- Abriendo caja
   PRINT abrircaja CLIPPED

   -- Obteniendo datos del tipo de documento de facturacion
   INITIALIZE imp1.* TO NULL 
   SELECT a.* 
    INTO  imp1.* 
    FROM  fac_formaimp a
    WHERE a.lnktdc = w_mae_tra.lnktdc

   -- Obteniendo nombre del vendedor
   INITIALIZE xnomven TO NULL 
   SELECT a.nomven 
    INTO  xnomven 
    FROM  glb_vendedrs a
    WHERE a.codven = w_mae_tra.codven

   -- Obteniendo nombre del cajero
   INITIALIZE xnomcaj TO NULL 
   SELECT a.nomusr 
    INTO  xnomcaj 
    FROM  glb_usuarios a
    WHERE a.userid = w_mae_tra.userid 

   -- Encabezado
   LET xnumdoc = "DOCUMENTO TRIBUTARIO ELECTRONICO"
   LET col = librut001_centrado(xnumdoc,150) 
   PRINT COLUMN col,xnumdoc CLIPPED
   LET xnumdoc = "FACTURA"
   LET col = librut001_centrado(xnumdoc,150) 
   PRINT COLUMN col,xnumdoc CLIPPED

  -- SKIP 1 LINE 
   
   IF LENGTH(imp1.top001) >0 THEN 
      LET col = librut001_centrado(imp1.top001,150) 
      PRINT COLUMN col,imp1.top001 CLIPPED 
   END IF 
   IF LENGTH(imp1.top002) >0 THEN 
      LET col = librut001_centrado(imp1.top002,150) 
      PRINT COLUMN col,imp1.top002 CLIPPED 
   END IF 
   IF LENGTH(imp1.top003) >0 THEN 
      LET col = librut001_centrado(imp1.top003,150) 
      PRINT COLUMN col,imp1.top003 CLIPPED 
   END IF 
   IF LENGTH(imp1.top004) >0 THEN 
      LET col = librut001_centrado(imp1.top004,150) 
      PRINT COLUMN col,imp1.top004 CLIPPED 
   END IF 
   IF LENGTH(imp1.top005) >0 THEN 
      LET col = librut001_centrado(imp1.top005,150) 
      PRINT COLUMN col,imp1.top005 CLIPPED 
   END IF 
   {IF LENGTH(imp1.top006) >0 THEN 
      LET col = librut001_centrado(imp1.top006,150) 
      PRINT COLUMN col,imp1.top006 CLIPPED 
   END IF 

   SKIP 1 LINES} 

   -- Verificando si documento es reimpresion
   IF reimpresion THEN
      LET col = librut001_centrado(imp1.top006,150) 
      --PRINT COLUMN col, "*** RE-IMPRESION ***"
      PRINT COLUMN col, imp1.top006 CLIPPED 
      SKIP 1 LINES
   END IF 

   -- Datos del documento
   LET xnumdoc = "CONTROL INTERNO: ", w_tip_doc.title1 CLIPPED," ",
                 w_mae_tra.nserie CLIPPED,"-",
                 w_mae_tra.numdoc USING "&&&&&&&&&&"
   LET col = librut001_centrado(xnumdoc,100)
   --PRINT COLUMN col,xnumdoc CLIPPED
   PRINT COLUMN 001, xnumdoc CLIPPED,
         COLUMN 111, "Fecha:        ",w_mae_tra.fecemi," ",TIME
   
   SELECT a.estatus, a.serie_e, a.numdoc_e, a.autorizacion, a.fecha_em
      INTO reg_e.estatus, reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion, reg_e.fecha_em
      FROM facturafel_e a
      WHERE a.num_int = w_mae_tra.lnktra
   --DISPLAY BY NAME reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion

   {LET xnumdoc = "AUTORIZACION: ", reg_e.autorizacion
   LET col = librut001_centrado(xnumdoc2,150) 
   --PRINT COLUMN col,xnumdoc CLIPPED

   PRINT COLUMN 01, xnumdoc CLIPPED,}
   PRINT COLUMN 001, "AUTORIZACION : ", reg_e.autorizacion CLIPPED, 
         COLUMN 111, "Cajero:       ",xnomcaj CLIPPED

   LET xnumdoc = "SERIE: ", reg_e.serie_e CLIPPED, " NUMERO: ", reg_e.numdoc_e
   LET col = librut001_centrado(xnumdoc,150) 
   --PRINT COLUMN col,xnumdoc CLIPPED
   PRINT COLUMN 01, xnumdoc CLIPPED

   SKIP 1 LINES

   --PRINT "Fecha:        ",w_mae_tra.fecemi," ",TIME
   --PRINT "Cajero:       ",xnomcaj CLIPPED

   -- Verificando si imprime nombre de vendedor 
   IF imprimevendedor THEN 
      PRINT "Atendido Por: ",xnomven CLIPPED
   END IF 

   

   -- Verificando nombre del cliente 
   --IF LENGTH(w_mae_tra.nomcli)>20 THEN 
   --   PRINT "              ",w_mae_tra.nomcli[21,40] CLIPPED 
   --END IF 

   -- Verificando IDENTIFICACION TRIBUTARIO
   --IF LENGTH(w_mae_tra.numnit)>0 THEN
      PRINT "N.I.T.:       ",w_mae_tra.numnit CLIPPED
   --END IF 

   --PRINT "Cliente:      ",w_mae_tra.nomcli[1,20] CLIPPED 
   PRINT "Cliente:      ",w_mae_tra.nomcli CLIPPED --[1,40] CLIPPED 

   PRINT "Direccion:    ", w_mae_tra.dircli CLIPPED 
   --TODO: Tildes
   --TODO: ANULACION
   -- Detalle de Productos
   PRINT "___________________________________________________________________________________________________________________________________________________"
   PRINT COLUMN 001, "Producto",
         COLUMN 077, "Cantidad",
         COLUMN 092, "Precio",
         COLUMN 108, "Valor " 
   PRINT "___________________________________________________________________________________________________________________________________________________"

  ON EVERY ROW
   -- Imprimiendo detalle 
   FOR i = 1 TO totlin
    IF v_products[i].cditem IS NULL OR 
       v_products[i].canuni IS NULL THEN
       CONTINUE FOR
    END IF 

    PRINT v_products[i].dsitem[1,39]
    PRINT                                          --8 SPACES,
          COLUMN 075,      v_products[i].canuni  USING  "####&.&&"  , --2 SPACES, 
          COLUMN 090,      v_products[i].preuni  USING  "#,##&.&&"  , --3 SPACES, 
          COLUMN 104,      v_products[i].totpro  USING "##,##&.&&" 
   END FOR   

  ON LAST ROW
   -- Totales 
   PRINT "___________________________________________________________________________________________________________________________________________________"
 
   --PRINT "SUB-TOTAL                   ",w_mae_tra.subtot   USING "###,##&.&&"
   --PRINT "IMPUESTO (+)                ",w_mae_tra.totiva   USING "###,##&.&&"
   PRINT COLUMN 075, "TOTAL PAGADO",
         COLUMN 103, w_mae_tra.totdoc   USING "###,##&.&&"

   IF (recibi>0) THEN 
      PRINT COLUMN 075, "TOTAL RECIBIDO", 
            COLUMN 103, recibi             USING "###,##&.&&"
   END IF 

   IF (xvuelto>0) THEN 
      PRINT COLUMN 075, "SU CAMBIO",
            COLUMN 103, xvuelto            USING "###,##&.&&"
   END IF 

   IF (w_mae_tra.totdes>0) THEN 
      PRINT "SU AHORRO                   ",w_mae_tra.totdes   USING "###,##&.&&"
   END IF 

   -- Forma de pago 
   SKIP 1 LINES

   -- Efectivo
   IF (w_mae_tra.efecti>0) THEN 
      PRINT COLUMN 075, "EFECTIVO",
            COLUMN 103, w_mae_tra.efecti USING "###,##&.&&" 
   END IF 

   -- Tarjeta 
   IF (w_mae_tra.tarcre>0) THEN 
      PRINT COLUMN 075, "TARJETA",
            COLUMN 103, w_mae_tra.tarcre USING "###,##&.&&" 
   END IF 

   SKIP 1 LINES

   {LET xnumdoc = "FECHA DE CERTIFICACION: "
   LET col = librut001_centrado(xnumdoc,150) 
   --PRINT COLUMN col,xnumdoc CLIPPED}
   
   {LET xnumdoc2 = reg_e.fecha_em
   LET col = librut001_centrado(xnumdoc2,150)}

   PRINT COLUMN 001, "FRASES:"
   PRINT COLUMN 005, "SUJETO A RETENCION DEFINITIVA ISR"

   SKIP 1 LINE 

   PRINT COLUMN 001, "DATOS DEL CERTIFICADOR:",
         COLUMN 075, "OBSERVACIONES:"
         
   PRINT COLUMN 005, "FECHA DE CERTIFICACION: ", reg_e.fecha_em,
         COLUMN 080, "EN REPUESTOS ELECTRICOS NO HAY CAMBIO NI DEVOLUCION"
         

   PRINT COLUMN 005, "CERTIFICADOR: G4S DOCUMENTA, S.A.   NIT: 60010207", 
         COLUMN 080, "POR CHEQUE RECHAZADO SE COBRARA Q.100.00"
   

   --SKIP 1 LINE 
   
   -- Pie de pagina
   {IF LENGTH(imp1.foot01) >0 THEN --certificador 
      LET col = librut001_centrado(imp1.foot01,100) 
      PRINT COLUMN col,imp1.foot01 CLIPPED 
   END IF
  IF LENGTH(imp1.foot03) >0 THEN --nit certificador
      LET col = librut001_centrado(imp1.foot03,100) 
      PRINT COLUMN col,imp1.foot03 CLIPPED 
   END IF  
   IF LENGTH(imp1.foot02) >0 THEN 
      LET col = librut001_centrado(imp1.foot02,100) 
      PRINT COLUMN col,imp1.foot02 CLIPPED 
   END IF 
   }
   {PRINT COLUMN 01, imp1.foot01, " ", imp1.foot03 --certificador, nit
   IF LENGTH(imp1.foot04) >0 THEN 
      LET col = librut001_centrado(imp1.foot04,150)
      SKIP 1 LINE  
      PRINT COLUMN col,imp1.foot04 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot05) >0 THEN 
      LET col = librut001_centrado(imp1.foot05,150) 
      PRINT COLUMN col,imp1.foot05 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot06) >0 THEN 
      LET col = librut001_centrado(imp1.foot06,150) 
      PRINT COLUMN col,imp1.foot06 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot07) >0 THEN 
      LET col = librut001_centrado(imp1.foot07,150) 
      PRINT COLUMN col,imp1.foot07 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot08) >0 THEN 
      LET col = librut001_centrado(imp1.foot08,150) 
      PRINT COLUMN col,imp1.foot08 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot09) >0 THEN 
      LET col = librut001_centrado(imp1.foot09,150) 
      PRINT COLUMN col,imp1.foot09 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot10) >0 THEN 
      LET col = librut001_centrado(imp1.foot10,150) 
      PRINT COLUMN col,imp1.foot10 CLIPPED 
   END IF} 

   {FOR i = 1 TO 8 
    PRINT " "
   END FOR 
   PRINT cortepapel CLIPPED} 
END REPORT 
