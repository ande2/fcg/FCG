{ 
Objetivo : Programa de variables globales facturacion de productos
Programo : Mynor Ramirez 
Fecha    : Diciembre 2011 
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname  = "facing001" 
CONSTANT imagendes = "mars_alerta" 
CONSTANT imagenreb = "mars_automatico" 
CONSTANT imagenpes = "mars_nodoroot" 
DEFINE w_mae_pro   RECORD LIKE inv_products.*,
       w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_tra   RECORD LIKE fac_mtransac.*,
       w_mae_act   RECORD LIKE inv_mtransac.*,
       w_tip_doc   RECORD LIKE fac_tipodocs.*,
       w_mae_tdc   RECORD LIKE fac_tdocxpos.*,
       w_mae_cli   RECORD LIKE fac_clientes.*,
       w_mae_pos   RECORD LIKE fac_puntovta.*,
       w_uni_med   RECORD LIKE inv_unimedid.*,
       v_products  DYNAMIC ARRAY OF RECORD
        codpro     LIKE inv_products.cditem,
        cditem     LIKE inv_products.codabr, 
        codepq     LIKE fac_dtransac.codepq,
        canepq     LIKE fac_dtransac.canepq, 
        canuni     LIKE fac_dtransac.cantid,
        dsitem     CHAR(50), 
        unimed     SMALLINT,
        nomuni     CHAR(30), 
        preuni     LIKE fac_dtransac.preuni,
        preori     LIKE fac_dtransac.preuni,
        pctdes     LIKE fac_dtransac.pordes,
        descto     LIKE fac_dtransac.descto,
        totpro     LIKE fac_dtransac.totpro,
        cliesp     SMALLINT,
        rebaja     SMALLINT,
        pesado     SMALLINT, 
        imgpes     VARCHAR(30) 
       END RECORD, 
       xnumpos           LIKE fac_mtransac.numpos,
       username          LIKE glb_permxusr.userid,
       totuni            DEC(14,2),
       totval            DEC(14,2),
       reimpresion       SMALLINT,
       totlin            SMALLINT,
       EnterParaImprimir SMALLINT,
       nomest            STRING,   
       b                 ui.ComboBox,
       cba               ui.ComboBox,
       w                 ui.Window,
       f                 ui.Form,
       recibi,xrecibi    DEC(14,2),
       vuelto,xvuelto    DEC(10,2),
       w_lnktra          INT
       
   DEFINE es_dte   CHAR(1)
   DEFINE ande_desa SMALLINT
   DEFINE gfac_id     INTEGER 
   DEFINE gcditem     INTEGER 
END GLOBALS
