{
Objetivo : Facturacion de Productos.
Programo : Mynor Ramirez
Fecha    : Diciembre 2012
}

-- Definicion de variables globales

GLOBALS "facglb001.4gl"
CONSTANT comandowindows   = "Bascula1"
CONSTANT archivofuente    = "C:\\SolTech\\LeerBas\\leerbas.txt"
CONSTANT NumeroLista      = 1 
CONSTANT ClienteDefault   = 2 
CONSTANT VendedorDefault  = 1
CONSTANT RutaDefault      = 1
CONSTANT DocumentoDefault = 2
CONSTANT PesajeBascula    = 0  
CONSTANT CantidadMaxima   = 9999.99 
DEFINE w_mae_ofe         RECORD LIKE fac_mofertas.*,
       wpais             VARCHAR(255), 
       redondeo          SMALLINT, 
       PermiteDuplicados SMALLINT, 
       tasimp            DEC(9,6), 
       pordesant         DEC(5,2), 
       maxdes            DEC(5,2), 
       grabodocto        SMALLINT, 
       regreso           SMALLINT,
       existe            SMALLINT,
       hayofe            SMALLINT,
       archivodestino    STRING, 
       msg               STRING 

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar9")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("facturacion")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "facturacion.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ 

 -- Obteniendo datos del pais 
 CALL librut003_parametros(1,0)
 RETURNING existe,wpais

 -- Logeando cajero 
 CALL facing001_login(1)
 RETURNING regreso,username 

 -- Verificando regreso 
 IF NOT regreso THEN 
    -- Menu de opciones
    CALL facing001_menu()
 END IF 
END MAIN

-- Subutina para el menu de facturacion 

FUNCTION facing001_menu()
 DEFINE regreso    SMALLINT, 
        titulo     STRING 

 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing001a AT 5,2  
  WITH FORM "facing001a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header("facing001",wpais,1)

  -- Desplegando campo de motivo de anulacion
  CALL f.setFieldHidden("motanl",1)
  CALL f.setElementHidden("titanl",1)

  -- Definiendo archivo de datos con peso de bascula 
  LET archivodestino = FGL_GETENV("SPOOLDIR") CLIPPED,"/pesobascula.spl"

  -- Inicializando datos 
  CALL facing001_inival(1)

  LET EnterParaImprimir = FALSE 

  -- Menu de opciones
  MENU "Facturacion" 
   BEFORE MENU
    -- Verificando accesos a opciones
    -- Consultar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Consultar"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Ingresar"
    END IF
    -- Anular     
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Anular"
    END IF  
   COMMAND "Consultar" 
    " Consulta de documentos de facturacion existentes."
    CALL facqbx001_facturacion(1)
   COMMAND KEY("c") 
    --Consultar 
    IF seclib001_accesos(progname,2,username) THEN
       CALL facqbx001_facturacion(1)
    END IF
   COMMAND "Ingresar" 
    " Emision de facturacion."
    CALL facing001_facturacion() 
   COMMAND KEY("i") 
    --Ingresar
    IF seclib001_accesos(progname,1,username) THEN
       CALL facing001_facturacion() 
    END IF
   COMMAND "Anular"
    " Anulacion de documentos de facturacion existentes."
    CALL facqbx001_facturacion(2)
   COMMAND KEY("a") 
    --Anular
    IF seclib001_accesos(progname,2,username) THEN
       CALL facqbx001_facturacion(2)
    END IF
   COMMAND "Salir"
    " Abandona el menu de facturacion." 
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para ingresar el cajero y el punto de venta

FUNCTION facing001_login(opc)
 DEFINE x         RECORD 
         userid   CHAR(15),
         passwd   CHAR(10)
        END RECORD,
        wfecha    VARCHAR(80), 
        xnompos   VARCHAR(40), 
        msg       STRING, 
        regreso   SMALLINT,
        loop      SMALLINT,
        seleccion SMALLINT,
        opc       SMALLINT,
        conteo    INTEGER, 
        w2        ui.Window,
        f2        ui.Form

 -- Abriendo la ventana del login
 OPEN WINDOW wing001b AT 5,2  
  WITH FORM "facing001b" ATTRIBUTE(BORDER)

  -- Desplegando fecha 
  LET wfecha = librut001_formatofecha(TODAY,1,wpais) CLIPPED
  CALL librut001_dpelement("fecdia",wfecha)

  -- Desactivando salida automatica del input 
  OPTIONS INPUT WRAP 

  -- Escondiendo punto de venta
  LET w2 = ui.Window.getCurrent()
  LET f2 = w2.getForm()
  CALL f2.setFieldHidden("formonly.numpos",1)
  CALL f2.setElementHidden("labelx",1)
  CALL f2.setElementHidden("labelb",1)
  CALL f2.setElementHidden("labelc",1)

  -- Verificando si cajero es ingresado o tomado del sistema 
  INITIALIZE x.* TO NULL 
  IF (opc=1) THEN 
     -- Obteniendo cajero logeado
     LET x.userid = FGL_GETENV("LOGNAME")
     DISPLAY BY NAME x.userid 
  END IF 

  -- Ingresando datos
  LET loop = TRUE
  WHILE loop
   INPUT BY NAME x.userid,
                 x.passwd WITHOUT DEFAULTS 
    ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)
   
    ON ACTION cancelar
     -- Salida 
     LET regreso = TRUE
     LET loop    = FALSE 
     EXIT INPUT 

    ON KEY(F4,CONTROL-E) 
     -- Salida 
     LET regreso = TRUE 
     LET loop    = FALSE 
     EXIT INPUT 

    ON ACTION aceptar  
     -- Verificando datos
     IF x.userid IS NULL THEN
        ERROR "Error: cajero invalido, VERIFICA."
        NEXT FIELD userid
     END IF 
     IF x.passwd IS NULL THEN
        ERROR "Error: password invalido, VERIFICA."
        NEXT FIELD passwd
     END IF 

     LET regreso = FALSE
     LET loop    = TRUE 
     EXIT INPUT 

    BEFORE INPUT
     -- Verificando si cajero es ingresado o tomado del sistema 
     IF (opc=1) THEN
        CALL Dialog.SetFieldActive("userid",0) 
     END IF 

    AFTER FIELD userid 
     -- Verificando cajero 
     IF (LENGTH(x.userid)=0) THEN 
        ERROR "Error: cajero invalido, VERIFICA."
        NEXT FIELD userid
     END IF 

    AFTER FIELD passwd
     -- Verificando password
     IF (LENGTH(x.passwd)=0) THEN 
        ERROR "Error: password invalido, VERIFICA."
        NEXT FIELD passwd
     END IF 

     EXIT INPUT  
   END INPUT

   -- Verificando puntos de venta por cajero
   IF loop THEN 
    -- Buscando puntos de venta por cajero 
    SELECT COUNT(*)
     INTO  conteo 
     FROM  fac_usuaxpos a
     WHERE a.userid = x.userid
     IF (conteo=0) THEN
        LET msg = "Cajero (",x.userid CLIPPED,
                  ") no tiene puntos de venta asignados."||
                  "\n VERIFICA."

        CALL fgl_winmessage("Atencion",msg,"stop")
        CONTINUE WHILE 
     ELSE
        LET xnumpos = NULL
        SELECT a.numpos 
         INTO  xnumpos   
         FROM  fac_usuaxpos a,fac_puntovta y
         WHERE a.numpos = y.numpos 
           AND a.userid = x.userid
           AND a.passwd = x.passwd
         IF (status=NOTFOUND) THEN
            ERROR "Atencion: password incorrecto, VERIFICA."
            CONTINUE WHILE  
         END IF 
         LET loop = FALSE 
     END IF 
   END IF 
  END WHILE

  -- Activando salida automatica del input 
  OPTIONS INPUT NO WRAP 
  
 CLOSE WINDOW wing001b

 RETURN regreso,x.userid 
END FUNCTION 

-- Subrutina para el ingreso de los datos del encabezado de la facturacion 

FUNCTION facing001_facturacion()
 DEFINE userauth        LIKE glb_permxusr.userid,
        haycorte        SMALLINT, 
        retroceso       SMALLINT,
        loop,existe     SMALLINT,
        contdc,conven   SMALLINT,
        acceso          SMALLINT,
        xparam,conteo   INTEGER,
        cmdstr          STRING 

 -- Obteniendo parametro de tipo de movimiento de facturacion en inventarios
 CALL librut003_parametros(13,1)
 RETURNING existe,xparam
 IF NOT existe THEN
  CALL fgl_winmessage(
  " Atencion",
  " No existe registrado tipo de movimiento de facturacion en inventarios."||
  "\n Definir parametro antes de facturar.",
  "stop")
  RETURN
 END IF

 -- Obteniendo tasa de impuesto
 CALL librut003_parametros(8,1)
 RETURNING existe,tasimp
 IF NOT existe THEN
  CALL fgl_winmessage(
  " Atencion",
  " No existe registrada la tasa de impuesto.\n Definir parametro antes de facturar.",
  "stop")
  RETURN
 END IF

 -- Obteniendo maximo porcentaje de descuento especial
 CALL librut003_parametros(12,1)
 RETURNING existe,maxdes
 IF NOT existe THEN
  CALL fgl_winmessage(
  " Atencion",
  " No existe registrado maximo porcentaje de descuento autorizado."||
  "\n Definir parametro antes de facturar.",
  "stop")
  RETURN
 END IF

 -- Obteniendo verificacion de redondeo de centavos
 CALL librut003_parametros(50,1)
 RETURNING existe,redondeo
 IF NOT existe THEN LET redondeo = 0 END IF 

 -- Obteniendo verificacion de ingreso de mas de una vez un producto en 
 -- detalle facturacion
 CALL librut003_parametros(50,2)
 RETURNING existe,PermiteDuplicados
 IF NOT existe THEN LET PermiteDuplicados = 0 END IF 

 -- Verificando si existe inicio de dia registrado
 SELECT COUNT(*)
  INTO  conteo
  FROM  fac_vicortes a
  WHERE a.numpos = xnumpos
    AND a.feccor = TODAY  
    AND a.fonini >0
    AND a.cierre IS NOT NULL
 IF (conteo=0) THEN
    CALL fgl_winmessage(
    "Atencion:",
    "No existe registrado el corte de caja del dia actual.\n"||
    "Al registrar el corte de caja ya se podra facturar.",
    "information")
    RETURN
 END IF 

 -- Verificando si ya se realizo el cierre
 SELECT COUNT(*)
  INTO  conteo
  FROM  fac_vicortes a
  WHERE a.numpos  = xnumpos
    AND a.feccor  = CURRENT 
    AND a.fonini  IS NOT NULL
    AND a.cierre  = 1
 IF (conteo>0) THEN
    CALL fgl_winmessage(
    "Atencion:",
    "Corte de caja del dia actual ya fue cerrado.\n"||
    "Ya no puede facturarse en el dia actual.",
    "information")
    RETURN
 END IF

 -- Inicio del loop
 LET retroceso  = FALSE
 LET grabodocto = FALSE
 LET loop       = TRUE
 WHILE loop   

  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos 
     CALL facing001_inival(1) 
  END IF

  -- Ingresando datos
   INPUT BY NAME w_mae_tra.lnktdc,
                w_mae_tra.codven,
                w_mae_tra.codcli,
                w_mae_tra.numnit,
                w_mae_tra.nomcli,
                w_mae_tra.dircli  WITHOUT DEFAULTS
    ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)

   ON ACTION cancel 
    -- Salida 
    IF INFIELD(lnktdc) THEN
       LET loop = FALSE
       EXIT INPUT
    ELSE
       -- Inicializando 
       CALL facing001_inival(1)
       LET grabodocto = FALSE
       NEXT FIELD lnktdc 
    END IF 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON ACTION egresos
    LET cmdstr = "fglrun egresoscaja.42r 1 ",w_mae_tra.numpos," ",w_mae_tra.fecemi
    DISPLAY cmdstr 
    RUN cmdstr WITHOUT WAITING 

   BEFORE INPUT
    -- Verificando si existe un solo tipo de documento 
    SELECT COUNT(*) 
     INTO  contdc  
     FROM  vis_tipdocxpos a 
     WHERE a.numpos = w_mae_tra.numpos
     IF contdc=1 THEN
        -- Buscando tipo de documento 
        SELECT a.lnktdc 
         INTO  w_mae_tra.lnktdc
         FROM  fac_tdocxpos a
         WHERE a.numpos = w_mae_tra.numpos

        -- Asignando datos del tipo de documento 
        CALL facing001_DatosTipoDocumentoPos() 
     ELSE 
        -- Asignando tipo de documento default de la sala de ventas
        LET w_mae_tra.lnktdc = DocumentoDefault 

        -- Asignando datos del tipo de documento 
        CALL facing001_DatosTipoDocumentoPos() 
     END IF 

    -- Verificando regreso despues de grabar
    IF grabodocto THEN 
       EXIT INPUT 
    END IF 

   ON CHANGE lnktdc 
    -- Asignando datos del tipo de documento
    LET es_dte=NULL 
    CALL facing001_DatosTipoDocumentoPos()
    
    --FEL Documento es DTE -> Se manda a certificar 
    IF w_mae_tra.tipdoc = 1 THEN
      LET es_dte="S"
    ELSE 
      LET es_dte="N"
    END IF 

   AFTER FIELD lnktdc
    -- Verificando tipo de documento
    IF w_mae_tra.lnktdc IS NULL THEN
       CALL fgl_winmessage(
       "Atencion","Tipo de documento invalido, VERIFICA.","stop")
       NEXT FIELD lnktdc 
    ELSE 
       -- Asignando datos del tipo de documento
       LET es_dte=NULL 
       CALL facing001_DatosTipoDocumentoPos()
       
       --FEL Documento es DTE -> Se manda a certificar 
       IF w_mae_tra.tipdoc = 1 THEN
         LET es_dte="S"
       ELSE 
         LET es_dte="N"
       END IF   
    END IF 

   AFTER FIELD codven
    -- Verificando vendedor 
    IF w_mae_tra.codven IS NULL THEN
       CALL fgl_winmessage(
       "Atencion","Vendedor invalido, VERIFICA.","stop")
       NEXT FIELD codven
    END IF 

   BEFORE FIELD codcli
    -- Verificando nombre de cliente 
   IF LENGTH(w_mae_tra.nomcli)>0 THEN
      -- Obteniendo datos del cliente
      INITIALIZE w_mae_cli.* TO NULL
      CALL librut003_bcliente(w_mae_tra.codcli) 
      RETURNING w_mae_cli.*,existe
      LET w_mae_tra.nomcli = w_mae_cli.nomcli 
      LET w_mae_tra.numnit = w_mae_cli.numnit 
      LET w_mae_tra.dircli = w_mae_cli.dircli 
      DISPLAY BY NAME w_mae_tra.numnit 
   END IF

   ON CHANGE codcli
    -- Obteniendo datos del cliente
    INITIALIZE w_mae_cli.* TO NULL
    CALL librut003_bcliente(w_mae_tra.codcli) 
    RETURNING w_mae_cli.*,existe
    LET w_mae_tra.nomcli = w_mae_cli.nomcli 
    LET w_mae_tra.numnit = w_mae_cli.numnit 
    LET w_mae_tra.dircli = w_mae_cli.dircli 
    DISPLAY BY NAME w_mae_tra.numnit 

   AFTER FIELD codcli 
    -- Verificando cliente
    IF w_mae_tra.codcli IS NULL THEN
       CALL fgl_winmessage(
       "Atencion","Cliente invalido, VERIFICA.","stop")
       NEXT FIELD codcli
    END IF 

    -- Obteniendo datos del cliente
    INITIALIZE w_mae_cli.* TO NULL
    CALL librut003_bcliente(w_mae_tra.codcli) 
    RETURNING w_mae_cli.*,existe
    LET w_mae_tra.numnit = w_mae_cli.numnit 
    LET w_mae_tra.dircli = w_mae_cli.dircli 
    DISPLAY BY NAME w_mae_tra.numnit,w_mae_cli.dircli  
  END INPUT 
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Ingresando detalle del movimiento 
  LET retroceso = facing001_detingreso()
 END WHILE

 -- Inicializando datos 
 CALL facing001_inival(1) 
END FUNCTION

-- Subrutina para asignar datos del tipo de documento x punto de venta 

FUNCTION facing001_DatosTipoDocumentoPos() 
 DEFINE existe SMALLINT 

 -- Obteniendo datos del tipo de documento x punto de venta 
 INITIALIZE w_mae_tdc.* TO NULL
 CALL librut003_btdocxpos(w_mae_tra.lnktdc) 
 RETURNING w_mae_tdc.*,existe 
 DISPLAY BY NAME w_mae_tra.lnktdc 

 -- Asignando datos de la empresa, tipo de documento, y serie 
 LET w_mae_tra.codemp = w_mae_tdc.codemp 
 LET w_mae_tra.tipdoc = w_mae_tdc.tipdoc 
 LET w_mae_tra.nserie = w_mae_tdc.nserie 
 LET w_mae_tra.haycor = w_mae_tdc.haycor 
 DISPLAY BY NAME w_mae_tra.nserie 

 -- Obteniendo datos de la empresa 
 INITIALIZE w_mae_emp.* TO NULL 
 CALL librut003_bempresa(w_mae_tra.codemp)
 RETURNING w_mae_emp.*,existe 

 -- Obteniendo datos del tipo de documento
 INITIALIZE w_tip_doc.* TO NULL
 CALL librut003_btipodoc(w_mae_tra.tipdoc)
 RETURNING w_tip_doc.*,regreso

 -- Asignando forma de venta
 LET w_mae_tra.credit = 0 --w_mae_tdc.credit 

 -- Si forma de venta es credito, no hay descuentos ni ofertas
 IF w_mae_tra.credit THEN 
    LET w_mae_ofe.pordes = 0 
    CALL librut001_dpelement("titofe","") 
 END IF 

 -- Verificando si el documento existe 
 SELECT UNIQUE a.codemp
  FROM  fac_mtransac a
  WHERE (a.codemp = w_mae_tra.codemp)
    AND (a.tipdoc = w_mae_tra.tipdoc)
    AND (a.nserie = w_mae_tra.nserie)
    AND (a.numdoc IS NOT NULL) 
  IF (status=NOTFOUND) THEN 
     -- Obteniendo correaltivo inicial del tipo de documento x punto de venta
     LET w_mae_tra.numdoc = w_mae_tdc.numcor
  ELSE
     -- Obteniendo correlativo del tipo de documento 
     LET w_mae_tra.numdoc = librut003_correltipdoc(w_mae_tra.codemp,
                                                   w_mae_tra.tipdoc,
                                                   w_mae_tra.nserie,
                                                   0,
                                                   0)
  END IF 

 -- Desplegando numero de documento
 DISPLAY BY NAME w_mae_tra.numdoc 
END FUNCTION 

-- Subutina para el ingreso del detalle de productos de la facturacion 

FUNCTION facing001_detingreso()
 DEFINE w_mae_art   RECORD LIKE inv_products.*,
        w_uni_vta   RECORD LIKE inv_unimedid.*,
        userauth    LIKE glb_permxusr.userid,
        canexi      LIKE fac_dtransac.cantid,
        loop,scr    SMALLINT,
        confirm     SMALLINT,
        opc,arr,i   SMALLINT, 
        linea       SMALLINT, 
        retroceso   SMALLINT, 
        acceso      SMALLINT, 
        hayexi      SMALLINT, 
        numepq      SMALLINT, 
        lastkey     INTEGER,  
        result      INTEGER,  
        repetido    SMALLINT,
        nuevodoc    SMALLINT,
        pesobascula STRING, 
        strcon      STRING,
        cmdstr      STRING 

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop

  -- Ingresando productos
  INPUT ARRAY v_products WITHOUT DEFAULTS FROM s_products.*
   ATTRIBUTE(INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,
             UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION accept 
    -- Aceptar
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET retroceso  = TRUE
    LET grabodocto = FALSE
    LET loop       = FALSE
    EXIT INPUT

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON ACTION egresos
    LET cmdstr = "fglrun egresoscaja.42r 1 ",w_mae_tra.numpos," ",w_mae_tra.fecemi
    DISPLAY cmdstr 
    RUN cmdstr WITHOUT WAITING 

   ON ACTION productos 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Consulta de productos
    CALL librut002_formlistpro("Consulta de Productos",
                               "Codigo",
                               "Descripcion del Producto",
                               " Precio Unitario",
                               "      Existencia",
                               "codabr",
                               "dsitem",
                               "presug",
                               "inv_products",
                               "estado=1 and status=1",
                               2,
                               1,
                               1)
    RETURNING v_products[arr].cditem,v_products[arr].dsitem,regreso
    IF regreso THEN
       NEXT FIELD cditem
    ELSE 
       -- Asignando descripcion
       DISPLAY v_products[arr].cditem TO s_products[scr].cditem 
       DISPLAY v_products[arr].dsitem TO s_products[scr].dsitem 
    END IF 

   ON KEY(F6,CONTROL-T) 
    -- Verificando acceso a descuento especial 
    -- Ingresando seguridad
    CALL facing001_seguridad() 
    RETURNING acceso,userauth
    IF acceso THEN
       CALL facing001_DescuentoEspecial() 
    END IF

   ON ACTION otrodocto
    -- Cancelando e ingresando un nuevo documento
    LET loop      = FALSE
    LET retroceso = FALSE
    EXIT INPUT 

   ON KEY("+") 
    -- Cancelando e ingresando un nuevo documento
    LET loop      = FALSE
    LET retroceso = FALSE
    EXIT INPUT 

   BEFORE INPUT 
    -- Desactivando append 
    CALL DIALOG.setActionHidden("append",TRUE)
    
    -- Totalizando unidades
    CALL facing001_totdet(1)

   BEFORE ROW
    LET totlin = ARR_COUNT()

   BEFORE FIELD cditem 
    -- Habilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",TRUE)
    CALL DIALOG.SetFieldActive("codepq",1)
    CALL DIALOG.SetFieldActive("preuni",1)
    CALL DIALOG.SetFieldActive("canepq",1)
    CALL DIALOG.SetFieldActive("canuni",1)

   ON CHANGE cditem
    -- Inicializando producto 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()
    INITIALIZE v_products[arr].codpro,v_products[arr].canuni 
    THRU       v_products[arr].imgpes  TO NULL
    CLEAR      s_products[scr].codpro,s_products[arr].canuni,
               s_products[scr].dsitem,s_products[arr].unimed,
               s_products[scr].nomuni,s_products[arr].preuni, 
               s_products[scr].preori,s_products[scr].pctdes,
               s_products[arr].descto,s_products[scr].totpro,
               s_products[arr].cliesp,s_products[scr].rebaja,
               s_products[arr].pesado,s_products[scr].imgpes 

   AFTER FIELD cditem 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()

    -- Tecla abajo 
    IF (lastkey = FGL_KEYVAL("down")) THEN 
       IF v_products[arr].cditem IS NULL THEN 
          NEXT FIELD cditem
       END IF 
       IF v_products[arr].canuni IS NULL THEN 
          NEXT FIELD cditem
       END IF 
    END IF 

    -- Tecla arriba 
    IF (lastkey = FGL_KEYVAL("up")) THEN 
       IF v_products[arr].cditem IS NOT NULL THEN 
          IF v_products[arr].canuni IS NULL THEN 
             NEXT FIELD cditem
          END IF 
       END IF 
    END IF 

   BEFORE FIELD codepq 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando producto nulo
    IF (LENGTH(v_products[arr].cditem) <=0) THEN 
        -- Consulta de productos
        CALL librut002_formlistpro("Consulta de Productos",
                                   "Codigo",
                                   "Descripcion del Producto",
                                   " Precio Unitario",
                                   "      Existencia",
                                   "codabr",
                                   "dsitem",
                                   "presug",
                                   "inv_products",
                                   "estado=1 and status=1",
                                   2,
                                   1,
                                   1)
        RETURNING v_products[arr].cditem,v_products[arr].dsitem,regreso
        IF regreso THEN
           NEXT FIELD cditem
        ELSE 
           -- Asignando descripcion
           DISPLAY v_products[arr].cditem TO s_products[scr].cditem 
           DISPLAY v_products[arr].dsitem TO s_products[scr].dsitem 
        END IF 
    END IF 
   
    -- Verificando producto negativo
    IF (v_products[arr].cditem <=0) THEN 
       CALL fgl_winmessage(
       " Atencion:",
       " Producto invalido VERIFICA.",
       "stop")
       INITIALIZE v_products[arr].* TO NULL
       CLEAR      s_products[scr].*
       NEXT FIELD cditem 
    END IF 

    -- Verificando si el producto existe
    INITIALIZE w_mae_art.* TO NULL
    CALL librut003_bproductoabr(v_products[arr].cditem)
    RETURNING w_mae_art.*,existe
    IF NOT existe THEN
       CALL fgl_winmessage(
       " Atencion:",
       " Producto no registrado VERIFICA.",
       "stop")
       INITIALIZE v_products[arr].* TO NULL
       CLEAR      s_products[scr].*
       NEXT FIELD cditem 
    END IF 

    -- Asignando descripcion
    LET v_products[arr].codpro = w_mae_art.cditem
    LET v_products[arr].dsitem = w_mae_art.dsitem 
    LET v_products[arr].unimed = w_mae_art.unimed 
    DISPLAY v_products[arr].dsitem TO s_products[scr].dsitem 

    -- Obteniendo datos de la unidad de medida
    INITIALIZE w_uni_med.* TO NULL
    CALL librut003_bumedida(w_mae_art.unimed) 
    RETURNING w_uni_med.*,existe

    -- ASignando unidad de medida
    LET v_products[arr].nomuni = w_uni_med.nomabr 
    DISPLAY v_products[arr].nomuni TO s_products[scr].nomuni 

    -- Verificando parametro de producto mas de una vez en detalle
    IF NOT PermiteDuplicados THEN 
     -- Verificando duplicados 
     LET repetido = FALSE
     FOR i = 1 TO totlin
      IF v_products[i].cditem IS NULL THEN
         CONTINUE FOR
      END IF

      -- Verificando producto
      IF (i!=ARR_CURR()) THEN 
       IF (v_products[i].cditem = v_products[arr].cditem) THEN
          LET repetido = TRUE
          EXIT FOR
       END IF 
      END IF
     END FOR

     -- Si hay repetido
     IF repetido THEN
      LET msg = "Producto ya registrado en el detalle. Linea (",i USING "<<<",")"||
                "\n VERIFICA." 
      CALL fgl_winmessage(" Atencion",msg,"stop")
      INITIALIZE v_products[arr].* TO NULL
      CLEAR      s_products[scr].*
      NEXT FIELD cditem 
     END IF
    END IF 

    -- Verificando estado del producto
    IF NOT w_mae_art.estado  THEN
       CALL fgl_winmessage(
       " Atencion",
       " Producto de BAJA no puede facturarse, VERIFICA.",
       "stop")
       INITIALIZE v_products[arr].* TO NULL
       CLEAR      s_products[scr].*
       NEXT FIELD cditem 
    END IF 

    -- Verificando estatus del producto
    IF NOT w_mae_art.status  THEN
       CALL fgl_winmessage(
       " Atencion",
       " Producto BLOQUEADO no puede facturarse, VERIFICA.",
       "stop")
       INITIALIZE v_products[arr].* TO NULL
       CLEAR      s_products[scr].*
       NEXT FIELD cditem 
    END IF 

    -- Asignando datos del producto 
    LET v_products[arr].pctdes = w_mae_ofe.pordes 
    LET v_products[arr].pesado = w_mae_art.pesado 
    LET v_products[arr].preori = w_mae_art.presug 
    LET v_products[arr].cliesp = 0 -- w_mae_cli.cliesp 
    LET v_products[arr].rebaja = 0
    LET v_products[arr].descto = 0 

    -- Verificando si producto es pesado 
    IF v_products[arr].pesado THEN 
       LET v_products[arr].imgpes = imagenpes 
    END IF 

    -- Desplegando datos 
    DISPLAY v_products[arr].pctdes TO s_products[scr].pctdes 
    DISPLAY v_products[arr].descto TO s_products[scr].descto 
    DISPLAY v_products[arr].imgpes TO s_products[scr].imgpes 

    -- Deshabilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",FALSE) 

    -- Cargando combobox de empaques x producto
    CALL librut003_cbxempaques(v_products[arr].codpro,1)

    -- Verificando si empaque es nulo
    IF v_products[arr].codepq IS NULL THEN
       LET v_products[arr].codepq = 0
       LET v_products[arr].canepq = 1 
       LET v_products[arr].canuni = 1 
    END IF 

    -- Verificando si producto solo tiene un empaque
    SELECT COUNT(*) 
     INTO  numepq
     FROM  inv_epqsxpro a
     WHERE a.cditem = v_products[arr].codpro 
     DISPLAY numepq 
     IF (numepq>1) THEN
        -- Activando cantidad de empaque
        CALL DIALOG.SetFieldActive("codepq",1)
     ELSE 
        -- Desactivando cantidad de empaque
        CALL DIALOG.SetFieldActive("codepq",0)
     END IF 

    -- Verificando si el producto es pesado por bascula
    IF w_mae_art.pesado THEN 
     -- Verificando si se usara peso obtenido de bascula
     IF PesajeBascula THEN 
       -- Obteniendo peso de bascula
       CALL librut001_leerbascula(comandowindows,archivofuente,archivodestino,"G")
       RETURNING result,pesobascula
       IF NOT result THEN 
          LET v_products[arr].canepq = NULL 
          CLEAR s_products[scr].canepq 
          NEXT FIELD cditem 
       END IF 

       -- Verificando peso 
       LET v_products[arr].canepq = pesobascula 
       DISPLAY "Peso bascula --- ",v_products[arr].canepq 
       DISPLAY v_products[arr].canepq TO s_products[scr].canepq 

       IF v_products[arr].canepq IS NULL OR 
          v_products[arr].canepq <=0 THEN 
          LET msg = "Peso de Bascula invalido [ ",
          v_products[arr].canuni USING "-<,<<&.&&"," ] \n" ||
          " Peso de bascula debe ser mayor a cero."

          CALL fgl_winmessage(" Atencion: ",msg,"stop")
          LET v_products[arr].canepq = NULL 
          CLEAR s_products[scr].canepq 
          NEXT FIELD cditem 
       END IF 

       -- Desactivando cantidad 
       CALL DIALOG.SetFieldActive("canepq",FALSE)
       CALL DIALOG.SetFieldActive("canuni",FALSE)
     END IF 

     -- Activando precio 
     CALL DIALOG.SetFieldActive("preuni",1)
    ELSE
     -- Desactivando precio 
     CALL DIALOG.SetFieldActive("preuni",0)
    END IF 

    -- Calculando cantidad total en unidades totales
    CALL facing001_cantidadtotal(arr,scr,1)

    -- Totalizando unidades
    CALL facing001_totdet(1)

    -- Verificando si producto no tiene precio 
    IF v_products[arr].preori IS NULL OR 
       v_products[arr].preori <=0 THEN 
       CALL fgl_winmessage(
       " Atencion",
       " Producto ["||v_products[arr].dsitem CLIPPED||"] sin precio asignado.\n VERIFICA.",
       "stop")
       INITIALIZE v_products[arr].* TO NULL
       CLEAR      s_products[scr].*
       NEXT FIELD cditem
    END IF

   ON CHANGE codepq  
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Calculando cantidad total en unidades totales
    CALL facing001_cantidadtotal(arr,scr,1)

    -- Totalizando unidades
    CALL facing001_totdet(1)

   AFTER FIELD codepq 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando empaque
    IF v_products[arr].codepq IS NULL THEN 
       NEXT FIELD codepq
    END IF

    -- Calculando cantidad total en unidades totales
    CALL facing001_cantidadtotal(arr,scr,1)

    -- Totalizando unidades
    CALL facing001_totdet(1)

   ON CHANGE canepq 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Calculando cantidad total en unidades totales
    CALL facing001_cantidadtotal(arr,scr,1)

    -- Totalizando unidades
    CALL facing001_totdet(1)

   AFTER FIELD canepq
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD canepq 
    END IF

    -- Verificando cantidad
    IF v_products[arr].canepq IS NULL OR
       (v_products[arr].canepq <=0) OR 
       (v_products[arr].canepq >CantidadMaxima) THEN 
       LET v_products[arr].canepq = 1
       DISPLAY v_products[arr].canepq TO s_products[scr].canepq 
    END IF 

    -- Calculando cantidad total en unidades totales
    CALL facing001_cantidadtotal(arr,scr,1)

    -- Totalizando unidades
    CALL facing001_totdet(1)
    
   ON CHANGE preuni 
    -- Calculando cantidad total en unidades totales
    CALL facing001_cantidadtotal(arr,scr,1)

    -- Totalizando unidades
    CALL facing001_totdet(1)

   AFTER FIELD preuni 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR
       (lastkey = FGL_KEYVAL("up")) THEN
       NEXT FIELD preuni 
    END IF

    -- Verificando precio 
    DISPLAY "PRECIO REAL ",v_products[arr].preori 
    IF v_products[arr].preuni IS NULL OR
       (v_products[arr].preuni <=0) OR 
       (v_products[arr].preuni >v_products[arr].preori) THEN 
       CALL fgl_winmessage(
       " Atencion",
       " Precio invalido, VERIFICA.",
       "stop")
       LET v_products[arr].preuni = NULL
       CLEAR s_products[scr].preuni
       NEXT FIELD preuni 
    END IF

    -- Calculando cantidad total en unidades totales
    CALL facing001_cantidadtotal(arr,scr,1)

    -- Totalizando unidades
    CALL facing001_totdet(1)

   AFTER ROW,INSERT,DELETE 
    LET totlin = ARR_COUNT()

    -- Totalizando unidades
    CALL facing001_totdet(1)

   ON ACTION DELETE 
    -- Eliminando linea seleccionada
    LET arr = ARR_CURR() 
    CALL v_products.deleteElement(arr)

    -- Totalizando unidades
    CALL facing001_totdet(1)

    NEXT FIELD cditem 

   ON ROW CHANGE 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Calculando cantidad total en unidades totales
    CALL facing001_cantidadtotal(arr,scr,1)

    -- Totalizando unidades
    CALL facing001_totdet(1)

   AFTER INPUT 
    LET totlin = ARR_COUNT()

    -- Totalizando unidades
    CALL facing001_totdet(1)
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Recalculando totales
  FOR i = 1 TO totlin
   IF v_products[i].canuni IS NULL THEN
      CONTINUE FOR
   END IF 

   -- Calculando cantidad total en unidades totales
   CALL facing001_cantidadtotal(i,i,0)
  END FOR

  -- Desplegando datos 
  DISPLAY ARRAY v_products TO s_products.*
   BEFORE DISPLAY 
    EXIT DISPLAY 
  END DISPLAY

  -- Verificando si punto de venta cheque existencia
  IF w_mae_pos.chkexi THEN
   LET hayexi = TRUE 
   FOR i = 1 TO totlin
    IF v_products[i].canuni IS NULL THEN
       CONTINUE FOR
    END IF 

    -- Obteniendo existencia
    LET canexi = librut003_chkexistencia(w_mae_pos.codemp,
                                         w_mae_pos.codsuc,
                                         w_mae_pos.codbod,
                                         v_products[i].codpro)

    -- Verificando chequeo de existencia por producto
    SELECT UNIQUE a.chkexi 
     FROM  inv_products a
     WHERE a.cditem = v_products[i].codpro 
       AND a.chkexi = 1 
     IF (STATUS!=NOTFOUND) THEN 
      IF (v_products[i].canuni>canexi) THEN
       LET msg = " Producto ( ",v_products[i].dsitem CLIPPED," ) \n",
                 " Con existencia insuficiente para la venta. \n",
                 " Existencia Actual ( ",canexi USING "-<<<,<<&"," )"

       CALL fgl_winmessage(" Atencion",msg,"stop")
       LET hayexi = FALSE
       EXIT FOR
     END IF 
    END IF
   END FOR 
   IF NOT hayexi THEN 
      CONTINUE WHILE 
   END IF 
  END IF  

  -- Totalizando unidades
  CALL facing001_totdet(1)

  -- Verificando lineas incompletas 
  LET linea = facing001_incompletos()
  IF (linea>0) THEN
     LET msg = " Linea ("||linea||") de producto incompleta. \n VERIFICA."
     CALL fgl_winmessage(
     " Atencion",
     msg, 
     "stop")
     CONTINUE WHILE
  END IF 

  -- Verificando que se ingrese al menos un producto
  IF (totval<=0) THEN
     CALL fgl_winmessage(
     " Atencion",
     " Debe facturarse al menos un producto. \n VERIFICA.",
     "stop")
     CONTINUE WHILE
  END IF

  -- Ingresando desgloce de pago
  -- Verificando si documento es de contado
  CALL facing001_desglocepago() 
  RETURNING regreso,nuevodoc
  IF regreso THEN 
     CONTINUE WHILE
  END IF 
  
  -- Verificando si es  
  IF nuevodoc THEN 
    LET retroceso = FALSE
    EXIT WHILE
  END IF 

  -- Menu de opciones
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop      = FALSE
    LET retroceso = FALSE
   WHEN 1 -- Grabando
    LET loop      = FALSE
    LET retroceso = FALSE

    -- Grabando 
    CALL facing001_grabar()
   WHEN 2 -- Modificando
    LET loop = TRUE 
  END CASE
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subrutina para calcular las unidades totales 
-- (unidades ingresdas * cantidad empaque)

FUNCTION facing001_cantidadtotal(arr,scr,dsp) 
 DEFINE w_mae_art     RECORD LIKE inv_products.*,
        wcanepq       LIKE fac_dtransac.canepq,
        arr,scr,dsp   SMALLINT

 -- Obteniendo datos del producto
 INITIALIZE w_mae_art.* TO NULL
 CALL librut003_bproductoabr(v_products[arr].cditem)
 RETURNING w_mae_art.*,existe

 -- Calculando cantidad totales
 LET wcanepq = librut003_canempaque(v_products[arr].codpro,v_products[arr].codepq)
 LET v_products[arr].canuni = (v_products[arr].canepq*wcanepq)

 DISPLAY "Precio ",v_products[arr].preuni 

 -- Asignando precio x cantidad
 IF NOT w_mae_art.pesado THEN
    LET v_products[arr].preuni = w_mae_art.presug 

    -- Verificando precio 3 
    IF v_products[arr].canuni>=w_mae_art.canva3 AND 
       w_mae_art.canva3>0 THEN 
       LET v_products[arr].preuni = w_mae_art.preva3
    ELSE 
       -- Verificando precio 2
       IF v_products[arr].canuni>=w_mae_art.canva2 AND 
          w_mae_art.canva2>0 THEN 
          LET v_products[arr].preuni = w_mae_art.preva2
       ELSE 
          -- Verificando precio 1
          IF v_products[arr].canuni>=w_mae_art.canva1 AND 
             w_mae_art.canva1>0 THEN 
             LET v_products[arr].preuni = w_mae_art.preva1
          END IF 
       END IF 
    END IF 
 ELSE 
    -- Asignando precio base 
    IF v_products[arr].preuni IS NULL OR
       v_products[arr].preuni =0 THEN 
       LET v_products[arr].preuni = w_mae_art.presug 
    END IF 
 END IF 

 -- Calculando precio total
 DISPLAY "Totalizando ",v_products[arr].canuni 
 LET v_products[arr].totpro = (v_products[arr].canuni*v_products[arr].preuni)

 -- Desplegando datos
 IF dsp THEN
  DISPLAY v_products[arr].codepq TO s_products[scr].codepq 
  DISPLAY v_products[arr].canepq TO s_products[scr].canepq 
  DISPLAY v_products[arr].canuni TO s_products[scr].canuni 
  DISPLAY v_products[arr].pctdes TO s_products[scr].pctdes 
  DISPLAY v_products[arr].descto TO s_products[scr].descto
  DISPLAY v_products[arr].totpro TO s_products[scr].totpro 
 END IF 
END FUNCTION 

-- Subrutina para totalizar unidades contadas

FUNCTION facing001_totdet(operacion)
 DEFINE i,operacion,totlns SMALLINT

 -- Totalizando
 LET totval           = 0
 LET w_mae_tra.totdes = 0 
 LET totlns           = 0
 FOR i = 1 TO totlin
  IF v_products[i].codpro IS NULL OR
     v_products[i].canuni IS NULL THEN 
     CONTINUE FOR
  END IF 

  -- Totalizando
  LET totval           = (totval+v_products[i].totpro)
  LET w_mae_tra.totdes = (w_mae_tra.totdes+v_products[i].descto) 
  LET totlns           = (totlns+1)
 END FOR
 
 -- Verificando parametro de redondeo
 IF redondeo THEN
    -- Redondeando total
    LET totval = librut001_redondeo(totval)
 END IF 

 -- Asignando totales 
 LET w_mae_tra.totpag  = totval

 -- Desplegando totales
 DISPLAY BY NAME totval,w_mae_tra.totpag,w_mae_tra.totdes
 DISPLAY totlns TO totlin 

 -- Totalizando desgloce 
 IF (operacion=1) THEN 
    LET w_mae_tra.efecti  = 0 
    LET w_mae_tra.tarcre  = 0 
    LET vuelto            = 0
 END IF 

 CALL facing001_totpago()
END FUNCTION 

-- Subrutina para verificar si hay productos incompletos

FUNCTION facing001_incompletos()
 DEFINE i,linea SMALLINT 

 LET linea = 0 
 FOR i = 1 TO 100 
  IF v_products[i].cditem IS NULL THEN
     CONTINUE FOR
  END IF

  -- Verificando lineas
  IF v_products[i].canuni IS NULL OR
     v_products[i].preuni IS NULL OR
     v_products[i].totpro IS NULL THEN
     LET linea = i 
     EXIT FOR 
  END IF 
 END FOR

 RETURN linea 
END FUNCTION

-- Subrutina para ingresar el descuento especial

FUNCTION facing001_DescuentoEspecial() 
 DEFINE descuento  DEC(5,2),
        i,regreso  SMALLINT,
        msg        STRING 

 -- Ingresando descuento especial
 OPEN WINDOW wing001d AT 5,2 WITH FORM "facing001e"

  LET regreso = FALSE 
  INPUT BY NAME descuento WITHOUT DEFAULTS
   ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)

   ON ACTION cancel 
    LET regreso = TRUE 
    EXIT INPUT 

   AFTER FIELD descuento
    -- Verificando descuento
    IF descuento IS NULL OR
       descuento <0 THEN
       ERROR "Error: porcentaje invalido, VERIFICA." 
       NEXT FIELD descuento 
    END IF

    -- Verificando maximo descuento permitido
    IF descuento >maxdes THEN
       ERROR "Error: maximo permitido ",maxdes USING "##&.&&"
       NEXT FIELD descuento 
    END IF
  END INPUT 

 CLOSE WINDOW wing001d 

 -- Si no hay regreso 
 IF NOT regreso THEN 
    LET w_mae_ofe.pordes = descuento 
    LET msg = "  DESCUENTO ** ESPECIAL **" 
    CALL librut001_dpelement("titofe",msg) 

    -- Recalculando totales
    FOR i = 1 TO totlin
     IF v_products[i].canuni IS NULL THEN
        CONTINUE FOR
     END IF

     -- Asignando descuento
     LET v_products[i].pctdes = w_mae_ofe.pordes



     -- Calculando cantidad total en unidades totales
     CALL facing001_cantidadtotal(i,i,0)
    END FOR

    -- Desplegando datos
    DISPLAY ARRAY v_products TO s_products.*
     BEFORE DISPLAY
      EXIT DISPLAY
    END DISPLAY

    -- Totalizando unidades
    CALL facing001_totdet(1)
 END IF 
END FUNCTION 

-- Subrutina para ingresar el desgloce de pago

FUNCTION facing001_desglocepago()
 DEFINE userauth     LIKE glb_permxusr.userid,
        nuevodoc     SMALLINT, 
        loop,acceso  SMALLINT, 
        regreso      SMALLINT, 
        lastkey      SMALLINT,
        msg          STRING 

 -- Desactivando salida automatica del input 
 -- OPTIONS INPUT WRAP 

 -- Ingresando desgloce 
 LET loop   = TRUE
 WHILE loop 
  LET nuevodoc = FALSE 
  INPUT BY NAME w_mae_tra.efecti,
                w_mae_tra.tarcre
                WITHOUT DEFAULTS
   ATTRIBUTE(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

  ON ACTION accept
   -- Confirmando desgloce 
   -- Totalizando desgloce 
   CALL facing001_totpago()

   -- Avanza a grabar documento 
   LET regreso  = FALSE 
   LET loop     = FALSE
   EXIT INPUT 

  ON ACTION cancel 
   -- Regreso al detalle de productos 
   LET regreso  = TRUE  
   LET loop     = FALSE
   EXIT INPUT

  ON ACTION otrodocto
   -- Cancelando e ingresando un nuevo documento 
   LET regreso  = FALSE 
   LET nuevodoc = TRUE 
   LET loop     = FALSE
   EXIT INPUT

  ON KEY("+") 
   -- Cancelando e ingresando un nuevo documento 
   LET regreso  = FALSE 
   LET nuevodoc = TRUE 
   LET loop     = FALSE
   EXIT INPUT

  ON ACTION calculator
   -- Cargando calculadora
   IF NOT winshellexec("calc") THEN
      ERROR "Atencion: calculadora no disponible."
   END IF

  ON KEY(F6,CONTROL-T) 
   -- Ingresando descuento especial 
   -- Activando salida automatica del input 
   -- OPTIONS INPUT NO WRAP 

   -- Verificando acceso a descuento especial 
   CALL facing001_seguridad()
   RETURNING acceso,userauth
   IF acceso THEN
      CALL facing001_DescuentoEspecial() 
   END IF

   -- Desactivando salida automatica del input 
   -- OPTIONS INPUT WRAP 

  ON CHANGE efecti 
   -- Calculando vuelto
   CALL facing001_CalculaVuelto()

  AFTER FIELD efecti 
   -- Verificando  efectivo 
   IF w_mae_tra.efecti IS NULL OR
      (w_mae_tra.efecti <=0) THEN
      LET w_mae_tra.efecti = 0
      DISPLAY BY NAME w_mae_tra.efecti
   END IF 

   -- Totalizando desgloce 
   CALL facing001_totpago()

  ON CHANGE tarcre
   -- Totalizando desgloce 
   CALL facing001_totpago()

  AFTER FIELD tarcre
   -- Verificando tarjeta 
   IF w_mae_tra.tarcre IS NULL OR
      (w_mae_tra.tarcre <=0) THEN
      LET w_mae_tra.tarcre = 0
      DISPLAY BY NAME w_mae_tra.tarcre
   END IF

   -- Inicializando vuelto
   LET vuelto = 0

   -- Totalizando desgloce 
   CALL facing001_totpago()
  END INPUT
  IF regreso OR 
     nuevodoc THEN
     EXIT WHILE
  END IF

  -- Calculando vuelto
  IF (vuelto=0) THEN 
     CALL facing001_CalculaVuelto()
  END IF 

  -- Totalizando desgloce 
  CALL facing001_totpago()

  -- Verificando totales del detalle del producto versus total desgloce 
  IF (w_mae_tra.totpag!=w_mae_tra.totdoc) THEN
     LET msg = " Total Recibido diferente al Total a Pagar. "||
               "\n VERIFICA que los totales sean iguales."

     CALL fgl_winmessage(
     " Atencion",msg,"stop")
     LET loop = TRUE 
     CONTINUE WHILE 
  END IF 

  -- Avanza a grabar documento 
  LET regreso  = FALSE 
  LET loop     = FALSE
 END WHILE

 -- Activando salida automatica del input 
 -- OPTIONS INPUT NO WRAP 

 RETURN regreso,nuevodoc 
END FUNCTION 

-- Subrutina para calcular el vuelto

FUNCTION facing001_CalculaVuelto()
 -- Calculando vuelto
 LET vuelto = 0
 LET recibi = 0

 IF (w_mae_tra.efecti>w_mae_tra.totpag) THEN
    LET w_mae_tra.tarcre = 0
    LET recibi           = w_mae_tra.efecti
    LET xrecibi          = recibi
    LET vuelto           = (w_mae_tra.efecti-w_mae_tra.totpag)
    LET xvuelto          = vuelto 
    LET w_mae_tra.efecti = w_mae_tra.totpag 
 END IF

 -- Totalizando desgloce 
 CALL facing001_totpago()
END FUNCTION 

-- Subrutina para totalizar el desgloce de pago 

FUNCTION facing001_totpago()
 -- Totalizando desgloce 
 LET w_mae_tra.subtot = (w_mae_tra.efecti+w_mae_tra.tarcre)

 -- Verificando si documento esta exento de impuesto
 IF NOT w_tip_doc.exeimp THEN 
  LET w_mae_tra.totiva = (w_mae_tra.subtot-(w_mae_tra.subtot/(1+(w_mae_tra.poriva/100)))) 
 ELSE
  LET w_mae_tra.totiva = 0
 END IF 

 -- Totalizando y desplegando totales 
 LET w_mae_tra.totdoc = (w_mae_tra.subtot) 
 LET w_mae_tra.subtot = (w_mae_tra.totdoc-w_mae_tra.totiva) 
 DISPLAY BY NAME w_mae_tra.efecti,w_mae_tra.tarcre,
                 w_mae_tra.totiva,w_mae_tra.totdoc
 DISPLAY xvuelto TO vuelto
END FUNCTION

-- Subrutina para ingresar la clave de seguridad para operacines especiales

FUNCTION facing001_seguridad()
 DEFINE passwd    LIKE fac_usuaxpos.passwd,
        username  LIKE fac_usuaxpos.userid,
        confirm   SMALLINT

 -- Desplegando pantalla de seguridad
 OPEN WINDOW wsecurity AT 9,27
  WITH FORM "facing001c"
  MESSAGE "   F4 Regresar" ATTRIBUTE(RED) 

  -- Ingresando clave de seguridad 
  LET confirm = TRUE  
  INPUT BY NAME passwd
   ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)

   ON ACTION cancel 
    LET confirm = FALSE 
    EXIT INPUT 

   AFTER FIELD passwd
    -- Verificanod password
    IF LENGTH(passwd)<=0 THEN
       ERROR "Error: password invalido."
       NEXT FIELD passwd
    END IF 

    -- Verificando clave de cajero
    LET username = NULL 
    SELECT UNIQUE a.userid
     INTO  username 
     FROM  fac_usuaxpos a
     WHERE a.numpos = w_mae_tra.numpos
       AND a.passwd = passwd
     IF (status=NOTFOUND) THEN
        ERROR "Error: password incorrecto."
        NEXT FIELD passwd 
     END IF
  END INPUT 
 CLOSE WINDOW wsecurity 

 RETURN confirm,username 
END FUNCTION 

-- Subrutina para grabar la facturacion 

FUNCTION facing001_grabar()
 DEFINE wsalact  LIKE fac_mtransac.saldos,
        i,correl SMALLINT,
        wapaid   INTEGER,
        wfecven  DATE,
        wfulpag  DATE,
        flag     SMALLINT,
        vfac_id INTEGER,
        cmd STRING
 DEFINE reg_e RECORD
   estatus LIKE facturafel_e.estatus,
   serie_e LIKE facturafel_e.serie_e,
   numdoc_e LIKE facturafel_e.numdoc_e,
   autorizacion LIKE facturafel_e.autorizacion
 END RECORD


	LET flag=FALSE 

 -- Grabando transaccion
 ERROR " Registrando Facturacion ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK
   -- 1. Grababdo encabezado de la facturacion 
   -- Asignando datos
   LET grabodocto       = TRUE 
   LET w_mae_tra.lnktra = 0 
   LET w_mae_tra.exenta = w_tip_doc.exeimp
   LET w_mae_tra.fecemi = CURRENT 
   LET w_mae_tra.feccor = CURRENT 
   LET w_mae_tra.fecsis = CURRENT 
   LET w_mae_tra.horsis = CURRENT HOUR TO SECOND
   LET w_mae_tra.usranl = NULL
   LET w_mae_tra.fecanl = NULL
   LET w_mae_tra.horanl = NULL
   LET w_mae_tra.lnkinv = 0 
   LET w_mae_tra.lnkapa = 0 
   LET w_mae_tra.numapa = 0 
   LET w_mae_tra.totori = 0 
   LET w_mae_tra.pordes = w_mae_ofe.pordes 

   -- Verificando nombre del cliente
   IF LENGTH(w_mae_tra.nomcli)=0 THEN 
      LET w_mae_tra.nomcli = "VENTA LOCAL" 
   END IF 

   -- Verificando total descuento
   IF w_mae_tra.totdes=0 THEN
      LET w_mae_tra.pordes = 0 
   END IF 

   -- Grabando
   SET LOCK MODE TO WAIT
   INSERT INTO fac_mtransac
   VALUES (w_mae_tra.*)
   LET w_mae_tra.lnktra = SQLCA.SQLERRD[2] 

   -- 2. Grabando detalle de la facturacion 
   LET correl = 0
   FOR i = 1 TO totlin 
     IF v_products[i].cditem IS NULL OR 
        v_products[i].canuni IS NULL THEN 
        CONTINUE FOR 
     END IF 
     LET correl = (correl+1) 

     -- Grabando
     SET LOCK MODE TO WAIT
     INSERT INTO fac_dtransac 
     VALUES (w_mae_tra.lnktra    , -- link del encabezado
             correl              , -- correlativo de ingreso
             w_mae_pos.codemp    , -- empresa del pos
             w_mae_pos.codsuc    , -- sucursal del pos
             w_mae_pos.codbod    , -- bodega del pos 
             v_products[i].codpro, -- codigo del producto 
             v_products[i].cditem, -- codigo del producto abreviado
             ""                  , -- Descripcion 
             v_products[1].unimed, -- unidad de medida del producto 
             v_products[i].codepq, -- codigo empaque
             v_products[i].canepq, -- cantidad empaque
             0,                    -- peso bruto del producto 
             v_products[i].canuni, -- cantidad original
             0,                    -- bonificaciones 
             v_products[i].preuni, -- precio unitario 
             v_products[i].preori, -- precio original
             v_products[i].descto, -- cantidad descuento  
             v_products[i].pctdes, -- porcentaje descuento  
             v_products[i].totpro, -- valor total 
             0,                    -- factor
             v_products[i].cliesp, -- 1= cliente precio especial,0=cliente normal
             v_products[i].rebaja, -- 1= producto rebajado 0= producto no rebajado 
             v_products[i].pesado, -- 1= producto pesado 0= producto no pesado   
             0,                    -- porcentaje isv
             0)                    -- total isv 
   END FOR 

   -- Actualizando inventario 
   SET LOCK MODE TO WAIT
   UPDATE fac_mtransac 
   SET    fac_mtransac.lnkinv = 1
   WHERE  fac_mtransac.lnktra = w_mae_tra.lnktra 

   ###############################################################
         #Registro en tablas de FEL
         IF es_dte = "S" THEN 
            CALL registra_tablas_FEL(1) RETURNING flag  --Inserta factura electronica
            IF flag = TRUE THEN
               ROLLBACK WORK 
            END IF
         END IF    
         ###############################################################

 -- Finalizando la transaccion
 COMMIT WORK

   --Envia factura a firma
   LET vfac_id = NULL;
      
   SELECT a.fac_id
   INTO vfac_id
   FROM facturafel_e a
   WHERE a.num_int = w_mae_tra.lnktra

   IF es_dte = "S" THEN
   
      LET cmd = "fglrun ande_fel_g4s.42r ",vfac_id USING "<<<<<<<"," segovia 0 0"
      
      IF ande_desa = 1 THEN 
         GOTO _imprimir 
      ELSE    
         DISPLAY "cmd: ",cmd
         RUN cmd
      END IF
      
   END IF 

   IF reg_e.estatus = "P" AND es_dte = "S" THEN
      -- Desplegando mensaje
      CALL fgl_winmessage(" Atencion","El documento no pudo ser firmado electronicamente.","stop")
   ELSE
      SELECT a.estatus, a.serie_e, a.numdoc_e, a.autorizacion
      INTO reg_e.estatus, reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion
      FROM facturafel_e a
      WHERE a.num_int = w_mae_tra.lnktra
      
      DISPLAY BY NAME reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion
      
      -- Desplegando mensaje
      IF es_dte = "S" THEN 
         CALL fgl_winmessage(" Atencion","No. de autorización electronica:"||reg_e.autorizacion,"information")
      END IF 
      
      
      

    LABEL _imprimir:
    -- Imprimiendo movimiento
    ERROR "Imprimiendo ... por favor espere ...."

    CALL facrpt001_facturacion(FALSE)
    ERROR "" 
  END IF   
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION facing001_inival(i)
 DEFINE i       SMALLINT,
        wfecha  VARCHAR(80) 
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_tra.* TO NULL 
   CLEAR FORM 
 END CASE 

 -- Inicializando datos
 LET w_mae_tra.numpos = xnumpos
 LET w_mae_tra.estado = "V" 
 LET w_mae_tra.hayord = 0 
 LET w_mae_tra.fecemi = TODAY 
 LET w_mae_tra.poriva = tasimp 
 LET w_mae_tra.tascam = 0
 LET w_mae_tra.moneda = 1           
 LET w_mae_tra.codcli = ClienteDefault 
 LET w_mae_tra.codven = VendedorDefault 
 LET w_mae_tra.noruta = RutaDefault 
 LET w_mae_tra.userid = username 
 LET w_mae_tra.usrope = username 
 LET w_mae_tra.fecsis = CURRENT 
 LET w_mae_tra.horsis = CURRENT HOUR TO SECOND
 LET w_mae_tra.efecti = 0
 LET w_mae_tra.cheque = 0
 LET w_mae_tra.monext = 0
 LET w_mae_tra.tarcre = 0
 LET w_mae_tra.ordcom = 0 
 LET w_mae_tra.depmon = 0 
 LET w_mae_tra.abonos = 0 
 LET w_mae_tra.saldos = 0 
 LET w_mae_tra.subtot = 0
 LET w_mae_tra.totiva = 0
 LET w_mae_tra.totdoc = 0
 LET w_mae_tra.totpag = 0 
 LET w_mae_tra.totdes = 0 
 LET w_mae_tra.lnkdes = 0 
 LET w_mae_tra.ordcmp = 0 
 LET w_mae_tra.totapa = 0 
 LET w_mae_tra.totpag = 0 
 LET w_mae_tra.pordes = 0 
 LET totlin           = 0 
 LET totval           = 0
 LET vuelto           = 0 
 LET xvuelto          = 0
 LET xrecibi          = 0 

 -- Obteniendo datos del punto de venta
 INITIALIZE w_mae_pos.* TO NULL
 CALL librut003_bpuntovta(w_mae_tra.numpos)
 RETURNING w_mae_pos.*,existe 
 CALL f.setElementText("nompos","PUNTO DE VENTA [ "||w_mae_pos.nompos CLIPPED||" ]")

 -- Llenando combo de tipos de documento x punto de venta
 CALL librut003_CbxTiposDocxPos(w_mae_tra.numpos)
 -- Llenando combo de vendedores 
 CALL librut003_CbxVendedores()
 -- Llenando combo de clientes 
 CALL librut003_CbxClientesLista(NumeroLista)

 -- Verificando si hay ofertas por periodo de fechas vigentes
 CALL librut003_ofertas(w_mae_tra.numpos,w_mae_tra.fecemi) 
 RETURNING hayofe,w_mae_ofe.* 
 IF NOT hayofe THEN 
    LET w_mae_ofe.pordes = 0
 ELSE
    LET msg = "  DESCUENTO ** ",w_mae_ofe.desofe CLIPPED," **" 
    CALL librut001_dpelement("titofe",msg) 
 END IF 

 -- Registrando descuento anterior
 LET pordesant = w_mae_ofe.pordes 

 -- Inicializando vectores de datos
 CALL facing001_inivec() 

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_tra.userid,w_mae_tra.fecemi,w_mae_tra.horsis,w_mae_tra.estado 
 DISPLAY BY NAME w_mae_tra.efecti,w_mae_tra.tarcre,w_mae_tra.totiva,w_mae_tra.totdoc
 DISPLAY BY NAME totlin,totval,vuelto,w_mae_tra.totdes,w_mae_tra.totpag 

 -- Desplegando fecha 
 LET wfecha = librut001_formatofecha(TODAY,1,wpais) CLIPPED
 CALL librut001_dpelement("fecdia",wfecha)
END FUNCTION

-- Subrutina para inicializar y limpiar los vectores de trabajo 

FUNCTION facing001_inivec()
 DEFINE i SMALLINT

 -- Inicializando vectores
 LET totlin = 0 
 CALL v_products.clear()

 -- Limpiando pantalla 
 DISPLAY ARRAY v_products TO s_products.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION 

-- Subrutina para buscar el precio de un producto segun lista

FUNCTION librut003_ListaPrecios(xcditem,xcantid)
 DEFINE xcditem LIKE inv_products.cditem,
        xcantid LIKE fac_dtransac.cantid,
        xpreuni LIKE fac_dtransac.preuni  

 -- Verificando si es cliente con precio especial
 SELECT NVL(a.preuni,0) 
  INTO  xpreuni
  FROM  vta_presxcli a
  WHERE a.codcli = w_mae_tra.codcli
    AND a.cditem = xcditem 
 IF (status=NOTFOUND) THEN
  -- Seleccionando precio 
  SELECT NVL(a.preuni,0)
   INTO  xpreuni
   FROM  vta_preuxcan a
   WHERE a.cditem = xcditem
    AND  xcantid BETWEEN a.canini AND a.canfin 
   IF (status=NOTFOUND) THEN
      LET xpreuni = 0 
   END IF 
 END IF 

 RETURN xpreuni 
END FUNCTION 

FUNCTION registra_tablas_FEL(tipooperacion)
DEFINE tipooperacion SMALLINT; --1=Emision de factura, 2=Anulaci�n de factura 
DEFINE flag SMALLINT
DEFINE fel_e RECORD LIKE facturafel_e.*
DEFINE fel_ed RECORD LIKE facturafel_ed.*
DEFINE emp_fel RECORD LIKE empresas1.*
DEFINE vtime DATETIME HOUR TO FRACTION(3)
DEFINE i SMALLINT
DEFINE vcodcat SMALLINT;
DEFINE v_subcat, v_color STRING 
DEFINE lprecio_t LIKE facturafel_ed.base

INITIALIZE fel_e.*, fel_ed.*, emp_fel TO NULL
LET vtime = CURRENT HOUR TO FRACTION;
LET flag = FALSE;

SELECT empresas1.*
INTO emp_fel.*
FROM empresas1
WHERE empresas1.bodega = 1


LET fel_e.serie   = w_mae_tra.nserie;
LET fel_e.num_doc = w_mae_tra.numdoc;
LET fel_e.tipod   = "FC"
LET fel_e.es_exenta = 0

--Establecimiento
-- TODO: Corregir establecimientos, ahorita solo está para 1 establecimiento
LET fel_e.sat_establecimiento = NULL 
SELECT sat_establecimiento INTO fel_e.sat_establecimiento 
  FROM fac_puntovta e
   WHERE e.numpos = w_mae_tra.numpos
IF fel_e.sat_establecimiento IS NULL OR fel_e.sat_establecimiento = 0 THEN
   LET fel_e.sat_establecimiento = 1
END IF 

   LET fel_e.sat_establecimiento = 2

IF tipooperacion = 1 THEN --ingreso
   LET fel_e.fecha   = w_mae_tra.fecemi;
   LET fel_e.fecha_em  = SFMT("%1-%2-%3T%4-06:00",YEAR(w_mae_tra.fecemi) USING "&&&&",MONTH(w_mae_tra.fecemi) USING "&&",DAY(w_mae_tra.fecemi) USING "&&",vtime);
   LET fel_e.fecha_anul = NULL; --SE LLENA SOLO SI ES ANULACION
   LET fel_e.estado_doc = "ORIGINAL"; --CUANDO SE FACTURA SE COLOCA ORIGINAL CUANDO SE ENVIA UNA ANULACION SE COLOCA "ANULADO"
   {IF w_mae_tra.tipfac = "E" THEN --Exportacion
      IF w_mae_tra.hayord THEN LET w_mae_tra.tdodol = w_mae_ord.totord END IF  
      LET fel_e.total_b          = w_mae_tra.tdodol; --Ej.  117.00
      LET fel_e.total_d          = w_mae_tra.tdodol; --Ej.  112.00 total con descuento, pero no manejan descuentos por lo que descuento es ero
      LET fel_e.ing_netosg       = w_mae_tra.tdodol; --Ej.  100.00
      LET fel_e.total_i          = 0;
      LET fel_e.total_iva1       = 0;
      LET fel_e.base1            = w_mae_tra.tdodol; --Ej.  100.00
      LET fel_e.tasa1            = 0; --Ej.   12.00
      LET fel_e.monto1           = 0; --Ej.   12.00
      LET fel_e.total_neto       = w_mae_tra.tdodol; --Ej.  100.00
      LET fel_e.total_en_letras  = librut001_numtolet(w_mae_tra.tdodol)
      LET fel_e.es_exportacion = 1;
      LET fel_e.incoterm = w_mae_tra.incote;
      LET fel_e.nomconcomprador = w_mae_tra.nomcom;
      LET fel_e.dirconcomprador = w_mae_tra.dircom;
      LET fel_e.otrreferencia = w_mae_tra.otrref;
   ELSE}
      LET fel_e.total_b          = w_mae_tra.totdoc; --Ej.  117.00
      LET fel_e.total_d          = w_mae_tra.totdoc; --Ej.  112.00 total con descuento, pero no manejan descuentos por lo que descuento es cero
      LET fel_e.ing_netosg       = w_mae_tra.subtot; --Ej.  100.00
      LET fel_e.total_i          = w_mae_tra.totiva; --Ej.   12.00 
      LET fel_e.total_iva1       = w_mae_tra.totiva; --Ej.   12.00
      LET fel_e.base1            = w_mae_tra.subtot; --Ej.  100.00 ******
      LET fel_e.tasa1            = w_mae_tra.poriva; --Ej.   12.00
      LET fel_e.monto1           = w_mae_tra.totiva; --Ej.   12.00 ******
      LET fel_e.total_neto       = w_mae_tra.totdoc; --Ej.  100.00
      LET fel_e.total_en_letras  = librut001_numtolet(w_mae_tra.totdoc)
      LET fel_e.es_exportacion = 0;
      --Si el impuesto viene en 0 o nulo
      IF fel_e.monto1 <= 0 OR fel_e.monto1 IS NULL THEN
         IF w_mae_tra.tipdoc = 4 THEN --factura exenta
            LET fel_e.es_exenta = 1
         ELSE  
            LET fel_e.monto1 = fel_e.base1 / 1.12 * 0.12
         END IF    
      END IF 
   --END IF
   LET fel_e.monto_d          = 0                 --Ej.    5.00, poner cero, porque no manejan descuentos
   LET fel_e.tipo1            = "IVA";
   LET fel_e.id_factura       = 0 --Apunta a un apuntador recursivo cuando es anulacion o NC, si es factura va nulo--De donde se obtiene
   
   IF w_mae_tra.totpag = 0 THEN
      LET fel_e.tipo_pago = "CR";
   ELSE
      LET fel_e.tipo_pago = "CO";
   END IF
ELSE
   LET fel_e.fecha   = TODAY;
   LET fel_e.fecha_em  = SFMT("%1-%2-%3T%4-06:00",YEAR(fel_e.fecha) USING "&&&&",MONTH(fel_e.fecha) USING "&&",DAY(fel_e.fecha) USING "&&",vtime);
   LET fel_e.fecha_anul = fel_e.fecha_em;
   LET fel_e.estado_doc = "ANULADO"; --CUANDO SE FACTURA SE COLOCA ORIGINAL CUANDO SE ENVIA UNA ANULACION SE COLOCA "ANULADO"

   SELECT facturafel_e.fac_id
   INTO fel_e.id_factura
   FROM facturafel_e
   WHERE facturafel_e.num_int  = w_mae_tra.lnktra
   AND facturafel_e.estado_doc = "ORIGINAL"
   AND facturafel_e.tipod = "FC"

   LET fel_e.tipo_pago        = "CO";
   LET fel_e.nota1 = w_mae_tra.motanl;
END IF

LET fel_e.ordcmp        = w_mae_tra.ordcmp --Orden de compra
LET fel_e.from          = emp_fel.e_mail; --  DEJAR QUEMADO EL CORREO QUE SE COLOCARA AQUI   
LET fel_e.to            = NULL --w_mae_tra.maicli;
LET fel_e.cc            = NULL;
LET fel_e.formats       = "PDF"; 
LET fel_e.tipo_doc      = "FACT"; --DEJAR QUEMADO

LET fel_e.ant_serie = NULL;  
LET fel_e.ant_numdoc = NULL; 
LET fel_e.ant_fecemi = NULL;
LET fel_e.ant_resoluc = NULL;
IF w_mae_tra.moneda = 1 THEN 
   LET fel_e.c_moneda   = "GTQ"; 
   LET fel_e.tipo_cambio      = 1;
ELSE
   LET fel_e.c_moneda   = "USD";
   LET fel_e.tipo_cambio      = w_mae_tra.tascam;
END IF

LET fel_e.num_int          = w_mae_tra.lnktra;
LET fel_e.nit_e            = emp_fel.nit; --tomarlo de la tabla empresas1.nit donde la bodega 1; 
LET fel_e.nombre_c         = emp_fel.nom_sucursal;-- tomarlo de la tabla empresas1.nom_sucursal
--TODO: Corregir lo de las sucursales
LET fel_e.nombre_c         = "CENTRO DE SERVICIOS AUTOMOTRICES STOCK CAR";
LET fel_e.idioma_e         = "ES";
LET fel_e.nombre_e         = emp_fel.razon_social; --tomarlo de la tabla empresas1.razon_social
--LET fel_e.codigo_e         = emp_fel.cod_sucursal; --Tomarlo de la tabla empresas1.cod_sucursal
--TODO: Revisar codigo de establecimiento
LET fel_e.codigo_e         = 2
LET fel_e.dispositivo_e    = NULL;
LET fel_e.direccion_e      = emp_fel.direccion1; --tomarlo de la tabla empresas1.direccion1
LET fel_e.departamento_e   = emp_fel.depto; --tomarlo de la tabla empresas1.depto
LET fel_e.municipio_e      = emp_fel.municipio; --tomarlo de la tabla empresas1.municipio
LET fel_e.pais_e           = "GT" --dejarlo quemado
LET fel_e.codpos_e         = emp_fel.codigo_postal --tomarlo de la tabla empresas1.codigo_postal
IF w_mae_tra.numnit = "C/F" THEN LET w_mae_tra.numnit = "CF" END IF
IF w_mae_tra.numnit = "C-F" THEN LET w_mae_tra.numnit = "CF" END IF 
LET fel_e.nit_r            = w_mae_tra.numnit;
LET fel_e.es_cui           = NULL;
LET fel_e.nombre_r         = w_mae_tra.nomcli;
LET fel_e.idioma_r         = "ES";
LET fel_e.direccion_r      = w_mae_tra.dircli;
LET fel_e.departamento_r   = NULL;
LET fel_e.municipio_r      = NULL;
LET fel_e.pais_r           = "GT";
LET fel_e.codpos_r         = NULL;
LET fel_e.serie_e          = NULL; --este campo lo llena la interfaz fel
LET fel_e.numdoc_e         = NULL; --este campo lo llena la interfaz fel
LET fel_e.autorizacion     = NULL; --este campo lo llena la interfaz fel
LET fel_e.estatus          = "P"; --P=pendiente, la interface lo vuelve C=certificado si todo bien
LET fel_e.fac_id           = 0; --Llave primaria 
LET fel_e.p_descuento      = 0; --PONER CERO no manejan descuento detallado en la factura
LET fel_e.mto_descuento    = 0; -- PONER CERO no manejan descuento detallado en la factura
LET fel_e.fel_msg          = NULL; --Lo llena la interfaz de fel
LET fel_e.info_reg         = "PAGO_TRIMESTRAL";
LET fel_e.num_oc           = w_mae_tra.ordcmp; --Número de Orden de Compra
LET fel_e.un_pago_de       = NULL;
LET fel_e.npagos           = NULL;
LET fel_e.mto_pago         = NULL;
LET fel_e.dia_pago         = NULL;
LET fel_e.fecha_pago       = NULL;
LET fel_e.nom_firma        = NULL;
LET fel_e.dpi_firma        = NULL;
LET fel_e.vencod           = w_mae_tra.usrope --AMPLIAR CAMPO EN TBLA A 15
LET fel_e.num_interno      = w_mae_tra.nserie CLIPPED||"-"||w_mae_tra.numdoc; 

SET LOCK MODE TO WAIT

   WHENEVER ERROR CONTINUE
   INSERT INTO facturafel_e(
   serie,num_doc,tipod,fecha,fecha_em,fecha_anul,
   from,to,cc,formats,tipo_doc,estado_doc,ant_serie,ant_numdoc,
   ant_fecemi,ant_resoluc,c_moneda,tipo_cambio,num_int,nit_e,nombre_c,
   idioma_e,nombre_e,codigo_e,dispositivo_e,direccion_e,departamento_e,municipio_e,
   pais_e,codpos_e,nit_r,es_cui,nombre_r,idioma_r,direccion_r,departamento_r,
   municipio_r,pais_r,codpos_r,total_b,total_d,monto_d,total_i,ing_netosg,total_iva1,
   tipo1,base1,tasa1,monto1,total_neto,serie_e,numdoc_e,autorizacion,estatus,
   fac_id,p_descuento,mto_descuento,fel_msg,id_factura,total_en_letras,tipo_pago,
   num_oc,un_pago_de,npagos,mto_pago,dia_pago,fecha_pago,nom_firma,dpi_firma,
   vencod,num_interno,info_reg,es_exportacion,incoterm,nomconcomprador,dirconcomprador,otrreferencia, 
   es_exenta, sat_establecimiento)
   VALUES(
   fel_e.serie, fel_e.num_doc, fel_e.tipod, fel_e.fecha, fel_e.fecha_em, fel_e.fecha_anul, 
   fel_e.from, fel_e.to, 
   fel_e.cc, fel_e.formats, fel_e.tipo_doc, fel_e.estado_doc, fel_e.ant_serie,
   fel_e.ant_numdoc, fel_e.ant_fecemi, fel_e.ant_resoluc, fel_e.c_moneda,
   fel_e.tipo_cambio, fel_e.num_int, fel_e.nit_e, fel_e.nombre_c,
   fel_e.idioma_e, fel_e.nombre_e, fel_e.codigo_e, fel_e.dispositivo_e, fel_e.direccion_e,
   fel_e.departamento_e, fel_e.municipio_e, fel_e.pais_e, fel_e.codpos_e, fel_e.nit_r,
   fel_e.es_cui, fel_e.nombre_r, fel_e.idioma_r, fel_e.direccion_r, fel_e.departamento_r,
   fel_e.municipio_r, fel_e.pais_r, fel_e.codpos_r, fel_e.total_b, fel_e.total_d,
   fel_e.monto_d, fel_e.total_i, fel_e.ing_netosg, fel_e.total_iva1, fel_e.tipo1,
   fel_e.base1, fel_e.tasa1, fel_e.monto1, fel_e.total_neto, fel_e.serie_e, fel_e.numdoc_e,
   fel_e.autorizacion, fel_e.estatus, fel_e.fac_id, fel_e.p_descuento, fel_e.mto_descuento,
   fel_e.fel_msg, fel_e.id_factura, fel_e.total_en_letras, fel_e.tipo_pago, fel_e.num_oc,
   fel_e.un_pago_de, fel_e.npagos, fel_e.mto_pago, fel_e.dia_pago, fel_e.fecha_pago,
   fel_e.nom_firma, fel_e.dpi_firma, fel_e.vencod, fel_e.num_interno, fel_e.info_reg,
   fel_e.es_exportacion, fel_e.incoterm, fel_e.nomconcomprador, fel_e.dirconcomprador, fel_e.otrreferencia,
   fel_e.es_exenta, fel_e.sat_establecimiento)

   WHENEVER ERROR STOP

   IF sqlca.sqlcode < 0 THEN
      ERROR "Existió un error al insertar el encabezado de facura FEL"
      LET flag = TRUE
      RETURN flag
   ELSE 
      LET fel_e.fac_id = SQLCA.sqlerrd[2] --Registrando valor serial asignado en la insercion del registro
      LET gfac_id = fel_e.fac_id
      DISPLAY "============================Pase por graba_FEL ", gfac_id 
   END IF 

   --IF NOT w_mae_tra.hayord THEN
      FOR i = 1 TO v_products.getLength() 
         IF v_products[i].cditem IS NULL OR 
            v_products[i].canuni IS NULL THEN 
               CONTINUE FOR 
         END IF
         INITIALIZE fel_ed.* TO NULL;

         LET fel_ed.fac_id = fel_e.fac_id;
         LET fel_ed.serie  = fel_e.serie;
         LET fel_ed.num_doc = fel_e.num_doc;
         LET fel_ed.tipod  = fel_e.tipod;
         LET fel_ed.fecha  = fel_e.fecha;
         LET fel_ed.descrip_p = v_products[i].dsitem CLIPPED;
         LET fel_ed.codigo_p  =  v_products[i].codpro;
         LET fel_ed.unidad_m  = v_products[i].nomuni;
         LET fel_ed.cantidad  = v_products[i].canuni; --Ej.    10
         {LET fel_ed.cantidad  = v_products[i].canori
         IF w_mae_tra.tipfac = "E" THEN
            LET fel_ed.precio_u  = v_products[i].predol;    --Ej.  6513.00
            LET fel_ed.precio_t  = v_products[i].prtdol;    --Ej. 65130.00
            LET fel_ed.precio_p  = v_products[i].prtdol;    --Ej.    42350.00
            LET fel_ed.precio_m  = v_products[i].prtdol;    --Ej.   42350.00
            LET fel_ed.total_iva = 0;
            LET fel_ed.total_imp = 0;
            LET fel_ed.ing_netos = v_products[i].prtdol; --Ej.    42350.00
            LET fel_ed.base      = v_products[i].prtdol - fel_ed.total_iva; --Ej.   37812.50
            LET fel_ed.tasa      = 0;
            LET fel_ed.monto     = fel_ed.total_iva;  --Ej     4537.50

         ELSE}
            IF w_mae_tra.tipdoc = 4 THEN --factura exenta
               LET fel_ed.total_iva = 0
               LET fel_ed.total_imp = 0
               LET fel_ed.base      = v_products[i].totpro  
               LET fel_ed.monto     = fel_ed.total_iva;  
            ELSE
               LET fel_ed.total_iva = v_products[i].totpro - (v_products[i].totpro/(1+(fel_e.tasa1/100)));   -- Ej. 4537.50
               LET fel_ed.total_imp = fel_ed.total_iva / fel_ed.cantidad; --Ej.      453.75
               LET fel_ed.base      = v_products[i].totpro  - fel_ed.total_iva; --Ej.   37812.50
               LET fel_ed.monto     = fel_ed.total_iva;  --Ej     4537.50
            END IF 
            LET fel_ed.precio_u  = v_products[i].preuni;    --Ej.  6513.00
            LET fel_ed.precio_t  = v_products[i].totpro ;    --Ej. 65130.00
            LET fel_ed.precio_p  = v_products[i].totpro;    --Ej.    42350.00
            LET fel_ed.precio_m  = v_products[i].totpro;    --Ej.   42350.00
            
            
            LET fel_ed.ing_netos = v_products[i].totpro; --Ej.    42350.00
            
            LET fel_ed.tasa      = fel_e.tasa1; --Ej    12.00000
            
         
         --END IF
         
         LET fel_ed.descto_t  = 0;  --No se manejan descuentos Ej.    22780.00
         LET fel_ed.descto_m  = 0; --NO se manejan descuentos  Ej.    22780.00
         
         
         LET fel_ed.tipo      = "IVA";
         
         LET vcodcat = NULL;
         SELECT inv_products.codcat
         INTO vcodcat
         FROM inv_products
         WHERE inv_products.cditem = v_products[i].codpro
         
         IF vcodcat = 6 THEN --OTROS  (SERVICIOS)
            LET fel_ed.categoria  =   "SERVICIO" --PREGUNTAR DONDE SE PUEDE VER SI ES BIEN O SERVICIO
         ELSE
            LET fel_ed.categoria  =   "BIEN" --PREGUNTAR DONDE SE PUEDE VER SI ES BIEN O SERVICIO
         END IF
         
         {IF v_products[i].deitemh IS NULL THEN
            LET fel_ed.txt_especial = "N";
         ELSE
            LET fel_ed.txt_especial = "S";
            LET fel_ed.txt_largo = v_products[i].deitemh;
         END IF}
         
         SET LOCK MODE TO WAIT

         WHENEVER ERROR CONTINUE
         INSERT INTO facturafel_ed(
         fac_id,serie,num_doc,tipod,fecha,descrip_p,codigo_p,unidad_m,cantidad,precio_u,
         precio_t,descto_t,descto_m,precio_p,precio_m,total_imp,ing_netos,total_iva,
         tipo,base,tasa,monto,categoria,txt_especial,txt_largo)
         VALUES(      
         fel_ed.fac_id,fel_ed.serie,fel_ed.num_doc,fel_ed.tipod,fel_ed.fecha,fel_ed.descrip_p,
         fel_ed.codigo_p,fel_ed.unidad_m,fel_ed.cantidad,fel_ed.precio_u,fel_ed.precio_t,
         fel_ed.descto_t,fel_ed.descto_m,fel_ed.precio_p,fel_ed.precio_m,fel_ed.total_imp,
         fel_ed.ing_netos,fel_ed.total_iva,fel_ed.tipo,fel_ed.base,fel_ed.tasa,fel_ed.monto,
         fel_ed.categoria,fel_ed.txt_especial,fel_ed.txt_largo)

         WHENEVER ERROR STOP
         IF sqlca.sqlcode < 0 THEN
            ERROR "Existió un problema al insertar el detalle de la factura FEL"
            LET flag = TRUE
            EXIT FOR
         END IF
      END FOR
   {ELSE --hay orden 
      FOR i = 1 TO v_ordenes.getLength()
         #IF v_products[i].cditem IS NULL OR v_products[i].canuni IS NULL THEN 
         IF v_ordenes[i].nuitem1 IS NULL THEN 
               CONTINUE FOR 
         END IF
         INITIALIZE fel_ed.* TO NULL;

         LET fel_ed.fac_id = fel_e.fac_id;
         LET fel_ed.serie  = fel_e.serie;
         LET fel_ed.num_doc = fel_e.num_doc;
         LET fel_ed.tipod  = fel_e.tipod;
         LET fel_ed.fecha  = fel_e.fecha;
         
         #LET fel_ed.descrip_p = v_products[i].dsitem CLIPPED;
         SELECT a.nomsub INTO v_subcat FROM  inv_subcateg a WHERE a.subcat = v_ordenes[i].subcat1
         SELECT a.nomcol INTO v_color FROM  inv_colorpro a WHERE a.codcol = v_ordenes[i].codcol1

         #LET fel_ed.unidad_m  = v_products[i].nomuni;
         SELECT nommed INTO fel_ed.unidad_m FROM inv_unimedid WHERE unimed = v_ordenes[i].medida1 ;
         

         LET v_ordenes[i].descrp1 = v_subcat CLIPPED, " ", v_color CLIPPED, " DE ", v_ordenes[i].xlargo1, " POR ", v_ordenes[i].yancho1
         LET fel_ed.linea1 = v_subcat CLIPPED, " ", v_color CLIPPED
         LET fel_ed.linea2 = v_ordenes[i].xlargo1, " X ", v_ordenes[i].yancho1 CLIPPED, ' ', fel_ed.unidad_m

         #LET fel_ed.descrip_p = v_ordenes[i]. .descrp1 CLIPPED; 
         
         #LET fel_ed.codigo_p  =  v_products[i].codpro;
         LET fel_ed.codigo_p  =  "ESP01";
         
         
         #LET fel_ed.cantidad  = v_products[i].canori
         LET fel_ed.cantidad  = v_ordenes[i].cantid1
         
         IF w_mae_tra.tipfac = "E" THEN
            #LET fel_ed.precio_u  = v_products[i].predol;    --Ej.  6513.00
            LET fel_ed.precio_u  = v_ordenes[i].precio1;    --Ej.  6513.00
            
            #LET fel_ed.precio_t  = v_products[i].prtdol;    --Ej. 65130.00
            LET fel_ed.precio_t  = v_ordenes[i].precio1 * v_ordenes[i].cantid1 ;
            
            #LET fel_ed.precio_p  = v_products[i].prtdol;    --Ej.    42350.00
            LET fel_ed.precio_p  = v_ordenes[i].precio1;    --Ej.    42350.00
            
            #LET fel_ed.precio_m  = v_products[i].prtdol;    --Ej.   42350.00
            LET fel_ed.precio_m  = v_ordenes[i].precio1;    --Ej.   42350.00
            
            LET fel_ed.total_iva = 0;
            LET fel_ed.total_imp = 0;

            #LET fel_ed.ing_netos = v_products[i].prtdol; --Ej.    42350.00
            LET fel_ed.ing_netos = v_ordenes[i].precio1; --Ej.    42350.00
            
            #LET fel_ed.base      = v_products[i].prtdol - fel_ed.total_iva; --Ej.   37812.50

            LET fel_ed.base      = v_ordenes[i].precio1 * v_ordenes[i].cantid1 
            --LET fel_ed.base      = v_ordenes[i].precio1 - fel_ed.total_iva; --Ej.   37812.50
               
            
            LET fel_ed.tasa      = 0;
            
            #LET fel_ed.monto     = fel_ed.total_iva;  --Ej     4537.50
            LET fel_ed.monto     = fel_ed.total_iva;  --Ej     4537.50

         ELSE
            #LET fel_ed.precio_u  = v_products[i].preuni;    --Ej.  6513.00
            LET fel_ed.precio_u  = v_ordenes[i].precio1;    --Ej.  6513.00
            
            #LET fel_ed.precio_t  = v_products[i].totpro;    --Ej. 65130.00
            LET lprecio_t = v_ordenes[i].precio1 * v_ordenes[i].cantid1
            LET fel_ed.precio_t  = lprecio_t;    --Ej. 65130.00
            
            #LET fel_ed.precio_p  = v_products[i].totpro;    --Ej.    42350.00
            LET fel_ed.precio_p  = lprecio_t;    --Ej.    42350.00
            
            #LET fel_ed.precio_m  = v_products[i].totpro;    --Ej.   42350.00
            LET fel_ed.precio_m  = lprecio_t;    --Ej.   42350.00
            
            #LET fel_ed.total_iva = v_products[i].totpro - (v_products[i].totpro/(1+(fel_e.tasa1/100)));   -- Ej. 4537.50
            LET fel_ed.total_iva = lprecio_t - (lprecio_t/(1+(fel_e.tasa1/100)));   -- Ej. 4537.50
            
            #LET fel_ed.total_imp = fel_ed.total_iva / fel_ed.cantidad; --Ej.      453.75
            LET fel_ed.total_imp = fel_ed.total_iva / fel_ed.cantidad; --Ej.      453.75
            
            #LET fel_ed.ing_netos = v_products[i].totpro; --Ej.    42350.00
            LET fel_ed.ing_netos = lprecio_t; --Ej.    42350.00
            
            #****LET fel_ed.base      = v_products[i].totpro - fel_ed.total_iva; --Ej.   37812.50
            LET fel_ed.base      = lprecio_t - fel_ed.total_iva; --Ej.   37812.50
            
            #LET fel_ed.tasa      = fel_e.tasa1; --Ej    12.00000
            LET fel_ed.tasa      = fel_e.tasa1; --Ej    12.00000
            
            #LET fel_ed.monto     = fel_ed.total_iva;  --Ej     4537.50
            LET fel_ed.monto     = fel_ed.total_iva;  --Ej     4537.50
         
         END IF
         
         LET fel_ed.descto_t  = 0;  --No se manejan descuentos Ej.    22780.00
         LET fel_ed.descto_m  = 0; --NO se manejan descuentos  Ej.    22780.00
         
         
         LET fel_ed.tipo      = "IVA";
         
         
            LET fel_ed.categoria  =   "BIEN" --PREGUNTAR DONDE SE PUEDE VER SI ES BIEN O SERVICIO
         

         LET fel_ed.txt_especial = "S";
         LET fel_ed.txt_largo = v_ordenes[i].descrp1
         
         
         SET LOCK MODE TO WAIT

         WHENEVER ERROR CONTINUE
         INSERT INTO facturafel_ed(
         fac_id,serie,num_doc,tipod,fecha,descrip_p,codigo_p,unidad_m,cantidad,precio_u,
         precio_t,descto_t,descto_m,precio_p,precio_m,total_imp,ing_netos,total_iva,
         tipo,base,tasa,monto,categoria,txt_especial,txt_largo, linea1, linea2)
         VALUES(      
         fel_ed.fac_id,fel_ed.serie,fel_ed.num_doc,fel_ed.tipod,fel_ed.fecha,fel_ed.descrip_p,
         fel_ed.codigo_p,fel_ed.unidad_m,fel_ed.cantidad,fel_ed.precio_u,fel_ed.precio_t,
         fel_ed.descto_t,fel_ed.descto_m,fel_ed.precio_p,fel_ed.precio_m,fel_ed.total_imp,
         fel_ed.ing_netos,fel_ed.total_iva,fel_ed.tipo,fel_ed.base,fel_ed.tasa,fel_ed.monto,
         fel_ed.categoria,fel_ed.txt_especial,fel_ed.txt_largo, fel_ed.linea1, fel_ed.linea2)

         WHENEVER ERROR STOP
         IF sqlca.sqlcode < 0 THEN
            ERROR "Existi� un problema al insertar el detalle de la factura FEL"
            LET flag = TRUE
            EXIT FOR
         END IF
      END FOR 
   END IF} 
RETURN flag 
END FUNCTION
