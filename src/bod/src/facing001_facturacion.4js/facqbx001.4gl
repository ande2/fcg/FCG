{ 
Objetivo : Programa de criterios de seleccion para:
           Consulta/Anulacion/Impresion de facturacion de productos
Programo : Mynor Ramirez
Fecha    : Diciembre 2011 
}

-- Definicion de variables globales 
GLOBALS "facglb001.4gl"
DEFINE maxdiamod,maxdiaanl SMALLINT  

-- Subrutina para consultar/anular/imprimir facturacion 

FUNCTION facqbx001_facturacion(operacion) --2=Anulacion
 DEFINE qrytext,qrypart     CHAR(500),
        loop,existe         SMALLINT,
        operacion           SMALLINT,
        acceso              SMALLINT, 
        titmenu             CHAR(15),
        qryres,msg,titqry   CHAR(80),
        wherestado          STRING,
        opc                 INTEGER, 
        userauth            CHAR(15) 

 -- Obteniendo maximo de dias de mofificacion 
 CALL librut003_parametros(10,1)
 RETURNING existe,maxdiamod
 IF NOT existe THEN
    CALL fgl_winmessage(
    "Atencion",
    "No existe registrado maximo de dias de modificacion de un documento."||
    "\nDefinir parametro antes de facturar.",
    "stop")
    RETURN
 END IF

 -- Obteniendo maximo de dias de anulacion 
 CALL librut003_parametros(10,2)
 RETURNING existe,maxdiaanl  
 IF NOT existe THEN
    CALL fgl_winmessage(
    "Atencion",
    "No existe registrado maximo de dias de anulacion de un documento."||
    "\nDefinir parametro antes de facturar.",
    "stop")
    RETURN
 END IF

 -- Verificando operacion
 CASE (operacion)
  WHEN 1 LET titmenu    = " Consultar"
         LET wherestado = NULL
  WHEN 2 LET titmenu    = " Anular" 
         LET wherestado = " AND a.estado IN ('V') AND a.fecemi >= TODAY-"||maxdiaanl 
 END CASE 

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Mostrando campo de motivo de anulacion 
 CALL f.setFieldHidden("formonly.motanl",0)
 CALL f.setElementHidden("titanl",0)

 -- Buscando datos 
 LET loop = TRUE 
 WHILE loop 
  -- Inicializando las variables 
  INITIALIZE qrytext,qrypart TO NULL
  LET int_flag = 0 
  CLEAR FORM

  -- Construyendo busqueda 
  CONSTRUCT BY NAME qrytext 
                 ON a.userid,
                    a.fecemi,
                    a.horsis,
                    a.estado, 
                    a.feccor, 
                    a.usrope,
                    a.pordes, 
                    a.lnktdc, 
                    a.nserie,  
                    a.numdoc,
                    a.codven,
                    a.codcli,
                    a.numnit,
                    a.dircli,  
                    a.efecti,
                    a.tarcre,
                    a.totdoc,
                    a.totiva,
                    a.totdes

   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT CONSTRUCT
  END CONSTRUCT
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Preparando la busqueda 
  ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

  -- Creando la busqueda 
  LET qrypart = "SELECT a.lnktra ",
                 "FROM  fac_mtransac a ",
                 "WHERE a.numpos = ",xnumpos,
                  " AND ", qrytext CLIPPED,
                  wherestado CLIPPED, 
                  " ORDER BY a.fecemi DESC,a.lnktra DESC" 

  -- Declarando el cursor 
  PREPARE estqbe001 FROM qrypart
  DECLARE c_factur SCROLL CURSOR WITH HOLD FOR estqbe001  
  OPEN c_factur 
  FETCH FIRST c_factur INTO w_mae_tra.lnktra
  IF (status = NOTFOUND) THEN
     INITIALIZE w_mae_tra.* TO NULL
     ERROR ""

     -- Verificando operacion
     CASE (operacion)
      WHEN 1  
       CALL fgl_winmessage(
       "Atencion",
       "No existen documentos con el criterio seleccionado.",
       "stop") 
      WHEN 2
       CALL fgl_winmessage(
       "Atencion",
       "No existen documentos con el criterio seleccionado. \n"||
       "La fecha del documento es anterior a los dias permitidos para anular.", 
       "stop") 
     END CASE 
  ELSE
     ERROR "" 

     -- Desplegando datos 
     CALL facqbx001_datos()

     -- Fetchando documentos 
     MENU titmenu 
      BEFORE MENU 
       -- Verificando tipo de operacion 
       CASE (operacion)
        WHEN 1 HIDE OPTION "Anular" 
        WHEN 2 HIDE OPTION "Modificar"
               HIDE OPTION "Imprimir" 
       END CASE 

      COMMAND "Anterior"
       "Visualiza el anterior documento en la lista."
       FETCH NEXT c_factur INTO w_mae_tra.lnktra 
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas documentos anteriores en lista.", 
           "information")
           FETCH LAST c_factur INTO w_mae_tra.lnktra
        END IF 

       -- Desplegando datos 
       CALL facqbx001_datos()

      COMMAND "Siguiente"
       "Visualiza el documento siguiente en la lista."
       FETCH PREVIOUS c_factur INTO w_mae_tra.lnktra
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas documentos siguientes en lista.", 
           "information")
           FETCH FIRST c_factur INTO w_mae_tra.lnktra
        END IF

       -- Desplegando datos 
       CALL facqbx001_datos()

      COMMAND "Primero" 
       "Visualiza el primer documento en la lista."
       FETCH FIRST c_factur INTO w_mae_tra.lnktra
        -- Desplegando datos 
        CALL facqbx001_datos()

      COMMAND "Ultimo" 
       "Visualiza el ultimo documento en la lista."
       FETCH LAST c_factur INTO w_mae_tra.lnktra
        -- Desplegando datos
        CALL facqbx001_datos()

      COMMAND "Anular"
       "Permite anular el documento actual en pantalla."

       -- Verificando si documento ya fue anulado
       IF (w_mae_tra.estado="A") THEN
          CALL fgl_winmessage(
          "Atencion",
          "Documento ya fue anulado.",
          "stop")
          CONTINUE MENU 
       END IF 

       -- Verificando acceso para anular
       CALL facing001_seguridad()
       RETURNING acceso,userauth
       IF NOT acceso THEN
          CONTINUE MENU 
       END IF

       -- Anulando documento 
       IF facqbx001_AnularDocumento() THEN
          CONTINUE MENU 
       END IF 

       -- Desplegando datos
       CALL facqbx001_datos()

      COMMAND "Productos"
       "Permite visualizar el detalle completo de productos del documento."
       CALL facing001_verproductos()

      COMMAND "Consultar" 
       "Regresa a la pantalla de seleccion."
       EXIT MENU

      ON ACTION imprimir
       -- Verificando si documento ya fue anulado
       IF (w_mae_tra.estado="A") THEN
          CALL fgl_winmessage(
          "Atencion",
          "Documento ya fue anulado, no puede reimprimirse.",
          "stop")
          CONTINUE MENU 
       END IF 

       -- Desplegando datos
       CALL facqbx001_datos()

       -- Verificando acceso para reimprimir 
       CALL facing001_seguridad()
       RETURNING acceso,userauth
       IF NOT acceso THEN
          CONTINUE MENU 
       END IF

       -- Reimpresion de documentos
       CALL facrpt001_facturacion(TRUE)

      ON ACTION modificar
       -- Verificando si documento ya fue anulado
       IF (w_mae_tra.estado="A") THEN
          CALL fgl_winmessage(
          "Atencion",
          "Documento ya fue anulado, no puede modificarse.",
          "stop")
          CONTINUE MENU 
       END IF 

       -- Verificando acceso para modificar
       CALL facing001_seguridad()
       RETURNING acceso,userauth
       IF NOT acceso THEN
          CONTINUE MENU 
       END IF

       -- Modificando documento
       IF facqbx001_modificar() THEN
       END IF 

       -- Desplegando datos
       CALL facqbx001_datos()

      ON ACTION cancel
       LET loop = FALSE
       EXIT MENU

      COMMAND KEY(F4,CONTROL-E)
       EXIT MENU
     END MENU
   END IF     
  CLOSE c_factur
 END WHILE

 -- Escondiendo campo de motivo de anulacion 
 CALL f.setFieldHidden("formonly.motanl",1)
 CALL f.setElementHidden("titanl",1)

 -- Inicializando datos 
 CALL facing001_inival(1)
END FUNCTION 

-- Subrutina para desplegar los datos del documento 

FUNCTION facqbx001_datos()
 DEFINE existe SMALLINT
 DEFINE reg_e RECORD
   estatus        LIKE facturafel_e.estatus,
   serie_e        LIKE facturafel_e.serie_e,
   numdoc_e       LIKE facturafel_e.numdoc_e,
   autorizacion   LIKE facturafel_e.autorizacion
 END RECORD

 -- Desplegando datos 
 CLEAR FORM

 -- Obteniendo datos del documento 
 CALL librut003_bfacturacion(w_mae_tra.lnktra)
 RETURNING w_mae_tra.*,existe

 -- Obteniendo datos del tipo de documento
 INITIALIZE w_tip_doc.* TO NULL
 CALL librut003_btipodoc(w_mae_tra.tipdoc)
 RETURNING w_tip_doc.*,existe 

 -- Desplegando datos del documento 
 CLEAR FORM 
 DISPLAY BY NAME w_mae_tra.userid,w_mae_tra.fecemi,
                 w_mae_tra.horsis,w_mae_tra.usrope,
                 w_mae_tra.estado,w_mae_tra.pordes,
                 w_mae_tra.lnktdc,w_mae_tra.nserie,
                 w_mae_tra.numdoc,w_mae_tra.codven,
                 w_mae_tra.codcli,w_mae_tra.numnit,
                 w_mae_tra.nomcli,w_mae_tra.dircli,
                 w_mae_tra.efecti,w_mae_tra.tarcre,
                 w_mae_tra.totdoc,w_mae_tra.totiva,
                 w_mae_tra.motanl,w_mae_tra.feccor

   SELECT a.estatus, a.serie_e, a.numdoc_e, a.autorizacion
      INTO reg_e.estatus, reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion
      FROM facturafel_e a
      WHERE a.num_int = w_mae_tra.lnktra
      AND a.estado_doc = 'ORIGINAL'
   DISPLAY BY NAME reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion
   
 -- Seleccionando detalle del documento
 CALL facqbx001_detalle(w_mae_tra.lnktra,0)
END FUNCTION 

-- Subrutina para seleccionar datos del detalle del documento 

FUNCTION facqbx001_detalle(wlnktra,operacion)
 DEFINE existe     SMALLINT,
        operacion  SMALLINT,
        wlnktra    INT 

 -- Inicializando vector de productos
 CALL facing001_inivec()

 -- Seleccionando detalle del documento 
 DECLARE cdet CURSOR FOR
 SELECT x.cditem,
        x.codabr,
        x.codepq,
        x.canepq,
        x.cantid,
        y.dsitem,
        y.unimed,
        z.nomabr, 
        x.preuni,
        x.preori,
        x.pordes,
        x.descto, 
        x.totpro,
        x.cliesp,
        x.rebaja,
        x.pesado, 
        "",
        x.correl
  FROM  fac_dtransac x, inv_products y, inv_unimedid z
  WHERE (x.lnktra = wlnktra)
    AND (y.cditem = x.cditem)
    AND (z.unimed = y.unimed) 
  ORDER BY x.correl 

  LET totlin = 1 
  FOREACH cdet INTO v_products[totlin].*
   -- Verificando si hay descuento y rebaja 
   IF v_products[totlin].pesado THEN LET v_products[totlin].imgpes = imagenpes END IF

   -- Acumulando contador
   LET totlin = (totlin+1) 
  END FOREACH
  CLOSE cdet
  FREE  cdet
  LET totlin = (totlin-1)  

  -- Desplegando datos del detalle
  DISPLAY ARRAY v_products TO s_products.* 
   ATTRIBUTES (COUNT=totlin) 
   BEFORE DISPLAY
    EXIT DISPLAY
  END DISPLAY 

  -- Desplegando totales
  CALL facing001_totdet(operacion) 
END FUNCTION 

-- Subrutina para ver los productos completos del documento

FUNCTION facing001_verproductos()
 -- Desplegando productos
 DISPLAY ARRAY v_products TO s_products.*
  ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE)

  ON ACTION cancel 
   -- Salida
   EXIT DISPLAY 

  BEFORE ROW 
   -- Verificando control del total de lineas
   IF (ARR_CURR()>totlin) THEN
      CALL FGL_SET_ARR_CURR(1)
   END IF
 END DISPLAY 
END FUNCTION 

-- Subrutina para ingresar el motivo de una anulacion de un documento

FUNCTION facqbx001_AnularDocumento()
 DEFINE loop,regreso SMALLINT,
        flag         SMALLINT, 
        vfac_id      INTEGER,
        msg          STRING,
        cmd          STRING 

   DEFINE reg_e RECORD
      estatus LIKE facturafel_e.estatus,
      serie_e LIKE facturafel_e.serie_e,
      numdoc_e LIKE facturafel_e.numdoc_e,
      autorizacion LIKE facturafel_e.autorizacion
   END RECORD
        
 -- Ingresando motivo 
 LET loop    = TRUE
 LET regreso = FALSE
 WHILE loop
  INPUT BY NAME w_mae_tra.motanl  
   ATTRIBUTES (CANCEL=FALSE,ACCEPT=FALSE,UNBUFFERED) 
   ON ACTION cancel
    -- Salida
    LET regreso = TRUE
    LET loop    = FALSE
    LET w_mae_tra.motanl = NULL
    CLEAR motanl 
    EXIT INPUT 
   AFTER FIELD motanl
    -- Verificando motivo
    IF (LENGTH(w_mae_tra.motanl)<=0) THEN
       ERROR "Error: debe ingresarse el motivo, VERIFICA ..."
       NEXT FIELD motanl
    END IF 
  END INPUT 
  IF regreso THEN     
     EXIT WHILE
  END IF 

  -- Verificando operacion
  LET msg = "Desea Anular el Documento"
  IF NOT librut001_yesornot("Confirmacion",
                            msg,
                            "Si",
                            "No",
                            "question") THEN

     LET w_mae_tra.motanl = NULL
     CLEAR motanl 
     LET regreso = TRUE 
     EXIT WHILE
  END IF 

  -- Anulando 
  LET loop = FALSE 
  ERROR " Anulando Documento ..."  ATTRIBUTE(CYAN)

  -- Iniciando Transaccion
  BEGIN WORK
   LET regreso = FALSE 

   -- Marcando maestro del documento como anulado 
   SET LOCK MODE TO WAIT 
   DECLARE c_anul CURSOR FOR
   SELECT a.*
    FROM  fac_mtransac a
    WHERE (a.lnktra = w_mae_tra.lnktra) 
    FOR UPDATE 
    FOREACH c_anul 
     UPDATE fac_mtransac
     SET    fac_mtransac.estado = "A",
            fac_mtransac.motanl = w_mae_tra.motanl, 
            fac_mtransac.usranl = USER,
            fac_mtransac.fecanl = CURRENT,
            fac_mtransac.horanl = CURRENT
     WHERE CURRENT OF c_anul

     ###############################################################
     #Registro en tablas de FEL
      CALL registra_tablas_FEL(2) RETURNING flag  --Anula factura electronica
      IF flag = TRUE THEN
         ROLLBACK WORK 
         EXIT WHILE
      END IF
     ############################################################### 

     
    END FOREACH

    	IF SQLCA.SQLCODE = 0 THEN
         -- Finalizando Transaccion
   		COMMIT WORK
			CALL box_valdato("El documento se anulo exitosamente.")
         
         --Envia factura a firma
         LET vfac_id = NULL;
      
         SELECT a.fac_id
         INTO vfac_id
         FROM facturafel_e a
         WHERE a.num_int = w_mae_tra.lnktra
         AND a.tipod = "FC"
         AND a.estado_doc = "ANULADO"
      
         LET cmd = "fglrun ande_fel_g4s.42r ",vfac_id USING "<<<<<<<"," storepos 0 0"
         DISPLAY "cmd: ",cmd
         --IF 
         RUN cmd

         SELECT a.estatus, a.serie_e, a.numdoc_e, a.autorizacion
            INTO reg_e.estatus, reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion
            FROM facturafel_e a
            WHERE a.num_int = w_mae_tra.lnktra
            AND a.tipod = "FC"
            AND a.estado_doc = "ANULADO"
         DISPLAY BY NAME reg_e.serie_e, reg_e.numdoc_e, reg_e.autorizacion
         IF reg_e.estatus = "P" THEN
         -- Desplegando mensaje
            CALL fgl_winmessage(" Atencion","El documento no pudo ser firmado electronicamente.","stop")
         ELSE
            CALL box_valdato("El documento FEL fue anulado exitosamente")
         END IF
	ELSE
		ROLLBACK WORK
		CALL box_valdato("No se pudo anular el documento.")
	END IF 
    
    CLOSE c_anul
    FREE  c_anul

   -- Finalizando Transaccion
   --CA-An COMMIT WORK

   --CS-An ERROR ""
   
   -- Desplegando mensaje
   --CS-An LET msg = "Documento fue anulado."
   --CS-An CALL fgl_winmessage(" Atencion",msg,"information")
 END WHILE 

 RETURN regreso 
END FUNCTION 

-- Subrutina para modificar datos de la factura
-- Fecha de emision, fecha de corte, efectivo, tarjeta 

FUNCTION facqbx001_modificar()
 DEFINE loop,regreso SMALLINT,
        msg          STRING 

 -- Ingresando datos
 LET loop = TRUE
 WHILE loop
  LET regreso = FALSE
  INPUT BY NAME w_mae_tra.efecti,
                w_mae_tra.tarcre
   WITHOUT DEFAULTS 
   ATTRIBUTES (UNBUFFERED) 
   ON ACTION cancel
    -- Salida
    LET regreso = TRUE
    LET loop    = FALSE
    EXIT INPUT 

   AFTER FIELD efecti
    -- Verificando efectivo
    IF w_mae_tra.efecti IS NULL OR
       (w_mae_tra.efecti <=0) THEN
       LET w_mae_tra.efecti = 0
       DISPLAY BY NAME w_mae_tra.efecti
    END IF

    -- Totalizando desgloce
    CALL facing001_totpago()

   AFTER FIELD tarcre
    -- Verificando tarjeta
    IF w_mae_tra.tarcre IS NULL OR
       (w_mae_tra.tarcre <=0) THEN
       LET w_mae_tra.tarcre = 0
       DISPLAY BY NAME w_mae_tra.tarcre
    END IF

    -- Totalizando desgloce
    CALL facing001_totpago()
  END INPUT 
  IF regreso THEN     
     EXIT WHILE
  END IF 

  -- Verificanod totales del detalle del producto versus total desgloce
  IF (w_mae_tra.totpag!=w_mae_tra.totdoc) THEN
     LET msg = " Total Recibido diferente al Total a Pagar. "||
               "\n VERIFICA que los totales sean iguales."

     CALL fgl_winmessage(
     " Atencion",msg,"stop")
     LET loop = TRUE
     CONTINUE WHILE
  END IF

  -- Verificando anulacion
  IF NOT librut001_yesornot("Confirmacion",
                            "Desea Actualizar el Documento ?",
                            "Si",
                            "No",
                            "question") THEN
     LET regreso = TRUE 
     EXIT WHILE
  END IF 

  -- Aactualizando documento 
  LET loop = FALSE 
  ERROR " Actualizando Documento ..." ATTRIBUTE(CYAN)

  -- Iniciando Transaccion
  BEGIN WORK

   -- Actualizando
   SET LOCK MODE TO WAIT 
   DECLARE c_modi CURSOR FOR
   SELECT a.*
    FROM  fac_mtransac a
    WHERE (a.lnktra = w_mae_tra.lnktra) 
    FOR UPDATE 
    FOREACH c_modi 
     UPDATE fac_mtransac
     SET    fac_mtransac.fecemi = w_mae_tra.fecemi, 
            fac_mtransac.feccor = w_mae_tra.fecemi,
            fac_mtransac.efecti = w_mae_tra.efecti,
            fac_mtransac.tarcre = w_mae_tra.tarcre 
     WHERE CURRENT OF c_modi
    END FOREACH
    CLOSE c_modi
    FREE  c_modi

   -- Finalizando Transaccion
   COMMIT WORK

   ERROR ""
 END WHILE 

 RETURN regreso 
END FUNCTION 
