{ 
Mynor Ramirez
Egresos de caja
}

DATABASE storepos 

-- Definicion de variables globales 

GLOBALS
CONSTANT progname  = "facing004"
CONSTANT maximoval = 999999.99
DEFINE w_mae_pro   RECORD LIKE fac_dvicorte.*,
       w_mae_cor   RECORD LIKE fac_vicortes.*,
       xnumpos     LIKE fac_mtransac.numpos,
       xfeccor     DATE, 
       username    CHAR(15) 
END GLOBALS

-- Subrutina principal

MAIN
 DEFINE regreso SMALLINT,
        xfecchr CHAR(10) 

 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/ToolBarEgresosCaja")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("egresoscaja")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Asignando usuario
 LET username = FGL_GETENV("LOGNAME")
 LET xnumpos  = ARG_VAL(2) 
 LET xfecchr  = ARG_VAL(3) 
 LET xfeccor  = CURRENT 

 -- Buscando corte de caja
 IF NOT facing004_BCorteCaja() THEN
    DISPLAY "Atencion: no hay corte" 
    RETURN 
 END IF 

 -- Ingresando egresos
 CALL facing004_Egresos()
END MAIN

-- Subrutina para ingresar los egresos

FUNCTION facing004_Egresos()
 DEFINE wpais               VARCHAR(255), 
        loop,existe,opc     SMALLINT,
        retroceso           SMALLINT,
        msg                 CHAR(80),
        savedata            SMALLINT,
        titulo              STRING,
        conteo              INT 

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing004a AT 5,2
  WITH FORM "facing004a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais

  CALL librut001_header("facing004",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Verificando si opcion es nuevo ingreso
  LET retroceso = FALSE

  -- Inicio del loop
  LET loop = TRUE
  WHILE loop
   -- Verificando que no sea regreso
   IF NOT retroceso THEN
      -- Inicializando datos
      CALL facing004_inival(1)
   END IF

   -- Ingresando datos
   INPUT BY NAME w_mae_pro.totval,
                 w_mae_pro.observ
                 WITHOUT DEFAULTS 
                 ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE) 

    ON ACTION cancel    
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION accept 
     LET loop = FALSE
     EXIT INPUT 

   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Menu de opciones
   LET savedata = FALSE 
   LET opc = librut001_menugraba("Confirmacion",
                                 "Que desea hacer?",
                                 "Guardar",
                                 "Modificar",
                                 "Cancelar",
                                 "")
   CASE (opc)
    WHEN 0 -- Cancelando
     LET loop = FALSE
    WHEN 1 -- Grabando
     LET loop = FALSE

     -- Grabando 
     CALL facing004_grabar()
     LET loop     = FALSE
     LET savedata = TRUE 
    WHEN 2 -- Modificar 
     LET retroceso = TRUE
     LET loop      = TRUE 
     CONTINUE WHILE
   END CASE 
  END WHILE
END FUNCTION

-- Subrutina para verificar si um corte ya existe registrado

FUNCTION facing004_BCorteCaja()
 INITIALIZE w_mae_cor.* TO NULL 
 SELECT x.*
  INTO  w_mae_cor.* 
  FROM  fac_vicortes x
  WHERE (x.numpos = xnumpos) 
   AND  (x.feccor = xfeccor) 
  IF (status!=NOTFOUND) THEN 
     RETURN TRUE 
  ELSE 
     RETURN FALSE 
  END IF 
END FUNCTION 

-- Subrutina para grabar el egreso de caja

FUNCTION facing004_grabar()
 DEFINE msg CHAR(80)

 -- Grabando transaccion
 ERROR " Registrando egreso de corte de caja ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

   -- Grabando 
   -- Asignando datos
   LET w_mae_pro.lnkcor = w_mae_cor.lnkcor
   LET w_mae_pro.numdoc = w_mae_cor.lnkcor 
   LET w_mae_pro.userid = username 
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND 

   -- Obteniendo correlativo de egreso del corte
   SELECT NVL(COUNT(*),0)+1
    INTO  w_mae_pro.correl
    FROM  fac_dvicorte x
    WHERE x.lnkcor = w_mae_pro.lnkcor

   -- Grabando encabezado 
   SET LOCK MODE TO WAIT
   INSERT INTO fac_dvicorte   
   VALUES (w_mae_pro.*)

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Asignando el mensaje 
 LET msg = "Egreso de Caja del ",w_mae_pro.fecemi," registrado."
 CALL fgl_winmessage("Atencion",msg,"information")

 -- Inicializando datos
 CALL facing004_inival(1)
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION facing004_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   -- Inicializando datos 
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.lnkcor = 0
   LET w_mae_pro.numpos = xnumpos 
   LET w_mae_pro.fecemi = xfeccor 
   LET w_mae_pro.codaut = 1 
   LET w_mae_pro.correl = 0
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.fecemi 
END FUNCTION
