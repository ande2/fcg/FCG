{
Programo : Mynor Ramirez 
Objetivo : Impresion de corte de caja   
}

-- Definicion de variables globales  

GLOBALS "facglb002.4gl" 
DEFINE existe   SMALLINT, 
       filename CHAR(90)

-- Subrutina para imprimir el corte de caja   

FUNCTION facrpt002_GeneraCorte(reimpresion,pipeline)
 DEFINE reimpresion  SMALLINT,
        pipeline     CHAR(90)

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/CortePOS.spl"

 -- Iniciando reporte
 START REPORT facrpt002_ImprimirCorte TO filename 
  OUTPUT TO REPORT facrpt002_ImprimirCorte(1,reimpresion)
 FINISH REPORT facrpt002_ImprimirCorte

 -- Imprimiendo el reporte
 CALL librut001_sendreport(filename,pipeline,"Corte de Caja","") 
END FUNCTION 

-- Subrutinar para generar la impresion del corte 

REPORT facrpt002_ImprimirCorte(numero,reimpresion) 
 DEFINE xnserie     LIKE fac_mtransac.nserie, 
        xnumdoc     LIKE fac_mtransac.numdoc, 
        xtotdoc     LIKE fac_mtransac.totdoc, 
        xuserid     LIKE fac_mtransac.userid, 
        imp1        RECORD 
         lnktdc     LIKE fac_mtransac.lnktdc,
         nomdoc     CHAR(40), 
         codcat     LIKE inv_products.codcat,
         nomcat     CHAR(40), 
         cditem     LIKE inv_products.cditem,
         codabr     LIKE inv_products.codabr,
         dsitem     CHAR(30),
         cantid     DEC(12,2),
         totpro     DEC(14,2),
         prepro     DEC(7,2)   
        END RECORD,
        xtotdep     DEC(12,2), 
        xnomcaj     CHAR(40), 
        xnompos     CHAR(40), 
        numero      INTEGER, 
        i,col,coi   SMALLINT, 
        lg          SMALLINT, 
        reimpresion SMALLINT,
        xcorini     INTEGER,
        xcorfin     INTEGER, 
        ant         INTEGER, 
        cortepapel  CHAR(10), 
        totcan      DEC(14,2), 
        texto       STRING,
        xtipcor     STRING 

  OUTPUT LEFT   MARGIN 0 
         PAGE   LENGTH 5 
         TOP    MARGIN 0 
         BOTTOM MARGIN 0

 FORMAT 
  BEFORE GROUP OF numero 
   -- Imprimiendo encabezado
   -- Inicializando impresora
   LET coi = 1
   LET lg  = 42+coi 
   LET cortepapel = ASCII 29,ASCII 86,"0"
   LET xtipcor    = "TIPO DE CORTE: FINAL" 
   LET xuserid    = w_mae_pro.usrcie 

   -- Obteniendo nombre del punto de venta
   INITIALIZE xnompos TO NULL 
   SELECT a.nompos 
    INTO  xnompos 
    FROM  fac_puntovta a 
    WHERE a.numpos = w_mae_pro.numpos 

   -- Obteniendo nombre del cajero
   INITIALIZE xnomcaj TO NULL 
   SELECT a.nomusr 
    INTO  xnomcaj 
    FROM  glb_usuarios a
    WHERE a.userid = xuserid 

   -- Obteniendo correlativo inicial y final de facturas 
   SELECT NVL(MIN(x.numdoc),0),NVL(MAX(x.numdoc),0)
    INTO  xcorini,xcorfin 
    FROM  vis_estadiscorte x
    WHERE x.numpos = w_mae_pro.numpos
      AND x.feccor = w_mae_pro.feccor

   -- Imprimiendo Encabezado
   -- Titulo de reporte 
   LET texto = "CORTE DE CAJA" 
   LET col = librut001_centrado(texto,lg) 
   PRINT COLUMN col,texto CLIPPED 

   -- Punto de venta    
   LET texto = xnompos 
   LET col = librut001_centrado(texto,lg) 
   PRINT COLUMN col,texto CLIPPED 

   -- Fecha de corte    
   LET texto = "FECHA DE CORTE ",w_mae_pro.feccor 
   LET col = librut001_centrado(texto,lg) 
   PRINT COLUMN col,texto CLIPPED 
   
   -- Tipo de corte    
   LET col = librut001_centrado(xtipcor,lg) 
   PRINT COLUMN col,xtipcor CLIPPED 
   SKIP 1 LINES 
   PRINT COLUMN coi,"Cajero: ",xnomcaj CLIPPED
   PRINT COLUMN coi,"Fecha:  ",w_mae_pro.feccie --," ",w_mae_pro.horcie 

   SKIP 1 LINES
   PRINT COLUMN coi,"========= INTEGRACION DE CORTE ========="
   PRINT COLUMN coi,"Fondo Inicial         (+): L. ",w_mae_pro.fonini USING "###,##&.&&"
   PRINT COLUMN coi,"Total Ventas          (+): L. ",w_mae_pro.totvta USING "###,##&.&&"
   PRINT COLUMN coi,"Total Egresos Caja    (-): L. ",w_mae_pro.totgas USING "###,##&.&&"
   PRINT COLUMN coi,"________________________________________"
   PRINT COLUMN coi,"Total Corte           (=): L. ",w_mae_pro.totcor USING "###,##&.&&"
   PRINT COLUMN coi,"========================================"
   SKIP 1 LINES
   PRINT COLUMN coi,"======== INTEGRACION DE EFECTIVO ======="
   PRINT COLUMN coi,"Total Efectivo Corte  (+): L. ",w_mae_pro.toecor USING "###,##&.&&"
   PRINT COLUMN coi,"Total Efectivo Cierre (-): L. ",w_mae_pro.toecie USING "###,##&.&&"
   PRINT COLUMN coi,"________________________________________"

   IF (w_mae_pro.totdif>0) THEN
    PRINT COLUMN coi,"Faltante Efectivo        : L. ",w_mae_pro.totdif USING "###,##&.&&"
   ELSE
    IF (w_mae_pro.totdif<0) THEN
     PRINT COLUMN coi,"Sobrante Efectivo        : L. ",w_mae_pro.totdif USING "###,##&.&&"
    ELSE
     PRINT COLUMN coi,"Sin Diferencias Efectivo : L. ",w_mae_pro.totdif USING "###,##&.&&"
    END IF
   END IF
   PRINT COLUMN coi,"========================================"

   -- Calculando deposito
   LET xtotdep = 0
   IF (w_mae_pro.toecie>0) THEN
    IF (w_mae_pro.toecie>w_mae_pro.fonini) THEN
      LET xtotdep = w_mae_pro.toecie-w_mae_pro.fonini
    END IF
   END IF

   PRINT COLUMN coi,"Total a Depositar ==>      L. ",xtotdep USING "###,##&.&&"
   PRINT COLUMN coi,"========================================"
   SKIP 1 LINES
   PRINT COLUMN coi,"==== INTEGRACION TARJETA DE CREDITO ===="
   PRINT COLUMN coi,"Total Tajeta Corte       : L. ",w_mae_pro.tottar USING "###,##&.&&"
   PRINT COLUMN coi,"Total Tarjeta POS        : L. ",w_mae_pro.ttacie USING "###,##&.&&"
   PRINT COLUMN coi,"________________________________________"

   IF (w_mae_pro.todita>0) THEN
    PRINT COLUMN coi,"Faltante Tarjeta         : L. ",w_mae_pro.todita USING "###,##&.&&"
   ELSE
    IF (w_mae_pro.todita<0) THEN
     PRINT COLUMN coi,"Sobrante Tarjeta         : L. ",w_mae_pro.todita USING "###,##&.&&"
    ELSE
     PRINT COLUMN coi,"Sin Diferencias Tarjeta  : L. ",w_mae_pro.todita USING "###,##&.&&"
    END IF
   END IF
   PRINT COLUMN coi,"========================================"
   SKIP 1 LINES

   PRINT COLUMN coi,"Estadisticas"
   PRINT COLUMN coi,"________________________________________"
   PRINT COLUMN coi,"Total Venta Efectivo     : L. ",w_mae_pro.totefe USING "###,##&.&&"
   PRINT COLUMN coi,"Total Venta Tarjeta      : L. ",w_mae_pro.tottar USING "###,##&.&&"
   SKIP 1 LINES
   PRINT COLUMN coi,"Total Venta Contado      : L. ",w_mae_pro.totcon USING "###,##&.&&"
   PRINT COLUMN coi,"Total Venta Credito      : L. ",w_mae_pro.totcre USING "###,##&.&&"
   SKIP 1 LINES
   PRINT COLUMN coi,"Total Impuesto Venta     : L. ",w_mae_pro.totisv USING "###,##&.&&"
   PRINT COLUMN coi,"Total Descuento Venta    : L. ",w_mae_pro.totdes USING "###,##&.&&"
   SKIP 1 LINES
   PRINT COLUMN coi,"Numero Documento Inicial : ",xcorini USING "&&&&&&&&"
   PRINT COLUMN coi,"Numero Documento Final   : ",xcorfin USING "&&&&&&&&"
   PRINT COLUMN coi,"----------------------------------------"

   -- Imprimiendo egresos 
   SKIP 1 LINES 
   PRINT COLUMN coi,"Detalle de Egresos"
   PRINT COLUMN coi,"________________________________________"
 
   FOR i = 1 TO totdoc
    IF v_doctos[i].numdoc IS NULL THEN
       CONTINUE FOR
    END IF

    PRINT COLUMN coi,v_doctos[i].fecemi                  ,1 SPACES,
                     v_doctos[i].numdoc[1,18]            ,1 SPACES,
                     v_doctos[i].totval USING "###,##&.&&" 
    PRINT COLUMN coi,v_doctos[i].observ[1,38]           
   END FOR 

   IF (totdoc=0) THEN
      PRINT COLUMN coi,"No se registraron egresos." 
   END IF 

   PRINT COLUMN coi,"________________________________________"
   PRINT COLUMN coi,"Total Egresos -->          L. ",
                    w_mae_pro.totgas USING "###,##&.&&" 
 
   -- Imprimmiendo estadistica de ventas 
   SKIP 1 LINES 
   PRINT COLUMN coi,"Estadistica de Productos Vendidos" 
   PRINT COLUMN coi,"________________________________________"
   PRINT COLUMN coi,"Producto                        Cantidad"
   SKIP 1 LINES 

   DECLARE cf CURSOR FOR
   SELECT x.lnktdc,x.nomdoc,x.codcat,x.nomcat,x.cditem,x.codabr,x.dsitem,
          SUM(x.cantid),SUM(x.totpro),(SUM(x.totpro)/SUM(x.cantid))
    FROM  vis_estadiscorte x,inv_products y
    WHERE x.numpos = w_mae_pro.numpos
      AND x.feccor = w_mae_pro.feccor 
      AND y.codabr = x.codabr
      AND y.escomb = 1 
    GROUP BY 1,2,3,4,5,6,7
    ORDER BY 3,9 DESC 

    LET totcan = 0 
    FOREACH cf INTO imp1.* 
     PRINT COLUMN coi,imp1.dsitem                     ,2 SPACES, 
                      imp1.cantid USING "####&.&&"
     LET totcan = totcan+imp1.cantid
    END FOREACH
    CLOSE cf
    FREE  cf 

   PRINT COLUMN coi,"________________________________________"
   PRINT COLUMN coi,32 SPACES,totcan USING "####&.&&"

   -- Imprimiendo firma del cajero 
   SKIP 10 LINES 
   PRINT COLUMN coi,"________________________________________"
   PRINT COLUMN coi,"Firma del Cajero" 

   -- Imprimiendo pie 
   SKIP 1 LINES
   FOR i = 1 TO 8 
    PRINT " "
   END FOR 
   PRINT cortepapel CLIPPED 
END REPORT 
