{ 
glbglo035.4gl
Mynor Ramirez
Mantenimiento de marcas 
Octubre 2011 
-- invmae0012a.per 
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae035"
DEFINE w_mae_pro   RECORD LIKE inv_marcapro.*,
       v_marcas    DYNAMIC ARRAY OF RECORD
        tcodmar    LIKE inv_marcapro.codmar,
        tnommar    LIKE inv_marcapro.nommar 
       END RECORD,
       username    VARCHAR(15)
END GLOBALS
