{ 
Programo : invrep007.4gl 
Objetivo : Reporte de toma de inventario inicial
}

DATABASE storepos 

{ Definicion de variables globales }

DEFINE w_datos   RECORD
        codcat   LIKE glb_categors.codcat,
        subcat   LIKE glb_subcateg.subcat,
        ordrep   SMALLINT 
       END RECORD,
       existe    SMALLINT,
       pagina    SMALLINT,
       filename  CHAR(100),
       fcodcat   STRING, 
       fsubcat   STRING 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar7")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rtomainicial")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep007_tomainicial()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep007_tomainicial()
 DEFINE w_mae_pro         RECORD LIKE inv_products.*,
        imp1              RECORD
         codcat           LIKE inv_products.codcat,
         nomcat           LIKE glb_categors.nomcat,
         subcat           LIKE inv_products.subcat,
         nomsub           LIKE glb_subcateg.nomsub, 
         cditem           LIKE inv_proenbod.cditem, 
         codabr           CHAR(20),
         despro           CHAR(70),
         nommed           CHAR(15)
        END RECORD,
        wpais             VARCHAR(255),
        pipeline          VARCHAR(100), 
        qrytxt            STRING,
        strcodcat         STRING,
        strsubcat         STRING,
        strcditem         STRING,
        strordenr         STRING,
        loop              SMALLINT

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep007a AT 5,2
  WITH FORM "invrep007a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invrep007",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/TomaInicial.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   CLEAR FORM
   LET w_datos.ordrep = 1 
   LET pagina = FALSE 

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codcat,
                 w_datos.subcat,
                 w_datos.ordrep 
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     EXIT INPUT 

    ON CHANGE codcat
     -- Limpiando combos
     LET w_datos.subcat = NULL
     CLEAR subcat

     -- Llenando combox de subcategorias
     IF w_datos.codcat IS NOT NULL THEN 
        CALL librut003_cbxsubcategorias(w_datos.codcat)
     END IF 

    AFTER INPUT
     -- Verificando datos
     IF pipeline IS NULL THEN
        NEXT FIELD codcat
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Verificando seleccion de categoria
   LET strcodcat = NULL
   IF w_datos.codcat IS NOT NULL THEN
      LET strcodcat = "AND d.codcat = ",w_datos.codcat
   END IF

   -- Verificando condicion de subcategoria
   LET strsubcat = NULL
   IF w_datos.subcat IS NOT NULL THEN
      LET strsubcat = "AND e.subcat = ",w_datos.subcat
   END IF

   -- Verificando orden del reporte
   LET strordenr = NULL
   CASE (w_datos.ordrep)
    WHEN 1 -- Alfabetico
     LET strordenr = " ORDER BY 1,2,3,4,7"
    WHEN 2 -- Codigo
     LET strordenr = " ORDER BY 1,2,3,4,6"
   END CASE

   -- Construyendo seleccion 
   LET qrytxt = 
     "SELECT b.codcat,d.nomcat,b.subcat,e.nomsub,b.cditem,",
            "b.codabr,b.despro,c.nommed ", 
      "FROM inv_products b,inv_unimedid c,glb_categors d,glb_subcateg e ",
      "WHERE b.estado = 1 ",
       " AND c.unimed = b.unimed ",
       " AND d.codcat = b.codcat ",
       " AND e.codcat = b.codcat ",
       " AND e.subcat = b.subcat ",
       strcodcat CLIPPED," ",
       strsubcat CLIPPED," ",
       strordenr CLIPPED

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep007 FROM qrytxt 
   DECLARE c_crep007 CURSOR FOR c_rep007
   LET existe = FALSE
   FOREACH c_crep007 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE
       START REPORT invrep007_GeneraReporte TO filename
    END IF 

    -- Llenando reporte
    OUTPUT TO REPORT invrep007_GeneraReporte(imp1.*)
   END FOREACH
   CLOSE c_crep007 
   FREE  c_crep007 

   IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT invrep007_GeneraReporte 

    -- Enviando reporte al destino seleccionado
    CALL librut001_sendreport
     (filename,pipeline,"Toma de Inventario Fisico",
      "--noline-numbers "||
      "--nofooter "||
      "--font-size 8 "||
      "--page-width 842 --page-height 595 "||
      "--left-margin 55 --right-margin 15 "||
      "--top-margin 35 --bottom-margin 25 "||
      "--icon logo_reportes_1.JPG "||
      "--icon-scale 0.15 "||
      "--title Inventarios")

    ERROR "" 
    CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
    ERROR "" 
    CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop")
   END IF 
  END WHILE
 CLOSE WINDOW wrep007a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT invrep007_GeneraReporte(imp1)
 DEFINE imp1              RECORD
         codcat           LIKE inv_products.codcat,
         nomcat           LIKE glb_categors.nomcat,
         subcat           LIKE inv_products.subcat,
         nomsub           LIKE glb_subcateg.nomsub,
         cditem           LIKE inv_proenbod.cditem, 
         codabr           CHAR(20),
         despro           CHAR(70),
         nommed           CHAR(15)
        END RECORD,
        linea             CHAR(154)

  OUTPUT PAGE   LENGTH 47 --50
         LEFT   MARGIN 0
         TOP    MARGIN 0
         BOTTOM MARGIN 0 

  FORMAT 
   PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________",
                "____"
   
    -- Imprimiendo Encabezado
    PRINT COLUMN   1,"Inventarios",
	  COLUMN 134,PAGENO USING "Pagina: <<<<"
    PRINT COLUMN   1,"Invrep007",
          COLUMN  51,"T O M A  D E  I N V E N T A R I O  I N I C I A L",
          COLUMN 134,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
    PRINT COLUMN   1,"(",FGL_GETENV("LOGNAME") CLIPPED,")",
          COLUMN 134,"Hora  : ",TIME 
    SKIP 1 LINES 
    PRINT COLUMN   1,"INVENTARIO REALIZADO EL ____________________" 
    PRINT linea 
    PRINT "Codigo                Descripcion del Producto                            ",
          "                    Unidad             Precio            E X I S T E N C I A"
    PRINT "Producto                                                                  ",
          "                    Medida             Unitario          FISICA INICIAL" 
    PRINT linea
    SKIP 1 LINES 

   BEFORE GROUP OF imp1.codcat
    -- Imprimiendo datos de la categoria
    IF NOT pagina THEN 
       LET pagina = TRUE 
    ELSE 
       SKIP TO TOP OF PAGE 
    END IF 

    -- Imprimiendo datos de la categoria 
    PRINT "CATEGORIA : ",imp1.codcat USING "<<<"," ",imp1.nomcat CLIPPED

   BEFORE GROUP OF imp1.subcat
    -- Imprimiendo datos de la sub-categoria
    SKIP 1 LINES
    PRINT "[ SUB-CATEGORIA : ",imp1.subcat USING "<<<"," ",imp1.nomsub CLIPPED," ]"
    SKIP 1 LINES 

   ON EVERY ROW
    -- Imprimiendo datos
    PRINT COLUMN   1,imp1.codabr                                 ,
          COLUMN  23,imp1.despro                                 ,2  SPACES,
                     imp1.nommed                                 ,22 SPACES,
                     "____________________"

    SKIP 1 LINES 

   AFTER GROUP OF imp1.codcat
    -- Imprimiendo datos de la categoria
    SKIP 1 LINES
    PRINT COLUMN   1,"TOTAL ",GROUP COUNT(*) USING "<<<,<<&",
                     " PRODUCTO[S] en CATEGORIA ",imp1.nomcat CLIPPED
END REPORT 
