{
glbqbe001.4gl 
MRS 
Mantenimiento de empresas 
}

{ Definicion de variables globales }

GLOBALS "glbglo001.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe001_empresas(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de paises
  CALL librut003_cbxpaises() 

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL glbqbe001_EstadoMenu(operacion," ")
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae001_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codemp,a.nomemp,a.nomabr,
                                a.numnit,a.numtel,a.diremp,
                                a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel
     -- Salida
     CALL glbmae001_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT
   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codemp,a.nomemp,a.numnit ",
                 " FROM glb_empresas a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_empresas SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_empresas.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_empresas INTO v_empresas[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_empresas
   FREE  c_empresas
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL glbqbe001_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_empresas TO s_empresas.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel
      -- Salida
      CALL glbmae001_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar 
      CALL glbmae001_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF glbmae001_empresas(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL glbqbe001_datos(v_empresas[ARR_CURR()].tcodemp)
      END IF 

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN 
       IF glbmae001_empresas(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL glbqbe001_datos(v_empresas[ARR_CURR()].tcodemp)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      LET res = glbqbe001_integridad()
      IF (res>0) THEN  
         LET msg = " Esta empresa ya tiene registros. \n Empresa no puede borrarse."
         CALL fgl_winmessage(
         " Atencion",msg,"stop")
         CONTINUE DISPLAY 
      END IF 

      -- Confirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar esta empresa ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL glbmae001_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Empresa"
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Nombre Anreviado" 
      LET arrcols[4] = "Numero de NIT" 
      LET arrcols[5] = "Numero Telefono" 
      LET arrcols[6] = "Direccion" 
      LET arrcols[7] = "Usuario Registro"
      LET arrcols[8] = "Fecha Registro"
      LET arrcols[9] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry         = "SELECT a.codemp,a.nomemp,a.nomabr,",
                              "\"'\"||a.numnit,a.numtel,",
                              "TRIM(a.diremp),a.userid,a.fecsis,a.horsis",
                        " FROM glb_empresas a ",
                        " WHERE ",qrytext CLIPPED,
                        " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res        = librut002_excelreport("Empresas",qry,9,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE 
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL glbqbe001_datos(v_empresas[ARR_CURR()].tcodemp)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen empresas con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Desplegando estado del menu
  CALL glbqbe001_EstadoMenu(0,"")
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION glbqbe001_datos(wcodemp)
 DEFINE wcodemp LIKE glb_empresas.codemp,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  glb_empresas a "||
              "WHERE a.codemp = "||wcodemp||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_empresast SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_empresast INTO w_mae_pro.*
 END FOREACH
 CLOSE c_empresast
 FREE  c_empresast

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomemp THRU w_mae_pro.diremp 
 DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para verificar si la empresa ya tiene registros 

FUNCTION glbqbe001_integridad()
 DEFINE conteo INTEGER 

 -- Verificando facturacion 
 SELECT COUNT(*)
  INTO  conteo
  FROM  pos_mtransac a
  WHERE (a.codemp = w_mae_pro.codemp)
  IF (conteo>0) THEN
     RETURN 1
  ELSE
     RETURN 0
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION glbqbe001_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Empresas - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Empresas - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Empresas - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Empresas - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Empresas - NUEVO")
 END CASE
END FUNCTION
