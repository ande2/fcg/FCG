{ 
glbglo001.4gl
MRS 
Mantenimiento de empresas
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae001"
DEFINE w_mae_pro   RECORD LIKE glb_empresas.*,
       v_empresas  DYNAMIC ARRAY OF RECORD
        tcodemp    LIKE glb_empresas.codemp,
        tnomemp    LIKE glb_empresas.nomemp, 
        tnumnit    LIKE glb_empresas.numnit 
       END RECORD,
       username    VARCHAR(15)
END GLOBALS
