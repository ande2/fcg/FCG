{ 
Fecha    : Enero 2011
Programo : invrep005.4gl 
Objetivo : Reporte comparativo de inventario fisico
}

DATABASE storepos 

{ Definicion de variables globales }

CONSTANT tipmovcmp = 100 

DEFINE w_mae_bod RECORD LIKE inv_mbodegas.*,
       w_mae_emp RECORD LIKE glb_empresas.*,
       w_mae_suc RECORD LIKE glb_sucsxemp.*,
       w_datos   RECORD
        codbod   LIKE inv_mtransac.codbod,
        codemp   LIKE inv_mtransac.codemp,
        codsuc   LIKE inv_mtransac.codbod,
        codcat   LIKE glb_categors.codcat,
        subcat   LIKE glb_subcateg.subcat,
        fecinv   DATE,
        ordrep   SMALLINT,
        estado   SMALLINT, 
        absorb   SMALLINT, 
        hayfis   SMALLINT 
       END RECORD,
       estadopro STRING, 
       absorbpro STRING, 
       existe    SMALLINT,
       filename  STRING,
       fcodcat   STRING,
       fsubcat   STRING,
       ffecinv   STRING, 
       totpro    INT 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar7")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rinvfisico")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep005_invfisico()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep005_invfisico()
 DEFINE w_pro_bod         RECORD LIKE inv_proenbod.*,
        w_mae_pro         RECORD LIKE inv_products.*,
        imp1              RECORD
         codemp           LIKE inv_proenbod.codemp, 
         codsuc           LIKE inv_proenbod.codsuc, 
         codbod           LIKE inv_proenbod.codbod,
         codcat           LIKE glb_categors.codcat,
         nomcat           LIKE glb_categors.nomcat,
         subcat           LIKE glb_subcateg.subcat,
         nomsub           LIKE glb_subcateg.nomsub,
         cditem           LIKE inv_proenbod.cditem, 
         codabr           CHAR(20),
         despro           CHAR(46),
         preuni           LIKE inv_products.pulcom, 
         nommed           CHAR(15),
         exican           LIKE inv_tofisico.canuni,
         exifis           LIKE inv_tofisico.canexi,
         prefis           DEC(14,2), 
         preexi           DEC(14,2),
         tomafi           CHAR(1) 
        END RECORD,
        upreuni           LIKE inv_products.pulcom, 
        xcanabp,xcanabn   DEC(12,2), 
        wpais             VARCHAR(255),
        xsalval           DEC(14,2), 
        pipeline,qrytxt   STRING,
        strcodcat         STRING,
        strsubcat         STRING,
        strcanfis         STRING,
        strordenr         STRING,
        strestado         STRING,
        strabsorb         STRING,
        loop              SMALLINT

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep005a AT 5,2
  WITH FORM "invrep005a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invrep005",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/ComparativoFisico.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de bodegas x usuario
  CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   CLEAR FORM
   LET w_datos.ordrep = 1
   LET w_datos.hayfis = 0 
   LET w_datos.estado = 2 
   LET w_datos.absorb = 1 

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codbod,
                 w_datos.codcat,
                 w_datos.subcat,
                 w_datos.fecinv,
                 w_datos.ordrep,
                 w_datos.estado, 
                 w_datos.absorb, 
                 w_datos.hayfis 
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     -- Verificando datos 
     IF w_datos.codbod IS NULL OR 
        w_datos.fecinv IS NULL THEN
        ERROR "Error: deben de completarse los filtros de seleccion." 
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     -- Verificando datos 
     IF w_datos.codbod IS NULL OR 
        w_datos.fecinv IS NULL THEN
        ERROR "Error: deben de completarse los filtros de seleccion." 
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON CHANGE codbod
     -- Obteniendo datos de la bodega
     CALL librut003_bbodega(w_datos.codbod)
     RETURNING w_mae_bod.*,existe

     -- Asignando datos de empresa y sucursal de la bodega
     LET w_datos.codemp = w_mae_bod.codemp
     LET w_datos.codsuc = w_mae_bod.codsuc

     -- Obteniendo datos de la empresa
     CALL librut003_bempresa(w_mae_bod.codemp)
     RETURNING w_mae_emp.*,existe
     -- Obteniendo datos de la sucursal
     CALL librut003_bsucursal(w_mae_bod.codsuc)
     RETURNING w_mae_suc.*,existe

     -- Desplegando datos de la empresa y sucursal
     DISPLAY BY NAME w_datos.codemp,w_datos.codsuc,w_mae_emp.nomemp,w_mae_suc.nomsuc

     -- Limpiando combos de categorias y subcategorias
     LET w_datos.codcat = NULL
     LET w_datos.subcat = NULL
     CLEAR codcat,subcat

    ON CHANGE codcat
     -- Limpiando combos
     LET w_datos.subcat = NULL
     CLEAR subcat

     -- Llenando combox de subcategorias
     IF w_datos.codcat IS NOT NULL THEN 
        CALL librut003_cbxsubcategorias(w_datos.codcat)
     END IF 

    AFTER FIELD codbod
     -- Verificando bodega 
     IF w_datos.codbod IS NULL THEN
        ERROR "Error: debe de seleccionarse la bodega a listar."
        NEXT FIELD codbod
     END IF

    BEFORE FIELD fecinv
     -- Cargando combo de fecha de inventario fisico
     CALL librut003_cbxfecfisico(w_datos.codemp,w_datos.codsuc,w_datos.codbod)

    AFTER FIELD fecinv 
     -- Verificando fecha de inventario
     IF w_datos.fecinv IS NULL THEN
        NEXT FIELD fecinv
     END IF

    AFTER INPUT 
     -- Verificando datos
     IF w_datos.codbod IS NULL OR 
        w_datos.fecinv IS NULL OR
        pipeline IS NULL THEN
        NEXT FIELD codbod
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Verificando seleccion de categoria
   LET strcodcat = NULL
   IF w_datos.codcat IS NOT NULL THEN
      LET strcodcat = "AND d.codcat = ",w_datos.codcat
   END IF

   -- Verificando condicion de subcategoria
   LET strsubcat = NULL
   IF w_datos.subcat IS NOT NULL THEN
      LET strsubcat = "AND e.subcat = ",w_datos.subcat
   END IF

   -- Verificando orden
   LET strordenr = NULL
   CASE (w_datos.ordrep)
    WHEN 1 LET strordenr = " ORDER BY 1,2,3,4,6,10"
    WHEN 2 LET strordenr = " ORDER BY 1,2,3,4,6,9"
   END CASE 

   -- Verificando condicion de estado
   LET strestado = NULL 
   LET estadopro = "Productos [Todos]" 
   CASE (w_datos.estado)
    WHEN 0 LET strestado = "AND b.estado = 0" 
           LET estadopro = "Productos [De Baja]" 
    WHEN 1 LET strestado = "AND b.estado = 1" 
           LET estadopro = "Productos [Activos]"  
   END CASE 

   -- Verificando si se incluye absorcion o no 
   CASE (w_datos.absorb)
    WHEN 0 LET absorbpro = "Absorciones [NO]" 
    WHEN 1 LET absorbpro = "Absorciones [SI]"  
   END CASE 

   -- Construyendo seleccion 
   LET qrytxt = 
    "SELECT x.codemp,x.codsuc,x.codbod,b.codcat,d.nomcat,b.subcat,",
           "e.nomsub,x.cditem,b.codabr,b.despro,b.pulcom,c.nommed,0,0,0,0 ",
      "FROM inv_proenbod x,inv_products b,inv_unimedid c,",
           "glb_categors d,glb_subcateg e ",
      "WHERE x.codemp = ",w_datos.codemp,
       " AND x.codsuc = ",w_datos.codsuc,
       " AND x.codbod = ",w_datos.codbod,
       " AND b.cditem = x.cditem ",
       strestado CLIPPED, 
       " AND c.unimed = b.unimed ",
       " AND d.codcat = b.codcat ",
       " AND e.codcat = b.codcat ",
       " AND e.subcat = b.subcat ",
       strcodcat CLIPPED," ",
       strsubcat CLIPPED," ",
       strordenr CLIPPED

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere."
   PREPARE c_rep005 FROM qrytxt 
   DECLARE c_crep005 CURSOR FOR c_rep005
   LET existe = FALSE
   LET totpro = 0 
   FOREACH c_crep005 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE
       START REPORT invrep005_impinvfis TO filename
    END IF 

    -- Obteniendo cantidades de fisico 
    LET imp1.tomafi = "" 
    SELECT NVL(y.canuni,0)
     INTO  imp1.exifis 
     FROM  inv_tofisico y 
     WHERE y.codemp = w_datos.codemp 
       AND y.codsuc = w_datos.codsuc 
       AND y.codbod = w_datos.codbod 
       AND y.aniotr = YEAR(w_datos.fecinv)
       AND y.mestra = MONTH(w_datos.fecinv) 
       AND y.fecinv = w_datos.fecinv
       AND y.cditem = imp1.cditem 
    IF (status=NOTFOUND) THEN
       LET imp1.tomafi = "**" 
    END IF 

    -- Calculando saldo anterior del producto
    CALL librut003_saldoantxpro(w_datos.codemp,
                                w_datos.codsuc,
                                w_datos.codbod,
                                imp1.cditem,
                                (w_datos.fecinv+1))
    RETURNING imp1.exican,xsalval 

    -- Excluyendo absorciones 
    IF w_datos.absorb=0 THEN
     -- Sumando absorciones positivas 
     SELECT NVL(SUM(a.canuni),0)
      INTO  xcanabp 
      FROM  inv_dtransac a
      WHERE a.codemp = w_datos.codemp
        AND a.codsuc = w_datos.codsuc
        AND a.codbod = w_datos.codbod
        AND a.tipmov IN (2) 
        AND a.cditem = imp1.cditem
        AND a.fecemi = w_datos.fecinv    
        AND a.estado = "V"
        AND a.actexi = 1
 
     -- Sumando absorciones negativas  
     SELECT NVL(SUM(a.canuni),0)
      INTO  xcanabn 
      FROM  inv_dtransac a
      WHERE a.codemp = w_datos.codemp
        AND a.codsuc = w_datos.codsuc
        AND a.codbod = w_datos.codbod
        AND a.tipmov IN (3) 
        AND a.cditem = imp1.cditem
        AND a.fecemi = w_datos.fecinv    
        AND a.estado = "V"
        AND a.actexi = 1

     -- Quitando absorciones
     LET imp1.exican = imp1.exican-xcanabp+xcanabn
    END IF 

    -- Verificando condicion de cantidad fisica
    CASE (w_datos.hayfis)
     WHEN 1 -- >0
      IF imp1.exifis = 0 THEN CONTINUE FOREACH END IF 
     WHEN 2 -- =0
      IF imp1.exifis > 0 THEN CONTINUE FOREACH END IF 
    END CASE 

    -- Obteniendo ultimo precio de compra
    SELECT NVL(MAX(a.prepro),0)
     INTO  upreuni 
     FROM  inv_dtransac a
     WHERE a.lnktra = (SELECT MAX(x.lnktra) 
                        FROM  inv_dtransac x
                        WHERE x.codemp = imp1.codemp
                          AND x.codsuc = imp1.codsuc
                          AND x.codbod = imp1.codbod
                          AND x.tipmov = tipmovcmp 
                          AND x.fecemi <= w_datos.fecinv 
                          AND x.cditem = imp1.cditem)
       AND a.codemp = imp1.codemp
       AND a.codsuc = imp1.codsuc
       AND a.codbod = imp1.codbod
       AND a.tipmov = tipmovcmp 
       AND a.fecemi <= w_datos.fecinv 
       AND a.cditem = imp1.cditem

    IF upreuni >0 THEN 
       LET imp1.preuni = upreuni 
    END IF

    -- Calculando valores
    LET totpro      = totpro+1 
    LET imp1.prefis = imp1.exifis*imp1.preuni 
    LET imp1.preexi = imp1.exican*imp1.preuni 

    -- Llenando el reporte
    OUTPUT TO REPORT invrep005_impinvfis(imp1.*)
   END FOREACH
   CLOSE c_crep005 
   FREE  c_crep005 

   IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT invrep005_impinvfis 

    IF (totpro>0) THEN 
     -- Enviando reporte al destino seleccionado
     CALL librut001_sendreport
     (filename,pipeline,"Comparativo de Inventario Fisico",
     "--noline-numbers "||
     "--nofooter "||
     "--font-size 7 "||
     "--page-width 842 --page-height 595 "||
     "--left-margin 55 --right-margin 25 "||
     "--top-margin 35 --bottom-margin 45 "||
     "--title Inventarios")

     ERROR "" 
     CALL fgl_winmessage(" Atencion","Reporte Emitido.","information") 
    ELSE
    ERROR "" 
    CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
    END IF 
   ELSE
    ERROR "" 
    CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE
 CLOSE WINDOW wrep005a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT invrep005_impinvfis(imp1)
 DEFINE imp1       RECORD
         codemp    LIKE inv_proenbod.codemp, 
         codsuc    LIKE inv_proenbod.codsuc, 
         codbod    LIKE inv_proenbod.codbod,
         codcat    LIKE glb_categors.codcat,
         nomcat    LIKE glb_categors.nomcat,
         subcat    LIKE glb_subcateg.subcat,
         nomsub    LIKE glb_subcateg.nomsub,
         cditem    LIKE inv_proenbod.cditem, 
         codabr    CHAR(20),
         despro    CHAR(46),
         preuni    LIKE inv_products.pulcom, 
         nommed    CHAR(4),
         exican    LIKE inv_proenbod.exican, 
         exifis    LIKE inv_proenbod.exican,
         prefis    DEC(14,2), 
         preexi    DEC(14,2),
         tomafi    CHAR(1) 
        END RECORD,
        faltantes  DEC(14,2),
        sobrantes  DEC(14,2), 
        linea      CHAR(158),
        exis       SMALLINT

  OUTPUT LEFT MARGIN 0
         PAGE LENGTH 55
         TOP MARGIN 0
         BOTTOM MARGIN 0

  FORMAT 
   PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________",
                "______________________________________"

    -- Imprimiendo Encabezado
    PRINT COLUMN   1,"Inventarios",
	  COLUMN 134,PAGENO USING "Pagina: <<<<"
    PRINT COLUMN   1,"Invrep005",
          COLUMN  60,"COMPARATIVO DE INVENTARIO FISICO",
          COLUMN 134,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
    PRINT COLUMN   1,FGL_GETENV("LOGNAME"), 
          COLUMN  60,"Fecha del Inventario ",w_datos.fecinv USING "dd/mmm/yyyy", 
          COLUMN 134,"Hora  : ",TIME 
    PRINT COLUMN   1,estadopro CLIPPED," - ",absorbpro CLIPPED 
    PRINT linea 
    PRINT 
     "Codigo                Descripcion del Producto                        Uni.   ",
     "Preco      --------- U N I D A D E S ---------- ------- V A L O R E S ----------"
    PRINT 
     "Producto                                                              Med.   ",
     "Unitario     Fisica   Inventario  Diferencia      Fisica  Inventario  Diferencia"
    PRINT linea  

   BEFORE GROUP OF imp1.codbod 
    -- Imprimiendo datos de la bodega
    PRINT "EMPRESA  ",imp1.codemp USING "<<<"," ",w_mae_emp.nomemp CLIPPED
    PRINT "SUCURSAL ",imp1.codsuc USING "<<<"," ",w_mae_suc.nomsuc CLIPPED
    PRINT "BODEGA   ",imp1.codbod USING "<<<"," ",w_mae_bod.nombod CLIPPED
    SKIP 1 LINES
   
    -- Inicializando
    LET faltantes = 0
    LET sobrantes = 0 

   BEFORE GROUP OF imp1.codcat
    -- Imprimiendo datos de la categoria
    SKIP 1 LINES
    PRINT "[ CATEGORIA : ",imp1.codcat USING "<<<"," ",imp1.nomcat CLIPPED," ]"

   BEFORE GROUP OF imp1.subcat
    -- Imprimiendo datos de la sub-categoria
    SKIP 1 LINES
    PRINT "* SUB-CATEGORIA : ",imp1.subcat USING "<<<"," ",imp1.nomsub CLIPPED," *"
 
   ON EVERY ROW
    -- Imprimiendo producto
    PRINT COLUMN   1,imp1.codabr                                    ,2 SPACES,
                     imp1.despro                                    ,2 SPACES,
                     imp1.nommed                                    ,1 SPACES,
                     imp1.preuni               USING "---,--&.&&"   ,1 SPACES,
                     imp1.exifis               USING "---,--&.&&"   ,2 SPACES,
                     imp1.exican               USING "---,--&.&&"   ,2 SPACES,
                    (imp1.exifis-
                     imp1.exican)              USING "----,--&.&&"  ,1 SPACES,
                     imp1.prefis               USING "----,--&.&&"  ,1 SPACES,
                     imp1.preexi               USING "----,--&.&&"  ,1 SPACES,
                    (imp1.prefis- 
                     imp1.preexi)              USING "----,--&.&&"  ,1 SPACES,
                     imp1.tomafi                      

    -- Totalizando sobrantes y faltantes
    IF (imp1.prefis-imp1.preexi)>0 THEN
       LET sobrantes = (sobrantes+(imp1.prefis-imp1.preexi)) 
    ELSE
       LET faltantes = (faltantes+(imp1.prefis-imp1.preexi)) 
    END IF 

   AFTER GROUP OF imp1.subcat
    -- Imprimiendo datos de la sub-categoria
    PRINT COLUMN   1,"TOTAL ",GROUP COUNT(*) USING "<<<,<<&",
                     " PRODUCTO[S] EN SUB-CATEGORIA ",imp1.nomsub CLIPPED,
          COLUMN 123,GROUP SUM(imp1.prefis)  USING "----,--&.&&"  ,1 SPACES,
                     GROUP SUM(imp1.preexi)  USING "----,--&.&&"  ,1 SPACES, 
                    (GROUP SUM(imp1.prefis)- 
                     GROUP SUM(imp1.preexi)) USING "----,--&.&&"  

   AFTER GROUP OF imp1.codcat
    -- Imprimiendo datos de la categoria
    SKIP 1 LINES
    PRINT COLUMN   1,"TOTAL ",GROUP COUNT(*) USING "<<<,<<&",
                     " PRODUCTO[S] en CATEGORIA ",imp1.nomcat CLIPPED, 
          COLUMN 123,GROUP SUM(imp1.prefis)  USING "----,--&.&&"  ,1 SPACES,
                     GROUP SUM(imp1.preexi)  USING "----,--&.&&"  ,1 SPACES, 
                    (GROUP SUM(imp1.prefis)- 
                     GROUP SUM(imp1.preexi)) USING "----,--&.&&"  

   AFTER GROUP OF imp1.codbod
    -- Totalizando por bodega
    SKIP 1 LINES
    PRINT COLUMN   1,"Total "  ,GROUP COUNT(*) USING "<<<,<<&"," Productos en Bodega",
          COLUMN 123,GROUP SUM(imp1.prefis)    USING "----,--&.&&"  ,1 SPACES,
                     GROUP SUM(imp1.preexi)    USING "----,--&.&&"  ,1 SPACES, 
                    (GROUP SUM(imp1.prefis)- 
                     GROUP SUM(imp1.preexi))   USING "----,--&.&&"  
    SKIP 1 LINES 
    PRINT "Total Valor Fisico     : ",GROUP SUM(imp1.prefis) USING "---,---,--&.&&"    
    PRINT "Total Valor Inventario : ",GROUP SUM(imp1.preexi) USING "---,---,--&.&&"
    PRINT "Total Valor Diferencia : ",GROUP SUM(imp1.prefis)- 
                                      GROUP SUM(imp1.preexi) USING "---,---,--&.&&"   
    SKIP 1 LINES 
    PRINT "Total Valor Sobrantes  : ",sobrantes              USING "---,---,--&.&&"   
    PRINT "Total Valor Faltantes  : ",faltantes              USING "---,---,--&.&&"   
    SKIP 1 LINES 

   ON LAST ROW
    -- Imprimiendo filtros
    PRINT "FILTROS"
    IF (LENGTH(fcodcat)>0) THEN
       PRINT "Categoria    : ",fcodcat
    END IF
    IF (LENGTH(fsubcat)>0) THEN
       PRINT "Subcategoria : ",fsubcat
    END IF
    CASE (w_datos.hayfis)
     WHEN 1 PRINT "Productoc con Fisico >0" 
     WHEN 2 PRINT "Productoc con Fisico =0" 
    END CASE 
END REPORT 
