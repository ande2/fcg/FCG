{
mainmenu.4gl 
Programo: Mynor Ramirez
Objetivo: Menu Principal del SISTEMA DE BODEGA
} 

DATABASE storepos 

DEFINE v_programas DYNAMIC ARRAY OF RECORD
        codpro     LIKE glb_programs.codpro, 
        actpro     LIKE glb_programs.actpro, 
        ordpro     LIKE glb_programs.ordpro  
       END RECORD,
       w_mae_usr   RECORD LIKE glb_usuarios.*, 
       totpro      INT 

-- Subrutina principal 

MAIN
 DEFINE aui        om.DomNode 
 DEFINE wpais      VARCHAR(255) 
 DEFINE existe,i   SMALLINT 
 DEFINE cmdstr     STRING 
 DEFINE username   STRING 
 DEFINE sistema    STRING
 DEFINE empresa    STRING
 DEFINE titulo     STRING 
 DEFINE version    STRING 

 OPTIONS ON CLOSE APPLICATION STOP

 -- Obteniendo nombre del sistema
 CALL librut003_parametros(0,1)
 RETURNING existe,sistema

 -- Obteniendo version del sistema
 CALL librut003_parametros(0,2)
 RETURNING existe,version

 -- Obteniendo nombre de la empresa
 CALL librut003_parametros(1,2)
 RETURNING existe,empresa

 -- Obteniendo nombre del usuario
 LET username = FGL_GETENV("LOGNAME")  

 -- Definiendo titulo del sistema
 LET titulo = sistema CLIPPED||" - Version "||version CLIPPED||" - "||empresa CLIPPED||" ( ",username CLIPPED||" )"

 -- Cargando estilos del menu
 CALL ui.Interface.setName("mainmenu")
 CALL ui.Interface.setType("container")
 CALL ui.Interface.setText(titulo)
 CALL ui.Interface.setImage("") 
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")

 -- Seleccionando datos de los usuarios
 INITIALIZE w_mae_usr.* TO NULL
 CALL librut003_BUsuario(username) 
 RETURNING w_mae_usr.*,existe

 -- Verificando si tipo de usuario es adminisrativo o mesero
 IF w_mae_usr.tipusu=1 THEN 
  -- Abriendo forma del menu
  OPEN FORM wmenu FROM "mainmenu" 
  DISPLAY FORM wmenu 

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("mainmenu",wpais,1)
  CALL librut001_dpelement("empresa",empresa CLIPPED) 

  -- Menu 
  MENU "Menu Principal"
   BEFORE MENU  
    -- Cargando programas del menu 
    CALL  mainmenu_programas()

    -- Verificando opciones
    FOR i = 1 TO totpro
      -- Deshabilitando opcion
     CALL Dialog.SetActionActive(v_programas[i].actpro CLIPPED,0) 

     -- Verificando si opcion tiene acceso
     IF seclib001_AccesosMenu(v_programas[i].codpro,username) THEN 
        -- Habilitando opcion 
        CALL Dialog.SetActionActive(v_programas[i].actpro CLIPPED,1) 
     END IF  
    END FOR 
   
   ON ACTION perfiles 
    LET cmdstr = "fglrun perfiles.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION usuarios           
    LET cmdstr = "fglrun usuarios.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION parametros         
    LET cmdstr = "fglrun parametros.42r 1"
    RUN cmdstr WITHOUT WAITING 

   -- Generales

   ON ACTION empresas              
    LET cmdstr = "fglrun empresas.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION clientes    
    LET cmdstr = "fglrun clientes.42r 1"
    RUN cmdstr WITHOUT WAITING 

   -- Inventarios 

   ON ACTION proveedores 
    LET cmdstr = "fglrun proveedores.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION categorias 
    LET cmdstr = "fglrun categorias.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION subcategorias 
    LET cmdstr = "fglrun subcategorias.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION unidadesmedida 
    LET cmdstr = "fglrun unidadesmedida.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION empaques   
    LET cmdstr = "fglrun empaques.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION productos 
    LET cmdstr = "fglrun productos.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION bodegas  
    LET cmdstr = "fglrun bodegas.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION tiposmovimiento 
    LET cmdstr = "fglrun tiposmovimiento.42r 1"
    RUN cmdstr WITHOUT WAITING 

   -- Procesos de inventario 

   ON ACTION movimientos 
    LET cmdstr = "fglrun movimientos.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION tomafisico 
    LET cmdstr = "fglrun tomafisico.42r 1"
    RUN cmdstr WITHOUT WAITING 

   {ON ACTION minimosmaximos 
    LET cmdstr = "fglrun minimosmaximos.42r 1"
    RUN cmdstr WITHOUT WAITING } 

   -- Reportes inventario

   ON ACTION rtomafisico
    LET cmdstr = "fglrun rtomafisico.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION rinvfisico   
    LET cmdstr = "fglrun rinvfisico.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION rexistencias
    LET cmdstr = "fglrun rexistencias.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION rkardex 
    LET cmdstr = "fglrun rkardex.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION rintegramovtos    
    LET cmdstr = "fglrun rintegramovtos.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION rdetallemovtos    
    LET cmdstr = "fglrun rdetallemovtos.42r 1"
    RUN cmdstr WITHOUT WAITING 

   -- Facturacion 

   ON ACTION puntoventa
    LET cmdstr = "fglrun puntoventa.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION tdocsxpos
    LET cmdstr = "fglrun tdocsxpos.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION autorizantes 
    LET cmdstr = "fglrun autorizantes.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION empleados    
    LET cmdstr = "fglrun empleados.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION cortecaja   
    -- Verificando que solo una vez pueda ejecutarse la opcion 
    IF ui.Interface.getChildInstances("cortecaja")==0 THEN
       LET cmdstr = "fglrun cortecaja.42r 1"
       RUN cmdstr WITHOUT WAITING
    ELSE
       CALL fgl_winmessage(
       "Atencion:","Opcion de corte de caja ya en ejecucion.","stop") 
    END IF 

   ON ACTION facturacion
    -- Verificando que solo una vez pueda ejecutarse la opcion 
    IF ui.Interface.getChildInstances("facturacion")==0 THEN
       LET cmdstr = "fglrun facturacion.42r 1"
       RUN cmdstr WITHOUT WAITING
    ELSE
       CALL fgl_winmessage(
       "Atencion:","Opcion de facturacion ya en ejecucion.","stop") 
    END IF 

   ON ACTION rcorteventas
    LET cmdstr = "fglrun rcorteventas.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION rresumencortecaja
    LET cmdstr = "fglrun rresumencortecaja.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION rdetallelibroventas 
    LET cmdstr = "fglrun rdetallelibroventas.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION restadisticaventas 
    LET cmdstr = "fglrun restadisticaventas.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION rventasxhoras      
    LET cmdstr = "fglrun rventasxhoras.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION rotroscobrosfac
    LET cmdstr = "fglrun rotroscobrosfac.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION salir             
    -- Verificando si existe algun programa abierto 
    IF ui.Interface.getChildCount()>0 THEN
       CALL fgl_winmessage(
       "Atencion:",
       "Deben cerrarse todas las ventanas antes de salir del sistema.",
       "exclamation")
    ELSE
       EXIT PROGRAM
    END IF

   COMMAND KEY(CONTROL-P) 
    LET cmdstr = "fglrun programas.42r 1"
    RUN cmdstr 

   COMMAND KEY(INTERRUPT)
    -- Verificando si existe algun programa abierto 
    IF ui.Interface.getChildCount()>0 THEN
       CALL fgl_winmessage(
       "Atencion:",
       "Deben cerrarse todas las ventanas antes de salir del sistema.",
       "exclamation")
    ELSE
       EXIT PROGRAM
    END IF
  END MENU 
  CLOSE FORM wmenu 
 ELSE
  -- Abriendo forma del menu
  OPEN FORM wmenu FROM "mainmenub" 
  DISPLAY FORM wmenu 

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("mainmenu",wpais,1)
  CALL librut001_dpelement("empresa",empresa CLIPPED) 

  -- Menu 
  MENU "Menu Principal"
   ON ACTION ordenescomidatpad
    -- Verificando que solo una vez pueda ejecutarse la opcion 
    IF ui.Interface.getChildInstances("ordenescomidatpad")==0 THEN
       LET cmdstr = "fglrun ordenescomidatpad.42r 1"
       RUN cmdstr WITHOUT WAITING
    ELSE
       CALL fgl_winmessage(
       "Atencion:","Opcion de ordenes de comida ya en ejecucion.","stop") 
    END IF 
   ON ACTION salir             
    -- Verificando si existe algun programa abierto 
    IF ui.Interface.getChildCount()>0 THEN
       CALL fgl_winmessage(
       "Atencion:",
       "Deben cerrarse todas las ventanas antes de salir del sistema.",
       "exclamation")
    ELSE
       EXIT PROGRAM
    END IF
   COMMAND KEY(INTERRUPT)
    -- Verificando si existe algun programa abierto 
    IF ui.Interface.getChildCount()>0 THEN
       CALL fgl_winmessage(
       "Atencion:",
       "Deben cerrarse todas las ventanas antes de salir del sistema.",
       "exclamation")
    ELSE
       EXIT PROGRAM
    END IF
  END MENU 
  CLOSE FORM wmenu 
 END IF 
END MAIN

-- Subrutina para cargar los programas del menu principal

FUNCTION mainmenu_programas()
 -- Inicializando vector
 CALL v_programas.clear()
 
 -- Cargando programas
 DECLARE cprog CURSOR FOR
 SELECT a.codpro,
        a.actpro,
        a.ordpro 
  FROM  glb_programs a
  WHERE LENGTH(a.actpro)>0 
  ORDER BY 3

  LET totpro = 1  
  FOREACH cprog INTO v_programas[totpro].*
   LET totpro = totpro+1  
  END FOREACH
  CLOSE cprog
  FREE  cprog
  LET totpro = totpro-1  
END FUNCTION 

-- Subrutina par crear el menu 

FUNCTION createTopMenuGroup(p,t,i)
 DEFINE p om.DomNode
 DEFINE t,i STRING
 DEFINE s om.DomNode
 
 LET s = p.createChild("TopMenuGroup")
 CALL s.setAttribute("text",t) 
 CALL s.setAttribute("image",i) 
 RETURN s
END FUNCTION 

-- Subrutina para crear las opciones del menu 

FUNCTION createTopMenuCommand(p,t,i)
 DEFINE p om.DomNode
 DEFINE t,i STRING
 DEFINE s om.DomNode
 
 LET s = p.createChild("TopMenuCommand")
 CALL s.setAttribute("text",t) 
 CALL s.setAttribute("image",i) 
 RETURN s
END FUNCTION 
