{
facqbe003.4gl 
Mynor Ramirez
Mantenimiento de tipos de documento x punto de venta
Modificado por: Fernando Lontero
}

{ Definicion de variables globales }

GLOBALS "facglo003.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION facqbe003_tipos(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        westado             LIKE fac_tdocxpos.estado, 
        qrytext,qrypart     CHAR(1000),    
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qryres,qry          STRING,   
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL facqbe003_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL facmae003_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.lnktdc,a.numpos,a.codemp,a.tipdoc,a.regfac,
                                a.numcai,a.fecemi,a.fecven,a.nserie,a.numcor,
                                a.numfin,a.haycor,a.nomdoc,a.estado,a.userid,
                                a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel
     -- Salida
     CALL facmae003_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = "SELECT UNIQUE a.lnktdc,b.nompos,c.nomemp,d.nomdoc,",
                                "a.nserie,a.estado ",
                  "FROM  fac_tdocxpos a,fac_puntovta b,glb_empresas c,",
                        "fac_tipodocs d ",
                  "WHERE b.numpos = a.numpos ",
                    "AND c.codemp = a.codemp ",
                    "AND d.tipdoc = a.tipdoc ",
                    "AND ",qrytext CLIPPED,
                  " ORDER BY 1,2,3,4"

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_datos1 SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_tipodocs.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_datos1 INTO v_tipodocs[totlin].tlnktdc THRU 
                         v_tipodocs[totlin].tnserie,
                         westado

    -- Verficando estado
    IF (westado="B") THEN
       LET v_tipodocs[totlin].testado = "mars_bloqueo.png"
    ELSE
       LET v_tipodocs[totlin].testado = "mars_cheque.png"
    END IF 

    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_datos1
   FREE  c_datos1
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL facqbe003_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_tipodocs TO s_tipodocs.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL facmae003_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL facmae003_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF facmae003_tipos(2) THEN
         EXIT DISPLAY 
      ELSE 
         -- Desplegando datos
         CALL facqbe003_datos(v_tipodocs[ARR_CURR()].tlnktdc)
      END IF 

     ON KEY (CONTROL-M) 
      IF (operacion=2) THEN 
       -- Modificando 
       IF facmae003_tipos(2) THEN
          EXIT DISPLAY 
       ELSE 
         -- Desplegando datos
          CALL facqbe003_datos(v_tipodocs[ARR_CURR()].tlnktdc)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF facqbe003_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este tipo de documento ya tiene movimientos, no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar este tipo de documento ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
            --  Eliminando
            CALL facmae003_grabar(3)
            EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte 
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Punto de Venta" 
      LET arrcols[2] = "Empresa"
      LET arrcols[3] = "Tipo de Documento"
      LET arrcols[4] = "Numero Resolucion"
      LET arrcols[5] = "Fecha Emision"
      LET arrcols[6] = "Fecha Vencimiento"
      LET arrcols[7] = "Numero Serie"
      LET arrcols[8] = "Correlativo Inicial"
      LET arrcols[9] = "Correlativo Final"
      LET arrcols[10]= "Corte Ventas"
      LET arrcols[11]= "Nombre Facturacion"
      LET arrcols[12]= "Estado"
      LET arrcols[13]= "Usuario Registro"
      LET arrcols[14]= "Fecha Registro"
      LET arrcols[15]= "Hora Registro" 

      -- Se aplica el mismo query de la seleccion para enviar al reporte 
      LET qry = 
       "SELECT b.nompos,c.nomemp,d.nomdoc,a.numcai,a.fecemi,a.fecven,",
              "a.nserie,a.numcor,a.numfin,",
           "CASE (a.haycor) WHEN 0 THEN 'No' WHEN 1 THEN 'Si' END,",
           "a.nomdoc,",
           "CASE (a.estado) WHEN 'A' THEN 'Activo' WHEN 'B' THEN 'Bloqueado' END,",
           "a.userid,a.fecsis,a.horsis ",
       " FROM fac_tdocxpos a,fac_puntovta b,glb_empresas c,fac_tipodocs d ",
       " WHERE b.numpos = a.numpos ",
          "AND c.codemp = a.codemp ",
          "AND d.tipdoc = a.tipdoc ",
          "AND ",qrytext CLIPPED,
       " ORDER BY 1,2,3,4 "

      -- Ejecutando el reporte
      LET res = librut002_excelreport("Tipos Documento x Punto de Venta",
                                      qry,15,0,arrcols)

     ON ACTION formato
      -- Formato de impresion
      CALL facqbe003_FormatoImpresion(v_tipodocs[ARR_CURR()].tlnktdc) 

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL facqbe003_datos(v_tipodocs[ARR_CURR()].tlnktdc)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen tipos de documento con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Desplegando estado del menu
  CALL facqbe003_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION facqbe003_datos(wlnktdc)
 DEFINE wlnktdc LIKE fac_tdocxpos.lnktdc,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  fac_tdocxpos a "||
              "WHERE a.lnktdc = "||wlnktdc||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_datos1t SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_datos1t INTO w_mae_pro.*
 END FOREACH
 CLOSE c_datos1t
 FREE  c_datos1t

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.lnktdc THRU w_mae_pro.haycor 
 DISPLAY BY NAME w_mae_pro.nomdoc THRU w_mae_pro.estado 
 DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis
END FUNCTION 

-- Subrutina para verificar si el tipo de documento ya tiene movimientos 

FUNCTION facqbe003_integridad()
 DEFINE conteo SMALLINT

 -- Verificando facturacion 
 SELECT COUNT(*)
  INTO  conteo
  FROM  pos_mtransac a 
  WHERE (a.lnktdc = w_mae_pro.lnktdc) 
  IF (conteo>0) THEN
     RETURN 1
  ELSE
     RETURN 0 
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION facqbe003_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Tipos de Documento - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Tipos de Documento - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Tipos de Documento - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Tipos de Documento - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Tipos de Documento - NUEVO")
 END CASE
END FUNCTION

-- Subrutina para registrar el formato de impresion del tipo de documento 

FUNCTION facqbe003_FormatoImpresion(xlnktdc)
 DEFINE w_mae_frm  RECORD LIKE fac_formaimp.*,
        xlnktdc    LIKE fac_formaimp.lnktdc, 
        idx,opc    SMALLINT,
        hayfrm     SMALLINT,
        loop       SMALLINT,
        conteo     INTEGER
 
 -- Desplegando pantalla
 OPEN WINDOW wformatoimp AT 9,27
  WITH FORM "facmae003b"
  CALL fgl_settitle("Formato de Impresion")
 
  -- Obteniendo tipo de usuario del cajero
  INITIALIZE w_mae_frm.* TO NULL 
  SELECT a.*         
   INTO  w_mae_frm.* 
   FROM  fac_formaimp a
   WHERE a.lnktdc = xlnktdc 
   IF (status=NOTFOUND) THEN
      LET hayfrm = FALSE 
   ELSE
      LET hayfrm = TRUE  
   END IF

  -- Ingresando datos 
  LET loop = TRUE 
  WHILE loop 
   INPUT BY NAME w_mae_frm.top001,
                 w_mae_frm.top002, 
                 w_mae_frm.top003, 
                 w_mae_frm.top004, 
                 w_mae_frm.top005, 
                 w_mae_frm.top006, 
                 w_mae_frm.foot01, 
                 w_mae_frm.foot02, 
                 w_mae_frm.foot03, 
                 w_mae_frm.foot04, 
                 w_mae_frm.foot05, 
                 w_mae_frm.foot06, 
                 w_mae_frm.foot07, 
                 w_mae_frm.foot08,
                 w_mae_frm.foot09, 
                 w_mae_frm.foot10  
                 WITHOUT DEFAULTS 
    ATTRIBUTE(UNBUFFERED)

    ON ACTION cancel
     LET loop = FALSE
     EXIT INPUT
   END INPUT

   -- Confirmando cambio
   IF loop THEN
      -- Grabando cajeros seleccionados
      LET opc = librut001_menugraba("Confirmacion",
                                    "Que desea hacer ?",
                                    "Grabar",
                                    "Modificar",
                                    "Cancelar",
                                    NULL)
     
      -- Verificando opcio
      CASE (opc)
       WHEN 1 -- Grabar
        LET loop = FALSE 

        -- Iniciando transaccion
        BEGIN WORK 
       
         -- Actualizando formato
         IF hayfrm THEN 
          SET LOCK MODE TO WAIT
          UPDATE fac_formaimp 
          SET    fac_formaimp.*      = w_mae_frm.* 
          WHERE  fac_formaimp.lnktdc = xlnktdc 
        ELSE
          -- Asignando datos
          LET w_mae_frm.lnktdc = xlnktdc 
          LET w_mae_frm.userid = username  
          LET w_mae_frm.fecsis = CURRENT
          LET w_mae_frm.horsis = CURRENT

          SET LOCK MODE TO WAIT
          INSERT INTO fac_formaimp
          VALUES (w_mae_frm.*) 
        END IF 

        -- Finalizando transaccion
        COMMIT WORK 
       WHEN 2 -- Modificar  
        LET loop = TRUE 
       WHEN 0 -- Cancelar
        LET loop = FALSE 
      END CASE 
   END IF 
  END WHILE 
 CLOSE WINDOW wformatoimp 
END FUNCTION 
