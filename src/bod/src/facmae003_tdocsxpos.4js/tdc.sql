
DBSCHEMA Schema Utility       INFORMIX-SQL Version 12.10.FC6WE







{ TABLE "sistemas".fac_tdocxpos row size = 140 number of columns = 20 index size = 9 }

create table "sistemas".fac_tdocxpos 
  (
    lnktdc serial not null ,
    numpos smallint not null ,
    codemp smallint not null ,
    tipdoc smallint not null ,
    regfac smallint not null ,
    numcai char(40),
    fecemi date,
    nserie char(10) not null ,
    numcor integer,
    numfin integer,
    fecven date,
    haycor smallint not null ,
    csmepl smallint,
    creepl smallint,
    regali smallint,
    nomdoc char(30) not null ,
    estado char(1) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (estado IN ('A' ,'B' )) constraint "sistemas".ckfactdocxpos1,
    primary key (lnktdc)  constraint "sistemas".pkfactdocxpos
  );

revoke all on "sistemas".fac_tdocxpos from "public" as "sistemas";




