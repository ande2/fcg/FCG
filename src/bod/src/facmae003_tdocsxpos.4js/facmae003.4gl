{
Mantenimiento de tipos de documento x punto de venta
facmae003.4gl 
MRS
}

-- Definicion de variables globales 

GLOBALS "facglo003.4gl"
CONSTANT progname = "facmae003"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarTDocxPos")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("tdocsxpos")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL facmae003_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION facmae003_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "facmae003a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Tipos de Documento x Punto de Venta"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de tipos de documento x punto de venta."
    CALL facqbe003_tipos(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo tipo de documento."
    LET savedata = facmae003_tipos(1) 
   COMMAND "Modificar"
    " Modificacion de un tipo de doumento x punto de venta."
    CALL facqbe003_tipos(2) 
   COMMAND "Borrar"
    " Eliminacion de un tipo de documento x punto de venta."
    CALL facqbe003_tipos(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 
FUNCTION facmae003_tipos(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        numfinn           SMALLINT,
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL facqbe003_EstadoMenu(4,"")                                            
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL facmae003_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.numpos,
                w_mae_pro.codemp,
                w_mae_pro.tipdoc,
                w_mae_pro.regfac,
                w_mae_pro.numcai, 
                w_mae_pro.fecemi,
                w_mae_pro.fecven, 
                w_mae_pro.nserie,
                w_mae_pro.numcor,
                w_mae_pro.numfin,
                w_mae_pro.haycor,
                w_mae_pro.nomdoc,
                w_mae_pro.estado
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE INPUT
    -- Verificando integridad
    -- Si tipo de docunmento ya tiene movimientos no se puede modificarse los 
    -- campos siguientes 
    IF (operacion=2) THEN -- Si es modificacion
     IF facqbe003_integridad() THEN
       CALL Dialog.SetFieldActive("numpos",FALSE)
       CALL Dialog.SetFieldActive("codemp",FALSE)
       CALL Dialog.SetFieldActive("tipdoc",FALSE)
     ELSE
       CALL Dialog.SetFieldActive("numpos",TRUE)
       CALL Dialog.SetFieldActive("codemp",TRUE)
       CALL Dialog.SetFieldActive("tipdoc",TRUE)
     END IF
    END IF 

   AFTER FIELD numpos 
    --Verificando punto de venta
    IF w_mae_pro.numpos IS NULL THEN
       ERROR "Error: punto de venta invalido, VERIFICA."
       NEXT FIELD numpos
    END IF

   AFTER FIELD codemp
    --Verificando empresa
    IF w_mae_pro.codemp IS NULL THEN
       ERROR "Error: empresa invalida, VERIFICA."
       NEXT FIELD codemp
    END IF

   AFTER FIELD tipdoc
    IF w_mae_pro.tipdoc IS NULL THEN
       ERROR "Error: tipo de documento invalido, VERIFICA."
       NEXT FIELD tipdoc
    END IF

   ON CHANGE regfac
    -- Habilitando y deshabilitando campos dependiendo si registra facturacion
    CASE (w_mae_pro.regfac)
     WHEN 1 CALL Dialog.SetFieldActive("numcai",TRUE)   
            CALL Dialog.SetFieldActive("numfin",TRUE)
            CALL Dialog.SetFieldActive("fecemi",TRUE)
            CALL Dialog.SetFieldActive("fecven",TRUE)
     WHEN 0 CALL Dialog.SetFieldActive("numcai",FALSE)   
            CALL Dialog.SetFieldActive("numfin",FALSE)
            CALL Dialog.SetFieldActive("fecemi",FALSE)
            CALL Dialog.SetFieldActive("fecven",FALSE)
    END CASE

   AFTER FIELD regfac
    IF w_mae_pro.regfac IS NULL THEN
       ERROR "Error: registra facturacion invalido, VERIFICA."
       NEXT FIELD regfac
    END IF

    -- Habilitando y deshabilitando campos dependiendo si registra facturacion
    CASE (w_mae_pro.regfac)
     WHEN 1 CALL Dialog.SetFieldActive("numcai",TRUE)   
            CALL Dialog.SetFieldActive("numfin",TRUE)
            CALL Dialog.SetFieldActive("fecemi",TRUE)
            CALL Dialog.SetFieldActive("fecven",TRUE)
     WHEN 0 CALL Dialog.SetFieldActive("numcai",FALSE)   
            CALL Dialog.SetFieldActive("numfin",FALSE)
            CALL Dialog.SetFieldActive("fecemi",FALSE)
            CALL Dialog.SetFieldActive("fecven",FALSE)
    END CASE

   AFTER FIELD numcai
    --Verificando numero resolucion 
    IF (LENGTH(w_mae_pro.numcai)<=0) THEN
       ERROR "Error: numero resolucion invalido, VERIFICA."
       NEXT FIELD numcai
    END IF

   AFTER FIELD fecemi 
    -- Verificando fecha de emision 
    IF w_mae_pro.fecemi IS NULL THEN
       ERROR "Error: fecha de emision invalda, VERIFICA."
       NEXT FIELD fecemi 
    END IF 

   AFTER FIELD fecven  
    --Verificando fecha de vencimiento 
    IF w_mae_pro.fecven IS NULL THEN
       ERROR "Error: fecha limite de emision invalida, VERIFICA."
       NEXT FIELD fecven
    END IF

   AFTER FIELD nserie
    -- Verificando numero de serie
    IF (LENGTH(w_mae_pro.nserie)<=0) THEN
       ERROR "Error: numero de serie invalido, VERIFICA."
       NEXT FIELD nserie
    END IF

   AFTER FIELD numcor 
    --Verificando correlativo inicial
    IF w_mae_pro.numcor IS NULL OR
       w_mae_pro.numcor <=0 THEN 
       ERROR "Error: rango inicial invalido, VERIFICA."
       NEXT FIELD numcor 
    END IF

   AFTER FIELD numfin
    --Verificando correlativo final
    IF w_mae_pro.numfin IS NULL OR
       w_mae_pro.numfin <=0 THEN 
       ERROR "Error: rango final invalido, VERIFICA."
       NEXT FIELD numfin 
    END IF
 
   AFTER FIELD haycor
    --Verificando si documento suma corte de ventas
    IF w_mae_pro.haycor IS NULL THEN
       ERROR "Error: hay corte de ventas invalido, VERIFICA."
       NEXT FIELD haycor
    END IF

   AFTER FIELD nomdoc 
    --Verificando nombre del documento 
    IF w_mae_pro.nomdoc IS NULL THEN 
       ERROR "Error: nombre a mostrar en facturacion invalido, VERIFICA."
       NEXT FIELD nomdoc   
    END IF

   AFTER FIELD estado  
    --Verificando estado
    IF w_mae_pro.estado IS NULL THEN
       ERROR "Error: estado invalido, VERIFICA."
       NEXT FIELD estado
    END IF

    -- Verificando si tipo de documento por empresa y serie ya existe
    SELECT UNIQUE a.lnktdc
     FROM  fac_tdocxpos a
     WHERE a.lnktdc != w_mae_pro.lnktdc
       AND a.codemp  = w_mae_pro.codemp  
       AND a.tipdoc  = w_mae_pro.tipdoc 
       AND a.nserie  = w_mae_pro.nserie
       AND a.numcor  = w_mae_pro.numcor
       AND a.numfin  = w_mae_pro.numfin
     IF (status!=NOTFOUND) THEN
       -- Desplegando mensaje
       CALL fgl_winmessage(
       " Atencion", 
       " Ya existe registrado un tipo de documento con dicha informacion. \n"||
       " VERIFICAR  empresa, tipo documento , serie, rengo inicial y final.",
       "information")
       CONTINUE INPUT 
     END IF 
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL facmae003_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL facmae003_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL facqbe003_EstadoMenu(0,"")                                            
    CALL facmae003_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un tipo de documento

FUNCTION facmae003_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando tipo de documento ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   LET w_mae_pro.lnktdc = 0
   LET w_mae_pro.csmepl = 0
   LET w_mae_pro.creepl = 0
   LET w_mae_pro.regali = 0

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO fac_tdocxpos   
   VALUES (w_mae_pro.*)
   LET w_mae_pro.lnktdc = SQLCA.SQLERRD[2]

   --Asignando el mensaje 
   LET msg = "Tipo de documento (",w_mae_pro.lnktdc USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE fac_tdocxpos
   SET    fac_tdocxpos.*      = w_mae_pro.*
   WHERE  fac_tdocxpos.lnktdc = w_mae_pro.lnktdc 

   --Asignando el mensaje 
   LET msg = "Tipo de Documento (",w_mae_pro.lnktdc USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando 
   DELETE FROM fac_tdocxpos 
   WHERE (fac_tdocxpos.lnktdc = w_mae_pro.lnktdc)

   --Asignando el mensaje 
   LET msg = "Tipo de Documento (",w_mae_pro.lnktdc USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL facmae003_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION facmae003_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.lnktdc = 0 
   LET w_mae_pro.estado = "A"
   LET w_mae_pro.regfac = 1
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Cargando combobox
 CALL librut003_cbxempresas() 
 CALL librut003_cbxpuntosventa()
 CALL librut003_cbxtiposdocumento(0) 

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.lnktdc THRU w_mae_pro.haycor 
 DISPLAY BY NAME w_mae_pro.nomdoc THRU w_mae_pro.estado
 DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis
END FUNCTION
