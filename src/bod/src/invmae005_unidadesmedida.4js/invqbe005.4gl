{
invqbe005.4gl 
Mynor Ramirez
Mantenimiento de unidades de medida
}

{ Definicion de variables globales }

GLOBALS "invglo005.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe005_unimedid(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL invqbe005_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae005_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.unimed,a.nommed,a.nomabr,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL invmae005_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.unimed,a.nommed "||
                 " FROM inv_unimedid a "||
                 " WHERE "||qrytext CLIPPED||
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_unimedid SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_unimedid.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_unimedid INTO v_unimedid[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_unimedid
   FREE  c_unimedid
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL invqbe005_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_unimedid TO s_unimedid.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL invmae005_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Ruscar 
      CALL invmae005_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Verificando integridad 
      IF invqbe005_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Esta unidad de medida ya tiene registros. \n Unidad de medida no puede modificarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Modificando 
      IF invmae005_unimedid(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL invqbe005_datos(v_unimedid[ARR_CURR()].tunimed)
      END IF 

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN 
       -- Verificando integridad 
       IF invqbe005_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Esta unidad de medida ya tiene registros. \n Unidad de medida no puede modificarse.",
         "stop")
         CONTINUE DISPLAY 
       END IF 

       -- Modificando 
       IF invmae005_unimedid(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL invqbe005_datos(v_unimedid[ARR_CURR()].tunimed)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF invqbe005_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Esta unidad de medida ya tiene registros. "||
         "\n Unidad de medida no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar esta unidad de medida ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL invmae005_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Unidade de Medida"
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Abreviatura"
      LET arrcols[4] = "Usuario Registro"
      LET arrcols[5] = "Fecha Registro"
      LET arrcols[6] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT a.* ",
                       " FROM inv_unimedid a ",
                       " WHERE ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res        = librut002_excelreport("Unidades de Medida",qry,6,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL invqbe005_datos(v_unimedid[1].tunimed)
      ELSE
         CALL invqbe005_datos(v_unimedid[ARR_CURR()].tunimed)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen unidades de medida con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Desplegando estado del programa
  CALL invqbe005_EstadoMenu(0,"") 
END FUNCTION

{ Subrutina para desplegar la datos del mantenimiento }

FUNCTION invqbe005_datos(wunimed)
 DEFINE wunimed LIKE inv_unimedid.unimed,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  inv_unimedid a "||
              "WHERE a.unimed = "||wunimed||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_unimedidt SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_unimedidt INTO w_mae_pro.*
 END FOREACH
 CLOSE c_unimedidt
 FREE  c_unimedidt

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nommed,w_mae_pro.nomabr 
 DISPLAY BY NAME w_mae_pro.unimed,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para verificar si el unidad de medida tiene registros

FUNCTION invqbe005_integridad()
 DEFINE conteo INTEGER  

 -- Verificando productos 
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_products a
  WHERE (a.unimed = w_mae_pro.unimed) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF

 RETURN FALSE 
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION invqbe005_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Unidades de Medida - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Unidades de Medida - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Unidades de Medida - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Unidades de Medida - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Unidades de Medida - NUEVO")
 END CASE
END FUNCTION
