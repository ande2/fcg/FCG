{ 
invglo005.4gl
Mynor Ramirez
Mantenimiento de unidades de medida    
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "invmae005"
DEFINE w_mae_pro   RECORD LIKE inv_unimedid.*,
       v_unimedid  DYNAMIC ARRAY OF RECORD
        tunimed    LIKE inv_unimedid.unimed,
        tnommed    LIKE inv_unimedid.nommed 
       END RECORD,
       username    VARCHAR(15) 
END GLOBALS
