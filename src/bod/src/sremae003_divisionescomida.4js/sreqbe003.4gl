{
sreqbe003.4gl 
Mantenimiento de divisiones de comidas 
}

{ Definicion de variables globales }

GLOBALS "sreglo003.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION sreqbe003_divisiones(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qrytext,qrypart     STRING,             
        qry,msg             STRING,   
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL sreqbe003_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL sremae003_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.coddiv,a.nomdiv,a.enmenu,a.norden,
                                a.estado,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel
     -- Salida
     CALL sremae003_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = "SELECT UNIQUE a.coddiv,a.norden,a.nomdiv,'' ",
                  "FROM  sre_divrecet a ",
                  "WHERE ",qrytext CLIPPED,
                  "ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_divisiones SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_divisiones.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_divisiones INTO v_divisiones[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_divisiones
   FREE  c_divisiones
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL sreqbe003_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_divisiones TO s_divisiones.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL sremae003_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL sremae003_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      LET res = sremae003_divisiones(2) 
      -- Desplegando datos
      CALL sreqbe003_datos(v_divisiones[ARR_CURR()].tcoddiv)

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN 
         -- Modificando 
         LET res = sremae003_divisiones(2) 
         -- Desplegando datos
         CALL sreqbe003_datos(v_divisiones[ARR_CURR()].tcoddiv)
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF librut004_IntegridadDivisionesComida(v_divisiones[ARR_CURR()].tcoddiv) THEN 
         CALL fgl_winmessage(
         " Atencion",
         " Esta division ya tiene movimientos, no puede borrarse.",
         "stop")
         CONTINUE DISPLAY
      END IF

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar esta Division ?"
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL sremae003_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte 
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Division" 
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "En Menu"
      LET arrcols[4] = "Orden Menu" 
      LET arrcols[5] = "Estado"
      LET arrcols[6] = "Usuario Registro"
      LET arrcols[7] = "Fecha Registro"
      LET arrcols[8] = "Hora Registro" 

      -- Se aplica el mismo query de la seleccion para enviar al reporte 
      LET qry        = 
       "SELECT a.coddiv,a.nomdiv,",
              "CASE (a.enmenu) WHEN 1 THEN 'Si' WHEN 2 THEN 'No' END,",
              "a.norden,", 
              "CASE (a.estado) WHEN 1 THEN 'Activada' WHEN 2 THEN 'Desactivada' END,",
              "a.userid,a.fecsis,a.horsis ",
        " FROM sre_divrecet a ",
        " WHERE ",qrytext CLIPPED,
        " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res    = librut002_excelreport("Divisiones de Comidas",qry,8,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL sreqbe003_datos(v_divisiones[ARR_CURR()].tcoddiv)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen divisiones de comidas con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Asignando enmenu del menu 
  CALL sreqbe003_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION sreqbe003_datos(wcoddiv)
 DEFINE wcoddiv LIKE sre_divrecet.coddiv,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  sre_divrecet a "||
              "WHERE a.coddiv = "||wcoddiv||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_divisionest SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_divisionest INTO w_mae_pro.*
 END FOREACH
 CLOSE c_divisionest
 FREE  c_divisionest

 -- Desplegando imagen 
 CALL f.setElementImage("imgpro",w_mae_pro.imagen CLIPPED)

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomdiv,w_mae_pro.enmenu,w_mae_pro.norden,w_mae_pro.estado 
 DISPLAY BY NAME w_mae_pro.coddiv,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para desplegar el enmenu del menu de acuerdo a la operacion a realizar

FUNCTION sreqbe003_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING      

 -- Desplegando enmenu del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Divisiones - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Divisiones - BUSCAR"||
         msg CLIPPED)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Divisiones - MODIFICAR"||
         msg CLIPPED)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Divisiones - BORRAR"||
         msg CLIPPED)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Divisiones - NUEVO")
 END CASE
END FUNCTION
