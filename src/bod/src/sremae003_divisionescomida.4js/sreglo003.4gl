{ 
sreglo003.4gl
Mantenimiento de divisiones de comidas 
Mynor Ramirez 
}

DATABASE storepos 

{ Definicion de variables globales }

GLOBALS
CONSTANT progname = "sremae003"
DEFINE w_mae_pro      RECORD LIKE sre_divrecet.*,
       v_divisiones   DYNAMIC ARRAY OF RECORD
        tcoddiv       LIKE sre_divrecet.coddiv,
        tnorden       LIKE sre_divrecet.norden, 
        tnomdiv       LIKE sre_divrecet.nomdiv, 
        tendrec       CHAR(1) 
       END RECORD,
       w              ui.Window,
       f              ui.Form,
       username       VARCHAR(15)
END GLOBALS
