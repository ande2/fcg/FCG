{
Mantenimiento de divisiones de comidas 
sremae003.4gl 
}

IMPORT os 

-- Definicion de variables globales 

CONSTANT DirectorioPictures = "/sistemas/apl/byr/pic/"
CONSTANT DirectorioBase     = "c:\\\\"
CONSTANT ExtensionImagenes  = "*.jpg *.gif *.ico *.bmp *.png" 
GLOBALS "sreglo003.4gl"
DEFINE filename STRING 

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("divcomidas")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo nombre del usuario 
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL sremae003_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION sremae003_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT,
        existe   SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "sremae003a" ATTRIBUTE(BORDER)

  -- Obteniendo datos de la ventana actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("sremae003",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Divisiones"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(progname,4,username) THEN 
       HIDE OPTION "Buscar"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    "Consulta de divisiones existentes."
    CALL sreqbe003_divisiones(1) 
   COMMAND "Nuevo"
    "Ingreso de una nueva division."
    LET savedata = sremae003_divisiones(1) 
   COMMAND "Modificar"
    "Modificacion de una division existente."
    CALL sreqbe003_divisiones(2) 
   COMMAND "Borrar"
    "Eliminacion de una division existente."
    CALL sreqbe003_divisiones(3) 
   COMMAND "Salir"
    "Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION sremae003_divisiones(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata,idx      SMALLINT,
        msg               CHAR(80),
        ExtensionArchivo  STRING, 
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL sreqbe003_EstadoMenu(4,"")
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL sremae003_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomdiv,
                w_mae_pro.enmenu,
                w_mae_pro.norden, 
                w_mae_pro.estado
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   ON ACTION addimagen    
    -- Agregando imagen de la division
    -- Seleccionando archivo de imagen
    LET filename = winopenfile(DirectorioBase,
                   "Imagenes",
                   ExtensionImagenes,
                   "Seleccion de la Imagen de la Division")

    IF (LENGTH(filename)>0) THEN 
     -- Abriendo archivo 
     LET ExtensionArchivo = os.Path.basename(filename) 
     LET idx              = ExtensionArchivo.GetIndexOf(".",1) 
     LET ExtensionArchivo = ExtensionArchivo.substring(idx,ExtensionArchivo.GetLength()) 
     LET w_mae_pro.imagen = DirectorioPictures CLIPPED,"IMGDIVPLATO",
                            w_mae_pro.coddiv USING "<<<<<<<<<<",
                            ExtensionArchivo CLIPPED

     -- Limpiando imagen 
     CALL f.setElementImage("imgpro","")

     -- Copiando el archivo del frontend al server 
     CALL librut001_getfile(filename,w_mae_pro.imagen) 

     -- Desplegando imagen
     CALL f.setElementImage("imgpro",w_mae_pro.imagen CLIPPED)

     -- Actualizando url de la imagen
     SET LOCK MODE TO WAIT 
     UPDATE sre_divrecet
     SET    sre_divrecet.imagen = w_mae_pro.imagen
     WHERE  sre_divrecet.coddiv = w_mae_pro.coddiv 
    END IF 

   ON ACTION delimagen
    -- Agregando imagen de la division
    LET w_mae_pro.imagen = NULL

    -- Desplegando imagen
    CALL f.setElementImage("imgpro",w_mae_pro.imagen CLIPPED)

    -- Actualizando url de la imagen
    SET LOCK MODE TO WAIT 
    UPDATE sre_divrecet
    SET    sre_divrecet.imagen = w_mae_pro.imagen
    WHERE  sre_divrecet.coddiv = w_mae_pro.coddiv 

   AFTER FIELD nomdiv  
    --Verificando nombre de la division  
    IF (LENGTH(w_mae_pro.nomdiv)=0) THEN
       ERROR "Error: nombre de la division invalida, VERIFICA."
       LET w_mae_pro.nomdiv = NULL
       NEXT FIELD nomdiv  
    END IF

    -- Verificando que no exista otra division con el mismo nombre            
    SELECT UNIQUE (a.coddiv)
     FROM  sre_divrecet a
     WHERE (a.coddiv != w_mae_pro.coddiv) 
       AND (a.nomdiv  = w_mae_pro.nomdiv) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otra division de comida con el mismo nombre, VERIFICA.",
        "information")
        NEXT FIELD nomdiv
     END IF 

   AFTER FIELD enmenu
    --Verificando enmenu
    IF w_mae_pro.enmenu IS NULL THEN 
       ERROR "Error: division en menu invalida, VERIFICA."
       NEXT FIELD enmenu
    END IF

   AFTER FIELD norden 
    --Verificando norden 
    IF w_mae_pro.norden IS NULL OR 
       w_mae_pro.norden <=0 THEN 
       LET w_mae_pro.norden = NULL 
       ERROR "Error: orden de la division invalido, VERIFICA."
       NEXT FIELD norden 
    END IF

   AFTER FIELD estado
    --Verificando estado
    IF w_mae_pro.estado IS NULL THEN 
       ERROR "Error: estado invalido, VERIFICA."
       NEXT FIELD estado
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL sremae003_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL sremae003_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL sreqbe003_EstadoMenu(0,"")
    CALL sremae003_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar una division de comida 

FUNCTION sremae003_grabar(operacion)
 DEFINE operacion SMALLINT,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando Division ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.coddiv),0)
    INTO  w_mae_pro.coddiv
    FROM  sre_divrecet a
    IF (w_mae_pro.coddiv IS NULL) THEN
       LET w_mae_pro.coddiv = 1
    ELSE 
       LET w_mae_pro.coddiv = (w_mae_pro.coddiv+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO sre_divrecet   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.coddiv 

   --Asignando el mensaje 
   LET msg = "Division de Comida (",w_mae_pro.coddiv USING "<<<<<<",") registrada."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE sre_divrecet
   SET    sre_divrecet.*      = w_mae_pro.*
   WHERE  sre_divrecet.coddiv = w_mae_pro.coddiv 

   --Asignando el mensaje 
   LET msg = "Division de Comida (",w_mae_pro.coddiv USING "<<<<<<",") actualizada."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando divisiones
   DELETE FROM sre_divrecet 
   WHERE (sre_divrecet.coddiv = w_mae_pro.coddiv)

   --Asignando el mensaje 
   LET msg = "Division de Comida (",w_mae_pro.coddiv USING "<<<<<<",") borrada."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL sremae003_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION sremae003_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.coddiv = 0 
   LET w_mae_pro.enmenu = 1 
   LET w_mae_pro.norden = 1  
   LET w_mae_pro.estado = 1 
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.coddiv,w_mae_pro.nomdiv,w_mae_pro.enmenu,
                 w_mae_pro.norden,w_mae_pro.estado 
 DISPLAY BY NAME w_mae_pro.coddiv,w_mae_pro.userid THRU w_mae_pro.horsis 

 -- Limpiando imagen de la division
 CALL f.setElementImage("imgpro","")
END FUNCTION
