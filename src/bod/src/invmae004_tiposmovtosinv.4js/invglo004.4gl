{ 
invglo004.4gl
Mynor Ramirez
Mantenimiento de tipos de movimiento
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "invmae004" 
DEFINE w_mae_pro   RECORD LIKE inv_tipomovs.*,
       v_tipomovs  DYNAMIC ARRAY OF RECORD
        ttipmov    LIKE inv_tipomovs.tipmov,
        tnommov    LIKE inv_tipomovs.nommov, 
        ttipope    CHAR(20)
       END RECORD, 
       v_permxusr  DYNAMIC ARRAY OF RECORD
        tcheckb    SMALLINT,
        tuserid    LIKE inv_permxtmv.userid,
        tnomusr    VARCHAR(50), 
        tusuaid    LIKE inv_permxtmv.usuaid,
        tfecsis    LIKE inv_permxtmv.fecsis,
        thorsis    LIKE inv_permxtmv.horsis,
        tendrec    CHAR(1)                        
       END RECORD,
       username    VARCHAR(15),
       haymov      SMALLINT,
       corinimov   SMALLINT 
END GLOBALS
