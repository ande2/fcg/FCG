{
glbqbe041.4gl 
Mynor Ramirez
Mantenimiento de clientes 
}

{ Definicion de variables globales }

GLOBALS "glbglo041.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe041_clientes(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        westado             LIKE fac_clientes.estado, 
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form,
        qrytext,qrypart     STRING

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL glbqbe041_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae041_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codcli,a.nomcli,a.numnit,
                                a.numtel,a.nmovil,a.dircli,
                                a.nompro,a.bemail,a.estado,
                                a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL glbmae041_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progres ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codcli,a.nomcli,a.numnit,a.estado,'' ",
                 " FROM fac_clientes a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_clientes SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_clientes.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_clientes INTO v_clientes[totlin].tcodcli THRU 
                           v_clientes[totlin].tnumnit,
                           westado

    -- Verficando estado
    IF (westado=0) THEN
       LET v_clientes[totlin].testado = "mars_cancelar.png"
    ELSE
       LET v_clientes[totlin].testado = "mars_cheque.png"
    END IF

    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_clientes
   FREE  c_clientes
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL glbqbe041_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_clientes TO s_clientes.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL glbmae041_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar
      CALL glbmae041_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF glbmae041_clientes(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL glbqbe041_datos(v_clientes[ARR_CURR()].tcodcli)
      END IF 

     ON KEY (CONTROL-M)   
      -- Modificando 
      IF (operacion=2) THEN 
       IF glbmae041_clientes(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL glbqbe041_datos(v_clientes[ARR_CURR()].tcodcli)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF glbqbe041_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este cliente ya tiene movimientos. Clientes no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Este SEGURO de Borrar este cliente ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL glbmae041_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1]  = "Codigo"
      LET arrcols[2]  = "Nombre"
      LET arrcols[3]  = "Numero de NIT"
      LET arrcols[4]  = "Numero Tel-Fijo"
      LET arrcols[5]  = "Numero Celular"
      LET arrcols[6]  = "Direccion"
      LET arrcols[7]  = "Contacto"
      LET arrcols[8]  = "Direccion de Correo"
      LET arrcols[9]  = "Estado Cliente"
      LET arrcols[10] = "Usuario Registro"
      LET arrcols[11] = "Fecha Registro"
      LET arrcols[12] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry  = 
      "SELECT a.codcli,a.nomcli,a.numnit,a.numtel,a.nmovil,",
             "a.dircli,a.nompro,a.bemail,",
       "CASE (a.estado) WHEN 1 THEN 'Alta' WHEN 0 THEN 'Baja' END CASE,",
       "a.userid,a.fecsis,a.horsis ",
      " FROM fac_clientes a ",
      " WHERE ",qrytext CLIPPED,
      " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res = librut002_excelreport("Clientes",qry,12,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
       -- Cargando datos del cliente 
       CALL glbqbe041_datos(v_clientes[ARR_CURR()].tcodcli)
      END IF 
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen clientes con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Asignando estado del menu
  CALL glbqbe041_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION glbqbe041_datos(wcodcli)
 DEFINE wcodcli LIKE fac_clientes.codcli,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  fac_clientes a "||
              "WHERE a.codcli = "||wcodcli||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_clientest SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_clientest INTO w_mae_pro.*
 END FOREACH
 CLOSE c_clientest
 FREE  c_clientest

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.codcli THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para verificar si el cliente ya tiene movimientos  

FUNCTION glbqbe041_integridad()
 DEFINE conteo INTEGER 

 -- Verificando inventarios
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_mtransac a
  WHERE (a.coddes = w_mae_pro.codcli) 
  IF (conteo>0) THEN
     RETURN TRUE 
  ELSE 
     RETURN FALSE 
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION glbqbe041_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Clientes - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Clientes - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Clientes - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Clientes - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Clientes - NUEVO")
 END CASE
END FUNCTION
