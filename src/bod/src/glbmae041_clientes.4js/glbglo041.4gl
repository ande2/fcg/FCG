{ 
glbglo041.4gl
Mynor Ramirez
Mantenimiento de clientes
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae041"
DEFINE w_mae_pro   RECORD LIKE fac_clientes.*,
       v_clientes  DYNAMIC ARRAY OF RECORD
        tcodcli    LIKE fac_clientes.codcli,
        tnomcli    LIKE fac_clientes.nomcli, 
        tnumnit    LIKE fac_clientes.numnit,
        testado    CHAR(20),
        tstatus    CHAR(20),
        tendrec    CHAR(1) 
       END RECORD, 
       username    VARCHAR(15)
END GLOBALS
