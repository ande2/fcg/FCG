{ 
Fecha    : Enero 2011 
Programo : invrep003.4gl 
Objetivo : Reporte de kardex de producto 
}

DATABASE storepos 

{ Definicion de variables globales }

DEFINE w_mae_bod RECORD LIKE inv_mbodegas.*,
       w_mae_emp RECORD LIKE glb_empresas.*,
       w_mae_suc RECORD LIKE glb_sucsxemp.*,
       w_datos   RECORD
        codbod   LIKE inv_mtransac.codbod,
        codemp   LIKE inv_mtransac.codemp,
        codsuc   LIKE inv_mtransac.codbod,
        codcat   LIKE glb_categors.codcat,
        subcat   LIKE glb_subcateg.subcat,
        cditem   LIKE inv_dtransac.cditem,
        fecini   DATE,
        fecfin   DATE
       END RECORD,
       fnt       RECORD
        cmp      CHAR(12),
        nrm      CHAR(12),
        tbl      CHAR(12),
        fbl,t88  CHAR(12),
        t66,p12  CHAR(12),
        p10,srp  CHAR(12),
        twd      CHAR(12),
        fwd      CHAR(12),
        tda,fda  CHAR(12),
        ini      CHAR(12)
       END RECORD,
       existe    SMALLINT,
       filename  STRING, 
       fcodcat   STRING, 
       fsubcat   STRING, 
       fcditem   STRING 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar7")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rkardex")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep003_kardex()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep003_kardex()
 DEFINE w_pro_bod         RECORD LIKE inv_proenbod.*,
        w_mae_pro         RECORD LIKE inv_products.*,
        imp1              RECORD
         codemp           LIKE inv_proenbod.codemp, 
         codsuc           LIKE inv_proenbod.codsuc, 
         codbod           LIKE inv_proenbod.codbod,
         cditem           LIKE inv_proenbod.cditem, 
         codabr           CHAR(20),
         dsitem           CHAR(60),
         nommed           CHAR(30),
         eximin           LIKE inv_proenbod.eximin, 
         eximax           LIKE inv_proenbod.eximax,
         usureg           LIKE inv_products.userid,
         fecreg           LIKE inv_products.fecsis,
         lnktra           LIKE inv_mtransac.lnktra,
         numrf1           CHAR(20),
         numrf2           CHAR(20), 
         observ           CHAR(200), 
         userid           LIKE inv_mtransac.userid,
         fecemi           LIKE inv_mtransac.fecemi,
         tipmov           LIKE inv_dtransac.tipmov,
         abrmov           LIKE inv_tipomovs.nomabr,
         tipope           LIKE inv_dtransac.tipope,
         canuni           LIKE inv_dtransac.canuni,
         horsis           LIKE inv_dtransac.horsis,
         nomcli           CHAR(30),
         nomprv           CHAR(30), 
         salanu           DEC(14,2),
         salanv           DEC(14,2)
        END RECORD,
        wpais             VARCHAR(255),
        loop,haymov       SMALLINT,
        pipeline          STRING, 
        qrytxt            STRING,
        strcodcat         STRING,
        strsubcat         STRING,
        strcditem         STRING 

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep003a AT 5,2
  WITH FORM "invrep003a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invrep003",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/KardexProducto.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de bodegas x usuario
  CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   CLEAR FORM

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codbod,
                 w_datos.codcat,
                 w_datos.subcat,
                 w_datos.cditem,
                 w_datos.fecini,
                 w_datos.fecfin
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)
     LET fcditem = GET_FLDBUF(w_datos.cditem)

     -- Verificando datos 
     IF w_datos.codbod IS NULL OR
        w_datos.fecini IS NULL OR
        w_datos.fecfin IS NULL THEN
        ERROR "Error: deben completarse los filtos de seleccion." 
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)
     LET fcditem = GET_FLDBUF(w_datos.cditem)

     -- Verificando datos 
     IF w_datos.codbod IS NULL OR
        w_datos.fecini IS NULL OR
        w_datos.fecfin IS NULL THEN
        ERROR "Error: deben completarse los filtos de seleccion." 
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON CHANGE codbod
     -- Obteniendo datos de la bodega
     CALL librut003_bbodega(w_datos.codbod)
     RETURNING w_mae_bod.*,existe

     -- Asignando datos de empresa y sucursal de la bodega
     LET w_datos.codemp = w_mae_bod.codemp
     LET w_datos.codsuc = w_mae_bod.codsuc

     -- Obteniendo datos de la empresa
     CALL librut003_bempresa(w_mae_bod.codemp)
     RETURNING w_mae_emp.*,existe
     -- Obteniendo datos de la sucursal
     CALL librut003_bsucursal(w_mae_bod.codsuc)
     RETURNING w_mae_suc.*,existe

     -- Desplegando datos de la empresa y sucursal
     DISPLAY BY NAME w_datos.codemp,w_datos.codsuc,w_mae_emp.nomemp,w_mae_suc.nomsuc

     -- Limpiando combos de categorias y subcategorias
     LET w_datos.codcat = NULL
     LET w_datos.subcat = NULL
     CLEAR codcat,subcat

    ON CHANGE codcat
     -- Limpiando combos
     LET w_datos.subcat = NULL
     LET w_datos.cditem = NULL
     CLEAR subcat,cditem

     -- Llenando combox de subcategorias
     IF w_datos.codcat IS NOT NULL THEN 
        CALL librut003_cbxsubcategorias(w_datos.codcat)

        -- Llenando combox de productos
        CALL librut003_cbxproductosdes(w_datos.codcat,w_datos.subcat) 
     END IF 

    ON CHANGE subcat
     -- Limpiando combos
     LET w_datos.cditem = NULL
     CLEAR cditem

     -- Llenando combox de productos 
     IF w_datos.subcat IS NOT NULL THEN 
        -- Llenando combox de productos
        CALL librut003_cbxproductosdes(w_datos.codcat,w_datos.subcat) 
     END IF 

    AFTER FIELD codbod
     -- Verificando bodega 
     IF w_datos.codbod IS NULL THEN
        ERROR "Error: debe de seleccionarse la bodega a listar."
        NEXT FIELD codbod
     END IF

    AFTER FIELD fecini
     -- Verificando fecha inicial
     IF w_datos.fecini IS NULL THEN
        ERROR "Error: debe ingresarse la fecha inicial a listar."
        NEXT FIELD fecini 
     END IF

    AFTER FIELD fecfin
     -- Verificando fecha final
     IF w_datos.fecfin IS NULL THEN
        ERROR "Error: debe ingresarse la fecha final a listar."
        NEXT FIELD fecfin 
     END IF

    AFTER INPUT
     -- Verificando datos
     IF w_datos.codbod IS NULL OR
        w_datos.fecini IS NULL OR
        w_datos.fecfin IS NULL OR
        pipeline IS NULL THEN
        NEXT FIELD codbod
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Verificando seleccion de categoria
   LET strcodcat = NULL
   IF w_datos.codcat IS NOT NULL THEN
      LET strcodcat = "AND d.codcat = ",w_datos.codcat
   END IF

   -- Verificando condicion de subcategoria
   LET strsubcat = NULL
   IF w_datos.subcat IS NOT NULL THEN
      LET strsubcat = "AND e.subcat = ",w_datos.subcat
   END IF

   -- Verificando condicion de producto
   LET strcditem = NULL
   IF w_datos.cditem IS NOT NULL THEN
      LET strcditem = "AND a.cditem = ",w_datos.cditem 
   END IF

   -- Construyendo seleccion 
   LET qrytxt = "SELECT a.codemp,a.codsuc,a.codbod,a.cditem,b.codabr,",
                       "b.despro,c.nommed,a.eximin,a.eximax,b.userid,b.fecsis ",
                  "FROM inv_proenbod a,inv_products b,inv_unimedid c,",
                        "glb_categors d,glb_subcateg e ",
                  "WHERE a.codemp = ",w_datos.codemp,
                   " AND a.codsuc = ",w_datos.codsuc,
                   " AND a.codbod = ",w_datos.codbod,
                   " AND b.cditem = a.cditem ",
                   " AND b.estado = 1 ",
                   " AND c.unimed = b.unimed ",
                   " AND d.codcat = b.codcat ",
                   " AND e.codcat = b.codcat ",
                   " AND e.subcat = b.subcat ",
                   strcodcat CLIPPED," ",
                   strsubcat CLIPPED," ",
                   strcditem CLIPPED,
                   " ORDER BY 1,2,3,4"

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep003 FROM qrytxt 
   DECLARE c_crep003 CURSOR FOR c_rep003
   LET existe = FALSE
   FOREACH c_crep003 INTO imp1.codemp THRU imp1.fecreg  
    -- Iniciando reporte
    IF NOT existe THEN
       -- Seleccionando fonts para impresora epson
       CALL librut001_fontsprn(pipeline,"pdf")
       RETURNING fnt.*

       LET existe = TRUE
       START REPORT invrep003_invkrxpro TO filename
    END IF 

    -- Calculando saldo anterior del producto
    CALL librut003_saldoantxpro(imp1.codemp,imp1.codsuc,imp1.codbod,
                                 imp1.cditem,w_datos.fecini)
    RETURNING imp1.salanu,imp1.salanv 

    -- Seleccionando movimientos del producto 
    DECLARE cmovto CURSOR FOR
    SELECT a.lnktra,a.numrf1,a.numrf2,a.observ,a.userid,d.fecemi,
           d.tipmov,t.nomabr,d.tipope,d.canuni,d.horsis,c.nomcli,
           p.nomprv 
     FROM  inv_mtransac a,inv_dtransac d,inv_tipomovs t,
           outer fac_clientes c,outer inv_provedrs p
     WHERE (a.lnktra  = d.lnktra)
       AND (a.estado  = "V") 
       AND (d.codemp  = imp1.codemp) 
       AND (d.codsuc  = imp1.codsuc) 
       AND (d.codbod  = imp1.codbod) 
       AND (d.cditem  = imp1.cditem) 
       AND (d.fecemi >= w_datos.fecini)     
       AND (d.fecemi <= w_datos.fecfin)       
       AND (t.tipmov  = d.tipmov) 
       AND (c.codcli  = a.coddes)
       AND (p.codprv  = a.codori)
     ORDER BY d.fecemi,d.horsis,d.lnktra   

     LET haymov = FALSE
     FOREACH cmovto INTO imp1.lnktra THRU imp1.nomprv 
      IF NOT haymov THEN
         LET haymov = TRUE
      END IF 

      -- Llenando reporte
      OUTPUT TO REPORT invrep003_invkrxpro(imp1.*,haymov)
     END FOREACH

     -- Si producto no tiene movimiento imprime una linea indicandolo
     IF NOT haymov THEN
        -- Llenando reporte
        OUTPUT TO REPORT invrep003_invkrxpro(imp1.*,haymov)
     END IF 
   END FOREACH
   CLOSE c_crep003 
   FREE  c_crep003 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT invrep003_invkrxpro 

      -- Enviando reporte al destino seleccionado
      CALL librut001_sendreport
      (filename,pipeline,"Kardes Por Producto",
      "--noline-numbers "||
      "--nofooter "||
      "--font-size 8 "||
      "--page-width 842 --page-height 595 "||
      "--left-margin 55 --right-margin 25 "||
      "--top-margin 35 --bottom-margin 45 "||
      "--title Inventarios")

      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE
 CLOSE WINDOW wrep003a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT invrep003_invkrxpro(imp1,haymov)
 DEFINE imp1              RECORD
         codemp           LIKE inv_proenbod.codemp, 
         codsuc           LIKE inv_proenbod.codsuc, 
         codbod           LIKE inv_proenbod.codbod,
         cditem           LIKE inv_proenbod.cditem, 
         codabr           CHAR(20),
         dsitem           CHAR(60),
         nommed           CHAR(30),
         eximin           LIKE inv_proenbod.eximin, 
         eximax           LIKE inv_proenbod.eximax,
         usureg           LIKE inv_products.userid,
         fecreg           LIKE inv_products.fecsis,
         lnktra           LIKE inv_mtransac.lnktra,
         numrf1           CHAR(20),
         numrf2           CHAR(20), 
         observ           CHAR(200), 
         userid           LIKE inv_mtransac.userid,
         fecemi           LIKE inv_mtransac.fecemi,
         tipmov           LIKE inv_dtransac.tipmov,
         abrmov           LIKE inv_tipomovs.nomabr,
         tipope           LIKE inv_dtransac.tipope,
         canuni           LIKE inv_dtransac.canuni,
         horsis           LIKE inv_dtransac.horsis,
         nomcli           CHAR(30),
         nomprv           CHAR(30), 
         salanu           DEC(14,2),
         salanv           DEC(14,2)
        END RECORD,
        w_cargou,w_saluni DEC(14,2),
        w_cartov,w_cartou DEC(14,2),
        w_abonou,w_abotou DEC(14,2),
        wobsrv            CHAR(200), 
        haymov            SMALLINT, 
        linea             CHAR(154),
        wdocto            CHAR(20),
        wobs              CHAR(50) 

  OUTPUT PAGE   LENGTH 50 
         LEFT   MARGIN 0
         TOP    MARGIN 0
         BOTTOM MARGIN 0 

  FORMAT
   PAGE HEADER
    LET linea = "--------------------------------------------------",
                "--------------------------------------------------",
                "--------------------------------------------------",
                "----"

    -- Configurando tipos de letra 
    PRINT fnt.ini CLIPPED,fnt.t66 CLIPPED,fnt.cmp CLIPPED,fnt.p12 CLIPPED

    -- Imprimiendo encabezado 
    PRINT COLUMN   1,"Inventarios",
          COLUMN 134,PAGENO USING "Pagina: <<<<"
    PRINT COLUMN   1,"Invrep003",
          COLUMN  58,"K A R D E X  P O R  P R O D U C T O",
          COLUMN 134,"Fecha : ",TODAY USING "dd/mmm/yyyy"
    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED, 
          COLUMN  56,"Periodo Del ",w_datos.fecini USING "dd/mmm/yyyy",
           	     " Al ",       w_datos.fecfin USING "dd/mmm/yyyy", 
          COLUMN 134,"Hora  : ",TIME
    PRINT linea	       
    PRINT "Fecha de     Tipo    Numero de            Descripcion del Docume",
          "nto                              |-",
          "-----------  Unidades  ------------|  Usuario"
    PRINT "Emision      Movto   Documento                                  ",
          "                                   ",
          "  Cargos        Abonos       Saldo    Opero"
    PRINT linea

  BEFORE GROUP OF imp1.cditem
   SKIP TO TOP OF PAGE

   -- Inicializando datos
   LET w_cartou = 0
   LET w_abotou = 0
   LET w_saluni = imp1.salanu

   -- Imprimiendo datos del producto
   PRINT fnt.twd CLIPPED,fnt.tbl  CLIPPED,
         w_mae_emp.nomemp         CLIPPED,
         fnt.fbl CLIPPED,fnt.fwd  CLIPPED
   PRINT fnt.twd                  CLIPPED,
         "SUCURSAL [",imp1.codsuc USING "<<<<","] ",
         w_mae_suc.nomsuc         CLIPPED,
         fnt.fwd                  CLIPPED
   PRINT fnt.twd                  CLIPPED,
         "BODEGA [",imp1.codbod   USING "<<<<","] ",
         w_mae_bod.nombod         CLIPPED,
         fnt.fwd                  CLIPPED

   SKIP 1 LINES 
   PRINT COLUMN   1,"Producto           : ",imp1.codabr  CLIPPED,2 SPACES,
                                            imp1.dsitem  CLIPPED,
         COLUMN 110,"Existencia Minima  :  ",imp1.eximin  USING "<<<,<<&.&&"
   PRINT COLUMN   1,"Unidad de Medida   : ",imp1.nommed  CLIPPED, 
         COLUMN 110,"Existencia Maxima  :  ",imp1.eximax  USING "<<<,<<&.&&"
   PRINT COLUMN   1,"Usuario Registro   : ",imp1.usureg 
   PRINT COLUMN   1,"Fecha de Registro  : ",imp1.fecreg  USING "dd/mmm/yyyy"
   PRINT COLUMN 103,"Saldo Anterior --> ",
         COLUMN 123,imp1.salanu  USING "--,---,--&.&&"
   PRINT linea

  ON EVERY ROW
   -- Imprimiendo movimientos  
   -- Verificando si hay movimiento        
   IF haymov THEN
      -- Inicializado datos 
      LET w_cargou = 0 
      LET w_abonou = 0
     
      -- Calculando saldos
      CASE (imp1.tipope)        
       WHEN 1 -- Cargos
        LET w_cargou  = imp1.canuni   
        LET w_cartou  = (w_cartou+imp1.canuni)
        LET w_saluni  = (w_saluni+imp1.canuni)
       WHEN 0 -- Abonos
        LET w_abonou  = imp1.canuni 
        LET w_abotou  = (w_abotou+imp1.canuni)
        LET w_saluni  = (w_saluni-imp1.canuni)
      END CASE

      -- Imprimiendo movimientos  
      IF (LENGTH(imp1.numrf2)>0) THEN
         LET wdocto = imp1.lnktra USING "<<<<<<<<<<","/",
                      imp1.numrf1 CLIPPED,"/",
                      imp1.numrf2 CLIPPED  
      ELSE
         LET wdocto = imp1.lnktra USING "<<<<<<<<<<","/",
                      imp1.numrf1 CLIPPED
      END IF 

      -- Verificando tipo de movimiento 
      CASE (imp1.tipope)
       WHEN 1 -- Cargos
        IF (LENGTH(imp1.observ)>0) THEN
           LET wobsrv = imp1.nomprv CLIPPED," / ",imp1.observ CLIPPED 
        ELSE
           LET wobsrv = imp1.nomprv CLIPPED
        END IF 
       WHEN 0 -- Abonos 
        IF (LENGTH(imp1.observ)>0) THEN
           LET wobsrv = imp1.nomcli CLIPPED," / ",imp1.observ CLIPPED 
        ELSE
           LET wobsrv = imp1.nomcli CLIPPED
        END IF 
      END CASE 

      -- Depurando observaciones (elimnando enters, returns y tabs)
      LET wobsrv = librut001_replace(wobsrv,"\r"," ",10)
      LET wobsrv = librut001_replace(wobsrv,"\n"," ",10)
      LET wobsrv = librut001_replace(wobsrv,"\t"," ",10)

      PRINT imp1.fecemi               USING "dd/mmm/yyyy"  ,2 SPACES,
            imp1.abrmov                                    ,2 SPACES,
            wdocto                                         ,1 SPACES,
            wobsrv[1,50]                                   ,2 SPACES,
            w_cargou                  USING "##,###,##&.&&",1 SPACES,
            w_abonou                  USING "##,###,##&.&&",1 SPACES,
            w_saluni                  USING "--,---,--&.&&",2 SPACES,
            imp1.userid                                    ,1 SPACES,
            EXTEND(imp1.horsis,HOUR TO MINUTE) 

      -- Verifica si tiene descripcion
      IF (LENGTH(wobsrv)>51) THEN
         PRINT COLUMN 43,wobsrv[51,100] 
      END IF
      IF (LENGTH(wobsrv)>100) THEN
         PRINT COLUMN 43,wobsrv[101,150]
      END IF
   END IF

  AFTER GROUP OF imp1.cditem
   -- Verificando si hubieron movimiento 
   IF haymov THEN
    PRINT linea 

    -- Totalizando
    PRINT COLUMN   1,"Existencia del Producto ...",
          COLUMN  95,w_cartou                  USING "##,###,##&.&&",1 SPACES,
                     w_abotou                  USING "##,###,##&.&&",1 SPACES,
                     w_saluni                  USING "--,---,--&.&&"
   ELSE
      PRINT COLUMN 1,fnt.twd CLIPPED,
                      "*** N O  E X I S T E N  M O V I M I E N T O S  R E G I S T R A D O S ***",
                      fnt.fwd CLIPPED 
      PRINT linea 
   END IF 

  ON LAST ROW
   -- Imprimiendo filtros
   SKIP 1 LINES
   PRINT fnt.twd CLIPPED,"FILTROS",fnt.fwd CLIPPED
   IF (LENGTH(fcodcat)>0) THEN
      PRINT "Categoria    : ",fcodcat
   END IF
   IF (LENGTH(fsubcat)>0) THEN
      PRINT "Subcategoria : ",fsubcat
   END IF
   IF (LENGTH(fcditem)>0) THEN
      PRINT "Producto     : ",fcditem
   END IF
END REPORT
