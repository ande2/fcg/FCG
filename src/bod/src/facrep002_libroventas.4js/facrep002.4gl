{ 
Programo : facrep002.4gl 
Objetivo : Reporte de libro de ventas
}

DATABASE storepos 

-- Definicion de variables globales 

TYPE datosreporte RECORD 
       numpos        LIKE fac_mtransac.numpos,
       nompos        LIKE fac_puntovta.nompos, 
       tipdoc        LIKE fac_mtransac.tipdoc,
       nomdoc        CHAR(40),
       lnktdc        LIKE fac_mtransac.lnktdc,
       nserie        LIKE fac_mtransac.nserie,
       fecemi        LIKE fac_mtransac.fecemi,
       minimo        INTEGER,
       maximo        INTEGER,
       totgra        DEC(12,2),
       totexe        DEC(12,2),
       totisv        DEC(12,2),
       totdoc        DEC(12,2)
END RECORD 

DEFINE w_datos       RECORD
        numpos       LIKE fac_vicortes.numpos,
        tipdoc       LIKE fac_tipodocs.tipdoc,
        fecini       DATE,
        fecfin       DATE
       END RECORD,
       existe        SMALLINT,
       lg,nlines     SMALLINT, 
       tituloreporte STRING, 
       filename      STRING,
       pipeline      STRING,
       np            CHAR(3), 
       s,d           CHAR(1)

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rlibroventas")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL facrep002_libroventas()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION facrep002_libroventas()
 DEFINE wpais      VARCHAR(255),
        loop,res   SMALLINT, 
        w          ui.Window

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep002a AT 5,2
  WITH FORM "facrep002a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("facrep002",wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/LibroVentas.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox
  CALL librut003_CbxPuntosVenta() 

  -- Inicializando datos
  INITIALIZE w_datos.* TO NULL
  CLEAR FORM

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline TO NULL
   LET s = 1 SPACES
   LET d = "~"

   -- Construyendo busqueda
   INPUT BY NAME w_datos.numpos,
                 w_datos.tipdoc, 
                 w_datos.fecini,
                 w_datos.fecfin 
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Verificando si filtros estan completos
     IF NOT facrep002_FiltrosCompletos() THEN 
        NEXT FIELD numpos
     END IF 
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 

     -- Verificando si filtros estan completos
     IF NOT facrep002_FiltrosCompletos() THEN 
        NEXT FIELD numpos 
     END IF 
     EXIT INPUT 

    ON ACTION excel
     -- Asignando dispositivo
     LET pipeline = "excel"
     LET s        = ASCII(9)
     LET d        = ASCII(9)

     -- Verificando si filtros estan completos
     IF NOT facrep002_FiltrosCompletos() THEN 
        NEXT FIELD numpos 
     END IF 
     EXIT INPUT 

    BEFORE FIELD tipdoc
     -- Llenando combo de tipos de documento x punto de venta
     CALL librut003_CbxTiposDocumentoLibroVentas() 

    AFTER INPUT 
     -- Verificando datos
     IF w_datos.numpos IS NULL OR 
        w_datos.tipdoc IS NULL OR 
        w_datos.fecini IS NULL OR 
        w_datos.fecfin IS NULL OR 
        pipeline IS NULL THEN
        NEXT FIELD numpos
     END IF
    
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Generando el reporte
   CALL facrep002_genlibro()
  END WHILE
 CLOSE WINDOW wrep002a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION facrep002_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE
 IF w_datos.numpos IS NULL OR
    w_datos.tipdoc IS NULL OR 
    w_datos.fecini IS NULL OR
    w_datos.fecfin IS NULL THEN
    CALL fgl_winmessage(
    "Atencion",
    "Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte de libro de ventas 

FUNCTION facrep002_genlibro()
 DEFINE w_mae_fac datosreporte, 
        s_query   STRING 

 -- Definiendo parametros del reporte
 LET lg     = 130 
 LET nlines = 72
 LET np     = "8" 

 -- Preparando condiciones del reporte 
 LET s_query = 
      "SELECT x.numpos,x.nompos,x.tipdoc,x.nomdoc,x.lnktdc,x.nserie,x.fecemi,",
             "MIN(x.numdoc),MAX(x.numdoc),SUM(x.totgra),SUM(x.totexe),",
             "SUM(x.totisv),SUM(x.totdoc) ",
        "FROM vis_libroventas x ",
        "WHERE x.numpos  = ",w_datos.numpos,
         " AND x.tipdoc  = ",w_datos.tipdoc,
         " AND x.fecemi >= '",w_datos.fecini,"' ",
         " AND x.fecemi <= '",w_datos.fecfin,"' ",
         " GROUP BY 1,2,3,4,5,6,7 ",
         " ORDER BY x.numpos,x.tipdoc,x.nserie,x.fecemi" 

 -- Seleccionando datos del reporte
 ERROR "Atencion: seleccionando datos ... por favor espere ..."

 PREPARE s_libro FROM s_query
 DECLARE c_libro CURSOR FOR s_libro
 LET existe = FALSE
 FOREACH c_libro INTO w_mae_fac.*
  -- Iniciando reporte
  IF NOT existe THEN
     -- Iniciando la impresion
     START REPORT facrep002_implibrovta TO filename
     LET existe = TRUE
  END IF

  -- Llenado el reporte
  OUTPUT TO REPORT facrep002_implibrovta(w_mae_fac.*) 
 END FOREACH
 CLOSE c_libro
 FREE  c_libro

 IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT facrep002_implibrovta   

     -- Transfiriendo reporte a excel
    IF pipeline = "excel" THEN
      CALL librut005_excel(filename)
    ELSE
    -- Enviando reporte al destino seleccionado
    CALL librut001_sendreport
    (filename,pipeline,tituloreporte,
     "--noline-numbers "||
     "--nofooter "||
     "--font-size 8 "||
     "--page-width 842 --page-height 595 "||
     "--left-margin 55 --right-margin 25 "||
     "--top-margin 35 --bottom-margin 45 "||
     "--title Facturacion")
    END IF 

    ERROR "" 
    CALL fgl_winmessage(" Atencion","Reporte Emitido.","information") 
 ELSE
    ERROR "" 
    display "QUERY ",s_query 
    CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
 END IF 
END FUNCTION 

-- Subrutina para imprimir el reporte 

REPORT facrep002_implibrovta(imp1)
 DEFINE imp1          datosreporte,
        wdocumentos   CHAR(32),
        linea         CHAR(130), 
        exis,i,col    SMALLINT,
        lfn           SMALLINT,
        periodo       STRING 

  OUTPUT LEFT   MARGIN 2 
         PAGE   LENGTH nlines 
         TOP    MARGIN 2
         BOTTOM MARGIN 2 

  FORMAT 
   PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "______________________________"

    -- Ajustando columna 
    LET lfn = lg-20  

    -- Periodo de fechas
    LET periodo = "PERIODO DEL ",w_datos.fecini," AL ",w_datos.fecfin

    -- Imprimiendo encabezado 
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Facturacion",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
	  COLUMN lfn,PAGENO USING "Pagina: <<"

    LET col = librut001_centrado(periodo,lg) 
    PRINT COLUMN   1,"Facrep002",
          COLUMN col,periodo CLIPPED, 
          COLUMN lfn,"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET col = librut001_centrado(imp1.nompos,lg) 
    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,imp1.nompos CLIPPED, 
          COLUMN lfn,"Hora  : ",TIME 

    PRINT linea 
    PRINT "FECHA          RANGO DOCUMENTOS                       VENTAS       ",
          "      VENTAS             TOTAL          TOTAL VENTAS"
    PRINT "EMISION                                               GRAVADAS     ", 
          "      EXCENTAS           IMPUESTO"
    PRINT linea
    SKIP 1 LINES

   BEFORE GROUP OF imp1.numpos 
    IF (pipeline="excel") THEN
       PRINT "LIBRO DE VENTAS",s
       PRINT periodo CLIPPED,s
       PRINT imp1.nompos CLIPPED,s
       PRINT s
       PRINT "Fecha",s,"Documentos",s,"Ventas",s,"Ventas",s,"Total",s,"Total"
       PRINT "Emision",s,s,"Gravadas",s,"Excentas",s,"Impuesto",s,"Ventas"
       PRINT s
    END IF

   BEFORE GROUP OF imp1.nserie 
    -- Imprimiendo tipo de documento
    PRINT imp1.nomdoc CLIPPED," Serie [ ",imp1.nserie CLIPPED," ]",s
    
   ON EVERY ROW
    -- Rango de documentos 
    LET wdocumentos = "DEL ",imp1.minimo USING "&&&&&&&&",
                      " AL ",imp1.maximo USING "&&&&&&&&" 

    -- Imprimiendo datos 
    PRINT imp1.fecemi                                               ,4 SPACES,s,
          wdocumentos                                               ,s,
          imp1.totgra                         USING "###,###,##&.&&",4 SPACES,s,
          imp1.totexe                         USING "###,###,##&.&&",4 SPACES,s,
          imp1.totisv                         USING "###,###,##&.&&",4 SPACES,s,
          imp1.totdoc                         USING "###,###,##&.&&",s

   AFTER GROUP OF imp1.nserie 
    -- Totalizando
    PRINT COLUMN   1,"Total Serie [ ",imp1.nserie CLIPPED," ]",s,s,
          COLUMN  49,GROUP SUM(imp1.totgra)   USING "###,###,##&.&&",4 SPACES,s,
                     GROUP SUM(imp1.totexe)   USING "###,###,##&.&&",4 SPACES,s,
                     GROUP SUM(imp1.totisv)   USING "###,###,##&.&&",4 SPACES,s,
                     GROUP SUM(imp1.totdoc)   USING "###,###,##&.&&",s
    PRINT s

   ON LAST ROW
    -- Totalizando reporte
    PRINT "TOTAL REPORTE --> "                   ,28 SPACES,s,s, 
          SUM(imp1.totgra) USING "###,###,##&.&&", 4 SPACES,s,
          SUM(imp1.totexe) USING "###,###,##&.&&", 4 SPACES,s,
          SUM(imp1.totisv) USING "###,###,##&.&&", 4 SPACES,s,
          SUM(imp1.totdoc) USING "###,###,##&.&&",s
END REPORT
