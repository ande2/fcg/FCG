
  drop view vis_libroventas;

create view vis_libroventas
(numpos,nompos,tipdoc,nomdoc,lnktdc,nserie,fecemi,numdoc,totgra,totexe,totisv,totdoc) as

select a.numpos,c.nompos,a.tipdoc,b.nomdoc,a.lnktdc,a.nserie,a.fecemi,a.numdoc,

(0),

(select nvl(sum(b.totpro),0) from fac_dtransac b
where b.lnktra = a.lnktra),

(0),

(select nvl(sum(totpro),0) from fac_dtransac b
where b.lnktra = a.lnktra)

from fac_mtransac a,fac_tipodocs b,fac_puntovta c
where a.estado = "V" 
  and c.numpos = a.numpos
  and b.tipdoc = a.tipdoc

union all

select a.numpos,c.nompos,a.tipdoc,b.nomdoc,a.lnktdc,a.nserie,a.fecemi,a.numdoc,0,0,0,0
from fac_mtransac a,fac_tipodocs b,fac_puntovta c
where a.estado = "A" 
  and c.numpos = a.numpos
  and b.tipdoc = a.tipdoc;

grant select on vis_libroventas to public;
