{ 
glbglo045.4gl
Mynor Ramirez
Mantenimiento de transportes 
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae045"
DEFINE w_mae_pro      RECORD LIKE glb_delivery.*,
       v_transportes DYNAMIC ARRAY OF RECORD
        tcoddlv       LIKE glb_delivery.coddlv,
        tnomdlv       LIKE glb_delivery.nomdlv, 
        tendrec       CHAR(1) 
       END RECORD,
       username       VARCHAR(15)
END GLOBALS
