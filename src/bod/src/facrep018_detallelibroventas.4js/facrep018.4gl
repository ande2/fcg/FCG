{ 
Programo : facrep018.4gl 
Objetivo : Reporte de detalle de libro de ventas
}

DATABASE storepos 

-- Definicion de variables globales 

CONSTANT apostrofe="'" 
DEFINE datosreporte RECORD LIKE vis_detallelibroventas.*,
       w_datos       RECORD
        numpos       LIKE fac_vicortes.numpos,
        tipdoc       LIKE fac_tipodocs.tipdoc,
        fecini       DATE,
        fecfin       DATE
       END RECORD,
       existe        SMALLINT,
       nlines,lg     SMALLINT,
       np            CHAR(3), 
       tituloreporte STRING, 
       filename      STRING,
       pipeline      STRING,
       s,d           CHAR(1)

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rdetallelibroventas")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL facrep018_DetalleLibroventas()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION facrep018_DetalleLibroventas()
 DEFINE wpais      VARCHAR(255),
        loop,res   SMALLINT, 
        w          ui.Window

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep002a AT 5,2
  WITH FORM "facrep018a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("facrep018",wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/DetalleLibroVentas.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox
  CALL librut003_CbxPuntosVenta() 

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   LET s = 1 SPACES
   LET d = "~"
   CLEAR FORM
   LET w_datos.tipdoc = 1
   LET w_datos.fecini = TODAY
   LET w_datos.fecfin = TODAY 

   -- Construyendo busqueda
   INPUT BY NAME w_datos.numpos,
                 w_datos.tipdoc, 
                 w_datos.fecini,
                 w_datos.fecfin 
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Verificando si filtros estan completos
     IF NOT facrep018_FiltrosCompletos() THEN 
        NEXT FIELD numpos
     END IF 
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 

     -- Verificando si filtros estan completos
     IF NOT facrep018_FiltrosCompletos() THEN 
        NEXT FIELD numpos 
     END IF 
     EXIT INPUT 

    ON ACTION excel
     -- Asignando dispositivo
     LET pipeline = "excel"
     LET s        = ASCII(9)
     LET d        = ASCII(9)

     -- Verificando si filtros estan completos
     IF NOT facrep018_FiltrosCompletos() THEN 
        NEXT FIELD numpos 
     END IF 
     EXIT INPUT 

    BEFORE FIELD tipdoc
     -- Llenando combo de tipos de documento x punto de venta
     CALL librut003_CbxTiposDocumentoLibroVentas() 

    AFTER INPUT 
     -- Verificando datos
     IF w_datos.numpos IS NULL OR 
        w_datos.tipdoc IS NULL OR 
        w_datos.fecini IS NULL OR 
        w_datos.fecfin IS NULL OR 
        pipeline IS NULL THEN
        NEXT FIELD numpos
     END IF
    
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Generando el reporte
   CALL facrep018_GeneraDetalleLibroVentas()
  END WHILE
 CLOSE WINDOW wrep002a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION facrep018_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE
 IF w_datos.numpos IS NULL OR
    w_datos.tipdoc IS NULL OR 
    w_datos.fecini IS NULL OR
    w_datos.fecfin IS NULL THEN
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte de libro de ventas 

FUNCTION facrep018_GeneraDetalleLibroVentas()
 DEFINE w_mae_fac RECORD LIKE vis_detallelibroventas.*,
      
        s_query   STRING 

 -- Definiendo parametros del reporte
 LET lg     = 163 
 LET nlines = 50 

 -- Preparando condiciones del reporte 
 LET s_query = 
  "SELECT x.numpos,x.nompos,x.tipdoc,x.nomdoc,x.lnktdc,x.nserie,x.fecemi,",
         "x.nomcli,x.numnit,x.numdoc,x.totgra,x.totexe,x.totisv,x.totdoc ",
    "FROM vis_detallelibroventas x ",
    "WHERE x.numpos  = ",w_datos.numpos,
     " AND x.tipdoc  = ",w_datos.tipdoc,
     " AND x.fecemi >= '",w_datos.fecini,"' ",
     " AND x.fecemi <= '",w_datos.fecfin,"' ",
     " ORDER BY x.numpos,x.tipdoc,x.nserie,x.fecemi,x.numdoc" 

 -- Seleccionando datos del reporte
 ERROR "Atencion: seleccionando datos ... por favor espere ..."

 PREPARE s_libro FROM s_query
 DECLARE c_libro CURSOR FOR s_libro
 LET existe  = FALSE
 FOREACH c_libro INTO w_mae_fac.*
  -- Iniciando reporte
  IF NOT existe THEN
     -- Iniciando la impresion
     START REPORT facrep018_ImprimeReporte TO filename
     LET existe = TRUE
  END IF

  -- Llenado el reporte
  OUTPUT TO REPORT facrep018_ImprimeReporte(w_mae_fac.*) 
 END FOREACH
 CLOSE c_libro
 FREE  c_libro

 IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT facrep018_ImprimeReporte   

     -- Transfiriendo reporte a excel
    IF pipeline = "excel" THEN
      CALL librut005_excel(filename)
    ELSE
     -- Enviando reporte al destino seleccionado
     CALL librut001_sendreport
     (filename,pipeline,tituloreporte,
     "--noline-numbers "||
     "--nofooter "||
     "--font-size 8 "||
     "--page-width 842 --page-height 595 "||
     "--left-margin 55 --right-margin 25 "||
     "--top-margin 35 --bottom-margin 45 "||
     "--title Facturacion")
    END IF 

    ERROR "" 
    CALL fgl_winmessage(" Atencion","Reporte Emitido.","information") 
 ELSE
    ERROR "" 
    CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
 END IF 
END FUNCTION 

-- Subrutina para imprimir el reporte 

REPORT facrep018_ImprimeReporte(imp1)
 DEFINE imp1          RECORD LIKE vis_detallelibroventas.*,
        linea         CHAR(163), 
        exis,i,col    SMALLINT,
        lfn           SMALLINT,
        periodo       STRING 

  OUTPUT LEFT   MARGIN 0 
         TOP    MARGIN 0
         BOTTOM MARGIN 0 
         PAGE   LENGTH nlines 

  FORMAT 
   PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________",
                "_______________"

    -- Columna titulos 
    LET lfn = lg-25  

    -- Imprimiendo encabezado 
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Facturacion",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
	  COLUMN lfn,PAGENO USING "Pagina: <<"

    LET periodo = "PERIODO DEL ",w_datos.fecini," AL ",w_datos.fecfin
    LET col = librut001_centrado(periodo,lg) 
    PRINT COLUMN   1,"Facrep002",
          COLUMN col,periodo CLIPPED, 
          COLUMN lfn,"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET col = librut001_centrado(imp1.nompos,lg) 
    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,imp1.nompos CLIPPED, 
          COLUMN lfn,"Hora  : ",TIME 

    PRINT linea 
    PRINT "FECHA       NOMBRE DEL CLIENTE                      ",
          "NUMERO NIT          NUMERO      NUMERO         VENTAS",
          "           VENTAS          TOTAL       TOTAL VENTAS"
    PRINT "EMISION                                             ",
          "                    SERIE       DOCUMENTO      GRAVADAS",
          "        EXCENTAS        IMPUESTO"
    PRINT linea

   BEFORE GROUP OF imp1.numpos 
    IF (pipeline="excel") THEN
       PRINT "LIBRO DE VENTAS DETALLADO",s
       PRINT periodo CLIPPED,s
       PRINT imp1.nompos CLIPPED ,s
       PRINT s
       PRINT "Fecha",s,"Nombre del Cliente",s,"Numero NIT",s,"Numero",s,"Numero",s,
             "Ventas",s,"Ventas",s,"Total",s,"Total"
       PRINT "Emision",s,s,s,"Serie",s,"Documento",s,"Gravadas",s,"Excentas",s,
             "Impuesto",s,"Ventas"
       PRINT s
    END IF

   BEFORE GROUP OF imp1.nserie 
    -- Imprimiendo tipo de documento
    PRINT imp1.nomdoc CLIPPED," Serie [ ",imp1.nserie CLIPPED," ]",s
    
   ON EVERY ROW
    -- Imprimiendo datos 
    IF pipeline!="excel" THEN
     PRINT imp1.fecemi                                              ,1 SPACES,s,
           imp1.nomcli[1,38]                                        ,1 SPACES,s, 
           imp1.numnit[1,18]                                        ,1 SPACES,s,
           imp1.nserie                                              ,1 SPACES,s,
           imp1.numdoc                         USING "&&&&&&&&"     ,1 SPACES,s, 
           imp1.totgra                         USING "##,###,##&.&&",2 SPACES,s,
           imp1.totexe                         USING "##,###,##&.&&",2 SPACES,s,
           imp1.totisv                         USING "##,###,##&.&&",2 SPACES,s,
           imp1.totdoc                         USING "##,###,##&.&&",s
    ELSE
     PRINT imp1.fecemi                                              ,s,
           imp1.nomcli[1,38]                                        ,s,apostrofe, 
           imp1.numnit[1,18]                                        ,s,
           imp1.nserie                                              ,s,apostrofe,
           imp1.numdoc                         USING "&&&&&&&&"     ,s,
           imp1.totgra                         USING "##,###,##&.&&",s,
           imp1.totexe                         USING "##,###,##&.&&",s,
           imp1.totisv                         USING "##,###,##&.&&",s,
           imp1.totdoc                         USING "##,###,##&.&&",s
    END IF 

   AFTER GROUP OF imp1.nserie 
    -- Totalizando
    IF pipeline!="excel" THEN
     PRINT COLUMN   1,"Total Serie [ ",imp1.nserie CLIPPED," ]",s,s,
           COLUMN  95,GROUP SUM(imp1.totgra)   USING "##,###,##&.&&",2 SPACES,s,
                      GROUP SUM(imp1.totexe)   USING "##,###,##&.&&",2 SPACES,s,
                      GROUP SUM(imp1.totisv)   USING "##,###,##&.&&",2 SPACES,s,
                      GROUP SUM(imp1.totdoc)   USING "##,###,##&.&&",s
    ELSE
     PRINT "Total Serie [ ",imp1.nserie CLIPPED," ]",s,s,s,s,s,
                      GROUP SUM(imp1.totgra)   USING "##,###,##&.&&",s,
                      GROUP SUM(imp1.totexe)   USING "##,###,##&.&&",s,
                      GROUP SUM(imp1.totisv)   USING "##,###,##&.&&",s,
                      GROUP SUM(imp1.totdoc)   USING "##,###,##&.&&",s
    END IF 
    PRINT s

   ON LAST ROW
    -- Totalizando reporte
    IF pipeline!="excel" THEN
     PRINT "TOTAL REPORTE --> "                  ,74 SPACES,s,s, 
           SUM(imp1.totgra) USING "##,###,##&.&&", 2 SPACES,s,
           SUM(imp1.totexe) USING "##,###,##&.&&", 2 SPACES,s,
           SUM(imp1.totisv) USING "##,###,##&.&&", 2 SPACES,s,
           SUM(imp1.totdoc) USING "##,###,##&.&&",s
    ELSE
     PRINT "TOTAL REPORTE --> "                  ,s,s,s,s,s,
           SUM(imp1.totgra) USING "##,###,##&.&&",s,
           SUM(imp1.totexe) USING "##,###,##&.&&",s,
           SUM(imp1.totisv) USING "##,###,##&.&&",s,
           SUM(imp1.totdoc) USING "##,###,##&.&&",s
    END IF 
END REPORT
