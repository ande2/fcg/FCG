
  drop view vis_detallelibroventas;

create view vis_detallelibroventas
(numpos,nompos,tipdoc,nomdoc,lnktdc,nserie,fecemi,
 nomcli,numnit,numdoc,totgra,totexe,totisv,totdoc) as

select a.numpos,c.nompos,a.tipdoc,b.nomdoc,a.lnktdc,
       a.nserie,a.fecemi,a.nomcli,a.numnit,a.numdoc,
       a.totgra,a.totexe,a.totisv,a.totcta
from pos_mtransac a,fac_tipodocs b,fac_puntovta c
where a.estado = 1
  and c.numpos = a.numpos
  and b.tipdoc = a.tipdoc

union all

select a.numpos,c.nompos,a.tipdoc,b.nomdoc,a.lnktdc,
       a.nserie,a.fecemi,a.nomcli,a.numnit,a.numdoc,0,0,0,0
from pos_mtransac a,fac_tipodocs b,fac_puntovta c
where a.estado = 0
  and c.numpos = a.numpos
  and b.tipdoc = a.tipdoc;

grant select on vis_detallelibroventas to public;
