{
Programo : Mynor Ramirez 
Objetivo : Impresion de comanda por factura 
}

-- Definicion de variables globales  

GLOBALS "facglb003.4gl" 
DEFINE existe          SMALLINT, 
       filename        CHAR(90),
       pipeline        CHAR(90)

-- Subrutina para imprimir una comanda

FUNCTION facrpt003b_GeneraComanda(reimpresion,imp3)
 DEFINE imp3        RECORD
         numesa     LIKE sre_mordenes.numesa,
         numdoc     LIKE pos_mtransac.numdoc,
         fecemi     LIKE pos_mtransac.fecemi,
         descrp     LIKE pos_mtransac.descrp,
         lnktra     LIKE pos_mtransac.lnktra,
         lnkord     LIKE sre_mordenes.lnkord,
         userid     LIKE sre_mordenes.userid
        END RECORD, 
        reimpresion SMALLINT,
        i,numcom    SMALLINT 

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/ComandaPOS.spl"

 -- Obteniendo columna inicial para impresora
 LET ColIniImpCoc = 1

 -- Asignando impresora de cocina por punto de venta 
 SELECT a.impcoc
  INTO  w_mae_pos.impcoc 
  FROM  fac_puntovta a
  WHERE a.numpos = w_mae_tra.numpos
  IF (status=NOTFOUND) THEN
     LET pipeline = "local"
  ELSE
     LET pipeline = w_mae_pos.impcoc 
  END IF

 IF pipeline != "Ninguna" THEN 
  -- Iniciando reporte
  START REPORT facrpt003b_ImprimirComanda TO filename 
   OUTPUT TO REPORT facrpt003b_ImprimirComanda(1,reimpresion,imp3.*)
  FINISH REPORT facrpt003b_ImprimirComanda 

  -- Obteniendo parametro de numero de comandas impresas
  LET numcom = 1

  -- Imprimiendo el reporte
  FOR i = 1 TO numcom
   CALL librut001_sendreport(filename,pipeline,"","")
  END FOR
 END IF 
END FUNCTION 

-- Subrutinar para generar la impresion de la comanda 

REPORT facrpt003b_ImprimirComanda(numero,reimpresion,imp3) 
 DEFINE imp1        RECORD LIKE fac_formaimp.*,
        imp2        RECORD
         nomrec     CHAR(34),
         cantid     LIKE sre_dplaspre.cantid
        END RECORD, 
        imp3        RECORD
         numesa     LIKE sre_mordenes.numesa,
         numdoc     LIKE pos_mtransac.numdoc,
         fecemi     LIKE pos_mtransac.fecemi,
         descrp     LIKE pos_mtransac.descrp,
         lnktra     LIKE pos_mtransac.lnktra,
         lnkord     LIKE sre_mordenes.lnkord,
         userid     LIKE sre_mordenes.userid
        END RECORD, 
        xnomcaj     CHAR(40), 
        xnompos     CHAR(40), 
        i,col,coi   SMALLINT, 
        lg,totpla   SMALLINT, 
        reimpresion SMALLINT,
        xnumdoc     CHAR(40), 
        cortepapel  CHAR(10), 
        xnompla     CHAR(34), 
        qrytext     STRING, 
        numero      INT 

  OUTPUT LEFT   MARGIN 0 
         PAGE   LENGTH 5 
         TOP    MARGIN 0 
         BOTTOM MARGIN 0

 FORMAT 
  BEFORE GROUP OF numero 
   -- Inicializando impresora
   LET coi = ColIniImpCoc 
   LET lg  = 42+coi
   LET cortepapel = ASCII 29,ASCII 86,"0"

   -- Obteniendo nombre del cajero
   INITIALIZE xnomcaj TO NULL 
   SELECT a.nomusr 
    INTO  xnomcaj 
    FROM  glb_usuarios a
    WHERE a.userid = imp3.userid 

   -- Punto de Venta
   LET xnompos = w_mae_pos.nompos  
   LET col = librut001_centrado(xnompos,lg) 
   PRINT COLUMN col,xnompos CLIPPED

   -- Numero de Mesa    
   IF (imp3.numesa>0) THEN 
      LET xnumdoc = "MESA # ",imp3.numesa USING "<<<<<<<<"
      LET col = librut001_centrado(xnumdoc,lg) 
      PRINT COLUMN col,xnumdoc CLIPPED 
   END IF 

   -- Numero de Comanda
   LET xnumdoc = "COMANDA # ",imp3.numdoc USING "&&&&&&&&"
   LET col = librut001_centrado(xnumdoc,lg) 
   PRINT COLUMN col,xnumdoc CLIPPED 

   -- Destino 
   IF w_mae_ord.llevar=1 THEN
      LET col = librut001_centrado("Para Llevar",lg) 
      PRINT COLUMN col,"Para Llevar" 
   ELSE
      LET col = librut001_centrado("Comer Aqui",lg) 
      PRINT COLUMN col,"Comer Aqui" 
   END IF 

   -- Detalle de Platos Servidos 
   SKIP 1 LINES 
   PRINT COLUMN coi,"Detalle de Platos Servidos"
   PRINT COLUMN coi,"________________________________________"
   PRINT COLUMN coi,"Cant Plato                              "
   PRINT COLUMN coi,"________________________________________"
 
  ON EVERY ROW
   -- Imprimiendo detalle de platos con precio 
   LET totpla = 0
   FOR i = 1 TO totlin
    IF v_cuenta[i].nomrec IS NULL OR 
       v_cuenta[i].cantid IS NULL OR 
       v_cuenta[i].totrec IS NULL THEN 
       CONTINUE FOR
    END IF 

    LET totpla  = totpla+1
    LET xnompla = v_cuenta[i].nomrec 
    PRINT COLUMN coi,v_cuenta[i].cantid  USING "##&",2 SPACES,
                     xnompla                       
   END FOR   

   -- Imprimiendo detalle de platos sin precio
   IF (imp3.lnktra>0) THEN 
    LET qrytext = 
     "SELECT z.nomrec,y.cantid ",
      "FROM  sre_dplaspre y,sre_mordenes x,sre_mrecetas z ",
      "WHERE x.lnkord = y.lnkord ",
       " AND x.lnktra = ",imp3.lnktra,
       " AND z.lnkrec = y.lnkrec ",
       " ORDER BY y.correl" 
   ELSE
    LET qrytext = 
     "SELECT z.nomrec,y.cantid ",
      "FROM  sre_dplaspre y,sre_mordenes x,sre_mrecetas z ",
      "WHERE x.lnkord = y.lnkord ",
       " AND x.lnkord = ",imp3.lnkord,
       " AND z.lnkrec = y.lnkrec ",
       " ORDER BY y.correl" 
   END IF 

   PREPARE p_psp FROM qrytext 
   DECLARE c_psp CURSOR FOR p_psp 
   FOREACH c_psp INTO imp2.*
     LET totpla = totpla+1
     --PRINT COLUMN coi,imp2.cantid  USING "##&",2 SPACES, 
     PRINT COLUMN coi,5 SPACES, 
                      imp2.nomrec           
   END FOREACH
   CLOSE c_psp
   FREE  c_psp

  ON LAST ROW
   -- Imprimiendo totales 
   PRINT COLUMN coi,"________________________________________"
   PRINT COLUMN coi,"Total Platos [ ",totpla USING "<<<"," ]" 

   -- Verificnado impresion de comentarios
   IF (LENGTH(imp3.descrp)>0) THEN
     -- Imprimiendo comentarios 
     SKIP 1 LINES 
     PRINT COLUMN coi,"Comentarios:" 
     PRINT COLUMN coi,imp3.descrp[1,40]  

     IF (LENGTH(imp3.descrp)>40) THEN
        PRINT COLUMN coi,imp3.descrp[41,80]  
     END IF 
     IF (LENGTH(imp3.descrp)>80) THEN
        PRINT COLUMN coi,imp3.descrp[81,120]  
     END IF 
     IF (LENGTH(imp3.descrp)>120) THEN
        PRINT COLUMN coi,imp3.descrp[121,160]  
     END IF 
     IF (LENGTH(imp3.descrp)>140) THEN
        PRINT COLUMN coi,imp3.descrp[141,200]  
     END IF 
   END IF 

   -- Fecha y cajero 
   SKIP 1 LINES 
   PRINT COLUMN coi,"Fecha:   ",imp3.fecemi," ",TIME
   PRINT COLUMN coi,"Cajero:  ",xnomcaj CLIPPED

   -- Imprimiendo pie 
   FOR i = 1 TO 6 
    PRINT " "
   END FOR 
   PRINT cortepapel CLIPPED 
END REPORT 
