IMPORT FGL wc_touchpad

FUNCTION DEMOPOS()
DEFINE l_touchpad RECORD
    touchpad STRING
END RECORD
DEFINE l_mode STRING
CALL ui.Interface.loadStyles("wc_touchpad_test")
--PEN WINDOW w WITH FORM "wc_touchpad_test" ATTRIBUTES(STYLE="touchpad")

        INPUT BY NAME l_touchpad.* ATTRIBUTES(UNBUFFERED, ACCEPT=FALSE, CANCEL=FALSE)
            BEFORE INPUT
             --numerical keypad
             CALL wc_touchpad.init_grid(3,4)
             CALL wc_touchpad.text_add2grid(1,1,1,1,"Combo 1","7")
             CALL wc_touchpad.text_add2grid(2,1,1,1,"Combo 2","8")
             CALL wc_touchpad.text_add2grid(3,1,1,1,"Combo 3","9")
             CALL wc_touchpad.text_add2grid(1,2,1,1,"Combo 4","4")
             CALL wc_touchpad.text_add2grid(2,2,1,1,"Combo 5","5")
             CALL wc_touchpad.text_add2grid(3,2,1,1,"Combo 6","6")
             CALL wc_touchpad.text_add2grid(1,3,1,1,"Combo 11","1")
             CALL wc_touchpad.text_add2grid(2,3,1,1,"Combo 12","2")
             CALL wc_touchpad.text_add2grid(3,3,1,1,"Combo 13","3")
             CALL wc_touchpad.text_add2grid(1,4,1,1,"Combo 14","minus")
             CALL wc_touchpad.text_add2grid(2,4,1,1,"Combo 15","0")
             CALL wc_touchpad.text_add2grid(3,4,1,1,"Combo 16","decimal")
             CALL wc_touchpad.html_send("formonly.touchpad")
                
            ON ACTION touchpad_clicked ATTRIBUTES(DEFAULTVIEW=NO)
                MESSAGE SFMT("Touchpad touched, value=%1", l_touchpad.touchpad)
            ON ACTION close
                EXIT INPUT
        END INPUT
END FUNCTION

FUNCTION wc_image(l_img)
   DEFINE l_img STRING
   RETURN ui.Interface.filenameToURI(SFMT("img/%1",l_img))
END FUNCTION

