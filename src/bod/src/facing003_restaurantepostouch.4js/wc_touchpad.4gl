-- Programa de librerias tabla touchpad para platos 
-- Mynor Ramirez 

CONSTANT fudge = 0.3 -- Evitar situacion de error de redondeo 

-- Variables globales 
DEFINE touchpad_html base.StringBuffer
DEFINE touchpad_size RECORD
        columns      INTEGER,
        rows         INTEGER
       END RECORD

-- Subrutina para inicializar el touchpad 

FUNCTION init()
 LET touchpad_html = base.StringBuffer.create()
 INITIALIZE touchpad_size.* TO NULL
END FUNCTION

-- Subrutina para inicializar el grid  

FUNCTION init_grid(columns, rows)
 DEFINE columns, rows INTEGER
 CALL init()
 LET touchpad_size.columns = columns
 LET touchpad_size.rows    = rows
END FUNCTION

-- Subrutina para agregar imagenes 
    
FUNCTION image_add(top, left, width, height, image, value)
 DEFINE top, left, width, height DECIMAL(11,2)
 DEFINE image, value STRING

 CALL touchpad_html.append(SFMT("<div style=\"position: absolute; left: %1%%; top: %2%%; height: %3%%; width: %4%%; font-size: 20; \"><img src=\"%5\" height=\"100%%\" width=\"100%%\"  onclick=\"execAction('%6')\" /></div>", left USING "##&.&&", top USING "##&.&&",height USING "##&.&&", width USING "##&.&&", image, value))
END FUNCTION

-- Subrutina para agregar datos al grid 

FUNCTION image_add2grid(col, row, column_span, row_span, image, value)
 DEFINE col, row, column_span, row_span INTEGER
 DEFINE image, value STRING
 DEFINE top, left, width, height DECIMAL(11,2)

 LET left = 100 * (col-1) / touchpad_size.columns
 LET top = 100 * (row-1) / touchpad_size.rows
 LET width = 100 * column_span / touchpad_size.columns + FUDGE
 LET height = 100 * row_span / touchpad_size.rows + FUDGE
 CALL image_add(top, left, width, height, image, value)
END FUNCTION

-- Subrutina para agregar texto 

FUNCTION text_add(top, left, width, height, txt, value)
 DEFINE top, left, width, height DECIMAL(11,2)
 DEFINE txt, value STRING

 CALL touchpad_html.append(SFMT("<div style=\"position: absolute;  display: table; overflow: hidden; padding: 5px; background-color: #ffe6e6; border-width:2px; border-style:groove; border-radius:6px; left: %1%%; top: %2%%; height: %3%%; width: %4%% \"  onclick=\"execAction('%6')\" ><div style=\"text-align: center; vertical-align: middle; display: table-cell; font-size:8; color: #8B0000; font-family: verdana; font-weight:bold; \">%5</div></div>", left USING "##&.&&", top USING "##&.&&",height USING "##&.&&", width USING "##&.&&", txt, value))
END FUNCTION

-- Subrutina para agregar texto al grid 

FUNCTION text_add2grid(col, row, column_span, row_span, txt, value)
 DEFINE col, row, column_span, row_span INTEGER
 DEFINE txt, value STRING
 DEFINE top, left, width, height DECIMAL(11,2)

 LET left = 100 * (col-1) / touchpad_size.columns
 LET top = 100 * (row-1) / touchpad_size.rows
 LET width = 90 * column_span / touchpad_size.columns + FUDGE
 LET height = 90 * row_span / touchpad_size.rows + FUDGE
 CALL text_add(top, left, width, height, txt, value)
END FUNCTION

-- Subrutina para hacer un append al touchpad 

FUNCTION html_append(txt)
 DEFINE txt STRING
 CALL touchpad_html.append(txt)
END FUNCTION

-- Subrutina para enviar datos al webcomponent 

FUNCTION html_send(fieldname)
 DEFINE fieldname STRING
 DEFINE l_result STRING

 CALL ui.interface.frontcall("webcomponent","call",[fieldname,"setById","touchpad_body",touchpad_html.toString()],l_result)
END FUNCTION
