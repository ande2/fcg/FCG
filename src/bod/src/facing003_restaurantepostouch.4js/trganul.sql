

drop   trigger trianulafactura; 
create trigger trianulafactura update of estado on pos_mtransac
referencing new as pos
    for each row
        when ((pos.estado = 0 ))
            (
            update inv_mtransac
               set inv_mtransac.estado = 'A',
                   inv_mtransac.motanl = pos.motanl ,
                   inv_mtransac.usranl = pos.usranl ,
                   inv_mtransac.fecanl = pos.fecanl ,
                   inv_mtransac.horanl = pos.horanl
             where numrf1 = pos.lnktra AND tipmov = 1
             ),
        when ((pos.estado = 1 ))
            (
            update inv_mtransac
               set inv_mtransac.estado = 'V',
                   inv_mtransac.motanl = pos.motanl ,
                   inv_mtransac.usranl = pos.usranl ,
                   inv_mtransac.fecanl = pos.fecanl ,
                   inv_mtransac.horanl = pos.horanl
             where numrf1 = pos.lnktra AND tipmov = 1
            );
