   drop view vis_ordenescomida;
create view vis_ordenescomida
( numpos,numesa,lnkrec,nomrec,cantid,prevta,totrec,totdes)

as

select x.numpos,x.numesa,y.lnkrec,z.nomrec,sum(y.cantid),
       (sum(y.totrec)/sum(y.cantid)),sum(y.totrec),sum(y.totdes) 
 from  sre_mordenes x,sre_dordenes y,sre_mrecetas z
 where x.lnkord = y.lnkord
   and x.estado = 1
   and x.status = 1
   and z.lnkrec = y.lnkrec
 group by 1,2,3,4;

grant select on vis_ordenescomida to public;
