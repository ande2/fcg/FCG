-- Programa de librerias tabla touchpad usada para teclado 
-- Mynor Ramirez 

CONSTANT fudge = 0.3 -- Evitar situacion de error de redondeo 

-- Variables globales 
DEFINE xtouchpad_html base.StringBuffer
DEFINE xtouchpad_size RECORD
        columns       INTEGER,
        rows          INTEGER
       END RECORD

-- Subrutina para inicializar el touchpad 

FUNCTION xinit()
 LET xtouchpad_html = base.StringBuffer.create()
 INITIALIZE xtouchpad_size.* TO NULL
END FUNCTION

-- Subrutina para inicializar el grid  

FUNCTION xinit_grid(columns, rows)
 DEFINE columns, rows INTEGER
 CALL xinit()
 LET xtouchpad_size.columns = columns
 LET xtouchpad_size.rows    = rows
END FUNCTION

-- Subrutina para agregar texto 

FUNCTION xtext_add(top, left, width, height, txt, value)
 DEFINE top, left, width, height DECIMAL(11,2)
 DEFINE txt, value STRING

 CALL xtouchpad_html.append(SFMT("<div style=\"position: absolute;  display: table; overflow: hidden; padding: 5px; background-color: #ffe6e6; border-width:2px; border-style:groove; border-radius:10px; left: %1%%; top: %2%%; height: %3%%; width: %4%% \"  onclick=\"execAction('%6')\" ><div style=\"text-align: center; vertical-align: middle; display: table-cell; font-size:14; color: #8B0000; font-family: verdana; font-weight:bold; \">%5</div></div>", left USING "##&.&&", top USING "##&.&&",height USING "##&.&&", width USING "##&.&&", txt, value))
END FUNCTION

-- Subrutina para agregar texto al grid 

FUNCTION xtext_add2grid(col, row, column_span, row_span, txt, value)
 DEFINE col, row, column_span, row_span INTEGER
 DEFINE txt, value STRING
 DEFINE top, left, width, height DECIMAL(11,2)

 LET left = 100 * (col-1) / xtouchpad_size.columns
 LET top = 100 * (row-1) / xtouchpad_size.rows
 LET width = 90 * column_span / xtouchpad_size.columns + FUDGE
 LET height = 90 * row_span / xtouchpad_size.rows + FUDGE
 CALL xtext_add(top, left, width, height, txt, value)
END FUNCTION

-- Subrutina para hacer un append al touchpad 

FUNCTION xhtml_append(txt)
 DEFINE txt STRING
 CALL xtouchpad_html.append(txt)
END FUNCTION

-- Subrutina para enviar datos al webcomponent 

FUNCTION xhtml_send(fieldname)
 DEFINE fieldname STRING
 DEFINE l_result STRING

 TRY
  CALL ui.interface.frontcall("webcomponent","call",[fieldname,"xsetById",
                              "xtouchpad_body",xtouchpad_html.toString()],l_result)
 CATCH 
  MESSAGE "Error: no puede obtenerse la interface. ", STATUS, " ", err_get(STATUS)
 END TRY 
END FUNCTION
