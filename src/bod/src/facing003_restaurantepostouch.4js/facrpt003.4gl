{
Programo : Mynor Ramirez 
Objetivo : Impresion de facturacion 
}

-- Definicion de variables globales  

GLOBALS "facglb003.4gl" 
DEFINE existe          SMALLINT, 
       filename        CHAR(90),
       pipeline        CHAR(90)

-- Subrutina para imprimir una factura 

FUNCTION facrpt003_GeneraFactura(reimpresion)
 DEFINE reimpresion,i,numfac SMALLINT 

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/FacturaPOS.spl"

 -- Obteniendo columna inicial para impresora
 LET ColIniImpFac = 1 

 -- Asignando impresora de facuracion por punto de venta 
 SELECT a.impfac
  INTO  w_mae_pos.impfac
  FROM  fac_puntovta a
  WHERE a.numpos = w_mae_tra.numpos 
  IF (status=NOTFOUND) THEN 
     LET pipeline = "local" 
  ELSE  
     LET pipeline = w_mae_pos.impfac 
  END IF 

 -- Obteniendo parametro de numero de facturas impresas
 CALL librut001_parametros(10,4)
 RETURNING existe,numfac
 IF NOT existe THEN
    LET numfac = 1
 END IF

 -- Verificando si existe parametro de imprimir doble copia cuando es pago con tarjeta
 IF ImpCopiaDocTarCre=1 THEN
    IF w_mae_tra.tarcre=0 THEN 
       LET numfac = 1
    END IF 
 END IF 

 -- Imprimiendo el reporte
 FOR i = 1 TO numfac

  -- Iniciando reporte
  START REPORT facrpt003_ImprimirFactura TO filename 
   OUTPUT TO REPORT facrpt003_ImprimirFactura(1,reimpresion,i)
  FINISH REPORT facrpt003_ImprimirFactura 

   CALL librut001_sendreport(filename,pipeline,"","")
 END FOR
END FUNCTION 

-- Subrutinar para generar la impresion de la facturacion 

REPORT facrpt003_ImprimirFactura(numero,reimpresion,copia) 
 DEFINE imp1        RECORD LIKE fac_formaimp.*,
        xporisv     LIKE pos_dtransac.porisv,
        xtotisv     LIKE pos_dtransac.totisv, 
        xnomcaj     CHAR(40), 
        xnomela     CHAR(40), 
        xnomdlv     CHAR(40), 
        xnomcli     CHAR(60), 
        xnomdat     CHAR(40), 
        xnompos     CHAR(40), 
        i,col,coi,j SMALLINT, 
        lg,copia    SMALLINT, 
        reimpresion SMALLINT,
        hayfirmas   SMALLINT, 
        xnumdoc     CHAR(40), 
        xtitle1     CHAR(30), 
        xtitisv     CHAR(31), 
        xorden      CHAR(20), 
        worden      CHAR(20), 
        wnomesa     CHAR(40), 
        cortepapel  CHAR(10), 
        abrircaja   CHAR(10), 
        xdescrp     CHAR(40), 
        xtitpla     CHAR(40), 
        exonerado   DEC(14,2),
        importe18   DEC(14,2),
        tasaali0    DEC(14,2),
        wcanletras  STRING,
        texto       STRING, 
        numero      INT 

  OUTPUT LEFT   MARGIN 0 
         PAGE   LENGTH 5 
         TOP    MARGIN 0 
         BOTTOM MARGIN 0

 FORMAT 
  BEFORE GROUP OF numero 
   -- Imprimiendo encabezado
   LET wcanletras = librut001_numtolet(w_mae_tra.totdoc) 

   -- Inicializando impresora
   LET coi = ColIniImpFac
   LET lg  = 42+coi
   LET cortepapel = ASCII 29,ASCII 86,"0"
   LET abrircaja  = ASCII 27,ASCII 112,"0"

   -- Abriendo caja
   PRINT abrircaja CLIPPED

   -- Obteniendo datos del tipo de documento de facturacion
   INITIALIZE imp1.* TO NULL 
   SELECT a.* 
    INTO  imp1.* 
    FROM  fac_formaimp a
    WHERE a.lnktdc = w_mae_tra.lnktdc

   -- Obteniendo nombre del cajero
   INITIALIZE xnomcaj TO NULL 
   SELECT a.nomusr 
    INTO  xnomcaj 
    FROM  glb_usuarios a
    WHERE a.userid = w_mae_tra.userid 

   -- Obteniendo titulo del tipo de documento 
   INITIALIZE xtitle1 TO NULL
   SELECT a.title1 
    INTO  xtitle1
    FROM  fac_tipodocs a 
    WHERE a.tipdoc = w_mae_tra.tipdoc 

   -- Encabezado
   IF LENGTH(imp1.top001) >0 THEN 
      LET col = librut001_centrado(imp1.top001,lg) 
      PRINT COLUMN col,imp1.top001 CLIPPED 
   END IF 
   IF LENGTH(imp1.top002) >0 THEN 
      LET col = librut001_centrado(imp1.top002,lg) 
      PRINT COLUMN col,imp1.top002 CLIPPED 
   END IF 
   IF LENGTH(imp1.top003) >0 THEN 
      LET col = librut001_centrado(imp1.top003,lg) 
      PRINT COLUMN col,imp1.top003 CLIPPED 
   END IF 
   IF LENGTH(imp1.top004) >0 THEN 
      LET col = librut001_centrado(imp1.top004,lg) 
      PRINT COLUMN col,imp1.top004 CLIPPED 
   END IF 
   IF LENGTH(imp1.top005) >0 THEN 
      LET col = librut001_centrado(imp1.top005,lg) 
      PRINT COLUMN col,imp1.top005 CLIPPED 
   END IF 
   IF LENGTH(imp1.top006) >0 THEN 
      LET col = librut001_centrado(imp1.top006,lg) 
      PRINT COLUMN col,imp1.top006 CLIPPED 
   END IF 
   SKIP 1 LINES 

   -- Datos de la factura 
   -- Numero de autorizacion 
   IF w_mae_tdc.regfac=1 THEN
      LET texto = "NUMERO DE CAI"
      LET col = librut001_centrado(texto,lg) 
      PRINT COLUMN col,texto CLIPPED

      LET texto = w_mae_tdc.numcai CLIPPED 
      LET col = librut001_centrado(texto,lg) 
      PRINT COLUMN col,texto CLIPPED

      LET texto = "DEL ", 
          w_mae_tdc.numcor USING "<<<<<<<<"," Al ",
          w_mae_tdc.numfin USING "<<<<<<<<"
      LET col = librut001_centrado(texto,lg) 
      PRINT COLUMN col,texto CLIPPED

      LET texto = "FECHA DE VENCIMIENTO: ",w_mae_tdc.fecven 
      LET col = librut001_centrado(texto,lg) 
      PRINT COLUMN col,texto CLIPPED
      SKIP 1 LINES 
   END IF 

   -- Datos de la orden 
   IF (w_mae_tra.numesa=0) THEN 
    LET xorden = w_mae_tra.numdoc 
    LET i = LENGTH(xorden) 
    IF (i=1) THEN
      LET j=i
    ELSE
      LET j=i-1
    END IF

    LET worden = "ORDEN # [ ",xorden[j,i]," ]" 
    LET col = librut001_centrado(worden,lg) 
    PRINT COLUMN col,worden CLIPPED 
   END IF 
   SKIP 1 LINES 

   -- Numero de documento 
   LET xnumdoc = w_mae_tra.nserie CLIPPED,"-",
                 w_mae_tra.numdoc USING "&&&&&&&&"

   PRINT COLUMN coi,xtitle1 CLIPPED," ",xnumdoc CLIPPED 

   -- Verificando si es de credito o contado
   IF w_mae_tra.credit=1 THEN
      PRINT COLUMN coi,"Forma Pago CREDITO"
   ELSE 
      PRINT COLUMN coi,"Forma Pago CONTADO"
   END IF

   -- Datos de cliente 
   LET xnomcli = w_mae_tra.nomcli CLIPPED 
   IF (w_mae_tra.codepl>0) THEN
      LET xnomdat = "Nombre:  ",xnomcli[1,30] 
      PRINT COLUMN coi,"Codigo:  ",w_mae_tra.codepl USING "<<<<" 
      PRINT COLUMN coi,xnomdat CLIPPED 
   ELSE 
    IF (LENGTH(w_mae_tra.nomcli)>0) THEN
      PRINT COLUMN coi,"Cliente: ",xnomcli[1,30] CLIPPED 
      IF (LENGTH(w_mae_tra.nomcli)>30) THEN
       PRINT COLUMN coi,"         ",xnomcli[31,60] CLIPPED 
      END IF 
    ELSE 
      PRINT COLUMN coi,"Cliente: "                     
      PRINT COLUMN coi,"        _______________________________"
    END IF 
   END IF 

   -- Verificando si esta afecto a regimen de facturacion
   IF w_mae_tdc.regfac=1 THEN
    -- Numero de Identifiacion Tributario 
    IF (LENGTH(w_mae_tra.numnit)>0) THEN
       PRINT COLUMN coi,DescripcionNT CLIPPED,":  ",w_mae_tra.numnit CLIPPED
    ELSE 
       PRINT COLUMN coi,DescripcionNT CLIPPED,":  "
       PRINT COLUMN coi,"        _______________________________"
    END IF 
   END IF
   SKIP 1 LINES 

   -- Fecha y cajero 
   PRINT COLUMN coi,"Fecha:   ",w_mae_tra.fecemi," ",w_mae_tra.horsis 
   PRINT COLUMN coi,"Cajero:  ",xnomcaj CLIPPED

   -- Datos de la orden o mesa
   IF (w_mae_tra.numesa>0) THEN 
    LET wnomesa = "MESA # [ ",w_mae_tra.numesa USING "<<<<<"," ]" 
    PRINT COLUMN coi,wnomesa CLIPPED 
   END IF 
   SKIP 1 LINES 

   -- Destino
   IF w_mae_ord.llevar=1 THEN
      LET xtitpla = "Detalle de Platos Servidos-Para Llevar" 
   ELSE
      LET xtitpla = "Detalle de Platos Servidos-Comer Aqui" 
   END IF

   -- Detalle de Platos Servidos 
   PRINT COLUMN coi,xtitpla CLIPPED 
   PRINT COLUMN coi,"_______________________________________"
   PRINT COLUMN coi,"Plato           Cant.    P/U      Valor" 
   PRINT COLUMN coi,"_______________________________________"
 
  ON EVERY ROW
   -- Imprimiendo detalle de platos 
   FOR i = 1 TO totlin
    IF v_cuenta[i].nomrec IS NULL OR 
       v_cuenta[i].cantid IS NULL OR 
       v_cuenta[i].totrec IS NULL THEN 
       CONTINUE FOR
    END IF 
    
    LET xdescrp = v_cuenta[i].nomrec[1,40] 
    PRINT COLUMN coi,xdescrp                               
    PRINT COLUMN coi                            ,18 SPACES,
          v_cuenta[i].cantid  USING "##&"       ,1 SPACES, 
          v_cuenta[i].prevta  USING "##&.&&"    ,2 SPACES, 
          v_cuenta[i].totrec  USING "##,##&.&&" 
   END FOR   

  ON LAST ROW
   -- Inicializando datos
   LET exonerado = 0
   LET tasaali0  = 0
   LET importe18 = 0 

   -- Imprimiendo totales 
   PRINT COLUMN coi,"_______________________________________"

   -- Verificando regimen de facturacion 
   IF w_mae_tdc.regfac=1 THEN
   
    -- Verificado impresion de desgloce de totales
    IF DesgloceTotales THEN 
     PRINT COLUMN coi,"IMPORTE EXONERADO         L. ",exonerado          USING "###,##&.&&"
     PRINT COLUMN coi,"IMPORTE EXENTO            L. ",w_mae_tra.totexe   USING "###,##&.&&"
     PRINT COLUMN coi,"IMPORTE GRAVADO (15%)     L. ",w_mae_tra.totgra   USING "###,##&.&&"
     PRINT COLUMN coi,"IMPORTE GRAVADO (18%)     L. ",importe18          USING "###,##&.&&"
     PRINT COLUMN coi,"DESCUENTOS Y REBAJAS      L. ",w_mae_tra.totdes   USING "###,##&.&&"

     -- Imprimiendo porcentaje de impuestos
     DECLARE cisv CURSOR FOR
     SELECT a.porisv,SUM(a.totisv) 
      FROM  pos_dtransac a
      WHERE (a.lnktra = w_mae_tra.lnktra) 
      GROUP BY 1 
      ORDER BY 1  
      FOREACH cisv INTO xporisv,xtotisv 
       LET xtitisv = "I.S.V.          (",xporisv USING "#&","%)     L."
       PRINT COLUMN coi,xtitisv CLIPPED,xtotisv          USING "####,##&.&&"
      END FOREACH
      CLOSE cisv
      FREE  cisv 

     -- Impuesto 18%
     LET xtitisv = "IMPUESTO        (",18 USING "#&","%)     L."
     PRINT COLUMN coi,xtitisv CLIPPED,0                USING "####,##&.&&"

     -- Imprimiendo total final 
     PRINT COLUMN coi,"TOTAL A PAGAR             L. ",w_mae_tra.totcta   USING "###,##&.&&"

     IF (w_mae_tra.propin>0) THEN
      PRINT COLUMN coi,"TOTAL PROPINA             L. ",w_mae_tra.propin   USING "###,##&.&&"
      PRINT COLUMN coi,"TOTAL A PAGAR + PROPINA   L. ",w_mae_tra.totpag   USING "###,##&.&&"
     END IF

     PRINT COLUMN coi,"_______________________________________"

     -- Forma de pago 
     -- Efectivo
     IF (recibi>0) THEN 
        PRINT COLUMN coi,"RECIBIDO                  L. ",recibi           USING "###,##&.&&" 
     END IF 
     IF (w_mae_tra.efecti>0) THEN 
        PRINT COLUMN coi,"EFECTIVO                  L. ",w_mae_tra.efecti USING "###,##&.&&" 
     END IF 
     IF (vuelto>0) THEN 
        PRINT COLUMN coi,"TOTAL CAMBIO              L. ",vuelto           USING "###,##&.&&"
     END IF 
     -- Tarjeta 
     IF (w_mae_tra.tarcre>0) THEN 
        PRINT COLUMN coi,"TARJETA DE CREDITO        L. ",w_mae_tra.tarcre USING "###,##&.&&" 
     END IF 

     SKIP 1 LINES
    ELSE
     -- Imprimiendo total final 
     PRINT COLUMN coi,"SUB-TOTAL                 L. ",w_mae_tra.totcta   USING "###,##&.&&"

     IF (w_mae_tra.propin>0) THEN
      PRINT COLUMN coi,"TOTAL PROPINA             L. ",w_mae_tra.propin   USING "###,##&.&&"
      PRINT COLUMN coi,"TOTAL                     L. ",w_mae_tra.totpag   USING "###,##&.&&"
     END IF 

     PRINT COLUMN coi,"_______________________________________"

     -- Forma de pago 
     -- Efectivo
     IF (recibi>0) THEN 
        PRINT COLUMN coi,"RECIBIDO                  L. ",recibi           USING "###,##&.&&" 
     END IF 
     IF (w_mae_tra.efecti>0) THEN 
        PRINT COLUMN coi,"EFECTIVO                  L. ",w_mae_tra.efecti USING "###,##&.&&" 
     END IF 
     IF (vuelto>0) THEN 
        PRINT COLUMN coi,"TOTAL CAMBIO              L. ",vuelto           USING "###,##&.&&"
     END IF 
     -- Tarjeta 
     IF (w_mae_tra.tarcre>0) THEN 
        PRINT COLUMN coi,"TARJETA DE CREDITO        L. ",w_mae_tra.tarcre USING "###,##&.&&" 
     END IF 

     -- Total Descuento 
     IF (w_mae_tra.totdes>0) THEN 
        PRINT COLUMN coi,"TOTAL DESCUENTO           L. ",w_mae_tra.totdes   USING "###,##&.&&"
     END IF 
     SKIP 1 LINES
    END IF 
   ELSE 
     -- Imprimiendo total final 
     PRINT COLUMN coi,"TOTAL                     L. ",w_mae_tra.totdoc   USING "###,##&.&&"
   END IF 

   -- Verificando si documento es reimpresion
   IF reimpresion THEN
      PRINT COLUMN coi,"*** RE-IMPRESION ***"
   END IF 

   -- Verificando si documento tiene firmas
   SELECT NVL(a.hayfir,0) 
    INTO  hayfirmas
    FROM  fac_tipodocs a
    WHERE a.tipdoc = w_mae_tra.tipdoc 
    IF (status!=NOTFOUND) THEN 
     IF hayfirmas THEN
      SKIP 4 LINES
      PRINT COLUMN coi,"___________________________________"
      PRINT COLUMN coi,"Firma de Aceptacion" 
     END IF 
   END IF 

   -- Pie de pagina 
   SKIP 1 LINES 
   IF LENGTH(imp1.foot01) >0 THEN 
      LET col = librut001_centrado(imp1.foot01,lg) 
      PRINT COLUMN col,imp1.foot01 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot02) >0 THEN 
      LET col = librut001_centrado(imp1.foot02,lg) 
      PRINT COLUMN col,imp1.foot02 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot03) >0 THEN 
      LET col = librut001_centrado(imp1.foot03,lg) 
      PRINT COLUMN col,imp1.foot03 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot04) >0 THEN 
      LET col = librut001_centrado(imp1.foot04,lg) 
      PRINT COLUMN col,imp1.foot04 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot05) >0 THEN 
      LET col = librut001_centrado(imp1.foot05,lg) 
      PRINT COLUMN col,imp1.foot05 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot06) >0 THEN 
      LET col = librut001_centrado(imp1.foot06,lg) 
      PRINT COLUMN col,imp1.foot06 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot07) >0 THEN 
      LET col = librut001_centrado(imp1.foot07,lg) 
      PRINT COLUMN col,imp1.foot07 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot08) >0 THEN 
      LET col = librut001_centrado(imp1.foot08,lg) 
      PRINT COLUMN col,imp1.foot08 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot09) >0 THEN 
      LET col = librut001_centrado(imp1.foot09,lg) 
      PRINT COLUMN col,imp1.foot09 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot10) >0 THEN 
      LET col = librut001_centrado(imp1.foot10,lg) 
      PRINT COLUMN col,imp1.foot10 CLIPPED 
   END IF 

   -- Imprimiendo trazabilidad
   IF (w_mae_tra.trazbd=1) THEN
      -- Obteniendo nombre del elaborador
      INITIALIZE xnomela TO NULL 
      SELECT a.nomela 
       INTO  xnomela 
       FROM  glb_elabodrs a
       WHERE a.codela = w_mae_tra.codela 

      -- Obteniendo nombre del delivery
      INITIALIZE xnomdlv TO NULL 
      SELECT a.nomdlv 
       INTO  xnomdlv 
       FROM  glb_delivery a
       WHERE a.coddlv = w_mae_tra.coddlv 

      SKIP 1 LINES
      PRINT COLUMN coi,"  Boleta de Control de Trazabilidad de" 
      PRINT COLUMN coi,"          Alimentos y Bebidas"
      PRINT COLUMN coi,"Factura No.:       ",xnumdoc CLIPPED
      PRINT COLUMN coi,"Fecha:             ",w_mae_tra.fecemi
      PRINT COLUMN coi,"Hora Elaboracion:  ",w_mae_tra.horsis
      PRINT COLUMN coi,"Hora Despacho:     ",w_mae_tra.horsis
      PRINT COLUMN coi,"Hora Entrega:      ",w_mae_tra.horsis
      PRINT COLUMN coi,"Alimentos Elaborados Por:" 
      PRINT COLUMN coi,xnomela CLIPPED
      PRINT COLUMN coi,"Alimentos Transportador Por:" 
      PRINT COLUMN coi,xnomdlv CLIPPED
      PRINT COLUMN coi,"Coordinador Responsable:"
      PRINT COLUMN coi,xnomcaj CLIPPED
      PRINT COLUMN coi,"Telefono Contacto: "
      PRINT COLUMN coi,numtel CLIPPED
      SKIP 1 LINES
   END IF 

   -- Imprimiendo pie 
   FOR i = 1 TO 6 
    PRINT " "
   END FOR 
   PRINT cortepapel CLIPPED 
END REPORT 
