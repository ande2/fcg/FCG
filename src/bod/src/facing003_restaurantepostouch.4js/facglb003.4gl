{ 
Fecha    : diciembre 2006 
Programo : Mynor Ramirez 
Objetivo : Programa de variables globales ordenes de comida 
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
DEFINE w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_tra   RECORD LIKE pos_mtransac.*,
       w_mae_tdc   RECORD LIKE fac_tdocxpos.*,
       w_mae_pos   RECORD LIKE fac_puntovta.*,
       w_mae_ord   RECORD LIKE sre_mordenes.*,
       v_divrecet  DYNAMIC ARRAY OF RECORD
        imagen     LIKE sre_divrecet.imagen, 
        nomdiv     LIKE sre_divrecet.nomdiv,
        coddiv     LIKE sre_divrecet.coddiv 
       END RECORD,
       v_scroll    DYNAMIC ARRAY OF RECORD
        nomrec     STRING                        
       END RECORD, 
       v_platos    DYNAMIC ARRAY OF RECORD
        lnkrec     LIKE sre_mrecetas.lnkrec, 
        nomrec     CHAR(40),                        
        prevta     LIKE sre_mrecetas.prevta,
        numrec     LIKE sre_mrecetas.numrec,
        combeb     LIKE sre_mrecetas.combeb,
        bebref     LIKE sre_mrecetas.bebref 
       END RECORD,
       w_dat_pla   RECORD 
        lnkrec     LIKE sre_mrecetas.lnkrec, 
        nomrec     CHAR(40), 
        prevta     LIKE sre_mrecetas.prevta,
        numrec     LIKE sre_mrecetas.numrec,
        combeb     LIKE sre_mrecetas.combeb,
        bebref     LIKE sre_mrecetas.bebref 
       END RECORD,
       v_orden     ARRAY[100] OF RECORD
        olnkrec    LIKE sre_mrecetas.lnkrec, 
        onumpla    SMALLINT,
        onomrec    LIKE sre_mrecetas.nomrec, 
        oprevta    LIKE sre_mrecetas.prevta,
        ototpro    DEC(14,2),
        onumrec    LIKE sre_mrecetas.numrec, 
        ocombeb    LIKE sre_mrecetas.combeb, 
        obebref    LIKE sre_mrecetas.bebref,
        ofinrec    CHAR(1),
        opreori    LIKE sre_mrecetas.prevta,
        ototdes    DEC(9,2),
        otipdes    SMALLINT
       END RECORD,
       v_cuenta    ARRAY[100] OF RECORD
        numesa     LIKE pos_mtransac.numesa,
        lnkrec     LIKE pos_dtransac.lnkrec,
        cantid     LIKE pos_dtransac.cantid,
        nomrec     LIKE sre_mrecetas.nomrec,
        preori     LIKE pos_dtransac.preori,
        prevta     LIKE pos_dtransac.prevta,
        totrec     LIKE pos_dtransac.totrec,
        porisv     LIKE pos_dtransac.porisv, 
        totisv     LIKE pos_dtransac.totisv,
        totdes     LIKE pos_dtransac.totdes,
        endcta     CHAR(1) 
       END RECORD,
       v_mesas     ARRAY[50] OF RECORD
        nomesa     SMALLINT, 
        valtot     DEC(14,2)
       END RECORD,
       v_listord   DYNAMIC ARRAY OF RECORD
        lnkord     LIKE sre_mordenes.lnkord,
        numord     LIKE sre_mordenes.numord,
        totord     LIKE sre_mordenes.totord,
        userid     LIKE sre_mordenes.userid,
        fecemi     LIKE sre_mordenes.fecemi,
        horsis     LIKE sre_mordenes.horsis
       END RECORD, 
       v_listdet   DYNAMIC ARRAY OF RECORD 
        cantid     LIKE pos_dtransac.cantid,
        nomrec     LIKE sre_mrecetas.nomrec,
        prevta     LIKE pos_dtransac.prevta,
        totrec     LIKE pos_dtransac.totrec,
        filler     CHAR(1) 
       END RECORD, 
       v_tarcre    ARRAY[5] OF RECORD
        numtar     CHAR(20), 
        numaut     CHAR(20),
        endrec     CHAR(20) 
       END RECORD,
       PuntoVenta      LIKE pos_mtransac.numpos,
       DesgloceTotales SMALLINT,
       DescripcionNT   CHAR(40),  
       username        CHAR(15), 
       totuni          DEC(14,2),
       totval          DEC(14,2),
       vuelto          DEC(9,2), 
       subtot          DEC(9,2), 
       recibi          DEC(9,2), 
       reimpresion     SMALLINT,
       totlin          SMALLINT,
       totdiv          SMALLINT, 
       totcom          SMALLINT, 
       totpla          SMALLINT, 
       totord          SMALLINT, 
       totmes          SMALLINT, 
       tottar          SMALLINT, 
       totbeb          SMALLINT, 
       totref          SMALLINT, 
       totdes          SMALLINT, 
       ColIniImpFac    SMALLINT, 
       ColIniImpCta    SMALLINT, 
       ColIniImpCoc    SMALLINT, 
       nomori          STRING,   
       nomest          STRING,   
       nomdes          STRING, 
       b               ui.ComboBox,
       cba             ui.ComboBox,
       w_lnktra        INT,
       w               ui.Window,
       f               ui.Form,
       numtel,NumTelCon  CHAR(10), 
       ImpCopiaDocTarCre SMALLINT, 
       Trazabilidad      SMALLINT 
END GLOBALS
