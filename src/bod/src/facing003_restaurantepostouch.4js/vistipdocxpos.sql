

--drop vis_tipdocxpos;

--create view "sistemas".vis_tipdocxpos (lnktdc,numpos,hayimp,nomdoc) as 

select x0.lnktdc ,
       x0.numpos ,
       x1.hayimp ,
       ((((TRIM ( BOTH ' ' FROM x1.nomdoc ) || ' SERIE ' ) || 
           TRIM ( BOTH ' ' FROM x0.nserie )) || ' ' ) || x0.nomdoc ) 
from fac_tdocxpos x0,fac_tipodocs x1,glb_empresas x2 
where x0.tipdoc = x1.tipdoc 
  AND x0.estado = 'A'
  AND x2.codemp = x0.codemp;                                        
          
grant select on vis_tipdocxpos to public; 
