
    drop   trigger "sistemas".trgupdateinvenxfac;

create trigger "sistemas".trgupdateinvenxfac update of lnkinv
    on "sistemas".pos_mtransac referencing old as pre new as pos

    for each row
        (
        execute procedure "sistemas".sptinsertfaccturasinv(pos.lnktra
    ,pos.numpos ,pos.codemp ,pos.nserie ,pos.numdoc ,pos.fecemi ,pos.codcli
    ,pos.nomcli ,pos.llevar ));
