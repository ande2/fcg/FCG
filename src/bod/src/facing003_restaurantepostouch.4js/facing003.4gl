{
Programo : Mynor Ramirez
Objetivo : Programa de ordenes de comida 
}

IMPORT FGL wc_touchpad
IMPORT FGL wt_touchpad

-- Definicion de variables globales

GLOBALS "facglb003.4gl"
CONSTANT MaxNumeroMesas=20 
CONSTANT MaxNumeroPlatos=50 
CONSTANT MaxRowsPad=9
CONSTANT MaxColsPad=4 
CONSTANT progname = "facing003" 
DEFINE l_touchpad          RECORD
	touchpad           STRING
       END RECORD,
       t_touchpad          RECORD
	xtouchpad          STRING
       END RECORD,
       wpais               VARCHAR(255), 
       Porcendesesp        DEC(5,2), 
       Porcen3raedad       DEC(5,2),
       SolicitarPassTiempo SMALLINT,
       TipoDocFactura      SMALLINT,
       regreso             SMALLINT,
       existe              SMALLINT,
       totdet              SMALLINT, 
       msg                 STRING

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("ordenescomidatpad")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
	 HELP FILE "ayuda.hlp",
	 MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ 

 -- Obteniendo datos del pais 
 CALL librut001_parametros(1,0)
 RETURNING existe,wpais

 -- Obteniendo usuario
 CALL facing003_AccesoUsuario(TRUE)
 RETURNING regreso,username

 -- Verificando regreso
 IF NOT regreso THEN 
    -- Menu de opciones
    CALL facing003_menu()
 END IF 
END MAIN

-- Subutina para el menu de ordenes de comida 

FUNCTION facing003_menu()
 DEFINE regreso    SMALLINT, 
	conteo     INTEGER, 
        xporcenesp STRING, 
	titulo     STRING,
        w          ui.Window,
        f          ui.Form

 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing003a AT 5,2  
  WITH FORM "facing003a" ATTRIBUTE(BORDER)

  -- Obteniendo datos de la forma 
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Desplegando datos del encabezado
  CALL librut001_header("facing003",wpais,1)
  CALL librut001_dpelement("cajero","Cajero: "||username)
  CALL f.SetElementHidden("tablamesas",1)

  -- Obteniendo parametro de tipo de documernto de facturas
  CALL librut003_parametros(10,5)
  RETURNING existe,TipoDocFactura 
  IF NOT existe THEN
     CALL fgl_winmessage(
     "Atencion:",
     "Parametro de tipo de documento de facturas no definido.\n"||
     "Debe definirse antes de poder registrar movimientos.",
     "stop")
     CLOSE WINDOW wing003a  
     RETURN  
  END IF

  -- Obteniendo parametro de descripcion de numero tributario 
  CALL librut003_parametros(10,6)
  RETURNING existe,DescripcionNT 
  IF NOT existe THEN
     LET DescripcionNT="R.T.N." 
  END IF

  -- Obteniendo parametro de desgloce de totales en facturacion 
  CALL librut003_parametros(10,7)
  RETURNING existe,DesgloceTotales 
  IF NOT existe THEN
     LET DesgloceTotales=1 
  END IF

  -- Obteniendo parametro de solicitud de password por tiempo transcurrido 
  CALL librut003_parametros(10,7)
  RETURNING existe,SolicitarPassTiempo 
  IF NOT existe THEN
     LET SolicitarPassTiempo=0 
  END IF

  -- Obteniendo parametro de porcentaje de descuento especial
  CALL librut003_parametros(10,10)
  RETURNING existe,Porcendesesp
  IF NOT existe THEN
     LET Porcendesesp=0
  END IF
  LET xporcenesp = "Descuento \n",Porcendesesp USING "<<&.&&%"
  CALL librut001_dpelement("descuentoesp",xporcenesp)

  -- Obteniendo parametro de porcentaje de descuento 3ra edad 
  CALL librut003_parametros(10,11)
  RETURNING existe,Porcen3raedad 
  IF NOT existe THEN
     LET Porcen3raedad=0 
  END IF

  -- Obteniendo parametro de impresion de doble documento por pago con tarjeta de credito
  CALL librut003_parametros(10,12)
  RETURNING existe,ImpCopiaDocTarCre
  IF NOT existe THEN
     LET ImpCopiaDocTarCre = 0 
  END IF

  -- Obteniendo parametro de trazabilidad
  CALL librut003_parametros(10,13)
  RETURNING existe,Trazabilidad
  IF NOT existe THEN
     LET Trazabilidad = 0 
  END IF

  -- Obteniendo parametro de numero de telefono de contacto
  CALL librut003_parametros(10,14)
  RETURNING existe,NumTelCon 

  INITIALIZE w_mae_tdc.* TO NULL  
  SELECT a.*
   INTO  w_mae_tdc.*
   FROM  fac_tdocxpos a
   WHERE a.numpos = PuntoVenta 
     AND a.tipdoc = TipoDocFactura 
     AND a.estado = "A" 
  IF (status=NOTFOUND) THEN
     CALL fgl_winmessage(
     "Atencion:",
     "No existe definido un tipo de documento para facturar."||
     "\nDebe definirse el tipo de documento para poder facturar.",
     "stop") 
     CLOSE WINDOW wing003a  
     RETURN  
  END IF 

  -- Verificando fecha de vencimiento del tipo de documento
  IF (w_mae_tdc.fecven<TODAY) THEN
     CALL fgl_winmessage(
     "Atencion:",
     "Fecha de vencimiento ("||w_mae_tdc.fecven||
     ") del tipo de documento expirada."||
     "\nVerificar la vigencia de la fecha para poder facturar.",
     "stop") 
     CLOSE WINDOW wing003a  
     RETURN  
  END IF 

  -- Verificando si existe apertura de caja inicial    
  SELECT COUNT(*)
   INTO  conteo
   FROM  fac_vicortes a
   WHERE a.numpos = PuntoVenta
     AND a.feccor = TODAY 
     AND a.cierre IS NOT NULL 
  IF (conteo=0) THEN
     CALL fgl_winmessage(
     "Atencion:",
     "No existe apertura de caja inicial.\n"||
     "Registrar en la opcion corte de caja.\n"|| 
     "Al registrarlo ya se podran hacer ordenes de comida.", 
     "information") 
     CLOSE WINDOW wing003a  
     RETURN  
  END IF 

  -- Verificando si ya se realizo el cierre 
  SELECT COUNT(*)
   INTO  conteo
   FROM  fac_vicortes a
   WHERE a.numpos  = PuntoVenta
     AND a.feccor  = TODAY 
     AND a.fonini  IS NOT NULL 
     AND a.cierre  = 1 
  IF (conteo>0) THEN
     CALL fgl_winmessage(
     "Atencion:",
     "Corte de ventas del dia actual ya fue procesado.\n"||
     "No pueden emitirse ordenes de comida.", 
     "information") 
     CLOSE WINDOW wing003a  
     RETURN  
  END IF 

  -- Desplegando datos del punto de venta  
  INITIALIZE w_mae_pos.* TO NULL
  CALL librut003_bpuntovta(PuntoVenta) 
  RETURNING w_mae_pos.*,existe 
  CALL f.setElementText("nombrepos",w_mae_pos.nompos CLIPPED)

  -- Inicializando datos 
  CALL facing003_inival(0) 

  -- Ingreso de ordenes de comida 
  CALL facing003_OrdenesComida(1,f) 
 CLOSE WINDOW wing003a
END FUNCTION

-- Subrutina para el ingreso de las ordenes de comida 

FUNCTION facing003_OrdenesComida(operacion,f)
 DEFINE retroceso         SMALLINT,
	operacion         SMALLINT,
	retorna           SMALLINT,
	arr,scr,res,idx   SMALLINT, 
	loop,existe,i     SMALLINT,
	f                 ui.Form, 
	d                 ui.Dialog

 -- Inicio del loop
 LET retroceso = FALSE
 LET loop = TRUE
 WHILE loop   

  -- Verificando que no sea regreso
  IF NOT retroceso THEN

     -- Obteniendo datos del tipo de documento de la facturacion
     INITIALIZE w_mae_tdc.* TO NULL  
     SELECT a.*
      INTO  w_mae_tdc.*
      FROM  fac_tdocxpos a
      WHERE a.numpos = PuntoVenta 
	AND a.tipdoc = TipoDocFactura 
	AND a.estado = "A" 
     IF (status=NOTFOUND) THEN
	CALL fgl_winmessage(
	"Atencion:",
	"No existe definido un tipo de documento para facturar."||
	"\nDebe definirse el tipo de documento para poder facturar.",
	"stop") 
	LET loop = FALSE 
	EXIT WHILE 
     END IF 

     -- Inicializando datos 
     CALL facing003_inival(1) 

     -- Eliminando orden si no se facturo
     CALL facing003_EliminarOrdenes() 
  END IF

  INPUT BY NAME w_mae_ord.numesa,
		w_mae_ord.numord WITHOUT DEFAULTS
   ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE) 
   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   ON ACTION facturarmesas 
    -- Seleccionando mesas abiertas
    CALL facing003_MesasAbiertas(1,1)

   ON ACTION ordenesmesa 
    -- Seleccionando ordenes de mesas abiertas 
    CALL facing003_MesasAbiertas(1,2)

   ON ACTION cambiarmesa 
    -- Seleccionando mesas abiertas 
    CALL facing003_MesasAbiertas(1,3)

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
     CALL fgl_winmessage("Atencion:","Calculadora no displonible.","stop") 
    END IF

   BEFORE INPUT
    -- Verificando tipo de facturacion del punto de venta
    IF w_mae_pos.tipfac=1 THEN -- Por orden   
       LET w_mae_ord.numesa = 0

       -- Obteniendo numero maximo de orden 
       CALL facing003_NumeroOrden() 
       EXIT INPUT 
    END IF 

   AFTER FIELD numesa
    -- Verificando numesa 
    IF w_mae_ord.numesa IS NULL OR 
       w_mae_ord.numesa <0 OR 
       w_mae_ord.numesa >MaxNumeroMesas THEN 
       LET w_mae_ord.numesa = 0 
    END IF 
    DISPLAY BY NAME w_mae_ord.numesa 

   BEFORE FIELD numord
    -- Verificando si tipo de documento factura con orden automatica 
    -- Obteniendo numero maximo de orden 
    CALL facing003_NumeroOrden() 
    EXIT INPUT  
  END INPUT 
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Ingresando datos de divisiones y platos de comida 
  DIALOG ATTRIBUTES(UNBUFFERED)

   -- Divisiones 
   DISPLAY ARRAY v_divrecet TO s_divrecet.*
    ATTRIBUTE(COUNT=totdiv) 

    ON KEY(CONTROL-M) 
     -- Seleccionando platos de comida de la division de recetas 
     LET arr = ARR_CURR() 
     IF NOT facing003_PlatosComida(v_divrecet[arr].coddiv,1) THEN 
	LET retroceso = FALSE 
	EXIT DIALOG 
     END IF 

    ON ACTION choosedivision
     -- Seleccionando platos de comida de la division de recetas 
     LET arr = ARR_CURR() 
     IF NOT facing003_PlatosComida(v_divrecet[arr].coddiv,1) THEN 
	LET retroceso = FALSE 
	EXIT DIALOG 
     END IF 

    BEFORE DISPLAY 
     CALL DIALOG.setActionHidden("close",TRUE)

     -- Verificando si tipo de facturacion es por orden 
     IF w_mae_pos.tipfac=1 THEN 
       LET d = ui.Dialog.getCurrent()
       CALL d.setActionActive("facturarmesas",FALSE) 
       CALL d.setActionActive("ordenesmesa",FALSE) 
       CALL d.setActionActive("cambiarmesa",FALSE)
     END IF 

    BEFORE ROW 
     {-- Validando fila
     IF (ARR_CURR()>totdiv) THEN
       CALL FGL_SET_ARR_CURR(1)
     END IF }

     -- Desplegando platos de comida de la division de recetas 
     LET arr = ARR_CURR() 
     LET res = facing003_PlatosComida(v_divrecet[arr].coddiv,2)
   END DISPLAY 

   -- Platos de comida 
   INPUT BY NAME l_touchpad.*  
    BEFORE INPUT
     CALL wc_touchpad.html_send("formonly.touchpad")

    ON ACTION touchpad_clicked ATTRIBUTES(DEFAULTVIEW=NO)
     -- Llenando orden con el plato de comida seleccionado
     LET arr = l_touchpad.touchpad 

     -- Verificando si casilla esta vacia 
     IF (v_platos[arr].lnkrec IS NULL) THEN
	CONTINUE DIALOG
     END IF 

     -- Buscando si el plato de comida ya existe en la orden
     LET idx = facing003_BuscaPlato(v_platos[arr].lnkrec) 

     -- Llenando orden
     CALL facing003_LlenandoOrden(arr,idx)

   END INPUT

   ON ACTION datoscuenta 
    -- Revision de cuenta de platos 
    IF (totord>0) THEN
       CALL facing003_RevisarCuentaPlatos() 
    ELSE
       CALL fgl_winmessage(
       "Atencion:","Orden sin platos de comida.","stop")
       CONTINUE DIALOG 
    END IF 

   ON ACTION parallevar
    IF w_mae_ord.llevar=0 THEN
       LET w_mae_ord.llevar = 1 
       CALL librut001_dpelement("pllevar1","Para Llevar")    
       CALL librut001_dpelement("parallevar","Comer Aqui")    
    ELSE
       LET w_mae_ord.llevar = 0 
       CALL librut001_dpelement("pllevar1","Comer Aqui")
       CALL librut001_dpelement("parallevar","Para Llevar")    
    END IF 

   ON ACTION comentarios    
    -- Ingresando comentarios   
    CALL facing003_Comentarios() 

   ON ACTION facturarmesas 
    -- Seleccionando mesas abiertas
    CALL facing003_MesasAbiertas(1,1)

   ON ACTION ordenesmesa 
    -- Seleccionando ordenes de mesas abiertas
    CALL facing003_MesasAbiertas(1,2)

   ON ACTION cambiarmesa 
    -- Seleccionando mesas abiertas 
    CALL facing003_MesasAbiertas(1,3)

   ON ACTION nuevaorden 
    -- Nueva orden 
    LET retroceso = FALSE 
    EXIT DIALOG  

   ON ACTION cancel 
    -- Salida
    -- Verificando tipo de facturacion del punto de venta
    IF w_mae_pos.tipfac=1 THEN -- Por orden 
       LET loop = FALSE
    END IF 
    LET retroceso = FALSE 
    EXIT DIALOG  

   ON ACTION accept 
    -- Verificando platos de comida
    IF NOT facing003_RevisarPlatosOrden() THEN
       EXIT DIALOG 
    END IF 

   ON IDLE 300 
    -- Verificando solicitud
    {IF SolicitarPassTiempo THEN
     -- Solicita de nuevo el pasword transcurrido un tiempo 
     CALL facing003_AccesoUsuario(FALSE)
     RETURNING retorna,username
    END IF}

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       CALL fgl_winmessage("Atencion:","Calculadora no displonible.","stop")
    END IF

  END DIALOG 

  IF NOT retroceso THEN
     CONTINUE WHILE 
  END IF 
 END WHILE
 MESSAGE " " 

 -- Inicializando datos 
 IF (operacion=1) THEN 
    CALL facing003_inival(0) 
 END IF 
END FUNCTION

-- Subrutina para cargar las divisiones de recetas

FUNCTION facing003_DivisionesRecetas() 
 -- Inicializando vector
 CALL v_divrecet.clear()

 -- Llenando vector de recetas
 DECLARE c1 CURSOR FOR
 SELECT a.imagen, 
	a.nomdiv,
	a.coddiv,
	a.norden
  FROM  sre_divrecet a
  WHERE a.enmenu = 1 
    AND a.estado = 1
  ORDER BY 4 
  LET totdiv = 1 
  FOREACH c1 INTO v_divrecet[totdiv].*
   LET totdiv = totdiv+1
  END FOREACH
  CLOSE c1
  FREE  c1
  LET totdiv = totdiv-1 

 -- Desplegando divisiones de recetas
 DISPLAY ARRAY v_divrecet TO s_divrecet.*
  ATTRIBUTE(COUNT=totdiv,ACCEPT=FALSE) 
  BEFORE DISPLAY
   EXIT DISPLAY 
 END DISPLAY 
END FUNCTION 

-- Subrutina para cargar los platos de comida por cada division 

FUNCTION facing003_PlatosComida(xcoddiv,operacion)
 DEFINE xcoddiv     LIKE sre_mrecetas.coddiv,
	operacion   SMALLINT,
	arr,x,i     SMALLINT,
	nrow,ncol   SMALLINT,
	regreso     SMALLINT 

 -- Inicializando vector
 CALL v_platos.clear()
 CALL wc_touchpad.init_grid(MaxColsPad,MaxRowsPad)

 -- Llenando vector de recetas
 DECLARE c2 CURSOR FOR
 SELECT a.lnkrec,
	a.nomrec,
	a.prevta,
	a.numrec,
        a.combeb,
        a.bebref 
  FROM  sre_mrecetas a
  WHERE a.coddiv = xcoddiv 
    AND a.enmenu = 1 
  ORDER BY a.numpos 

  LET totpla = 1 
  FOREACH c2 INTO w_dat_pla.* 
   LET v_platos[totpla].* = w_dat_pla.* 
   LET totpla = totpla+1
  END FOREACH
  FREE  c2
  LET totpla = totpla-1 

  LET nrow = 1
  LET ncol = 1 
  FOR i = 1 TO MaxRowsPad*MaxColsPad
   CALL wc_touchpad.text_add2grid(ncol,nrow,1,1,v_platos[i].nomrec CLIPPED,i)

   LET ncol = ncol+1 
   IF (ncol>MaxColsPad) THEN 
      LET ncol = 1
      LET nrow = (nrow+1) 
   END IF 
   IF (nrow>MaxRowsPad) THEN 
      EXIT FOR
   END IF 
  END FOR 

 -- Verificando operacion
 IF (operacion=1) THEN 
  -- Si division no tiene platos de comida 
  IF (totpla<=0) THEN
     CALL fgl_winmessage(
     "Atencion:","Division sin platos de comida.","stop")
     RETURN TRUE 
  END IF 
 END IF  

 -- Desplegando platos de la division 
 INPUT BY NAME l_touchpad.* ATTRIBUTES(UNBUFFERED)
  BEFORE INPUT
   CALL wc_touchpad.html_send("formonly.touchpad")
   EXIT INPUT
 END INPUT

 RETURN TRUE 
END FUNCTION 

-- Subrutina para llengar la orden con un plato de comida 

FUNCTION facing003_LlenandoOrden(idx,arr)
 DEFINE idx,arr SMALLINT 

 -- Llenando orden
 -- Asignando datos 
 IF (arr=0) THEN 
  LET totord = totord+1
  LET v_orden[totord].olnkrec = v_platos[idx].lnkrec 
  LET v_orden[totord].onumpla = 1
  LET v_orden[totord].onomrec = v_platos[idx].nomrec
  LET v_orden[totord].oprevta = v_platos[idx].prevta
  LET v_orden[totord].opreori = v_platos[idx].prevta
  LET v_orden[totord].ototpro = 0 
  LET v_orden[totord].ototdes = 0 
  LET v_orden[totord].otipdes = 0 
  LET v_orden[totord].onumrec = v_platos[idx].numrec  
  LET v_orden[totord].ocombeb = v_platos[idx].combeb 
  LET v_orden[totord].obebref = v_platos[idx].bebref 
  LET v_orden[totord].ofinrec = "" 
 ELSE
  LET v_orden[arr].onumpla = v_orden[arr].onumpla+1

  -- Calculando descuento
  CALL facing002_CalculaDescuento(arr,v_orden[arr].otipdes,1)
 END IF 

 -- Totalizando
 CALL facing003_TotalizaOrden()

 -- Desplegando platos
 DISPLAY ARRAY v_orden TO s_orden.*
  ATTRIBUTE(COUNT=totord) 
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY
END FUNCTION 

-- Subrutina para validar platos de comida duplicados 

FUNCTION facing003_BuscaPlato(wlnkrec)
 DEFINE wlnkrec LIKE sre_mrecetas.lnkrec,
	i,idx   SMALLINT

 LET idx = 0
 FOR i = 1 TO totord 
  IF (v_orden[i].olnkrec IS NULL) THEN
   CONTINUE FOR
  END IF

  IF (v_orden[i].olnkrec=wlnkrec) THEN
     LET idx = i                  
     EXIT FOR
  END IF
 END FOR
 RETURN idx 
END FUNCTION

-- Subrutina para revisar la cuenta de los platos en la orden

FUNCTION facing003_RevisarCuentaPlatos() 
 DEFINE att ARRAY[100] OF RECORD
         olnkrec          STRING,
         onumpla          STRING,
         onomrec          STRING,
         oprevta          STRING,
         ototpro          STRING,
         onumrec          STRING,
         ocombeb          STRING,
         obebref          STRING,
         ofinrec          STRING,
         opreori          STRING,
         ototdes          STRING,
         otipdes          STRING 
        END RECORD,
     	arr,loop,regreso  SMALLINT, 
	retroceso,opc,scr SMALLINT,
	i                 SMALLINT

 -- Verificando si ya existe descuento 
 CALL att.clear() 
 FOR i = 1 TO 100
  IF v_orden[i].olnkrec IS NULL THEN
     CONTINUE FOR
  END IF
          
  -- Verificando si ya hay descuento
  IF v_orden[i].ototdes>0 THEN
     LET att[i].onumpla = "red" 
     LET att[i].onomrec = "red"
     LET att[i].ototpro = "red" 
  END IF 
 END FOR 

 LET loop = TRUE 
 WHILE loop
  LET regreso   = FALSE 
  LET retroceso = FALSE 

  -- Ingresando productos
  INPUT ARRAY v_orden FROM s_orden.*
   ATTRIBUTE(COUNT=totord,WITHOUT DEFAULTS,INSERT ROW=FALSE,ACCEPT=FALSE,
	     CANCEL=FALSE,UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION accept
    -- Calculando descuento
    LET arr = ARR_CURR() 
    CALL facing002_CalculaDescuento(arr,v_orden[arr].otipdes,1)

    -- Totalizando
    CALL facing003_TotalizaOrden()

    -- Aceptar
    LET regreso = FALSE 
    LET loop    = FALSE 
    EXIT INPUT

   ON ACTION cancel
    -- Cancelar
    LET regreso = TRUE
    LET loop    = FALSE 
    EXIT INPUT

   ON ACTION mas
    -- Agregar           
    LET arr = ARR_CURR() 

    -- Si plato no tiene ya descuento aplica agregar
    IF v_orden[arr].onumpla<MaxNumeroPlatos THEN
       LET v_orden[arr].onumpla = v_orden[arr].onumpla+1
    END IF 

    -- Calculando descuento
    CALL facing002_CalculaDescuento(arr,v_orden[arr].otipdes,1)

    -- Totalizando
    CALL facing003_TotalizaOrden()

   ON ACTION menos
    -- Reducir 
    LET arr = ARR_CURR() 

    -- Si plato no tiene ya descuento aplica reduccion 
    IF v_orden[arr].onumpla>1 THEN
       LET v_orden[arr].onumpla = v_orden[arr].onumpla-1
    END IF 

    -- Calculando descuento
    CALL facing002_CalculaDescuento(arr,v_orden[arr].otipdes,1)

    -- Totalizando
    CALL facing003_TotalizaOrden()

   ON ACTION terceraedad
    -- Descuento tercera edad
    LET arr = ARR_CURR() 

    -- Si ya hay un descuento especial no aplica tercera edad
    IF v_orden[arr].otipdes=2 THEN
     CALL fgl_winmessage(
     "Atencion:","Plato ya tiene descuento aplicado.","stop") 
     CONTINUE INPUT 
    END IF 

    -- Verificando si ya existe un descuento
    IF NOT facing005_HayDescuento(arr,1) THEN 
     -- Descuento se aplica se precio de venta es mayor que cero y cantidad de
     -- platos es igual a 1  
     IF (v_orden[arr].oprevta>0) THEN 
      --IF (v_orden[arr].onumpla=1) THEN DON MITRI 
       LET v_orden[arr].otipdes = 1 

       -- Calculando descuento
       CALL facing002_CalculaDescuento(arr,v_orden[arr].otipdes,0)
      --END IF 
     END IF 
    ELSE 
     CALL fgl_winmessage(
     "Atencion:","Ya existe un descuento aplicado.","stop") 
    END IF 

    -- Totalizando
    LET totord = ARR_COUNT()
    CALL facing003_TotalizaOrden()

   ON ACTION descuentoesp 
    -- Descuento especial
    LET arr = ARR_CURR() 

    -- Si ya hay un descuento tercera edad no aplica descuento especial
    IF v_orden[arr].otipdes=1 THEN
     CALL fgl_winmessage(
     "Atencion:","Plato ya tiene descuento 3ra edad aplicado.","stop") 
     CONTINUE INPUT 
    END IF 

    -- Verificando si ya existe un descuento
    IF NOT facing005_HayDescuento(arr,2) THEN 
     -- Descuento se aplica si precio de venta es mayor que cero 
     IF (v_orden[arr].oprevta>0) THEN 
       LET v_orden[arr].otipdes = 2 

       -- Calculando descuento
       CALL facing002_CalculaDescuento(arr,v_orden[arr].otipdes,0)
     END IF 
    ELSE 
     CALL fgl_winmessage(
     "Atencion:","Ya existe un descuento aplicado.","stop") 
    END IF 

    -- Totalizando
    LET totord = ARR_COUNT()
    CALL facing003_TotalizaOrden()

   ON ACTION comentarios    
    -- Ingresando comentarios   
    CALL facing003_Comentarios() 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       CALL fgl_winmessage("Atencion:","Calculadora no displonible.","stop")
    END IF

   BEFORE INPUT
    -- Desabilitanto opcion de append 
    CALL DIALOG.setActionHidden("append",TRUE)

    -- Asignando atributos 
    CALL DIALOG.setCellAttributes(att)

   BEFORE ROW
    -- Validando fila
    IF (ARR_CURR()>totord) THEN
       IF (ARR_CURR()>1) THEN
	  LET retroceso = TRUE 
	  EXIT INPUT 
       END IF 
    END IF

   ON CHANGE onumpla 
    -- Verificando plato
    LET arr = ARR_CURR() 
    LET scr = SCR_LINE() 

    -- Verificando plato con precio
    IF (v_orden[arr].oprevta>0) THEN
     -- Si descuento es cero
     IF v_orden[arr].ototdes=0 THEN 
      IF v_orden[arr].onumpla IS NULL OR 
         v_orden[arr].onumpla <=0 OR
         v_orden[arr].onumpla >MaxNumeroPlatos THEN
         LET v_orden[arr].onumpla = 1
      END IF 
     ELSE
      -- Si descuento es tercera edad siempre asigna 1 DON MITRI 
      --IF v_orden[arr].otipdes=1 THEN
         --LET v_orden[arr].onumpla =  1
      --END IF 

      -- Verificando descuento
      IF (v_orden[arr].otipdes>0) THEN 
         -- Calculando descuento
         CALL facing002_CalculaDescuento(arr,v_orden[arr].otipdes,1)
      END IF 
     END IF 
    ELSE
     LET v_orden[arr].onumpla = 1
    END IF 
    DISPLAY v_orden[arr].onumpla TO s_orden[scr].onumpla 

    -- Totalizando
    CALL facing003_TotalizaOrden()

   AFTER FIELD onumpla 
    -- Verificando plato
    LET arr = ARR_CURR() 
    LET scr = SCR_LINE() 

    -- Verificando plato con precio
    IF (v_orden[arr].oprevta>0) THEN
     -- Si descuento es cero
     IF v_orden[arr].ototdes=0 THEN 
      IF v_orden[arr].onumpla IS NULL OR 
         v_orden[arr].onumpla <=0 OR
	 v_orden[arr].onumpla >MaxNumeroPlatos THEN
         LET v_orden[arr].onumpla = 1
      END IF 
     ELSE 
      -- Si descuento es tercera edad siempre asigna 1 DON MITRI 
      --IF v_orden[arr].otipdes=1 THEN
       --  LET v_orden[arr].onumpla = 1
      --END IF 

      -- Verificando descuento
      IF (v_orden[arr].otipdes>0) THEN 
         -- Calculando descuento
         CALL facing002_CalculaDescuento(arr,v_orden[arr].otipdes,1)
      END IF 
     END IF 
    ELSE
     LET v_orden[arr].onumpla = 1
    END IF 
    DISPLAY v_orden[arr].onumpla TO s_orden[scr].onumpla 

    -- Totalizando
    CALL facing003_TotalizaOrden()

   AFTER ROW,INSERT,DELETE 
    -- Totalizando
    LET totord = ARR_COUNT()
    CALL facing003_TotalizaOrden()

   ON ROW CHANGE 
    -- Totalizando
    LET totord = ARR_COUNT()
    CALL facing003_TotalizaOrden()

   AFTER INPUT 
    -- Totalizando
    LET totord = ARR_COUNT()
    CALL facing003_TotalizaOrden()
  END INPUT 

  -- Verificando si hay datos de la tercera edad si hay descuento
  IF (w_mae_ord.totdes>0) THEN
   IF NOT loop THEN 
    -- Ingresando datos tercera edad 
    INPUT BY NAME w_mae_tra.nomcli WITHOUT DEFAULTS
     ATTRIBUTE(UNBUFFERED)
     ON ACTION cancel
      EXIT INPUT

     AFTER FIELD nomcli
      -- Verificando ingreso de datos
      IF LENGTH(w_mae_tra.nomcli)=0 THEN
       CALL fgl_winmessage( 
       "Atencion","Ingresar informacion del descuento, VERIFICA.","stop") 
       NEXT FIELD nomcli
      END IF 
    END INPUT
   END IF 
  END IF 
 END WHILE 
END FUNCTION 

-- Subrutina para verificar si hay existe un descuento 

FUNCTION facing005_HayDescuento(arr,xtipdes)  
 DEFINE arr,i,haydes,xtipdes SMALLINT

 LET haydes = FALSE 
 FOR i = 1 TO totord 
  IF v_orden[i].olnkrec IS NULL THEN
     CONTINUE FOR 
  END IF 

  -- Si es tercera edad
  IF xtipdes=1 THEN
   IF (v_orden[i].ototdes>0 AND v_orden[i].otipdes=2) THEN
    IF (arr!=i) THEN 
     LET haydes = TRUE 
     EXIT FOR 
    END IF 
   END IF 
  ELSE
   -- Si es descuento especial
   IF (v_orden[i].ototdes>0 AND v_orden[i].otipdes=1) THEN
    IF (arr!=i) THEN 
     LET haydes = TRUE 
     EXIT FOR 
    END IF 
   END IF 
  END IF 
 END FOR 
 RETURN haydes 
END FUNCTION 

-- Subrutina para revisar los platos en la orden

FUNCTION facing003_RevisarPlatosOrden()
 DEFINE	regreso,opc  SMALLINT, 
	nombreopcion STRING

 -- Verificando si hay platos en la orden
 IF (totord<=0) THEN
    CALL fgl_winmessage(
    "Atencion:",
    "Debe seleccionarse al menos un plato de comida, VERIFICA.",
    "stop")
    RETURN TRUE 
 END IF
 MESSAGE "" 

 -- Verificando que total de orden sea mayor que cero
 IF (w_mae_ord.totord<=0) THEN
    CALL fgl_winmessage(
    "Atencion:",
    "Total orden de comida debe ser mayora a cero, VERIFICA.",
    "stop")
    RETURN TRUE 
 END IF

 -- Verificando cuadre de numero de bebidas de combos con numero 
 -- de bebidas de sabores
 IF (totbeb!=totref) THEN
  CALL fgl_winmessage(
  "Atencion:",
  "Total de bebidas sabores diferente a "||
  "total de bebidas combos, VERIFICA.\n"||
  "Total combos "||totbeb||" Total sabores "||totref||".", 
  "stop")
  RETURN TRUE 
 END IF 

 -- Verificando tipo de facturacion del punto de venta
 IF w_mae_pos.tipfac=1 THEN -- Por orden 
  LET nombreopcion = "Facturar"
 ELSE                       -- Por mesa
  -- Verificando si es mesa de contado
  IF (w_mae_ord.numesa>0) THEN
   LET nombreopcion = "Guardar"
  ELSE
   LET nombreopcion = "Facturar" 
  END IF 
 END IF 

 -- Menu de opciones 
 LET opc = librut001_menugraba("Confirmacion",
			       "Que desea hacer?",
			       nombreopcion, 
			       "Modificar",
			       "Cancelar",
			       "")
 -- Verificando opcion
 CASE (opc)
  WHEN 0 -- Cancelando
   LET regreso = FALSE 
  WHEN 1 -- Grabando/Facturar 
   LET regreso = FALSE 
	 
   -- Verificando tipo de facturacion del punto de venta
   IF w_mae_pos.tipfac=1 THEN -- Por orden 
      -- Grabando orden de comida 
      CALL facing003_GrabarOrden()
      -- Facturando orden 
      CALL facing003_FacturarMesasAbiertas(w_mae_ord.numesa,
					   w_mae_ord.lnkord,
					   w_mae_ord.numord,
					   w_mae_pos.tipfac) 
   ELSE                       -- Por mesa 
      -- Verificando si se acumula orden por mesa 
      IF (w_mae_ord.numesa>0) THEN
	 -- Grabando orden de comida 
	 CALL facing003_GrabarOrden()

         -- Cargando ordenes de la mesa 
         CALL facing003_CargaOrdenesMesa(w_mae_ord.numesa,
                                         w_mae_ord.lnkord,
                                         0,1) 

         -- Imprimiendo comanda de la mesa
         CALL facrpt003b_GeneraComanda(FALSE,
                                       w_mae_ord.numesa,
                                       w_mae_ord.lnkord,
                                       w_mae_ord.fecemi,
                                       w_mae_ord.observ,
                                       0,                     
                                       w_mae_ord.lnkord, 
                                       w_mae_ord.userid)
      ELSE
	 -- Se graba orden de contado 
	 -- Grabando orden de comida 
	 CALL facing003_GrabarOrden()

	 -- Facturando orden 
	 CALL facing003_FacturarMesasAbiertas(w_mae_ord.numesa,
					      w_mae_ord.lnkord,
					      w_mae_ord.numord,
					      1) 
      END IF 
   END IF 
  WHEN 2 -- Modificando
   LET regreso = TRUE  
 END CASE

 RETURN regreso 
END FUNCTION 

-- Subrutina para calcular descuentos

FUNCTION facing002_CalculaDescuento(arr,xtipdes,accion)
 DEFINE arr,xtipdes SMALLINT,
        accion      SMALLINT,
        xporcen     DEC(5,2) 

 -- Verificando tipo de descuento 
 DISPLAY "TIPO DES ",xtipdes 
 CASE (xtipdes)
  WHEN 0 LET xporcen = 0 
  WHEN 1 LET xporcen = Porcen3raedad
  WHEN 2 LET xporcen = Porcendesesp
 END CASE

 -- Obteniendo precio original del plato
 IF accion=1 THEN
    SELECT NVL(a.prevta,0) 
     INTO  v_orden[arr].oprevta 
     FROM  sre_mrecetas a
     WHERE a.lnkrec = v_orden[arr].olnkrec 

    LET v_orden[arr].opreori = v_orden[arr].oprevta 
 END IF 

 -- Calculando
 LET v_orden[arr].ototdes = (v_orden[arr].oprevta-
                            (v_orden[arr].opreori-
                            ((v_orden[arr].opreori*xporcen)/100)))
 LET v_orden[arr].oprevta = (v_orden[arr].opreori-v_orden[arr].ototdes) 
 LET v_orden[arr].ototdes = (v_orden[arr].onumpla*v_orden[arr].ototdes) 

 DISPLAY "DESCUENTO ",v_orden[arr].ototdes

 -- Verificando si no hubo descuento 
 IF v_orden[arr].ototdes<=0 THEN 
    LET v_orden[arr].otipdes = 0 
 END IF 
            
 {IF v_orden[arr].ototdes>0 THEN  
    LET att[arr].onumpla = "red" 
    LET att[arr].onomrec = "red"
    LET att[arr].ototpro = "red" 
 ELSE
    LET att[arr].onumpla = "normal"
    LET att[arr].onomrec = "normal"
    LET att[arr].ototpro = "normal"
    LET w_mae_tra.nomcli = NULL
    DISPLAY BY NAME w_mae_tra.nomcli 
 END IF }
END FUNCTION 

-- Subrutina para totalizar la orden

FUNCTION facing003_TotalizaOrden()
 DEFINE i SMALLINT 

 -- Totalizando
 LET w_mae_ord.totord = 0
 LET w_mae_ord.totdes = 0
 LET totbeb = 0
 LET totref = 0
 FOR i = 1 TO totord
  IF v_orden[i].olnkrec IS NULL THEN 
     CONTINUE FOR
  END IF

  IF v_orden[i].olnkrec IS NOT NULL THEN 
   IF v_orden[i].onumpla IS NULL THEN 
    LET v_orden[i].onumpla = 1 
   END IF
  END IF 

  -- Totalizando total del plato 
  LET v_orden[i].ototpro = (v_orden[i].onumpla*v_orden[i].oprevta)
  LET w_mae_ord.totord   = w_mae_ord.totord+v_orden[i].ototpro 
  LET w_mae_ord.totdes   = w_mae_ord.totdes+v_orden[i].ototdes 

  -- Verificando si plato es combo es con bebida 
  IF (v_orden[i].ocombeb=1) THEN 
     LET totbeb  = (totbeb+v_orden[i].onumpla) 
  END IF 

  -- Verificando si plato es bebida de referencia 
  IF (v_orden[i].obebref=1) THEN 
     LET totref  = (totref+v_orden[i].onumpla) 
  END IF 
 END FOR 
 DISPLAY BY NAME w_mae_ord.totord,w_mae_ord.totdes 
END FUNCTION 

-- Subrutina para ingresar las comentarios   

FUNCTION facing003_Comentarios()
 -- Ingresando comentarios   
 INPUT BY NAME w_mae_ord.observ WITHOUT DEFAULTS
  ATTRIBUTE(UNBUFFERED) 
  ON ACTION cancel 
   EXIT INPUT
 END INPUT 
END FUNCTION 

-- Subrutina para grabar la orden de comida 

FUNCTION facing003_GrabarOrden()
 DEFINE i,correl,correl1 SMALLINT

 -- Grabando transaccion
 -- Iniciando la transaccion
 BEGIN WORK
   -- 1. Grabando encabezado la orden 

   -- Grabando
   SET LOCK MODE TO WAIT
   INSERT INTO sre_mordenes 
   VALUES (w_mae_ord.*)
   LET w_mae_ord.lnkord = SQLCA.SQLERRD[2] 

   -- 2. Grabando detalle de la orden 
   LET correl  = 0
   LET correl1 = 0
   FOR i = 1 TO totord 
     IF v_orden[i].olnkrec IS NULL OR
	v_orden[i].onumpla IS NULL THEN 
	CONTINUE FOR
     END IF

     -- Verificando productos con precio
     IF (v_orden[i].oprevta>0) THEN 
       LET correl = (correl+1) 
       -- Grabando platos con precio 
       SET LOCK MODE TO WAIT
       INSERT INTO sre_dordenes 
       VALUES (w_mae_ord.lnkord  , -- link del encabezado
	       v_orden[i].olnkrec, -- link del numero de plato
	       v_orden[i].onumrec, -- numero de la receta
	       v_orden[i].onumpla, -- numero de platos 
	       v_orden[i].oprevta, -- precio de venta del plato
	       0                 , -- costo de preparacion del plato
	       v_orden[i].ototpro, -- total del plato 
	       correl            , -- correlativo
	       v_orden[i].opreori, -- precio de venta original 
	       v_orden[i].ototdes, -- total descuento 
	       v_orden[i].otipdes) -- tipo de descuento 
     ELSE
       -- Grabando platos sin precio
       LET correl1 = (correl1+1) 
       SET LOCK MODE TO WAIT
       INSERT INTO sre_dplaspre 
       VALUES (w_mae_ord.lnkord  , -- link del encabezado
	       v_orden[i].olnkrec, -- link del numero de plato
	       v_orden[i].onumrec, -- numero de la receta
	       v_orden[i].onumpla, -- numero de platos 
	       correl1)            -- correlativo
     END IF 
   END FOR 

 -- Finalizando la transaccion
 COMMIT WORK
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION facing003_inival(i)
 DEFINE xporcenesp  STRING, 
        i,res       SMALLINT
 
 -- Inicializando 
 INITIALIZE w_mae_ord.*,w_mae_tra.* TO NULL 
 CALL facing003_inivec()
 CLEAR FORM 
 MESSAGE "" 

 -- Inicializando datos
 LET w_mae_ord.lnkord = 0                
 LET w_mae_ord.numpos = PuntoVenta 
 LET w_mae_ord.totord = 0 
 LET w_mae_ord.totdes = 0 
 LET w_mae_ord.estado = 1
 LET w_mae_ord.status = 1 
 LET w_mae_ord.llevar = 1 
 LET w_mae_tra.totdes = 0
 LET w_mae_tra.pordes = Porcen3raedad
 LET w_mae_tra.propin = 0
 LET w_mae_tra.porpro = 0 
 LET w_mae_tra.trazbd = Trazabilidad 
 LET w_mae_tra.coddlv = 1
 LET w_mae_tra.codela = 1 
 LET w_mae_ord.fecemi = CURRENT 
 LET w_mae_ord.userid = username 
 LET w_mae_ord.fecsis = CURRENT 
 LET w_mae_ord.horsis = CURRENT HOUR TO SECOND
 LET w_mae_ord.lnktra = 0                            
 LET totord           = 0 
 LET totlin           = 0 
 LET totdet           = 0 
 LET tottar           = 1 
 LET totbeb           = 0 
 LET totref           = 0 
 LET numtel           = NULL 

 -- Desplegando mesas abiertas 
 IF (i>0) THEN
    -- Llenando divisiones de recetas 
    CALL facing003_DivisionesRecetas() 

    -- Cargando platos de comida de la division 
    LET res = facing003_PlatosComida(v_divrecet[1].coddiv,2)
 END IF 

 -- Obteniendo parametro de porcentaje de descuento especial
 CALL librut003_parametros(10,10)
 RETURNING existe,Porcendesesp
 IF NOT existe THEN
    LET Porcendesesp=0
 END IF
 LET xporcenesp = "Descuento \n",Porcendesesp USING "<<&.&&%"
 CALL librut001_dpelement("descuentoesp",xporcenesp)

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_ord.totord,w_mae_ord.totdes  
 CALL librut001_dpelement("pllevar1","Para Llevar")
 CALL librut001_dpelement("parallevar","Comer Aqui")
END FUNCTION

-- Subrutina para inicializar y limpiar los vectores de trabajo 

FUNCTION facing003_inivec()
 -- Inicializando vectores
 CALL v_divrecet.clear()
 CALL v_platos.clear()
 CALL v_orden.clear()
 CALL v_tarcre.clear()
 CALL v_mesas.clear() 
 CALL v_cuenta.clear() 
 CALL v_listord.clear() 
 CALL v_listdet.clear() 
END FUNCTION 

-- Subrutina para obtener las mesas abiertas  

FUNCTION facing003_MesasAbiertas(operacion,opcion)
 DEFINE operacion SMALLINT, 
	arr,loop  SMALLINT, 
	opcion    SMALLINT

 -- Escondiendo y mostrando elementos 
 CALL f.SetElementHidden("tablamesas",0)

 -- Desplegando titulo 
 --CASE (opcion)
 -- WHEN 1 CALL librut001_DPElement("titulomesas","Facturar Mesas")
 -- WHEN 2 CALL librut001_DPElement("titulomesas","Ordenes Mesas")
 -- WHEN 3 CALL librut001_DPElement("titulomesas","Cambiar Mesas")
 --END CASE 

 -- Iniciando loop
 LET loop = TRUE
 WHILE loop 
  -- Inicializando datos
  CALL v_mesas.clear()
  LET totmes = 1 

  -- Seleccionando mesas abiertas
  DECLARE cmesas CURSOR FOR 
  SELECT a.numesa,NVL(SUM(a.totord),0)
   FROM  sre_mordenes a
   WHERE a.numpos = w_mae_ord.numpos
     AND a.numesa IS NOT NULL 
     AND a.fecemi = TODAY  		
     AND a.estado = 1 
     AND a.status = 1 
   GROUP BY 1 
   ORDER BY 1 
   FOREACH cmesas INTO v_mesas[totmes].*
    LET totmes=totmes+1
   END FOREACH
   CLOSE cmesas
   FREE  cmesas
   LET totmes=totmes-1 

  -- Desplegando mesas abiertas
  DISPLAY ARRAY v_mesas TO s_mesas.*
   ATTRIBUTE(COUNT=totmes,ACCEPT=FALSE) 

   ON ACTION cancel
    -- Salida
    LET loop = FALSE 
    EXIT DISPLAY 

   ON KEY(CONTROL-M) 
    -- Verificando opcion 
    LET arr = ARR_CURR() 
    CASE (opcion) 
     WHEN 1 -- Facturacion
      -- Facturacion de la mesa 
      CALL facing003_FacturarMesasAbiertas(v_mesas[arr].nomesa,0,0,2) 
      EXIT DISPLAY 
     WHEN 2 -- Ordenes 
      -- Lista de ordenes 
      CALL facing003_ListarOrdenes(v_mesas[arr].nomesa) 
      EXIT DISPLAY 
     WHEN 3 -- Cambiar mesas 
      -- Cambio de mesas 
      CALL facing003_CambiarMesas(v_mesas[arr].nomesa) 
      EXIT DISPLAY 
    END CASE 

   ON ACTION choosemesa 
    -- Verificando opcion 
    LET arr = ARR_CURR() 
    CASE (opcion) 
     WHEN 1 -- Facturacion
      -- Facturacion de la mesa 
      CALL facing003_FacturarMesasAbiertas(v_mesas[arr].nomesa,0,0,2) 
      EXIT DISPLAY 
     WHEN 2 -- Ordenes 
      -- Lista de ordenes 
      CALL facing003_ListarOrdenes(v_mesas[arr].nomesa) 
      EXIT DISPLAY 
     WHEN 3 -- Cambiar mesas 
      -- Cambio de mesas 
      CALL facing003_CambiarMesas(v_mesas[arr].nomesa) 
      EXIT DISPLAY 
    END CASE 

   BEFORE DISPLAY
    -- Verificando operacion
    IF (operacion=0) THEN
       LET loop = FALSE 
       EXIT DISPLAY
    ELSE
       IF (totmes=0) THEN
	  EXIT DISPLAY
       END IF 
    END IF 
  END DISPLAY 

  -- Si hay mesas abiertas se despliegan
  IF (totmes=0) THEN
   LET loop = FALSE 
   IF (operacion>0) THEN
    CALL fgl_winmessage(
    "Atencion:","No existen mesas abiertas, VERIFICA.","exclamation") 
   END IF 
  END IF 
 END WHILE 

 -- Escondiendo y mostrando elementos 
 CALL f.SetElementHidden("tablamesas",1)
 -- CALL librut001_DPElement("titulomesas","Ordenar Comida")
END FUNCTION 

-- Subrutina para facturar mesas abiertas 

FUNCTION facing003_FacturarMesasAbiertas(xnomesa,xlnkord,xnumord,operacion)
 DEFINE xnomesa     LIKE sre_mordenes.numesa,
	xlnkord     LIKE sre_mordenes.lnkord, 
	xnumord     LIKE sre_mordenes.numord,
	xtitle1     LIKE fac_tipodocs.title1,
        haypropina  SMALLINT, 
        haycredito  SMALLINT, 
	operacion   SMALLINT, 
	arr,loop,fp SMALLINT,
	opc         SMALLINT,
	reingreso   SMALLINT,
	xhayepl     SMALLINT,
	xnumdoc     CHAR(20), 
	w           ui.Window,
	f           ui.Form

 -- Abriendo ventana
 OPEN WINDOW wing003c AT 5,2
  WITH FORM "facing003c" ATTRIBUTE(BORDER)

  -- Asignando datos default
  LET w_mae_tra.numpos = w_mae_ord.numpos 
  LET w_mae_tra.codemp = w_mae_tdc.codemp
  LET w_mae_tra.lnktdc = w_mae_tdc.lnktdc 
  LET w_mae_tra.tipdoc = w_mae_tdc.tipdoc 
  LET w_mae_tra.nserie = w_mae_tdc.nserie 
  LET w_mae_tra.fecemi = CURRENT 
  LET w_mae_tra.llevar = w_mae_ord.llevar 
  LET w_mae_tra.descrp = w_mae_ord.observ 
  LET w_mae_tra.userid = username  
  LET w_mae_tra.numesa = xnomesa  
  LET w_mae_tra.codepl = 0 
  LET w_mae_tra.credit = 0 
  LET haypropina       = TRUE 
  LET haycredito       = FALSE  

  -- Verificando si no hay descuento 
  IF (w_mae_ord.totdes=0) THEN 
     LET w_mae_tra.nomcli = "CONSUMIDOR FINAL" 
  END IF 

  -- Obteniendo nombre del tipo de documento
  INITIALIZE xtitle1 TO NULL  
  SELECT a.title1
   INTO  xtitle1
   FROM  fac_tipodocs a
   WHERE a.tipdoc = w_mae_tdc.tipdoc 

  -- Obteniendo maximo numero de factura del tipo de documento
  SELECT NVL(MAX(a.numdoc),0)+1
   INTO  w_mae_tra.numdoc
   FROM  pos_mtransac a 
   WHERE (a.lnktdc = w_mae_tra.lnktdc)
   IF w_mae_tra.numdoc=1 THEN
      LET w_mae_tra.numdoc = w_mae_tdc.numcor
   END IF 

  -- Desplegando datos de la facturacion 
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando tipo de facturacion 
  IF operacion=1 THEN 
   -- Por orden 
   CALL fgl_settitle("Facturar Orden")

   -- Verificando si es para llevar o comet aqui
   CASE (w_mae_tra.llevar)
    WHEN 0 CALL f.setElementText("numeromesa","Orden #"||xnumord||" - Comer Aqui")
    WHEN 1 CALL f.setElementText("numeromesa","Orden #"||xnumord||" - Para Llevar")
   END CASE 
  ELSE 
   -- Por mesa 
   CALL fgl_settitle("Facturar Mesa")
   CALL f.setElementText("numeromesa","Mesa #"||w_mae_tra.numesa)
  END IF 

  -- Desplegando delivery
  -- Cargando combobox de delivery
  LET w_mae_tra.coddlv = 1 
  CALL librut003_CbxDelivery() 
  DISPLAY BY NAME w_mae_tra.coddlv 

  -- Desplegando elaboracion
  LET w_mae_tra.codela = 1 
  CALL librut003_CbxElaboradores()
  DISPLAY BY NAME w_mae_tra.codela 

  -- Desplegando telefono
  LET numtel = NumTelCon 
  DISPLAY BY NAME numtel 

  -- Inicializando datos del pago con tarjeta
  CALL v_tarcre.clear() 
  LET tottar = 1 

  -- Inicializando datos del tipo de documento 
  CALL f.SetElementHidden("llnktdc",1) 
  CALL f.SetElementHidden("formonly.lnktdc",1) 
  CALL f.SetElementHidden("formonly.codepl",1) 
  LET xnumdoc=w_mae_tra.nserie CLIPPED,"-",w_mae_tra.numdoc USING "&&&&&&&&"
  CALL f.setElementText("fechaemision","Fecha de Emision: "||w_mae_tra.fecemi) 
  CALL f.setElementText("cajero","Cajero:           "||username) 
  CALL f.setElementText("lb1",xtitle1 CLIPPED||" "||xnumdoc CLIPPED) 

  -- Cargando ordenes de la mesa 
  IF (operacion=1) THEN 
     CALL facing003_CargaOrdenesMesa(w_mae_tra.numesa,xlnkord,1,1) 
  ELSE
     CALL facing003_CargaOrdenesMesa(w_mae_tra.numesa,0,1,1) 
  END IF 

  -- Verificando forma de pago
  MENU 
   ON ACTION efectivo 
    LET loop             = TRUE
    LET w_mae_tra.efecti = w_mae_tra.totpag 
    LET w_mae_tra.tarcre = 0
    LET fp               = 1 
    EXIT MENU 
   COMMAND KEY ("e","E") 
    LET loop             = TRUE
    LET w_mae_tra.efecti = w_mae_tra.totpag 
    LET w_mae_tra.tarcre = 0
    LET fp               = 1 
    EXIT MENU 
   ON ACTION tarjeta
    LET loop             = TRUE
    LET w_mae_tra.tarcre = w_mae_tra.totpag 
    LET w_mae_tra.efecti = 0
    LET fp               = 2 
    EXIT MENU 
   COMMAND KEY ("t","T") 
    LET loop             = TRUE
    LET w_mae_tra.tarcre = w_mae_tra.totpag 
    LET w_mae_tra.efecti = 0
    LET fp               = 2 
    EXIT MENU 
   ON ACTION efectar  
    LET loop             = TRUE
    LET w_mae_tra.efecti = 0
    LET w_mae_tra.tarcre = 0 
    LET fp               = 3 
    EXIT MENU 
   COMMAND KEY ("m","M") 
    LET loop             = TRUE
    LET w_mae_tra.efecti = 0
    LET w_mae_tra.tarcre = 0 
    LET fp               = 3 
    EXIT MENU 
   ON ACTION cancel 
    LET loop             = FALSE 
    LET fp               = 0

    -- Eliminando orden si no se facturo
    CALL facing003_EliminarOrdenes() 

    EXIT MENU 
  END MENU 

  -- Inicio del loop
  WHILE loop 
   LET reingreso = FALSE 
   DISPLAY BY NAME w_mae_tra.totdoc 
   DIALOG ATTRIBUTES(UNBUFFERED)

    -- Ingresando datos 
    INPUT BY NAME w_mae_tra.nomcli,
		  w_mae_tra.numnit,
		  w_mae_tra.efecti,
		  w_mae_tra.tarcre 
     ATTRIBUTE(WITHOUT DEFAULTS=TRUE) 

     BEFORE INPUT 
      CALL DIALOG.setActionHidden("close",TRUE)

      -- Verificando tipo de facturacion del punto de venta por orden
      IF (operacion=1) THEN
         CALL DIALOG.setActionHidden("imprimircuenta",1)
      ELSE 
	 CALL DIALOG.setActionHidden("imprimircuenta",0)
      END IF 

      -- Verificando si tipo de documento requiere empleado
      LET xhayepl = facing003_TipoDocumentoEmpleado(w_mae_tra.lnktdc)

      -- Verificando si tipo documento solicita empleado
      IF (xhayepl=1) THEN
         CALL Dialog.SetFieldActive("nomcli",0)
         CALL Dialog.SetFieldActive("numnit",0)
      ELSE
         CALL Dialog.SetFieldActive("nomcli",1)
         CALL Dialog.SetFieldActive("numnit",1)
      END IF

      -- Verificando forma de pago
      CASE (fp)
       WHEN 1 -- Efectivo
	CALL Dialog.SetFieldActive("efecti",1)
	CALL Dialog.SetFieldActive("tarcre",0)
	NEXT FIELD efecti 
       WHEN 2 -- Tarjeta
	CALL Dialog.SetFieldActive("efecti",0)
	CALL Dialog.SetFieldActive("tarcre",1)
	NEXT FIELD tarcre 
       WHEN 3 -- Efectivo + Tarjeta
	CALL Dialog.SetFieldActive("efecti",1)
	CALL Dialog.SetFieldActive("tarcre",1)
	NEXT FIELD efecti 
      END CASE 
		  
     AFTER FIELD efecti
      -- Verificando efectivo
      IF (fp=1) THEN 
	 IF w_mae_tra.efecti IS NULL OR 
	    w_mae_tra.efecti <=0 THEN
	    LET w_mae_tra.efecti = w_mae_tra.totpag 
	 END IF 
      ELSE 
	 IF w_mae_tra.efecti IS NULL OR
	    w_mae_tra.efecti<=0 THEN
	    LET w_mae_tra.efecti= 0
	    NEXT FIELD efecti 
	 END IF 
      END IF
      DISPLAY BY NAME w_mae_tra.efecti,w_mae_tra.totdoc 

     AFTER FIELD tarcre
      -- Verificando tarjeta
      IF (fp=2) THEN 
	 LET w_mae_tra.tarcre = w_mae_tra.totpag 
      ELSE
	 IF w_mae_tra.tarcre IS NULL OR
	    w_mae_tra.tarcre <=0 THEN
	    LET w_mae_tra.tarcre = 0
	    NEXT FIELD tarcre
	 END IF 
      END IF 
      DISPLAY BY NAME w_mae_tra.tarcre,w_mae_tra.totdoc 

     ON ACTION efectivo 
      LET w_mae_tra.efecti = w_mae_tra.totpag 
      LET w_mae_tra.tarcre = 0
      LET fp               = 1 
      LET reingreso        = TRUE   
      EXIT DIALOG 

     ON ACTION tarjeta
      LET w_mae_tra.tarcre = w_mae_tra.totpag 
      LET w_mae_tra.efecti = 0
      LET fp               = 2 
      LET reingreso        = TRUE   
      EXIT DIALOG 

     ON ACTION efectar 
      LET w_mae_tra.tarcre = 0 
      LET w_mae_tra.efecti = 0
      LET fp               = 3 
      LET reingreso        = TRUE   
      EXIT DIALOG 

     ON ACTION cancel
      -- Salida 
      -- Eliminando orden si no se facturo
      CALL facing003_EliminarOrdenes() 

      LET loop = FALSE 
      EXIT DIALOG 

     ON ACTION accept
      -- Avanzar
      EXIT DIALOG 

     ON ACTION imprimircuenta 
      -- Imprimiendo cuenta
      IF (operacion=2) THEN -- Si es cuenta de mesa 
       CALL facrpt003a_GeneraCuenta(w_mae_tra.lnktdc, 
                                    w_mae_tra.numesa,
                                    w_mae_tra.fecemi,
                                    w_mae_tra.totdoc,
                                    w_mae_tra.totgra,
                                    w_mae_tra.totexe,
                                    w_mae_tra.totisv,
                                    w_mae_tra.totpag,
                                    w_mae_tra.totcta, 
                                    w_mae_tra.propin,
                                    w_mae_tra.userid)
      ELSE
       CALL fgl_winmessage(
       "Atencion:","Solo puede imprimirse cuenta de una mesa.","stop") 
      END IF 

     ON ACTION tiposdocumento
      -- Seleccionando tipo de documento 
      CALL facing003_CambiarTiposDocumento(xnomesa,xlnkord,xnumord,f)

      -- Verificando si tipo de documento requiere empleado
      LET xhayepl = facing003_TipoDocumentoEmpleado(w_mae_tra.lnktdc)

      -- Verificando si tipo documento solicita empleado
      IF (xhayepl=1) THEN
         CALL Dialog.SetFieldActive("nomcli",0)
         CALL Dialog.SetFieldActive("numnit",0)
      ELSE
         CALL Dialog.SetFieldActive("nomcli",1)
         CALL Dialog.SetFieldActive("numnit",1)
      END IF

     ON ACTION propina
      -- Habilitando o deshabilitando propina
      -- Si es para llevar no aplica habitlitar o deshabilitar propina
      IF (w_mae_tra.llevar=1) THEN
        CALL fgl_winmessage(
        "Atencion: ",
        "No aplica propina ya que la orden es para LLEVAR.",
        "warning")
      ELSE
       IF haypropina THEN
          LET haypropina = FALSE 
       ELSE
          LET haypropina = TRUE 
       END IF 

       -- Inicializando datos del pago con tarjeta
       CALL v_tarcre.clear() 
       LET tottar = 1 

       -- Cargando ordenes de la mesa 
       IF (operacion=1) THEN 
          CALL facing003_CargaOrdenesMesa(w_mae_tra.numesa,xlnkord,1,haypropina) 
       ELSE
          CALL facing003_CargaOrdenesMesa(w_mae_tra.numesa,0,1,haypropina) 
       END IF 

       -- Asigna efectivo por default     
       LET w_mae_tra.efecti = w_mae_tra.totpag 
       LET w_mae_tra.tarcre = 0
       LET fp               = 1 
       LET reingreso        = TRUE   
       EXIT DIALOG 
      END IF 

     ON ACTION credito
      -- Habilitando o deshabilitando credito 
      IF haycredito THEN
         LET haycredito = FALSE
         LET w_mae_tra.credit = 0 
         CALL librut001_dpelement("credito","CONTADO")    
      ELSE
         LET haycredito = TRUE  
         LET w_mae_tra.credit = 1 
         CALL librut001_dpelement("credito","CREDITO")    
      END IF 

     ON ACTION teclado 
      -- Llamando a teclado touchpad 
      CALL facing003_TecladoTouchPad(Fgl_Dialog_GetFieldName())

     ON ACTION trazabd
      -- Registrando trazabilidad
      CALL facing003_Trazabilidad()

     ON KEY (CONTROL-F) 
      -- Desplegando detalle 
      DISPLAY ARRAY v_cuenta TO s_cuenta.*
       ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE) 
       ON ACTION cancel
        EXIT DISPLAY
      END DISPLAY 
    END INPUT       
   END DIALOG 
   IF NOT loop THEN
      EXIT WHILE 
   END IF 
   IF reingreso THEN
      CONTINUE WHILE
   END IF 

   -- Verificando efectivo y tarjeta
   IF w_mae_tra.efecti IS NULL OR 
      w_mae_tra.tarcre IS NULL THEN
      CALL fgl_winmessage("Atencion","Valor efectivo o tarjeta invalidos.","stop") 
      CONTINUE WHILE
   END IF 

   -- Verificando si hay datos de la tarjeta de credito 
   IF w_mae_tra.tarcre>0 THEN
      IF facing003_DatosTarjetaCredito() THEN
	 CONTINUE WHILE
      END IF  
   END IF 

   -- Calculando vuelto
   LET vuelto = 0
   LET recibi = 0

   IF (fp=1) THEN
    IF (w_mae_tra.efecti>=w_mae_tra.totpag) THEN
       CALL v_tarcre.clear()
       LET tottar           = 1 
       LET w_mae_tra.tarcre = 0
       LET recibi           = w_mae_tra.efecti 
       LET vuelto           = (w_mae_tra.efecti-w_mae_tra.totpag)
       LET w_mae_tra.efecti = w_mae_tra.totpag  
       DISPLAY BY NAME w_mae_tra.efecti,w_mae_tra.tarcre,vuelto,subtot 
    END IF 
   END IF

   -- Totalizando 
   CALL facing003_TotalRecibido()

   -- Verificando total a pagar 
   IF (w_mae_tra.totpag!=w_mae_tra.totdoc) THEN
      CALL fgl_winmessage(
      "Atencion:",
      "Total recibido diferente a total a pagar, VERIFICA.",
      "stop")
      CONTINUE WHILE
   END IF

   IF (fp=1) THEN
       DISPLAY recibi TO totdoc 
   END IF 

   -- Menu de opciones 
   LET opc = librut001_menugraba("Confirmacion",
				 "Que desea hacer?",
				 "Guardar",
				 "Modificar",
				 "Cancelar",
				 "")
   -- Verificando opcion
   CASE (opc)
    WHEN 0 -- Cancelando
     LET loop = FALSE

     -- Eliminando orden si no se facturo
     CALL facing003_EliminarOrdenes() 

    WHEN 1 -- Grabando
     LET loop = FALSE
 
     -- Grabando orden de comidas 
     IF (operacion=1) THEN 
        CALL facing003_GrabarFactura(xlnkord)
     ELSE
        CALL facing003_GrabarFactura(0)
     END IF 
    WHEN 2 -- Modificando
     LET loop = TRUE
   END CASE
  END WHILE 

 -- Cerrando ventana
 CLOSE WINDOW wing003c 
END FUNCTION 

-- Subrutina para el ingreso de datos de la tarjeta 

FUNCTION facing003_DatosTarjetaCredito()
 DEFINE arr,loop,regreso  SMALLINT,
	retroceso,opc,scr SMALLINT, 
	lastkey           INTEGER 

 LET loop = TRUE 
 WHILE loop
  LET regreso = FALSE 

  -- Ingresando productos
  INPUT ARRAY v_tarcre FROM s_tarcre.*
   ATTRIBUTE(MAXCOUNT=tottar,WITHOUT DEFAULTS,INSERT ROW=FALSE,ACCEPT=FALSE,
             DELETE ROW=FALSE,
	     CANCEL=FALSE,UNBUFFERED,FIELD ORDER FORM)

   ON ACTION accept
    -- Aceptar
    LET loop    = FALSE 
    LET regreso = FALSE 
    EXIT INPUT

   ON ACTION cancel
    -- Cancelar
    LET regreso = TRUE
    LET loop    = FALSE 
    EXIT INPUT

   BEFORE INPUT
    -- Desabilitanto opcion de append 
    CALL DIALOG.setActionHidden("append",TRUE)

   AFTER FIELD numtar 
    LET arr = ARR_CURR() 

    -- Verificando numero tarjeta 
    IF LENGTH(v_tarcre[arr].numtar)<=0 THEN
       CALL fgl_winmessage(
       "Atencion:","Numero de tarjeta invalido, VERIFICA.","stop") 
       NEXT FIELD numtar
    END IF 

    -- Aceptar
    LET loop    = FALSE 
    LET regreso = FALSE 
    EXIT INPUT

   AFTER ROW,INSERT
    -- Totalizando
    LET tottar = ARR_COUNT()

   ON ROW CHANGE 
    -- Totalizando
    LET tottar = ARR_COUNT()

   AFTER INPUT 
    -- Totalizando
    LET tottar = ARR_COUNT()
  END INPUT 

  -- Verificando tarjetas
  IF NOT regreso THEN 
   IF tottar=0 THEN
     CALL fgl_winmessage
     ("Atencion:","Validar numero de tarjeta.","stop")
     LET loop = TRUE 
     CONTINUE WHILE 
   END IF 
  END IF 

  -- Salida 
  IF NOT loop THEN
     EXIT WHILE
  END IF 
 END WHILE 
 RETURN regreso 
END FUNCTION 

-- Subrutina para ingresar los datos de la trazabilidad
-- Delivery y elaboracion

FUNCTION facing003_trazabilidad()

 -- Ingresando datos 
 INPUT BY NAME w_mae_tra.coddlv,
               w_mae_tra.codela,
               numtel
  ATTRIBUTE(WITHOUT DEFAULTS=TRUE) 

  AFTER FIELD coddlv
   -- Verificando delivery
   IF w_mae_tra.coddlv IS NULL THEN
      NEXT FIELD coddlv
   END IF 

  AFTER FIELD codela
   -- Verificando elaboracion
   IF w_mae_tra.codela IS NULL THEN
      NEXT FIELD codela
   END IF 

  AFTER FIELD numtel
   IF numtel IS NULL THEN
      LET numtel = NumTelCon
      DISPLAY BY NAME numtel 
   END IF 
 END INPUT 
END FUNCTION 

-- Subrutina para seleccionar el tipo de documento a facturar

FUNCTION facing003_CambiarTiposDocumento(xnomesa,xlnkord,xnumord,f)
 DEFINE w         RECORD
	 lnktdc   LIKE fac_tdocxpos.lnktdc,
	 codepl   INTEGER,
	 nomemp   CHAR(40) 
	END RECORD, 
	xnomesa   LIKE sre_mordenes.numesa,
	xlnkord   LIKE sre_mordenes.lnkord,
	xnumord   LIKE sre_mordenes.numord,
	xnumdoc   CHAR(20),
	avanza    SMALLINT, 
	xhayepl   SMALLINT, 
	f         ui.Form

 -- Haciendo visible la seleccion  
 CALL f.SetElementHidden("llnktdc",0) 
 CALL f.SetElementHidden("formonly.lnktdc",0) 
 CALL f.SetElementHidden("formonly.codepl",0) 

 -- Cargando combobox 
 CALL librut003_CbxTiposDocxPos(PuntoVenta)
 CALL librut003_CbxEmpleados()

 -- Seleccionando tipos de documento
 INITIALIZE w.* TO NULL 
 LET avanza = TRUE 
 INPUT BY NAME w.lnktdc,
	       w.codepl WITHOUT DEFAULTS 

  ON ACTION cancel
   LET avanza = FALSE 
   EXIT INPUT

  ON CHANGE lnktdc 
   -- Verificando si tipo de documento requiere empleado
   LET xhayepl = facing003_TipoDocumentoEmpleado(w.lnktdc)

  AFTER FIELD lnktdc
   -- Verificando tipo de documento 
   IF w.lnktdc IS NULL THEN
      CALL fgl_winmessage(
      "Atencion:",
      "Debe seeccionarse el tipo de documento, VERIFICA.",
      "stop")
      NEXT FIELD lnktdc 
   END IF 

   -- Verificando si tipo de documento requiere empleado
   LET xhayepl = facing003_TipoDocumentoEmpleado(w.lnktdc)

  BEFORE FIELD codepl
   -- Verificando si tipo documento solicita empleado
   IF (xhayepl=1) THEN
      CALL Dialog.SetFieldActive("codepl",1)
   ELSE
      CALL Dialog.SetFieldActive("codepl",0)
   END IF 

  AFTER FIELD codepl
   -- Verificando empleado
   IF w.codepl IS NULL THEN 
      CALL fgl_winmessage(
      "Atencion:",
      "Debe seeccionarse el empleado, VERIFICA.",
      "stop")
      NEXT FIELD codepl 
   END IF 
 END INPUT 
  
 -- Verificando avance
 IF avanza THEN 
  -- Obteniendo datos del tipo de documento de la facturacion
  LET w_mae_tdc.lnktdc = w.lnktdc 

  -- Asignando datos del tipo de documento 
  CALL facing003_AsignaTipoDocumento(w.lnktdc,xnomesa,xlnkord,xnumord,f,w.codepl)
 END IF 
		  
 -- Escondiendo visible la seleccion  
 CALL f.SetElementHidden("llnktdc",1) 
 CALL f.SetElementHidden("formonly.lnktdc",1) 
 CALL f.SetElementHidden("formonly.codepl",1) 
END FUNCTION 

-- Subrutina para calcular el total recibido

FUNCTION facing003_TotalRecibido()
 LET w_mae_tra.totdoc = (w_mae_tra.efecti+w_mae_tra.tarcre)
 DISPLAY BY NAME w_mae_tra.totdoc,vuelto,subtot 
END FUNCTION

-- Subrutina para cargar las ordenes de la mesa

FUNCTION facing003_CargaOrdenesMesa(xnomesa,xlnkord,DespDatos,ValPropina)
 DEFINE w_mae_cta  RECORD 
	 numesa    LIKE pos_mtransac.numesa,
	 lnkrec    LIKE pos_dtransac.lnkrec,
	 nomrec    LIKE sre_mrecetas.nomrec,
	 cantid    LIKE pos_dtransac.cantid,
	 prevta    LIKE pos_dtransac.prevta,
	 totrec    LIKE pos_dtransac.totrec,
	 totdes    LIKE pos_dtransac.totdes,
	 porisv    LIKE pos_dtransac.porisv,
	 totisv    LIKE pos_dtransac.totisv
	END RECORD,
	xnomesa    LIKE sre_mordenes.numesa,
	xlnkord    LIKE sre_mordenes.lnkord,
        DespDatos  SMALLINT,
        ValPropina SMALLINT, 
        qrytext    STRING 

 -- Inicializando
 CALL v_cuenta.clear()
 LET totlin = 0 

 -- Verificando numero de orden 
 IF xlnkord=0 THEN 
    LET xlnkord = NULL 
 END IF  

 LET qrytext = 
  "SELECT x.numesa,y.lnkrec,z.nomrec,sum(y.cantid),",
        "(sum(y.totrec)/sum(y.cantid)),sum(y.totrec),sum(y.totdes) ",
   "FROM  sre_mordenes x,sre_dordenes y,sre_mrecetas z ",
   "WHERE x.lnkord = y.lnkord ",
   " AND x.lnkord = "||xlnkord, 
   " AND x.numpos = ",w_mae_ord.numpos, 
   " AND x.numesa = ",xnomesa,
   " AND x.fecemi = TODAY ",
   " AND x.estado = 1 ",
   " AND x.status = 1 ",
   " AND z.lnkrec = y.lnkrec ",
   " GROUP BY 1,2,3" 

 -- Seleccionando ordenes
 PREPARE x_ctas FROM qrytext 
 DECLARE c_ctas CURSOR FOR x_ctas 
 LET w_mae_tra.totpag = 0 
 LET w_mae_tra.totcta = 0 
 LET w_mae_tra.totdoc = 0 
 LET w_mae_tra.totisv = 0 
 LET w_mae_tra.totexe = 0
 LET w_mae_tra.totgra = 0 
 LET w_mae_tra.totdes = 0 
 FOREACH c_ctas INTO w_mae_cta.*
   -- Obteniendo porcentaje de isv
   SELECT NVL(a.porisv,0) 
    INTO  w_mae_cta.porisv 
    FROM  sre_mrecetas a
    WHERE a.lnkrec = w_mae_cta.lnkrec 

   -- Asignando datos 
   LET totlin = totlin+1 
   LET v_cuenta[totlin].numesa = w_mae_cta.numesa
   LET v_cuenta[totlin].lnkrec = w_mae_cta.lnkrec
   LET v_cuenta[totlin].cantid = w_mae_cta.cantid 
   LET v_cuenta[totlin].nomrec = w_mae_cta.nomrec
   LET v_cuenta[totlin].preori = w_mae_cta.prevta 
   LET v_cuenta[totlin].prevta = w_mae_cta.prevta 
   LET v_cuenta[totlin].totrec = w_mae_cta.totrec 
   LET v_cuenta[totlin].porisv = w_mae_cta.porisv 
   LET v_cuenta[totlin].totdes = w_mae_cta.totdes       

   -- Calculando impuesto 
   IF (w_mae_cta.porisv>0) THEN 
      LET v_cuenta[totlin].totisv = (w_mae_cta.totrec-
                            (w_mae_cta.totrec/(1+(w_mae_cta.porisv/100)))) 
      LET w_mae_tra.totgra = w_mae_tra.totgra+
			    (w_mae_cta.totrec/(1+(w_mae_cta.porisv/100))) 
   ELSE
      LET v_cuenta[totlin].totisv = 0 
      LET w_mae_tra.totexe = w_mae_tra.totexe+w_mae_cta.totrec 
   END IF 

   -- Totalizando 
   LET w_mae_tra.totpag = w_mae_tra.totpag+v_cuenta[totlin].totrec 
   LET w_mae_tra.totcta = w_mae_tra.totcta+v_cuenta[totlin].totrec 
   LET w_mae_tra.totisv = w_mae_tra.totisv+v_cuenta[totlin].totisv 
   LET w_mae_tra.totdes = w_mae_tra.totdes+v_cuenta[totlin].totdes 
 END FOREACH
 CLOSE c_ctas
 FREE  c_ctas 

 -- Calculado propina
 LET w_mae_tra.propin = 0 
 LET w_mae_tra.porpro = 0 

 -- Si es para comer aqui lleva propina 
 IF (w_mae_tra.llevar=0) THEN 
  IF (w_mae_pos.propin=1) THEN 
   IF ValPropina THEN 
    LET w_mae_tra.porpro = w_mae_pos.porpro 
    LET w_mae_tra.propin = ((w_mae_tra.totpag*w_mae_tra.porpro)/100) 
   END IF 
  END IF 
 END IF 

 -- Calculando total a pagar 
 LET w_mae_tra.totpag = w_mae_tra.totpag+w_mae_tra.propin 
 LET w_mae_tra.efecti = 0 
 LET w_mae_tra.tarcre = 0 
 LET vuelto           = 0 
 LET subtot           = w_mae_tra.totpag-w_mae_tra.totisv

 -- Desplegando datos de la cuenta 
 IF DespDatos THEN 
   DISPLAY ARRAY v_cuenta TO s_cuenta.*
    ATTRIBUTE(COUNT=totlin) 
    BEFORE DISPLAY
     EXIT DISPLAY
   END DISPLAY 

   -- Desplegando totales 
   DISPLAY BY NAME w_mae_tra.totpag,w_mae_tra.efecti,
                   w_mae_tra.tarcre,w_mae_tra.totdoc,
                   w_mae_tra.totcta,
		   vuelto          ,subtot 
   DISPLAY w_mae_tra.totisv TO totiva 
 END IF 
END FUNCTION 

-- Subrutina para grabar la facutra 

FUNCTION facing003_GrabarFactura(xlnkord) 
 DEFINE xnumrec   LIKE sre_mrecetas.numrec,  
	xcospre   LIKE sre_mrecetas.cospre, 
        xlnkord   LIKE sre_mordenes.lnkord, 
	xhaycmd   SMALLINT,
	i,correl  SMALLINT,
        ciclo     SMALLINT 

 -- Grabando transaccion
 -- Iniciando la transaccion
 BEGIN WORK
   -- 1. Grabando encabezado de la facturacion 
   -- Asignando datos
   LET w_mae_tra.lnktra = 0 
   LET w_mae_tra.codcli = 1 
   LET w_mae_tra.moneda = 1 
   LET w_mae_tra.cheque = 0 
   LET w_mae_tra.estado = 1 
   LET w_mae_tra.feccor = TODAY 
   LET w_mae_tra.haycor = w_mae_tdc.haycor 
   LET w_mae_tra.csmepl = w_mae_tdc.csmepl 
   LET w_mae_tra.regali = w_mae_tdc.regali
   LET w_mae_tra.porpro = w_mae_pos.porpro 
   LET w_mae_tra.usrope = username 
   LET w_mae_tra.fecsis = CURRENT 
   LET w_mae_tra.horsis = CURRENT HOUR TO SECOND  
   LET w_mae_tra.motanl = NULL
   LET w_mae_tra.usranl = NULL
   LET w_mae_tra.fecanl = NULL 
   LET w_mae_tra.horanl = NULL  

   -- Verificando nombre del cliente   
   IF LENGTH(w_mae_tra.nomcli)=0 THEN  
      LET w_mae_tra.nomcli = "CONSUMIDOR FINAL" 
   END IF 

   -- Grabando
   LET ciclo = TRUE 
   WHILE ciclo 
    TRY 
     SET LOCK MODE TO WAIT
     INSERT INTO pos_mtransac
     VALUES (w_mae_tra.*)
     LET w_mae_tra.lnktra = SQLCA.SQLERRD[2] 
     LET ciclo = FALSE 
     DISPLAY "FACTURA CORRECTA "||w_mae_tra.numdoc 
    CATCH 
     DISPLAY "ERROR: FACTURA "||w_mae_tra.numdoc 
     LET w_mae_tra.numdoc = w_mae_tra.numdoc+1 
     DISPLAY "NUEVA FACTURA "||w_mae_tra.numdoc 
     CONTINUE WHILE 
    END TRY 
   END WHILE 

   -- 2. Grabando detalle de la facturacion 
   LET correl = 0
   FOR i = 1 TO totlin 
    IF v_cuenta[i].numesa IS NULL OR 
       v_cuenta[i].lnkrec IS NULL OR
       v_cuenta[i].cantid IS NULL OR 
       v_cuenta[i].totrec IS NULL THEN 
       CONTINUE FOR 
    END IF 
    LET correl = (correl+1) 

    -- Obteniendo datos de la receta
    INITIALIZE xnumrec,xcospre TO NULL 
    SELECT a.numrec,a.cospre 
     INTO  xnumrec,xcospre
     FROM  sre_mrecetas a
     WHERE a.lnkrec = v_cuenta[i].lnkrec 

    -- Grabando
    SET LOCK MODE TO WAIT
    INSERT INTO pos_dtransac 
    VALUES (w_mae_tra.lnktra    , -- link del encabezado
	    w_mae_pos.codemp    , -- empresa del pos
	    w_mae_pos.codsuc    , -- sucursal del pos
	    w_mae_pos.codbod    , -- bodega del pos 
	    v_cuenta[i].lnkrec  , -- link de la receta del plato 
	    xnumrec             , -- numero de la receta del plato 
	    v_cuenta[i].cantid  , -- cantidad de platos 
	    v_cuenta[i].prevta  , -- precio de venta del plato 
	    xcospre             , -- precio costo del plato 
	    v_cuenta[i].totrec  , -- total de los platos
	    correl              , -- correlativo del plato 
	    v_cuenta[i].totisv  , -- total impuesto del plato
	    v_cuenta[i].porisv  , -- porcentaje impuesto del plato
	    v_cuenta[i].totdes  , -- total descuento
	    v_cuenta[i].preori  , -- precio original del plato
            0)                    -- tipo de descuento 
   END FOR 

   -- Actualizando ordenes por mesa y orden 
   SET LOCK MODE TO WAIT
   IF (xlnkord=0) THEN 
    UPDATE sre_mordenes
       SET sre_mordenes.status = 0,
           sre_mordenes.lnktra = w_mae_tra.lnktra 
     WHERE sre_mordenes.lnkord IS NOT NULL  
       AND sre_mordenes.numpos = w_mae_tra.numpos 
       AND sre_mordenes.numesa = w_mae_tra.numesa
       AND sre_mordenes.fecemi = TODAY 
       AND sre_mordenes.estado = 1
       AND sre_mordenes.status = 1
   ELSE
    UPDATE sre_mordenes
       SET sre_mordenes.status = 0,
           sre_mordenes.lnktra = w_mae_tra.lnktra 
     WHERE sre_mordenes.lnkord = xlnkord 
       AND sre_mordenes.numpos = w_mae_tra.numpos 
       AND sre_mordenes.numesa = w_mae_tra.numesa
       AND sre_mordenes.fecemi = TODAY 
       AND sre_mordenes.estado = 1
       AND sre_mordenes.status = 1
   END IF 

   -- Grabando detalle de tarjetas
   IF (tottar>0) THEN
      LET correl = 0
      FOR i = 1 TO tottar 
       IF v_tarcre[i].numtar IS NULL THEN 
	  CONTINUE FOR
       END IF
       LET correl = (correl+1) 

       -- Grabando
       SET LOCK MODE TO WAIT
       INSERT INTO fac_tarjetas
       VALUES (w_mae_tra.lnktra, 
	       correl,
	       0,
	       v_tarcre[i].numtar, 
	       v_tarcre[i].numaut, 
	       0) 
      END FOR 
   END IF 

   -- Actualizando inventario
   SET LOCK MODE TO WAIT 
   UPDATE pos_mtransac
   SET    pos_mtransac.lnkinv = 1 
   WHERE  pos_mtransac.lnktra = w_mae_tra.lnktra  

 -- Finalizando la transaccion
 COMMIT WORK

 -- Imprimiendo movimiento
 CALL facrpt003_GeneraFactura(FALSE)

 -- Verificando si tipo de documento imprime comanda 
 SELECT NVL(a.haycmd,0)
  INTO  xhaycmd 
  FROM  fac_tipodocs a
  WHERE a.tipdoc = w_mae_tdc.tipdoc 
  IF xhaycmd=1 THEN
     -- Verificando si es factura por orden para imprimir comanda
     IF (w_mae_tra.numesa=0) THEN 
       CALL facrpt003b_GeneraComanda(FALSE, 
                                     w_mae_tra.numesa,
                                     w_mae_tra.numdoc, 
                                     w_mae_tra.fecemi,
                                     w_mae_tra.descrp,
                                     w_mae_tra.lnktra, 
                                     0, 
                                     w_mae_tra.userid)
     END IF 
  END IF
END FUNCTION 

-- Subrutina para obtener el numero maximo de orden 

FUNCTION facing003_NumeroOrden()   
 -- Obteniendo numero de orden 
 SELECT NVL(MAX(a.numord),0)+1
  INTO  w_mae_ord.numord
  FROM  sre_mordenes a
  WHERE a.numpos = w_mae_ord.numpos 
  DISPLAY BY NAME w_mae_ord.numord  
END FUNCTION 

-- Subrutina para ingresar el cajero y el punto de venta

FUNCTION facing003_AccesoUsuario(haycancel) 
 DEFINE x          RECORD 
	 userid    CHAR(15),
	 passwd    CHAR(10),
	 numpos    SMALLINT 
	END RECORD,
	wfecha     VARCHAR(80), 
	regreso    SMALLINT,
	loop       SMALLINT,
	haycancel  SMALLINT,
	opc        SMALLINT,
	conteo     INTEGER, 
	msg        STRING 

 -- Abriendo la ventana 
 OPEN WINDOW wing003b AT 5,2  
  WITH FORM "facing003b" ATTRIBUTE(BORDER)

  -- Desplegando fecha 
  LET wfecha = librut001_formatofecha(TODAY,1,wpais) CLIPPED
  CALL librut001_dpelement("fecha",wfecha)

  -- Desactivando salida automatica del input 
  OPTIONS INPUT WRAP 

  -- Ingresando datos
  INITIALIZE x.* TO NULL 

  -- Obteniendo datos del usuario 
  LET x.userid = FGL_GETENV("LOGNAME")

  -- Inicio del loop 
  LET loop = TRUE
  WHILE loop
   INPUT BY NAME x.userid,
		 x.passwd WITHOUT DEFAULTS 
    ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)
   
    ON ACTION cancelar
     -- Salida 
     LET regreso = TRUE
     LET loop    = FALSE 
     EXIT INPUT 

    ON KEY(F4,CONTROL-E) 
     -- Salida 
     LET regreso = TRUE 
     LET loop    = FALSE 
     EXIT INPUT 

    ON ACTION aceptar  
     -- Verificando datos
     IF x.userid IS NULL THEN
	ERROR "Error: cajero invalido, VERIFICA."
	NEXT FIELD userid
     END IF 
     IF x.passwd IS NULL THEN
       ERROR "Error: password invalido, VERIFICA."
       NEXT FIELD passwd
     END IF 

     LET regreso = FALSE
     LET loop    = TRUE 
     EXIT INPUT 

    BEFORE INPUT
     -- Verificando si se valida tecla cancelar
     IF NOT haycancel THEN
       CALL DIALOG.setActionActive("cancelar",FALSE)
       NEXT FIELD passwd 
     END IF 

    AFTER FIELD userid 
     -- Verificando cajero 
     IF (LENGTH(x.userid)=0) THEN 
	ERROR "Error: cajero invalido, VERIFICA."
	NEXT FIELD userid
     END IF 

    AFTER FIELD passwd
     -- Verificando password
     IF (LENGTH(x.passwd)=0) THEN 
	ERROR "Error: password invalido, VERIFICA."
	NEXT FIELD passwd
     END IF 
     EXIT INPUT  
   END INPUT

   -- Verificando puntos de venta por cajero
   IF loop THEN 
    -- Buscando puntos de venta por cajero 
    SELECT COUNT(*)
     INTO  conteo 
     FROM  fac_usuaxpos a
     WHERE a.userid = x.userid
     IF (conteo=0) THEN
	LET msg = "Cajero [ ",x.userid CLIPPED,
                  " ] sin puntos de venta asignados. "||
		  "\n VERIFICA."
	CALL fgl_winmessage(" Atencion",msg,"stop")
	CONTINUE WHILE 
     ELSE
	LET PuntoVenta = NULL
	SELECT a.numpos 
	 INTO  PuntoVenta   
	 FROM  fac_usuaxpos a,fac_puntovta y
	 WHERE a.numpos = y.numpos 
	   AND a.userid = x.userid
	   AND a.passwd = x.passwd
	 IF (status=NOTFOUND) THEN
	    ERROR "Atencion: password incorrecto, VERIFICA."
	    CONTINUE WHILE  
	 END IF 
	 LET loop = FALSE 
     END IF 
   END IF 
  END WHILE

  -- Activando salida automatica del input 
  OPTIONS INPUT NO WRAP 
	  
 CLOSE WINDOW wing003b

 RETURN regreso,x.userid 
END FUNCTION 

-- Subrutina para listar las ordenes de una mesa

FUNCTION facing003_ListarOrdenes(xnomesa) 
 DEFINE xnomesa     LIKE sre_mordenes.numesa, 
	xlnkord     LIKE sre_mordenes.lnkord, 
	arr,loop    SMALLINT,
	w           ui.Window,
	f           ui.Form

 -- Abriendo ventana
 OPEN WINDOW wing003d AT 5,2
  WITH FORM "facing003d" ATTRIBUTE(BORDER)

  -- Desplegando datos de la facturacion 
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL fgl_settitle("Ordenes Por Mesa")
  CALL f.setElementText("numeromesa","Mesa #"||xnomesa)
  CALL f.setElementText("fechaemision","Fecha de Emision: "||w_mae_ord.fecemi) 
  CALL f.setElementText("cajero","Cajero:           "||username) 

  -- Cargando lista de ordenes de la mesa 
  CALL facing003_CargaListaOrdenesMesa(xnomesa) 

  -- Totalizando ordenes de la mesa 
  CALL facing003_TotalOrdenesMesa()

  -- Inicio del loop
  LET loop   = TRUE
  WHILE loop 
   DIALOG ATTRIBUTES(UNBUFFERED)
    -- Desplegando datos
    DISPLAY ARRAY v_listord TO s_listord.*
     ATTRIBUTE(COUNT=totlin) 

     ON ACTION anularorden 
      -- Eliminar orden vigente y abierta 
      LET msg = "Desea Anular la Orden de la Mesa?" 
      IF librut001_yesornot("Confirmacion",msg,"Si","No","question") THEN
	 -- Anular orden  
	 LET xlnkord = v_listord[ARR_CURR()].lnkord 
	 CALL v_listord.deleteElement(ARR_CURR())

	 -- Inicio transaccion
	 BEGIN WORK

	  -- Anular registros
	  SET LOCK MODE TO WAIT
	  UPDATE sre_mordenes
	  SET    sre_mordenes.estado = 0,  
		 sre_mordenes.usranl = username,
		 sre_mordenes.fecanl = CURRENT,
		 sre_mordenes.horanl = CURRENT HOUR TO SECOND 
	  WHERE  sre_mordenes.lnkord = xlnkord  
	    AND  sre_mordenes.numpos = w_mae_ord.numpos
	    AND  sre_mordenes.numesa = xnomesa
            AND  sre_mordenes.fecemi = TODAY 
	    AND  sre_mordenes.estado = 1
	    AND  sre_mordenes.status = 1

	 -- Finalizando la transaccion
	 COMMIT WORK

	 -- Desplegando detalle de la orden
	 CALL facing003_CargaDetalleOrden(v_listord[ARR_CURR()].lnkord, 
					  v_listord[ARR_CURR()].numord)

	 -- Totalizando ordenes de la mesa 
	 CALL facing003_TotalOrdenesMesa()

	 -- Verificando si mesa queda sin ordenes
	 IF (v_listord.getlength()=1) THEN
	  IF v_listord[ARR_CURR()].lnkord IS NULL THEN 
	   CALL fgl_winmessage(
	   "Atencion:",
           "Mesa ya sin ordenes, mesa quedara disponible.",
           "exclamation") 
	   LET loop = FALSE 
	   EXIT DIALOG 
	  END IF  
	 END IF 
      END IF

     ON ACTION imprimircuenta
      -- Cargando ordenes de la mesa 
      CALL facing003_CargaOrdenesMesa(xnomesa,0,0,1) 

      -- Imprimiendo cuenta
      CALL facrpt003a_GeneraCuenta(w_mae_tdc.lnktdc, 
                                   xnomesa,
                                   CURRENT, 
                                   w_mae_tra.totdoc,
                                   w_mae_tra.totgra,
                                   w_mae_tra.totexe,
                                   w_mae_tra.totisv,
                                   w_mae_tra.totpag,
                                   w_mae_tra.totcta, 
                                   w_mae_tra.propin,
                                   username)

      BEFORE DISPLAY 
       CALL DIALOG.setActionHidden("close",TRUE)

      BEFORE ROW 
       -- Desplegando detalle de la orden
       CALL facing003_CargaDetalleOrden(v_listord[ARR_CURR()].lnkord, 
                                        v_listord[ARR_CURR()].numord)
     END DISPLAY 

     -- Desplegando datos
     DISPLAY ARRAY v_listdet TO s_listdet.*
      ATTRIBUTE(COUNT=totdet) 

      BEFORE DISPLAY 
       CALL DIALOG.setActionHidden("close",TRUE)
     END DISPLAY 

    ON ACTION cancel
     -- Salida 
     LET loop = FALSE 
     EXIT DIALOG 

   END DIALOG 
   IF NOT loop THEN
      EXIT WHILE 
   END IF 
  END WHILE 

 -- Cerrando ventana
 CLOSE WINDOW wing003d 
END FUNCTION 

-- Subrutina para cargar la lista de las ordenes de la mesa

FUNCTION facing003_CargaListaOrdenesMesa(xnomesa)
 DEFINE w_mae_cta  RECORD 
	 lnkord    LIKE sre_mordenes.lnkord,
	 numord    LIKE sre_mordenes.numord,
	 totord    LIKE sre_mordenes.totord,
	 userid    LIKE sre_mordenes.userid,
	 fecemi    LIKE sre_mordenes.fecemi,
	 horsis    LIKE sre_mordenes.horsis
	END RECORD,
	xnomesa    LIKE sre_mordenes.numesa

 -- Inicializando
 CALL v_listord.clear()
 LET totlin = 0 

 -- Seleccionando ordenes
 DECLARE c_ords CURSOR FOR 
 SELECT a.lnkord,a.numord,a.totord,a.userid,a.fecemi,a.horsis 
  FROM  sre_mordenes a 
  WHERE a.numpos = w_mae_ord.numpos 
    AND a.numesa = xnomesa
    AND a.fecemi = TODAY
    AND a.estado = 1
    AND a.status = 1 
  ORDER BY 1 
  FOREACH c_ords INTO w_mae_cta.*
   -- Asignando datos 
   LET totlin = totlin+1 
   LET v_listord[totlin].lnkord = w_mae_cta.lnkord
   LET v_listord[totlin].numord = w_mae_cta.numord
   LET v_listord[totlin].totord = w_mae_cta.totord 
   LET v_listord[totlin].userid = w_mae_cta.userid
   LET v_listord[totlin].fecemi = w_mae_cta.fecemi 
   LET v_listord[totlin].horsis = w_mae_cta.horsis 
  END FOREACH
  CLOSE c_ords
  FREE  c_ords 
END FUNCTION 

-- Subrutina para recalcular el total de las ordenes de una mesa

FUNCTION facing003_TotalOrdenesMesa()
 DEFINE totalord LIKE sre_mordenes.totord,
		i        SMALLINT

 -- Totalizando
 LET totalord = 0
 FOR i = 1 TO totlin 
  IF v_listord[i].totord IS NULL THEN
     CONTINUE FOR
  END IF 
  LET totalord = totalord+v_listord[i].totord 
 END FOR
 DISPLAY BY NAME totalord 
END FUNCTION 

-- Subrutina para cargar el detalle de una orden de una mesa 

FUNCTION facing003_CargaDetalleOrden(xlnkord,xnumord)
 DEFINE w_mae_cta  RECORD 
	 cantid    LIKE pos_dtransac.cantid,
	 nomrec    LIKE sre_mrecetas.nomrec,
	 prevta    LIKE pos_dtransac.prevta,
	 totrec    LIKE pos_dtransac.totrec
	END RECORD,
	xlnkord    LIKE sre_mordenes.lnkord,
	xnumord    LIKE sre_mordenes.numord 

 -- Inicializando
 CALL librut001_DPElement("detalle","Detalle de Platos de la Orden # "||xnumord)
 CALL v_listdet.clear()
 LET totdet = 0 

 -- Seleccionando ordenes
 DECLARE c_det CURSOR FOR 
 SELECT x.cantid,y.nomrec,x.prevta,x.totrec
  FROM  sre_dordenes x,sre_mrecetas y
  WHERE x.lnkord = xlnkord  
    AND y.lnkrec = x.lnkrec 
  ORDER BY 1 
  FOREACH c_det INTO w_mae_cta.*
   -- Asignando datos 
   LET totdet = totdet+1 
   LET v_listdet[totdet].cantid = w_mae_cta.cantid 
   LET v_listdet[totdet].nomrec = w_mae_cta.nomrec
   LET v_listdet[totdet].prevta = w_mae_cta.prevta 
   LET v_listdet[totdet].totrec = w_mae_cta.totrec
   LET v_listdet[totdet].filler = ""
  END FOREACH
  CLOSE c_det
  FREE  c_det 

  -- Desplegando datos del detalle de la orden
  IF (totdet>0) THEN
     DISPLAY ARRAY v_listdet TO s_listdet.*
      ATTRIBUTE(COUNT=totdet)
      BEFORE DISPLAY
       EXIT DISPLAY 
     END DISPLAY
  END IF 
END FUNCTION 

-- Subrutina para cambiar mesas 

FUNCTION facing003_CambiarMesas(xnomesa) 
 DEFINE xnomesa  LIKE sre_mordenes.numesa, 
	xnuemes  LIKE sre_mordenes.numesa,
	numesa   LIKE sre_mordenes.numesa,
	loop     SMALLINT 

 -- Abriendo ventana
 OPEN WINDOW wing003e AT 5,2
  WITH FORM "facing003e" ATTRIBUTE(BORDER)

  -- Cargando lista de ordenes de la mesa 
  CALL facing003_CargaListaOrdenesMesa(xnomesa) 

  -- Desplegando datos
  DISPLAY ARRAY v_listord TO s_listord.*
   ATTRIBUTE(COUNT=totlin)
   BEFORE DISPLAY
    EXIT DISPLAY
  END DISPLAY 

  -- Cargando mesas 
  LET numesa = NULL 
  CALL librut003_CbxMesas(MaxNumeroMesas,xnomesa) 

  -- Ingresando datos del cambio  
  LET loop = TRUE 
  WHILE loop 
   DISPLAY BY NAME xnomesa 
   INPUT BY NAME numesa WITHOUT DEFAULTS
    ATTRIBUTE(UNBUFFERED) 
    ON ACTION cancel
     -- Salida
     LET loop    = FALSE 
     LET regreso = TRUE 
     EXIT INPUT

    ON ACTION accept 
     -- Avanza
     LET regreso = FALSE 
     EXIT INPUT 

    AFTER FIELD numesa  
     -- Verificando numero de mesa
     IF numesa IS NULL OR 
	numesa=0 THEN 
	ERROR "Error: mesa invalida, VERIFICA." 
	NEXT FIELD numesa  
     END IF 
   END INPUT

   -- Verificando que no sea regreso 
   IF NOT regreso THEN
    -- Verificando numero de mesa
    IF numesa IS NULL OR 
       numesa=0 THEN 
       ERROR "Error: mesa invalida, VERIFICA." 
       CONTINUE WHILE     
    END IF 

    -- Verificando cambio de mesa 
    LET msg = "Desea Cambiar la Mesa?" 
    IF librut001_yesornot("Confirmacion",msg,"Si","No","question") THEN
       LET loop    = FALSE 
       LET xnuemes = numesa 

       -- Actualizando cambio
       -- Iniciando Transaccion
       BEGIN WORK

        -- Actualizando nueva mesa
        SET LOCK MODE TO WAIT 
        UPDATE sre_mordenes
        SET    sre_mordenes.numesa = xnuemes,
               sre_mordenes.numant = xnomesa,
               sre_mordenes.usrcam = username,
               sre_mordenes.feccam = CURRENT,
               sre_mordenes.horcam = CURRENT HOUR TO SECOND
        WHERE  sre_mordenes.lnkord IS NOT NULL
          AND  sre_mordenes.numpos = w_mae_ord.numpos 
          AND  sre_mordenes.numesa = xnomesa 
          AND  sre_mordenes.fecemi = TODAY 
          AND  sre_mordenes.estado = 1
          AND  sre_mordenes.status = 1

       -- Finalizando Transaccion
       COMMIT WORK 
    END IF 
   END IF 
  END WHILE 
 CLOSE WINDOW wing003e
END FUNCTION 

-- Subrutina para asignar datos del tipo de documento

FUNCTION facing003_AsignaTipoDocumento(xlnktdc,xnomesa,xlnkord,xnumord,f,xcodepl)
 DEFINE xnomesa     LIKE sre_mordenes.numesa,
        xlnkord     LIKE sre_mordenes.lnkord, 
        xnumord     LIKE sre_mordenes.numord,
        xlnktdc     LIKE fac_tdocxpos.lnktdc, 
        xtitle1     LIKE fac_tipodocs.title1,
        xcodepl     LIKE pos_mtransac.codepl, 
        xnomemp     CHAR(40), 
        xnumdoc     CHAR(20), 
        f           ui.Form

 -- Obteniendo datos del tipo de documento 
 INITIALIZE w_mae_tdc.* TO NULL  
 SELECT a.*
  INTO  w_mae_tdc.*
  FROM  fac_tdocxpos a
  WHERE a.lnktdc = xlnktdc 

 -- Obteniendo nombre del tipo de documento
 INITIALIZE xtitle1 TO NULL  
 SELECT a.title1
  INTO  xtitle1
  FROM  fac_tipodocs a
  WHERE a.tipdoc = w_mae_tdc.tipdoc 

 -- Obteniendo nombre del empleado
 IF (xcodepl>0) THEN 
    LET w_mae_tra.codepl = xcodepl 
    LET w_mae_tra.numnit = NULL 
    LET w_mae_tra.nomcli = NULL 

    SELECT a.nomemp 
     INTO  w_mae_tra.nomcli 
     FROM  pla_empleads a  
     WHERE a.codepl = w_mae_tra.codepl 
 ELSE
    LET w_mae_tra.codepl = 0 
    LET w_mae_tra.nomcli = "CONSUMIDOR FINAL" 
 END IF 

 -- Asignando datos default
 LET w_mae_tra.numpos = w_mae_ord.numpos 
 LET w_mae_tra.codemp = w_mae_tdc.codemp
 LET w_mae_tra.lnktdc = w_mae_tdc.lnktdc 
 LET w_mae_tra.tipdoc = w_mae_tdc.tipdoc 
 LET w_mae_tra.nserie = w_mae_tdc.nserie 
 LET w_mae_tra.fecemi = CURRENT 
 LET w_mae_tra.userid = username  
 LET w_mae_tra.numesa = xnomesa  

 -- Obteniendo maximo numero de factura del tipo de documento
 SELECT NVL(MAX(a.numdoc),0)+1 
  INTO  w_mae_tra.numdoc
  FROM  pos_mtransac a 
  WHERE (a.lnktdc = w_mae_tra.lnktdc)
  IF w_mae_tra.numdoc=1 THEN
     LET w_mae_tra.numdoc = w_mae_tdc.numcor
  END IF 

 -- Inicializando datos del tipo de documento 
 LET xnumdoc = w_mae_tra.nserie CLIPPED,"-",w_mae_tra.numdoc USING "&&&&&&&&"
 CALL f.setElementText("lb1",xtitle1 CLIPPED||" "||xnumdoc CLIPPED) 
END FUNCTION 

-- Subrutina para desplegar teclado touchpad 

FUNCTION facing003_TecladoTouchPad(fieldname)
 DEFINE dato,fieldname  STRING,
        numero          STRING,
        x,avanza        SMALLINT 

 DISPLAY "CAMPO ",fieldname 

 -- Abriendo ventana
 OPEN WINDOW wing003f AT 5,2
  WITH FORM "facing003f" ATTRIBUTE(BORDER)

  -- Llenando teclado
  CALL wt_touchpad.xinit_grid(3,4)
  CALL wt_touchpad.xtext_add2grid(1,1,1,1,"7",7)
  CALL wt_touchpad.xtext_add2grid(2,1,1,1,"8",8)
  CALL wt_touchpad.xtext_add2grid(3,1,1,1,"9",9)
  CALL wt_touchpad.xtext_add2grid(1,2,1,1,"4",4)
  CALL wt_touchpad.xtext_add2grid(2,2,1,1,"5",5)
  CALL wt_touchpad.xtext_add2grid(3,2,1,1,"6",6)
  CALL wt_touchpad.xtext_add2grid(1,3,1,1,"1",1)
  CALL wt_touchpad.xtext_add2grid(2,3,1,1,"2",2)
  CALL wt_touchpad.xtext_add2grid(3,3,1,1,"3",3)
  CALL wt_touchpad.xtext_add2grid(1,4,1,1,"Limpiar",100)
  CALL wt_touchpad.xtext_add2grid(2,4,1,1,"0",0)
  CALL wt_touchpad.xtext_add2grid(3,4,1,1,".",".")

  -- Teclado numerico 
  LET numero = NULL
  INPUT BY NAME t_touchpad.xtouchpad ATTRIBUTE(UNBUFFERED) 
   BEFORE INPUT
    CALL wt_touchpad.xhtml_send("formonly.xtouchpad")

   ON ACTION ttouchpad_clicked ATTRIBUTES(DEFAULTVIEW=NO)
    -- Llenando orden con el plato de comida seleccionado
    LET dato = fgl_dialog_getbuffer() 

    LET avanza = TRUE 
    IF (dato=".") THEN 
       LET x = numero.getIndexof(".",1)
       DISPLAY "Posicion ",x 
       IF x>0 THEN
          LET avanza = FALSE 
          LET dato = NULL 
       ELSE
        IF (numero.getlength()=0) THEN 
         IF dato.substring(1,1) ="." THEN
            LET dato = "0." 
         END IF 
        END IF 
       END IF 
    END IF 

    IF avanza THEN 
     IF dato!="100" THEN 
       -- Creando numero
       LET numero = numero CLIPPED,dato CLIPPED 
       MESSAGE "Numero "||numero 

       -- Asignando numero 
       CASE (fieldname)
        WHEN "efecti" LET w_mae_tra.efecti = numero
        WHEN "tarcre" LET w_mae_tra.tarcre = numero
       END CASE   
     ELSE 
       -- Limpiando numero 
       LET numero = NULL 
       MESSAGE "Numero "||numero 

       -- Asignando numero 
       CASE (fieldname)
        WHEN "efecti" LET w_mae_tra.efecti = 0
        WHEN "tarcre" LET w_mae_tra.tarcre = 0
       END CASE   
     END IF 
    END IF 

    -- Desplegando numero 
    CURRENT WINDOW IS wing003c 
    DISPLAY BY NAME w_mae_tra.efecti,w_mae_tra.tarcre 
    CURRENT WINDOW IS wing003f 
  END INPUT
 CLOSE WINDOW wing003f
END FUNCTION 

-- Subrutina para eliminar las ordenes de comida que se cancelan por no proceder

FUNCTION facing003_EliminarOrdenes()
 -- Inicio transaccion
 BEGIN WORK

   -- Eliminando orden
   DELETE FROM sre_mordenes 
   WHERE sre_mordenes.lnkord IS NOT NULL  
     AND sre_mordenes.numpos = PuntoVenta 
     AND sre_mordenes.numesa = 0 
     AND sre_mordenes.userid = username               
     AND sre_mordenes.estado = 1
     AND sre_mordenes.status = 1
     AND sre_mordenes.lnktra = 0 
     
 -- Cerrando transaccion
 COMMIT WORK 
END FUNCTION 

-- Subrutina para verificar si tipo de documento requiere empleado

FUNCTION facing003_TipoDocumentoEmpleado(xlnktdc)
 DEFINE xlnktdc LIKE pos_mtransac.lnktdc,
        xhayepl LIKE fac_tipodocs.hayepl 

 -- Verificando si tipo de documento requiere empleado
 SELECT NVL(x.hayepl,0)
  INTO  xhayepl
  FROM  fac_tipodocs x,fac_tdocxpos y
  WHERE y.lnktdc = xlnktdc
    AND x.tipdoc = y.tipdoc

 RETURN xhayepl
END FUNCTION 
