{
Programo : Mynor Ramirez 
Objetivo : Impresion de cuenta de la mesa 
}

-- Definicion de variables globales  

GLOBALS "facglb003.4gl" 
DEFINE fnt             RECORD
        cmp            CHAR(12),
        nrm            CHAR(12),
        tbl            CHAR(12),
        fbl,t88        CHAR(12),
        t66,p12        CHAR(12),
        p10,srp        CHAR(12),
        twd            CHAR(12),
        fwd            CHAR(12),
        tda,fda        CHAR(12),
        ini            CHAR(12)
       END RECorD,
       existe          SMALLINT, 
       filename        CHAR(90),
       pipeline        CHAR(90)

-- Subrutina para imprimir la cuenta de una mesa 

FUNCTION facrpt003a_GeneraCuenta(imp1)
 DEFINE imp1        RECORD 
         lnktdc     LIKE pos_mtransac.lnktdc, 
         numesa     LIKE pos_mtransac.numesa, 
         fecemi     LIKE pos_mtransac.fecemi, 
         totdoc     LIKE pos_mtransac.totdoc,
         totgra     LIKE pos_mtransac.totgra,
         totexe     LIKE pos_mtransac.totexe,
         totisv     LIKE pos_mtransac.totisv,
         totpag     LIKE pos_mtransac.totpag,
         totcta     LIKE pos_mtransac.totcta,
         propin     LIKE pos_mtransac.propin,
         userid     LIKE pos_mtransac.userid 
        END RECORD

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/CuentaMesa.spl"

 -- Obteniendo columna inicial para impresora
 LET ColIniImpCta = 1

 -- Asignando impresora de cuentas por punto de venta 
 SELECT a.impcmd
  INTO  w_mae_pos.impcmd 
  FROM  fac_puntovta a
  WHERE a.numpos = w_mae_tra.numpos
  IF (status=NOTFOUND) THEN
     LET pipeline = "local"
  ELSE
     LET pipeline = w_mae_pos.impcmd 
  END IF

 IF pipeline!="Ninguna" THEN 
  -- Seleccionando fonts para impresora epson
  CALL librut001_fontsprn(pipeline,"epson") 
  RETURNING fnt.* 

  -- Iniciando reporte
  START REPORT facrpt003a_ImprimirCuenta TO filename 
   OUTPUT TO REPORT facrpt003a_ImprimirCuenta(1,imp1.*)
  FINISH REPORT facrpt003a_ImprimirCuenta 

  -- Imprimiendo el reporte
  CALL librut001_sendreport(filename,pipeline,"","")
 END IF
END FUNCTION 

-- Subrutinar para generar la impresion de la cuenta 

REPORT facrpt003a_ImprimirCuenta(numero,imp2) 
 DEFINE imp1        RECORD LIKE fac_formaimp.*,
        imp2        RECORD 
         lnktdc     LIKE pos_mtransac.lnktdc, 
         numesa     LIKE pos_mtransac.numesa, 
         fecemi     LIKE pos_mtransac.fecemi, 
         totdoc     LIKE pos_mtransac.totdoc,
         totgra     LIKE pos_mtransac.totgra,
         totexe     LIKE pos_mtransac.totexe,
         totisv     LIKE pos_mtransac.totisv,
         totpag     LIKE pos_mtransac.totpag,
         totcta     LIKE pos_mtransac.totcta,
         propin     LIKE pos_mtransac.propin,
         userid     LIKE pos_mtransac.userid 
        END RECORD,
        xnomcaj     CHAR(40), 
        xnompos     CHAR(40), 
        xnumesa     CHAR(40), 
        i,col,coi   SMALLINT, 
        lg          SMALLINT, 
        cortepapel  CHAR(10), 
        wcanletras  STRING,
        texto       STRING,  
        numero      INT 

  OUTPUT LEFT   MARGIN 0 
         PAGE   LENGTH 5 
         TOP    MARGIN 0 
         BOTTOM MARGIN 0

 FORMAT 
  BEFORE GROUP OF numero 
   -- Imprimiendo encabezado
   LET wcanletras = librut001_numtolet(imp2.totdoc) 

   -- Inicializando impresora
   LET coi = ColIniImpCta 
   LET lg  = 42+coi 
   PRINT fnt.ini CLIPPED
   LET cortepapel = ASCII 29,ASCII 86,"0,48"

   -- Obteniendo datos del tipo de documento de facturacion
   INITIALIZE imp1.* TO NULL 
   SELECT a.* 
    INTO  imp1.* 
    FROM  fac_formaimp a
    WHERE a.lnktdc = imp2.lnktdc

   -- Obteniendo nombre del cajero
   INITIALIZE xnomcaj TO NULL 
   SELECT a.nomusr 
    INTO  xnomcaj 
    FROM  glb_usuarios a
    WHERE a.userid = imp2.userid 

   -- Encabezado
   -- Punto de Venta
   LET xnompos = w_mae_pos.nompos  
   LET col = librut001_centrado(xnompos,lg) 
   PRINT COLUMN col,xnompos CLIPPED
   SKIP 1 LINES 
   LET texto = "********** CALCULO DE CUENTA **********"
   PRINT COLUMN coi,texto CLIPPED 
   LET texto = "*** ESTA NO ES UNA FACTURA CONTABLE ***"
   PRINT COLUMN coi,texto CLIPPED 

   -- Datos de la cuenta 
   -- Numero de Mesa   
   LET xnumesa = "MESA No. ",imp2.numesa USING "<<<<<" 
   LET col = librut001_centrado(xnumesa,lg) 

   -- Fecha y cajero 
   PRINT COLUMN coi,"Fecha:   ",imp2.fecemi," ",TIME
   PRINT COLUMN coi,"Cajero:  ",xnomcaj CLIPPED
   PRINT COLUMN coi,xnumesa CLIPPED 

   -- Detalle de Platos Servidos 
   PRINT COLUMN coi,"Detalle de Platos Servidos" 
   PRINT COLUMN coi,"_______________________________________"
   PRINT COLUMN coi,"Plato   Cantidad    Precio       Valor " 
   PRINT COLUMN coi,"_______________________________________"
 
  ON EVERY ROW
   -- Imprimiendo detalle de platos 
   FOR i = 1 TO totlin
    IF v_cuenta[i].nomrec IS NULL OR 
       v_cuenta[i].cantid IS NULL OR 
       v_cuenta[i].totrec IS NULL THEN 
       CONTINUE FOR
    END IF 

    PRINT COLUMN coi,v_cuenta[i].nomrec[1,38]
    PRINT COLUMN coi                            ,8 SPACES,
          v_cuenta[i].cantid  USING "####&.&&"  ,2 SPACES, 
          v_cuenta[i].prevta  USING "#,##&.&&"  ,3 SPACES, 
          v_cuenta[i].totrec  USING "##,##&.&&" 
   END FOR   

  ON LAST ROW
   -- Imprimiendo totales 
   PRINT COLUMN coi,"_______________________________________"
   PRINT COLUMN coi,"TOTAL DE LA CUENTA       Q. ",imp2.totcta   USING "###,##&.&&"
   PRINT COLUMN coi,"PROPINA                  Q. ",imp2.propin   USING "###,##&.&&"
   PRINT COLUMN coi,"SU TOTAL                 Q. ",imp2.totpag   USING "###,##&.&&"
   SKIP 1 LINES 
   LET texto = "*** ESTA NO ES UNA FACTURA CONTABLE ***"
   PRINT COLUMN coi,texto CLIPPED 

   -- Datos de cliente 
   PRINT COLUMN coi,"PARA SU FACTURA POR FAVOR ANOTE NOMBRE"
   PRINT COLUMN coi,"Y N.I.T A CONTINUACION A AL DORSO."
   SKIP 1 LINES 
   PRINT COLUMN coi,"Cliente:"
   SKIP 1 LINES 
   PRINT COLUMN coi,"_______________________________________"
   SKIP 1 LINES 
   PRINT COLUMN coi,"_______________________________________"
   SKIP 1 LINES 
   -- Numero de Registro Tributario 
   PRINT COLUMN coi,DescripcionNT CLIPPED,":  "
   SKIP 1 LINES 
   PRINT COLUMN coi,"_______________________________________"
   SKIP 1 LINES 

   -- Imprimiendo pie 
   SKIP 1 LINES
   FOR i = 1 TO 8 
    PRINT " "
   END FOR 
   PRINT cortepapel CLIPPED
END REPORT 
