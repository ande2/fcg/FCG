--drop view vis_consumosinvxi;

CREATE VIEW vis_consumosinvxi
(numpos,fecemi,codcat,nomcat,cditem,codabr,dsitem,unimed,nommed,cantid) AS

SELECT x.numpos,x.feccor,c.codcat,e.nomcat,c.cditem,c.codabr,c.dsitem,
       c.unimed,d.nommed,(y.cantid*z.canuni)
FROM   pos_mtransac x,pos_dtransac y,sre_mrecetas a,sre_drecetas z,
       inv_products c,inv_unimedid d,glb_categors e
WHERE  x.lnktra = y.lnktra
  AND  x.estado = 1
  AND  z.lnkrec = y.lnkrec
  AND  a.lnkrec = z.lnkrec
  AND  c.cditem = z.cditem
  AND  d.unimed = c.unimed
  AND  e.codcat = c.codcat

union all

SELECT x.numpos,x.fecemi,c.codcat,e.nomcat,c.cditem,c.codabr,c.dsitem,
       c.unimed,d.nommed,(y.cantid*z.canuni)
FROM   sre_mordenes x,sre_dplaspre y,sre_mrecetas a,sre_drecetas z,
       inv_products c,inv_unimedid d,glb_categors e
WHERE  x.lnkord = y.lnkord
  AND  x.estado = 1
  AND  x.status = 0
  AND  z.lnkrec = y.lnkrec
  AND  a.lnkrec = z.lnkrec
  AND  c.cditem = z.cditem
  AND  d.unimed = c.unimed
  AND  e.codcat = c.codcat ;

grant select on vis_consumosinvxi to public;
