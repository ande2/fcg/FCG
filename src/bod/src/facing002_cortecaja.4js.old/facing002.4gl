{
facing002.4gl 
Mynor Ramirez
Corte de caja    
}

-- Definicion de variables globales 

GLOBALS "facglb002.4gl"
CONSTANT maximoval = 999999.99
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 DEFINE regreso SMALLINT 

 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/ToolBarCorteCaja")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("cortecaja")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Logeando cajero
 CALL facing002_login(1)
 RETURNING regreso,username

 -- Verificando regreso
 IF NOT regreso THEN
    -- Menu de opciones
    CALL facing002_menu()
 END IF
END MAIN

-- Subrutina para el menu principal 

FUNCTION facing002_menu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT,
        formname STRING, 
        cmdstr   STRING 

 -- Abriendo la ventana de mantenimiento
 LET formname = "facing002a"
 IF NOT seclib001_accesos(progname,3,username) THEN
    LET formname = "facing002e"
 END IF

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing002a AT 5,2
  WITH FORM formname ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais

  CALL librut001_header("facing002",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando puntos de venta 
  CALL librut003_CbxPuntosVenta() 

  -- Inicializando datos
  CALL facing002_inival(1)

  -- Menu de opciones
  MENU "Cortes de Caja"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Consultar" 
    END IF
     --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Ingresar"
    END IF
   ON ACTION consultar   
    CALL facqbx002_CortesCaja(1) 
   ON ACTION ingresar    
    LET savedata = facing002_CortesCaja(1) 
   ON ACTION salir
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing002a
END FUNCTION

-- Subrutina para la emision de un corte de caja   

FUNCTION facing002_CortesCaja(operacion) 
 DEFINE loop,existe,opc     SMALLINT,
        retroceso           SMALLINT,
        savedata,operacion  SMALLINT,
        msg                 CHAR(80),
        qrytext             STRING,
        conteo              INT 

 -- Verificando si opcion es nuevo ingreso
 OPTIONS INPUT WRAP 

 IF (operacion=1) THEN
    CALL facqbx002_EstadoMenu(4,"")
    LET retroceso = FALSE
 ELSE
    CALL facqbx002_EstadoMenu(2,"")
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     CALL facing002_inival(1)
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.observ,
                w_mae_pro.fonini, 
                w_mae_pro.toecie,
                w_mae_pro.ttacie 
                WITHOUT DEFAULTS 
                ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   ON ACTION accept 
    -- Verificando que exista grabado fondo inicial
    IF w_mae_pro.fonini=0 THEN
       CALL fgl_winmessage(
       "Atencion",
       "Debe registrase desgloce inicial para poder grabar, VERIFICA.",
       "stop")
       CONTINUE INPUT 
    END IF 
    EXIT INPUT 

   ON ACTION fondoini 
    NEXT FIELD fonini 

   ON ACTION totefecie 
    NEXT FIELD toecie

   ON ACTION tottarcie 
    NEXT FIELD ttacie

   ON ACTION egresos 
    -- Mostrando egresos 
    CALL facing002_EgresosCorte() 

    -- Totalizando egresos 
    CALL facing002_TotalEgresos()    
    DISPLAY BY NAME w_mae_pro.totgas 

    -- Totalizando corte 
    CALL facing002_TotalCorte()

   BEFORE FIELD observ 
    -- Verificando si ya existe corte del mismo dia
    IF (operacion=1) THEN
       IF facing002_BCorteCaja() THEN 
          LET loop = FALSE 
          EXIT INPUT    
       END IF 
    END IF 

    -- Totalizando ventas de la fecha de corte 
    CALL facing002_TotalVentas()
    -- Totalizando corte 
    CALL facing002_TotalCorte()
    LET retroceso = TRUE 

   AFTER FIELD fonini 
    -- Verificando dato
    IF w_mae_pro.fonini IS NULL OR 
       w_mae_pro.fonini <0 THEN
       LET w_mae_pro.fonini =  0
    END IF 
    DISPLAY BY NAME w_mae_pro.fonini 

   AFTER FIELD toecie
    -- Verificando efectivo
    IF w_mae_pro.toecie IS NULL OR
       w_mae_pro.toecie <0 OR
       w_mae_pro.toecie >maximoval THEN
       LET w_mae_pro.toecie = 0
       DISPLAY BY NAME w_mae_pro.toecie
    END IF

    -- Verificando que exista total corte para ingresar total cierre
    IF w_mae_pro.totcor=0 AND
       w_mae_pro.toecie>0 THEN
       CALL fgl_winmessage(
       "Atencion:",
       "Total corte debe ser mayor que cero "||
       "para poder registrar total efectivo cierre, VERIFICA.",
       "stop")
       LET w_mae_pro.toecie = 0
       DISPLAY BY NAME w_mae_pro.toecie
       NEXT FIELD toecie
    END IF

    -- Totalizando corte 
    CALL facing002_TotalCorte()

   AFTER FIELD ttacie
    -- Verificando tarjeta
    IF w_mae_pro.ttacie IS NULL OR
       w_mae_pro.ttacie <0 OR
       w_mae_pro.ttacie >maximoval THEN
       LET w_mae_pro.ttacie = 0
       DISPLAY BY NAME w_mae_pro.ttacie
    END IF

    -- Totalizando corte 
    CALL facing002_TotalCorte()

  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Totalizando corte 
  CALL facing002_TotalCorte()

  -- Verificando total del corte
  LET retroceso = FALSE 
  IF (w_mae_pro.totcor<0) THEN
     CALL fgl_winmessage(
     "Atencion",
     "Total del corte debe ser mayor que cero, VERIFICA.",
     "stop") 
     LET retroceso = TRUE 
     CONTINUE WHILE
  END IF 
    
  -- Menu de opciones
  LET savedata = FALSE 
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL facing002_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificar 
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE
 OPTIONS INPUT NO WRAP 

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL facqbx002_EstadoMenu(0,"")
    CALL facing002_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para verificar si um corte ya existe registrado

FUNCTION facing002_BCorteCaja()
 DEFINE conteo INT

 SELECT COUNT(*) 
  INTO  conteo
  FROM  fac_vicortes x
  WHERE (x.numpos = w_mae_pro.numpos) 
   AND  (x.feccor = w_mae_pro.feccor) 
  IF (conteo>0) THEN 
     CALL fgl_winmessage(
     "Atencion",
     "Fecha de corte del "||w_mae_pro.feccor||" ya registrada.\n"||
     "Utilizar opcion modificar si se desean hacer cambios.",
     "information") 
     RETURN TRUE 
  ELSE 
     RETURN FALSE 
  END IF 
END FUNCTION 

-- Subrutina para grabar el corte de caja 

FUNCTION facing002_grabar(operacion)
 DEFINE msg                 CHAR(80),
        operacion,correl,i  SMALLINT 

 -- Grabando transaccion
 ERROR " Registrando corte de caja ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

  CASE (operacion)
   WHEN 1 -- Grabando 
    -- Asignando datos
    LET w_mae_pro.lnkcor = 0

    -- Grabando encabezado 
    SET LOCK MODE TO WAIT
    INSERT INTO fac_vicortes   
    VALUES (w_mae_pro.*)
    LET w_mae_pro.lnkcor = SQLCA.SQLERRD[2]

    --Asignando el mensaje 
    LET msg = "Corte de Caja del ",w_mae_pro.feccor," registrado."

   WHEN 2 -- Modificando 
    -- Modificando
    SET LOCK MODE TO WAIT
    UPDATE fac_vicortes
    SET    fac_vicortes.fonini = w_mae_pro.fonini,
           fac_vicortes.totvta = w_mae_pro.totvta,
           fac_vicortes.totgas = w_mae_pro.totgas,
           fac_vicortes.totcor = w_mae_pro.totcor,
           fac_vicortes.totefe = w_mae_pro.totefe,
           fac_vicortes.tottar = w_mae_pro.tottar,
           fac_vicortes.totcon = w_mae_pro.totcon,
           fac_vicortes.totcre = w_mae_pro.totcre,
           fac_vicortes.totcos = w_mae_pro.totcos,
           fac_vicortes.totdon = w_mae_pro.totdon,
           fac_vicortes.totisv = w_mae_pro.totisv, 
           fac_vicortes.totdes = w_mae_pro.totdes,
           fac_vicortes.bil500 = w_mae_pro.bil500,
           fac_vicortes.bil100 = w_mae_pro.bil100,
           fac_vicortes.bil050 = w_mae_pro.bil050,
           fac_vicortes.bil020 = w_mae_pro.bil020,
           fac_vicortes.bil010 = w_mae_pro.bil010,
           fac_vicortes.bil005 = w_mae_pro.bil005,
           fac_vicortes.bil002 = w_mae_pro.bil002,
           fac_vicortes.bil001 = w_mae_pro.bil001,
           fac_vicortes.mon050 = w_mae_pro.mon050,
           fac_vicortes.mon020 = w_mae_pro.mon020,
           fac_vicortes.mon010 = w_mae_pro.mon010,
           fac_vicortes.mon005 = w_mae_pro.mon005,
           fac_vicortes.cbi500 = w_mae_pro.cbi500,
           fac_vicortes.cbi100 = w_mae_pro.cbi100,
           fac_vicortes.cbi050 = w_mae_pro.cbi050,
           fac_vicortes.cbi020 = w_mae_pro.cbi020,
           fac_vicortes.cbi010 = w_mae_pro.cbi010,
           fac_vicortes.cbi005 = w_mae_pro.cbi005,
           fac_vicortes.cbi002 = w_mae_pro.cbi002,
           fac_vicortes.cbi001 = w_mae_pro.cbi001,
           fac_vicortes.cmo050 = w_mae_pro.cmo050,
           fac_vicortes.cmo020 = w_mae_pro.cmo020,
           fac_vicortes.cmo010 = w_mae_pro.cmo010,
           fac_vicortes.cmo005 = w_mae_pro.cmo005,
           fac_vicortes.observ = w_mae_pro.observ, 
           fac_vicortes.toecor = w_mae_pro.toecor, 
           fac_vicortes.toecie = w_mae_pro.toecie,
           fac_vicortes.totdif = w_mae_pro.totdif,
           fac_vicortes.ttacie = w_mae_pro.ttacie,
           fac_vicortes.todita = w_mae_pro.todita
    WHERE  fac_vicortes.lnkcor = w_mae_pro.lnkcor 

    --Asignando el mensaje 
    LET msg = "Corte de Caja del ",w_mae_pro.feccor," actualizado."

   WHEN 3 -- Eliminando 
    -- Eliminando 
    SET LOCK MODE TO WAIT
    DELETE FROM fac_vicortes
    WHERE fac_vicortes.lnkcor = w_mae_pro.lnkcor 

    --Asignando el mensaje 
    LET msg = "Corte de Caja del ",w_mae_pro.feccor," eliminado."

   WHEN 4 -- Cerrando 
    -- Cerrando             
    SET LOCK MODE TO WAIT
    UPDATE fac_vicortes
    SET    fac_vicortes.fonini = w_mae_pro.fonini,
           fac_vicortes.totvta = w_mae_pro.totvta,
           fac_vicortes.totgas = w_mae_pro.totgas,
           fac_vicortes.totcor = w_mae_pro.totcor,
           fac_vicortes.totefe = w_mae_pro.totefe,
           fac_vicortes.tottar = w_mae_pro.tottar,
           fac_vicortes.totcon = w_mae_pro.totcon,
           fac_vicortes.totcre = w_mae_pro.totcre,
           fac_vicortes.totcos = w_mae_pro.totcos,
           fac_vicortes.totdon = w_mae_pro.totdon, 
           fac_vicortes.totisv = w_mae_pro.totisv, 
           fac_vicortes.totdes = w_mae_pro.totdes,
           fac_vicortes.bil500 = w_mae_pro.bil500,
           fac_vicortes.bil100 = w_mae_pro.bil100,
           fac_vicortes.bil050 = w_mae_pro.bil050,
           fac_vicortes.bil020 = w_mae_pro.bil020,
           fac_vicortes.bil010 = w_mae_pro.bil010,
           fac_vicortes.bil005 = w_mae_pro.bil005,
           fac_vicortes.bil002 = w_mae_pro.bil002,
           fac_vicortes.bil001 = w_mae_pro.bil001,
           fac_vicortes.mon050 = w_mae_pro.mon050,
           fac_vicortes.mon020 = w_mae_pro.mon020,
           fac_vicortes.mon010 = w_mae_pro.mon010,
           fac_vicortes.mon005 = w_mae_pro.mon005,
           fac_vicortes.cbi500 = w_mae_pro.cbi500,
           fac_vicortes.cbi100 = w_mae_pro.cbi100,
           fac_vicortes.cbi050 = w_mae_pro.cbi050,
           fac_vicortes.cbi020 = w_mae_pro.cbi020,
           fac_vicortes.cbi010 = w_mae_pro.cbi010,
           fac_vicortes.cbi005 = w_mae_pro.cbi005,
           fac_vicortes.cbi002 = w_mae_pro.cbi002,
           fac_vicortes.cbi001 = w_mae_pro.cbi001,
           fac_vicortes.cmo050 = w_mae_pro.cmo050,
           fac_vicortes.cmo020 = w_mae_pro.cmo020,
           fac_vicortes.cmo010 = w_mae_pro.cmo010,
           fac_vicortes.cmo005 = w_mae_pro.cmo005,
           fac_vicortes.observ = w_mae_pro.observ, 
           fac_vicortes.toecor = w_mae_pro.toecor, 
           fac_vicortes.toecie = w_mae_pro.toecie,
           fac_vicortes.totdif = w_mae_pro.totdif, 
           fac_vicortes.ttacie = w_mae_pro.ttacie, 
           fac_vicortes.todita = w_mae_pro.todita, 
           fac_vicortes.cierre = 1,
           fac_vicortes.usrcie = USER,
           fac_vicortes.feccie = CURRENT,
           fac_vicortes.horcie = CURRENT HOUR TO SECOND
    WHERE  fac_vicortes.lnkcor = w_mae_pro.lnkcor 

    -- Asignando el mensaje 
    LET msg = "Corte de Caja del ",w_mae_pro.feccor," cerrado."
  END CASE 

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Verificando operacion 
 IF (operacion!=4) THEN  
  -- Desplegando mensaje
  CALL fgl_winmessage("Atencion",msg,"information")

  -- Inicializando datos
  CALL facing002_inival(1)
 END IF  
END FUNCTION

-- Totalizando el corte de caja    

FUNCTION facing002_TotalCorte()
 -- Totalizando
 LET w_mae_pro.totcor = (w_mae_pro.fonini+w_mae_pro.totvta)-w_mae_pro.totgas
 DISPLAY BY NAME w_mae_pro.totcor 

 -- Calculando diferencia
 LET w_mae_pro.toecor = (w_mae_pro.totcor-w_mae_pro.tottar) 
 LET w_mae_pro.totdif = (w_mae_pro.toecor-w_mae_pro.toecie) 
 DISPLAY BY NAME w_mae_pro.totdif,w_mae_pro.toecor 

 IF (w_mae_pro.totdif>0) THEN
    CALL librut001_dpelement("lg05c","Total Faltante:") 
 ELSE
    IF (w_mae_pro.totdif<0) THEN
       CALL librut001_dpelement("lg05c","Total Sobrante:") 
    ELSE
       CALL librut001_dpelement("lg05c","Total Diferencia:") 
    END IF
 END IF

 -- Calculando diferencia tarjeta
 LET w_mae_pro.todita = (w_mae_pro.tottar-w_mae_pro.ttacie)
 DISPLAY BY NAME w_mae_pro.todita
 DISPLAY w_mae_pro.tottar TO ttacor

 IF (w_mae_pro.todita>0) THEN
    CALL librut001_dpelement("lg06c","Total Faltante:")
 ELSE
    IF (w_mae_pro.todita<0) THEN
       CALL librut001_dpelement("lg06c","Total Sobrante:")
    ELSE
       CALL librut001_dpelement("lg06c","Total Diferencia:")
    END IF
 END IF
END FUNCTION 

-- Subrutina para obtener el total de ventas 

FUNCTION facing002_TotalVentas()
 -- Totalizando ventas totales
 LET w_mae_pro.totvta = 0 
 LET w_mae_pro.totisv = 0 
 LET w_mae_pro.totdes = 0 
 SELECT NVL(SUM(x.totdoc),0),NVL(SUM(x.totiva),0),NVL(SUM(x.totdes),0) 
  INTO  w_mae_pro.totvta,w_mae_pro.totisv,w_mae_pro.totdes 
  FROM  fac_mtransac x,fac_tdocxpos z 
  WHERE x.numpos = w_mae_pro.numpos
    AND x.feccor = w_mae_pro.feccor  
    AND x.estado = "V" 
    AND x.credit IS NOT NULL 
    AND z.lnktdc = x.lnktdc 
    AND z.haycor = 1

 -- Totalizando ventas de contado 
 LET w_mae_pro.totcon = 0 
 SELECT NVL(SUM(x.totdoc),0) 
  INTO  w_mae_pro.totcon 
  FROM  fac_mtransac x,fac_tdocxpos z 
  WHERE x.numpos = w_mae_pro.numpos
    AND x.feccor = w_mae_pro.feccor  
    AND x.estado = "V" 
    AND x.credit = 0  
    AND z.lnktdc = x.lnktdc 
    AND z.haycor = 1

 -- Totalizando ventas de credito 
 LET w_mae_pro.totcre = 0 
 SELECT NVL(SUM(x.totdoc),0) 
  INTO  w_mae_pro.totcre 
  FROM  fac_mtransac x,fac_tdocxpos z 
  WHERE x.numpos = w_mae_pro.numpos
    AND x.feccor = w_mae_pro.feccor  
    AND x.estado = "V" 
    AND x.credit = 1  
    AND z.lnktdc = x.lnktdc 
    AND z.haycor = 1

 -- Totalizando efectivo y tarjeta de credito 
 LET w_mae_pro.totefe = 0 
 LET w_mae_pro.tottar = 0 
 SELECT NVL(SUM(x.efecti),0), 
        NVL(SUM(x.tarcre),0) 
  INTO  w_mae_pro.totefe,
        w_mae_pro.tottar 
  FROM  fac_mtransac x,fac_tdocxpos z 
  WHERE x.numpos = w_mae_pro.numpos
    AND x.feccor = w_mae_pro.feccor  
    AND x.estado = "V" 
    AND x.credit IS NOT NULL 
    AND z.lnktdc = x.lnktdc 
    AND z.haycor = 1

 -- Totalizando ventas por consumo de empleados
 LET w_mae_pro.totcos = 0 
 {SELECT NVL(SUM(x.totdoc),0) 
  INTO  w_mae_pro.totcos 
  FROM  fac_mtransac x
  WHERE x.numpos = w_mae_pro.numpos
    AND x.feccor = w_mae_pro.feccor  
    AND x.estado = "V" 
    AND x.haycor = 0
    AND x.csmepl = 1}

 -- Totalizando ventas por donaciones                
 LET w_mae_pro.totdon = 0 
 {SELECT NVL(SUM(x.totdoc),0) 
  INTO  w_mae_pro.totdon 
  FROM  fac_mtransac x
  WHERE x.numpos = w_mae_pro.numpos
    AND x.feccor = w_mae_pro.feccor  
    AND x.estado = "V" 
    AND x.haycor = 0
    AND x.regali = 1}

 DISPLAY BY NAME w_mae_pro.totvta,w_mae_pro.totefe,w_mae_pro.tottar,
                 w_mae_pro.totcon,w_mae_pro.totcre,w_mae_pro.totcos,
                 w_mae_pro.totdon,w_mae_pro.totdes,w_mae_pro.totisv 
 DISPLAY w_mae_pro.tottar TO ttacor
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION facing002_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   -- Inicializando datos 
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.numpos = xnumpos 
   LET w_mae_pro.tipcor = 1   
   LET w_mae_pro.fonini = 0
   LET w_mae_pro.totvta = 0
   LET w_mae_pro.totgas = 0
   LET w_mae_pro.totcor = 0
   LET w_mae_pro.totisv = 0
   LET w_mae_pro.totdes = 0
   LET w_mae_pro.totefe = 0
   LET w_mae_pro.tottar = 0
   LET w_mae_pro.bil500 = 0 
   LET w_mae_pro.bil100 = 0 
   LET w_mae_pro.bil200 = 0 
   LET w_mae_pro.bil050 = 0 
   LET w_mae_pro.bil020 = 0 
   LET w_mae_pro.bil010 = 0 
   LET w_mae_pro.bil005 = 0 
   LET w_mae_pro.bil002 = 0 
   LET w_mae_pro.bil001 = 0 
   LET w_mae_pro.mon050 = 0 
   LET w_mae_pro.mon020 = 0 
   LET w_mae_pro.mon025 = 0 
   LET w_mae_pro.mon010 = 0 
   LET w_mae_pro.mon005 = 0 
   LET w_mae_pro.mon001 = 0 
   LET w_mae_pro.toecie = 0
   LET w_mae_pro.cbi500 = 0 
   LET w_mae_pro.cbi100 = 0 
   LET w_mae_pro.cbi200 = 0 
   LET w_mae_pro.cbi050 = 0 
   LET w_mae_pro.cbi020 = 0 
   LET w_mae_pro.cbi010 = 0 
   LET w_mae_pro.cbi005 = 0 
   LET w_mae_pro.cbi002 = 0 
   LET w_mae_pro.cbi001 = 0 
   LET w_mae_pro.cmo001 = 0 
   LET w_mae_pro.cmo050 = 0 
   LET w_mae_pro.cmo020 = 0 
   LET w_mae_pro.cmo025 = 0 
   LET w_mae_pro.cmo010 = 0 
   LET w_mae_pro.cmo005 = 0 
   LET w_mae_pro.feccor = CURRENT 
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME")
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   LET w_mae_pro.cierre = 0  
   LET w_mae_pro.toecor = 0 
   LET w_mae_pro.totcon = 0 
   LET w_mae_pro.totcre = 0 
   LET w_mae_pro.totcos = 0 
   LET w_mae_pro.totdon = 0 
   LET w_mae_pro.totdif = 0 
   LET w_mae_pro.ttacie = 0 
   LET w_mae_pro.todita = 0 

   -- Inicializando vector de egresos 
   LET totdoc = 0 
   CALL v_doctos.clear()
   CALL librut001_dpelement("lg05c","Total Diferencia:") 
   CALL librut001_dpelement("lg06c","Total Diferencia:") 
   CLEAR FORM
   DISPLAY BY NAME w_mae_pro.numpos 
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.feccor,w_mae_pro.totvta,w_mae_pro.totgas,
                 w_mae_pro.totgas,w_mae_pro.totcor,w_mae_pro.totefe,
                 w_mae_pro.tottar,w_mae_pro.totdes,w_mae_pro.totisv,
                 w_mae_pro.toecie,w_mae_pro.fonini,w_mae_pro.totcon,
                 w_mae_pro.totcre,w_mae_pro.totcos,w_mae_pro.totdon,
                 w_mae_pro.totdif,w_mae_pro.ttacie,w_mae_pro.todita 
 DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION

-- Subrutina para el desplegar los egresos de corte

FUNCTION facing002_EgresosCorte()                   
 DEFINE i,arr,scr SMALLINT,
        lastkey   INTEGER 

 -- Abriendo ventana
 OPEN WINDOW wing002b AT 5,2
  WITH FORM "facing002b" 
  ATTRIBUTE(BORDER,
            TEXT="Egresos del Corte de Caja del "||w_mae_pro.feccor)

  -- Totalizando 
  CALL facing002_TotalEgresos()

  -- Ingresando datos
  DISPLAY ARRAY v_doctos TO s_doctos.*
   ATTRIBUTE(COUNT=totdoc) 
   ON ACTION accept
    -- Salir
    EXIT DISPLAY
  END DISPLAY 
 CLOSE WINDOW wing002b 
END FUNCTION 

-- Totalizando egresos                 

FUNCTION facing002_TotalEgresos()
 DEFINE i SMALLINT

 -- Totalizando
 LET w_mae_pro.totgas = 0 
 FOR i = 1 TO totdoc
  IF v_doctos[i].totval IS NULL THEN
     CONTINUE FOR
  END IF
  LET w_mae_pro.totgas = (w_mae_pro.totgas+v_doctos[i].totval) 
 END FOR
 DISPLAY BY NAME w_mae_pro.totgas 
END FUNCTION

-- Subrutina para ingresar el cajero y el punto de venta

FUNCTION facing002_login(opc)
 DEFINE x         RECORD 
         userid   CHAR(15),
         passwd   CHAR(10)
        END RECORD,
        wpais     VARCHAR(255), 
        wfecha    VARCHAR(80), 
        xnompos   VARCHAR(40), 
        msg       STRING, 
        regreso   SMALLINT,
        loop      SMALLINT,
        conteo    SMALLINT,
        opc       SMALLINT,
        w2        ui.Window,
        f2        ui.Form

 -- Abriendo la ventana del login
 OPEN WINDOW wing002p AT 5,2  
  WITH FORM "facing002p" ATTRIBUTE(BORDER)

  -- Desplegando fecha 
  LET wfecha = librut001_formatofecha(TODAY,1,wpais) CLIPPED
  CALL librut001_dpelement("fecdia",wfecha)

  -- Desactivando salida automatica del input 
  OPTIONS INPUT WRAP 

  -- Escondiendo punto de venta
  LET w2 = ui.Window.getCurrent()
  LET f2 = w2.getForm()
  CALL f2.setFieldHidden("formonly.numpos",1)
  CALL f2.setElementHidden("labelx",1)
  CALL f2.setElementHidden("labelb",1)
  CALL f2.setElementHidden("labelc",1)

  -- Verificando si cajero es ingresado o tomado del sistema 
  INITIALIZE x.* TO NULL 
  IF (opc=1) THEN 
     -- Obteniendo cajero logeado
     LET x.userid = FGL_GETENV("LOGNAME")
     DISPLAY BY NAME x.userid 
  END IF 

  -- Ingresando datos
  LET loop = TRUE
  WHILE loop
   INPUT BY NAME x.userid,
                 x.passwd WITHOUT DEFAULTS 
    ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)
   
    ON ACTION cancelar
     -- Salida 
     LET regreso = TRUE
     LET loop    = FALSE 
     EXIT INPUT 

    ON KEY(F4,CONTROL-E) 
     -- Salida 
     LET regreso = TRUE 
     LET loop    = FALSE 
     EXIT INPUT 

    ON ACTION aceptar  
     -- Verificando datos
     IF x.userid IS NULL THEN
        ERROR "Error: cajero invalido, VERIFICA."
        NEXT FIELD userid
     END IF 
     IF x.passwd IS NULL THEN
        ERROR "Error: password invalido, VERIFICA."
        NEXT FIELD passwd
     END IF 

     LET regreso = FALSE
     LET loop    = TRUE 
     EXIT INPUT 

    BEFORE INPUT
     -- Verificando si cajero es ingresado o tomado del sistema 
     IF (opc=1) THEN
        CALL Dialog.SetFieldActive("userid",0) 
     END IF 

    AFTER FIELD userid 
     -- Verificando cajero 
     IF (LENGTH(x.userid)=0) THEN 
        ERROR "Error: cajero invalido."
        NEXT FIELD userid
     END IF 

    AFTER FIELD passwd
     -- Verificando password
     IF (LENGTH(x.passwd)=0) THEN 
        ERROR "Error: password invalido."
        NEXT FIELD passwd
     END IF 

     EXIT INPUT  
   END INPUT

   -- Verificando puntos de venta por cajero
   IF loop THEN 
    -- Buscando puntos de venta por cajero 
    SELECT COUNT(*)
     INTO  conteo 
     FROM  fac_usuaxpos a
     WHERE a.userid = x.userid
     IF (conteo=0) THEN
        LET msg = "Cajero (",x.userid CLIPPED,
                  ") no tiene puntos de venta asignados."||
                  "\n VERIFICA."

        CALL fgl_winmessage("Atencion",msg,"stop")
        CONTINUE WHILE 
     ELSE
        LET xnumpos = NULL
        SELECT a.numpos 
         INTO  xnumpos   
         FROM  fac_usuaxpos a,fac_puntovta y
         WHERE a.numpos = y.numpos 
           AND a.userid = x.userid
           AND a.passwd = x.passwd
         IF (status=NOTFOUND) THEN
            ERROR "Atencion: password incorrecto."
            CONTINUE WHILE  
         END IF 
         LET loop = FALSE 
     END IF 
   END IF 
  END WHILE

  -- Activando salida automatica del input 
  OPTIONS INPUT NO WRAP 
  
 CLOSE WINDOW wing002p 

 RETURN regreso,x.userid 
END FUNCTION 
