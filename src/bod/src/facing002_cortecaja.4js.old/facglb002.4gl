{ 
facglb002.4gl
Mynor Ramirez
Corte de caja   
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "facing002"
DEFINE w_mae_pro   RECORD LIKE fac_vicortes.*,
       v_cortes    DYNAMIC ARRAY OF RECORD
        tlnkcor    LIKE fac_vicortes.lnkcor,
        tfeccor    LIKE fac_vicortes.feccor,
        tfonini    LIKE fac_vicortes.fonini,
        ttotvta    LIKE fac_vicortes.totvta,
        ttotgas    LIKE fac_vicortes.totgas,
        ttotcor    LIKE fac_vicortes.totcor,
        tcierre    CHAR(15), 
        relleno    CHAR(1) 
       END RECORD,
       v_doctos    DYNAMIC ARRAY OF RECORD
        fecemi     DATE,
        numdoc     CHAR(20),
        totval     DEC(9,2),     
        codaut     LIKE fac_dvicorte.codaut, 
        observ     CHAR(100),
        rellen     CHAR(1)
       END RECORD,
       xnumpos     LIKE fac_mtransac.numpos,
       username    CHAR(15), 
       totdoc      SMALLINT,
       xfonini     LIKE fac_vicortes.totcor,
       xtoecie     LIKE fac_vicortes.toecie
END GLOBALS
