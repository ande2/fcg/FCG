{
glbmae011.4gl 
Mynor Ramirez 
Mantenimiento de usuarios 
}

-- Definicion de variables globales 

GLOBALS "glbglo011.4gl"
DEFINE xpasswd CHAR(20) 

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("usuarios")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Obteniendo nombre del usuario 
 LET username = 'sistemas' --FGL_GETENV("LOGNAME")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL glbmae011_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION glbmae011_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        existe   SMALLINT, 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "glbmae011a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("glbmae011",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Usuarios"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Buscar"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    "Busqueda de usuarios."
    CALL glbqbe011_usuarios(1) 
   COMMAND "Nuevo"
    "Ingreso de un nuevo usuario."
    LET savedata = glbmae011_usuarios(1) 
   COMMAND "Modificar"
    "Modificacion de un usuario existente."
    CALL glbqbe011_usuarios(2) 
   COMMAND "Borrar"
    "Eliminacion de un usuario existente."
    CALL glbqbe011_usuarios(3) 
   COMMAND "Salir"
    "Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION glbmae011_usuarios(operacion)
 DEFINE w_mae_usr         RECORD LIKE glb_usuarios.*,
        loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL glbqbe011_EstadoMenu(4,"") 
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL glbmae011_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.userid,
                w_mae_pro.nomusr,
                w_mae_pro.email1,
                w_mae_pro.roleid,
                w_mae_pro.usupwd
                --xpasswd 
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   {BEFORE INPUT  
    -- Verificando tipo de operacion
    IF (operacion=1) THEN
       CALL Dialog.SetFieldActive("xpasswd",0) 
    ELSE
       CALL Dialog.SetFieldActive("xpasswd",1) 
    END IF} 

   BEFORE FIELD userid 
    -- Verificando si no es ingreso
    IF (operacion=2) THEN
       NEXT FIELD nomusr
    END IF 

   AFTER FIELD userid
    --Verificando usuario
    IF (LENGTH(w_mae_pro.userid)=0) THEN
       ERROR "Error: nombre del usuario invalido, VERIFICA."
       LET w_mae_pro.userid = NULL
       CLEAR userid
       NEXT FIELD userid
    END IF

    -- Verificando si el usuario existe registrado
    INITIALIZE w_mae_usr.* TO NULL
    CALL librut003_busuario(w_mae_pro.userid)
    RETURNING w_mae_usr.*,existe
    IF existe THEN
       ERROR "Error: usuario ya existe registrado, VERIFICA."
       NEXT FIELD userid
    END IF 

   AFTER FIELD nomusr  
    --Verificando nombre del usuario
    IF (LENGTH(w_mae_pro.nomusr)=0) THEN
       ERROR "Error: nombre del usuario invalido, VERIFICA."
       LET w_mae_pro.nomusr = NULL
       NEXT FIELD nomusr  
    END IF

    -- Verificando que no exista otro usuario con el mismo nombre
    SELECT UNIQUE (a.userid)
     FROM  glb_usuarios a
     WHERE (a.userid != w_mae_pro.userid) 
       AND (a.nomusr  = w_mae_pro.nomusr) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        "Atencion",
        "Existe otro usuario con el mismo nombre, VERIFICA.",
        "information")
        NEXT FIELD nomusr
     END IF 
   
   AFTER FIELD roleid
    -- Verificando perfil
    IF w_mae_pro.roleid IS NULL THEN
       ERROR "Error: perfil invalido, VERIFICA."
       NEXT FIELD roleid
    END IF 
   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.userid IS NULL THEN 
       NEXT FIELD userid
    END IF
    IF w_mae_pro.nomusr IS NULL THEN 
       NEXT FIELD nomusr
    END IF
    IF w_mae_pro.roleid IS NULL THEN 
       NEXT FIELD roleid 
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")
  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL glbmae011_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando usuario
    CALL glbmae011_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL glbqbe011_EstadoMenu(0,"") 
    CALL glbmae011_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un usuario

FUNCTION glbmae011_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando usuario ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO glb_usuarios   
   VALUES (w_mae_pro.*)

   --Asignando el mensaje 
   LET msg = "Usuario (",w_mae_pro.userid CLIPPED,") registrado."

   -- Creando usuario en el sistema operativo 
   CALL fgl_system("mkusr "||w_mae_pro.userid CLIPPED||" '"||w_mae_pro.nomusr CLIPPED||"'")

  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE glb_usuarios
   SET    glb_usuarios.*      = w_mae_pro.*
   WHERE  glb_usuarios.userid = w_mae_pro.userid 

   --Asignando el mensaje 
   LET msg = "Usuario (",w_mae_pro.userid CLIPPED,") actualizado."

   -- Actualizando password del usuario           
   {IF xpasswd IS NOT NULL THEN 
      CALL fgl_system("chpass "||w_mae_pro.userid CLIPPED||" "||xpasswd CLIPPED)
   END IF} 
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   -- Borrando usuarios
   DELETE FROM glb_usuarios 
   WHERE (glb_usuarios.userid = w_mae_pro.userid)

   --Asignando el mensaje 
   LET msg = "Usuario (",w_mae_pro.userid CLIPPED,") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL glbmae011_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION glbmae011_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.*,xpasswd TO NULL
   LET w_mae_pro.tipusu = 1 
   LET w_mae_pro.usuaid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Cargando perfil de usuario
 CALL librut003_cbxperfiles() 

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.userid,w_mae_pro.nomusr,
                 w_mae_pro.email1,w_mae_pro.roleid 
 DISPLAY BY NAME w_mae_pro.userid,w_mae_pro.usuaid THRU w_mae_pro.horsis 
END FUNCTION
