{
invqbe017.4gl 
Mynor Ramirez
Mantenimiento de puntos de venta
}

-- Definicion de variables globales 

GLOBALS "invglo017.4gl" 
DEFINE totlin,totusr INT, 
       w             ui.Window,
       f             ui.Form

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe017_puntos(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de bodegas
  CALL librut003_cbxbodegas(2)

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL invqbe017_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae017_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.numpos,a.nompos,a.tipfac,a.propin,
                                a.porpro,a.impfac,a.impcoc,a.impcmd, 
                                a.chkinv,a.codbod,a.chkexi,a.userid,
                                a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL invmae017_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Seleccionando ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.numpos,a.nompos,'' ",
                 " FROM fac_puntovta a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_puntos SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_puntos.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_puntos INTO v_puntos[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_puntos
   FREE  c_puntos
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL invqbe017_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_puntos TO s_puntos.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel  
      -- Salida
      CALL invmae017_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar
      CALL invmae017_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      LET res = invmae017_puntos(2) 
      -- Desplegando datos
      CALL invqbe017_datos(v_puntos[ARR_CURR()].tnumpos)
      CALL invqbe017_cajerospos(v_puntos[ARR_CURR()].tnumpos,1)

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN
         -- Modificando 
         LET res = invmae017_puntos(2) 
         -- Desplegando datos
         CALL invqbe017_datos(v_puntos[ARR_CURR()].tnumpos)
         CALL invqbe017_cajerospos(v_puntos[ARR_CURR()].tnumpos,1)
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF librut004_IntegridadPuntosVenta(v_puntos[ARR_CURR()].tnumpos) THEN 
         CALL fgl_winmessage(
         " Atencion",
         " Este punto de venta ya tiene movimientos registrados, no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar este punto de venta? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL invmae017_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION cajeros
      IF (totusr=0) THEN
         ERROR "Atencion: no existen cajeros asignados al punto de venta."
      END IF

      -- Desplegando cajeros
      CALL invqbe017_detcajeros(v_puntos[ARR_CURR()].tnumpos) 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1]  = "Numero"
      LET arrcols[2]  = "Nombre del Punto" 
      LET arrcols[3]  = "Tipo Facturacion"
      LET arrcols[4]  = "Paga Propina" 
      LET arrcols[5]  = "% Propina" 
      LET arrcols[6]  = "Impresora Facturacion"
      LET arrcols[7]  = "Impresora Cocina"
      LET arrcols[8]  = "Impresora Comandas"
      LET arrcols[9]  = "Chequea Inventario"
      LET arrcols[10] = "Bodega"
      LET arrcols[11] = "Chequea Existencia"
      LET arrcols[12] = "Usuario Registro"
      LET arrcols[13] = "Fecha Registro"
      LET arrcols[14] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry =
       "SELECT a.numpos,a.nompos,",
              "CASE (a.tipfac) WHEN 1 THEN 'Por Orden' WHEN 2 THEN 'Por Mesa' END CASE,",
              "CASE (a.propin) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END CASE,",
              "a.porpro,a.impfac,a.impcoc,a.impcmd,",
              "CASE (a.chkinv) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END CASE,",
              "a.codbod,",
              "CASE (a.chkexi) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END CASE,",
              "a.userid,a.fecsis,a.horsis ", 
       " FROM fac_puntovta a",
       " WHERE ",qrytext CLIPPED,
       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res = librut002_excelreport("Puntos de Venta",qry,14,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
	 CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
	 CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

      -- Escondiendo campo de seleccion de cajeros
      CALL f.setFieldHidden("tcheckb",1)

     BEFORE ROW 
      -- Verificando control total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL invqbe017_datos(v_puntos[ARR_CURR()].tnumpos)
         CALL invqbe017_cajerospos(v_puntos[ARR_CURR()].tnumpos,1)
      END IF 
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen puntos de venta con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Desplegando estado del menu
  CALL invqbe017_EstadoMenu(0,"")
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION invqbe017_datos(wnumpos)
 DEFINE wnumpos LIKE fac_puntovta.numpos,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  fac_puntovta a "||
              "WHERE a.numpos = "||wnumpos||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_puntost SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_puntost INTO w_mae_pro.*

  -- Verificando si hay chequeo de existencia
  IF w_mae_pro.chkinv THEN 
   -- Obteniendo datos de la bodega
   CALL librut003_bbodega(w_mae_pro.codbod)
   RETURNING w_mae_bod.*,existe

   -- Obteniendo datos de la empresa
   CALL librut003_bempresa(w_mae_bod.codemp)
   RETURNING w_mae_emp.*,existe

   -- Obteniendo datos de la sucursal
   CALL librut003_bsucursal(w_mae_bod.codsuc)
   RETURNING w_mae_suc.*,existe
  ELSE
   INITIALIZE w_mae_bod.*,w_mae_suc.*,w_mae_emp.* TO NULL
   LET w_mae_pro.codemp = NULL 
   LET w_mae_pro.codsuc = NULL 
   LET w_mae_pro.codbod = NULL 
  END IF

  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.numpos THRU w_mae_pro.horsis 
  DISPLAY BY NAME w_mae_emp.nomemp
  DISPLAY BY NAME w_mae_suc.nomsuc 
 END FOREACH
 CLOSE c_puntost
 FREE  c_puntost

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.numpos THRU w_mae_pro.horsis 
 DISPLAY BY NAME w_mae_emp.nomemp
 DISPLAY BY NAME w_mae_suc.nomsuc 
END FUNCTION 

-- Subrutina para buscar los accesos a usuarios

FUNCTION invqbe017_cajerospos(wnumpos,opcion)
 DEFINE wnumpos   LIKE fac_puntovta.numpos,
        i,opcion  SMALLINT,
        qrytext   STRING

 -- Inicializando vector de datos
 CALL v_cajeros.clear()
 DISPLAY ARRAY v_cajeros TO s_cajeros.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 

 -- Seleccionando datos
 CASE (opcion)
  WHEN 1 -- Consulta de cajeros
   LET qrytext = "SELECT 1,a.userid,b.nomusr,a.usuaid,a.fecsis,a.horsis,'' "||
                  "FROM  fac_usuaxpos a, glb_usuarios b " ||
                  "WHERE a.numpos = "||wnumpos||
                   " AND b.userid = a.userid "||
                   " ORDER BY a.fecsis DESC "

  WHEN 2 -- Cargando cajeros para asignar                 
   LET qrytext = "SELECT 0,a.userid,a.nomusr,USER,CURRENT,CURRENT HOUR TO SECOND,'' "||
                  "FROM  glb_usuarios a " ||
                  "WHERE NOT exists (SELECT b.userid FROM fac_usuaxpos b "||
                                     "WHERE b.numpos = "||wnumpos||
                                      " AND b.userid = a.userid) "||
                   "ORDER BY 2"
 END CASE

 -- Seleccionando datos
 PREPARE cp1 FROM qrytext
 DECLARE ccaj CURSOR FOR cp1
 LET totusr = 1
 FOREACH ccaj INTO v_cajeros[totusr].*
  -- Desplegando datos
  {IF (totusr<=4) THEN
      DISPLAY v_cajeros[totusr].* TO s_cajeros[totusr].*
  END IF} 
  LET totusr = (totusr+1)
 END FOREACH
 CLOSE ccaj
 FREE  ccaj
 LET totusr = (totusr-1)

 -- Desplegando datos 
 DISPLAY ARRAY v_cajeros TO s_cajeros.*
  ATTRIBUTE (COUNT=totusr) 
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION

-- Subrutina para visualizar el detalle de cajeros asignados

FUNCTION invqbe017_detcajeros(wnumpos)
 DEFINE wnumpos    LIKE fac_puntovta.numpos,
        loop,opc,i SMALLINT

 -- Desplegando cajeros
 LET loop = TRUE
 WHILE loop
  DISPLAY ARRAY v_cajeros TO s_cajeros.*
   ATTRIBUTE(COUNT=totusr,ACCEPT=FALSE,CANCEL=FALSE)
   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT DISPLAY

   ON ACTION agregarcajeros 
    -- Agregando cajeros
    CALL invqbe017_cajerospos(wnumpos,2)
    IF (totusr=0) THEN
        CALL fgl_winmessage(
        " Atencion",
        " No existen mas cajeros que asignar.\n Todos los cajeros ya asignados.",
        "stop")

       -- Cargando usuarios con accesos
       CALL invqbe017_cajerospos(wnumpos,1)
       EXIT DISPLAY
    ELSE
      -- Mostrando campo de seleccion de cajeros
      CALL f.setFieldHidden("tcheckb",0)

      -- Seleccionando cajeros
      INPUT ARRAY v_cajeros WITHOUT DEFAULTS FROM s_cajeros.*
       ATTRIBUTE(MAXCOUNT=totusr,INSERT ROW=FALSE,
                 APPEND ROW=FALSE,DELETE ROW=FALSE,
                 ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)

       ON ACTION cancel
        -- Salida
        EXIT INPUT

       ON ACTION accept
        -- Verificando si existen cajeros que agregar
        IF NOT invqbe017_haycajeros(1) THEN
           ERROR "Atencion: no existen cajeros marcados que agregar."
           CONTINUE INPUT
        END IF

        -- Grabando cajeros seleccionados
        LET opc = librut001_menugraba("Confirmacion de Cajeros",
                                      "Que desea hacer ?",
                                      "Guardar",
                                      "Modificar",
                                      "Cancelar",
                                      NULL)

        CASE (opc)
         WHEN 0 -- Cancelando
          EXIT INPUT
         WHEN 1 -- Grabando
          -- Iniciando la transaccion
          BEGIN WORK

          -- Guardando cajeros
          FOR i = 1 TO totusr
           IF (v_cajeros[i].tcheckb=0) OR
              (v_cajeros[i].tcheckb IS NULL) THEN
              CONTINUE FOR
           END IF

           -- Grabando
           SET LOCK MODE TO WAIT
           INSERT INTO fac_usuaxpos
           VALUES (wnumpos,
                   v_cajeros[i].tuserid,
                   1, 
                   "nopass",
                   v_cajeros[i].tusuaid,
                   v_cajeros[i].tfecsis,
                   v_cajeros[i].thorsis)
          END FOR

          -- Terminando la transaccion
          COMMIT WORK

          EXIT INPUT
         WHEN 2 -- Modificando
          CONTINUE INPUT
        END CASE

       BEFORE ROW
        -- Verificando control del total de lineas
        IF (ARR_CURR()>totusr) THEN
           CALL FGL_SET_ARR_CURR(1)
        END IF
      END INPUT

      -- Escondiendo campo de seleccion de cajeros
      CALL f.setFieldHidden("tcheckb",1)

      -- Cargando cajeros                  
      CALL invqbe017_cajerospos(wnumpos,1)
      EXIT DISPLAY
    END IF

   ON ACTION eliminarcajeros 
    -- Eliminando cajeros  
    -- Mostrando campo de seleccion de cajeros
    CALL f.setFieldHidden("tcheckb",0)

    -- Seleccionando cajeros
    INPUT ARRAY v_cajeros WITHOUT DEFAULTS FROM s_cajeros.*
     ATTRIBUTE(MAXCOUNT=totusr,INSERT ROW=FALSE,
               APPEND ROW=FALSE,DELETE ROW=FALSE,
               ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
     ON ACTION cancel
      -- Salida
      EXIT INPUT

     ON ACTION accept
      -- Verificando si existen cajeros que eliminar
      IF NOT invqbe017_haycajeros(0) THEN
         ERROR "Atencion: no existen cajeros desmarcados que borrar."
         CONTINUE INPUT
      END IF

      -- Grabando cajeros seleccionados
      LET opc = librut001_menugraba("Confirmacion de Cajeros",
                                    "Que desea hacer ?",
                                    "Borrar",
                                    "Modificar",
                                    "Cancelar",
                                    NULL)

      CASE (opc)
       WHEN 0 -- Cancelando
        EXIT INPUT
       WHEN 1 -- Grabando
        -- Iniciando la transaccion
        BEGIN WORK

        -- Guardando cajeros
        FOR i = 1 TO totusr
         IF (v_cajeros[i].tcheckb=1) OR
            (v_cajeros[i].tcheckb IS NULL) THEN
            CONTINUE FOR
         END IF

         -- Eliminando
         SET LOCK MODE TO WAIT
         DELETE FROM fac_usuaxpos
         WHERE fac_usuaxpos.numpos = wnumpos
           AND fac_usuaxpos.userid = v_cajeros[i].tuserid
        END FOR

        -- Terminando la transaccion
        COMMIT WORK

        EXIT INPUT
       WHEN 2 -- Modificando
        CONTINUE INPUT
      END CASE

     BEFORE ROW
      -- Verificando control del total de lineas
      IF (ARR_CURR()>totusr) THEN
         CALL FGL_SET_ARR_CURR(1)
      END IF
    END INPUT

    -- Escondiendo campo de seleccion de cajeros
    CALL f.setFieldHidden("tcheckb",1)

    -- Cargando cajeros
    CALL invqbe017_cajerospos(wnumpos,1)
    EXIT DISPLAY

   ON ACTION passwordcajeros 
    -- Editando password del cajero
    CALL invmae017_PassCajeros(wnumpos,ARR_CURR())

   BEFORE DISPLAY 
    -- Verificando si hay usaurios registrados
    IF (totusr>0) THEN
       CALL Dialog.SetActionActive("passwordcajeros",1) 
    ELSE
       CALL Dialog.SetActionActive("passwordcajeros",0) 
    END IF 

   BEFORE ROW
    -- Verificando control del total de lineas
    IF (ARR_CURR()>totusr) THEN
       CALL FGL_SET_ARR_CURR(1)
    END IF
  END DISPLAY
 END WHILE
END FUNCTION

-- Subrutina para verificar si existen cajeros seleccionados que agregar o que borrar

FUNCTION invqbe017_haycajeros(estado)
 DEFINE i,estado,haycaj SMALLINT

 -- Chequeando estado
 LET haycaj = 0
 FOR i = 1 TO totusr
  -- Verificando estado
  IF (v_cajeros[i].tcheckb=estado) THEN
     LET haycaj = 1
     EXIT FOR
  END IF
 END FOR

 RETURN haycaj
END FUNCTION

-- Subrutina para editar los password de los cajeros 

FUNCTION invmae017_PassCajeros(wnumpos,idx)
 DEFINE passwd  LIKE fac_usuaxpos.passwd,
        wnumpos LIKE fac_usuaxpos.numpos,
        idx,opc SMALLINT,
        conteo  SMALLINT,
        loop    SMALLINT 

 -- Desplegando pantalla de seguridad
 OPEN WINDOW wpassword AT 9,27
  WITH FORM "invmae017b"
  CALL fgl_settitle("Cambio de Password")
  DISPLAY v_cajeros[idx].tnomusr TO cajero 

  -- Ingresando clave de confirmacion
  LET loop = TRUE 
  LET passwd = NULL 
  WHILE loop 
   INPUT BY NAME passwd WITHOUT DEFAULTS 
    ATTRIBUTE(UNBUFFERED)

    ON ACTION cancel
     LET loop = FALSE
     EXIT INPUT

    AFTER FIELD passwd
     -- Verificanod password
     IF LENGTH(passwd)<=0 THEN
        ERROR "Error: password invalido."
        NEXT FIELD passwd
     END IF

    AFTER INPUT 
     -- Verificando que el password no se repita
     SELECT COUNT(*)
      INTO  conteo 
      FROM  fac_usuaxpos a
      WHERE a.passwd = passwd
      IF (conteo>0) THEN 
         CALL fgl_winmessage(
         " Atencion",
         " Password ya existe registrado, seleccionar uno diferente. \n VERIFICA.",
         "exclamation") 
         NEXT FIELD passwd 
      END IF 
   END INPUT

   -- Confirmando cambio
   IF loop THEN
      -- Grabando cajeros seleccionados
      LET opc = librut001_menugraba("Confirmacion",
                                    "Que desea hacer ?",
                                    "Grabar",
                                    "Modificar",
                                    "Cancelar",
                                    NULL)
     
      -- Verificando opcio
      CASE (opc)
       WHEN 1 -- Grabar
        LET loop = FALSE 

        -- Iniciando transaccion
        BEGIN WORK 
       
         -- Actualizando password
         SET LOCK MODE TO WAIT
         UPDATE fac_usuaxpos 
         SET    fac_usuaxpos.passwd = passwd 
         WHERE  fac_usuaxpos.numpos = wnumpos 
           AND  fac_usuaxpos.userid = v_cajeros[idx].tuserid 

        -- Finalizando transaccion
        COMMIT WORK 
       WHEN 2 -- Modificar  
        LET loop = TRUE 
       WHEN 0 -- Cancelar
        LET loop = FALSE 
      END CASE 
   END IF 
  END WHILE 
 CLOSE WINDOW wpassword 
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION invqbe017_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Puntos de Venta - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Puntos de Venta - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Puntos de Venta - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Puntos de Venta - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Puntos de Venta - NUEVO")
 END CASE
END FUNCTION
