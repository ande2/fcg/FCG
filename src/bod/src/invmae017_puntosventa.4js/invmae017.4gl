{
Mantenimiento de puntos de venta
invmae017.4gl 
MRS
Enero 2011 
}

-- Definicion de variables globales 

GLOBALS "invglo017.4gl"
CONSTANT progname = "invmae017"
DEFINE username VARCHAR(15),
       existe   SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarPuntoVenta")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("puntoventa")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL invmae017_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae017_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae017a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header(progname,wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Puntos de Venta"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de puntos de venta."
    CALL invqbe017_puntos(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo punto de venta."
    LET savedata = invmae017_puntos(1) 
   COMMAND "Modificar"
    " Modificacion de un punto de venta existente."
    CALL invqbe017_puntos(2) 
   COMMAND "Borrar"
    " Eliminacion de un punto de venta existente."
    CALL invqbe017_puntos(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae017_puntos(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL invqbe017_EstadoMenu(4,"")

    -- Llenando  combobox de bodegas
    CALL librut003_cbxbodegas(1)

    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae017_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nompos,
                w_mae_pro.tipfac,
                w_mae_pro.propin, 
                w_mae_pro.porpro,
                w_mae_pro.impfac,
                w_mae_pro.impcoc,
                w_mae_pro.impcmd,
                w_mae_pro.chkinv, 
		w_mae_pro.codbod,
                w_mae_pro.chkexi
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   ON CHANGE codbod
    -- Obteniendo datos de la bodega
    CALL librut003_bbodega(w_mae_pro.codbod)
    RETURNING w_mae_bod.*,existe

    -- Asignando datos de empresa y sucursal de la bodega
    LET w_mae_pro.codemp = w_mae_bod.codemp
    LET w_mae_pro.codsuc = w_mae_bod.codsuc

    -- Obteniendo datos de la empresa
    CALL librut003_bempresa(w_mae_bod.codemp)
    RETURNING w_mae_emp.*,existe

    -- Obteniendo datos de la sucursal
    CALL librut003_bsucursal(w_mae_bod.codsuc)
    RETURNING w_mae_suc.*,existe

    -- Desplegando datos de la empresa y sucursal
    DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.codsuc,w_mae_emp.nomemp,w_mae_suc.nomsuc

   AFTER FIELD nompos  
    --Verificando nombre del punto de venta
    IF (LENGTH(w_mae_pro.nompos)=0) THEN
       ERROR "Error: nombre del punto de venta invalido, VERIFICA."
       LET w_mae_pro.nompos = NULL
       NEXT FIELD nompos  
    END IF

    -- Verificando que no exista otra punto con el mismo nombre
    SELECT UNIQUE (a.numpos)
     FROM  fac_puntovta a
     WHERE (a.numpos != w_mae_pro.numpos) 
       AND (a.nompos  = w_mae_pro.nompos) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion",
        " Existe otro punto de venta con el mismo nombre, VERIFICA.",
        "information")
        NEXT FIELD nompos
     END IF 

   ON CHANGE chkinv
    --Verificando si se afecta el inventario
    CASE (w_mae_pro.chkinv)
     WHEN 0 LET w_mae_pro.codbod = NULL
            LET w_mae_pro.codemp = NULL
            LET w_mae_pro.codsuc = NULL
            LET w_mae_pro.chkexi = 0
            DISPLAY BY NAME w_mae_pro.codbod,w_mae_pro.codemp,
                            w_mae_pro.codsuc,w_mae_pro.chkexi 
            CLEAR codemp,codsuc,nomemp,nomsuc 
            CALL Dialog.setFieldActive("codbod",0)
            CALL Dialog.setFieldActive("chkexi",0)
     WHEN 1 LET w_mae_pro.codbod = NULL
            LET w_mae_pro.codemp = NULL
            LET w_mae_pro.codsuc = NULL
            LET w_mae_pro.chkexi = 1
            DISPLAY BY NAME w_mae_pro.codbod,w_mae_pro.codemp,
                            w_mae_pro.codsuc,w_mae_pro.chkexi 
            CLEAR codemp,codsuc,nomemp,nomsuc 
            CALL Dialog.setFieldActive("codbod",1)
            CALL Dialog.setFieldActive("chkexi",1)
    END CASE

   AFTER FIELD tipfac 
    --Verificando tipo de facturacion
    IF w_mae_pro.tipfac IS NULL THEN 
      ERROR "Error: tipo de facturacion invalida, VERIFICA." 
      NEXT FIELD tipfac
    END IF

   AFTER FIELD propin 
    --Verificando propina
    IF w_mae_pro.propin IS NULL THEN 
      ERROR "Error: paga propina invalida, VERIFICA." 
      NEXT FIELD propin 
    END IF

   AFTER FIELD porpro 
    --Verificando porcentaje de propina 
    IF w_mae_pro.porpro IS NULL OR 
       w_mae_pro.porpro <0 OR 
       w_mae_pro.porpro >50 THEN 
       LET w_mae_pro.porpro = 0
       DISPLAY BY NAME w_mae_pro.porpro 
    END IF

   AFTER FIELD impfac 
    --Verificando impresora 
    IF w_mae_pro.impfac IS NULL THEN 
      ERROR "Error: tipo de impresora invalida, VERIFICA." 
      NEXT FIELD impfac 
    END IF

   AFTER FIELD impcoc 
    --Verificando impresora 
    IF w_mae_pro.impcoc IS NULL THEN 
      ERROR "Error: tipo de impresora invalida, VERIFICA." 
      NEXT FIELD impcoc 
    END IF

   AFTER FIELD impcmd 
    --Verificando impresora 
    IF w_mae_pro.impcmd IS NULL THEN 
      ERROR "Error: tipo de impresora invalida, VERIFICA." 
      NEXT FIELD impcmd 
    END IF

   AFTER FIELD chkinv 
    --Verificando si se afecta el inventario
    IF w_mae_pro.chkinv IS NULL THEN 
      ERROR "Error: chequea inventario invalido, VERIFICA." 
      NEXT FIELD chkinv
    END IF

    -- Habilitando bodega
    CASE (w_mae_pro.chkinv)
     WHEN 0 CALL Dialog.setFieldActive("codbod",0)
            CALL Dialog.setFieldActive("chkexi",0)
     WHEN 1 CALL Dialog.setFieldActive("codbod",1)
            CALL Dialog.setFieldActive("chkexi",1)
    END CASE

    -- Verificando operacion
    IF (operacion=1) AND NOT w_mae_pro.chkinv THEN
       LET w_mae_pro.codbod = NULL
       LET w_mae_pro.codemp = NULL
       LET w_mae_pro.codsuc = NULL
       LET w_mae_pro.chkexi = 0
       DISPLAY BY NAME w_mae_pro.codbod,w_mae_pro.codemp,
                       w_mae_pro.codsuc,w_mae_pro.chkexi  
       CLEAR codemp,codsuc,nomemp,nomsuc 
    END IF

   AFTER FIELD codbod
    -- Verificando bodega 
    IF w_mae_pro.codbod IS NULL THEN
       ERROR "Error: bodega invalida, VERIFICA."
       NEXT FIELD codbod 
    END IF

   AFTER FIELD chkexi 
    --Verificando chequea de existencia
    IF w_mae_pro.chkexi IS NULL THEN 
       ERROR "Error: chequa eistencia invalida, VERIFICA."
       NEXT FIELD chkexi
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")
  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae017_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL invmae017_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso 
 IF (operacion=1) THEN
    CALL invqbe017_EstadoMenu(0,"")
    CALL invmae017_inival(1) 
 END IF 

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar una punto

FUNCTION invmae017_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando punto de venta ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.numpos),0)
    INTO  w_mae_pro.numpos 
    FROM  fac_puntovta a
    IF (w_mae_pro.numpos IS NULL) THEN
       LET w_mae_pro.numpos = 1
    ELSE
       LET w_mae_pro.numpos = w_mae_pro.numpos+1
    END IF

   -- Verficando campos
   IF NOT w_mae_pro.chkinv THEN
      LET w_mae_pro.codbod = 0
      LET w_mae_pro.codemp = 0
      LET w_mae_pro.codsuc = 0 
   END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO fac_puntovta   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.numpos 

   --Asignando el mensaje 
   LET msg = "Punto de Venta (",w_mae_pro.numpos USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   -- Verficando campos
   IF NOT w_mae_pro.chkinv THEN
      LET w_mae_pro.codbod = 0
      LET w_mae_pro.codemp = 0
      LET w_mae_pro.codsuc = 0 
   END IF 

   --Actualizando 
   UPDATE fac_puntovta
   SET    fac_puntovta.*      = w_mae_pro.*
   WHERE  fac_puntovta.numpos = w_mae_pro.numpos 

   --Asignando el mensaje 
   LET msg = "Punto de Venta (",w_mae_pro.numpos USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando
   DELETE FROM fac_puntovta 
   WHERE  fac_puntovta.numpos = w_mae_pro.numpos 

   --Asignando el mensaje 
   LET msg = "Punto de Venta (",w_mae_pro.numpos USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae017_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae017_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.numpos = 0 
   LET w_mae_pro.tipfac = 1 
   LET w_mae_pro.chkinv = 1 
   LET w_mae_pro.propin = 0 
   LET w_mae_pro.porpro = 0 
   LET w_mae_pro.impfac = "local"
   LET w_mae_pro.impcoc = "local"
   LET w_mae_pro.impcmd = "local"
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.nompos THRU w_mae_pro.chkexi  
 DISPLAY BY NAME w_mae_pro.numpos,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION
