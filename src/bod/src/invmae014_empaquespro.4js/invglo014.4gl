{ 
invglo014.4gl
Mynor Ramirez
Mantenimiento de empaques
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS

CONSTANT linpan = 50

DEFINE w_mae_pro   RECORD LIKE inv_empaques.*,
       v_empaques  DYNAMIC ARRAY OF RECORD
        tcodepq    LIKE inv_empaques.codepq,
        tnomepq    LIKE inv_empaques.nomepq, 
        tcantid    LIKE inv_empaques.cantid,
        tunimed    LIKE inv_unimedid.nommed 
       END RECORD
END GLOBALS
