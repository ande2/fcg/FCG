{
Mantenimiento de empaques
invmae014.4gl 
MRS 
Enero 2011 
}

-- Definicion de variables globales 

GLOBALS "invglo014.4gl"
CONSTANT progname = "invmae014"
DEFINE username VARCHAR(15),
       existe   SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("empaques")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL invmae014_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae014_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae014a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header(progname,wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Empaques"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de empaques."
    CALL invqbe014_empaques(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo empaque."
    LET savedata = invmae014_empaques(1) 
   COMMAND "Modificar"
    " Modificacion de un empaque existente."
    CALL invqbe014_empaques(2) 
   COMMAND "Borrar"
    " Eliminacion de un empaque existente."
    CALL invqbe014_empaques(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae014_empaques(operacion)
 DEFINE w_mae_usr         RECORD LIKE inv_empaques.*,
        loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL invqbe014_EstadoMenu(4,"") 
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae014_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomepq,
                w_mae_pro.cantid, 
                w_mae_pro.unimed
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE INPUT
    -- Verificando integridad
    -- Si el empaque ya existe en algun movimiento no se puede modificar la cantidad ni la unidad de medida
    IF (operacion=2) THEN -- Si es modificacion
     IF invqbe014_integridad() THEN
        CALL Dialog.SetFieldActive("cantid",FALSE) 
        CALL Dialog.SetFieldActive("unimed",FALSE) 
     ELSE
        CALL Dialog.SetFieldActive("cantid",TRUE)
        CALL Dialog.SetFieldActive("unimed",TRUE) 
     END IF 
    END IF 

   AFTER FIELD nomepq  
    --Verificando nombre del empaque
    IF (LENGTH(w_mae_pro.nomepq)=0) THEN
       ERROR "Error: nombre del empaque invalido, VERIFICA"
       LET w_mae_pro.nomepq = NULL
       NEXT FIELD nomepq  
    END IF

    -- Verificando que no exista otro empaque con el mismo nombre
    SELECT UNIQUE (a.codepq)
     FROM  inv_empaques a
     WHERE (a.codepq != w_mae_pro.codepq) 
       AND (a.nomepq  = w_mae_pro.nomepq) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion",
        " Existe otro empaque con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nomepq
     END IF 

   AFTER FIELD cantid
    -- Verificando cantidad
    IF w_mae_pro.cantid IS NULL OR
       (w_mae_pro.cantid <=0) THEN 
       ERROR "Error: equivalencia invalida, VERIFICA ..." 
       LET w_mae_pro.cantid = NULL
       CLEAR cantid
       NEXT FIELD cantid
    END IF 

   BEFORE FIELD unimed
    -- Cargando combobox de unidades de medida
    CALL librut003_cbxunidadesmedida("unimed")

   AFTER FIELD unimed 
    -- Verificando unidad de medida
    IF w_mae_pro.unimed IS NULL THEN
       NEXT FIELD unimed
    END IF

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.codepq IS NULL THEN 
       NEXT FIELD codepq
    END IF
    IF w_mae_pro.nomepq IS NULL THEN 
       NEXT FIELD nomepq
    END IF
    IF w_mae_pro.cantid IS NULL THEN 
       NEXT FIELD cantid
    END IF
    IF w_mae_pro.unimed IS NULL THEN 
       NEXT FIELD unimed
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae014_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando empaque
    CALL invmae014_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL invqbe014_EstadoMenu(0,"") 
    CALL invmae014_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un empaque

FUNCTION invmae014_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando empaque ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.codepq),0)
    INTO  w_mae_pro.codepq
    FROM  inv_empaques a
    IF (w_mae_pro.codepq IS NULL) THEN
       LET w_mae_pro.codepq = 1
    ELSE
       LET w_mae_pro.codepq = (w_mae_pro.codepq+1)
    END IF

   SET LOCK MODE TO WAIT
   INSERT INTO inv_empaques   
   VALUES (w_mae_pro.*)

   --Asignando el mensaje 
   LET msg = "Empaque (",w_mae_pro.codepq USING "<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE inv_empaques
   SET    inv_empaques.*      = w_mae_pro.*
   WHERE  inv_empaques.codepq = w_mae_pro.codepq 

   --Asignando el mensaje 
   LET msg = "Empaque (",w_mae_pro.codepq USING "<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando empaques
   DELETE FROM inv_empaques 
   WHERE (inv_empaques.codepq = w_mae_pro.codepq)

   --Asignando el mensaje 
   LET msg = "Empaque (",w_mae_pro.codepq USING "<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae014_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae014_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codepq = 0
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codepq,w_mae_pro.nomepq THRU w_mae_pro.unimed 
 DISPLAY BY NAME w_mae_pro.codepq,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION
