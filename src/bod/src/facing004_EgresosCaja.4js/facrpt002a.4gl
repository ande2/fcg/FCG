{
Programo : Mynor Ramirez 
Objetivo : Generacion e impresion de facturas al corte de caja 
}

-- Definicion de variables globales  

GLOBALS "facglb002.4gl" 
DEFINE existe   SMALLINT, 
       filename CHAR(90)

-- Subrutina para imprimir el corte de caja   

FUNCTION facrpt002a_GeneraFacturas(reimpresion,pipeline)
 DEFINE reimpresion  SMALLINT,
        pipeline     CHAR(90)

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/CortePOS.spl"

 -- Iniciando reporte
 --START REPORT facrpt002_ImprimirCorte TO filename 
  --OUTPUT TO REPORT facrpt002_ImprimirCorte(1,reimpresion)
 --FINISH REPORT facrpt002_ImprimirCorte

 -- Imprimiendo el reporte
{ CALL librut001_sendreport(filename,pipeline,"",
 "--noline-numbers "||
 "--nofooter "||
 "--font-size 8 "||
 "--page-width 842 --page-height 595 "||
 "--left-margin 55 --right-margin 25 "||
 "--top-margin 35 --bottom-margin 45 "||
 "--title Facturacion")}

END FUNCTION 
