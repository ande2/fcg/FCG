

drop view "sistemas".vis_programas;

create view vis_programas (codmod,codpro,nompro,parent,progid,lnkpro,ordopc) as
  select x0.codmod,
         CASE WHEN (x1.progid = 0)  
          THEN x0.codpro  ELSE x1.codopc
         END ,
         x1.nomopc,
         CASE WHEN (x1.progid = 0) 
          THEN '0'  ELSE x0.codpro  
         END,
         x1.progid,
         x1.lnkpro,
         x1.ordopc 
   from  glb_programs x0,glb_dprogram x1 
   where (x0.codpro = x1.codpro) AND (x0.ordpro >0); 

grant select on vis_programas to public; 
