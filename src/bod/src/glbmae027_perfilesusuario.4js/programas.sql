

drop table glb_dprogram ;
create table "sistemas".glb_dprogram
  (
    lnkpro serial not null ,
    codpro char(15) not null ,
    progid smallint not null ,
    codopc varchar(15,1),
    nomopc varchar(40,1),
    ordopc smallint,
    passwd smallint,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (codpro,progid)  constraint "sistemas".uqglbdprogram
  );

alter table "sistemas".glb_dprogram add constraint (foreign key
    (codpro) references "sistemas".glb_programs  on delete cascade
    constraint "sistemas".fkglbprograms1);


create view "sistemas".vis_programas (codpro,nompro,parent,progid,lnkpro,ordopc) as
  select CASE WHEN (x1.progid = 0 )  THEN x0.codpro  ELSE x1.codopc
     END ,x1.nomopc ,CASE WHEN (x1.progid = 0  )  THEN '0'  ELSE
    x0.codpro  END ,x1.progid ,x1.lnkpro ,x1.ordopc from "sistemas"
    .glb_programs x0 ,"sistemas".glb_dprogram x1 where (x0.codpro
    = x1.codpro ) ;

grant select on vis_programas to public;


drop PROCEDURE "sistemas".sptcreaprogramabase;
CREATE PROCEDURE "sistemas".sptcreaprogramabase(xcodpro VARCHAR(15),xnompro VARCHAR(50),
                                                xordpro SMALLINT)
 -- Procedimiento para crear el programa base o raiz en la tabla de detalle
 -- de programas. Este programa es el nodo raiz para los accesos por programa
 -- Mynor Ramirez
 -- Octubre 2011

 DEFINE xcodopc LIKE glb_dprogram.codopc;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Creando codigo de la opcion
 SELECT TRIM(a.codpro)||"-0"
  INTO  xcodopc
  FROM  glb_programs a
  WHERE a.codpro = xcodpro;

 -- Insertando programa
 SET LOCK MODE TO WAIT;
 INSERT INTO glb_dprogram
 VALUES (0,                         -- id unico de la opcion
         xcodpro,                   -- codigo del programa padre
         0,                         -- id unico de la opcion por programa padre
         xcodopc,                   -- codigo de la opcion (codigo programa-id opcion)
         xnompro,                   -- nombre de la opcion
         xordpro,                   -- orden de la opcion
         0,                         -- opcion no requerie password
         USER,                      -- usuario registro la opcion
         CURRENT,                   -- fecha de registro de la opcion
         CURRENT HOUR TO SECOND);   -- hora de registro de la opcion
END PROCEDURE;

drop trigger "sistemas".trgcreaprogramabase;
create trigger "sistemas".trgcreaprogramabase insert on "sistemas"
    .glb_programs referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptcreaprogramabase(pos.codpro
    ,pos.nompro ,pos.ordpro ));
