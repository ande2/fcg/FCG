create table "sistemas".glb_programs 
  (
    codpro char(15) not null ,
    nompro varchar(100,1) not null ,
    dcrpro varchar(100,1),
    codmod smallint,
    ordpro smallint,
    actpro varchar(30,1),
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codpro)  constraint "sistemas".pkglbprograms
  );

create trigger "sistemas".trgcreaprogramabase insert on "sistemas"
    .glb_programs referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptcreaprogramabase(pos.codpro 
    ,pos.nompro ,pos.ordpro ));

create trigger "sistemas".trgupdnombreprograma update of nompro 
    on "sistemas".glb_programs referencing new as pos
    for each row
        (
        update "sistemas".glb_dprogram set "sistemas".glb_dprogram.nomopc 
    = pos.nompro  where ((codpro = pos.codpro ) AND (progid = 0 ) ) );
    

create trigger "sistemas".trgupdordenprograma update of ordpro 
    on "sistemas".glb_programs referencing new as pos
    for each row
        (
        update "sistemas".glb_dprogram set "sistemas".glb_dprogram.ordopc 
    = pos.ordpro  where ((codpro = pos.codpro ) AND (progid = 0 ) ) );
    

create table "sistemas".glb_mmodulos 
  (
    codmod smallint not null ,
    nommod char(40) not null ,
    numord smallint not null ,
    estado smallint not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codmod)  constraint "sistemas".pkglbmmodulos
  );
