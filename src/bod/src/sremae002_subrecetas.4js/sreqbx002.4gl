{ 
Fecha    : Diciembre 2015        
Programo : Fernando Lontero
Objetivo : Programa de criterios de seleccion para:
           Consulta/Anulacion/Eliminacion/Impresion de subrecetas
}

-- Definicion de variables globales 
GLOBALS "sreglb002.4gl" 

-- Subrutina para consultar/anular/imprimir subrecetas 

FUNCTION sreqbx001_subrecetas(operacion)
 DEFINE qrytext,qrypart     CHAR(500),
        cndanl              CHAR(20),
        loop,existe         SMALLINT,
        operacion           SMALLINT,
        qryres,msg,titqry   CHAR(80)

 -- Verificando operacion
 -- Desplegando estado de menu
 CASE (operacion)
  WHEN 1 CALL librut001_dpelement("group1","Datos Generales de Subrecetas - CONSULTAR")
         LET cndanl = NULL 
 END CASE

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Mostrando campo de motivo de anulacion 
 CALL f.setFieldHidden("formonly.motanl",0)
 CALL f.setElementHidden("labelb",0)

 -- Buscando datos 
 LET loop = TRUE 
 WHILE loop 
  -- Inicializando las variables 
  INITIALIZE qrytext,qrypart TO NULL
  LET int_flag = 0 
  CLEAR FORM

  -- Construyendo busqueda 
  CONSTRUCT BY NAME qrytext 
                 ON a.numrec,
                    a.nomrec,
                    a.cospre,
                    a.numpor,
                    a.fecsis,
                    a.horsis,
                    a.userid
   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT CONSTRUCT
  END CONSTRUCT
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Preparando la busqueda 
  ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

  -- Creando la busqueda 
  LET qrypart = "SELECT a.lnkrec,a.numrec,a.nomrec ",
                 "FROM  sre_subrecet a ",
                 "WHERE  ",qrytext CLIPPED,
                  " ORDER BY 1" 

  -- Declarando el cursor 
  PREPARE estqbe001 FROM qrypart
  DECLARE c_movtos SCROLL CURSOR WITH HOLD FOR estqbe001  
  OPEN c_movtos 
  FETCH FIRST c_movtos INTO w_mae_tra.lnkrec
  IF (status = NOTFOUND) THEN
     INITIALIZE w_mae_tra.* TO NULL
     ERROR ""
     CALL fgl_winmessage(
     "Atencion",
     "No existen subrecetas con el criterio seleccionado.",
     "stop") 
  ELSE
     ERROR "" 

     -- Desplegando datos 
     CALL sreqbx001_datos()

     -- Fetchando subrecetas 
     MENU "Subrecetas" 
      BEFORE MENU 
       -- Verificando tipo de operacion 
       CASE (operacion)
        WHEN 2 HIDE OPTION "Imprimir" 
               HIDE OPTION "Modificar"
       END CASE 
      COMMAND "Siguiente"
       "Visualiza el siguiente receta en la lista."
       FETCH NEXT c_movtos INTO w_mae_tra.lnkrec 
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas subrecetas siguientes en lista.", 
           "information")
           FETCH LAST c_movtos INTO w_mae_tra.lnkrec
       END IF 

       -- Desplegando datos 
       CALL sreqbx001_datos()

      COMMAND "Anterior"
       "Visualiza el subrecetas anterior en la lista."
       FETCH PREVIOUS c_movtos INTO w_mae_tra.lnkrec
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas subrecetas anteriores en lista.", 
           "information")
           FETCH FIRST c_movtos INTO w_mae_tra.lnkrec
        END IF

        -- Desplegando datos 
        CALL sreqbx001_datos()

      COMMAND "Primero" 
       "Visualiza el primer receta en la lista."
       FETCH FIRST c_movtos INTO w_mae_tra.lnkrec
        -- Desplegando datos 
        CALL sreqbx001_datos()

      COMMAND "Ultimo" 
       "Visualiza el ultimo receta en la lista."
       FETCH LAST c_movtos INTO w_mae_tra.lnkrec
        -- Desplegando datos
        CALL sreqbx001_datos()

       -- Desplegando datos
       CALL sreqbx001_datos()

      COMMAND "Productos"
       "Permite visualizar el detalle completo de productos de receta."
       CALL sreqbx001_detalle(0)

      COMMAND "Imprimir" 
       "Permite imprimir la receta actual en pantalla." 
       -- Verificando si receta ya fue anulado

       -- Imprimiendo reporte
       ERROR "Imprimiendo reporte ... por favor espere ..."
       LET reimpresion = TRUE 
       --CALL srerpt001_reportemov()
       ERROR "" 

      COMMAND "Modificar"
       "Permitie modificar el receta actual en pantalla."
       -- Verificando si receta ya fue anulado

       -- Recargando productos
       CALL sreqbx001_detalle(1)

       -- Modificando mivimiento 
       CALL sreing002_subrecetas(2)

       -- Desplegando datos
       CALL sreqbx001_datos()

      ON ACTION delete
       -- Eliminando receta 
       IF sreqbx001_EliminarSubreceta() THEN
          EXIT MENU
       END IF

      COMMAND "Consultar" 
       "Regresa a la pantalla de seleccion."
       EXIT MENU

      ON ACTION cancel 
       LET loop = FALSE 
       EXIT MENU 

      COMMAND KEY(F4,CONTROL-E)
       LET loop = FALSE 
       EXIT MENU
     END MENU
   END IF     
  CLOSE c_movtos
 END WHILE

 -- Inicializando datos 
 CALL sreing002_inival(1)

 -- Desplegando estado de menu 
 CALL librut001_dpelement("group1","Datos Generales de Subreceta - MENU")
END FUNCTION 

-- Subrutina para desplegar los datos de registro 

FUNCTION sreqbx001_datos()
 DEFINE existe SMALLINT

 -- Desplegando datos 
 CLEAR FORM

 -- Obteniendo datos de receta
 CALL sreqbx001_bsubrecetas(w_mae_tra.lnkrec)
 RETURNING w_mae_tra.*,existe

 -- Desplegando datos de receta 
 CLEAR FORM 
 DISPLAY BY NAME w_mae_tra.numrec,w_mae_tra.nomrec, 
                 w_mae_tra.cospre,w_mae_tra.numpor, 
                 w_mae_tra.fecsis,w_mae_tra.horsis,
                 w_mae_tra.userid

 -- Seleccionando detalle de receta 
 CALL sreqbx001_detalle(1)
END FUNCTION 

-- Subrutina para seleccionar datos de detalle 

FUNCTION sreqbx001_detalle(dsp)
 DEFINE existe,dsp SMALLINT

 -- Inicializando vector de productos
 CALL sreing002_inivec()
 
 -- Seleccionando detalle de receta 
 DECLARE cdet CURSOR FOR
 SELECT x.codabr,	
        y.despro,
        y.unimed,
        z.nommed,
        x.canuni,
        "",
        x.correl 
  FROM  sre_dsubrect x, inv_products y, inv_unimedid z
  WHERE (x.lnkrec = w_mae_tra.lnkrec)
    AND (y.cditem = x.cditem)
    AND (y.unimed = z.unimed)
  ORDER BY x.correl
  
  LET totlin = 1 
  FOREACH cdet INTO v_subrecetas[totlin].*
   -- Acumulando contador
   LET totlin = (totlin+1) 
  END FOREACH
  CLOSE cdet
  FREE  cdet
  LET totlin = (totlin-1)  

  -- Desplegando datos de detalle
  DISPLAY ARRAY v_subrecetas TO s_subrecetas.* 
   ATTRIBUTE(ACCEPT=FALSE) 
   BEFORE DISPLAY
    -- Verificando si es solo display
    IF dsp THEN 
       EXIT DISPLAY
    END IF 
  END DISPLAY 

  -- Desplegando totales
  CALL sreing002_totdet() 
END FUNCTION 

-- Subrutina para eliminar el receta 

FUNCTION sreqbx001_EliminarSubreceta()
 DEFINE opc,elimina SMALLINT,
        msg         STRING

 -- Verificando operacion
 LET msg = "Desea Eliminar la Subreceta ?"
 IF librut001_yesornot("Confirmacion",msg,"Si","No","question") THEN
  LET elimina = TRUE

  -- Iniciando Transaccion
  BEGIN WORK

   -- Eliminando receta 
   SET LOCK MODE TO WAIT
   DELETE FROM sre_subrecet
   WHERE (sre_subrecet.lnkrec = w_mae_tra.lnkrec)

  -- Finalizando Transaccion
  COMMIT WORK

  LET msg = "Subreceta Eliminada."
 ELSE
  LET elimina = FALSE
  LET msg = "Eliminacion Cancelada."
 END IF

 -- Desplegando mensaje de accion
 CALL fgl_winmessage("Atencion",msg,"information")

 RETURN elimina
END FUNCTION

-- Subrutina para buscar los datos de una receta (sre_subrecet)

FUNCTION sreqbx001_bsubrecetas(wlnkrec)
 DEFINE w_mae_tra RECORD LIKE sre_subrecet.*,
        wlnkrec   LIKE sre_subrecet.lnkrec

 INITIALIZE w_mae_tra.* TO NULL
 SELECT a.*
  INTO  w_mae_tra.*
  FROM  sre_subrecet a
  WHERE (a.lnkrec= wlnkrec)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_tra.*,FALSE
  ELSE
     RETURN w_mae_tra.*,TRUE
  END IF
END FUNCTION


