{
Fecha    : Diciembre 2015 
Programo : Fernando Lontero
Objetivo : Programa de ingreso de sub-recetas.
}

-- Definicion de variables globales

GLOBALS "sreglb002.4gl"
CONSTANT PermiteRepetidos = FALSE 
DEFINE   fechasmesactual SMALLINT,
         grabaeimprime   SMALLINT, 
         regreso         SMALLINT,
         existe          SMALLINT,
         msg             STRING,
         fechainicioinv  DATE, 
         result          INT 

-- Subrutina principal
MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbarecetas")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("subrecetas")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de opciones
 CALL sreing002_menu()
END MAIN

-- Subutina para el menu de subrecetas de inventario 
FUNCTION sreing002_menu()
 DEFINE regreso    SMALLINT, 
        wpais      VARCHAR(255), 
        titulo     STRING 

 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing001a AT 5,2  
  WITH FORM "sreing002a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ 

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header(progname,wpais,1)
  CALL f.setElementText("labela","Observaciones:")
  CALL f.setElementText("labeld","Numero de Subreceta")

  -- Inicializando datos 
  CALL sreing002_inival(1)

  -- Obteniendo parametro si se desea impresion despues de grabar 
  CALL librut003_parametros(13,11)
  RETURNING existe,grabaeimprime 
  IF NOT existe THEN LET grabaeimprime = 0 END IF 

  -- Menu de opciones
  MENU " Opciones" 
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Consultar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Anular
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Anular"
    END IF
   COMMAND "Consultar" 
    " Consulta de subrecetas existentes."
    CALL sreqbx001_subrecetas(1)
   COMMAND "Ingresar" 
    " Ingreso de nuevos subrecetas."
    CALL sreing002_subrecetas(1) 
   COMMAND "Salir"
    " Abandona el menu de subrecetas." 
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU

 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso de los datos generales del receta (encabezado) 

FUNCTION sreing002_subrecetas(operacion)
 DEFINE w_mae_art         RECORD LIKE inv_products.*,
        retroceso         SMALLINT,
        operacion         SMALLINT,
        loop,existe,i     SMALLINT,
        conteo            SMALLINT,
        w_lnkrec          INTEGER 

 -- Escondiendo motivo de anulacion 
 CALL f.setFieldHidden("formonly.motanl",1)
 CALL f.setElementHidden("labelb",1)

 -- Llenando combobox de tipo de receta 
 CALL librut003_cbxtipomovxusuario(username) 

 -- Inicio del loop
 IF (operacion=1) THEN
    CALL librut001_dpelement("group1","Datos Generales del Subreceta - INGRESAR")
    LET retroceso = FALSE
 ELSE
    CALL librut001_dpelement("group1","Datos Generales del Subreceta - MODIFICAR")
    LET retroceso = TRUE
 END IF 

 LET loop  = TRUE
 WHILE loop   

  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos 
     CALL sreing002_inival(1) 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_tra.nomrec,
                w_mae_tra.cospre,
                w_mae_tra.numpor WITHOUT DEFAULTS
    ATTRIBUTES(UNBUFFERED) 

   ON ACTION cancel 
    -- Salida 
    IF (operacion=1) THEN  -- Ingreso 
     IF INFIELD(nomrec) THEN
        LET loop = FALSE
        CALL sreing002_inival(1)
        EXIT INPUT
     ELSE
        CALL sreing002_inival(1)
        NEXT FIELD nomrec
     END IF 
    ELSE                   -- Modifidacion 
     LET loop = FALSE 
     EXIT INPUT 
    END IF 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   AFTER FIELD nomrec
    -- Verificando nombre de receta
    IF (LENGTH(w_mae_tra.nomrec)<=0)THEN 
       IF w_mae_tra.nomrec IS NULL THEN
          ERROR "Error: Ingrese nombre de receta, VERIFICA."
          NEXT FIELD nomrec 
       END IF 
    END IF

   AFTER FIELD cospre
    -- Verificando campo costo de preparacion
     IF (w_mae_tra.cospre < 0) OR
          w_mae_tra.cospre IS NULL THEN
          ERROR "Error: Ingrese costo de preparacion, VERIFICA."
          NEXT FIELD cospre 
     END IF   
 
   AFTER FIELD numpor
    -- Verificando numero de porciones
     IF (w_mae_tra.numpor<= 0) OR
          w_mae_tra.numpor IS NULL THEN
          ERROR "Error: Ingrese numero de porciones, VERIFICA."
          NEXT FIELD numpor
     END IF 

   AFTER INPUT
    -- Verificando nulos
    IF w_mae_tra.nomrec IS NULL THEN
      NEXT FIELD nomrec
    END IF
    IF w_mae_tra.cospre IS NULL THEN
      NEXT FIELD cospre
    END IF
    IF w_mae_tra.numpor IS NULL THEN
      NEXT FIELD numpor
    END IF
  END INPUT 
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Ingresando detalle del receta 
  LET retroceso = sreing002_detingreso(0,operacion)

  -- Verificando operacion
  IF (operacion=2) THEN
     EXIT WHILE 
  END IF 
 END WHILE

 -- Inicializando datos 
 IF (operacion=1) THEN 
    CALL librut001_dpelement("group1","Datos Generales de la Subreceta - MENU")
    CALL sreing002_inival(1) 
 ELSE 
    CALL librut001_dpelement("group1","Datos Generales de la Subreceta - CONSULTAR")
 END IF 
END FUNCTION

-- Subutina para el ingreso del detalle de productos del receta (detalle) 

FUNCTION sreing002_detingreso(modifica,operacion)
 DEFINE w_mae_art    RECORD LIKE inv_products.*,
        w_mae_uni    RECORD LIKE inv_unimedid.*,
        loop,arr,scr SMALLINT,
        opc,repetido SMALLINT, 
        retroceso    SMALLINT, 
        operacion    SMALLINT, 
        modifica,i   SMALLINT,
        conteo       SMALLINT,
        lastkey      INTEGER 

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop
  -- Ingresando productos
  INPUT ARRAY v_subrecetas WITHOUT DEFAULTS FROM s_subrecetas.*
   ATTRIBUTE(COUNT=totlin,INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,
             UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION accept
    -- Salida
    EXIT INPUT 

   ON ACTION cancel
    -- Regreso
    LET loop      = FALSE
    LET retroceso = TRUE
    EXIT INPUT  

   ON ACTION listaproducto
    -- Asignando posicion actual
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Seleccionado lista de productos
    CALL librut002_formlist("Consulta de Productos",
                            "Producto",
                            "Descripcion del Producto",
                            "Precio Unitario",
                            "codabr",
                            "despro",
                            "presug",
                            "inv_products",
                            "estado=1 and status=1",
                            2,
                            1,
                            1)
    RETURNING v_subrecetas[arr].codabr,v_subrecetas[arr].dsitem,regreso
    IF regreso THEN
       NEXT FIELD codabr
    ELSE
       -- Asignando descripcion
       DISPLAY v_subrecetas[arr].codabr TO s_subrecetas[scr].codabr
       DISPLAY v_subrecetas[arr].dsitem TO s_subrecetas[scr].dsitem
    END IF

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   BEFORE INPUT
    -- Desabilitando botones de append 
    CALL DIALOG.setActionHidden("append",TRUE)

   BEFORE ROW
    -- Asignando total de lineas
    LET totlin = ARR_COUNT()

   BEFORE FIELD codabr
    -- Habilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",TRUE)

   AFTER FIELD codabr
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) THEN
       NEXT FIELD codabr
    END IF

   AFTER FIELD unimed
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) THEN
       NEXT FIELD unimed
    END IF

   BEFORE FIELD canuni 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Obteniendo lista de productos
    IF v_subrecetas[arr].codabr IS NULL THEN 
     -- Seleccionado lista de productos
     CALL librut002_formlist("Consulta de Productos",
                             "Producto",
                             "Descripcion del Producto",
                             "Precio Unitario",
                             "codabr",
                             "despro",
                             "presug",
                             "inv_products",
                             "estado=1 and status=1",
                             2,
                             1,
                             1)
     RETURNING v_subrecetas[arr].codabr,v_subrecetas[arr].dsitem,regreso
     IF regreso THEN
        NEXT FIELD codabr
     ELSE 
        -- Asignando descripcion
        DISPLAY v_subrecetas[arr].codabr TO s_subrecetas[scr].codabr 
        DISPLAY v_subrecetas[arr].dsitem TO s_subrecetas[scr].dsitem 
     END IF 
    END IF 

    -- Verificando producto
    IF (v_subrecetas[arr].codabr <=0) THEN 
       CALL fgl_winmessage(
       " Atencion:",
       " Producto invalido VERIFICA.",
       "stop")
       INITIALIZE v_subrecetas[arr].codabr THRU v_subrecetas[arr].canuni TO NULL
        CLEAR s_subrecetas[scr].codabr,s_subrecetas[scr].dsitem,
              s_subrecetas[scr].nomuni,s_subrecetas[scr].canuni
        NEXT FIELD codabr 
    END IF 

   -- Verificando producto
    IF (v_subrecetas[arr].canuni <=0) THEN
       CALL fgl_winmessage(
       " Atencion:",
       " Cantidad invalida VERIFICA.",
       "stop")
       INITIALIZE v_subrecetas[arr].codabr THRU v_subrecetas[arr].canuni TO NULL
        CLEAR s_subrecetas[scr].codabr,s_subrecetas[scr].dsitem,
              s_subrecetas[scr].nomuni,s_subrecetas[scr].canuni
        NEXT FIELD canuni 
    END IF 

    -- Verificando si el producto existe
    INITIALIZE w_mae_art.* TO NULL
    CALL librut003_bproductoabr(v_subrecetas[arr].codabr)
    RETURNING w_mae_art.*,existe
    IF NOT existe THEN
       CALL fgl_winmessage(
       " Atencion:",
       " Producto no registrado VERIFICA.",
       "stop")
       INITIALIZE v_subrecetas[arr].codabr THRU v_subrecetas[arr].canuni TO NULL
        CLEAR s_subrecetas[scr].codabr,s_subrecetas[scr].dsitem,
              s_subrecetas[scr].nomuni,s_subrecetas[scr].canuni
        NEXT FIELD codabr 
    END IF 

    -- Asignando descripcion
    LET v_subrecetas[arr].dsitem = w_mae_art.despro 
    LET v_subrecetas[arr].unimed = w_mae_art.unimed 
    DISPLAY v_subrecetas[arr].dsitem TO s_subrecetas[scr].dsitem 

  -- Obteniendo datos de la unidad de medida
    INITIALIZE w_uni_med.* TO NULL
    CALL librut003_bumedida(w_mae_art.unimed)
    RETURNING w_uni_med.*,existe

    -- Asignando unidad de medida
    LET v_subrecetas[arr].nomuni = w_uni_med.nommed
    DISPLAY v_subrecetas[arr].nomuni TO s_subrecetas[scr].nomuni
   
  -- Verificando productos repetidos
    IF NOT PermiteRepetidos THEN 
     LET repetido = FALSE
     FOR i = 1 TO totlin
      IF v_subrecetas[i].codabr IS NULL THEN
         CONTINUE FOR
      END IF

      -- Verificando producto
      IF (i!=arr) THEN 
       IF (v_subrecetas[i].codabr = v_subrecetas[arr].codabr) THEN
          LET repetido = TRUE
          EXIT FOR
       END IF 
      END IF
     END FOR

     -- Si hay repetido
     IF repetido THEN
        LET msg = "Producto ya registrado en el detalle. "||
                  "Linea (",i USING "<<<",") \nVERIFICA." 
        CALL fgl_winmessage(" Atencion",msg,"stop")
        INITIALIZE v_subrecetas[arr].codabr THRU v_subrecetas[arr].canuni TO NULL
        CLEAR s_subrecetas[scr].codabr,s_subrecetas[scr].dsitem,
              s_subrecetas[scr].nomuni,s_subrecetas[scr].canuni
        NEXT FIELD codabr 
     END IF
    END IF 

    -- Verificando estado del producto
    IF NOT w_mae_art.estado  THEN
       CALL fgl_winmessage(
       " Atencion",
       " Producto de BAJA no puede utilizarse, VERIFICA.",
       "stop")
       INITIALIZE v_subrecetas[arr].codabr THRU v_subrecetas[arr].canuni TO NULL
        CLEAR s_subrecetas[scr].codabr,s_subrecetas[scr].dsitem,
              s_subrecetas[scr].nomuni,s_subrecetas[scr].canuni
        NEXT FIELD codabr 
    END IF 

    -- Verificando estatus del producto
    IF NOT w_mae_art.status THEN
       CALL fgl_winmessage(
       " Atencion",
       " Producto BLOQUEADO no puede utilizarse, VERIFICA.",
       "stop")
       INITIALIZE v_subrecetas[arr].codabr THRU v_subrecetas[arr].canuni TO NULL
        CLEAR s_subrecetas[scr].codabr,s_subrecetas[scr].dsitem,
              s_subrecetas[scr].nomuni,s_subrecetas[scr].canuni
        NEXT FIELD codabr 
    END IF 

    -- Totalizando productos 
    CALL sreing002_totdet()

    AFTER FIELD canuni
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR
       (lastkey = FGL_KEYVAL("up")) THEN
       NEXT FIELD canuni
    END IF

    -- Verificando cantidad de unidades
    IF v_subrecetas[arr].canuni IS NULL OR
       (v_subrecetas[arr].canuni <0) THEN
       ERROR "Error: cantidad unidad invalida, VERIFICA."
       LET v_subrecetas[arr].canuni = NULL
       CLEAR s_subrecetas[scr].canuni
       NEXT FIELD canuni
    END IF

    -- Deshabilitando la opcion de aceptar
    CALL DIALOG.SetActionActive("accept",FALSE) 

   AFTER INPUT 
    -- Totalizando productos 
    CALL sreing002_totdet()

   AFTER ROW,INSERT,DELETE 
    LET totlin = ARR_COUNT()

    -- Totalizando unidades
    CALL sreing002_totdet()

   ON ACTION delete 
    -- Eliminando linea seleccionada
    LET arr = ARR_CURR()
    CALL v_subrecetas.deleteElement(arr)
    LET totlin = ARR_COUNT()

    -- Totalizando unidades
    CALL sreing002_totdet()
    NEXT FIELD codabr

    END INPUT
    IF NOT loop THEN
      EXIT WHILE
    END IF

  -- Verificando que se ingrese al menos un producto
  IF (totuni<=0) THEN
     CALL fgl_winmessage(
     " Atencion",
     "Debe ingresarse al menos un producto en el detalle del receta.",
     "stop")
     CONTINUE WHILE
  END IF

  -- Menu de opciones
  LET opc = librut001_menugraba("Confirmacion","Que desea hacer?","Guardar","Modificar","Cancelar","")

  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop      = FALSE
    LET retroceso = FALSE
   WHEN 1 -- Grabando
    LET loop      = FALSE
    LET retroceso = FALSE

    -- Grabando receta
    CALL sreing002_grabar(operacion)

   WHEN 2 -- Modificando
    LET loop = TRUE 
  END CASE
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subrutina para totalizar el detalle de productos 

FUNCTION sreing002_totdet()
 DEFINE i INT

 -- Totalilzando detalle
 LET totuni = 0
 LET totpdt = 0
 FOR i=1 TO totlin
  IF v_subrecetas[i].codabr IS NULL OR
     v_subrecetas[i].canuni IS NULL THEN
     CONTINUE FOR
  END IF 
 
 LET totuni = (totuni+v_subrecetas[i].canuni)  
 LET totpdt = (totpdt + 1)
END FOR

-- Desplegando totales
 DISPLAY BY NAME totpdt

END FUNCTION 

-- Subrutina para grabar la receta 

FUNCTION sreing002_grabar(operacion)
 DEFINE w_mae_art    RECORD LIKE inv_products.*, 
        wtipmov      LIKE inv_mtransac.tipmov,
        wopeuni      LIKE inv_dtransac.opeuni, 
        wopeval      LIKE inv_dtransac.opeval, 
        wactexi      LIKE inv_dtransac.actexi,
        ultimoprecio LIKE inv_dtransac.prepro,
        operacion    SMALLINT,
        conteo       SMALLINT,
        xnumrec      INTEGER,
        i,correl     SMALLINT,
        wlnkhis      INTEGER,
        strsql       STRING,
        xtotrec      INT 

 -- Grabando transaccion
 ERROR " Registrando Subreceta ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

   -- 1. Grabando datos generales de receta (encabezado) 
   -- Asignando datos
   IF (operacion=1) THEN -- ingreso
      LET w_mae_tra.lnkrec = 0
   
    SELECT NVL(MAX(a.numrec),0)+1
     INTO  xnumrec
      FROM  sre_subrecet a
      LET w_mae_tra.numrec = xnumrec USING "&&&&"

      SET LOCK MODE TO WAIT
      INSERT INTO sre_subrecet
      VALUES (w_mae_tra.*)
      LET w_mae_tra.lnkrec = SQLCA.SQLERRD[2]
   ELSE
      -- Actualizando datos del encabezado
      SET LOCK MODE TO WAIT
      UPDATE sre_subrecet
      SET    sre_subrecet.*      = w_mae_tra.*
      WHERE  sre_subrecet.lnkrec = w_mae_tra.lnkrec
 
      -- Eliminando datos del detalle antes de grabar
      SET LOCK MODE TO WAIT
      DELETE FROM sre_dsubrect
      WHERE sre_dsubrect.lnkrec = w_mae_tra.lnkrec
  END IF

    -- 2. Grabando detalle de productos de receta (detalle)
    LET correl = 0 
    FOR i = 1 TO totlin 
     IF v_subrecetas[i].codabr IS NULL OR 
        v_subrecetas[i].canuni IS NULL THEN 
        CONTINUE FOR 
     END IF 
     LET correl = (correl+1) 

    -- Obteniendo datos del producto
    INITIALIZE w_mae_art.* TO NULL
    CALL librut003_bproductoabr(v_subrecetas[i].codabr)
    RETURNING w_mae_art.*,existe

    -- Grabando
    SET LOCK MODE TO WAIT
    INSERT INTO sre_dsubrect
    VALUES (w_mae_tra.lnkrec        , -- id de la receta 
            w_mae_art.cditem        , -- codigo del producto 
            v_subrecetas[i].codabr  , -- codigo del producto
            v_subrecetas[i].canuni  , -- cantidad unidad * 
            correl)                   -- correlativo de receta 
   END FOR 

   -- Desplegando numero de receta 
   CASE (operacion)
    WHEN 1 LET msg = " Subreceta registrada presione [ENTER] para continuar" 
    WHEN 2 LET msg = " Subreceta actualizada presione [ENTER] para continuar" 
   END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Imprimiendo receta si opcion de grabar e imprimir esta habilitada
 IF grabaeimprime THEN
    ERROR "Imprimiendo receta ... por favor espere ...."
    LET reimpresion = FALSE
   -- CALL srerpt001_reportemov()
    ERROR "" 
 END IF 
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION sreing002_inival(i)
 DEFINE i SMALLINT
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_tra.*  TO NULL 
   CLEAR FORM 
 END CASE 

 -- Inicializando datos
 LET totpdt           = 0 
 LET totuni           = 0 
 LET w_mae_tra.userid = username 
 LET w_mae_tra.fecsis = CURRENT 
 LET w_mae_tra.horsis = CURRENT HOUR TO SECOND

 -- Inicializando vectores de datos
 CALL sreing002_inivec() 

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_tra.userid,w_mae_tra.fecsis,w_mae_tra.horsis 
DISPLAY BY NAME w_mae_tra.numrec,totpdt
END FUNCTION

-- Subrutina para inicializar y limpiar los vectores de trabajo 

FUNCTION sreing002_inivec()
 DEFINE i SMALLINT

 -- Inicializando vectores
 CALL v_subrecetas.clear()

 LET totlin = 0 
 FOR i = 1 TO 5
  -- Limpiando vector       
  CLEAR s_subrecetas[i].*
 END FOR 
END FUNCTION 

