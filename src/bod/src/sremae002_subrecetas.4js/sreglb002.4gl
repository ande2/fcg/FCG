{ 
Fecha    : diciembre 2015 
Programo : Fernando Lontero
Objetivo : Programa de variables globales subrecetas
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname  = "sreing002"
DEFINE w_mae_tra   RECORD LIKE sre_subrecet.*,
       w_mae_act   RECORD LIKE sre_subrecet.*,
       w_uni_med   RECORD LIKE inv_unimedid.*,
        v_subrecetas  DYNAMIC ARRAY OF RECORD
        codabr     LIKE sre_dsubrect.codabr, 
        dsitem     CHAR(50), 
        unimed     SMALLINT,
        nomuni     CHAR(50), 
        canuni     LIKE sre_dsubrect.canuni,
        rellen     CHAR(1) 
       END RECORD, 
        totuni      DEC(14,2), 
        totpdt      SMALLINT, 
        reimpresion SMALLINT,
        totlin      SMALLINT,
        nomprv      STRING,   
        nomest      STRING,   
        nomcli      STRING,
        b           ui.ComboBox,
        cba         ui.ComboBox,
        w_lnkrec    INT,
        w           ui.Window,
        f           ui.Form,
        maxdiamod   SMALLINT, 
        maxdiaeli   SMALLINT, 
        tipmovcmp   INTEGER, 
        username    VARCHAR(15)
END GLOBALS
