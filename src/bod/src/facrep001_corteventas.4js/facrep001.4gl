{ 
Programo : facrep001.4gl 
Objetivo : Reporte de corte de ventas
}

DATABASE storepos 

-- Definicion de variables globales 

DEFINE w_datos       RECORD
        numpos       LIKE fac_mtransac.numpos,
        fecini       DATE,
        fecfin       DATE,
        detmov       SMALLINT
       END RECORD,
       xnompos       LIKE fac_puntovta.nompos, 
       lg,nlines     SMALLINT, 
       existe        SMALLINT,
       tituloreporte STRING,
       filename      STRING,
       pipeline      STRING,
       periodo       STRING 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes2")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()=0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rcorteventas")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL facrep001_corteventas()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION facrep001_corteventas()
 DEFINE wpais      VARCHAR(255),
        loop,res   SMALLINT,
        w          ui.Window

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep001a AT 5,2
  WITH FORM "facrep001a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("facrep001",wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/CorteVentas.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox
  CALL librut003_CbxPuntosVenta()

  -- Inicializando datos
  INITIALIZE w_datos.* TO NULL
  LET w_datos.detmov = 0
  LET w_datos.fecini = TODAY 
  LET w_datos.fecfin = TODAY 
  LET w_datos.detmov = 0
  LET w_datos.numpos = 1 
  CLEAR FORM

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline TO NULL

   -- Construyendo busqueda
   INPUT BY NAME w_datos.numpos,
                 w_datos.fecini, 
                 w_datos.fecfin, 
                 w_datos.detmov
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Verificando si filtros estan completos
     IF NOT facrep003_FiltrosCompletos() THEN
        NEXT FIELD numpos 
     END IF
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 

     -- Verificando si filtros estan completos
     IF NOT facrep003_FiltrosCompletos() THEN
        NEXT FIELD numpos 
     END IF
     EXIT INPUT 

    AFTER FIELD numpos
     -- Verificando punto de venta 
     IF w_datos.numpos IS NULL THEN
        CALL fgl_winmessage("Atencion","Punto de venta invalido, VERIFICA.","stop") 
        NEXT FIELD numpos
     END IF

    AFTER FIELD detmov
     -- Verificando detalle de movimientos
     IF w_datos.detmov IS NULL THEN
        LET w_datos.detmov = 0
        DISPLAY BY NAME w_datos.detmov 
     END IF 

    AFTER INPUT 
     -- Verificando datos
     IF w_datos.numpos IS NULL OR 
        w_datos.fecini IS NULL OR 
        w_datos.fecfin IS NULL OR 
        pipeline       IS NULL THEN
        NEXT FIELD numpos
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Generando el reporte
   CALL facrep001_gencorte()
  END WHILE
 CLOSE WINDOW wrep001a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION facrep003_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE
 IF w_datos.numpos IS NULL OR
    w_datos.fecini IS NULL OR
    w_datos.fecfin IS NULL THEN
    CALL fgl_winmessage(
    "Atencion",
    "Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte de corte de ventas 

FUNCTION facrep001_gencorte()
 DEFINE w_mae_fac RECORD LIKE fac_mtransac.*,
        xnomusr   LIKE glb_usuarios.nomusr, 
        s_query   STRING, 
        strfeccor STRING

 -- Definiendo parametros
 LET lg     = 156 
 LET nlines = 50 

 -- Seleccionando nombre del punto de venta 
 SELECT NVL(a.nompos,"")
  INTO  xnompos  
  FROM  fac_puntovta a
  WHERE a.numpos = w_datos.numpos 

 -- Verificando condicion de fecha de corte
 LET strfeccor = " AND x.feccor >= '",w_datos.fecini,"'",
                 " AND x.feccor <= '",w_datos.fecfin,"' "
 
 -- Preparando condiciones del reporte 
 LET s_query = 
  "SELECT x.* ",
   "FROM  fac_mtransac x ",
   "WHERE x.numpos = ",w_datos.numpos, 
   strfeccor CLIPPED,
   " ORDER BY x.tipdoc,x.fecemi,x.nserie,x.numdoc" 

 -- Seleccionando datos del reporte
 ERROR "Atencion: seleccionando datos ... por favor espere ..."

 PREPARE s_corte FROM s_query
 DECLARE c_corte CURSOR FOR s_corte
 LET existe    = FALSE
 FOREACH c_corte INTO w_mae_fac.*
  -- Iniciando reporte
  IF NOT existe THEN
     -- Iniciando la impresion
     START REPORT facrep001_impcorvta TO filename
     LET existe = TRUE
  END IF

  -- Verificando si documento esta anulada para poner valores a cero
  IF (w_mae_fac.estado="A") THEN
     LET w_mae_fac.efecti = 0 
     LET w_mae_fac.tarcre = 0 
     LET w_mae_fac.totdoc = 0 
  ELSE
     LET w_mae_fac.totdoc = w_mae_fac.efecti+w_mae_fac.tarcre
  END IF 

  -- Llenado el reporte
  OUTPUT TO REPORT facrep001_impcorvta(w_mae_fac.*) 
 END FOREACH
 CLOSE c_corte
 FREE  c_corte

 IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT facrep001_impcorvta   

    -- Enviando reporte al destino seleccionado
    CALL librut001_sendreport
    (filename,pipeline,tituloreporte, 
     "--noline-numbers "||
     "--nofooter "||
     "--font-size 8 "||
     "--page-width 842 --page-height 595 "||
     "--left-margin 55 --right-margin 25 "||
     "--top-margin 35 --bottom-margin 45 "||
     "--title Facturacion")

    ERROR "" 
    CALL fgl_winmessage("Atencion","Reporte Emitido.","information") 
 ELSE
    ERROR "" 
    CALL fgl_winmessage("Atencion","No existen datos con el filtro seleccionado.","stop") 
 END IF 
END FUNCTION 

-- Subrutina para imprimir el reporte 

REPORT facrep001_impcorvta(imp1)
 DEFINE imp1          RECORD LIKE fac_mtransac.*,
        w_tip_doc     RECORD LIKE fac_tipodocs.*,
        w_detmov      RECORD LIKE fac_dtransac.*,
        exis,i,col    SMALLINT, 
        linea         CHAR(156), 
        wdsitem       CHAR(40),
        wnomrec       CHAR(40),
        xnomcli       CHAR(44), 
        nestado       CHAR(3)

  OUTPUT LEFT   MARGIN 0 
         TOP    MARGIN 0 
         BOTTOM MARGIN 0 
         PAGE   LENGTH nlines 

  FORMAT 
   PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________",
                "______"

    -- Imprimiendo encabezado 
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Facturacion",
          COLUMN col,tituloreporte CLIPPED,            
	  COLUMN (lg-20),PAGENO USING "Pagina: <<"

    LET periodo = "PERIODO DEL ",w_datos.fecini," AL ",w_datos.fecfin 
    LET col = librut001_centrado(periodo,lg) 
    PRINT COLUMN   1,"Facrep001",
          COLUMN col,periodo CLIPPED,
          COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET col = librut001_centrado(xnompos,lg) 
    PRINT COLUMN col,xnompos CLIPPED, 
          COLUMN (lg-20),"Hora  : ",TIME 

    PRINT linea 
    PRINT "Fecha        Serie     Numero       EFECTIVO     TARJETA     ",
          " T O T A L     NOMBRE DEL CLIENTE                            ",
          "Cajero           Hora       Estado" 
    PRINT "Emision                Documento                 CREDITO     ",
          " V E N T A                                                   ",
          "Opero            Operacion        " 
    PRINT linea

   BEFORE GROUP OF imp1.tipdoc
    -- Obteniendo datos del tipo de documento 
    INITIALIZE w_tip_doc.* TO NULL
    CALL librut003_btipodoc(imp1.tipdoc)
    RETURNING w_tip_doc.*,exis
    PRINT "DOCUMENTO: ",w_tip_doc.nomdoc CLIPPED
    SKIP 1 LINES 

   ON EVERY ROW
    -- Verificando si documento esta anulado
    LET nestado = NULL
    IF (imp1.estado="A") THEN
       LET nestado = "A"
    END IF 

    -- Imprimiendo datos 
    LET xnomcli = imp1.nomcli 
    PRINT imp1.fecemi                         USING "dd/mm/yyyy"   ,3 SPACES,
          imp1.nserie                                              ," ", 
          imp1.numdoc                         USING "&&&&&&&&"     ,2 SPACES,
          imp1.efecti                         USING "###,##&.&&"   ,2 SPACES,
          imp1.tarcre                         USING "###,##&.&&"   ,2 SPACES,
          imp1.totdoc                         USING "##,###,##&.&&",5 SPACES,
          xnomcli                                                  ,2 SPACES,
          imp1.usrope                                              ,2 SPACES, 
          imp1.horsis                                              ,2 SPACES,
          nestado             

    -- Verificando si factura esta anulada para imprimir el motivo
    IF (imp1.estado="A") THEN
       PRINT "ANULADA POR: ",imp1.motanl CLIPPED
    END IF 
    
    -- Verificando si hay impresion de detalle
    IF w_datos.detmov THEN
       -- Seleccionando detalle del documento
       DECLARE c_det CURSOR FOR
       SELECT a.*,b.dsitem
        FROM  fac_dtransac a,inv_products b
        WHERE a.lnktra = imp1.lnktra 
          AND b.cditem = a.cditem
        ORDER BY a.correl

        SKIP 1 LINES 
        PRINT "   Producto             ",
              "Nombre del Producto                             ",
              "Cantidad     Precio Uni          Total"
        PRINT "   ---------------------------------------------------",
              "--------------------------------------------------------"
        FOREACH c_det INTO w_detmov.*,wdsitem
         PRINT                                           3 SPACES, 
               w_detmov.codabr                         , 1 SPACES,
               wdsitem                                 , 3 SPACES,
               w_detmov.cantid    USING "##,###,##&.&&", 2 SPACES,
               w_detmov.preuni    USING "##,##&.&&&&&&", 2 SPACES,
               w_detmov.totpro    USING "##,###,##&.&&"
        END FOREACH
        CLOSE c_det
        FREE  c_det
        PRINT "   ---------------------------------------------------",
              "--------------------------------------------------------"
        SKIP 1 LINES 
    END IF 

   AFTER GROUP OF imp1.tipdoc
    -- Totalizando por tipo de documento 
    SKIP 1 LINES 
    PRINT COLUMN   1,"TOTAL DOCUMENTO -->",
          COLUMN  35,GROUP SUM(imp1.efecti)   USING "###,##&.&&"   , 2 SPACES,
                     GROUP SUM(imp1.tarcre)   USING "###,##&.&&"   , 2 SPACES,
                     GROUP SUM(imp1.totdoc)   USING "##,###,##&.&&"
    SKIP 1 LINES 

   ON LAST ROW
    -- Totalizando
    PRINT COLUMN   1,"TOTAL VENTA     -->",
          COLUMN  35,SUM(imp1.efecti) 
                     WHERE imp1.haycor=1      USING "###,##&.&&"   , 2 SPACES,
                     SUM(imp1.tarcre)         
                     WHERE imp1.haycor=1      USING "###,##&.&&"   , 2 SPACES,
                     SUM(imp1.totdoc)    
                     WHERE imp1.haycor=1      USING "##,###,##&.&&"
END REPORT
