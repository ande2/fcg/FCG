{ 
glbglo046.4gl
Mynor Ramirez
Mantenimiento de elaboradores 
}

DATABASE storepos 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae046"
DEFINE w_mae_pro      RECORD LIKE glb_elabodrs.*,
       v_elaboradores DYNAMIC ARRAY OF RECORD
        tcodela       LIKE glb_elabodrs.codela,
        tnomela       LIKE glb_elabodrs.nomela, 
        tendrec       CHAR(1) 
       END RECORD,
       username       VARCHAR(15)
END GLOBALS
