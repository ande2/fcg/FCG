{
Programa : librut006.4gl 
Programo : Mynor Ramirez
Objetivo : Subrutinas generales
}

DATABASE storepos 

-- Subrutina para grabar el correlativo de etiquetas

FUNCTION librut006_CorrelativoEtiquetas() 
 DEFINE wnumetq INTEGER 

 -- Generando numero correlativo automatico de etiqueta
 INSERT INTO inv_ncorretq
 VALUES (0)
 LET wnumetq = SQLCA.SQLERRD[2]

 RETURN wnumetq
END FUNCTION

-- Subrutina para imprimir una factura 

FUNCTION librut006_GeneraFactura(imp0,ParamsImp) 
 DEFINE imp0                 RECORD LIKE fac_mtransac.*,
        ParamsImp            RECORD 
         Reimpresion         SMALLINT,
         ColIniImpFac        SMALLINT, 
         ImpCopiaDocTarcre   SMALLINT,
         NumeroCopia         SMALLINT,
         DescripcionNT       CHAR(40),
         DesgloceTotales     SMALLINT, 
         Recibi              DEC(9,2),
         Vuelto              DEC(9,2),
         ImpresionPdf        SMALLINT 
        END RECORD,
        ximpfac              LIKE fac_puntovta.impfac, 
        i,numfac             SMALLINT, 
        existe               SMALLINT, 
        filename             STRING,
        pipeline             STRING 

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/FacturaPOS.spl"

 -- Asignando impresora de facturacion por punto de venta si no es impresion pdf
 IF NOT paramsimp.Impresionpdf THEN
  SELECT a.impfac
   INTO  ximpfac
   FROM  fac_puntovta a
   WHERE a.numpos = imp0.numpos 
   IF (status=NOTFOUND) THEN 
      LET pipeline = "local" 
   ELSE  
      LET pipeline = ximpfac 
   END IF 
 ELSE
   LET pipeline = "pdf2" 
 END IF 

 -- Obteniendo parametro de numero de facturas impresas
 CALL librut001_parametros(10,4)
 RETURNING existe,numfac
 IF NOT existe THEN
    LET numfac = 1
 END IF

 -- Verificando si existe parametro de imprimir doble copia cuando es pago con tarjeta
 IF paramsimp.ImpCopiaDocTarCre=1 THEN
    IF imp0.tarcre=0 THEN 
       LET numfac = 1
    END IF 
 END IF 

 -- Imprimiendo el reporte
 FOR i = 1 TO numfac
  LET paramsimp.NumeroCopia = i

  -- Iniciando reporte
  START REPORT librut006_ImprimirFactura TO filename 
   -- Llenando reporte 
   OUTPUT TO REPORT librut006_ImprimirFactura(1,imp0.*,paramsimp.*)
  -- Finalizando reporte 
  FINISH REPORT librut006_ImprimirFactura 

  -- Enviando a impresion 
  IF NOT paramsimp.Impresionpdf THEN
     CALL librut001_sendreport(filename,pipeline,"","")
  ELSE
     CALL librut001_sendreport(filename,pipeline,"",
     "--noline-numbers "||
     "--nofooter "||
     "--title Impresion-Documentos")
  END IF 
 END FOR
END FUNCTION 

-- Subrutinar para generar la impresion de la facturacion 

REPORT librut006_ImprimirFactura(numero,imp0,paramsimp) 
 DEFINE imp0                 RECORD LIKE fac_mtransac.*, 
        imp1                 RECORD LIKE fac_formaimp.*,
        imp2                 RECORD LIKE fac_tdocxpos.*,
        ParamsImp            RECORD 
         Reimpresion         SMALLINT,
         ColIniImpFac        SMALLINT, 
         ImpCopiaDocTarcre   SMALLINT,
         NumeroCopia         SMALLINT, 
         DescripcionNT       CHAR(40), 
         DesgloceTotales     SMALLINT, 
         Recibi              DEC(9,2),
         Vuelto              DEC(9,2), 
         ImpresionPdf        SMALLINT 
        END RECORD,
        v_platos             RECORD 
         nomrec              CHAR(40),
         cantid              LIKE pos_dtransac.cantid, 
         prevta              LIKE pos_dtransac.prevta, 
         totrec              LIKE pos_dtransac.totrec  
        END RECORD,
        xporisv              LIKE pos_dtransac.porisv,
        xtotisv              LIKE pos_dtransac.totisv, 
        totdes3raedad        DEC(9,2),
        xnomcaj              CHAR(40), 
        xnomcli              CHAR(60), 
        xnomdat              CHAR(40), 
        xnompos              CHAR(40), 
        i,col,coi,j          SMALLINT, 
        lg                   SMALLINT, 
        hayfirmas            SMALLINT, 
        xnumdoc              CHAR(40), 
        xtitle1              CHAR(30), 
        xtitisv              CHAR(31), 
        xorden               CHAR(20), 
        worden               CHAR(20), 
        wnomesa              CHAR(40), 
        cortepapel           CHAR(10), 
        abrircaja            CHAR(10), 
        xdescrp              CHAR(40), 
        xtitpla              CHAR(40), 
        exonerado            DEC(14,2),
        importe18            DEC(14,2),
        tasaali0             DEC(14,2),
        wcanletras           STRING,
        texto                STRING, 
        numero               INT 

  OUTPUT LEFT   MARGIN 0 
         PAGE   LENGTH 5 
         TOP    MARGIN 0 
         BOTTOM MARGIN 0

 FORMAT 
  BEFORE GROUP OF numero 
   -- Imprimiendo encabezado
   LET wcanletras = librut001_numtolet(imp0.totdoc) 

   -- Inicializando impresora
   LET coi = paramsimp.ColIniImpFac
   LET lg  = 42+coi
   LET cortepapel = ASCII 29,ASCII 86,"0"
   LET abrircaja  = ASCII 27,ASCII 112,"0"

   -- Abriendo caja
   IF NOT paramsimp.Impresionpdf THEN
      PRINT abrircaja CLIPPED
   END IF 

   -- Obteniendo datos del tipo de documento de facturacion
   INITIALIZE imp1.* TO NULL 
   SELECT a.* 
    INTO  imp1.* 
    FROM  fac_formaimp a
    WHERE a.lnktdc = imp0.lnktdc

   -- Obteniendo nombre del cajero
   INITIALIZE xnomcaj TO NULL 
   SELECT a.nomusr 
    INTO  xnomcaj 
    FROM  glb_usuarios a
    WHERE a.userid = imp0.userid 

   -- Obteniendo titulo del tipo de documento 
   INITIALIZE xtitle1 TO NULL
   SELECT a.title1,NVL(a.hayfir,0) 
    INTO  xtitle1,hayfirmas
    FROM  fac_tipodocs a 
    WHERE a.tipdoc = imp0.tipdoc 

   -- Obteniendo datos del documento de facturacion 
   INITIALIZE imp2.* TO NULL
   SELECT a.*
    INTO  imp2.*
    FROM  fac_tdocxpos a
    WHERE a.lnktdc = imp0.lnktdc 

   -- Encabezado
   IF LENGTH(imp1.top001) >0 THEN 
      LET col = librut001_centrado(imp1.top001,lg) 
      PRINT COLUMN col,imp1.top001 CLIPPED 
   END IF 
   IF LENGTH(imp1.top002) >0 THEN 
      LET col = librut001_centrado(imp1.top002,lg) 
      PRINT COLUMN col,imp1.top002 CLIPPED 
   END IF 
   IF LENGTH(imp1.top003) >0 THEN 
      LET col = librut001_centrado(imp1.top003,lg) 
      PRINT COLUMN col,imp1.top003 CLIPPED 
   END IF 
   IF LENGTH(imp1.top004) >0 THEN 
      LET col = librut001_centrado(imp1.top004,lg) 
      PRINT COLUMN col,imp1.top004 CLIPPED 
   END IF 
   IF LENGTH(imp1.top005) >0 THEN 
      LET col = librut001_centrado(imp1.top005,lg) 
      PRINT COLUMN col,imp1.top005 CLIPPED 
   END IF 
   IF LENGTH(imp1.top006) >0 THEN 
      LET col = librut001_centrado(imp1.top006,lg) 
      PRINT COLUMN col,imp1.top006 CLIPPED 
   END IF 
   SKIP 1 LINES 

   -- Datos de la factura 
   -- Numero de autorizacion 
   IF imp2.regfac=1 THEN
      LET texto = "NUMERO DE CAI"
      LET col = librut001_centrado(texto,lg) 
      PRINT COLUMN col,texto CLIPPED

      LET texto = imp2.numcai CLIPPED 
      LET col = librut001_centrado(texto,lg) 
      PRINT COLUMN col,texto CLIPPED

      LET texto = "DEL ", 
          imp2.numcor USING "<<<<<<<<"," Al ",
          imp2.numfin USING "<<<<<<<<"
      LET col = librut001_centrado(texto,lg) 
      PRINT COLUMN col,texto CLIPPED

      LET texto = "FECHA DE VENCIMIENTO: ",imp2.fecven 
      LET col = librut001_centrado(texto,lg) 
      PRINT COLUMN col,texto CLIPPED
      SKIP 1 LINES 
   END IF 

   -- Datos de la orden 
   {IF (imp0.numesa=0) THEN 
    LET xorden = imp0.numdoc 
    LET i = LENGTH(xorden) 
    IF (i=1) THEN
      LET j=i
    ELSE
      LET j=i-1
    END IF

    LET worden = "ORDEN # [ ",xorden[j,i]," ]" 
    LET col = librut001_centrado(worden,lg) 
    PRINT COLUMN col,worden CLIPPED 
   END IF } 
   SKIP 1 LINES 

   -- Numero de documento 
   LET xnumdoc = imp0.nserie CLIPPED,"-",
                 imp0.numdoc USING "&&&&&&&&"

   PRINT COLUMN coi,xtitle1 CLIPPED," ",xnumdoc CLIPPED 

   -- Verificando si es de credito o contado
   IF imp0.credit=1 THEN
      PRINT COLUMN coi,"Forma Pago CREDITO"
   ELSE 
      PRINT COLUMN coi,"Forma Pago CONTADO"
   END IF

   -- Datos de cliente 
   LET xnomcli = imp0.nomcli CLIPPED 
   {IF (imp0.codepl>0) THEN
      LET xnomdat = "Nombre:  ",xnomcli[1,30] 
      PRINT COLUMN coi,"Codigo:  ",imp0.codepl USING "<<<<" 
      PRINT COLUMN coi,xnomdat CLIPPED 
   ELSE 
    IF (LENGTH(imp0.nomcli)>0) THEN
      PRINT COLUMN coi,"Cliente: ",xnomcli[1,30] CLIPPED 
      IF (LENGTH(imp0.nomcli)>30) THEN
       PRINT COLUMN coi,"         ",xnomcli[31,60] CLIPPED 
      END IF 
    ELSE 
      PRINT COLUMN coi,"Cliente: "                     
      PRINT COLUMN coi,"        _______________________________"
    END IF 
   END IF } 

   -- Verificando si esta afecto a regimen de facturacion
   IF imp2.regfac=1 THEN
    -- Numero de Identifiacion Tributario 
    IF (LENGTH(imp0.numnit)>0) THEN
       PRINT COLUMN coi,paramsimp.DescripcionNT CLIPPED,":  ",imp0.numnit CLIPPED
    ELSE 
       PRINT COLUMN coi,paramsimp.DescripcionNT CLIPPED,":  "
       PRINT COLUMN coi,"        _______________________________"
    END IF 
   END IF
   SKIP 1 LINES 

   -- Fecha y cajero 
   PRINT COLUMN coi,"Fecha:   ",imp0.fecemi," ",imp0.horsis 
   PRINT COLUMN coi,"Cajero:  ",xnomcaj CLIPPED

   {-- Datos de la orden o mesa
   IF (imp0.numesa>0) THEN 
    LET wnomesa = "MESA # [ ",imp0.numesa USING "<<<<<"," ]" 
    PRINT COLUMN coi,wnomesa CLIPPED 
   END IF 
   SKIP 1 LINES } 

   -- Destino
   {IF imp0.llevar=1 THEN
      LET xtitpla = "Detalle de Platos Servidos-Para Llevar" 
   ELSE
      LET xtitpla = "Detalle de Platos Servidos-Comer Aqui" 
   END IF} 

   -- Detalle de Platos Servidos 
   PRINT COLUMN coi,xtitpla CLIPPED 
   PRINT COLUMN coi,"_______________________________________"
   PRINT COLUMN coi,"Plato           Cant.    P/U      Valor" 
   PRINT COLUMN coi,"_______________________________________"
 
  ON EVERY ROW
   -- Imprimiendo detalle de platos 
   DECLARE cpla CURSOR FOR
   SELECT y.nomrec,
          x.cantid,
          x.prevta,
          x.totrec 
    FROM  pos_dtransac x,sre_mrecetas y 
    WHERE x.lnktra = imp0.lnktra
      AND y.lnkrec = x.lnkrec 
    ORDER BY x.correl
    FOREACH cpla INTO v_platos.*
     -- Imprimiendo datos 
     PRINT COLUMN coi,v_platos.nomrec 
     PRINT COLUMN coi                         ,18 SPACES,
           v_platos.cantid  USING "##&"       ,1 SPACES, 
           v_platos.prevta  USING "##&.&&"    ,2 SPACES, 
           v_platos.totrec  USING "##,##&.&&" 
    END FOREACH
    CLOSE cpla
    FREE  cpla

  ON LAST ROW
   -- Inicializando datos
   LET exonerado = 0
   LET tasaali0  = 0
   LET importe18 = 0 

   -- Imprimiendo totales 
   PRINT COLUMN coi,"_______________________________________"

   -- Verificando regimen de facturacion 
   {IF imp2.regfac=1 THEN
   
    -- Verificado impresion de desgloce de totales
    IF paramsimp.DesgloceTotales THEN 
     PRINT COLUMN coi,"IMPORTE EXONERADO         L. ",exonerado     USING "###,##&.&&"
     PRINT COLUMN coi,"IMPORTE EXENTO            L. ",imp0.totexe   USING "###,##&.&&"
     PRINT COLUMN coi,"IMPORTE GRAVADO (15%)     L. ",imp0.totgra   USING "###,##&.&&"
     PRINT COLUMN coi,"IMPORTE GRAVADO (18%)     L. ",importe18     USING "###,##&.&&"
     PRINT COLUMN coi,"DESCUENTOS Y REBAJAS      L. ",imp0.totdes   USING "###,##&.&&"

     -- Imprimiendo porcentaje de impuestos
     DECLARE cisv CURSOR FOR
     SELECT a.porisv,SUM(a.totisv) 
      FROM  pos_dtransac a
      WHERE (a.lnktra = imp0.lnktra) 
      GROUP BY 1 
      ORDER BY 1  
      FOREACH cisv INTO xporisv,xtotisv 
       LET xtitisv = "I.S.V.          (",xporisv USING "#&","%)     L."
       PRINT COLUMN coi,xtitisv CLIPPED,xtotisv          USING "####,##&.&&"
      END FOREACH
      CLOSE cisv
      FREE  cisv 

     -- Impuesto 18%
     LET xtitisv = "IMPUESTO        (",18 USING "#&","%)     L."
     PRINT COLUMN coi,xtitisv CLIPPED,0                  USING "####,##&.&&"

     -- Imprimiendo total final 
     PRINT COLUMN coi,"TOTAL A PAGAR             L. ",imp0.totcta    USING "###,##&.&&"

     IF (imp0.propin>0) THEN
      PRINT COLUMN coi,"TOTAL PROPINA             L. ",imp0.propin   USING "###,##&.&&"
      PRINT COLUMN coi,"TOTAL A PAGAR + PROPINA   L. ",imp0.totpag   USING "###,##&.&&"
     END IF

     PRINT COLUMN coi,"_______________________________________"

     -- Forma de pago 
     -- Efectivo
     IF (paramsimp.Recibi>0) THEN 
        PRINT COLUMN coi,"RECIBIDO                  L. ",paramsimp.Recibi USING "###,##&.&&" 
     END IF 
     IF (imp0.efecti>0) THEN 
        PRINT COLUMN coi,"EFECTIVO                  L. ",imp0.efecti USING "###,##&.&&" 
     END IF 
     IF (paramsimp.Vuelto>0) THEN 
        PRINT COLUMN coi,"TOTAL CAMBIO              L. ",paramsimp.Vuelto USING "###,##&.&&"
     END IF 
     -- Tarjeta 
     IF (imp0.tarcre>0) THEN 
        PRINT COLUMN coi,"TARJETA DE CREDITO        L. ",imp0.tarcre USING "###,##&.&&" 
     END IF 

     -- Descuentos 3ra edad 
     LET totdes3raedad = 0
     SELECT NVL(SUM(y.totdes),0)
      INTO  totdes3raedad 
      FROM  sre_mordenes x,sre_dordenes y
      WHERE x.lnkord = y.lnkord
        AND x.lnktra = imp0.lnktra 
        AND y.tipdes = 1 
     IF totdes3raedad IS NULL THEN
        LET totdes3raedad = 0
     END IF 

     IF (totdes3raedad>0) THEN
      SKIP 1 LINES 
      PRINT COLUMN coi,"DESCUENTO 3RA EDAD        L. ",totdes3raedad USING "###,##&.&&"
     END IF 

     SKIP 1 LINES
    ELSE
     -- Imprimiendo total final 
     PRINT COLUMN coi,"SUB-TOTAL                 L. ",imp0.totcta    USING "###,##&.&&"

     IF (imp0.propin>0) THEN
      PRINT COLUMN coi,"TOTAL PROPINA             L. ",imp0.propin   USING "###,##&.&&"
      PRINT COLUMN coi,"TOTAL                     L. ",imp0.totpag   USING "###,##&.&&"
     END IF 

     PRINT COLUMN coi,"_______________________________________"

     -- Forma de pago 
     -- Efectivo
     IF (paramsimp.Recibi>0) THEN 
        PRINT COLUMN coi,"RECIBIDO                  L. ",paramsimp.Recibi USING "###,##&.&&" 
     END IF 
     IF (imp0.efecti>0) THEN 
        PRINT COLUMN coi,"EFECTIVO                  L. ",imp0.efecti USING "###,##&.&&" 
     END IF 
     IF (paramsimp.Vuelto>0) THEN 
        PRINT COLUMN coi,"TOTAL CAMBIO              L. ",paramsimp.Vuelto USING "###,##&.&&"
     END IF 
     -- Tarjeta 
     IF (imp0.tarcre>0) THEN 
        PRINT COLUMN coi,"TARJETA DE CREDITO        L. ",imp0.tarcre USING "###,##&.&&" 
     END IF 
     -- Total Descuento 
     IF (imp0.totdes>0) THEN 
        PRINT COLUMN coi,"TOTAL DESCUENTO           L. ",imp0.totdes USING "###,##&.&&"
     END IF 
     SKIP 1 LINES
    END IF 
   ELSE 
     -- Imprimiendo total final 
     PRINT COLUMN coi,"TOTAL                     L. ",imp0.totdoc    USING "###,##&.&&"
   END IF 

   -- Verificando si documento tiene firmas
   IF hayfirmas THEN
      SKIP 4 LINES
      PRINT COLUMN coi,"___________________________________"
      PRINT COLUMN coi,"Firma de Aceptacion" 
   END IF } 

   -- Pie de pagina 
   SKIP 1 LINES 
   -- Verificando si documento es reimpresion
   IF paramsimp.Reimpresion THEN
      LET col = librut001_centrado("( R )",lg) 
      PRINT COLUMN col,"( R )" 
   END IF 
   IF LENGTH(imp1.foot01) >0 THEN 
      LET col = librut001_centrado(imp1.foot01,lg) 
      PRINT COLUMN col,imp1.foot01 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot02) >0 THEN 
      LET col = librut001_centrado(imp1.foot02,lg) 
      PRINT COLUMN col,imp1.foot02 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot03) >0 THEN 
      LET col = librut001_centrado(imp1.foot03,lg) 
      PRINT COLUMN col,imp1.foot03 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot04) >0 THEN 
      LET col = librut001_centrado(imp1.foot04,lg) 
      PRINT COLUMN col,imp1.foot04 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot05) >0 THEN 
      LET col = librut001_centrado(imp1.foot05,lg) 
      PRINT COLUMN col,imp1.foot05 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot06) >0 THEN 
      LET col = librut001_centrado(imp1.foot06,lg) 
      PRINT COLUMN col,imp1.foot06 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot07) >0 THEN 
      LET col = librut001_centrado(imp1.foot07,lg) 
      PRINT COLUMN col,imp1.foot07 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot08) >0 THEN 
      LET col = librut001_centrado(imp1.foot08,lg) 
      PRINT COLUMN col,imp1.foot08 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot09) >0 THEN 
      LET col = librut001_centrado(imp1.foot09,lg) 
      PRINT COLUMN col,imp1.foot09 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot10) >0 THEN 
      LET col = librut001_centrado(imp1.foot10,lg) 
      PRINT COLUMN col,imp1.foot10 CLIPPED 
   END IF 

   -- Imprimiendo pie 
   FOR i = 1 TO 6 
    PRINT " "
   END FOR 

   -- Cortando papel 
   IF NOT paramsimp.Impresionpdf THEN
      PRINT cortepapel CLIPPED 
   END IF 
END REPORT 
