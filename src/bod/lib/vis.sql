



create view "sistemas".vis_tipdocxpostodos (lnktdc,numpos,hayimp,nomdoc) as 
  select x0.lnktdc ,x0.numpos ,x1.hayimp ,((((TRIM ( BOTH ' ' FROM 
    x1.nomdoc ) || ' SERIE ' ) || TRIM ( BOTH ' ' FROM x0.nserie 
    ) ) || ' ' ) || x0.nomdoc ) from "sistemas".fac_tdocxpos x0 
    ,"sistemas".fac_tipodocs x1 ,"sistemas".glb_empresas x2 where 
    ((x0.tipdoc = x1.tipdoc ) AND (x2.codemp = x0.codemp ) ) ;                                        

grant select on vis_tipdocxpostodos to public;
          



