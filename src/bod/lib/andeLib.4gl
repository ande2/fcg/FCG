--IMPORT FGL fgl_excel

{CS FUNCTION gral_reporte(tamanio, posicion, tipo, columnas, nom_reporte)
DEFINE tamanio     STRING
DEFINE posicion    STRING
DEFINE tipo        STRING
DEFINE ancho       STRING
DEFINE alto        STRING
DEFINE columnas    SMALLINT
DEFINE nom_reporte STRING
DEFINE myReporte   om.SaxDocumentHandler

        IF tamanio = "carta" OR tamanio = "CARTA" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "11in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "11in"
           END IF
        END IF

        IF tamanio = "oficio" OR tamanio = "OFICIO" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "13in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "13in"
           END IF       
        END IF CS}

        {IF tamanio = "unapagina" THEN
           LET ancho = "10000"
           LET alto  = "10000"  
        END IF}

        {CS IF NOT fgl_report_loadCurrentSettings(NULL) THEN
            RETURN
         END IF
         DISPLAY "ancho y alto ", ancho, alto
         CALL fgl_report_configurePageSize(ancho,alto)
DISPLAY "Columnas ", columnas         
         --topMargin STRING, bottomMargin STRING, leftMargin STRING, rightMargin STRING) 
         CALL fgl_report_setPageMargins("0","0","0.25inch","0.0inch")
        -- topMargin STRING, bottomMargin STRING, leftMargin STRING, rightMargin STRING )         
        --CALL fgl_report_setPaperMargins("0","0","0","0")
        -- pageWidthInCharacters, fontName, fidelity, reportName, reportCategory, systemId
         CALL fgl_report_configureCompatibilityOutput("124","Monospaced",false,null,"","")
         --CALL fgl_report_configureCompatibilityOutput(2,"Monospaced",false,null,"","")
         --CALL fgl_report_configureCompatibilityOutput(columnas,"Monospaced",false,nom_reporte,"","")
         --CALL fgl_report_configureMultipageOutput(0, 0, FALSE)
        --type STRING COMPATIBILITY - FLAT  LIST - NEW LIST
         CALL fgl_report_selectDevice("PDF")
         --CALL fgl_report_setAutoformatType("NEW LIST")
         --CALL fgl_report_setAutoformatType("NEW LIST")
         --CALL fgl_report_selectDevice(tipo)
         IF tamanio = "unapagina" THEN
           CALL fgl_report_configureXLSDevice(1,1,1,1,1,1,0)  
        END IF 
         LET myReporte = fgl_report_commitCurrentSettings()
RETURN myReporte
END FUNCTION}



# Recibe como parametro un string de caracteres largo y devuelve un arreglo
# con tantas lineas como se necesite para contener el string recibido
# Recibe como parametro el lago de linea del arreglo que retorna

#########################################################################
## Function  : isGWC()
## Parametros: Ninguno
## Retorna   : False o True
## Comments  : Verifica si el ambiente es GWC
#########################################################################
FUNCTION isGWC()
   RETURN ui.Interface.getFrontEndName() == "GWC"
END FUNCTION

#########################################################################
## Function  : isGWC()
## Parametros: Ninguno
## Retorna   : False o True
## Comments  : Verifica si el ambiente es GWC
#########################################################################
FUNCTION isGDC()
   RETURN ui.Interface.getFrontEndName() == "GDC"
END FUNCTION


#########################################################################
## Function  : convierte()
## Parametros: String a separar
##             Largo de la linea en que se va a separar el string
## Retorna   : Arreglo con el mismo string pero separado en lineas del 
##             largo requerido
## Comments  : No corta palabras, si la palabra no cabe la pasa a la linea
##             de abajo
##             Borra espacios al inicio de cada linea
##             Si no hay espacios corta la palabra al llegar al largo de 
##             linea requerido
##             Por ejemplo:
##                LET myArr = convierte('Esta es una prueba', 10)
##                myArr[1] 'Esta es '
##                myArr[2] 'una prueba'
#########################################################################
FUNCTION convierte(txt,lg)
DEFINE lg SMALLINT 
DEFINE txt, res STRING 
DEFINE a DYNAMIC ARRAY OF STRING
DEFINE ini, fin, i  SMALLINT 

   -- 0. Definir posicion inicial y final
   LET i = 0
   LET ini = 1
   LET fin = lg
   IF fin > txt.getlength() THEN LET fin = txt.getlength() END IF
   
   WHILE TRUE 
       
      LET i = i + 1
      CALL separa(txt, ini, fin, lg) RETURNING res, ini
      LET a[i] = res
      LET fin = ini + lg
      IF fin > txt.getlength() THEN LET fin = txt.getlength() END IF 
      IF ini > txt.getlength() THEN EXIT WHILE END IF
      
   END WHILE  
   
   RETURN a

END FUNCTION 

FUNCTION separa(txt, ini, fin, lg)
DEFINE txt, res, res2 STRING
DEFINE ini, fin, lg SMALLINT
DEFINE i, flag, ini2 SMALLINT  

-- 1. Puede ser que no vaya ningun espacio
--    Corta la palabra hasta donde llegue

   LET flag = 0 
   FOR i = ini TO fin
      IF txt.getCharAt(i) = ' ' THEN LET flag = 1 EXIT FOR END IF 
   END FOR
   IF flag = 0 THEN 
      LET res = txt.subString(ini,fin)
   END IF 

-- 2. Si es la ultima linea
   IF fin >= txt.getLength() THEN 
      LET res = txt.subString(ini,fin)
   ELSE 
   -- 3. valida que termina en una palabra
   --    a) La ultima posición sea espacio
   --    b) O que largo de linea + 1 sea espacio
   --    En ese caso al arreglo le asigna a la linea completa

      IF txt.getCharAt(fin) = ' ' OR txt.getCharAt(fin+1) = ' ' THEN
         LET res = txt.subString(ini,fin)
      ELSE --busca el primer espacio de atras para adelante
         FOR i = fin TO ini STEP -1
            IF txt.getCharAt(i) = ' ' THEN
               LET fin = i-1
               LET res = txt.subString(ini,fin)
               EXIT FOR 
            END IF 
         END FOR 
      END IF 
   END IF

   -- 4. Quitando espacios al inicio
   FOR i = 1 TO res.getLength()
      IF res.getCharAt(i) = ' ' THEN 
         LET ini2 = i+1 
      ELSE 
         LET ini2 = i
         EXIT FOR 
      END IF 
   END FOR 
   LET res2 = res.subString(ini2,res.getLength())
   RETURN res2, fin+1 

END FUNCTION 


{FUNCTION convierte(lvar,lg)
DEFINE lvar             STRING  	--string a dividir
DEFINE lg	            SMALLINT  --largo de linea
DEFINE larr 	         DYNAMIC ARRAY OF STRING  --array resultado
DEFINE ini,fin,i,j,lv 	SMALLINT
DISPLAY "ERROR ", lvar, lg
LET lv  = lvar.getlength()

--display "string ", lvar, " --largo-- ",lv

--Si es menor o igual al largo de linea
IF lv <= lg THEN  
   LET larr[1] = lvar CLIPPED  
   GOTO _salir
END IF  

--Mas de una linea
LET ini = 1
LET fin = lg
LET lv  = lvar.getlength()
LET i   = 1

WHILE TRUE 

  IF lvar.getCharAt(fin) = ' ' THEN  
     LET fin = fin - 1
     LET larr[i] = lvar.subString(ini,fin)
     LET ini  = fin  + 2 
  ELSE  
     FOR j = fin TO ini STEP - 1
      
       IF lvar.getCharAt(j) = ' ' THEN
          --DISPLAY "ini ", ini, "  fin ", fin
          --DISPLAY "largo de string ", lvar.getLength()
          LET fin = j - 1
          LET larr[i] = lvar.subString(ini,fin) 
          --display "linea ", i, " -- ", larr[i]
          EXIT FOR  
       END IF  
       
     END FOR   
  END IF  

  LET i = i + 1
  
  --LET ini = j + 1
  LET fin = ini + lg
  IF fin > lv THEN 
     LET larr[i] = lvar.subString(ini,lv) EXIT WHILE 
     END IF 
  IF ini > lv THEN EXIT WHILE END IF   

END WHILE 

LABEL _salir:

RETURN larr

END FUNCTION

# Recibe un string de caracteres y devuelve un segundo string justificado
# Recibe como parámetro el string a justificar y el largo de linea 
# Retorna el string justificado
FUNCTION justifical(str,ll)
DEFINE str      STRING 
DEFINE ll       SMALLINT 
DEFINE ts,i, ini, j, salir     SMALLINT 

  IF LENGTH(str) = ll THEN 
     GOTO _regresa
  END IF 

   LET ts = ll - LENGTH(str)
   LET j = 0
   WHILE ts > 0 
      LET ini = 1
      LET salir = 0
      LET j = j + 1
      WHILE TRUE  
         --buscar espacio
         FOR i = ini TO LENGTH(str)
            IF str.getCharAt(i) = " " THEN 
               LET str = str.subString(1,i), " ", str.subString(i+1,str.getLength())
               LET ts = ts - 1
               IF ts = 0 THEN LET salir = 1 END IF 
               LET ini = i + j + 1
               EXIT FOR 
            ELSE 
               IF i = LENGTH(str) THEN LET salir=1 END IF 
            END IF 
         END FOR 
         IF salir = 1 THEN EXIT WHILE END IF
   END WHILE 
 END WHILE   

  LABEL _regresa:

  RETURN str
END FUNCTION} 



FUNCTION gral_reporte(tamanio, posicion, tipo, columnas, nom_reporte)
DEFINE tamanio     STRING
DEFINE posicion    STRING
DEFINE tipo        STRING
DEFINE ancho       STRING
DEFINE alto        STRING
DEFINE columnas    SMALLINT
DEFINE nom_reporte STRING
DEFINE myReporte   om.SaxDocumentHandler

        IF tamanio = "carta" OR tamanio = "CARTA" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "11in"
              LET alto  = "8.5in"
              DISPLAY "orientacion horizontal"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "11in"
           END IF
        END IF

        IF tamanio = "oficio" OR tamanio = "OFICIO" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "13in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "13in"
           END IF
        END IF

        IF NOT fgl_report_loadCurrentSettings(NULL) THEN
            RETURN
         END IF
         DISPLAY "ancho y alto ", ancho, alto
         CALL fgl_report_configurePageSize(ancho,alto)
         CALL fgl_report_configureCompatibilityOutput(124,"Monospaced",false,nom_reporte,"","")
         --CALL fgl_report_configureCompatibilityOutput(columnas,"Monospaced",false,nom_reporte,"","")
         --CALL fgl_report_configureMultipageOutput(0, 0, FALSE)
         CALL fgl_report_selectDevice(tipo)
         IF tamanio = "unapagina" THEN
           CALL fgl_report_configureXLSDevice(1,1,1,1,1,1,0)
        END IF
         LET myReporte = fgl_report_commitCurrentSettings()
RETURN myReporte
END FUNCTION

#########################################################################
## Function  : getPeso()
##
## Parameters: tabValida    << Tabla donde va a validar
##             colValida    << Columna que va a ir a validar
##             valKey       << Variable local que tiene la llave que va a validar
##
## Returnings: True / False
##
## Comments  : Recibe como parámetro el nombre del parametro global y
##             retorna el valor del parámetro
##             El nombre del parametro se ingresa en el catalogo general
##             de parámetros glbm0003.42r
#########################################################################

FUNCTION gral_reporte_all(tamanio, posicion, tipo, columnas, nom_reporte)
DEFINE tamanio     STRING
DEFINE posicion    STRING
DEFINE tipo        STRING
DEFINE ancho       STRING
DEFINE alto        STRING
DEFINE columnas    SMALLINT
DEFINE nom_reporte STRING
DEFINE myReporte   om.SaxDocumentHandler

DISPLAY "Entre a AndeLib"
        IF tamanio = "carta" OR tamanio = "CARTA" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "11in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "11in"
           END IF
        END IF

        IF tamanio = "oficio" OR tamanio = "OFICIO" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "13in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "13in"
           END IF       
        END IF
        
        IF NOT fgl_report_loadCurrentSettings(NULL) THEN
            RETURN
         END IF
        DISPLAY "columnas ", columnas
         --CALL fgl_report_configurePageSize(ancho,alto)
         --CALL fgl_report_configureCompatibilityOutput(columnas,"fixed-8",false,nom_reporte,"","")
         --CALL fgl_report_configureCompatibilityOutput(columnas,"Monospaced",false,nom_reporte,"","")
         CALL fgl_report_configureCompatibilityOutput(columnas,"FreeMono",false,nom_reporte,"","")
         --CALL fgl_report_configureMultipageOutput(0, 0, FALSE)
         CALL fgl_report_selectDevice(tipo)
         CALL fgl_report_configureXLSDevice(NULL,NULL,NULL,NULL,TRUE ,NULL,TRUE)
         LET myReporte = fgl_report_commitCurrentSettings()
RETURN myReporte
END FUNCTION

FUNCTION round(dval,dpos)

DEFINE 
  dval DECIMAL ,
  dpos SMALLINT ,
  ival DECIMAL (16,0)
  
LET ival = dval * (10 ** dpos)
LET dval = ival / (10 ** dpos)
RETURN dval

END FUNCTION 

#------------------------------------------------------
# Funcion:  numsem
# Recibe una fecha y devuelve el numero de la semana
#
# Let lNumSem = numSem(lfecha)
#
#------------------------------------------------------
FUNCTION numsem(fec1)
  DEFINE fec1, fec2 DATE 

  DEFINE numday SMALLINT 
  DEFINE numwday TINYINT 
  DEFINE numsem SMALLINT 

  LET fec2 = MDY(1,1,YEAR(TODAY))
  
  LET numday = (fec1 - fec2) + 1

  LET numwday = WEEKDAY (TODAY)

  LET numsem = (numday - numwday + 10) / 7

  RETURN numsem
  
END FUNCTION

{FUNCTION sql_to_excel(sql STRING, filename STRING, header BOOLEAN) RETURNS BOOLEAN
DEFINE hdl base.SqlHandle
DEFINE row_idx, col_idx INTEGER 

DEFINE workbook     fgl_excel.workbookType 
DEFINE sheet        fgl_excel.sheetType  
DEFINE row          fgl_excel.rowType  
DEFINE cell         fgl_excel.cellType 
DEFINE header_style fgl_excel.cellStyleType
DEFINE header_font  fgl_excel.fontType

DEFINE datatype STRING
    
    LET hdl = base.SqlHandle.create()
    TRY
        CALL hdl.prepare(sql)
        CALL hdl.open()
    CATCH
        RETURN FALSE
    END TRY

    CALL fgl_excel.workbook_create() RETURNING workbook

    -- create a worksheet
    CALL fgl_excel.workbook_createsheet(workbook) RETURNING sheet

    -- create data rows
    LET row_idx = 0 
    
    WHILE TRUE
        CALL hdl.fetch()
        IF STATUS=NOTFOUND THEN
            EXIT WHILE
        END IF
        LET row_idx = row_idx + 1

        IF row_idx = 1 AND header THEN
            -- create a font, will be used in header
            CALL fgl_excel.font_create(workbook) RETURNING header_font
            CALL fgl_excel.font_set(header_font, "weight", "bold")

            -- create a style, will be used in header
            CALL fgl_excel.style_create(workbook) RETURNING header_style
            CALL fgl_excel.style_set(header_style, "alignment","center")
            CALL fgl_excel.style_font_set(header_style, header_font)
   
            -- Add column headers
            CALL fgl_excel.sheet_createrow(sheet, 0) RETURNING row
            FOR col_idx = 1 TO hdl.getResultCount()
                CALL fgl_excel.row_createcell(row, col_idx-1) RETURNING cell
                CALL fgl_excel.cell_value_set(cell, hdl.getResultName(col_idx))
                CALL fgl_excel.cell_style_set(cell, header_style)
            END FOR
        END IF
        CALL fgl_excel.sheet_createrow(sheet, IIF(header,row_idx, row_idx-1)) RETURNING row

        FOR col_idx = 1 TO hdl.getResultCount()
            CALL fgl_excel.row_createcell(row, col_idx-1) RETURNING cell
            LET datatype = hdl.getResultType(col_idx) 
            CASE 
                WHEN datatype =  "INTEGER" -- TODO check logic
                  OR datatype MATCHES "DECIMAL*"
                  OR datatype MATCHES "FLOAT*"
                  OR datatype MATCHES "*INT*"
                    CALL fgl_excel.cell_number_set(cell, hdl.getResultValue(col_idx))
                OTHERWISE
                    CALL fgl_excel.cell_value_set(cell, hdl.getResultValue(col_idx))
            END CASE
        END FOR
    END WHILE

    -- TODO this code should automatically size the columns
    -- However it is very very slow for reasons I can't determine
    -- Uncomment and test at your leisure
    --IF hdl IS NOT NULL THEN
        --FOR col_idx = 1 TO hdl.getResultCount()
            --CALL fgl_excel.sheet_autosizecolumn(sheet, col_idx-1)
        --END FOR
    --END IF

    -- Write to File
    CALL fgl_excel.workbook_writeToFile(workbook, filename)

    RETURN TRUE   
END FUNCTION}



FUNCTION populate()
DEFINE idx INTEGER
DEFINE rec RECORD
    integer_type INTEGER,
    date_type DATE,
    char_type CHAR(20),
    float_type FLOAT
END RECORD

    WHENEVER ERROR CONTINUE 
      DROP TABLE test_data
   WHENEVER ERROR STOP 
    CREATE TABLE test_data 
        (integer_type INTEGER,
         date_type DATE,
         char_type VARCHAR(20),
         float_type FLOAT)

    FOR idx = 1 TO 26
        LET rec.integer_type = idx
        LET rec.date_type = TODAY+idx
        LET rec.char_type = ASCII(64+idx)
        LET rec.float_type = 1/idx
        
        INSERT INTO test_data VALUES(rec.*)
    END FOR
END FUNCTION

FUNCTION nommes(vmes)
DEFINE vmes SMALLINT;
DEFINE nomMes STRING

CASE  vmes
   WHEN 1  LET nomMes = "ENERO"
   WHEN 2  LET nomMes = "FEBRERO"
   WHEN 3  LET nomMes = "MARZO"
   WHEN 4  LET nomMes = "ABRIL"
   WHEN 5  LET nomMes = "MAYO"
   WHEN 6  LET nomMes = "JUNIO"
   WHEN 7  LET nomMes = "JULIO"
   WHEN 8  LET nomMes = "AGOSTO"
   WHEN 9  LET nomMes = "SEPTIEMBRE"
   WHEN 10 LET nomMes = "OCTUBRE"
   WHEN 11 LET nomMes = "NOVIEMBRE"
   WHEN 12 LET nomMes = "DICIEMBRE"
END CASE

RETURN nomMes

END FUNCTION

FUNCTION ValidarNit( lNit )
DEFINE
   lnit     CHAR(20),
   tnit     CHAR(20),
   digito   CHAR(1),
   r1, r2, r3,
   j,
   i        SMALLINT,
   lSuma    SMALLINT

   LET tnit = " "
   --Reemplazar el guion (-), por si trae
   FOR i = 1 TO LENGTH(lNit)
      LET digito =  lnit[i,i]
      IF digito  <> "-" THEN
         LET tnit = tnit CLIPPED, digito
      END IF
   END FOR
   LET lnit = tnit

   --Buscar el ultimo digito
   LET i = LENGTH(lNit)
   LET digito = lnit[i,i]

   --Validar el digito
   IF NOT validarDigito(digito) THEN
      RETURN FALSE
   END IF

   LET lsuma = 0
   LET j = LENGTH(lnit)
   FOR i = 1 TO LENGTH(lnit)-1
      LET lsuma = lsuma + ( lnit[i,i] * j )
      LET j = j - 1
   END FOR

   LET r1 = lsuma MOD 11
   LET r2 = 11 - r1
   LET r3 = r2 MOD 11

   IF r3 = 10 THEN
      IF digito = "K" OR digito = "k" THEN
         RETURN TRUE
      ELSE
         RETURN FALSE
      END IF
   ELSE
      IF r3 = digito THEN
         RETURN TRUE
      ELSE
         RETURN FALSE
      END IF
   END IF
END FUNCTION

FUNCTION ValidarDigito ( digito )
DEFINE
   digito   CHAR(1),
   valido   SMALLINT

   LET valido = FALSE

   CASE digito
      WHEN "0" LET valido = TRUE
      WHEN "1" LET valido = TRUE
      WHEN "2" LET valido = TRUE
      WHEN "3" LET valido = TRUE
      WHEN "4" LET valido = TRUE
      WHEN "5" LET valido = TRUE
      WHEN "6" LET valido = TRUE
      WHEN "7" LET valido = TRUE
      WHEN "8" LET valido = TRUE
      WHEN "9" LET valido = TRUE
      WHEN "K" LET valido = TRUE
   END CASE
   RETURN valido
END FUNCTION