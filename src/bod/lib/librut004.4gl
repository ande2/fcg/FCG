{
Programa : librut003.4gl 
Programo : Mynor Ramirez
Objetivo : Subrutinas de librerias -- chequeo de integridad de datos
}

DATABASE storepos 

-- Subrutina para verificar si los usuarios ya tienen registros

FUNCTION librut004_IntegridadUsuarios(wuserid)
 DEFINE wuserid  LIKE glb_usuarios.userid,
        conteo   INTEGER  

 -- Verificando pesaje
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_mtransac a 
  WHERE (a.userid = wuserid)
  IF (conteo>0) THEN
     RETURN 1
  ELSE
     RETURN 0
  END IF
END FUNCTION

-- Subrutina para verificar si el perfil ya tiene registros 

FUNCTION librut004_IntegridadPerfiles(wroleid) 
 DEFINE wroleid  LIKE glb_usuarios.roleid,
        conteo   INTEGER  

 -- Verificando usuarios 
 SELECT COUNT(*)
  INTO  conteo
  FROM  glb_usuarios a
  WHERE (a.roleid = wroleid)
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION

-- Subrutina para verificar si el tipo de documento ya tiene registros 

FUNCTION librut004_IntegridadTiposDocumento(wtipdoc) 
 DEFINE wtipdoc LIKE fac_tipodocs.tipdoc,
        conteo  INTEGER 

 -- Verificando 
 -- Tipos de documento por punto de venta 
 SELECT COUNT(*)
  INTO  conteo
  FROM  fac_tdocxpos a 
  WHERE (a.tipdoc = wtipdoc)
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     -- Facturacion 
     SELECT COUNT(*)
      INTO  conteo
      FROM  fac_mtransac a 
      WHERE (a.tipdoc = wtipdoc)
      IF (conteo>0) THEN
         RETURN TRUE
      ELSE
         RETURN FALSE
      END IF
  END IF
END FUNCTION 

-- Subrutina para verificar si la division de comida ya tiene registros 

FUNCTION librut004_IntegridadDivisionesComida(wcoddiv) 
 DEFINE wcoddiv LIKE sre_mrecetas.coddiv,
        conteo  INTEGER 

 -- Verificando 
 -- Divisiones de platos de comida 
 SELECT COUNT(*)
  INTO  conteo
  FROM  sre_mrecetas a 
  WHERE (a.coddiv = wcoddiv)
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION 

-- Subrutina para verificar si el punto de venta ya tiene registrados

FUNCTION librut004_IntegridadPuntosVenta(wnumpos) 
 DEFINE wnumpos  LIKE fac_puntovta.numpos,
        conteo   INTEGER  

 -- Verificando 
 -- Tipos de documento x punto de venta
 SELECT COUNT(*)
  INTO  conteo
  FROM  fac_tdocxpos a
  WHERE (a.numpos = wnumpos)
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     -- Ordenes 
     SELECT COUNT(*)
      INTO  conteo
      FROM  sre_mordenes a 
      WHERE (a.numpos = wnumpos)
      IF (conteo>0) THEN
         RETURN TRUE
      ELSE
         -- Facturas 
         SELECT COUNT(*)
          INTO  conteo
          FROM  fac_mtransac a 
          WHERE (a.numpos = wnumpos)
          IF (conteo>0) THEN
             RETURN TRUE
          ELSE
             RETURN FALSE
          END IF 
      END IF 
  END IF
END FUNCTION
