{ 
Programa : librut003.4gl 
Programo : Mynor Ramirez
Objetivo : Subrutinas de uso general
}

DATABASE storepos 

DEFINE qrytext STRING 

-- Subrutina que carga el combobox de los modulos de un sistema

FUNCTION librut003_CbxModulos()
 -- Llenando combobox
 LET qrytext = "SELECT a.codmod,a.nommod,a.numord ",
               " FROM glb_mmodulos a ",
               " WHERE a.estado = 1 ",
               " ORDER BY 3 "

 CALL librut002_combobox("codmod",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de recetas  

FUNCTION librut003_cbxrecetas() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.numrec,a.nomrec||' '||a.numrec ",
               " FROM sre_mrecetas a ",
               " ORDER BY 2 "

 CALL librut002_combobox("numrec",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las divisiones  

FUNCTION librut003_cbxdivisionesrecetas() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.coddiv,a.nomdiv ",
               " FROM sre_divrecet a ",
               " ORDER BY 2 "

 CALL librut002_combobox("coddiv",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las empresas  

FUNCTION librut003_CbxEmpresas() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.codemp,a.nomemp ",
               " FROM glb_empresas a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codemp",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las sucursales  

FUNCTION librut003_cbxsucursales(wcodemp) 
 DEFINE wcodemp LIKE glb_empresas.codemp 

 -- Llenando combobox 
 LET qrytext = "SELECT a.codsuc,a.nomsuc ",
               " FROM  glb_sucsxemp a ",
               " WHERE a.codemp = ",wcodemp,
               " ORDER BY 2 "

 CALL librut002_combobox("codsuc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las categorias 

FUNCTION librut003_cbxcategorias()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codcat,a.nomcat ",
               " FROM glb_categors a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codcat",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las marcas 

FUNCTION librut003_CbxMarcas()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codmar,a.nommar ",
               " FROM inv_marcapro a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codmar",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los departos

FUNCTION librut003_CbxDepartos() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.coddep,a.nomdep ",
               " FROM  glb_departos a ",
               " ORDER BY 2 "

 CALL librut002_combobox("coddep",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las ciudades x pais

FUNCTION librut003_cbxciudades(wcodpai)
 DEFINE wcodpai LIKE glb_ciudades.codpai

 -- Llenando combobox
 LET qrytext = "SELECT a.ciudad,a.nomciu ",
               " FROM  glb_ciudades a ",
               " WHERE a.codpai = ",wcodpai,
               " ORDER BY 2 "

 CALL librut002_combobox("ciudad",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los proveedores 

FUNCTION librut003_cbxproveedores()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codprv,a.nomprv ",
               " FROM inv_provedrs a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codprv",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los proveedores de combustible

FUNCTION librut003_cbxproveedorescombustible()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codprv,a.nomprv ",
               " FROM inv_provedrs a ",
               " WHERE a.codalt = '1' ",
               " ORDER BY 2 "

 CALL librut002_combobox("codprv",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los origenes

FUNCTION librut003_cbxOrigenesMovtos()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codprv,a.nomprv ",
               " FROM inv_provedrs a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codori",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las undiades de medida 

FUNCTION librut003_cbxunidadesmedida(campoforma)
 DEFINE campoforma VARCHAR(15)

 -- Llenando combobox 
 LET qrytext = "SELECT a.unimed,a.nommed ",
               " FROM inv_unimedid a ",
               " ORDER BY 2 "

 CALL librut002_combobox(campoforma,qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las subcategorias 

FUNCTION librut003_cbxsubcategorias(wcodcat) 
 DEFINE wcodcat LIKE glb_subcateg.codcat 

 -- Llenando combobox 
 LET qrytext = "SELECT a.subcat,a.nomsub ",
               " FROM  glb_subcateg a ",
               " WHERE a.codcat = ",wcodcat,
               " ORDER BY 2 "

 CALL librut002_combobox("subcat",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las subcategorias sin categoria 

FUNCTION librut003_cbxsubcateg() 
 DEFINE wcodcat LIKE glb_subcateg.codcat 

 -- Llenando combobox 
 LET qrytext = "SELECT a.subcat,a.nomsub ",
               " FROM  glb_subcateg a ",
               " WHERE a.lonmed = 1 ",
               " ORDER BY 2 "

 CALL librut002_combobox("subcat",qrytext)
END FUNCTION

-- Subrutina que carga el combobox los perfiles

FUNCTION librut003_cbxperfiles()
 -- Llenando combobox
 LET qrytext = "SELECT a.roleid,a.nmrole ",
               " FROM glb_rolesusr a ",
               " ORDER BY 2 "

 CALL librut002_combobox("roleid",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de productos 

FUNCTION librut003_cbxproductosabr() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.cditem,a.codabr ",
               " FROM  inv_products a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codabr",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de descripciones de producto }

FUNCTION librut003_cbxproductosdes(wcodcat,wsubcat)
 DEFINE wcodcat   LIKE inv_products.codcat,
        wsubcat   LIKE inv_products.subcat,
        strcodcat STRING, 
        strsubcat STRING

 -- Verificando categoria
 LET strcodcat = NULL
 IF wcodcat IS NOT NULL THEN
    LET strcodcat = " AND a.codcat = ",wcodcat
 END IF

 -- Verificando subcategoria
 LET strsubcat = NULL
 IF wsubcat IS NOT NULL THEN
    LET strsubcat = " AND a.subcat = ",wsubcat
 END IF

 -- Llenando combobox
 LET qrytext = "SELECT a.cditem,trim(a.dsitem)||' ('||trim(a.codabr)||')' ",
               " FROM  inv_products a ",
               " WHERE a. estado = 1 ",
               strcodcat CLIPPED,
               strsubcat CLIPPED,
               " ORDER BY 2 "

 CALL librut002_combobox("cditem",qrytext)
END FUNCTION


-- Subrutina que carga el combobox de bodegas

FUNCTION librut003_CbxBodegass(wcamfor)
DEFINE wcamfor STRING
 -- Llenando combobox
 LET qrytext = "SELECT a.codbod,a.nombod ",
               " FROM inv_mbodegas a ",
               " ORDER BY 2 "

 CALL librut002_combobox(wcamfor,qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las bodegas x usuario 

FUNCTION librut003_cbxbodegasxusuario(wuserid) 
 DEFINE wuserid LIKE inv_permxbod.userid 

 -- Llenando combobox 
 LET qrytext = "SELECT a.codbod,a.nombod ",
               " FROM  inv_mbodegas a ",
               " WHERE EXISTS (SELECT b.userid FROM inv_permxbod b ",
                               "WHERE b.codbod = a.codbod ",
                                " AND b.userid = '",wuserid CLIPPED,"')",
               " ORDER BY 2 "

 CALL librut002_combobox("codbod",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de las bodegas }

FUNCTION librut003_cbxbodegas(whayfac)
 DEFINE whayfac SMALLINT,
        wstrfac STRING

 -- Verificando condicion de facturacion
 LET wstrfac = NULL
 CASE (whayfac)
  WHEN 0 LET wstrfac = "WHERE a.hayfac = 0"
  WHEN 1 LET wstrfac = "WHERE a.hayfac = 1"
  WHEN 2 LET wstrfac = NULL
 END CASE 

 -- Llenando combobox
 LET qrytext = "SELECT a.codbod,a.nombod ",
               " FROM  inv_mbodegas a ",
               wstrfac CLIPPED,
               " ORDER BY 2 "

 CALL librut002_combobox("codbod",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los paises 

FUNCTION librut003_cbxpaises()
 -- Llenando combobox
 LET qrytext = "SELECT a.codpai,nompai ",
               " FROM  glb_mtpaises a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codpai",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de movimientos de inventario x usuario 

FUNCTION librut003_cbxtipomovxusuario(wuserid) 
 DEFINE wuserid LIKE inv_permxtmv.userid 

 -- Llenando combobox 
 LET qrytext = "SELECT a.tipmov,a.nommov ",
               " FROM  inv_tipomovs a ",
               " WHERE EXISTS (SELECT b.userid FROM inv_permxtmv b ",
                               "WHERE b.tipmov = a.tipmov ",
                                " AND b.userid = '",wuserid CLIPPED,"')",
               " ORDER BY 2 "

 CALL librut002_combobox("tipmov",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de movimientos 

FUNCTION librut003_CbxTiposMovimiento() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.tipmov,a.nommov ",
               " FROM  inv_tipomovs a ",
               " ORDER BY 2 "

 CALL librut002_combobox("tipmov",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los grupos de tipos de movimientos 

FUNCTION librut003_CbxGruposTiposMovimiento() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.codgru,a.nomgru ",
               " FROM  inv_grupomov a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codgru",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los empaques x producto 

FUNCTION librut003_cbxempaques(wcditem,filtro)
 DEFINE wcditem LIKE inv_epqsxpro.cditem,
        filtro  SMALLINT

 -- Llenando combobox
 IF filtro THEN 
    LET qrytext = "SELECT a.codepq,a.nomepq ",
                  " FROM  inv_epqsxpro a ",
                  " WHERE a.cditem = ",wcditem, 
                  " ORDER BY 2 "
 ELSE
    LET qrytext = "SELECT a.codepq,a.nomepq ",
                  " FROM  inv_epqsxpro a ",
                  " ORDER BY 2 "
 END IF 

 CALL librut002_combobox("codepq",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los destinos

FUNCTION librut003_CbxDestinosMovtos()
 -- Llenando combobox
 LET qrytext = "SELECT a.codcli,TRIM(a.nomcli)||' ('||a.codcli||')' ",
               " FROM fac_clientes a ",
               " ORDER BY 2 "

 CALL librut002_combobox("coddes",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los destinos del producto

FUNCTION librut003_CbxDestinos()
 -- Llenando combobox 
 LET qrytext = "SELECT a.tipdes,a.nomdes ",
               " FROM inv_destipro a ",
               " ORDER BY 2 "

 CALL librut002_combobox("tipdes",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los puntos de venta 

FUNCTION librut003_cbxpuntosventa()
 -- Llenando combobox 
 LET qrytext = "SELECT a.numpos,a.nompos ",
               " FROM fac_puntovta a ",
               " ORDER BY 2 "

 CALL librut002_combobox("numpos",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las divisiones de recetas 

FUNCTION librut003_cbxdivrecetas()
 -- Llenando combobox 
 LET qrytext = "SELECT a.coddiv,a.nomdiv ",
               " FROM sre_divrecet a ",
               " ORDER BY 2 "

 CALL librut002_combobox("coddiv",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los puntos de venta x usuario

FUNCTION librut003_CbxPuntoVentaXUsuario(userid) 
 DEFINE userid LIKE fac_permxpos.userid 

 -- Llenando combobox
 LET qrytext = "SELECT a.numpos,a.nompos ",
               " FROM fac_puntovta a ",
               " WHERE exists (SELECT x.numpos FROM fac_permxpos x ",
                              " WHERE x.numpos = a.numpos ",
                              "   AND x.userid = '",userid CLIPPED,"')",
               " ORDER BY 2 "

 CALL librut002_combobox("numpos",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las fases de una orden de trabajo 

FUNCTION librut003_cbxfasesot()
 -- Llenando combobox 
 LET qrytext = "SELECT a.tippar,a.valchr ",
               " FROM glb_paramtrs a ",
               " WHERE a.numpar = 100 ", 
               " ORDER BY 1 "

 -- Llenando combobox 
 CALL librut002_combobox("nofase",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los cajeros, x puntos de venta, empresa y fecha de corte

FUNCTION librut003_cbxcajeroscorte(wnumpos,wcodemp,ts)
 DEFINE wnumpos      LIKE fac_mtransac.numpos,
        wcodemp      LIKE fac_mtransac.codemp,
        numreg       SMALLINT,
        ts           SMALLINT,
        strfeccorte  STRING

 -- Condicion de corte
 CASE (ts)
  WHEN 0 LET strfeccorte = " AND a.feccor IS NOT NULL " 
  WHEN 1 LET strfeccorte = " AND a.feccor IS NULL " 
  WHEN 2 LET strfeccorte = NULL 
 END CASE 

 -- Llenando combobox 
 LET qrytext = "SELECT UNIQUE a.userid,b.nomusr ",
               " FROM  fac_mtransac a,glb_usuarios b ",
               " WHERE a.numpos = ",wnumpos,
                 " AND a.codemp = ",wcodemp,
                 strfeccorte CLIPPED,
                 " AND b.userid = a.userid ",
               " ORDER BY 2 "

 LET numreg = librut002_cbxdin("cajero",qrytext)
 RETURN numreg
END FUNCTION

-- Subrutina que carga el combobox de rutas, x puntos de venta, empresa y fecha de corte

FUNCTION librut003_CbxRutasCorte(wnumpos,wcodemp,ts)
 DEFINE wnumpos      LIKE fac_mtransac.numpos,
        wcodemp      LIKE fac_mtransac.codemp,
        numreg       SMALLINT,
        ts           SMALLINT,
        strfeccor    STRING,
        strcodemp    STRING

 -- Condicion de corte
 CASE (ts)
  WHEN 0 LET strfeccor = " AND a.feccor IS NOT NULL " 
  WHEN 1 LET strfeccor = " AND a.feccor IS NULL " 
  WHEN 2 LET strfeccor = NULL 
 END CASE 

 -- Condicion de empresa
 LET strcodemp = NULL 
 IF wcodemp IS NOT NULL THEN 
    LET strcodemp = " AND a.codemp = ",wcodemp
 END IF

 -- Llenando combobox 
 LET qrytext = "SELECT UNIQUE a.noruta,b.nomrut ",
               " FROM  fac_mtransac a,vta_rutasmer b ",
               " WHERE a.numpos = ",wnumpos,
               strcodemp CLIPPED,
               strfeccor CLIPPED,
               " AND b.noruta = a.noruta ",
               " ORDER BY 2 "

 LET numreg = librut002_cbxdin("noruta",qrytext)
 RETURN numreg
END FUNCTION

-- Subrutina que carga el combobox de los cajeros con corte de ventas 

FUNCTION librut003_cbxcajeros()
 DEFINE numreg SMALLINT 

 -- Llenando combobox
 LET qrytext = "SELECT UNIQUE a.userid,b.nomusr ",
               " FROM  fac_vicortes a,glb_usuarios b ",
               " WHERE b.userid = a.userid ",
               " ORDER BY 2 "

 LET numreg = librut002_cbxdin("cajero",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los puntos de venta x cajero }

FUNCTION librut003_cbxpuntosventaxcajero(wuserid)
 DEFINE wuserid LIKE fac_usuaxpos.userid

 -- Llenando combobox 
 LET qrytext = "SELECT a.numpos,a.nompos ",
               " FROM fac_puntovta a,fac_usuaxpos b ",
               " WHERE a.numpos = b.numpos ",
                 " AND b.userid = '",wuserid CLIPPED,"'",
               " ORDER BY 2 "

 CALL librut002_combobox("numpos",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de documento 

FUNCTION librut003_cbxtiposdocumento(wtipqry)
 DEFINE wtipqry SMALLINT,
        wwhere  STRING 
 
 -- Llenando combobox 
 IF wtipqry=100 THEN 
  LET qrytext = "SELECT a.tipdoc,a.nomdoc ",
                " FROM  fac_tipodocs a ",
                " WHERE a.tipdoc = 1 ", 
                " ORDER BY 2 "
 ELSE
  LET qrytext = "SELECT a.tipdoc,a.nomdoc ",
                " FROM fac_tipodocs a ",
                " ORDER BY 2 "
 END IF 

 CALL librut002_combobox("tipdoc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de documento por punto de venta 
-- Solamente los que estan en estado activo 

FUNCTION librut003_cbxtiposdocxpos(wnumpos)
 DEFINE wnumpos LIKE fac_tdocxpos.numpos
 
 -- Llenando combobox 
 LET qrytext = "SELECT a.lnktdc,a.nomdoc ",
                "FROM  vis_tipdocxpos a ",
                "WHERE a.numpos = ",wnumpos,
                --"  AND exists (SELECT x.lnktdc FROM fac_permxtdc x ",
                              --" WHERE x.lnktdc = a.lnktdc ",
                              --"   AND x.userid = USER) ",
                " ORDER BY 2"

 CALL librut002_combobox("lnktdc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los autorizantes

FUNCTION librut003_CbxAutorizantes()
 -- Llenando combobox
 LET qrytext = "SELECT a.codaut,a.nomaut ",
               " FROM  glb_userauth a ",
               " WHERE a.estado = 1 ",
               " ORDER BY 2 "

 CALL librut002_combobox("codaut",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los empleados 

FUNCTION librut003_CbxEmpleados()
 -- Llenando combobox
 LET qrytext = "SELECT a.codepl,a.nomemp ",
               " FROM  pla_empleads a ",
               " WHERE a.estado = 1 ",
               " ORDER BY 2 "

 CALL librut002_combobox("codepl",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de documento con impuesto por punto de venta

FUNCTION librut003_cbxtiposdocxposimp(wnumpos)
 DEFINE wnumpos LIKE fac_tdocxpos.numpos
 
 -- Llenando combobox 
 LET qrytext = "SELECT a.lnktdc,a.nomdoc ",
                "FROM  vis_tipdocxpos a ",
                "WHERE a.numpos = ",wnumpos,
                " AND  a.hayimp = 1", 
                " ORDER BY 2"

 CALL librut002_combobox("lnktdc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de documento por punto de venta 
-- Todos los tipos de documento 

FUNCTION librut003_cbxtiposdocxpostodos(wnumpos)
 DEFINE wnumpos LIKE fac_tdocxpos.numpos
 
 -- Llenando combobox 
 LET qrytext = "SELECT a.lnktdc,a.nomdoc ",
                "FROM  vis_tipdocxpostodos a ",
                "WHERE a.numpos = "||wnumpos,
                " ORDER BY 2"

 CALL librut002_combobox("lnktdc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las taras

FUNCTION librut003_CbxTaras()
 -- Llenando combobox
 LET qrytext = "SELECT a.codtar,a.destar ",
               " FROM  glb_pesotara a ",
               " ORDER BY 1 "

 CALL librut002_combobox("codtar",qrytext)
END FUNCTION

-- Subrutina que llena el combobox de anios 

FUNCTION librut003_cbxanio(wannioc)
 DEFINE cba       ui.ComboBox,
        wannioc,i INT

 -- Asignando anio presente y el anterior
 LET cba = ui.ComboBox.forname("aniotr")
 CALL cba.clear()

 -- Verificando si anio tiene valor
 IF (wannioc=0) THEN
    LET wannioc = YEAR(CURRENT)-1
    FOR i=1 TO 2
     CALL cba.addItem(wannioc,wannioc)
     LET wannioc = (wannioc+1)
    END FOR
 ELSE
    CALL cba.addItem(wannioc,wannioc)
 END IF
END FUNCTION

-- Subrutina que carga el combobox de los delivery 

FUNCTION librut003_CbxDelivery()
 -- Llenando combobox
 LET qrytext = "SELECT a.coddlv,a.nomdlv ",
               " FROM glb_delivery a ",
               " WHERE a.estado = 1 ",
               " ORDER BY 2 "

 CALL librut002_combobox("coddlv",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las personas que elaboran los platos

FUNCTION librut003_CbxElaboradores()
 -- Llenando combobox
 LET qrytext = "SELECT a.codela,a.nomela ",
               " FROM glb_elabodrs a ",
               " WHERE a.estado = 1 ",
               " ORDER BY 2 "

 CALL librut002_combobox("codela",qrytext)
END FUNCTION

-- Subrutina para crear el combobox de mesas 

FUNCTION librut003_CbxMesas(nmesas,xnomesa)
 DEFINE nmesas,i   SMALLINT,
        xnomesa    SMALLINT, 
        listamesas STRING 

 LET listamesas = NULL
 FOR i = 1 TO nmesas
  IF (i=xnomesa) THEN
     CONTINUE FOR
  END IF 
  LET listamesas = listamesas CLIPPED,"|",i USING "<<" 
 END FOR 

 CALL librut002_combobox("numesa",listamesas)
END FUNCTION 

-- Subrutina que carga el combobox de las fechas de inventario fisico

FUNCTION librut003_cbxfecfisico(wcodemp,wcodsuc,wcodbod)
 DEFINE wcodemp LIKE inv_tofisico.codemp,
        wcodsuc LIKE inv_tofisico.codsuc,
        wcodbod LIKE inv_tofisico.codbod

 -- Llenando combobox
 LET qrytext = "SELECT UNIQUE a.fecinv,a.fecinv ",
               " FROM  inv_tofisico a ",
               " WHERE a.codemp = ",wcodemp,
                 " AND a.codsuc = ",wcodsuc,
                 " AND a.codbod = ",wcodbod,
               " ORDER BY 1 DESC"

 CALL librut002_combobox("fecinv",qrytext)
END FUNCTION

-- Subrutina para buscar los datos de un tipo de movimiento (inv_tipomovs) 

FUNCTION librut003_btipomovimiento(wtipmov)
 DEFINE w_tip_mov RECORD LIKE inv_tipomovs.*,
        wtipmov   LIKE inv_tipomovs.tipmov

 INITIALIZE w_tip_mov.* TO NULL 
 SELECT a.*
  INTO  w_tip_mov.*
  FROM  inv_tipomovs a
  WHERE (a.tipmov = wtipmov)
  IF (status=NOTFOUND) THEN
     RETURN w_tip_mov.*,FALSE
  ELSE
     RETURN w_tip_mov.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una empresa (glb_empresas) 

FUNCTION librut003_bempresa(wcodemp)
 DEFINE w_mae_emp RECORD LIKE glb_empresas.*,
        wcodemp   LIKE glb_empresas.codemp

 INITIALIZE w_mae_emp.* TO NULL 
 SELECT a.*
  INTO  w_mae_emp.*
  FROM  glb_empresas a
  WHERE (a.codemp = wcodemp)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_emp.*,FALSE
  ELSE
     RETURN w_mae_emp.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una sucursal (glb_sucsxemp) 

FUNCTION librut003_bsucursal(wcodsuc)
 DEFINE w_mae_suc RECORD LIKE glb_sucsxemp.*,
        wcodsuc   LIKE glb_sucsxemp.codsuc

 INITIALIZE w_mae_suc.* TO NULL 
 SELECT a.*
  INTO  w_mae_suc.*
  FROM  glb_sucsxemp a
  WHERE (a.codsuc = wcodsuc)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_suc.*,FALSE
  ELSE
     RETURN w_mae_suc.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una bodega (inv_mbodegas) 

FUNCTION librut003_bbodega(wcodbod)
 DEFINE w_mae_bod RECORD LIKE inv_mbodegas.*,
        wcodbod   LIKE inv_mbodegas.codbod

 INITIALIZE w_mae_bod.* TO NULL 
 SELECT a.*
  INTO  w_mae_bod.*
  FROM  inv_mbodegas a
  WHERE (a.codbod = wcodbod)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_bod.*,FALSE
  ELSE
     RETURN w_mae_bod.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un producto (inv_products) 

FUNCTION librut003_BProducto(wcditem)
 DEFINE w_inv_pro RECORD LIKE inv_products.*,
        wcditem   LIKE inv_products.cditem

 INITIALIZE w_inv_pro.* TO NULL 
 SELECT a.*
  INTO  w_inv_pro.*
  FROM  inv_products a
  WHERE (a.cditem = wcditem)
  IF (status=NOTFOUND) THEN
     RETURN w_inv_pro.*,FALSE
  ELSE
     RETURN w_inv_pro.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un producto x su codigo abreviado(inv_products) 

FUNCTION librut003_bproductoabr(wcodabr)
 DEFINE w_inv_pro RECORD LIKE inv_products.*,
        wcodabr   LIKE inv_products.codabr

 INITIALIZE w_inv_pro.* TO NULL 
 SELECT a.*
  INTO  w_inv_pro.*
  FROM  inv_products a
  WHERE (a.codabr= wcodabr)
  IF (status=NOTFOUND) THEN
     RETURN w_inv_pro.*,FALSE
  ELSE
     RETURN w_inv_pro.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un proveedor (inv_provedrs) 

FUNCTION librut003_bproveedor(wcodprv)
 DEFINE w_mae_prv RECORD LIKE inv_provedrs.*,
        wcodprv   LIKE inv_provedrs.codprv

 INITIALIZE w_mae_prv.* TO NULL 
 SELECT a.*
  INTO  w_mae_prv.*
  FROM  inv_provedrs a
  WHERE (a.codprv= wcodprv)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_prv.*,FALSE
  ELSE
     RETURN w_mae_prv.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una unidad de medida (inv_unimedid) 

FUNCTION librut003_bumedida(wunimed)
 DEFINE w_mae_reg RECORD LIKE inv_unimedid.*,
        wunimed   LIKE inv_unimedid.unimed

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  inv_unimedid a
  WHERE (a.unimed= wunimed)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un peso tara (glb_pesotara) 

FUNCTION librut003_bpesotara(wcodtar)
 DEFINE w_mae_reg RECORD LIKE glb_pesotara.*,
        wcodtar   LIKE glb_pesotara.codtar

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  glb_pesotara a
  WHERE (a.codtar = wcodtar)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un movimiento (inv_mtransac) 

FUNCTION librut003_bmovimiento(wlnktra)
 DEFINE w_mae_tra RECORD LIKE inv_mtransac.*,
        wlnktra   LIKE inv_mtransac.lnktra

 INITIALIZE w_mae_tra.* TO NULL 
 SELECT a.*
  INTO  w_mae_tra.*
  FROM  inv_mtransac a
  WHERE (a.lnktra= wlnktra)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_tra.*,FALSE
  ELSE
     RETURN w_mae_tra.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un documento de facturacion (fac_mtransac)

FUNCTION librut003_bfacturacion(wlnktra)
 DEFINE w_mae_tra RECORD LIKE fac_mtransac.*,
        wlnktra   LIKE fac_mtransac.lnktra

 INITIALIZE w_mae_tra.* TO NULL
 SELECT a.*
  INTO  w_mae_tra.*
  FROM  fac_mtransac a
  WHERE (a.lnktra= wlnktra)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_tra.*,FALSE
  ELSE
     RETURN w_mae_tra.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un punto de venta (fac_puntovta) 

FUNCTION librut003_bpuntovta(wnumpos)
 DEFINE w_mae_reg RECORD LIKE fac_puntovta.*,
        wnumpos   LIKE fac_puntovta.numpos

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  fac_puntovta a
  WHERE (a.numpos= wnumpos)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un usuario (glb_usuarios) 

FUNCTION librut003_busuario(wuserid)
 DEFINE w_mae_reg RECORD LIKE glb_usuarios.*,
        wuserid   LIKE glb_usuarios.userid

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  glb_usuarios a
  WHERE (a.userid= wuserid)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un tipo de documento x punto de venta (fac_tdocxpos) 

FUNCTION librut003_btdocxpos(wlnktdc)
 DEFINE w_mae_reg RECORD LIKE fac_tdocxpos.*,
        wlnktdc   LIKE fac_tdocxpos.lnktdc

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  fac_tdocxpos a
  WHERE (a.lnktdc= wlnktdc)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un tipo de documento (fac_tipodocs) 

FUNCTION librut003_btipodoc(wtipdoc)
 DEFINE w_mae_reg RECORD LIKE fac_tipodocs.*,
        wtipdoc   LIKE fac_tipodocs.tipdoc

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  fac_tipodocs a
  WHERE (a.tipdoc= wtipdoc)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END function

-- Subrutina para buscar los datos de un empaque x producto

FUNCTION librut003_bempaquexpro(wcditem,wcodepq)            
 DEFINE w_mae_reg RECORD LIKE inv_epqsxpro.*,
        wcditem   LIKE inv_epqsxpro.cditem,
        wcodepq   LIKE inv_epqsxpro.codepq

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  inv_epqsxpro a
  WHERE (a.cditem = wcditem)
    AND (a.codepq = wcodepq)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para obtener parametros del sistema

FUNCTION librut003_parametros(wnumpar,wtippar)
 DEFINE wnumpar LIKE glb_paramtrs.numpar,
        wtippar LIKE glb_paramtrs.tippar,
        wvalchr LIKE glb_paramtrs.valchr

 -- Seleccionando parametros
 INITIALIZE wvalchr TO NULL
 SELECT a.valchr
  INTO  wvalchr
  FROM  glb_paramtrs a
  WHERE (a.numpar = wnumpar)
    AND (a.tippar = wtippar)
  IF (status!=NOTFOUND) THEN
     RETURN TRUE,wvalchr
  ELSE
     RETURN FALSE,wvalchr
  END IF
END FUNCTION

-- Subrutina para obtener los saldos de un producto por empresa, sucursal, bodega, 
-- tipo de saldo, anio, mes y producto

FUNCTION librut003_saldosxproxmes(wcodemp,wcodsuc,wcodbod,wtipsld,waniotr,wmestra,wcditem)
 DEFINE wcodemp LIKE inv_saldopro.codemp, 
        wcodsuc LIKE inv_saldopro.codsuc, 
        wcodbod LIKE inv_saldopro.codbod, 
        wtipsld LIKE inv_saldopro.tipsld, 
        waniotr LIKE inv_saldopro.aniotr, 
        wmestra LIKE inv_saldopro.mestra, 
        wcditem LIKE inv_saldopro.cditem, 
        wsaluni LIKE inv_saldopro.canuni,
        wsalval LIKE inv_saldopro.totpro 

 -- Obteniendo saldos
 SELECT NVL(SUM(a.canuni),0),NVL(SUM(a.totpro),0)
  INTO  wsaluni,wsalval 
  FROM  inv_saldopro a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.tipsld = wtipsld 
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem 

 IF wsaluni IS NULL THEN LET wsaluni = 0 END IF 
 IF wsalval IS NULL THEN LET wsalval = 0 END IF 

 RETURN wsaluni,wsalval
END FUNCTION 

-- Subrutina para obtener los movimientos de un producto por empresa, sucursal, bodega, anio, mes y producto

FUNCTION librut003_saldosxmovxmes(wcodemp,wcodsuc,wcodbod,waniotr,wmestra,wcditem,wtipope)
 DEFINE wcodemp LIKE inv_dtransac.codemp, 
        wcodsuc LIKE inv_dtransac.codsuc, 
        wcodbod LIKE inv_dtransac.codbod, 
        waniotr LIKE inv_dtransac.aniotr, 
        wmestra LIKE inv_dtransac.mestra, 
        wcditem LIKE inv_dtransac.cditem, 
        wtipope LIKE inv_dtransac.tipope, 
        wsaluni LIKE inv_dtransac.canuni,
        wsalval LIKE inv_dtransac.totpro 

 -- Obteniendo saldos
 SELECT a.saluni,a.salval
  INTO  wsaluni,wsalval 
  FROM  vis_saldosxmovxmes a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem 
    AND a.tipope = wtipope

 IF wsaluni IS NULL THEN LET wsaluni = 0 END IF 
 IF wsalval IS NULL THEN LET wsalval = 0 END IF 

 RETURN wsaluni,wsalval
END FUNCTION 

-- Subrutina para obtener el saldo fisico de un producto

FUNCTION librut003_fisicoxproxmes(wcodemp,wcodsuc,wcodbod,waniotr,wmestra,wcditem)
 DEFINE wcodemp LIKE inv_tofisico.codemp, 
        wcodsuc LIKE inv_tofisico.codsuc, 
        wcodbod LIKE inv_tofisico.codbod, 
        waniotr LIKE inv_tofisico.aniotr, 
        wmestra LIKE inv_tofisico.mestra, 
        wcditem LIKE inv_tofisico.cditem, 
        wfisuni LIKE inv_tofisico.canuni,
        wfisval LIKE inv_tofisico.totpro,
        wfecinv DATE 
 
 -- Obteniendo saldos
 SELECT NVL(SUM(a.canuni),0),NVL(SUM(a.totpro),0)
  INTO  wfisuni,wfisval 
  FROM  inv_tofisico a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem 
    AND a.finmes = 1

 IF wfisuni IS NULL THEN LET wfisuni = 0 END IF 
 IF wfisval IS NULL THEN LET wfisval = 0 END IF 

 RETURN wfisuni,wfisval
END FUNCTION 

-- Subrutina para chequear la existencia de un producto

FUNCTION librut003_chkexistencia(wcodemp,wcodsuc,wcodbod,wcditem)
 DEFINE wcodemp LIKE inv_proenbod.codemp, 
        wcodsuc LIKE inv_proenbod.codsuc, 
        wcodbod LIKE inv_proenbod.codbod, 
        wcditem LIKE inv_proenbod.cditem, 
        wexican LIKE inv_proenbod.exican 

 -- Obteniendo existencia
 SELECT NVL(a.exican,0)
  INTO  wexican
  FROM  inv_proenbod a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.cditem = wcditem 

 IF (wexican IS NULL) THEN LET wexican = 0 END IF

 RETURN wexican 
END FUNCTION 

-- Subrutina para buscar el saldo anterior de un producto x empresa, sucursal, bodega y fecha

FUNCTION librut003_saldoantxpro(wcodemp,wcodsuc,wcodbod,wcditem,wfecini)
 DEFINE wcodemp LIKE inv_proenbod.codemp, 
        wcodsuc LIKE inv_proenbod.codsuc, 
        wcodbod LIKE inv_proenbod.codbod, 
        wcditem LIKE inv_proenbod.cditem, 
        wmovcan LIKE inv_proenbod.exican,
        wmovval LIKE inv_proenbod.exival,
        wfecini DATE,
        wfecfin DATE 

 -- Calculando saldo de movimientos antes de la fecha inicial
 SELECT NVL(SUM(a.opeuni),0),
        NVL(SUM(a.opeval),0)
  INTO  wmovcan,
        wmovval
  FROM  inv_dtransac a
  WHERE a.codemp  = wcodemp
    AND a.codsuc  = wcodsuc
    AND a.codbod  = wcodbod
    AND a.cditem  = wcditem
    AND a.fecemi  < wfecini
    AND a.estado  = "V"
    AND a.actexi  = 1 

 IF (wmovcan IS NULL) THEN LET wmovcan = 0 END IF
 IF (wmovval IS NULL) THEN LET wmovval = 0 END IF

 RETURN wmovcan,wmovval 
END FUNCTION 

-- Subrutina para obtener la cantidad equivalente de un empaque

FUNCTION librut003_canempaque(wcditem,wcodepq)
 DEFINE wcanepq  LIKE inv_epqsxpro.cantid,
        wcditem  LIKE inv_epqsxpro.cditem,
        wcodepq  LIKE inv_epqsxpro.codepq

 -- Obteniendo cantidad de empaque
 SELECT NVL(a.cantid,1)
  INTO  wcanepq
  FROM  inv_epqsxpro a
  WHERE a.cditem = wcditem
    AND a.codepq = wcodepq
  IF wcanepq IS NULL THEN
     LET wcanepq = 1
  END IF

 RETURN wcanepq
END FUNCTION

-- Subrutina para buscar los datos de un programa (glb_programs)

FUNCTION librut003_bprograma(wcodpro)
 DEFINE w_mae_reg RECORD LIKE glb_programs.*,
        wcodpro   LIKE glb_programs.codpro

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  glb_programs a
  WHERE (a.codpro= wcodpro)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un programa (glb_programs)

FUNCTION librut003_bpermprog(wcodpro,wprogid)
 DEFINE wcodpro   LIKE glb_dprogram.codpro,
        wprogid   LIKE glb_dprogram.progid

 SELECT UNIQUE a.progid
  FROM  glb_dprogram a
  WHERE (a.codpro= wcodpro)
    AND (a.progid= wprogid)
  IF (status=NOTFOUND) THEN
     RETURN FALSE
  ELSE
     RETURN TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una subcategoria (glb_subcateg)

FUNCTION librut003_BSubCategoria(wsubcat)
 DEFINE w_mae_reg RECORD LIKE glb_subcateg.*,
        wsubcat   LIKE glb_subcateg.subcat

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  glb_subcateg a
  WHERE (a.subcat= wsubcat)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para contar los registros seleccionados de un qry

FUNCTION librut003_NumeroRegistros(qry)
 DEFINE qry     STRING,
        xdato   CHAR(1),
        totreg  INT

 -- Contando registros
 PREPARE nrows FROM qry
 DECLARE nrg CURSOR FOR nrows
 LET totreg = 1
 FOREACH nrg INTO xdato
  LET totreg = totreg+1
 END FOREACH
 CLOSE nrg
 FREE  nrg
 LET totreg = totreg-1
 RETURN totreg
END FUNCTION

-- Subrutina para la libreria de grupos de proveedores

FUNCTION librut003_CbxGruposProv()
 --llenando combobox
 LET qrytext = "SELECT a.codgru,a.nomgru ",
                "FROM glb_gruprovs a " ,
                "ORDER BY 2 "
 CALL librut002_combobox("codgru",qrytext)
END FUNCTION

-- Subrutina para buscar los datos de un cliente (fac_clientes)

FUNCTION librut003_bcliente(wcodcli)
 DEFINE w_mae_cli RECORD LIKE fac_clientes.*,
        wcodcli   LIKE fac_clientes.codcli

 SELECT a.*
  INTO  w_mae_cli.*
  FROM  fac_clientes a
  WHERE (a.codcli= wcodcli)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_cli.*,FALSE
  ELSE
     RETURN w_mae_cli.*,TRUE
  END IF
END FUNCTION

-- Subrutina que carga el combobox de los tipos de documento afectos al 
--libro de ventas

FUNCTION librut003_CbxTiposDocumentoLibroVentas()
 -- Llenando combobox
 LET qrytext = "SELECT a.tipdoc,a.nomdoc ",
               " FROM  fac_tipodocs a ",
               " WHERE a.tipdoc = 1 ",
               " ORDER BY 2 "

 CALL librut002_combobox("tipdoc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de documento que son otros cobros
-- facturacion 

FUNCTION librut003_CbxTiposDocumentoOtrosCobros()
 -- Llenando combobox
 LET qrytext = "SELECT a.tipdoc,a.nomdoc ",
               " FROM  fac_tipodocs a ",
               " WHERE a.tipdoc > 1 ",
               " ORDER BY 2 "

 CALL librut002_combobox("tipdoc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los empleados activos e inactivos

FUNCTION librut003_CbxEmpleadosTodos()
 -- Llenando combobox
 LET qrytext = "SELECT a.codepl,a.nomemp ",
               " FROM  pla_empleads a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codepl",qrytext)
END FUNCTION

-- Subrutina para obtener el correlativo maximo de un documento por empresa serie y tipo documento de venta 

FUNCTION librut003_correltipdoc(wr)
 DEFINE wr RECORD
         codemp LIKE fac_mtransac.codemp,
         tipdoc LIKE fac_mtransac.tipdoc, 
         nserie LIKE fac_mtransac.nserie,
         correl INTEGER, 
         conteo SMALLINT 
        END RECORD

 -- Capturando errores
 WHENEVER ERROR CONTINUE
 WHILE TRUE
  -- Seleccionando el numero maximo
  SELECT MAX(i.numdoc)
   INTO  wr.correl
   FROM  fac_mtransac i
   WHERE (i.codemp = wr.codemp)
     AND (i.tipdoc = wr.tipdoc)
     AND (i.nserie = wr.nserie)
     AND (i.numdoc IS NOT NULL)
   IF wr.correl IS NULL THEN
      LET wr.correl = 1
   ELSE
      LET wr.correl = (wr.correl+1)
   END IF

  -- Verificando que correlativo maximo no exista
  SELECT COUNT(*)
   INTO  wr.conteo
   FROM  fac_mtransac
   WHERE (i.codemp = wr.codemp)
     AND (i.tipdoc = wr.tipdoc)
     AND (i.nserie = wr.nserie)
     AND (i.numdoc = wr.correl)
   IF (wr.conteo>0) THEN
      CONTINUE WHILE
   END IF
   EXIT WHILE
 END WHILE

 -- Retornando captura de errores
 WHENEVER ERROR STOP

 -- Retornando correlativo
 RETURN wr.correl
END FUNCTION

-- Subrutina que carga el combobox de los vendedores. Vendedores activos unicamente

FUNCTION librut003_cbxvendedores()
 -- Llenando combobox
 LET qrytext = "SELECT a.codven,a.nomven ",
               " FROM glb_vendedrs a ",
               " WHERE a.estado = 1 ",
               " ORDER BY 2 "

 CALL librut002_combobox("codven",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los clientes de una determinada lista de
-- precios

FUNCTION librut003_CbxClientesLista(xnumlis)
 DEFINE xnumlis INTEGER

 -- Llenando combobox
 LET qrytext = "SELECT a.codcli,TRIM(a.nomcli)||' ('||a.codcli||')' ",
               " FROM fac_clientes a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codcli",qrytext)
END FUNCTION

-- Subrutna para verificar si hay ofertas disponibles

FUNCTION librut003_ofertas(xnumpos,xfecha) 
 DEFINE w_mae_ofe  RECORD LIKE fac_mofertas.*,
        xnumpos    LIKE fac_mofertas.numpos,
        xfecha     DATE

 -- Verificando ofertas
 INITIALIZE w_mae_ofe.* TO NULL 
 SELECT a.*
  INTO  w_mae_ofe.*
  FROM  fac_mofertas a
  WHERE a.numpos = xnumpos
    AND xfecha BETWEEN a.fecini AND a.fecfin
    AND a.estado = 1
  IF status=NOTFOUND THEN
     RETURN FALSE,w_mae_ofe.*
  ELSE
     RETURN TRUE,w_mae_ofe.*
  END IF 
END FUNCTION 

#########################################################################
## Function  : combo_din2
##
## Parameters: inputParameters
##
## Returnings: returnParameters
##
## Comments  : Funcion que genera un combo dinamico
#########################################################################
function combo_din2(vl_2_campo, vl_2_query)
define		vl_2_campo		  string
define		vl_2_query		  string
define		vl_2_lista		  char(500)
define		vl_2_lista2	  char(500)
define		vl_2_metodo	  string
define		vl_2_valor		  string
define		vl_2_valor2	  string
define		vl_2_combo      ui.ComboBox
define		vl_2_lista_nodb base.StringTokenizer

	let vl_2_campo = vl_2_campo.trim()
	let vl_2_query = vl_2_query.trim()

	#-- Obtiene el nodo del metodo Combo de la clase ui
	let vl_2_combo = ui.ComboBox.forName(vl_2_campo)
	if vl_2_combo is null
	then
		return
	end if

	#-- Limpia los registros anteriores del combo
	call vl_2_combo.clear()

	#-- Procesa segun el metodo convirtiendo a mayusculas para mejor tratamiento
	let vl_2_metodo = vl_2_query.toUpperCase()

	if vl_2_metodo matches "SELECT*"   then	
		#--Prepara el query
		prepare q_comboList2 from vl_2_query
		declare c_comboList2 cursor for q_comboList2

		#-- Obtiene la lista de valores de la tabla segun el query enviado
		foreach c_comboList2 into vl_2_lista, vl_2_lista2
			let vl_2_valor = vl_2_lista clipped
			let vl_2_valor2 = vl_2_lista2 clipped
			call vl_2_combo.addItem(vl_2_valor, vl_2_valor2)
		end foreach	
	else
	   #-- Obtiene la lista de valores que no son de base de datos
		let vl_2_lista_nodb = base.StringTokenizer.create(vl_2_query, "|")
		while vl_2_lista_nodb.hasMoreTokens()
         let vl_2_valor = vl_2_lista_nodb.nextToken()
         call vl_2_combo.addItem(vl_2_valor, vl_2_valor)
		end while
	end if
end function

