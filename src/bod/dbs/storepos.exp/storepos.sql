{ DATABASE storepos  delimiter | }

grant dba to "sistemas";
grant connect to "public";











{ TABLE "sistemas".inv_tipomovs row size = 92 number of columns = 11 index size = 0 }

{ unload file name = inv_t00100.unl number of rows = 7 }

create table "sistemas".inv_tipomovs 
  (
    tipmov smallint not null ,
    nommov varchar(50,1) not null ,
    tipope smallint not null ,
    nomabr char(6) not null ,
    hayval smallint not null ,
    hayres smallint not null ,
    coraut smallint,
    codgru smallint,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_tipomovs from "public" as "sistemas";

{ TABLE "sistemas".glb_programs row size = 275 number of columns = 9 index size = 20 }

{ unload file name = glb_p00101.unl number of rows = 34 }

create table "sistemas".glb_programs 
  (
    codpro char(15) not null ,
    nompro varchar(100,1) not null ,
    dcrpro varchar(100,1),
    codmod smallint,
    ordpro smallint,
    actpro varchar(30,1),
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codpro)  constraint "sistemas".pkglbprograms
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_programs from "public" as "sistemas";

{ TABLE "sistemas".inv_unimedid row size = 80 number of columns = 6 index size = 7 }

{ unload file name = inv_u00102.unl number of rows = 9 }

create table "sistemas".inv_unimedid 
  (
    unimed smallint not null ,
    nommed varchar(50,1) not null ,
    nomabr char(4) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (unimed)  constraint "sistemas".pkinvunimedid
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_unimedid from "public" as "sistemas";

{ TABLE "sistemas".inv_permxbod row size = 40 number of columns = 5 index size = 0 }

{ unload file name = inv_p00103.unl number of rows = 8 }

create table "sistemas".inv_permxbod 
  (
    codbod smallint not null ,
    userid char(15) not null ,
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_permxbod from "public" as "sistemas";

{ TABLE "sistemas".inv_permxtmv row size = 40 number of columns = 5 index size = 0 }

{ unload file name = inv_p00104.unl number of rows = 32 }

create table "sistemas".inv_permxtmv 
  (
    tipmov smallint not null ,
    userid char(15) not null ,
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_permxtmv from "public" as "sistemas";

{ TABLE "sistemas".inv_mtransac row size = 590 number of columns = 28 index size = 21 }

{ unload file name = inv_m00105.unl number of rows = 21517 }

create table "sistemas".inv_mtransac 
  (
    lnktra serial not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    tipmov smallint not null ,
    codori integer not null ,
    nomori varchar(50,1),
    coddes integer not null ,
    nomdes varchar(50,1),
    numrf1 varchar(20),
    numrf2 varchar(20),
    numord char(15),
    aniotr smallint not null ,
    mestra smallint not null ,
    fecemi date not null ,
    tipope smallint not null ,
    observ varchar(255),
    estado char(1) not null ,
    lnkcon integer,
    conaut smallint,
    motanl varchar(100),
    fecanl date,
    horanl datetime hour to second,
    usranl char(10),
    impres char(1) not null ,
    userid char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnktra)  constraint "sistemas".pkinvmtransac
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_mtransac from "public" as "sistemas";

{ TABLE "sistemas".inv_dtransac row size = 205 number of columns = 31 index size = 38 }

{ unload file name = inv_d00106.unl number of rows = 45376 }

create table "sistemas".inv_dtransac 
  (
    lnktra integer not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    tipmov smallint not null ,
    codori integer not null ,
    coddes integer not null ,
    fecemi date not null ,
    aniotr smallint not null ,
    mestra smallint not null ,
    cditem integer not null ,
    codabr varchar(20,1) not null ,
    nserie varchar(30,1),
    codepq smallint not null ,
    canepq decimal(14,2) not null ,
    canuni decimal(14,2) not null ,
    preuni decimal(14,6) not null ,
    totpro decimal(14,2) not null ,
    prepro decimal(14,2) not null ,
    correl smallint not null ,
    barcod varchar(20),
    tipope smallint not null ,
    estado char(1) not null ,
    opeval decimal(14,2) not null ,
    opeuni decimal(14,2) not null ,
    actexi smallint not null ,
    porisv decimal(5,2),
    totisv decimal(12,2) not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_dtransac from "public" as "sistemas";

{ TABLE "sistemas".inv_saldopro row size = 86 number of columns = 15 index size = 30 }

{ unload file name = inv_s00107.unl number of rows = 0 }

create table "sistemas".inv_saldopro 
  (
    lnksal serial not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    tipsld smallint not null ,
    aniotr smallint not null ,
    mestra smallint not null ,
    cditem integer not null ,
    codabr varchar(20,1) not null ,
    canuni decimal(16,2) not null ,
    prepro decimal(16,6) not null ,
    totpro decimal(16,2) not null ,
    userid char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (codemp,codsuc,codbod,tipsld,aniotr,mestra,cditem)  constraint "sistemas".uqinvsaldopro,
    primary key (lnksal)  constraint "sistemas".pkinvsaldopro
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_saldopro from "public" as "sistemas";

{ TABLE "sistemas".inv_empaques row size = 66 number of columns = 7 index size = 7 }

{ unload file name = inv_e00108.unl number of rows = 0 }

create table "sistemas".inv_empaques 
  (
    codepq smallint not null ,
    nomepq varchar(30) not null ,
    cantid decimal(12,6) not null ,
    unimed smallint not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (cantid > 0. ) constraint "sistemas".ckinvempaques1,
    primary key (codepq)  constraint "sistemas".pkiviempaques
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_empaques from "public" as "sistemas";

{ TABLE "sistemas".inv_epqsxpro row size = 82 number of columns = 8 index size = 29 }

{ unload file name = inv_e00109.unl number of rows = 1754 }

create table "sistemas".inv_epqsxpro 
  (
    lnkepq serial not null ,
    cditem integer not null ,
    codepq smallint not null ,
    nomepq varchar(40,1) not null ,
    cantid decimal(12,6) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (cditem,codepq)  constraint "sistemas".uqinvepqsxpro,
    primary key (lnkepq)  constraint "sistemas".pkinvepqsxpro
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_epqsxpro from "public" as "sistemas";

{ TABLE "sistemas".glb_sucsxemp row size = 244 number of columns = 11 index size = 14 }

{ unload file name = glb_s00110.unl number of rows = 1 }

create table "sistemas".glb_sucsxemp 
  (
    codsuc smallint not null ,
    codemp smallint not null ,
    nomsuc varchar(50,1) not null ,
    nomabr varchar(16,1) not null ,
    numnit varchar(15,1) not null ,
    numtel varchar(15,1),
    numfax varchar(15,1),
    dirsuc varchar(100,1) not null ,
    userid char(15) not null ,
    fecsis date,
    horsis datetime hour to second,
    primary key (codsuc)  constraint "sistemas".pkglbsucsxemp
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_sucsxemp from "public" as "sistemas";

{ TABLE "sistemas".inv_mbodegas row size = 97 number of columns = 10 index size = 21 }

{ unload file name = inv_m00111.unl number of rows = 1 }

create table "sistemas".inv_mbodegas 
  (
    codbod smallint not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    nombod varchar(50,1) not null ,
    nomabr varchar(12,1),
    chkexi smallint not null ,
    hayfac smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (chkexi IN (1 ,0 )) constraint "sistemas".ckinvmbodegas1,
    primary key (codbod)  constraint "sistemas".pkinvmbodegas
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_mbodegas from "public" as "sistemas";

{ TABLE "sistemas".inv_proenbod row size = 103 number of columns = 15 index size = 15 }

{ unload file name = inv_p00112.unl number of rows = 1600 }

create table "sistemas".inv_proenbod 
  (
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    cditem integer not null ,
    codabr varchar(20) not null ,
    eximin decimal(14,2) not null ,
    eximax decimal(14,2) not null ,
    fulent date,
    fulsal date,
    exican decimal(14,2) not null ,
    exival decimal(14,2) not null ,
    cospro decimal(14,6) not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (eximin >= 0. ) constraint "sistemas".ckinvproenbod1,
    
    check (eximax >= 0. ) constraint "sistemas".ckinvproenbod2,
    primary key (codemp,codsuc,codbod,cditem)  constraint "sistemas".pkinvproenbod
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_proenbod from "public" as "sistemas";

{ TABLE "sistemas".inv_tofisico row size = 131 number of columns = 20 index size = 23 }

{ unload file name = inv_t00113.unl number of rows = 0 }

create table "sistemas".inv_tofisico 
  (
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    aniotr smallint not null ,
    mestra smallint not null ,
    fecinv date not null ,
    cditem integer not null ,
    codabr varchar(20,1) not null ,
    cancon decimal(16,2) not null ,
    canepq decimal(16,2) not null ,
    totepq decimal(16,2) not null ,
    canhhd decimal(16,2) not null ,
    cansue decimal(16,2) not null ,
    canuni decimal(16,2) not null ,
    totpro decimal(16,2) not null ,
    canexi decimal(16,2) not null ,
    finmes smallint not null ,
    userid char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (finmes IN (1 ,0 )) constraint "sistemas".ckinvtofisico6,
    
    check (fecinv >= DATE ('01/01/2010' ) ) constraint "sistemas".ckinvtofisico7,
    primary key (codemp,codsuc,codbod,aniotr,mestra,fecinv,cditem)  constraint "sistemas".pkinvtofisico
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_tofisico from "public" as "sistemas";

{ TABLE "sistemas".glb_subcateg row size = 78 number of columns = 6 index size = 9 }

{ unload file name = glb_s00114.unl number of rows = 42 }

create table "sistemas".glb_subcateg 
  (
    codcat smallint not null ,
    subcat smallint not null ,
    nomsub varchar(50,1) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codcat,subcat)  constraint "sistemas".pkinvsubcateg
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_subcateg from "public" as "sistemas";

{ TABLE "sistemas".inv_provedrs row size = 701 number of columns = 12 index size = 9 }

{ unload file name = inv_p00115.unl number of rows = 5 }

create table "sistemas".inv_provedrs 
  (
    codprv integer not null ,
    nomprv varchar(100,1) not null ,
    numnit varchar(30,1) not null ,
    numtel varchar(30,1),
    dirprv varchar(100,1) not null ,
    nomcon varchar(50,1),
    bemail varchar(100,1),
    tipprv smallint not null ,
    observ varchar(255,1),
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codprv)  constraint "sistemas".pkinvprovedrs
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_provedrs from "public" as "sistemas";

{ TABLE "sistemas".glb_mtpaises row size = 67 number of columns = 5 index size = 7 }

{ unload file name = glb_m00116.unl number of rows = 2 }

create table "sistemas".glb_mtpaises 
  (
    codpai smallint not null ,
    nompai varchar(40,1) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codpai)  constraint "sistemas".pkglbmtpaises
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_mtpaises from "public" as "sistemas";

{ TABLE "sistemas".fac_emitacre row size = 78 number of columns = 6 index size = 7 }

{ unload file name = fac_e00117.unl number of rows = 0 }

create table "sistemas".fac_emitacre 
  (
    codemi smallint not null ,
    nomemi varchar(40) not null ,
    nomabr char(12) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codemi)  constraint "sistemas".pkfacemitacre
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_emitacre from "public" as "sistemas";

{ TABLE "sistemas".fac_tipodocs row size = 98 number of columns = 12 index size = 7 }

{ unload file name = fac_t00118.unl number of rows = 1 }

create table "sistemas".fac_tipodocs 
  (
    tipdoc smallint not null ,
    nomdoc varchar(40) not null ,
    nomabr char(6) not null ,
    exeimp smallint not null ,
    hayimp smallint not null ,
    title1 varchar(20,1) not null ,
    haycmd smallint not null ,
    hayfir smallint not null ,
    hayepl smallint not null ,
    userid char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (exeimp IN (0 ,1 )) constraint "sistemas".ckfactipodocs1,
    
    check (hayimp IN (0 ,1 )) constraint "sistemas".ckfactipodocs2,
    primary key (tipdoc)  constraint "sistemas".pkfactipodocs
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_tipodocs from "public" as "sistemas";

{ TABLE "sistemas".fac_puntovta row size = 145 number of columns = 16 index size = 7 }

{ unload file name = fac_p00119.unl number of rows = 1 }

create table "sistemas".fac_puntovta 
  (
    numpos smallint not null ,
    nompos varchar(40) not null ,
    tipfac smallint not null ,
    chkinv smallint not null ,
    propin smallint not null ,
    porpro decimal(5,2) not null ,
    impfac char(20) not null ,
    impcoc char(20) not null ,
    impcmd char(20) not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    chkexi smallint not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (chkexi IN (1 ,0 )) constraint "sistemas".ckfacpuntovta1,
    
    check (chkinv IN (1 ,0 )) constraint "sistemas".ckfacpuntovta2,
    primary key (numpos)  constraint "sistemas".pkfacpuntovta
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_puntovta from "public" as "sistemas";

{ TABLE "sistemas".fac_tdocxpos row size = 140 number of columns = 20 index size = 9 }

{ unload file name = fac_t00120.unl number of rows = 1 }

create table "sistemas".fac_tdocxpos 
  (
    lnktdc serial not null ,
    numpos smallint not null ,
    codemp smallint not null ,
    tipdoc smallint not null ,
    regfac smallint not null ,
    numcai char(40),
    fecemi date,
    nserie char(10) not null ,
    numcor integer,
    numfin integer,
    fecven date,
    haycor smallint not null ,
    csmepl smallint,
    creepl smallint,
    regali smallint,
    nomdoc char(30) not null ,
    estado char(1) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (estado IN ('A' ,'B' )) constraint "sistemas".ckfactdocxpos1,
    primary key (lnktdc)  constraint "sistemas".pkfactdocxpos
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_tdocxpos from "public" as "sistemas";

{ TABLE "sistemas".fac_clientes row size = 424 number of columns = 12 index size = 9 }

{ unload file name = fac_c00121.unl number of rows = 2 }

create table "sistemas".fac_clientes 
  (
    codcli serial not null ,
    nomcli char(60) not null ,
    numnit char(30) not null ,
    numtel char(30),
    nmovil char(15),
    dircli char(100) not null ,
    nompro char(60),
    bemail char(100),
    estado smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (estado IN (1 ,0 )) constraint "sistemas".ckfacclientes1,
    primary key (codcli)  constraint "sistemas".pkfacclientes
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_clientes from "public" as "sistemas";

{ TABLE "sistemas".fac_tarjetas row size = 53 number of columns = 6 index size = 9 }

{ unload file name = fac_t00122.unl number of rows = 0 }

create table "sistemas".fac_tarjetas 
  (
    lnktra integer not null ,
    correl smallint not null ,
    codemi smallint not null ,
    numtar char(20),
    numaut char(20),
    totval decimal(8,2) not null ,
    
    check (correl > 0 ) constraint "sistemas".ckfactarjetas1
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_tarjetas from "public" as "sistemas";

{ TABLE "sistemas".fac_usuaxpos row size = 59 number of columns = 7 index size = 7 }

{ unload file name = fac_u00123.unl number of rows = 3 }

create table "sistemas".fac_usuaxpos 
  (
    numpos smallint not null ,
    userid varchar(15) not null ,
    tipusu smallint,
    passwd varchar(15) not null ,
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_usuaxpos from "public" as "sistemas";

{ TABLE "sistemas".fac_tipomovs row size = 69 number of columns = 6 index size = 7 }

{ unload file name = fac_t00124.unl number of rows = 0 }

create table "sistemas".fac_tipomovs 
  (
    tipmov smallint not null ,
    nommov varchar(40,1) not null ,
    tipope smallint not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (tipmov)  constraint "sistemas".pkfactipomovs
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_tipomovs from "public" as "sistemas";

{ TABLE "sistemas".glb_categors row size = 56 number of columns = 5 index size = 7 }

{ unload file name = glb_c00125.unl number of rows = 22 }

create table "sistemas".glb_categors 
  (
    codcat smallint not null ,
    nomcat varchar(30,1) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codcat)  constraint "sistemas".pkinvcategpro
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_categors from "public" as "sistemas";

{ TABLE "sistemas".glb_ciudades row size = 69 number of columns = 6 index size = 7 }

{ unload file name = glb_c00126.unl number of rows = 1 }

create table "sistemas".glb_ciudades 
  (
    ciudad smallint not null ,
    codpai smallint not null ,
    nomciu varchar(40,1) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (ciudad)  constraint "sistemas".pkglbciudades
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_ciudades from "public" as "sistemas";

{ TABLE "sistemas".glb_paramtrs row size = 349 number of columns = 7 index size = 0 }

{ unload file name = glb_p00127.unl number of rows = 29 }

create table "sistemas".glb_paramtrs 
  (
    numpar integer not null ,
    tippar integer not null ,
    nompar varchar(60,1),
    valchr varchar(255,1) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_paramtrs from "public" as "sistemas";

{ TABLE "sistemas".glb_empresas row size = 222 number of columns = 9 index size = 7 }

{ unload file name = glb_e00128.unl number of rows = 1 }

create table "sistemas".glb_empresas 
  (
    codemp smallint not null ,
    nomemp varchar(50,1) not null ,
    nomabr varchar(12,1) not null ,
    numnit varchar(15,1) not null ,
    numtel varchar(15,1),
    diremp varchar(100,1) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codemp)  constraint "sistemas".pkglbemrpesas
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_empresas from "public" as "sistemas";

{ TABLE "sistemas".glb_rolesusr row size = 79 number of columns = 5 index size = 9 }

{ unload file name = glb_r00129.unl number of rows = 3 }

create table "sistemas".glb_rolesusr 
  (
    roleid integer not null ,
    nmrole varchar(50,1) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (roleid)  constraint "sistemas".pkglbrolesusr
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_rolesusr from "public" as "sistemas";

{ TABLE "sistemas".glb_permxrol row size = 51 number of columns = 8 index size = 20 }

{ unload file name = glb_p00130.unl number of rows = 165 }

create table "sistemas".glb_permxrol 
  (
    lnkacc serial not null ,
    roleid smallint not null ,
    lnkpro integer not null ,
    codpro char(15) not null ,
    progid smallint not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (roleid,lnkpro)  constraint "sistemas".uqglbpermrol,
    primary key (lnkacc)  constraint "sistemas".pkglbpermxrol
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_permxrol from "public" as "sistemas";

{ TABLE "sistemas".glb_permxusr row size = 95 number of columns = 13 index size = 35 }

{ unload file name = glb_p00131.unl number of rows = 308 }

create table "sistemas".glb_permxusr 
  (
    lnkper serial not null ,
    roleid smallint not null ,
    lnkpro integer not null ,
    userid char(15) not null ,
    codpro char(15) not null ,
    progid smallint not null ,
    activo smallint not null ,
    passwd char(20),
    fecini date,
    fecfin date,
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (roleid,lnkpro,userid)  constraint "sistemas".uqglbpermxusr,
    primary key (lnkper)  constraint "sistemas".pkglbpermxusr
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_permxusr from "public" as "sistemas";

{ TABLE "sistemas".glb_usuarios row size = 146 number of columns = 8 index size = 21 }

{ unload file name = glb_u00132.unl number of rows = 4 }

create table "sistemas".glb_usuarios 
  (
    userid varchar(15,1) not null ,
    nomusr varchar(50,1) not null ,
    email1 varchar(50,1),
    tipusu smallint not null ,
    roleid smallint not null ,
    usuaid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (userid)  constraint "sistemas".pkglbusuarios
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_usuarios from "public" as "sistemas";

{ TABLE "sistemas".glb_dprogram row size = 106 number of columns = 10 index size = 42 }

{ unload file name = glb_d00133.unl number of rows = 160 }

create table "sistemas".glb_dprogram 
  (
    lnkpro serial not null ,
    codpro char(15) not null ,
    progid smallint not null ,
    codopc varchar(15,1),
    nomopc varchar(40,1),
    ordopc smallint,
    passwd smallint,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (codpro,progid)  constraint "sistemas".uqglbdprogram
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_dprogram from "public" as "sistemas";

{ TABLE "sistemas".inv_marcapro row size = 76 number of columns = 5 index size = 7 }

{ unload file name = inv_m00134.unl number of rows = 1 }

create table "sistemas".inv_marcapro 
  (
    codmar smallint not null ,
    nommar varchar(50,1) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codmar)  constraint "sistemas".pkinvmarcapro
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_marcapro from "public" as "sistemas";

{ TABLE "sistemas".fac_formaimp row size = 1644 number of columns = 20 index size = 9 }

{ unload file name = fac_f00135.unl number of rows = 1 }

create table "sistemas".fac_formaimp 
  (
    lnktdc integer not null ,
    top001 varchar(100,1),
    top002 varchar(100,1),
    top003 varchar(100,1),
    top004 varchar(100,1),
    top005 varchar(100,1),
    top006 varchar(100,1),
    foot01 varchar(100,1),
    foot02 varchar(100,1),
    foot03 varchar(100,1),
    foot04 varchar(100,1),
    foot05 varchar(100,1),
    foot06 varchar(100,1),
    foot07 varchar(100,1),
    foot08 varchar(100,1),
    foot09 varchar(100,1),
    foot10 varchar(100,1),
    userid varchar(15,1),
    fecsis date,
    horsis datetime hour to second
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_formaimp from "public" as "sistemas";

{ TABLE "sistemas".inv_products row size = 486 number of columns = 42 index size = 32 }

{ unload file name = inv_p00136.unl number of rows = 1754 }

create table "sistemas".inv_products 
  (
    cditem serial not null ,
    codcat smallint not null ,
    subcat smallint not null ,
    codprv integer not null ,
    codmar smallint not null ,
    unimed smallint not null ,
    dsitem varchar(100,1) not null ,
    descto smallint not null ,
    rebaja smallint not null ,
    chkexi smallint not null ,
    codabr varchar(20,1) not null ,
    diavig smallint not null ,
    tipdes smallint not null ,
    pesado smallint not null ,
    ixpeso smallint not null ,
    cxpeso decimal(14,2) not null ,
    canepq decimal(14,2) not null ,
    essubp smallint not null ,
    tipepq smallint not null ,
    premin decimal(12,2) not null ,
    presug decimal(12,2) not null ,
    prereb decimal(12,2) not null ,
    pulcom decimal(12,2) not null ,
    canva1 decimal(7,2) not null ,
    canva2 decimal(7,2) not null ,
    canva3 decimal(7,2) not null ,
    preva1 decimal(12,2) not null ,
    preva2 decimal(12,2) not null ,
    preva3 decimal(12,2) not null ,
    porisv decimal(5,2) not null ,
    estado smallint not null ,
    status smallint not null ,
    exiact decimal(12,2) not null ,
    fulcam date,
    urlimg varchar(100,1),
    despro varchar(100,1) not null ,
    haypre smallint not null ,
    esserv smallint not null ,
    escomb smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (premin >= 0. ) constraint "sistemas".ckinvproducts1,
    
    check (estado IN (1 ,0 )) constraint "sistemas".ckinvproducts2,
    
    check (status IN (1 ,0 )) constraint "sistemas".ckinvproducts3,
    primary key (cditem)  constraint "sistemas".pkinvproducts
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_products from "public" as "sistemas";

{ TABLE "sistemas".inv_grupomov row size = 61 number of columns = 7 index size = 7 }

{ unload file name = inv_g00137.unl number of rows = 5 }

create table "sistemas".inv_grupomov 
  (
    codgru smallint not null ,
    nomgru varchar(30,1) not null ,
    tipope smallint not null ,
    numord smallint not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codgru)  constraint "sistemas".pkinvgrupomov
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_grupomov from "public" as "sistemas";

{ TABLE "sistemas".inv_destipro row size = 56 number of columns = 5 index size = 7 }

{ unload file name = inv_d00138.unl number of rows = 1 }

create table "sistemas".inv_destipro 
  (
    tipdes smallint not null ,
    nomdes varchar(30,1) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (tipdes)  constraint "sistemas".fkinvdestipro
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_destipro from "public" as "sistemas";

{ TABLE "sistemas".glb_pesotara row size = 61 number of columns = 6 index size = 7 }

{ unload file name = glb_p00139.unl number of rows = 0 }

create table "sistemas".glb_pesotara 
  (
    codtar smallint not null ,
    destar varchar(30) not null ,
    canpes decimal(5,2) not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codtar)  constraint "sistemas".pkglbpesotara
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_pesotara from "public" as "sistemas";

{ TABLE "sistemas".inv_ncorretq row size = 4 number of columns = 1 index size = 9 }

{ unload file name = inv_n00140.unl number of rows = 0 }

create table "sistemas".inv_ncorretq 
  (
    numcor serial not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_ncorretq from "public" as "sistemas";

{ TABLE "sistemas".inv_dproduct row size = 63 number of columns = 8 index size = 18 }

{ unload file name = inv_d00141.unl number of rows = 0 }

create table "sistemas".inv_dproduct 
  (
    cditem integer not null ,
    citems integer not null ,
    codabr varchar(20,1) not null ,
    cantid decimal(14,8) not null ,
    correl smallint not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_dproduct from "public" as "sistemas";

{ TABLE "sistemas".inv_scfisico row size = 120 number of columns = 19 index size = 9 }

{ unload file name = inv_s00142.unl number of rows = 0 }

create table "sistemas".inv_scfisico 
  (
    lnkreg serial not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    aniotr smallint not null ,
    mestra smallint not null ,
    fecinv date not null ,
    cditem integer not null ,
    codabr varchar(20,1) not null ,
    codepq smallint not null ,
    canepq decimal(16,2) not null ,
    canuni decimal(16,2) not null ,
    canexi decimal(16,2) not null ,
    finmes smallint not null ,
    numrck varchar(10,1),
    nufico varchar(10,1),
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnkreg)  constraint "sistemas".pkinvscfisico
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_scfisico from "public" as "sistemas";

{ TABLE "sistemas".fac_dtdoctos row size = 171 number of columns = 8 index size = 0 }

{ unload file name = fac_d00143.unl number of rows = 0 }

create table "sistemas".fac_dtdoctos 
  (
    lnktra integer not null ,
    correl smallint not null ,
    codbco smallint not null ,
    numdoc varchar(50) not null ,
    totval decimal(8,2) not null ,
    prefec smallint not null ,
    feccob date,
    observ varchar(100,1),
    
    check (correl > 0 ) constraint "sistemas".ckfacdtdoctos1,
    
    check (totval > 0. ) constraint "sistemas".ckfacdtdoctos2
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_dtdoctos from "public" as "sistemas";

{ TABLE "sistemas".fac_permxpos row size = 40 number of columns = 5 index size = 0 }

{ unload file name = fac_p00144.unl number of rows = 0 }

create table "sistemas".fac_permxpos 
  (
    numpos smallint not null ,
    userid char(15) not null ,
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_permxpos from "public" as "sistemas";

{ TABLE "sistemas".fac_permxtdc row size = 43 number of columns = 5 index size = 9 }

{ unload file name = fac_p00145.unl number of rows = 0 }

create table "sistemas".fac_permxtdc 
  (
    lnktdc integer not null ,
    userid varchar(15) not null ,
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_permxtdc from "public" as "sistemas";

{ TABLE "sistemas".glb_gruprovs row size = 59 number of columns = 6 index size = 7 }

{ unload file name = glb_g00146.unl number of rows = 2 }

create table "sistemas".glb_gruprovs 
  (
    codgru smallint not null ,
    nomgru varchar(30,1) not null ,
    afecmp smallint not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codgru)  constraint "sistemas".pkglbgruprovs
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_gruprovs from "public" as "sistemas";

{ TABLE "sistemas".sre_divrecet row size = 165 number of columns = 10 index size = 7 }

{ unload file name = sre_d00147.unl number of rows = 12 }

create table "sistemas".sre_divrecet 
  (
    coddiv smallint not null ,
    nomdiv varchar(30,1) not null ,
    codabr char(1),
    norden smallint not null ,
    imagen varchar(100,1),
    enmenu smallint not null ,
    estado smallint not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (enmenu IN (0 ,1 )) constraint "sistemas".cksredivrecet1,
    
    check (estado IN (0 ,1 )) constraint "sistemas".cksredivrecet2,
    primary key (coddiv)  constraint "sistemas".pksredivrecet
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".sre_divrecet from "public" as "sistemas";

{ TABLE "sistemas".sre_mrecetas row size = 218 number of columns = 16 index size = 9 }

{ unload file name = sre_m00148.unl number of rows = 0 }

create table "sistemas".sre_mrecetas 
  (
    lnkrec serial not null ,
    numrec char(10) not null ,
    coddiv smallint not null ,
    nomrec varchar(50,1) not null ,
    cospre decimal(9,2) not null ,
    prevta decimal(9,2) not null ,
    porisv decimal(5,2) not null ,
    numpos smallint not null ,
    enmenu smallint not null ,
    imagen varchar(100,1),
    combeb smallint not null ,
    bebref smallint not null ,
    receta smallint not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnkrec)  constraint "sistemas".pksremrecetas
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".sre_mrecetas from "public" as "sistemas";

{ TABLE "sistemas".sre_drecetas row size = 46 number of columns = 8 index size = 9 }

{ unload file name = sre_d00149.unl number of rows = 0 }

create table "sistemas".sre_drecetas 
  (
    lnkrec integer not null ,
    tingre char(1) not null ,
    cditem integer not null ,
    codabr varchar(20,1) not null ,
    unimed integer not null ,
    canuni decimal(14,6) not null ,
    correl smallint not null ,
    entrga smallint not null ,
    
    check (tingre IN ('P' ,'S' )) constraint "sistemas".chksredrecetas1,
    
    check (canuni > 0. ) constraint "sistemas".chksredrecetas2,
    
    check (correl > 0 ) constraint "sistemas".chksredrecetas3
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".sre_drecetas from "public" as "sistemas";

{ TABLE "sistemas".sre_subrecet row size = 101 number of columns = 8 index size = 9 }

{ unload file name = sre_s00150.unl number of rows = 0 }

create table "sistemas".sre_subrecet 
  (
    lnkrec serial not null ,
    numrec char(10) not null ,
    nomrec varchar(50,1) not null ,
    cospre decimal(9,2) not null ,
    numpor decimal(9,2) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (cospre >= 0. ) constraint "sistemas".chksresubrecet1,
    
    check (numpor >= 0. ) constraint "sistemas".chksresubrecet2,
    primary key (lnkrec)  constraint "sistemas".pksresubrecet
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".sre_subrecet from "public" as "sistemas";

{ TABLE "sistemas".sre_dsubrect row size = 37 number of columns = 5 index size = 9 }

{ unload file name = sre_d00151.unl number of rows = 0 }

create table "sistemas".sre_dsubrect 
  (
    lnkrec integer not null ,
    cditem integer not null ,
    codabr varchar(20,1) not null ,
    canuni decimal(9,2) not null ,
    correl smallint not null ,
    
    check (canuni > 0. ) constraint "sistemas".chksredsubrect1,
    
    check (correl > 0 ) constraint "sistemas".chksredsubrect2
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".sre_dsubrect from "public" as "sistemas";

{ TABLE "sistemas".sre_dordenes row size = 54 number of columns = 11 index size = 9 }

{ unload file name = sre_d00152.unl number of rows = 0 }

create table "sistemas".sre_dordenes 
  (
    lnkord integer not null ,
    lnkrec integer not null ,
    numrec char(10) not null ,
    cantid smallint not null ,
    prevta decimal(9,2) not null ,
    cospre decimal(9,2) not null ,
    totrec decimal(9,2) not null ,
    correl smallint not null ,
    preori decimal(9,2) not null ,
    totdes decimal(9,2) not null ,
    tipdes smallint not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".sre_dordenes from "public" as "sistemas";

{ TABLE "sistemas".sre_mordenes row size = 314 number of columns = 22 index size = 9 }

{ unload file name = sre_m00153.unl number of rows = 0 }

create table "sistemas".sre_mordenes 
  (
    lnkord serial not null ,
    numpos smallint not null ,
    numesa smallint not null ,
    numord integer not null ,
    fecemi date not null ,
    totord decimal(14,2) not null ,
    totdes decimal(9,2) not null ,
    llevar smallint,
    observ char(200),
    estado smallint not null ,
    status smallint not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    usranl varchar(15,1),
    fecanl date,
    horanl datetime hour to second,
    lnktra integer not null ,
    numant smallint,
    feccam date,
    usrcam varchar(15,1),
    horcam datetime hour to second,
    primary key (lnkord)  constraint "sistemas".pksremordenes
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".sre_mordenes from "public" as "sistemas";

{ TABLE "sistemas".pos_dtransac row size = 72 number of columns = 16 index size = 9 }

{ unload file name = pos_d00154.unl number of rows = 0 }

create table "sistemas".pos_dtransac 
  (
    lnktra integer not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    lnkrec integer not null ,
    numrec char(10) not null ,
    cantid smallint not null ,
    prevta decimal(9,2) not null ,
    cospre decimal(9,2) not null ,
    totrec decimal(14,2) not null ,
    correl smallint not null ,
    totisv decimal(9,2) not null ,
    porisv decimal(5,2) not null ,
    totdes decimal(9,2) not null ,
    preori decimal(9,2) not null ,
    tipdes smallint not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".pos_dtransac from "public" as "sistemas";

{ TABLE "sistemas".glb_mmodulos row size = 70 number of columns = 7 index size = 7 }

{ unload file name = glb_m00155.unl number of rows = 4 }

create table "sistemas".glb_mmodulos 
  (
    codmod smallint not null ,
    nommod char(40) not null ,
    numord smallint not null ,
    estado smallint not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codmod)  constraint "sistemas".pkglbmmodulos
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_mmodulos from "public" as "sistemas";

{ TABLE "sistemas".inv_pesajpro row size = 160 number of columns = 20 index size = 9 }

{ unload file name = inv_p00156.unl number of rows = 0 }

create table "sistemas".inv_pesajpro 
  (
    lnketq serial not null ,
    numetq varchar(20,1) not null ,
    cditem integer not null ,
    codabr char(20) not null ,
    fecpro date not null ,
    fecven date not null ,
    diavig smallint not null ,
    codtar smallint not null ,
    pbruto decimal(12,2) not null ,
    pstara decimal(12,2) not null ,
    canpes decimal(12,2) not null ,
    unipes varchar(10,1) not null ,
    ixpeso smallint not null ,
    cxpeso decimal(14,2) not null ,
    canepq decimal(14,2) not null ,
    cditma integer,
    codaba varchar(20,1),
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnketq)  constraint "sistemas".pkinvpesajpro
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".inv_pesajpro from "public" as "sistemas";

{ TABLE "sistemas".fac_vicortes row size = 444 number of columns = 59 index size = 9 }

{ unload file name = fac_v00158.unl number of rows = 194 }

create table "sistemas".fac_vicortes 
  (
    lnkcor serial not null ,
    numpos smallint not null ,
    feccor date not null ,
    tipcor smallint not null ,
    observ char(200),
    totvta decimal(12,2) not null ,
    fonini decimal(12,2) not null ,
    totgas decimal(12,2) not null ,
    totcor decimal(12,2) not null ,
    totefe decimal(12,2) not null ,
    tottar decimal(12,2) not null ,
    totcon decimal(12,2) not null ,
    totcre decimal(12,2) not null ,
    totcos decimal(12,2) not null ,
    totdon decimal(12,2) not null ,
    totisv decimal(12,2) not null ,
    totdes decimal(16) not null ,
    bil500 smallint not null ,
    bil200 smallint not null ,
    bil100 smallint not null ,
    bil050 smallint not null ,
    bil020 smallint not null ,
    bil010 smallint not null ,
    bil005 smallint not null ,
    bil002 smallint not null ,
    bil001 smallint not null ,
    mon001 smallint not null ,
    mon050 smallint not null ,
    mon025 smallint not null ,
    mon020 smallint not null ,
    mon010 smallint not null ,
    mon005 smallint not null ,
    toecie decimal(12,2) not null ,
    cbi500 smallint not null ,
    cbi200 smallint not null ,
    cbi100 smallint not null ,
    cbi050 smallint not null ,
    cbi020 smallint not null ,
    cbi010 smallint not null ,
    cbi005 smallint not null ,
    cbi002 smallint not null ,
    cbi001 smallint not null ,
    cmo001 smallint not null ,
    cmo050 smallint not null ,
    cmo020 smallint not null ,
    cmo025 smallint not null ,
    cmo010 smallint not null ,
    cmo005 smallint not null ,
    toecor decimal(12,2) not null ,
    totdif decimal(12,2) not null ,
    ttacie decimal(12,2) not null ,
    todita decimal(12,2) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    cierre smallint not null ,
    usrcie varchar(15,1),
    feccie date,
    horcie datetime hour to second,
    
    check (feccor >= DATE ('01/06/2016' ) ) constraint "sistemas".ckfacvicortes1,
    primary key (lnkcor)  constraint "sistemas".pkfacvicortes
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_vicortes from "public" as "sistemas";

{ TABLE "sistemas".sre_dplaspre row size = 22 number of columns = 5 index size = 9 }

{ unload file name = sre_d00159.unl number of rows = 0 }

create table "sistemas".sre_dplaspre 
  (
    lnkord integer not null ,
    lnkrec integer not null ,
    numrec char(10) not null ,
    cantid smallint not null ,
    correl smallint not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".sre_dplaspre from "public" as "sistemas";

{ TABLE "sistemas".glb_userauth row size = 78 number of columns = 6 index size = 7 }

{ unload file name = glb_u00160.unl number of rows = 1 }

create table "sistemas".glb_userauth 
  (
    codaut smallint not null ,
    nomaut varchar(50,1) not null ,
    estado smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codaut)  constraint "sistemas".pkglbuserauth
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".glb_userauth from "public" as "sistemas";

{ TABLE "sistemas".pla_empleads row size = 70 number of columns = 6 index size = 9 }

{ unload file name = pla_e00161.unl number of rows = 0 }

create table "sistemas".pla_empleads 
  (
    codepl integer,
    nomemp char(40) not null ,
    estado smallint not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codepl)  constraint "sistemas".pkplaempleads
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".pla_empleads from "public" as "sistemas";

{ TABLE "sistemas".pos_mtransac row size = 606 number of columns = 47 index size = 32 }

{ unload file name = pos_m00162.unl number of rows = 0 }

create table "sistemas".pos_mtransac 
  (
    lnktra serial not null ,
    numpos smallint not null ,
    codemp smallint not null ,
    lnktdc integer not null ,
    tipdoc smallint not null ,
    nserie char(10) not null ,
    numdoc integer not null ,
    fecemi date not null ,
    numesa smallint not null ,
    codcli integer not null ,
    numnit char(20),
    nomcli char(60),
    moneda smallint not null ,
    efecti decimal(14,2) not null ,
    cheque decimal(14,2) not null ,
    tarcre decimal(14,2) not null ,
    totdoc decimal(14,2) not null ,
    totpag decimal(14,2) not null ,
    totisv decimal(14,2) not null ,
    totgra decimal(14,2) not null ,
    totexe decimal(14,2) not null ,
    totdes decimal(14,2) not null ,
    pordes decimal(5,2) not null ,
    llevar smallint,
    descrp char(200),
    estado smallint not null ,
    feccor date not null ,
    haycor smallint not null ,
    credit smallint not null ,
    csmepl smallint not null ,
    regali smallint not null ,
    codepl smallint,
    usrope varchar(15) not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    motanl varchar(100,1),
    usranl varchar(15),
    fecanl date,
    horanl datetime hour to second,
    propin decimal(7,2) not null ,
    porpro decimal(5,2) not null ,
    totcta decimal(14,2) not null ,
    lnkinv integer,
    trazbd smallint,
    coddlv smallint,
    codela smallint,
    unique (numpos,tipdoc,nserie,numdoc)  constraint "sistemas".uqposmtransac,
    
    check (estado IN (1 ,0 )) constraint "sistemas".ckposmtransac1,
    
    check (numdoc > 0 ) constraint "sistemas".ckposmtransac2,
    
    check (fecemi > DATE ('01/01/2016' ) ) constraint "sistemas".ckposmtransac3,
    
    check (efecti >= 0. ) constraint "sistemas".ckposmtransac4,
    
    check (cheque >= 0. ) constraint "sistemas".ckposmtransac5,
    
    check (tarcre >= 0. ) constraint "sistemas".ckposmtransac6,
    
    check (totdoc >= 0. ) constraint "sistemas".ckposmtransac7,
    
    check (totpag >= 0. ) constraint "sistemas".ckposmtransac8,
    
    check (totisv >= 0. ) constraint "sistemas".ckposmtransac9,
    primary key (lnktra)  constraint "sistemas".pkposmtransac
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".pos_mtransac from "public" as "sistemas";

{ TABLE "sistemas".glb_delivery row size = 58 number of columns = 6 index size = 7 }

{ unload file name = glb_d00163.unl number of rows = 4 }

create table "sistemas".glb_delivery 
  (
    coddlv smallint not null ,
    nomdlv varchar(30,1) not null ,
    estado smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (coddlv)  constraint "sistemas".pkglbdelivery
  ) extent size 16 next size 16 lock mode page;

revoke all on "sistemas".glb_delivery from "public" as "sistemas";

{ TABLE "sistemas".glb_elabodrs row size = 58 number of columns = 6 index size = 7 }

{ unload file name = glb_e00164.unl number of rows = 5 }

create table "sistemas".glb_elabodrs 
  (
    codela smallint not null ,
    nomela varchar(30,1) not null ,
    estado smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codela)  constraint "sistemas".pkglbelabords
  ) extent size 16 next size 16 lock mode page;

revoke all on "sistemas".glb_elabodrs from "public" as "sistemas";

{ TABLE "sistemas".fac_dtransac row size = 382 number of columns = 25 index size = 18 }

{ unload file name = fac_d00165.unl number of rows = 45375 }

create table "sistemas".fac_dtransac 
  (
    lnktra integer not null ,
    correl smallint not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    cditem integer not null ,
    codabr char(20) not null ,
    dsitem varchar(250,1),
    unimed smallint not null ,
    codepq smallint not null ,
    canepq decimal(14,2) not null ,
    pbruto decimal(14,2) not null ,
    cantid decimal(14,2) not null ,
    canbon decimal(14,6) not null ,
    preuni decimal(14,6) not null ,
    preori decimal(12,2) not null ,
    descto decimal(12,2) not null ,
    pordes decimal(5,2) not null ,
    totpro decimal(14,2) not null ,
    factor decimal(14,6) not null ,
    cliesp smallint not null ,
    rebaja smallint not null ,
    pesado smallint not null ,
    porisv decimal(5,2),
    totisv decimal(12,2),
    
    check (correl > 0 ) constraint "sistemas".ckfacdtransac1,
    
    check (totpro >= 0. ) constraint "sistemas".ckfacdtransac4
  ) extent size 16 next size 16 lock mode page;

revoke all on "sistemas".fac_dtransac from "public" as "sistemas";

{ TABLE "sistemas".fac_mofertas row size = 95 number of columns = 10 index size = 9 }

{ unload file name = fac_m00166.unl number of rows = 0 }

create table "sistemas".fac_mofertas 
  (
    lnkofe serial not null ,
    numpos smallint not null ,
    desofe varchar(50,1) not null ,
    fecini date not null ,
    fecfin date not null ,
    pordes decimal(5,2) not null ,
    estado smallint not null ,
    userid varchar(15,1),
    fecsis date not null ,
    horsis datetime hour to second not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_mofertas from "public" as "sistemas";

{ TABLE "sistemas".glb_vendedrs row size = 80 number of columns = 7 index size = 7 }

{ unload file name = glb_v00167.unl number of rows = 1 }

create table "sistemas".glb_vendedrs 
  (
    codven smallint not null ,
    nomven varchar(50,1) not null ,
    estado smallint not null ,
    haycom smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codven)  constraint "sistemas".pkglbvendedrs
  ) extent size 16 next size 16 lock mode page;

revoke all on "sistemas".glb_vendedrs from "public" as "sistemas";

{ TABLE "sistemas".vta_presxcli row size = 66 number of columns = 9 index size = 0 }

{ unload file name = vta_p00168.unl number of rows = 0 }

create table "sistemas".vta_presxcli 
  (
    lnkpre serial not null ,
    codcli integer not null ,
    cditem integer not null ,
    codabr varchar(20,1),
    correl smallint not null ,
    preuni decimal(12,6) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  ) extent size 16 next size 16 lock mode page;

revoke all on "sistemas".vta_presxcli from "public" as "sistemas";

{ TABLE "sistemas".vta_preuxcan row size = 78 number of columns = 10 index size = 9 }

{ unload file name = vta_p00169.unl number of rows = 0 }

create table "sistemas".vta_preuxcan 
  (
    lnkpre serial not null ,
    cditem integer not null ,
    codabr varchar(20,1),
    correl smallint not null ,
    canini decimal(14,2) not null ,
    canfin decimal(14,2) not null ,
    preuni decimal(12,6) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  ) extent size 16 next size 16 lock mode page;

revoke all on "sistemas".vta_preuxcan from "public" as "sistemas";

{ TABLE "sistemas".fac_mtransac row size = 639 number of columns = 55 index size = 46 }

{ unload file name = fac_m00170.unl number of rows = 21516 }

create table "sistemas".fac_mtransac 
  (
    lnktra serial not null ,
    numpos smallint not null ,
    codemp smallint not null ,
    lnktdc integer not null ,
    tipdoc smallint not null ,
    nserie char(10) not null ,
    numdoc integer not null ,
    fecemi date not null ,
    codcli integer not null ,
    numnit varchar(20),
    nomcli varchar(60),
    dircli varchar(100),
    telcli varchar(30,1),
    diacre smallint,
    credit smallint not null ,
    noruta smallint not null ,
    codven smallint not null ,
    numrf1 varchar(30,1),
    ordcmp char(20) not null ,
    hayord smallint not null ,
    exenta smallint not null ,
    moneda smallint not null ,
    tascam decimal(14,6) not null ,
    efecti decimal(14,2) not null ,
    cheque decimal(14,2) not null ,
    tarcre decimal(14,2) not null ,
    ordcom decimal(14,2) not null ,
    depmon decimal(14,2) not null ,
    subtot decimal(14,2) not null ,
    totdoc decimal(14,2) not null ,
    totori decimal(14,2) not null ,
    totiva decimal(14,2) not null ,
    poriva decimal(5,2) not null ,
    totpag decimal(14,2) not null ,
    monext decimal(14,2) not null ,
    totdes decimal(14,2) not null ,
    abonos decimal(14,2) not null ,
    saldos decimal(14,2) not null ,
    totapa decimal(14,2) not null ,
    lnkdes integer not null ,
    estado char(1) not null ,
    feccor date not null ,
    usrope varchar(15) not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    motanl varchar(100,1),
    usranl varchar(15),
    fecanl date,
    horanl datetime hour to second,
    lnkinv integer not null ,
    lnkapa integer not null ,
    pordes decimal(5,2) not null ,
    numapa integer not null ,
    haycor smallint,
    
    check (estado IN ('V' ,'A' )) constraint "sistemas".ckfacmtransac1,
    
    check (poriva >= 0. ) constraint "sistemas".ckfacmtransac10,
    
    check (totiva >= 0. ) constraint "sistemas".ckfacmtransac11,
    
    check (numdoc > 0 ) constraint "sistemas".ckfacmtransac2,
    
    check (fecemi > DATE ('01/01/2011' ) ) constraint "sistemas".ckfacmtransac3,
    
    check (efecti >= 0. ) constraint "sistemas".ckfacmtransac4,
    
    check (cheque >= 0. ) constraint "sistemas".ckfacmtransac5,
    
    check (tarcre >= 0. ) constraint "sistemas".ckfacmtransac6,
    
    check (subtot >= 0. ) constraint "sistemas".ckfacmtransac7,
    
    check (totdoc >= 0. ) constraint "sistemas".ckfacmtransac8,
    primary key (lnktra)  constraint "sistemas".pkfacmtransac
  ) extent size 16 next size 16 lock mode page;

revoke all on "sistemas".fac_mtransac from "public" as "sistemas";

{ TABLE "sistemas".actpro row size = 134 number of columns = 3 index size = 0 }

{ unload file name = actpr00186.unl number of rows = 230 }

create table "sistemas".actpro 
  (
    cditem integer,
    dsitem char(100),
    codabr char(30)
  ) extent size 16 next size 16 lock mode page;

revoke all on "sistemas".actpro from "public" as "sistemas";

{ TABLE "sistemas".fac_dvicorte row size = 160 number of columns = 10 index size = 9 }

{ unload file name = fac_d00192.unl number of rows = 651 }

create table "sistemas".fac_dvicorte 
  (
    lnkcor integer not null ,
    numpos smallint not null ,
    fecemi date not null ,
    numdoc char(20) not null ,
    totval decimal(12,2) not null ,
    codaut smallint not null ,
    observ char(100),
    correl smallint not null ,
    userid char(15) not null ,
    horsis datetime hour to second not null 
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".fac_dvicorte from "public" as "sistemas";




grant select on "sistemas".inv_tipomovs to "public" as "sistemas";
grant update on "sistemas".inv_tipomovs to "public" as "sistemas";
grant insert on "sistemas".inv_tipomovs to "public" as "sistemas";
grant delete on "sistemas".inv_tipomovs to "public" as "sistemas";
grant index on "sistemas".inv_tipomovs to "public" as "sistemas";
grant select on "sistemas".glb_programs to "public" as "sistemas";
grant update on "sistemas".glb_programs to "public" as "sistemas";
grant insert on "sistemas".glb_programs to "public" as "sistemas";
grant delete on "sistemas".glb_programs to "public" as "sistemas";
grant index on "sistemas".glb_programs to "public" as "sistemas";
grant select on "sistemas".inv_unimedid to "public" as "sistemas";
grant update on "sistemas".inv_unimedid to "public" as "sistemas";
grant insert on "sistemas".inv_unimedid to "public" as "sistemas";
grant delete on "sistemas".inv_unimedid to "public" as "sistemas";
grant index on "sistemas".inv_unimedid to "public" as "sistemas";
grant select on "sistemas".inv_permxbod to "public" as "sistemas";
grant update on "sistemas".inv_permxbod to "public" as "sistemas";
grant insert on "sistemas".inv_permxbod to "public" as "sistemas";
grant delete on "sistemas".inv_permxbod to "public" as "sistemas";
grant index on "sistemas".inv_permxbod to "public" as "sistemas";
grant select on "sistemas".inv_permxtmv to "public" as "sistemas";
grant update on "sistemas".inv_permxtmv to "public" as "sistemas";
grant insert on "sistemas".inv_permxtmv to "public" as "sistemas";
grant delete on "sistemas".inv_permxtmv to "public" as "sistemas";
grant index on "sistemas".inv_permxtmv to "public" as "sistemas";
grant select on "sistemas".inv_mtransac to "public" as "sistemas";
grant update on "sistemas".inv_mtransac to "public" as "sistemas";
grant insert on "sistemas".inv_mtransac to "public" as "sistemas";
grant delete on "sistemas".inv_mtransac to "public" as "sistemas";
grant index on "sistemas".inv_mtransac to "public" as "sistemas";
grant select on "sistemas".inv_dtransac to "public" as "sistemas";
grant update on "sistemas".inv_dtransac to "public" as "sistemas";
grant insert on "sistemas".inv_dtransac to "public" as "sistemas";
grant delete on "sistemas".inv_dtransac to "public" as "sistemas";
grant index on "sistemas".inv_dtransac to "public" as "sistemas";
grant select on "sistemas".inv_saldopro to "public" as "sistemas";
grant update on "sistemas".inv_saldopro to "public" as "sistemas";
grant insert on "sistemas".inv_saldopro to "public" as "sistemas";
grant delete on "sistemas".inv_saldopro to "public" as "sistemas";
grant index on "sistemas".inv_saldopro to "public" as "sistemas";
grant select on "sistemas".inv_empaques to "public" as "sistemas";
grant update on "sistemas".inv_empaques to "public" as "sistemas";
grant insert on "sistemas".inv_empaques to "public" as "sistemas";
grant delete on "sistemas".inv_empaques to "public" as "sistemas";
grant index on "sistemas".inv_empaques to "public" as "sistemas";
grant select on "sistemas".inv_epqsxpro to "public" as "sistemas";
grant update on "sistemas".inv_epqsxpro to "public" as "sistemas";
grant insert on "sistemas".inv_epqsxpro to "public" as "sistemas";
grant delete on "sistemas".inv_epqsxpro to "public" as "sistemas";
grant index on "sistemas".inv_epqsxpro to "public" as "sistemas";
grant select on "sistemas".glb_sucsxemp to "public" as "sistemas";
grant update on "sistemas".glb_sucsxemp to "public" as "sistemas";
grant insert on "sistemas".glb_sucsxemp to "public" as "sistemas";
grant delete on "sistemas".glb_sucsxemp to "public" as "sistemas";
grant index on "sistemas".glb_sucsxemp to "public" as "sistemas";
grant select on "sistemas".inv_mbodegas to "public" as "sistemas";
grant update on "sistemas".inv_mbodegas to "public" as "sistemas";
grant insert on "sistemas".inv_mbodegas to "public" as "sistemas";
grant delete on "sistemas".inv_mbodegas to "public" as "sistemas";
grant index on "sistemas".inv_mbodegas to "public" as "sistemas";
grant select on "sistemas".inv_proenbod to "public" as "sistemas";
grant update on "sistemas".inv_proenbod to "public" as "sistemas";
grant insert on "sistemas".inv_proenbod to "public" as "sistemas";
grant delete on "sistemas".inv_proenbod to "public" as "sistemas";
grant index on "sistemas".inv_proenbod to "public" as "sistemas";
grant select on "sistemas".inv_tofisico to "public" as "sistemas";
grant update on "sistemas".inv_tofisico to "public" as "sistemas";
grant insert on "sistemas".inv_tofisico to "public" as "sistemas";
grant delete on "sistemas".inv_tofisico to "public" as "sistemas";
grant index on "sistemas".inv_tofisico to "public" as "sistemas";
grant select on "sistemas".glb_subcateg to "public" as "sistemas";
grant update on "sistemas".glb_subcateg to "public" as "sistemas";
grant insert on "sistemas".glb_subcateg to "public" as "sistemas";
grant delete on "sistemas".glb_subcateg to "public" as "sistemas";
grant index on "sistemas".glb_subcateg to "public" as "sistemas";
grant select on "sistemas".inv_provedrs to "public" as "sistemas";
grant update on "sistemas".inv_provedrs to "public" as "sistemas";
grant insert on "sistemas".inv_provedrs to "public" as "sistemas";
grant delete on "sistemas".inv_provedrs to "public" as "sistemas";
grant index on "sistemas".inv_provedrs to "public" as "sistemas";
grant select on "sistemas".glb_mtpaises to "public" as "sistemas";
grant update on "sistemas".glb_mtpaises to "public" as "sistemas";
grant insert on "sistemas".glb_mtpaises to "public" as "sistemas";
grant delete on "sistemas".glb_mtpaises to "public" as "sistemas";
grant index on "sistemas".glb_mtpaises to "public" as "sistemas";
grant select on "sistemas".fac_emitacre to "public" as "sistemas";
grant update on "sistemas".fac_emitacre to "public" as "sistemas";
grant insert on "sistemas".fac_emitacre to "public" as "sistemas";
grant delete on "sistemas".fac_emitacre to "public" as "sistemas";
grant index on "sistemas".fac_emitacre to "public" as "sistemas";
grant select on "sistemas".fac_tipodocs to "public" as "sistemas";
grant update on "sistemas".fac_tipodocs to "public" as "sistemas";
grant insert on "sistemas".fac_tipodocs to "public" as "sistemas";
grant delete on "sistemas".fac_tipodocs to "public" as "sistemas";
grant index on "sistemas".fac_tipodocs to "public" as "sistemas";
grant select on "sistemas".fac_puntovta to "public" as "sistemas";
grant update on "sistemas".fac_puntovta to "public" as "sistemas";
grant insert on "sistemas".fac_puntovta to "public" as "sistemas";
grant delete on "sistemas".fac_puntovta to "public" as "sistemas";
grant index on "sistemas".fac_puntovta to "public" as "sistemas";
grant select on "sistemas".fac_tdocxpos to "public" as "sistemas";
grant update on "sistemas".fac_tdocxpos to "public" as "sistemas";
grant insert on "sistemas".fac_tdocxpos to "public" as "sistemas";
grant delete on "sistemas".fac_tdocxpos to "public" as "sistemas";
grant index on "sistemas".fac_tdocxpos to "public" as "sistemas";
grant select on "sistemas".fac_clientes to "public" as "sistemas";
grant update on "sistemas".fac_clientes to "public" as "sistemas";
grant insert on "sistemas".fac_clientes to "public" as "sistemas";
grant delete on "sistemas".fac_clientes to "public" as "sistemas";
grant index on "sistemas".fac_clientes to "public" as "sistemas";
grant select on "sistemas".fac_tarjetas to "public" as "sistemas";
grant update on "sistemas".fac_tarjetas to "public" as "sistemas";
grant insert on "sistemas".fac_tarjetas to "public" as "sistemas";
grant delete on "sistemas".fac_tarjetas to "public" as "sistemas";
grant index on "sistemas".fac_tarjetas to "public" as "sistemas";
grant select on "sistemas".fac_usuaxpos to "public" as "sistemas";
grant update on "sistemas".fac_usuaxpos to "public" as "sistemas";
grant insert on "sistemas".fac_usuaxpos to "public" as "sistemas";
grant delete on "sistemas".fac_usuaxpos to "public" as "sistemas";
grant index on "sistemas".fac_usuaxpos to "public" as "sistemas";
grant select on "sistemas".fac_tipomovs to "public" as "sistemas";
grant update on "sistemas".fac_tipomovs to "public" as "sistemas";
grant insert on "sistemas".fac_tipomovs to "public" as "sistemas";
grant delete on "sistemas".fac_tipomovs to "public" as "sistemas";
grant index on "sistemas".fac_tipomovs to "public" as "sistemas";
grant select on "sistemas".glb_categors to "public" as "sistemas";
grant update on "sistemas".glb_categors to "public" as "sistemas";
grant insert on "sistemas".glb_categors to "public" as "sistemas";
grant delete on "sistemas".glb_categors to "public" as "sistemas";
grant index on "sistemas".glb_categors to "public" as "sistemas";
grant select on "sistemas".glb_ciudades to "public" as "sistemas";
grant update on "sistemas".glb_ciudades to "public" as "sistemas";
grant insert on "sistemas".glb_ciudades to "public" as "sistemas";
grant delete on "sistemas".glb_ciudades to "public" as "sistemas";
grant index on "sistemas".glb_ciudades to "public" as "sistemas";
grant select on "sistemas".glb_paramtrs to "public" as "sistemas";
grant update on "sistemas".glb_paramtrs to "public" as "sistemas";
grant insert on "sistemas".glb_paramtrs to "public" as "sistemas";
grant delete on "sistemas".glb_paramtrs to "public" as "sistemas";
grant index on "sistemas".glb_paramtrs to "public" as "sistemas";
grant select on "sistemas".glb_empresas to "public" as "sistemas";
grant update on "sistemas".glb_empresas to "public" as "sistemas";
grant insert on "sistemas".glb_empresas to "public" as "sistemas";
grant delete on "sistemas".glb_empresas to "public" as "sistemas";
grant index on "sistemas".glb_empresas to "public" as "sistemas";
grant select on "sistemas".glb_rolesusr to "public" as "sistemas";
grant update on "sistemas".glb_rolesusr to "public" as "sistemas";
grant insert on "sistemas".glb_rolesusr to "public" as "sistemas";
grant delete on "sistemas".glb_rolesusr to "public" as "sistemas";
grant index on "sistemas".glb_rolesusr to "public" as "sistemas";
grant select on "sistemas".glb_permxrol to "public" as "sistemas";
grant update on "sistemas".glb_permxrol to "public" as "sistemas";
grant insert on "sistemas".glb_permxrol to "public" as "sistemas";
grant delete on "sistemas".glb_permxrol to "public" as "sistemas";
grant index on "sistemas".glb_permxrol to "public" as "sistemas";
grant select on "sistemas".glb_permxusr to "public" as "sistemas";
grant update on "sistemas".glb_permxusr to "public" as "sistemas";
grant insert on "sistemas".glb_permxusr to "public" as "sistemas";
grant delete on "sistemas".glb_permxusr to "public" as "sistemas";
grant index on "sistemas".glb_permxusr to "public" as "sistemas";
grant select on "sistemas".glb_usuarios to "public" as "sistemas";
grant update on "sistemas".glb_usuarios to "public" as "sistemas";
grant insert on "sistemas".glb_usuarios to "public" as "sistemas";
grant delete on "sistemas".glb_usuarios to "public" as "sistemas";
grant index on "sistemas".glb_usuarios to "public" as "sistemas";
grant select on "sistemas".glb_dprogram to "public" as "sistemas";
grant update on "sistemas".glb_dprogram to "public" as "sistemas";
grant insert on "sistemas".glb_dprogram to "public" as "sistemas";
grant delete on "sistemas".glb_dprogram to "public" as "sistemas";
grant index on "sistemas".glb_dprogram to "public" as "sistemas";
grant select on "sistemas".inv_marcapro to "public" as "sistemas";
grant update on "sistemas".inv_marcapro to "public" as "sistemas";
grant insert on "sistemas".inv_marcapro to "public" as "sistemas";
grant delete on "sistemas".inv_marcapro to "public" as "sistemas";
grant index on "sistemas".inv_marcapro to "public" as "sistemas";
grant select on "sistemas".fac_formaimp to "public" as "sistemas";
grant update on "sistemas".fac_formaimp to "public" as "sistemas";
grant insert on "sistemas".fac_formaimp to "public" as "sistemas";
grant delete on "sistemas".fac_formaimp to "public" as "sistemas";
grant index on "sistemas".fac_formaimp to "public" as "sistemas";
grant select on "sistemas".inv_products to "public" as "sistemas";
grant update on "sistemas".inv_products to "public" as "sistemas";
grant insert on "sistemas".inv_products to "public" as "sistemas";
grant delete on "sistemas".inv_products to "public" as "sistemas";
grant index on "sistemas".inv_products to "public" as "sistemas";
grant select on "sistemas".inv_grupomov to "public" as "sistemas";
grant update on "sistemas".inv_grupomov to "public" as "sistemas";
grant insert on "sistemas".inv_grupomov to "public" as "sistemas";
grant delete on "sistemas".inv_grupomov to "public" as "sistemas";
grant index on "sistemas".inv_grupomov to "public" as "sistemas";
grant select on "sistemas".inv_destipro to "public" as "sistemas";
grant update on "sistemas".inv_destipro to "public" as "sistemas";
grant insert on "sistemas".inv_destipro to "public" as "sistemas";
grant delete on "sistemas".inv_destipro to "public" as "sistemas";
grant index on "sistemas".inv_destipro to "public" as "sistemas";
grant select on "sistemas".glb_pesotara to "public" as "sistemas";
grant update on "sistemas".glb_pesotara to "public" as "sistemas";
grant insert on "sistemas".glb_pesotara to "public" as "sistemas";
grant delete on "sistemas".glb_pesotara to "public" as "sistemas";
grant index on "sistemas".glb_pesotara to "public" as "sistemas";
grant select on "sistemas".inv_ncorretq to "public" as "sistemas";
grant update on "sistemas".inv_ncorretq to "public" as "sistemas";
grant insert on "sistemas".inv_ncorretq to "public" as "sistemas";
grant delete on "sistemas".inv_ncorretq to "public" as "sistemas";
grant index on "sistemas".inv_ncorretq to "public" as "sistemas";
grant select on "sistemas".inv_dproduct to "public" as "sistemas";
grant update on "sistemas".inv_dproduct to "public" as "sistemas";
grant insert on "sistemas".inv_dproduct to "public" as "sistemas";
grant delete on "sistemas".inv_dproduct to "public" as "sistemas";
grant index on "sistemas".inv_dproduct to "public" as "sistemas";
grant select on "sistemas".inv_scfisico to "public" as "sistemas";
grant update on "sistemas".inv_scfisico to "public" as "sistemas";
grant insert on "sistemas".inv_scfisico to "public" as "sistemas";
grant delete on "sistemas".inv_scfisico to "public" as "sistemas";
grant index on "sistemas".inv_scfisico to "public" as "sistemas";
grant select on "sistemas".fac_dtdoctos to "public" as "sistemas";
grant update on "sistemas".fac_dtdoctos to "public" as "sistemas";
grant insert on "sistemas".fac_dtdoctos to "public" as "sistemas";
grant delete on "sistemas".fac_dtdoctos to "public" as "sistemas";
grant index on "sistemas".fac_dtdoctos to "public" as "sistemas";
grant select on "sistemas".fac_permxpos to "public" as "sistemas";
grant update on "sistemas".fac_permxpos to "public" as "sistemas";
grant insert on "sistemas".fac_permxpos to "public" as "sistemas";
grant delete on "sistemas".fac_permxpos to "public" as "sistemas";
grant index on "sistemas".fac_permxpos to "public" as "sistemas";
grant select on "sistemas".fac_permxtdc to "public" as "sistemas";
grant update on "sistemas".fac_permxtdc to "public" as "sistemas";
grant insert on "sistemas".fac_permxtdc to "public" as "sistemas";
grant delete on "sistemas".fac_permxtdc to "public" as "sistemas";
grant index on "sistemas".fac_permxtdc to "public" as "sistemas";
grant select on "sistemas".glb_gruprovs to "public" as "sistemas";
grant update on "sistemas".glb_gruprovs to "public" as "sistemas";
grant insert on "sistemas".glb_gruprovs to "public" as "sistemas";
grant delete on "sistemas".glb_gruprovs to "public" as "sistemas";
grant index on "sistemas".glb_gruprovs to "public" as "sistemas";
grant select on "sistemas".sre_divrecet to "public" as "sistemas";
grant update on "sistemas".sre_divrecet to "public" as "sistemas";
grant insert on "sistemas".sre_divrecet to "public" as "sistemas";
grant delete on "sistemas".sre_divrecet to "public" as "sistemas";
grant index on "sistemas".sre_divrecet to "public" as "sistemas";
grant select on "sistemas".sre_mrecetas to "public" as "sistemas";
grant update on "sistemas".sre_mrecetas to "public" as "sistemas";
grant insert on "sistemas".sre_mrecetas to "public" as "sistemas";
grant delete on "sistemas".sre_mrecetas to "public" as "sistemas";
grant index on "sistemas".sre_mrecetas to "public" as "sistemas";
grant alter on "sistemas".sre_mrecetas to "public" as "sistemas";
grant references on "sistemas".sre_mrecetas to "public" as "sistemas";
grant select on "sistemas".sre_drecetas to "public" as "sistemas";
grant update on "sistemas".sre_drecetas to "public" as "sistemas";
grant insert on "sistemas".sre_drecetas to "public" as "sistemas";
grant delete on "sistemas".sre_drecetas to "public" as "sistemas";
grant index on "sistemas".sre_drecetas to "public" as "sistemas";
grant alter on "sistemas".sre_drecetas to "public" as "sistemas";
grant references on "sistemas".sre_drecetas to "public" as "sistemas";
grant select on "sistemas".sre_dordenes to "public" as "sistemas";
grant update on "sistemas".sre_dordenes to "public" as "sistemas";
grant insert on "sistemas".sre_dordenes to "public" as "sistemas";
grant delete on "sistemas".sre_dordenes to "public" as "sistemas";
grant index on "sistemas".sre_dordenes to "public" as "sistemas";
grant select on "sistemas".sre_mordenes to "public" as "sistemas";
grant update on "sistemas".sre_mordenes to "public" as "sistemas";
grant insert on "sistemas".sre_mordenes to "public" as "sistemas";
grant delete on "sistemas".sre_mordenes to "public" as "sistemas";
grant index on "sistemas".sre_mordenes to "public" as "sistemas";
grant select on "sistemas".pos_dtransac to "public" as "sistemas";
grant update on "sistemas".pos_dtransac to "public" as "sistemas";
grant insert on "sistemas".pos_dtransac to "public" as "sistemas";
grant delete on "sistemas".pos_dtransac to "public" as "sistemas";
grant index on "sistemas".pos_dtransac to "public" as "sistemas";
grant select on "sistemas".glb_mmodulos to "public" as "sistemas";
grant update on "sistemas".glb_mmodulos to "public" as "sistemas";
grant insert on "sistemas".glb_mmodulos to "public" as "sistemas";
grant delete on "sistemas".glb_mmodulos to "public" as "sistemas";
grant index on "sistemas".glb_mmodulos to "public" as "sistemas";
grant select on "sistemas".inv_pesajpro to "public" as "sistemas";
grant update on "sistemas".inv_pesajpro to "public" as "sistemas";
grant insert on "sistemas".inv_pesajpro to "public" as "sistemas";
grant delete on "sistemas".inv_pesajpro to "public" as "sistemas";
grant index on "sistemas".inv_pesajpro to "public" as "sistemas";
grant select on "sistemas".fac_vicortes to "public" as "sistemas";
grant update on "sistemas".fac_vicortes to "public" as "sistemas";
grant insert on "sistemas".fac_vicortes to "public" as "sistemas";
grant delete on "sistemas".fac_vicortes to "public" as "sistemas";
grant index on "sistemas".fac_vicortes to "public" as "sistemas";
grant select on "sistemas".sre_dplaspre to "public" as "sistemas";
grant update on "sistemas".sre_dplaspre to "public" as "sistemas";
grant insert on "sistemas".sre_dplaspre to "public" as "sistemas";
grant delete on "sistemas".sre_dplaspre to "public" as "sistemas";
grant index on "sistemas".sre_dplaspre to "public" as "sistemas";
grant select on "sistemas".glb_userauth to "public" as "sistemas";
grant update on "sistemas".glb_userauth to "public" as "sistemas";
grant insert on "sistemas".glb_userauth to "public" as "sistemas";
grant delete on "sistemas".glb_userauth to "public" as "sistemas";
grant index on "sistemas".glb_userauth to "public" as "sistemas";
grant select on "sistemas".pla_empleads to "public" as "sistemas";
grant update on "sistemas".pla_empleads to "public" as "sistemas";
grant insert on "sistemas".pla_empleads to "public" as "sistemas";
grant delete on "sistemas".pla_empleads to "public" as "sistemas";
grant index on "sistemas".pla_empleads to "public" as "sistemas";
grant select on "sistemas".pos_mtransac to "public" as "sistemas";
grant update on "sistemas".pos_mtransac to "public" as "sistemas";
grant insert on "sistemas".pos_mtransac to "public" as "sistemas";
grant delete on "sistemas".pos_mtransac to "public" as "sistemas";
grant index on "sistemas".pos_mtransac to "public" as "sistemas";
grant select on "sistemas".glb_delivery to "public" as "sistemas";
grant update on "sistemas".glb_delivery to "public" as "sistemas";
grant insert on "sistemas".glb_delivery to "public" as "sistemas";
grant delete on "sistemas".glb_delivery to "public" as "sistemas";
grant index on "sistemas".glb_delivery to "public" as "sistemas";
grant select on "sistemas".glb_elabodrs to "public" as "sistemas";
grant update on "sistemas".glb_elabodrs to "public" as "sistemas";
grant insert on "sistemas".glb_elabodrs to "public" as "sistemas";
grant delete on "sistemas".glb_elabodrs to "public" as "sistemas";
grant index on "sistemas".glb_elabodrs to "public" as "sistemas";
grant select on "sistemas".fac_dtransac to "public" as "sistemas";
grant update on "sistemas".fac_dtransac to "public" as "sistemas";
grant insert on "sistemas".fac_dtransac to "public" as "sistemas";
grant delete on "sistemas".fac_dtransac to "public" as "sistemas";
grant index on "sistemas".fac_dtransac to "public" as "sistemas";
grant select on "sistemas".fac_mofertas to "public" as "sistemas";
grant update on "sistemas".fac_mofertas to "public" as "sistemas";
grant insert on "sistemas".fac_mofertas to "public" as "sistemas";
grant delete on "sistemas".fac_mofertas to "public" as "sistemas";
grant index on "sistemas".fac_mofertas to "public" as "sistemas";
grant select on "sistemas".glb_vendedrs to "public" as "sistemas";
grant update on "sistemas".glb_vendedrs to "public" as "sistemas";
grant insert on "sistemas".glb_vendedrs to "public" as "sistemas";
grant delete on "sistemas".glb_vendedrs to "public" as "sistemas";
grant index on "sistemas".glb_vendedrs to "public" as "sistemas";
grant select on "sistemas".vta_presxcli to "public" as "sistemas";
grant update on "sistemas".vta_presxcli to "public" as "sistemas";
grant insert on "sistemas".vta_presxcli to "public" as "sistemas";
grant delete on "sistemas".vta_presxcli to "public" as "sistemas";
grant index on "sistemas".vta_presxcli to "public" as "sistemas";
grant select on "sistemas".vta_preuxcan to "public" as "sistemas";
grant update on "sistemas".vta_preuxcan to "public" as "sistemas";
grant insert on "sistemas".vta_preuxcan to "public" as "sistemas";
grant delete on "sistemas".vta_preuxcan to "public" as "sistemas";
grant index on "sistemas".vta_preuxcan to "public" as "sistemas";
grant select on "sistemas".fac_mtransac to "public" as "sistemas";
grant update on "sistemas".fac_mtransac to "public" as "sistemas";
grant insert on "sistemas".fac_mtransac to "public" as "sistemas";
grant delete on "sistemas".fac_mtransac to "public" as "sistemas";
grant index on "sistemas".fac_mtransac to "public" as "sistemas";
grant select on "sistemas".actpro to "public" as "sistemas";
grant update on "sistemas".actpro to "public" as "sistemas";
grant insert on "sistemas".actpro to "public" as "sistemas";
grant delete on "sistemas".actpro to "public" as "sistemas";
grant index on "sistemas".actpro to "public" as "sistemas";
grant select on "sistemas".fac_dvicorte to "public" as "sistemas";
grant update on "sistemas".fac_dvicorte to "public" as "sistemas";
grant insert on "sistemas".fac_dvicorte to "public" as "sistemas";
grant delete on "sistemas".fac_dvicorte to "public" as "sistemas";
grant index on "sistemas".fac_dvicorte to "public" as "sistemas";








CREATE FUNCTION  "sistemas".fxsaldofisico(wcodemp SMALLINT,wcodsuc SMALLINT,wcodbod SMALLINT,waniotr SMALLINT,
                               wmestra SMALLINT,wcditem VARCHAR(20))
 RETURNING DEC(12,2)

 DEFINE spcanuni LIKE inv_saldopro.canuni;

 -- Funcion para obtener el saldo de inventario fisico de un producto
 -- Mynor Ramirez
 -- Diciembre 2010

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;
 LET  spcanuni = 0; 

 -- Obteniendo inventario fisico del producto
 IF spcanuni IS NULL THEN LET spcanuni = 0; END IF;

 RETURN spcanuni;
END FUNCTION;

CREATE FUNCTION  "sistemas".fxsaldomovimientos(wcodemp SMALLINT,wcodsuc SMALLINT,wcodbod SMALLINT,
                                    waniotr SMALLINT,wmestra SMALLINT,wcditem VARCHAR(20))
 RETURNING DEC(16,2),DEC(14,6),DEC(16,2),DATE,DATE

 DEFINE spcanuni LIKE inv_proenbod.exican;
 DEFINE spcospro LIKE inv_proenbod.cospro;
 DEFINE sptotpro LIKE inv_proenbod.exival;
 DEFINE spopeuni LIKE inv_proenbod.exican;
 DEFINE spopeval LIKE inv_proenbod.exival;
 DEFINE xcanuni  LIKE inv_proenbod.exican;
 DEFINE xcospro  LIKE inv_proenbod.cospro;
 DEFINE xtotpro  LIKE inv_proenbod.exival;
 DEFINE xfulent  LIKE inv_proenbod.fulent;
 DEFINE xfulsal  LIKE inv_proenbod.fulsal;
 DEFINE aniant   SMALLINT;
 DEFINE mesant   SMALLINT;
 DEFINE conteo   INTEGER;

 -- Funcion para calcular el saldo de un producto por sus movimientos
 -- Los saldos son por empresa, sucursal, bodega y producto
 -- Mynor Ramirez
 -- Diciembre 2010

 -- Definiendo nivel de aislamiento
 SET ISOLATION COMMITTED READ;

 -- Calculando saldo de movimientos
 SELECT NVL(SUM(a.opeuni),0),
        NVL(SUM(a.opeval),0)
  INTO  spopeuni,
        spopeval
  FROM  inv_dtransac a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.cditem = wcditem
    AND a.tipope >= 0 
    AND a.estado = "V" 
    AND a.actexi = 1;

 -- Calculando fecha de ultima entrada
 SELECT MAX(a.fecemi)
  INTO  xfulent
  FROM  inv_dtransac a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.cditem = wcditem
    AND a.tipope = 1
    AND a.estado = "V"
    AND a.actexi = 1;

 -- Calculando fecha de ultima salida
 SELECT MAX(a.fecemi)
  INTO  xfulsal
  FROM  inv_dtransac a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.cditem = wcditem
    AND a.tipope = 0
    AND a.estado = "V"
    AND a.actexi = 1;

 -- Asignando saldos
 LET xcanuni = (spopeuni);
 LET xtotpro = (spopeval);

 -- Calculando costo promedio
 LET xcospro = 0;
 IF (xcanuni>0) THEN
    LET xcospro = (xtotpro/xcanuni);
 END IF

 -- Verificando nulos
 IF xcanuni IS NULL THEN LET xcanuni = 0; END IF;
 IF xcospro IS NULL THEN LET xcospro = 0; END IF;
 IF xtotpro IS NULL THEN LET xtotpro = 0; END IF;

 RETURN xcanuni,xcospro,xtotpro,xfulent,xfulsal;
END FUNCTION;





CREATE PROCEDURE "sistemas".sptempaquedefault(wcditem INT)
 -- Procedimiento para crear el empaque default de un producto despues de grabarlo en la tabla de empaques por producto
 -- Mynor Ramirez
 -- Diciembre 2010

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Borrando empaque antes de grabar
 SET LOCK MODE TO WAIT;
 DELETE FROM inv_epqsxpro
 WHERE inv_epqsxpro.cditem = wcditem;

 -- Creando empaque default
 INSERT INTO inv_epqsxpro
 SELECT 0,a.cditem,0,b.nommed,1,USER,CURRENT,CURRENT HOUR TO SECOND
  FROM  inv_products a,inv_unimedid b
  WHERE b.unimed = a.unimed
    AND a.cditem = wcditem;

END PROCEDURE;

CREATE PROCEDURE "sistemas".sptcrearaccesosxusuario(xroleid INT,xuserid VARCHAR(15))
 -- Procedimiento para crear los accesos x usuario de acuerdo al perfil
 -- Mynor Ramirez
 -- Octubre 2011

 DEFINE splnkpro LIKE glb_permxrol.lnkpro;
 DEFINE spcodpro LIKE glb_permxrol.codpro;
 DEFINE spprogid LIKE glb_permxrol.progid;
 DEFINE conteo   INT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Insertando los accesos por perfil que no existan
 FOREACH SELECT a.lnkpro,
                a.codpro,
                a.progid
          INTO  splnkpro,
                spcodpro,
                spprogid
          FROM  glb_permxrol a
          WHERE a.roleid = xroleid
            AND a.progid >0
          ORDER BY a.lnkpro

    -- Verificando si permiso ya existe
    SELECT COUNT(*)
     INTO  conteo
     FROM  glb_permxusr a
     WHERE a.roleid = xroleid
       AND a.lnkpro = splnkpro
       AND a.userid = xuserid;

    -- Si existe no lo grabar para mantener datos actualzados desde el usuario
    IF (conteo>0) THEN
       CONTINUE FOREACH;
    END IF;

    -- Insertando accesos
    SET LOCK MODE TO WAIT;
    INSERT INTO glb_permxusr
    VALUES (0,                           -- id unico del permiso
            xroleid,                     -- id del perfil
            splnkpro,                    -- id del acceso
            xuserid,                     -- usuario del perfil
            spcodpro,                    -- codigo de la opcion
            spprogid,                    -- id de la opcion
            1,                           -- marcando perfil de activo
            NULL,                        -- sin password
            NULL,                        -- sin fecha de inicio
            NULL,                        -- sin fecha de finalizacion
            USER,                        -- usuario registro
            CURRENT,                     -- fecha de registro
            CURRENT HOUR TO SECOND);     -- ghora de registro
 END FOREACH;

 -- Elimina todos aquellos permisos que no son del usuario por si fue cambio de perfil
 SET LOCK MODE TO WAIT;
 DELETE FROM glb_permxusr
 WHERE glb_permxusr.roleid != xroleid
   AND glb_permxusr.userid  = xuserid;
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptcreaprogramabase(xcodpro VARCHAR(15),xnompro VARCHAR(50),
                                                xordpro SMALLINT)
 -- Procedimiento para crear el programa base o raiz en la tabla de detalle
 -- de programas. Este programa es el nodo raiz para los accesos por programa
 -- Mynor Ramirez
 -- Octubre 2011

 DEFINE xcodopc LIKE glb_dprogram.codopc;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Creando codigo de la opcion
 SELECT TRIM(a.codpro)||"-0"
  INTO  xcodopc
  FROM  glb_programs a
  WHERE a.codpro = xcodpro;

 -- Insertando programa
 SET LOCK MODE TO WAIT;
 INSERT INTO glb_dprogram
 VALUES (0,                         -- id unico de la opcion
         xcodpro,                   -- codigo del programa padre
         0,                         -- id unico de la opcion por programa padre
         xcodopc,                   -- codigo de la opcion (codigo programa-id opcion)
         xnompro,                   -- nombre de la opcion
         xordpro,                   -- orden de la opcion
         0,                         -- opcion no requerie password
         USER,                      -- usuario registro la opcion
         CURRENT,                   -- fecha de registro de la opcion
         CURRENT HOUR TO SECOND);   -- hora de registro de la opcion
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptsaldosxproducto(wcodemp SMALLINT,wcodsuc SMALLINT,wcodbod SMALLINT,
                                    wtipmov SMALLINT,waniotr SMALLINT,wmestra SMALLINT,
                                    wcditem INT,wtipope SMALLINT,wcodabr VARCHAR(20),wpreuni DEC(14,6))

 -- Procedimiento para calcular y registrar el saldo x producto
 -- Los saldos son por empresa, sucursal, bodega, y producto
 -- Mynor Ramirez
 -- Diciembre 2010

 DEFINE xcanuni  LIKE inv_proenbod.exican;
 DEFINE xcospro  LIKE inv_proenbod.cospro;
 DEFINE xtotpro  LIKE inv_proenbod.exival;
 DEFINE xcodemp  LIKE inv_proenbod.codemp;
 DEFINE xfulent  LIKE inv_proenbod.fulent;
 DEFINE xfulsal  LIKE inv_proenbod.fulsal;
 DEFINE whayval  LIKE inv_tipomovs.hayval;

-- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Calculando saldo en base a movimientos
 CALL fxsaldomovimientos(wcodemp,wcodsuc,wcodbod,waniotr,wmestra,wcditem)
 RETURNING xcanuni,xcospro,xtotpro,xfulent,xfulsal;

 -- Verificando si producto ya existe en saldos x bodega
 LET xcodemp = NULL;
 SELECT UNIQUE a.codemp
  INTO  xcodemp
  FROM  inv_proenbod a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.cditem = wcditem;

 -- Si ya existe el saldo del producto lo actualiza, sino lo inserta
 IF xcodemp IS NOT NULL THEN
    -- Actualizando
    SET LOCK MODE TO WAIT;
    UPDATE inv_proenbod
    SET    inv_proenbod.fulent = xfulent,
           inv_proenbod.fulsal = xfulsal,
           inv_proenbod.exican = xcanuni,
           inv_proenbod.exival = xtotpro,
           inv_proenbod.cospro = xcospro
    WHERE  inv_proenbod.codemp = wcodemp
      AND  inv_proenbod.codsuc = wcodsuc
      AND  inv_proenbod.codbod = wcodbod
      AND  inv_proenbod.cditem = wcditem;
 ELSE
    -- Insertando
    SET LOCK MODE TO WAIT;
    INSERT INTO inv_proenbod
    VALUES (wcodemp,
            wcodsuc,
            wcodbod,
            wcditem,
            wcodabr,
            0,
            0,
            xfulent,
            xfulsal,
            xcanuni,
            xtotpro,
            xcospro,
            USER,
            CURRENT,
            CURRENT HOUR TO SECOND);
 END IF

 -- Actualizando existencia total del producto tomando en cuenta todas las
 -- bodegas
 -- Totalizando existencia
 SELECT NVL(SUM(a.exican),0)
  INTO  xcanuni
  FROM  inv_proenbod a
  WHERE (a.cditem = wcditem);

 -- Actualizando ultimo precio de compra si el tipo de movimiento esta parametrizado
 LET whayval = 0;
 SELECT NVL(a.hayval,0)
  INTO  whayval
  FROM  inv_tipomovs a
  WHERE a.tipmov = wtipmov;

 -- Verificando si tipo de movimiento actualiza valores
 IF (whayval=1) THEN
    -- Actualizando producto
    SET LOCK MODE TO WAIT;
    UPDATE inv_products
    SET    inv_products.pulcom = wpreuni,
           inv_products.exiact = xcanuni
    WHERE  inv_products.cditem = wcditem;
 ELSE
    -- Actualizando existencia
    SET LOCK MODE TO WAIT;
    UPDATE inv_products
    SET    inv_products.exiact = xcanuni
    WHERE  inv_products.cditem = wcditem;
 END IF
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptingresosxpeso(wcodemp SMALLINT,
                                             wcodsuc SMALLINT,
                                             wcodbod SMALLINT,
                                             wfecemi DATE,
                                             wtipope SMALLINT,
                                             wcodori SMALLINT,
                                             wnomori VARCHAR(100),
                                             wcditem INT,
                                             wcodabr VARCHAR(20),
                                             wnumetq VARCHAR(20),
                                             wcanuni DEC(12,2),
                                             wcxpeso DEC(12,2))

 -- Procedimiento para insertar un movimiento de ingreso x peso automaticamente
 -- Mynor Ramirez
 -- Agosto 2012
 -- Si la cantidad x peso (wcxpeso=0) toma wcanuni=cantidad peso, sino toma
 -- wcxpeso=cantidad unidades 

 DEFINE wtipmov  LIKE inv_mtransac.tipmov;
 DEFINE waniotr  LIKE inv_mtransac.aniotr;
 DEFINE wmestra  LIKE inv_mtransac.mestra;
 DEFINE wobserv  LIKE inv_mtransac.observ;
 DEFINE westado  LIKE inv_mtransac.estado;
 DEFINE wchkinv  LIKE fac_puntovta.chkinv;
 DEFINE wlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xcodabr  LIKE inv_dtransac.codabr;
 DEFINE xnserie  LIKE inv_dtransac.nserie;
 DEFINE xcodepq  LIKE inv_dtransac.codepq;
 DEFINE xcanepq  LIKE inv_dtransac.canepq;
 DEFINE xpreuni  LIKE inv_dtransac.preuni;
 DEFINE xtotpro  LIKE inv_dtransac.totpro;
 DEFINE xcorrel  LIKE inv_dtransac.correl;
 DEFINE xopeval  LIKE inv_dtransac.opeval;
 DEFINE xopeuni  LIKE inv_dtransac.opeuni;
 DEFINE xtipmov  LIKE inv_dtransac.tipmov;
 DEFINE xnumdoc  LIKE inv_mtransac.numrf1;
 DEFINE xtipope  LIKE inv_dtransac.tipope;
 DEFINE xcanuni  DEC(14,6);
 DEFINE weximov  SMALLINT;
 DEFINE conteo   INT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Verificando si tipo de movimiento es cargo
 IF (wtipope=1) THEN

   -- Obteniendo tipo de movimiento default de ingresos por peso
   SELECT NVL(a.valchr,0)
    INTO  wtipmov
    FROM  glb_paramtrs a
    WHERE a.numpar = 13
      AND a.tippar = 8;

   -- Seleccionando tipo de movimiento
   SELECT NVL(a.tipmov,0),NVL(a.tipope,0)
    INTO  xtipmov,xtipope
    FROM  inv_tipomovs a
    WHERE a.tipmov = wtipmov;

   -- Verificando si existen datos
   IF (xtipmov>0) THEN

    -- Verificando si existe un movimiento de ingreso por peso de la misma fecha
    SELECT UNIQUE a.lnktra
     INTO  xlnktra
     FROM  inv_mtransac a
     WHERE a.codemp = wcodemp
       AND a.codsuc = wcodsuc
       AND a.codbod = wcodbod
       AND a.tipmov = xtipmov
       AND a.fecemi = wfecemi;

    IF (xlnktra=0) OR xlnktra IS NULL THEN
     -- Creando encabezado
     -- Asignando datos
     LET wlnktra = 0;
     LET xnumdoc = "";
     LET wobserv = "INGRESOS POR PESO";
     LET wmestra = MONTH(wfecemi);
     LET waniotr = YEAR(wfecemi);
     LET xcorrel = 1;
     LET westado = "V";
     LET xnserie = "";
     LET xpreuni = 0;

     -- Insertando encabezado
     SET LOCK MODE TO WAIT;
     INSERT INTO inv_mtransac
     VALUES (0                     ,  -- Id unico
             wcodemp               ,  -- Empresa
             wcodsuc               ,  -- Sucursal
             wcodbod               ,  -- Bodega
             xtipmov               ,  -- Tipo de movimiento
             wcodori               ,  -- Origen
             wnomori               ,  -- Nombre del origen
             0                     ,  -- Destino
             ""                    ,  -- Nombre del Destino
             wlnktra               ,  -- Numero id de transaccion que origino el consumo
             xnumdoc               ,  -- Numero de documento
             0                     ,  -- Movimiento es externo
             waniotr               ,  -- Anio
             wmestra               ,  -- Mes
             wfecemi               ,  -- Fecha de emision
             xtipope               ,  -- Tipo de operacion del movimiento
             wobserv               ,  -- Observaciones
             westado               ,  -- Estado V=igente
             wlnktra               ,  -- Numero id de transaccion que origino el consumo
             0                     ,  -- Consumo automatico
             NULL                  ,  -- Motivo de la anulacion
             NULL                  ,  -- Fecha de anulacion
             NULL                  ,  -- Hora de la anulacion
             NULL                  ,  -- Usuario anulo
             "S"                   ,  -- Documento impreso
             USER                  ,  -- Usuario registro
             CURRENT               ,  -- Fecha de registro
             CURRENT HOUR TO SECOND); -- Hora de registro

     -- Obteniendo serial de la tabla encabezado
     SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
      INTO  xlnktra
      FROM  inv_mtransac;
    ELSE
     -- Asignando datos
     LET wmestra = MONTH(wfecemi);
     LET waniotr = YEAR(wfecemi);
     LET westado = "V";
     LET xnserie = "";
     LET xpreuni = 0;

     -- Obteniendo correlativo de producto de la transaccion
     SELECT (NVL(MAX(a.correl),0)+1)
      INTO  xcorrel
      FROM  inv_dtransac a
      WHERE a.lnktra = xlnktra;
    END IF;

    --- Creando detalle
    -- Calculando cantidad y valor para calculo de saldo

    -- Si cantidad x peso (wcxpeso=0) se toma peso en libras que viene en (wcanuni),
    -- de lo contrario se toma la cantidad en unidades que viene en (wcxpeso) 
    IF (wcxpeso=0) THEN 
       LET xcanuni = wcanuni;
    ELSE
       LET xcanuni = wcxpeso;
    END IF;

    LET xcanepq = xcanuni;
    LET xtotpro = 0;
    LET xopeuni = xcanuni;
    LET xopeval = xtotpro;

    -- Insertando detalle
    SET LOCK MODE TO WAIT;
    INSERT INTO inv_dtransac
    VALUES (xlnktra               ,  -- id transaccion
            wcodemp               ,  -- empresa
            wcodsuc               ,  -- sucursal
            wcodbod               ,  -- bodega
            xtipmov               ,  -- tipo de movimiento
            wcodori               ,  -- origen
            0                     ,  -- destino
            wfecemi               ,  -- fecha de emision
            waniotr               ,  -- anio
            wmestra               ,  -- mes
            wcditem               ,  -- codigo de producto
            wcodabr               ,  -- codigo abreviado
            xnserie               ,  -- numero de serie
            0                     ,  -- codigo de empaque
            xcanepq               ,  -- cantidad de empaque
            xcanuni               ,  -- cantidad
            xpreuni               ,  -- precio unitario
            xtotpro               ,  -- total del producto
            0                     ,  -- precio promedio
            xcorrel               ,  -- correlativo
            wnumetq               ,  -- Numero de etiqueta
            xtipope               ,  -- tipo de operacion del movimniento
            westado               ,  -- estado
            xopeval               ,  -- total
            xopeuni               ,  -- cantidad
            1                     ,  -- Actualiza existencia automaticamente 
            USER                  ,  -- Usuario registro
            CURRENT               ,  -- Fecha de registro
            CURRENT HOUR TO SECOND); -- Hora de registro
  END IF;

 END IF;
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptrebajaconsumos(wlnktra INTEGER,
                                              wcodemp SMALLINT,
                                              wcodsuc SMALLINT,
                                              wcodbod SMALLINT,
                                              wfecemi DATE,
                                              wtipope SMALLINT)

 -- Procedimiento para insertar los consumos de producto automaticos en el inventario
 -- Mynor Ramirez
 -- Juio 2012

 DEFINE wtipmov  LIKE inv_mtransac.tipmov;
 DEFINE waniotr  LIKE inv_mtransac.aniotr;
 DEFINE wmestra  LIKE inv_mtransac.mestra;
 DEFINE wobserv  LIKE inv_mtransac.observ;
 DEFINE westado  LIKE inv_mtransac.estado;
 DEFINE wchkinv  LIKE fac_puntovta.chkinv;
 DEFINE xlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xcodabr  LIKE inv_dtransac.codabr;
 DEFINE xnserie  LIKE inv_dtransac.nserie;
 DEFINE xcodepq  LIKE inv_dtransac.codepq;
 DEFINE xcanepq  LIKE inv_dtransac.canepq;
 DEFINE xpreuni  LIKE inv_dtransac.preuni;
 DEFINE xtotpro  LIKE inv_dtransac.totpro;
 DEFINE xcorrel  LIKE inv_dtransac.correl;
 DEFINE xbarcod  LIKE inv_dtransac.barcod;
 DEFINE xopeval  LIKE inv_dtransac.opeval;
 DEFINE xopeuni  LIKE inv_dtransac.opeuni;
 DEFINE xtipmov  LIKE inv_dtransac.tipmov;
 DEFINE xnumdoc  LIKE inv_mtransac.numrf1;
 DEFINE xtipope  LIKE inv_dtransac.tipope;
 DEFINE xcditem  LIKE inv_dtransac.cditem;
 DEFINE xcoddes  LIKE inv_mtransac.coddes;
 DEFINE xnomdes  LIKE inv_mtransac.nomdes;
 DEFINE wcditem  LIKE inv_dtransac.cditem;
 DEFINE wcanuni  LIKE inv_dtransac.canuni;
 DEFINE xcanuni  DEC(14,6);
 DEFINE weximov  SMALLINT;
 DEFINE conteo   INT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Verificando si tipo de movimiento es cargo 
 IF (wtipope=1) THEN

   -- Eliminando movimiento de consumo
   SET LOCK MODE TO WAIT;
   DELETE FROM inv_mtransac
   WHERE inv_mtransac.lnkcon = wlnktra;

   -- Obteniendo tipo de movimiento default de consumos automaticos
   SELECT NVL(a.valchr,0)
    INTO  wtipmov
    FROM  glb_paramtrs a
    WHERE a.numpar = 13
      AND a.tippar = 2;

   -- Seleccionando tipo de movimiento
   SELECT NVL(a.tipmov,0),NVL(a.tipope,0)
    INTO  xtipmov,xtipope
    FROM  inv_tipomovs a
    WHERE a.tipmov = wtipmov;

   -- Verificando si existen datos
   IF (xtipmov>0) THEN

    -- Verificando si movimiento tiene productos que contengan subproductos
    SELECT COUNT(*)
     INTO  conteo
     FROM  inv_dtransac d ,inv_dproduct s
     WHERE d.lnktra = wlnktra
       AND s.cditem = d.cditem
       AND s.cantid >0;

    IF (conteo>0) THEN
     -- Creando encabezado
     -- Seleccionando datos
     SELECT NVL(a.numrf1,"0"),NVL(a.coddes,0),a.nomdes
      INTO  xnumdoc,xcoddes,xnomdes
      FROM  inv_mtransac a
      WHERE a.lnktra = wlnktra;

     -- Asignando datos
     LET wmestra = MONTH(wfecemi);
     LET waniotr = YEAR(wfecemi);
     LET wobserv = "CONSUMO AUTOMATICO";
     LET xbarcod = "";
     LET westado = "V";
     LET xnserie = "";
     LET xpreuni = 0;

     -- Seleccionando datos
     SET LOCK MODE TO WAIT;
     INSERT INTO inv_mtransac
     VALUES (0                     ,  -- Id unico
             wcodemp               ,  -- Empresa
             wcodsuc               ,  -- Sucursal
             wcodbod               ,  -- Bodega
             xtipmov               ,  -- Tipo de movimiento
             0                     ,  -- Origen
             ""                    ,  -- Nombre del origen
             xcoddes               ,  -- Destino
             xnomdes               ,  -- Nombre del Destino
             wlnktra               ,  -- ID Movimiento
             xnumdoc               ,  -- Numero de documento
             0                     ,  -- Movimiento es externo
             waniotr               ,  -- Anio
             wmestra               ,  -- Mes
             wfecemi               ,  -- Fecha de emision
             xtipope               ,  -- Tipo de operacion del movimiento
             wobserv               ,  -- Observaciones
             westado               ,  -- Estado V=igente
             wlnktra               ,  -- Numero id de transaccion que origino el consumo
             0                     ,  -- Consumo automatico
             NULL                  ,  -- Motivo de la anulacion
             NULL                  ,  -- Fecha de anulacion
             NULL                  ,  -- Hora de la anulacion
             NULL                  ,  -- Usuario anulo
             "S"                   ,  -- Documento impreso
             USER                  ,  -- Usuario registro
             CURRENT               ,  -- Fecha de registro
             CURRENT HOUR TO SECOND); -- Hora de registro

     -- Obteniendo serial de la tabla encabezado
     SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
      INTO  xlnktra
      FROM  inv_mtransac;

    -- Seleccionando productos que tiene subproductos
    -- Verificando si movimiento tiene productos que contengan subproductos
    FOREACH SELECT a.cditem,
                   a.canuni,
                   b.citems,
                   b.codabr,
                   b.cantid
             INTO  wcditem,
                   wcanuni,
                   xcditem,
                   xcodabr,
                   xcanuni
             FROM  inv_dtransac a,inv_dproduct b
             WHERE a.lnktra = wlnktra
               AND b.cditem = a.cditem
               AND b.cantid >0

      -- Obteniendo correlativo de producto de la transaccion
      SELECT (NVL(MAX(a.correl),0)+1)
       INTO  xcorrel
       FROM  inv_dtransac a
       WHERE a.lnktra = xlnktra;

      -- Calculando cantidad y valor para calculo de saldo
      LET xcanuni = (xcanuni*wcanuni);
      LET xcanepq = xcanuni;
      LET xtotpro = 0;

      IF (xtipope=0) THEN
         LET xopeuni = (xcanuni*(-1));
         LET xopeval = (xtotpro*(-1));
      ELSE
         LET xopeuni = xcanuni;
         LET xopeval = xtotpro;
      END IF;

      -- Insertando detalle
      SET LOCK MODE TO WAIT;
      INSERT INTO inv_dtransac
      VALUES (xlnktra               ,  -- id transaccion
              wcodemp               ,  -- empresa
              wcodsuc               ,  -- sucursal
              wcodbod               ,  -- bodega
              xtipmov               ,  -- tipo de movimiento
              0                     ,  -- origen
              xcoddes               ,  -- destino
              wfecemi               ,  -- fecha de emision
              waniotr               ,  -- anio
              wmestra               ,  -- mes
              xcditem               ,  -- codigo de producto
              xcodabr               ,  -- codigo abreviado
              xnserie               ,  -- numero de serie
              0                     ,  -- codigo de empaque
              xcanepq               ,  -- cantidad de empaque
              xcanuni               ,  -- cantidad
              xpreuni               ,  -- precio unitario
              xtotpro               ,  -- total del producto
              0                     ,  -- precio promedio
              xcorrel               ,  -- correlativo
              xbarcod               ,  -- codigo de barra
              xtipope               ,  -- tipo de operacion del movimniento
              westado               ,  -- estado
              xopeval               ,  -- total
              xopeuni               ,  -- cantidad
              1                     ,  -- actualiza existencia automaticamente 
              USER                  ,  -- Usuario registro
              CURRENT               ,  -- Fecha de registro
              CURRENT HOUR TO SECOND); -- Hora de registro

    END FOREACH;

   END IF;

  END IF;

 END IF;
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptsalidasxpeso(wcodemp SMALLINT,
                                            wcodsuc SMALLINT,
                                            wcodbod SMALLINT,
                                            wfecemi DATE,
                                            wtipope SMALLINT,
                                            wcoddes SMALLINT,
                                            wnomdes VARCHAR(100),
                                            wcditem INT,
                                            wcodabr VARCHAR(20),
                                            wnumetq VARCHAR(20),
                                            wcanuni DEC(12,2))

 -- Procedimiento para insertar un movimiento de salida x peso automaticamente
 -- Mynor Ramirez
 -- Octubre 2012

 DEFINE wtipmov  LIKE inv_mtransac.tipmov;
 DEFINE waniotr  LIKE inv_mtransac.aniotr;
 DEFINE wmestra  LIKE inv_mtransac.mestra;
 DEFINE wobserv  LIKE inv_mtransac.observ;
 DEFINE westado  LIKE inv_mtransac.estado;
 DEFINE wchkinv  LIKE fac_puntovta.chkinv;
 DEFINE wlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xcodabr  LIKE inv_dtransac.codabr;
 DEFINE xnserie  LIKE inv_dtransac.nserie;
 DEFINE xcodepq  LIKE inv_dtransac.codepq;
 DEFINE xcanepq  LIKE inv_dtransac.canepq;
 DEFINE xpreuni  LIKE inv_dtransac.preuni;
 DEFINE xtotpro  LIKE inv_dtransac.totpro;
 DEFINE xcorrel  LIKE inv_dtransac.correl;
 DEFINE xopeval  LIKE inv_dtransac.opeval;
 DEFINE xopeuni  LIKE inv_dtransac.opeuni;
 DEFINE xtipmov  LIKE inv_dtransac.tipmov;
 DEFINE xnumdoc  LIKE inv_mtransac.numrf1;
 DEFINE xtipope  LIKE inv_dtransac.tipope;
 DEFINE xcanuni  DEC(14,6);
 DEFINE weximov  SMALLINT;
 DEFINE conteo   INT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Verificando si tipo de movimiento es abono
 IF (wtipope=0) AND 
    wcditem IS NOT NULL AND 
    wcodabr IS NOT NULL THEN 

   -- Obteniendo tipo de movimiento default de salidas por peso
   SELECT NVL(a.valchr,0)
    INTO  wtipmov
    FROM  glb_paramtrs a
    WHERE a.numpar = 13
      AND a.tippar = 9;

   -- Seleccionando tipo de movimiento
   SELECT NVL(a.tipmov,0),NVL(a.tipope,0)
    INTO  xtipmov,xtipope
    FROM  inv_tipomovs a
    WHERE a.tipmov = wtipmov;

   -- Verificando si existen datos
   IF (xtipmov>0) THEN

    -- Verificando si existe un movimiento de salida por peso de la misma fecha
    SELECT UNIQUE a.lnktra
     INTO  xlnktra
     FROM  inv_mtransac a
     WHERE a.codemp = wcodemp
       AND a.codsuc = wcodsuc
       AND a.codbod = wcodbod
       AND a.tipmov = xtipmov
       AND a.fecemi = wfecemi;

    IF (xlnktra=0) OR xlnktra IS NULL THEN
     -- Creando encabezado
     -- Asignando datos
     LET wlnktra = 0;
     LET xnumdoc = "";
     LET wobserv = "SALIDAS POR PESO";
     LET wmestra = MONTH(wfecemi);
     LET waniotr = YEAR(wfecemi);
     LET xcorrel = 1;
     LET westado = "V";
     LET xnserie = "";
     LET xpreuni = 0;

     -- Insertando encabezado
     SET LOCK MODE TO WAIT;
     INSERT INTO inv_mtransac
     VALUES (0                     ,  -- Id unico
             wcodemp               ,  -- Empresa
             wcodsuc               ,  -- Sucursal
             wcodbod               ,  -- Bodega
             xtipmov               ,  -- Tipo de movimiento
             0                     ,  -- Origen
             ""                    ,  -- Nombre del origen
             wcoddes               ,  -- Destino
             wnomdes               ,  -- Nombre del Destino
             wlnktra               ,  -- Numero id de transaccion que origino el consumo
             xnumdoc               ,  -- Numero de documento
             0                     ,  -- Movimiento es externo
             waniotr               ,  -- Anio
             wmestra               ,  -- Mes
             wfecemi               ,  -- Fecha de emision
             xtipope               ,  -- Tipo de operacion del movimiento
             wobserv               ,  -- Observaciones
             westado               ,  -- Estado V=igente
             wlnktra               ,  -- Numero id de transaccion que origino el consumo
             0                     ,  -- Consumo automatico
             NULL                  ,  -- Motivo de la anulacion
             NULL                  ,  -- Fecha de anulacion
             NULL                  ,  -- Hora de la anulacion
             NULL                  ,  -- Usuario anulo
             "S"                   ,  -- Documento impreso
             USER                  ,  -- Usuario registro
             CURRENT               ,  -- Fecha de registro
             CURRENT HOUR TO SECOND); -- Hora de registro

     -- Obteniendo serial de la tabla encabezado
     SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
      INTO  xlnktra
      FROM  inv_mtransac;
    ELSE
     -- Asignando datos
     LET wmestra = MONTH(wfecemi);
     LET waniotr = YEAR(wfecemi);
     LET westado = "V";
     LET xnserie = "";
     LET xpreuni = 0;

     -- Obteniendo correlativo de producto de la transaccion
     SELECT (NVL(MAX(a.correl),0)+1)
      INTO  xcorrel
      FROM  inv_dtransac a
      WHERE a.lnktra = xlnktra;
    END IF;

    --- Creando detalle
    -- Calculando cantidad y valor para calculo de saldo
    LET xcanuni = wcanuni;
    LET xcanepq = xcanuni;
    LET xtotpro = 0;
    LET xopeuni = (xcanuni*(-1));
    LET xopeval = (xtotpro*(-1));

    -- Insertando detalle
    SET LOCK MODE TO WAIT;
    INSERT INTO inv_dtransac
    VALUES (xlnktra               ,  -- id transaccion
            wcodemp               ,  -- empresa
            wcodsuc               ,  -- sucursal
            wcodbod               ,  -- bodega
            xtipmov               ,  -- tipo de movimiento
            0                     ,  -- origen
            wcoddes               ,  -- destino
            wfecemi               ,  -- fecha de emision
            waniotr               ,  -- anio
            wmestra               ,  -- mes
            wcditem               ,  -- codigo de producto
            wcodabr               ,  -- codigo abreviado
            xnserie               ,  -- numero de serie
            0                     ,  -- codigo de empaque
            xcanepq               ,  -- cantidad de empaque
            xcanuni               ,  -- cantidad
            xpreuni               ,  -- precio unitario
            xtotpro               ,  -- total del producto
            0                     ,  -- precio promedio
            xcorrel               ,  -- correlativo
            wnumetq               ,  -- Numero de etiqueta
            xtipope               ,  -- tipo de operacion del movimniento
            westado               ,  -- estado
            xopeval               ,  -- total
            xopeuni               ,  -- cantidad
            1                     ,  -- Actualiza existencia automaticamente
            USER                  ,  -- Usuario registro
            CURRENT               ,  -- Fecha de registro
            CURRENT HOUR TO SECOND); -- Hora de registro
  END IF;

 END IF;
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptsumafisico(wcodemp SMALLINT,wcodsuc SMALLINT,wcodbod SMALLINT,
                                          wfecinv DATE,wcditem INT,wcodabr CHAR(20))

 -- Procedimiento para totalizar lo registrado en el inventario fisico
 -- Mynor Ramirez
 -- Mayo 2010

 DEFINE xtotuni DEC(12,2);
 DEFINE wmestra LIKE inv_tofisico.mestra;
 DEFINE waniotr LIKE inv_tofisico.aniotr;
 DEFINE wlnktra LIKE inv_mtransac.lnktra;
 DEFINE wtipmov LIKE inv_mtransac.tipmov;
 DEFINE conteo  INT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;
 LET waniotr = YEAR(wfecinv);
 LET wmestra = MONTH(wfecinv);

 -- Totalizando conteo fisico
 SELECT NVL(SUM(a.canuni),0)
  INTO  xtotuni
  FROM  inv_scfisico a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.fecinv = wfecinv
    AND a.cditem = wcditem;

 -- Verificando si producto ya existe regstrado en inventario fisico
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_tofisico a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.fecinv = wfecinv
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem;
  IF (conteo>0) THEN
     -- Si ya existe borra el inventario
     SET LOCK MODE TO WAIT;
     DELETE FROM inv_tofisico
     WHERE inv_tofisico.codemp = wcodemp
       AND inv_tofisico.codsuc = wcodsuc
       AND inv_tofisico.codbod = wcodbod
       AND inv_tofisico.fecinv = wfecinv
       AND inv_tofisico.aniotr = waniotr
       AND inv_tofisico.mestra = wmestra
       AND inv_tofisico.cditem = wcditem;
  END IF

  -- Insertando ivnentario
  SET LOCK MODE TO WAIT;
  INSERT INTO inv_tofisico
  VALUES (wcodemp,
          wcodsuc,
          wcodbod,
          waniotr,
          wmestra,
          wfecinv,
          wcditem,
          wcodabr,
          0,
          0,
          0,
          xtotuni,
          0,
          xtotuni,
          0,
          0,
          0,
          USER,
          CURRENT,
          CURRENT HOUR TO SECOND);
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptinsertfaccturasinv(wlnktra INTEGER,
                                                  wnumpos SMALLINT,
                                                  wcodemp SMALLINT,
                                                  wnserie CHAR(10),
                                                  wnumdoc INTEGER,
                                                  wfecemi DATE,
                                                  wcodcli INTEGER,
                                                  wnomcli CHAR(100),
                                                  wllevar SMALLINT)

 -- Procedimiento para insertar los documentos de facturacion en el inventario
 -- Mynor Ramirez

 DEFINE wtipmov  LIKE inv_mtransac.tipmov;
 DEFINE wcodsuc  LIKE inv_mtransac.codsuc;
 DEFINE wcodbod  LIKE inv_mtransac.codbod;
 DEFINE waniotr  LIKE inv_mtransac.aniotr;
 DEFINE wmestra  LIKE inv_mtransac.mestra;
 DEFINE wobserv  LIKE inv_mtransac.observ;
 DEFINE westado  LIKE inv_mtransac.estado;
 DEFINE wtipope  LIKE inv_mtransac.tipope;
 DEFINE wafeinv  LIKE fac_puntovta.chkinv;
 DEFINE xlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xcditem  LIKE inv_dtransac.cditem;
 DEFINE xcodabr  LIKE inv_dtransac.codabr;
 DEFINE xnserie  LIKE inv_dtransac.nserie;
 DEFINE xcodepq  LIKE inv_dtransac.codepq;
 DEFINE xcanepq  LIKE inv_dtransac.canepq;
 DEFINE xcanuni  LIKE inv_dtransac.canuni;
 DEFINE xpreuni  LIKE inv_dtransac.preuni;
 DEFINE xtotpro  LIKE inv_dtransac.totpro;
 DEFINE xcorrel  LIKE inv_dtransac.correl;
 DEFINE xbarcod  LIKE inv_dtransac.barcod;
 DEFINE xopeval  LIKE inv_dtransac.opeval;
 DEFINE xopeuni  LIKE inv_dtransac.opeuni;
 DEFINE xentrga  LIKE sre_drecetas.entrga;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Seleccionando datos del punto de venta
 SELECT NVL(a.codsuc,0),NVL(a.codbod,0),1
  INTO  wcodsuc,wcodbod,wafeinv
  FROM  fac_puntovta a
  WHERE a.numpos = wnumpos;

 -- Verificando si existen datos
 IF (wcodsuc>0) THEN

  -- Verificando si punto de venta afecta inventario
  IF (wafeinv>0) THEN

   -- Obteniendo tipo de movimiento default de para facturacion
   SELECT NVL(a.valchr,0)
    INTO  wtipmov
    FROM  glb_paramtrs a
    WHERE a.numpar = 13
      AND a.tippar = 1;

   -- Seleccionando tipo de movimiento
   SELECT NVL(a.tipmov,0),NVL(a.tipope,0)
    INTO  wtipmov,wtipope
    FROM  inv_tipomovs a
    WHERE a.tipmov = wtipmov;

   -- Verificando si existen datos
   IF (wtipmov>0) THEN

    -- Asignando datos
    LET wmestra = MONTH(wfecemi);
    LET waniotr = YEAR(wfecemi);
    LET wobserv = "FACTURACION DOCUMENTO # "||wnserie||"-"||wnumdoc;
    LET westado = "V";

    -- Seleccionando datos
    INSERT INTO inv_mtransac
    VALUES (0                     ,  -- Id unico
            wcodemp               ,  -- Empresa
            wcodsuc               ,  -- Sucursal
            wcodbod               ,  -- Bodega
            wtipmov               ,  -- Tipo de movimiento
            0                     ,  -- Origen
            ""                    ,  -- Nombre origen
            wcodcli               ,  -- Destino
            wnomcli               ,  -- Nombre destino
            wlnktra               ,  -- ID factura
            wnumdoc               ,  -- Numero de documento de facturacion
            0                     ,  -- Movimiento es externo Movimiento es externo
            waniotr               ,  -- Anio
            wmestra               ,  -- Mes
            wfecemi               ,  -- Fecha de emision
            wtipope               ,  -- Tipo de operacion del movimiento
            wobserv               ,  -- Observaciones
            westado               ,  -- Estado V=igente
            0                     ,  -- ID numero de consumo
            0                     ,  -- 1=Consumo automatico,0=No consumo automatico
            NULL                  ,  -- Motivo de la anulacion
            NULL                  ,  -- Fecha de anulacion
            NULL                  ,  -- Hora de la anulacion
            NULL                  ,  -- Usuario anulo
            "S"                   ,  -- Documento impreso
            USER                  ,  -- Usuario registro
            CURRENT               ,  -- Fecha de registro
            CURRENT HOUR TO SECOND); -- Hora de registro

    -- Obteniendo serial de la tabla encabezado
    SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
     INTO  xlnktra
     FROM  inv_mtransac;

    -- Creando detalle de la transaccion
    LET xnserie = "";
    FOREACH SELECT y.cditem,
                   y.codabr,
                   0,
                   (NVL(y.canuni,0)*NVL(x.cantid,0)),
                   (NVL(y.canuni,0)*NVL(x.cantid,0)),
                   0,   -- precio
                   0,   -- total
                   "0",
                   y.entrga,
                   x.correl
             INTO  xcditem,
                   xcodabr,
                   xcodepq,
                   xcanepq,
                   xcanuni,
                   xpreuni,
                   xtotpro,
                   xbarcod,
                   xentrga,
                   xcorrel
             FROM  pos_dtransac x,sre_drecetas y
             WHERE x.lnktra = wlnktra
               AND y.lnkrec = x.lnkrec

     -- Verificando tipo de entrega
     IF xentrga<2 THEN
        IF xentrga!=wllevar THEN
           CONTINUE FOREACH;
        END IF;
     END IF;

     -- Asignando precio de costo
     LET xpreuni = (SELECT NVL(a.cospro,0) FROM inv_proenbod a 
                     WHERE a.codemp = wcodemp
                       AND a.codsuc = wcodsuc
                       AND a.codbod = wcodbod
                       AND a.cditem = xcditem);

     -- Verificando precio costo
     IF xpreuni IS NULL THEN
        LET xpreuni = 0;
     END IF 

     -- Calculando total
     LET xtotpro = (xcanuni*xpreuni);

     -- Calculando cantidad y valor para calculo de saldo
     IF (wtipope=0) THEN
        LET xopeuni = (xcanuni*(-1));
        LET xopeval = (xtotpro*(-1));
     ELSE
        LET xopeuni = xcanuni;
        LET xopeval = xtotpro;
     END IF;

     -- Insertando detalle
     INSERT INTO inv_dtransac
     VALUES (xlnktra               ,  -- id transaccion
             wcodemp               ,  -- empresa
             wcodsuc               ,  -- sucursal
             wcodbod               ,  -- bodega
             wtipmov               ,  -- tipo de movimiento
             0                     ,  -- origen
             wcodcli               ,  -- destino
             wfecemi               ,  -- fecha de emision
             waniotr               ,  -- anio
             wmestra               ,  -- mes
             xcditem               ,  -- codigo de producto
             xcodabr               ,  -- codigo abreviado
             xnserie               ,  -- numero de serie
             0                     ,  -- codigo de empaque
             xcanepq               ,  -- cantidad de empaque
             xcanuni               ,  -- cantidad
             xpreuni               ,  -- precio unitario
             xtotpro               ,  -- total del producto
             0                     ,  -- precio promedio
             xcorrel               ,  -- correlativo
             xbarcod               ,  -- codigo de barra
             wtipope               ,  -- tipo de operacion del movimniento
             westado               ,  -- estado
             xopeval               ,  -- total
             xopeuni               ,  -- cantidad
             1                     ,  -- Actualiza existencia
             0                     ,  -- porcentaje isv
             0                     ,  -- total isv
             USER                  ,  -- Usuario registro
             CURRENT               ,  -- Fecha de registro
             CURRENT HOUR TO SECOND); -- Hora de registro

    END FOREACH;

   END IF;

  END IF;

 END IF;
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptinsertafacturas(wlnktra INTEGER,
                                               wnumpos SMALLINT,
                                               wcodemp SMALLINT,
                                               wnumdoc INTEGER,
                                               wfecemi DATE,
                                               wcodcli INTEGER)

 -- Procedimiento para insertar los documentos de facturacion en el inventario
 -- Mynor Ramirez
 -- Abril 2011

 DEFINE wtipmov  LIKE inv_mtransac.tipmov;
 DEFINE wcodsuc  LIKE inv_mtransac.codsuc;
 DEFINE wcodbod  LIKE inv_mtransac.codbod;
 DEFINE waniotr  LIKE inv_mtransac.aniotr;
 DEFINE wmestra  LIKE inv_mtransac.mestra;
 DEFINE wobserv  LIKE inv_mtransac.observ;
 DEFINE westado  LIKE inv_mtransac.estado;
 DEFINE wtipope  LIKE inv_mtransac.tipope;
 DEFINE wchkinv  LIKE fac_puntovta.chkinv;
 DEFINE xlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xcditem  LIKE inv_dtransac.cditem;
 DEFINE xcodabr  LIKE inv_dtransac.codabr;
 DEFINE xnserie  LIKE inv_dtransac.nserie;
 DEFINE xcodepq  LIKE inv_dtransac.codepq;
 DEFINE xcanepq  LIKE inv_dtransac.canepq;
 DEFINE xcanuni  LIKE inv_dtransac.canuni;
 DEFINE xpreuni  LIKE inv_dtransac.preuni;
 DEFINE xtotpro  LIKE inv_dtransac.totpro;
 DEFINE xcorrel  LIKE inv_dtransac.correl;
 DEFINE xbarcod  LIKE inv_dtransac.barcod;
 DEFINE xopeval  LIKE inv_dtransac.opeval;
 DEFINE xopeuni  LIKE inv_dtransac.opeuni;
 DEFINE xporisv  LIKE inv_dtransac.porisv;
 DEFINE xtotisv  LIKE inv_dtransac.totisv;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Seleccionando datos del punto de venta
 SELECT NVL(a.codsuc,0),NVL(a.codbod,0),NVL(a.chkinv,0)
  INTO  wcodsuc,wcodbod,wchkinv
  FROM  fac_puntovta a
  WHERE a.numpos = wnumpos;

 -- Verificando si existen datos
 IF (wcodsuc>0) THEN

  -- Verificando si punto de venta afecta inventario
  IF (wchkinv>0) THEN

   -- Obteniendo tipo de movimiento default de para facturacion
   SELECT NVL(a.valchr,0)
    INTO  wtipmov
    FROM  glb_paramtrs a
    WHERE a.numpar = 13
      AND a.tippar = 1;

   -- Seleccionando tipo de movimiento
   SELECT NVL(a.tipmov,0),NVL(a.tipope,0)
    INTO  wtipmov,wtipope
    FROM  inv_tipomovs a
    WHERE a.tipmov = wtipmov;

   -- Verificando si existen datos
   IF (wtipmov>0) THEN

    -- Asignando datos
    LET wmestra = MONTH(wfecemi);
    LET waniotr = YEAR(wfecemi);
    LET wobserv = "FACTURACION DOCUMENTO # "||wnumdoc;
    LET westado = "V";

    -- Seleccionando datos
    INSERT INTO inv_mtransac
    VALUES (0                     ,  -- Id unico
            wcodemp               ,  -- Empresa
            wcodsuc               ,  -- Sucursal
            wcodbod               ,  -- Bodega
            wtipmov               ,  -- Tipo de movimiento
            0                     ,  -- Origen
            ""                    ,  -- Nombre origen
            wcodcli               ,  -- Destino
            ""                    ,  -- Nombre destino
            wlnktra               ,  -- ID factura
            wnumdoc               ,  -- Numero de documento de facturacion
            0                     ,  -- Movimiento es externo Movimiento es externo
            waniotr               ,  -- Anio
            wmestra               ,  -- Mes
            wfecemi               ,  -- Fecha de emision
            wtipope               ,  -- Tipo de operacion del movimiento
            wobserv               ,  -- Observaciones
            westado               ,  -- Estado V=igente
            0                     ,  -- ID numero de consumo
            0                     ,  -- 1=Consumo automatico, 0=No consumo automatico
            NULL                  ,  -- Motivo de la anulacion
            NULL                  ,  -- Fecha de anulacion
            NULL                  ,  -- Hora de la anulacion
            NULL                  ,  -- Usuario anulo
            "S"                   ,  -- Documento impreso
            USER                  ,  -- Usuario registro
            CURRENT               ,  -- Fecha de registro
            CURRENT HOUR TO SECOND); -- Hora de registro

    -- Obteniendo serial de la tabla encabezado
    SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
     INTO  xlnktra
     FROM  inv_mtransac;

    -- Creando detalle de la transaccion
    LET xnserie = "";
    FOREACH SELECT a.cditem,
                   a.codabr,
                   a.codepq,
                   a.canepq,
                   a.cantid,
                   a.preuni,
                   a.totpro,
                   "0",
                   a.correl,
                   a.porisv,
                   a.totisv
             INTO  xcditem,
                   xcodabr,
                   xcodepq,
                   xcanepq,
                   xcanuni,
                   xpreuni,
                   xtotpro,
                   xbarcod,
                   xcorrel,
                   xporisv,
                   xtotisv
             FROM  fac_dtransac a
             WHERE a.lnktra = wlnktra

     -- Calculando cantdad y valor para calculo de saldo
     IF (wtipope=0) THEN
        LET xopeuni = (xcanuni*(-1));
        LET xopeval = (xtotpro*(-1));
     ELSE
        LET xopeuni = xcanuni;
        LET xopeval = xtotpro;
     END IF;

     -- Insertando detalle
     INSERT INTO inv_dtransac
     VALUES (xlnktra               ,  -- id transaccion
             wcodemp               ,  -- empresa
             wcodsuc               ,  -- sucursal
             wcodbod               ,  -- bodega
             wtipmov               ,  -- tipo de movimiento
             0                     ,  -- origen
             wcodcli               ,  -- destino
             wfecemi               ,  -- fecha de emision
             waniotr               ,  -- anio
             wmestra               ,  -- mes
             xcditem               ,  -- codigo de producto
             xcodabr               ,  -- codigo abreviado
             xnserie               ,  -- numero de serie
             0                     ,  -- codigo de empaque
             xcanepq               ,  -- cantidad de empaque
             xcanuni               ,  -- cantidad
             xpreuni               ,  -- precio unitario
             xtotpro               ,  -- total del producto
             0                     ,  -- precio promedio
             xcorrel               ,  -- correlativo
             xbarcod               ,  -- codigo de barra
             wtipope               ,  -- tipo de operacion del movimniento
             westado               ,  -- estado
             xopeval               ,  -- total
             xopeuni               ,  -- cantidad
             1                     ,  -- Actualiza existencia
             xporisv               ,  -- porcentaje isv
             xtotisv               ,  -- total isv
             USER                  ,  -- Usuario registro
             CURRENT               ,  -- Fecha de registro
             CURRENT HOUR TO SECOND); -- Hora de registro
    END FOREACH;

   END IF;

  END IF;

 END IF;
END PROCEDURE;





grant  execute on function "sistemas".fxsaldofisico (smallint,smallint,smallint,smallint,smallint,varchar) to "public" as "sistemas";
grant  execute on function "sistemas".fxsaldomovimientos (smallint,smallint,smallint,smallint,smallint,varchar) to "public" as "sistemas";
grant  execute on procedure "sistemas".sptempaquedefault (integer) to "public" as "sistemas";
grant  execute on procedure "sistemas".sptcrearaccesosxusuario (integer,varchar) to "public" as "sistemas";
grant  execute on procedure "sistemas".sptcreaprogramabase (varchar,varchar,smallint) to "public" as "sistemas";
grant  execute on procedure "sistemas".sptsaldosxproducto (smallint,smallint,smallint,smallint,smallint,smallint,integer,smallint,varchar,decimal) to "public" as "sistemas";
grant  execute on procedure "sistemas".sptingresosxpeso (smallint,smallint,smallint,date,smallint,smallint,varchar,integer,varchar,varchar,decimal,decimal) to "public" as "sistemas";
grant  execute on procedure "sistemas".sptrebajaconsumos (integer,smallint,smallint,smallint,date,smallint) to "public" as "sistemas";
grant  execute on procedure "sistemas".sptsalidasxpeso (smallint,smallint,smallint,date,smallint,smallint,varchar,integer,varchar,varchar,decimal) to "public" as "sistemas";
grant  execute on procedure "sistemas".sptsumafisico (smallint,smallint,smallint,date,integer,char) to "public" as "sistemas";
grant  execute on procedure "sistemas".sptinsertfaccturasinv (integer,smallint,smallint,char,integer,date,integer,char,smallint) to "public" as "sistemas";
grant  execute on procedure "sistemas".sptinsertafacturas (integer,smallint,smallint,integer,date,integer) to "public" as "sistemas";


revoke usage on language SPL from public ;

grant usage on language SPL to public ;



create view "sistemas".vis_saldosxmovxmes (codemp,codsuc,codbod,aniotr,mestra,cditem,tipope,saluni,salval) as 
  select x0.codemp ,x0.codsuc ,x0.codbod ,x0.aniotr ,x0.mestra 
    ,x0.cditem ,x0.tipope ,NVL (sum(x0.canuni ) ,0 ),NVL (sum(x0.totpro 
    ) ,0 )from "sistemas".inv_dtransac x0 where (x0.estado = 'V'
     ) group by x0.codemp ,x0.codsuc ,x0.codbod ,x0.aniotr ,x0.mestra 
    ,x0.cditem ,x0.tipope ;                                  
                                                             
                                     
create view "sistemas".vis_tipdocxpos (lnktdc,numpos,hayimp,nomdoc) as 
  select x0.lnktdc ,x0.numpos ,x1.hayimp ,((((TRIM ( BOTH ' ' FROM 
    x1.nomdoc ) || ' SERIE ' ) || TRIM ( BOTH ' ' FROM x0.nserie 
    ) ) || ' ' ) || x0.nomdoc ) from "sistemas".fac_tdocxpos x0 
    ,"sistemas".fac_tipodocs x1 ,"sistemas".glb_empresas x2 where 
    (((x0.tipdoc = x1.tipdoc ) AND (x0.estado = 'A' ) ) AND (x2.codemp 
    = x0.codemp ) ) ;                                        
                                                             
                
create view "sistemas".vis_productos (cditem,codabr,dsitem,presug) as 
  select x0.cditem ,x0.codabr ,x0.dsitem ,x0.presug from "sistemas"
    .inv_products x0 ;                                       
                                                             
           
create view "sistemas".vis_integramovtos (codemp,codsuc,codbod,nomemp,fecemi,codgru,nomgru,codcat,cditem,codabr,dsitem,nomuni,canuni) as 
  select x0.codemp ,x0.codsuc ,x0.codbod ,x3.nomemp ,x0.fecemi 
    ,x5.codgru ,x2.nomgru ,x4.codcat ,x1.cditem ,x1.codabr ,x4.despro 
    ,x6.nomabr ,x1.canuni from "sistemas".inv_mtransac x0 ,"sistemas"
    .inv_dtransac x1 ,"sistemas".inv_grupomov x2 ,"sistemas".glb_empresas 
    x3 ,"sistemas".inv_products x4 ,"sistemas".inv_tipomovs x5 
    ,"sistemas".inv_unimedid x6 where ((((((((x0.lnktra = x1.lnktra 
    ) AND (x0.estado = 'V' ) ) AND (x0.tipope >= 0 ) ) AND (x4.cditem 
    = x1.cditem ) ) AND (x3.codemp = x0.codemp ) ) AND (x5.tipmov 
    = x0.tipmov ) ) AND (x2.codgru = x5.codgru ) ) AND (x6.unimed 
    = x4.unimed ) ) ;                                       
create view "sistemas".vis_productobodega (codemp,nomemp,codsuc,nomsuc,codbod,nombod,codcat,nomcat,cditem,codabr,dsitem,nomuni) as 
  select x0.codemp ,x1.nomemp ,x0.codsuc ,x2.nomsuc ,x0.codbod 
    ,x3.nombod ,x4.codcat ,x6.nomcat ,x0.cditem ,x0.codabr ,x4.dsitem 
    ,x5.nommed from "sistemas".inv_proenbod x0 ,"sistemas".glb_empresas 
    x1 ,"sistemas".glb_sucsxemp x2 ,"sistemas".inv_mbodegas x3 
    ,"sistemas".inv_products x4 ,"sistemas".inv_unimedid x5 ,"sistemas"
    .glb_categors x6 where ((((((x1.codemp = x0.codemp ) AND 
    (x2.codsuc = x0.codsuc ) ) AND (x3.codbod = x0.codbod ) ) 
    AND (x4.cditem = x0.cditem ) ) AND (x5.unimed = x4.unimed 
    ) ) AND (x6.codcat = x4.codcat ) ) ;                     
                                                             
                                 
create view "sistemas".vis_tipdocxpostodos (lnktdc,numpos,hayimp,nomdoc) as 
  select x0.lnktdc ,x0.numpos ,x1.hayimp ,((((TRIM ( BOTH ' ' FROM 
    x1.nomdoc ) || ' SERIE ' ) || TRIM ( BOTH ' ' FROM x0.nserie 
    ) ) || ' ' ) || x0.nomdoc ) from "sistemas".fac_tdocxpos x0 
    ,"sistemas".fac_tipodocs x1 ,"sistemas".glb_empresas x2 where 
    ((x0.tipdoc = x1.tipdoc ) AND (x2.codemp = x0.codemp ) ) 
    ;                                                        
                                                             
                     
create view "sistemas".vis_reporterecetas (numrec,nomrec,coddiv,nomdiv,cospre,prevta,codabr,dsitem,unimed,nommed,canuni) as 
  select x0.numrec ,x0.nomrec ,x4.coddiv ,x4.nomdiv ,x0.cospre 
    ,x0.prevta ,x2.codabr ,x2.dsitem ,x3.unimed ,x3.nommed ,x1.canuni 
    from "sistemas".sre_mrecetas x0 ,"sistemas".sre_drecetas x1 
    ,"sistemas".inv_products x2 ,"sistemas".inv_unimedid x3 ,"sistemas"
    .sre_divrecet x4 where ((((x1.lnkrec = x0.lnkrec ) AND (x2.cditem 
    = x1.cditem ) ) AND (x3.unimed = x1.unimed ) ) AND (x4.coddiv 
    = x0.coddiv ) ) ;                                        
                                                             
                                                             
                                                             
                                      
create view "sistemas".vis_programas (codmod,codpro,nompro,parent,progid,lnkpro,ordopc) as 
  select x0.codmod ,CASE WHEN (x1.progid = 0 )  THEN x0.codpro 
     ELSE x1.codopc  END ,x1.nomopc ,CASE WHEN (x1.progid = 0 
    )  THEN '0'  ELSE x0.codpro  END ,x1.progid ,x1.lnkpro ,x1.ordopc 
    from "sistemas".glb_programs x0 ,"sistemas".glb_dprogram x1 
    where ((x0.codpro = x1.codpro ) AND (x0.ordpro > 0 ) ) ; 
                                                             
                                                             
         
create view "sistemas".vis_ordenescomida (numpos,numesa,lnkrec,nomrec,cantid,prevta,totrec,totdes) as 
  select x0.numpos ,x0.numesa ,x1.lnkrec ,x2.nomrec ,sum(x1.cantid 
    ) ,(sum(x1.totrec ) / sum(x1.cantid ) ) ,sum(x1.totrec ) 
    ,sum(x1.totdes ) from "sistemas".sre_mordenes x0 ,"sistemas"
    .sre_dordenes x1 ,"sistemas".sre_mrecetas x2 where ((((x0.lnkord 
    = x1.lnkord ) AND (x0.estado = 1 ) ) AND (x0.status = 1 ) 
    ) AND (x2.lnkrec = x1.lnkrec ) ) group by x0.numpos ,x0.numesa 
    ,x1.lnkrec ,x2.nomrec ;                   
create view "sistemas".vis_detallelibroventas (numpos,nompos,tipdoc,nomdoc,lnktdc,nserie,fecemi,nomcli,numnit,numdoc,totgra,totexe,totisv,totdoc) as 
  select x0.numpos ,x2.nompos ,x0.tipdoc ,x1.nomdoc ,x0.lnktdc 
    ,x0.nserie ,x0.fecemi ,x0.nomcli ,x0.numnit ,x0.numdoc ,x0.totgra 
    ,x0.totexe ,x0.totisv ,x0.totcta from "sistemas".pos_mtransac 
    x0 ,"sistemas".fac_tipodocs x1 ,"sistemas".fac_puntovta x2 
    where (((x0.estado = 1 ) AND (x2.numpos = x0.numpos ) ) AND 
    (x1.tipdoc = x0.tipdoc ) )  union all select x3.numpos ,x5.nompos 
    ,x3.tipdoc ,x4.nomdoc ,x3.lnktdc ,x3.nserie ,x3.fecemi ,x3.nomcli 
    ,x3.numnit ,x3.numdoc ,0 ,0 ,0 ,0 from "sistemas".pos_mtransac 
    x3 ,"sistemas".fac_tipodocs x4 ,"sistemas".fac_puntovta x5 
    where (((x3.estado = 0 ) AND (x5.numpos = x3.numpos ) ) AND 
    (x4.tipdoc = x3.tipdoc ) ) ;                             
                                                             
                                                             
                                                             
                             
create view "sistemas".vis_consumosinvxi (numpos,fecemi,codcat,nomcat,cditem,codabr,dsitem,unimed,nommed,cantid) as 
  select x0.numpos ,x0.feccor ,x4.codcat ,x6.nomcat ,x4.cditem 
    ,x4.codabr ,x4.dsitem ,x4.unimed ,x5.nommed ,(x1.cantid * 
    x3.canuni ) from "sistemas".pos_mtransac x0 ,"sistemas".pos_dtransac 
    x1 ,"sistemas".sre_mrecetas x2 ,"sistemas".sre_drecetas x3 
    ,"sistemas".inv_products x4 ,"sistemas".inv_unimedid x5 ,"sistemas"
    .glb_categors x6 where (((((((x0.lnktra = x1.lnktra ) AND 
    (x0.estado = 1 ) ) AND (x3.lnkrec = x1.lnkrec ) ) AND (x2.lnkrec 
    = x3.lnkrec ) ) AND (x4.cditem = x3.cditem ) ) AND (x5.unimed 
    = x4.unimed ) ) AND (x6.codcat = x4.codcat ) )  union all 
    select x7.numpos ,x7.fecemi ,x11.codcat ,x13.nomcat ,x11.cditem 
    ,x11.codabr ,x11.dsitem ,x11.unimed ,x12.nommed ,(x8.cantid 
    * x10.canuni ) from "sistemas".sre_mordenes x7 ,"sistemas".sre_dplaspre 
    x8 ,"sistemas".sre_mrecetas x9 ,"sistemas".sre_drecetas x10 
    ,"sistemas".inv_products x11 ,"sistemas".inv_unimedid x12 ,
    "sistemas".glb_categors x13 where ((((((((x7.lnkord = x8.lnkord 
    ) AND (x7.estado = 1 ) ) AND (x7.status = 0 ) ) AND (x10.lnkrec 
    = x8.lnkrec ) ) AND (x9.lnkrec = x10.lnkrec ) ) AND (x11.cditem 
    = x10.cditem ) ) AND (x12.unimed = x11.unimed ) ) AND (x13.codcat 
    = x11.codcat ) ) ;                
create view "sistemas".vis_libroventas (numpos,nompos,tipdoc,nomdoc,lnktdc,nserie,fecemi,numdoc,totgra,totexe,totisv,totdoc) as 
  select x0.numpos ,x2.nompos ,x0.tipdoc ,x1.nomdoc ,x0.lnktdc 
    ,x0.nserie ,x0.fecemi ,x0.numdoc ,0 ,(select NVL (sum(x6.totpro 
    ) ,0 )from "sistemas".fac_dtransac x6 where (x6.lnktra = x0.lnktra 
    ) ) ,0 ,(select NVL (sum(x7.totpro ) ,0 )from "sistemas".fac_dtransac 
    x7 where (x7.lnktra = x0.lnktra ) ) from "sistemas".fac_mtransac 
    x0 ,"sistemas".fac_tipodocs x1 ,"sistemas".fac_puntovta x2 
    where (((x0.estado = 'V' ) AND (x2.numpos = x0.numpos ) ) 
    AND (x1.tipdoc = x0.tipdoc ) )  union all select x3.numpos 
    ,x5.nompos ,x3.tipdoc ,x4.nomdoc ,x3.lnktdc ,x3.nserie ,x3.fecemi 
    ,x3.numdoc ,0 ,0 ,0 ,0 from "sistemas".fac_mtransac x3 ,"sistemas"
    .fac_tipodocs x4 ,"sistemas".fac_puntovta x5 where (((x3.estado 
    = 'A' ) AND (x5.numpos = x3.numpos ) ) AND (x4.tipdoc = x3.tipdoc 
    ) ) ;                                                    
                                                             
                     
create view "sistemas".vis_ventasxhoras (numpos,feccor,horini,horfin,totdoc,totvta) as 
  select x0.numpos ,x0.feccor ,'00:00' ,'00:59' ,count(*) ,sum(x0.totdoc 
    ) from "sistemas".fac_mtransac x0 where ((((EXTEND (x0.horsis 
    ,hour to minute) >= datetime(00:00) hour to minute ) AND 
    (EXTEND (x0.horsis ,hour to minute) <= datetime(00:59) hour 
    to minute ) ) AND (x0.estado = 'V' ) ) AND (x0.haycor = 1 
    ) ) group by x0.numpos ,x0.feccor ,3 ,4  union all select 
    x1.numpos ,x1.feccor ,'07:00' ,'07:59' ,count(*) ,sum(x1.totdoc 
    ) from "sistemas".fac_mtransac x1 where ((((EXTEND (x1.horsis 
    ,hour to minute) >= datetime(07:00) hour to minute ) AND 
    (EXTEND (x1.horsis ,hour to minute) <= datetime(07:59) hour 
    to minute ) ) AND (x1.estado = 'V' ) ) AND (x1.haycor = 1 
    ) ) group by x1.numpos ,x1.feccor ,3 ,4  union all select 
    x2.numpos ,x2.feccor ,'08:00' ,'08:59' ,count(*) ,sum(x2.totdoc 
    ) from "sistemas".fac_mtransac x2 where ((((EXTEND (x2.horsis 
    ,hour to minute) >= datetime(08:00) hour to minute ) AND 
    (EXTEND (x2.horsis ,hour to minute) <= datetime(08:59) hour 
    to minute ) ) AND (x2.estado = 'V' ) ) AND (x2.haycor = 1 
    ) ) group by x2.numpos ,x2.feccor ,3 ,4  union all select 
    x3.numpos ,x3.feccor ,'09:00' ,'09:59' ,count(*) ,sum(x3.totdoc 
    ) from "sistemas".fac_mtransac x3 where ((((EXTEND (x3.horsis 
    ,hour to minute) >= datetime(09:00) hour to minute ) AND 
    (EXTEND (x3.horsis ,hour to minute) <= datetime(09:59) hour 
    to minute ) ) AND (x3.estado = 'V' ) ) AND (x3.haycor = 1 
    ) ) group by x3.numpos ,x3.feccor ,3 ,4  union all select 
    x4.numpos ,x4.feccor ,'10:00' ,'10:59' ,count(*) ,sum(x4.totdoc 
    ) from "sistemas".fac_mtransac x4 where ((((EXTEND (x4.horsis 
    ,hour to minute) >= datetime(10:00) hour to minute ) AND 
    (EXTEND (x4.horsis ,hour to minute) <= datetime(10:59) hour 
    to minute ) ) AND (x4.estado = 'V' ) ) AND (x4.haycor = 1 
    ) ) group by x4.numpos ,x4.feccor ,3 ,4  union all select 
    x5.numpos ,x5.feccor ,'11:00' ,'11:59' ,count(*) ,sum(x5.totdoc 
    ) from "sistemas".fac_mtransac x5 where ((((EXTEND (x5.horsis 
    ,hour to minute) >= datetime(11:00) hour to minute ) AND 
    (EXTEND (x5.horsis ,hour to minute) <= datetime(11:59) hour 
    to minute ) ) AND (x5.estado = 'V' ) ) AND (x5.haycor = 1 
    ) ) group by x5.numpos ,x5.feccor ,3 ,4  union all select 
    x6.numpos ,x6.feccor ,'12:00' ,'12:59' ,count(*) ,sum(x6.totdoc 
    ) from "sistemas".fac_mtransac x6 where ((((EXTEND (x6.horsis 
    ,hour to minute) >= datetime(12:00) hour to minute ) AND 
    (EXTEND (x6.horsis ,hour to minute) <= datetime(12:59) hour 
    to minute ) ) AND (x6.estado = 'V' ) ) AND (x6.haycor = 1 
    ) ) group by x6.numpos ,x6.feccor ,3 ,4  union all select 
    x7.numpos ,x7.feccor ,'13:00' ,'13:59' ,count(*) ,sum(x7.totdoc 
    ) from "sistemas".fac_mtransac x7 where ((((EXTEND (x7.horsis 
    ,hour to minute) >= datetime(13:00) hour to minute ) AND 
    (EXTEND (x7.horsis ,hour to minute) <= datetime(13:59) hour 
    to minute ) ) AND (x7.estado = 'V' ) ) AND (x7.haycor = 1 
    ) ) group by x7.numpos ,x7.feccor ,3 ,4  union all select 
    x8.numpos ,x8.feccor ,'14:00' ,'14:59' ,count(*) ,sum(x8.totdoc 
    ) from "sistemas".fac_mtransac x8 where ((((EXTEND (x8.horsis 
    ,hour to minute) >= datetime(14:00) hour to minute ) AND 
    (EXTEND (x8.horsis ,hour to minute) <= datetime(14:59) hour 
    to minute ) ) AND (x8.estado = 'V' ) ) AND (x8.haycor = 1 
    ) ) group by x8.numpos ,x8.feccor ,3 ,4  union all select 
    x9.numpos ,x9.feccor ,'15:00' ,'15:59' ,count(*) ,sum(x9.totdoc 
    ) from "sistemas".fac_mtransac x9 where ((((EXTEND (x9.horsis 
    ,hour to minute) >= datetime(15:00) hour to minute ) AND 
    (EXTEND (x9.horsis ,hour to minute) <= datetime(15:59) hour 
    to minute ) ) AND (x9.estado = 'V' ) ) AND (x9.haycor = 1 
    ) ) group by x9.numpos ,x9.feccor ,3 ,4  union all select 
    x10.numpos ,x10.feccor ,'16:00' ,'16:59' ,count(*) ,sum(x10.totdoc 
    ) from "sistemas".fac_mtransac x10 where ((((EXTEND (x10.horsis 
    ,hour to minute) >= datetime(16:00) hour to minute ) AND 
    (EXTEND (x10.horsis ,hour to minute) <= datetime(16:59) hour 
    to minute ) ) AND (x10.estado = 'V' ) ) AND (x10.haycor = 
    1 ) ) group by x10.numpos ,x10.feccor ,3 ,4  union all select 
    x11.numpos ,x11.feccor ,'17:00' ,'17:59' ,count(*) ,sum(x11.totdoc 
    ) from "sistemas".fac_mtransac x11 where ((((EXTEND (x11.horsis 
    ,hour to minute) >= datetime(17:00) hour to minute ) AND 
    (EXTEND (x11.horsis ,hour to minute) <= datetime(17:59) hour 
    to minute ) ) AND (x11.estado = 'V' ) ) AND (x11.haycor = 
    1 ) ) group by x11.numpos ,x11.feccor ,3 ,4  union all select 
    x12.numpos ,x12.feccor ,'18:00' ,'18:59' ,count(*) ,sum(x12.totdoc 
    ) from "sistemas".fac_mtransac x12 where ((((EXTEND (x12.horsis 
    ,hour to minute) >= datetime(18:00) hour to minute ) AND 
    (EXTEND (x12.horsis ,hour to minute) <= datetime(18:59) hour 
    to minute ) ) AND (x12.estado = 'V' ) ) AND (x12.haycor = 
    1 ) ) group by x12.numpos ,x12.feccor ,3 ,4  union all select 
    x13.numpos ,x13.feccor ,'19:00' ,'19:59' ,count(*) ,sum(x13.totdoc 
    ) from "sistemas".fac_mtransac x13 where ((((EXTEND (x13.horsis 
    ,hour to minute) >= datetime(19:00) hour to minute ) AND 
    (EXTEND (x13.horsis ,hour to minute) <= datetime(19:59) hour 
    to minute ) ) AND (x13.estado = 'V' ) ) AND (x13.haycor = 
    1 ) ) group by x13.numpos ,x13.feccor ,3 ,4  union all select 
    x14.numpos ,x14.feccor ,'20:00' ,'20:59' ,count(*) ,sum(x14.totdoc 
    ) from "sistemas".fac_mtransac x14 where ((((EXTEND (x14.horsis 
    ,hour to minute) >= datetime(20:00) hour to minute ) AND 
    (EXTEND (x14.horsis ,hour to minute) <= datetime(20:59) hour 
    to minute ) ) AND (x14.estado = 'V' ) ) AND (x14.haycor = 
    1 ) ) group by x14.numpos ,x14.feccor ,3 ,4  union all select 
    x15.numpos ,x15.feccor ,'21:00' ,'21:59' ,count(*) ,sum(x15.totdoc 
    ) from "sistemas".fac_mtransac x15 where ((((EXTEND (x15.horsis 
    ,hour to minute) >= datetime(21:00) hour to minute ) AND 
    (EXTEND (x15.horsis ,hour to minute) <= datetime(21:59) hour 
    to minute ) ) AND (x15.estado = 'V' ) ) AND (x15.haycor = 
    1 ) ) group by x15.numpos ,x15.feccor ,3 ,4  union all select 
    x16.numpos ,x16.feccor ,'22:00' ,'22:59' ,count(*) ,sum(x16.totdoc 
    ) from "sistemas".fac_mtransac x16 where ((((EXTEND (x16.horsis 
    ,hour to minute) >= datetime(22:00) hour to minute ) AND 
    (EXTEND (x16.horsis ,hour to minute) <= datetime(22:59) hour 
    to minute ) ) AND (x16.estado = 'V' ) ) AND (x16.haycor = 
    1 ) ) group by x16.numpos ,x16.feccor ,3 ,4 ;            
                                                             
                                                             
                            
create view "sistemas".vis_estadiscorte (numpos,codemp,lnktdc,nomdoc,nserie,numdoc,fecemi,feccor,codcat,nomcat,cditem,dsitem,codabr,cantid,preuni,totpro,totisv,totdes) as 
  select x0.numpos ,x0.codemp ,x0.lnktdc ,x4.nomdoc ,x0.nserie 
    ,x0.numdoc ,x0.fecemi ,x0.feccor ,x2.codcat ,x3.nomcat ,x1.cditem 
    ,x2.dsitem ,x1.codabr ,x1.cantid ,x1.preuni ,x1.totpro ,0 
    ,x1.descto from "sistemas".fac_mtransac x0 ,"sistemas".fac_dtransac 
    x1 ,"sistemas".inv_products x2 ,"sistemas".glb_categors x3 
    ,"sistemas".fac_tipodocs x4 ,"sistemas".fac_tdocxpos x5 where 
    (((((((x0.lnktra = x1.lnktra ) AND (x0.estado = 'V' ) ) AND 
    (x2.cditem = x1.cditem ) ) AND (x3.codcat = x2.codcat ) ) 
    AND (x4.tipdoc = x0.tipdoc ) ) AND (x5.lnktdc = x0.lnktdc 
    ) ) AND (x5.haycor = 1 ) ) ;                   
create view "sistemas".vis_estadisticaventas (numpos,codemp,nserie,numdoc,fecemi,feccor,codcat,nomcat,cditem,dsitem,codabr,cantid,prevta,totrec,totisv,totdes) as 
  select x0.numpos ,x0.codemp ,x0.nserie ,x0.numdoc ,x0.fecemi 
    ,x0.feccor ,x2.codcat ,x3.nomcat ,x1.cditem ,x2.dsitem ,x1.codabr 
    ,x1.cantid ,x1.preuni ,x1.totpro ,x1.totisv ,x1.descto from 
    "sistemas".fac_mtransac x0 ,"sistemas".fac_dtransac x1 ,"sistemas"
    .inv_products x2 ,"sistemas".glb_categors x3 where (((((x0.lnktra 
    = x1.lnktra ) AND (x0.estado = 'V' ) ) AND (x0.haycor = 1 
    ) ) AND (x2.cditem = x1.cditem ) ) AND (x3.codcat = x2.codcat 
    ) ) ;                                                    
                                                             
                                                         



create index "sistemas".idxinvmtransac_1 on "sistemas".inv_mtransac 
    (lnktra,estado,tipope) using btree  in datadbs;
create index "sistemas".idxinvdtransac_1 on "sistemas".inv_dtransac 
    (codemp,codsuc,codbod,cditem,tipope,estado,actexi) using 
    btree  in datadbs;
create unique index "sistemas".ix183_1 on "sistemas".inv_ncorretq 
    (numcor) using btree  in datadbs;
create index "sistemas".idx_facdtransac_1 on "sistemas".fac_dtransac 
    (cditem) using btree  in datadbs1;
create unique index "sistemas".ix154_1 on "sistemas".fac_mofertas 
    (lnkofe) using btree  in datadbs;
create unique index "sistemas".ix193_1 on "sistemas".vta_preuxcan 
    (lnkpre) using btree  in datadbs1;
create index "sistemas".idxfacmtransac_1 on "sistemas".fac_mtransac 
    (numpos,feccor,estado,credit) using btree  in datadbs1;
create index "sistemas".idxfacmtransac_2 on "sistemas".fac_mtransac 
    (codemp,tipdoc,nserie,numdoc) using btree  in datadbs1;



alter table "sistemas".inv_dtransac add constraint (foreign key 
    (lnktra) references "sistemas".inv_mtransac  constraint "sistemas"
    .fkinvmtransac1);
alter table "sistemas".inv_dtransac add constraint (foreign key 
    (cditem) references "sistemas".inv_products  constraint "sistemas"
    .fkinvproducts6);
alter table "sistemas".inv_epqsxpro add constraint (foreign key 
    (cditem) references "sistemas".inv_products  on delete cascade 
    constraint "sistemas".fkinvproducts3);
alter table "sistemas".glb_sucsxemp add constraint (foreign key 
    (codemp) references "sistemas".glb_empresas  constraint "sistemas"
    .fkglbempresas);
alter table "sistemas".inv_mbodegas add constraint (foreign key 
    (codemp) references "sistemas".glb_empresas  constraint "sistemas"
    .fkglbempresas2);
alter table "sistemas".inv_mbodegas add constraint (foreign key 
    (codsuc) references "sistemas".glb_sucsxemp  constraint "sistemas"
    .fkglbsucsxemp1);
alter table "sistemas".fac_tarjetas add constraint (foreign key 
    (lnktra) references "sistemas".pos_mtransac  on delete cascade 
    constraint "sistemas".fkposmtransac2);
alter table "sistemas".fac_usuaxpos add constraint (foreign key 
    (numpos) references "sistemas".fac_puntovta  on delete cascade 
    constraint "sistemas".fkfacpuntovta1);
alter table "sistemas".glb_dprogram add constraint (foreign key 
    (codpro) references "sistemas".glb_programs  on delete cascade 
    constraint "sistemas".fkglbprograms1);
alter table "sistemas".inv_products add constraint (foreign key 
    (codcat) references "sistemas".glb_categors  constraint "sistemas"
    .fkinvcategpro1 disabled );
alter table "sistemas".inv_products add constraint (foreign key 
    (codprv) references "sistemas".inv_provedrs  constraint "sistemas"
    .fkinvprovedrs1);
alter table "sistemas".inv_products add constraint (foreign key 
    (unimed) references "sistemas".inv_unimedid  constraint "sistemas"
    .fkinvunimedid1);
alter table "sistemas".inv_dproduct add constraint (foreign key 
    (cditem) references "sistemas".inv_products  on delete cascade 
    constraint "sistemas".fkinvproducts5);
alter table "sistemas".inv_dproduct add constraint (foreign key 
    (citems) references "sistemas".inv_products  on delete cascade 
    constraint "sistemas".fkinvproducts7);
alter table "sistemas".fac_permxtdc add constraint (foreign key 
    (lnktdc) references "sistemas".fac_tdocxpos  on delete cascade 
    constraint "sistemas".fkfactdocxpos1);
alter table "sistemas".sre_drecetas add constraint (foreign key 
    (lnkrec) references "sistemas".sre_mrecetas  constraint "sistemas"
    .fksremrecetas1);
alter table "sistemas".sre_dsubrect add constraint (foreign key 
    (lnkrec) references "sistemas".sre_subrecet  on delete cascade 
    constraint "sistemas".fksremsubrecet1);
alter table "sistemas".sre_dordenes add constraint (foreign key 
    (lnkord) references "sistemas".sre_mordenes  on delete cascade 
    constraint "sistemas".fksremordenes1);
alter table "sistemas".pos_dtransac add constraint (foreign key 
    (lnktra) references "sistemas".pos_mtransac  on delete cascade 
    constraint "sistemas".fkposmtransac1);
alter table "sistemas".sre_dplaspre add constraint (foreign key 
    (lnkord) references "sistemas".sre_mordenes  on delete cascade 
    constraint "sistemas".fksremordenes2);
alter table "sistemas".fac_formaimp add constraint (foreign key 
    (lnktdc) references "sistemas".fac_tdocxpos  on delete cascade 
    constraint "sistemas".fkfactdocxpos2);
alter table "sistemas".fac_dtransac add constraint (foreign key 
    (lnktra) references "sistemas".fac_mtransac  on delete cascade 
    constraint "sistemas".fkfacmtransac1);
alter table "sistemas".fac_dvicorte add constraint (foreign key 
    (lnkcor) references "sistemas".fac_vicortes  on delete cascade 
    constraint "sistemas".fkfacvicortes1);



create trigger "sistemas".trgcreaprogramabase insert on "sistemas"
    .glb_programs referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptcreaprogramabase(pos.codpro 
    ,pos.nompro ,pos.ordpro ));

create trigger "sistemas".trgupdnombreprograma update of nompro 
    on "sistemas".glb_programs referencing new as pos
    for each row
        (
        update "sistemas".glb_dprogram set "sistemas".glb_dprogram.nomopc 
    = pos.nompro  where ((codpro = pos.codpro ) AND (progid = 0 ) ) );
    

create trigger "sistemas".trgupdordenprograma update of ordpro 
    on "sistemas".glb_programs referencing new as pos
    for each row
        (
        update "sistemas".glb_dprogram set "sistemas".glb_dprogram.ordopc 
    = pos.ordpro  where ((codpro = pos.codpro ) AND (progid = 0 ) ) );
    

create trigger "sistemas".trgupdestadomovto update of estado 
    on "sistemas".inv_mtransac referencing new as pos
    for each row
        (
        update "sistemas".inv_dtransac set "sistemas".inv_dtransac.estado 
    = pos.estado  where (lnktra = pos.lnktra ) );

create trigger "sistemas".trgeliminarmovto delete on "sistemas"
    .inv_mtransac referencing old as pre
    for each row
        (
        delete from "sistemas".inv_dtransac  where (lnktra = 
    pre.lnktra ) );

create trigger "sistemas".trgupdfechaemision update of fecemi 
    on "sistemas".inv_mtransac referencing new as pos
    for each row
        (
        update "sistemas".inv_dtransac set "sistemas".inv_dtransac.fecemi 
    = pos.fecemi  where (lnktra = pos.lnktra ) );

create trigger "sistemas".trgupdtipomovimiento update of tipmov 
    on "sistemas".inv_mtransac referencing new as pos
    for each row
        (
        update "sistemas".inv_dtransac set "sistemas".inv_dtransac.tipmov 
    = pos.tipmov  where (lnktra = pos.lnktra ) );

create trigger "sistemas".trgupdorigen update of codori on "sistemas"
    .inv_mtransac referencing new as pos
    for each row
        (
        update "sistemas".inv_dtransac set "sistemas".inv_dtransac.codori 
    = pos.codori  where (lnktra = pos.lnktra ) );

create trigger "sistemas".trgupddestino update of coddes on "sistemas"
    .inv_mtransac referencing new as pos
    for each row
        (
        update "sistemas".inv_dtransac set "sistemas".inv_dtransac.coddes 
    = pos.coddes  where (lnktra = pos.lnktra ) );

create trigger "sistemas".trgupdanio update of aniotr on "sistemas"
    .inv_mtransac referencing new as pos
    for each row
        (
        update "sistemas".inv_dtransac set "sistemas".inv_dtransac.aniotr 
    = pos.aniotr  where (lnktra = pos.lnktra ) );

create trigger "sistemas".trgupdmes update of mestra on "sistemas"
    .inv_mtransac referencing new as pos
    for each row
        (
        update "sistemas".inv_dtransac set "sistemas".inv_dtransac.mestra 
    = pos.mestra  where (lnktra = pos.lnktra ) );

create trigger "sistemas".trginsertarproducto insert on "sistemas"
    .inv_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp 
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem 
    ,pos.tipope ,pos.codabr ,pos.preuni ));

create trigger "sistemas".trgupdestadoproducto update of estado 
    on "sistemas".inv_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp 
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem 
    ,pos.tipope ,pos.codabr ,pos.preuni ));

create trigger "sistemas".trgupdcanuniproducto update of canuni 
    on "sistemas".inv_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp 
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem 
    ,pos.tipope ,pos.codabr ,pos.preuni ));

create trigger "sistemas".trgcupdpreuniproducto update of preuni 
    on "sistemas".inv_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp 
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem 
    ,pos.tipope ,pos.codabr ,pos.preuni ));

create trigger "sistemas".trgeliminarproducto delete on "sistemas"
    .inv_dtransac referencing old as pre
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pre.codemp 
    ,pre.codsuc ,pre.codbod ,pre.tipmov ,pre.aniotr ,pre.mestra ,pre.cditem 
    ,pre.tipope ,pre.codabr ,pre.preuni ));

create trigger "sistemas".trgcrearaccesosxusuario insert on "sistemas"
    .glb_usuarios referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptcrearaccesosxusuario(pos.roleid 
    ,pos.userid ));

create trigger "sistemas".trgupdateaccesosxusuario update of 
    roleid on "sistemas".glb_usuarios referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptcrearaccesosxusuario(pos.roleid 
    ,pos.userid ));

create trigger "sistemas".trgdeleteaccesosxusuario delete on 
    "sistemas".glb_usuarios referencing old as pre
    for each row
        (
        delete from "sistemas".glb_permxusr  where (userid = 
    pre.userid ) );

create trigger "sistemas".trginsertarempaque insert on "sistemas"
    .inv_products referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptempaquedefault(pos.cditem 
    ));

create trigger "sistemas".trgupdateunimed update of unimed on 
    "sistemas".inv_products referencing old as pre new as pos
    for each row
        when ((pre.unimed != pos.unimed ) )
            (
            execute procedure "sistemas".sptempaquedefault(pos.cditem 
    ));

create trigger "sistemas".trginsertafisico insert on "sistemas"
    .inv_scfisico referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptsumafisico(pos.codemp 
    ,pos.codsuc ,pos.codbod ,pos.fecinv ,pos.cditem ,pos.codabr ));

create trigger "sistemas".trgsremrecetas1 delete on "sistemas"
    .sre_mrecetas referencing old as pre
    for each row
        (
        delete from "sistemas".sre_drecetas  where (lnkrec = 
    pre.lnkrec ) );

create trigger "sistemas".trgupdateinvenxfac update of lnkinv 
    on "sistemas".pos_mtransac referencing old as pre new as pos
    
    for each row
        (
        execute procedure "sistemas".sptinsertfaccturasinv(pos.lnktra 
    ,pos.numpos ,pos.codemp ,pos.nserie ,pos.numdoc ,pos.fecemi ,pos.codcli 
    ,pos.nomcli ,pos.llevar ));

create trigger "sistemas".trianulafactura update of estado on 
    "sistemas".pos_mtransac referencing new as pos
    for each row
        when ((pos.estado = 0 ) )
            (
            update "sistemas".inv_mtransac set "sistemas".inv_mtransac.estado 
    = 'A' ,"sistemas".inv_mtransac.motanl = pos.motanl ,"sistemas".inv_mtransac.usranl 
    = pos.usranl ,"sistemas".inv_mtransac.fecanl = pos.fecanl ,"sistemas".inv_mtransac.horanl 
    = pos.horanl  where ((numrf1 = pos.lnktra ) AND (tipmov = 1 ) ) ),
        when ((pos.estado = 1 ) )
            (
            update "sistemas".inv_mtransac set "sistemas".inv_mtransac.estado 
    = 'V' ,"sistemas".inv_mtransac.motanl = pos.motanl ,"sistemas".inv_mtransac.usranl 
    = pos.usranl ,"sistemas".inv_mtransac.fecanl = pos.fecanl ,"sistemas".inv_mtransac.horanl 
    = pos.horanl  where ((numrf1 = pos.lnktra ) AND (tipmov = 1 ) ) );

create trigger "sistemas".trgupdateinventario update of lnkinv 
    on "sistemas".fac_mtransac referencing old as pre new as pos
    
    for each row
        when ((pos.lnkapa = 0 ) )
            (
            execute procedure "sistemas".sptinsertafacturas(pos.lnktra 
    ,pos.numpos ,pos.codemp ,pos.numdoc ,pos.fecemi ,pos.codcli ));

create trigger "sistemas".trganulafac update of estado on "sistemas"
    .fac_mtransac referencing new as pos
    for each row
        when ((pos.estado = 'A' ) )
            (
            update "sistemas".inv_mtransac set "sistemas".inv_mtransac.estado 
    = pos.estado ,"sistemas".inv_mtransac.motanl = pos.motanl ,"sistemas".inv_mtransac.usranl 
    = pos.usranl ,"sistemas".inv_mtransac.fecanl = pos.fecanl ,"sistemas".inv_mtransac.horanl 
    = pos.horanl  where ((numrf1 = pos.lnktra ) AND (tipmov = 1 ) ) ),
        when ((pos.estado = 'V' ) )
            (
            update "sistemas".inv_mtransac set "sistemas".inv_mtransac.estado 
    = pos.estado ,"sistemas".inv_mtransac.motanl = pos.motanl ,"sistemas".inv_mtransac.usranl 
    = pos.usranl ,"sistemas".inv_mtransac.fecanl = pos.fecanl ,"sistemas".inv_mtransac.horanl 
    = pos.horanl  where ((numrf1 = pos.lnktra ) AND (tipmov = 1 ) ) );



grant select on "sistemas".vis_saldosxmovxmes to "all" as "sistemas";
grant select on "sistemas".vis_tipdocxpos to "public" as "sistemas";
grant select on "sistemas".vis_productos to "public" as "sistemas";
grant select on "sistemas".vis_integramovtos to "public" as "sistemas";
grant select on "sistemas".vis_productobodega to "public" as "sistemas";
grant select on "sistemas".vis_tipdocxpostodos to "public" as "sistemas";
grant select on "sistemas".vis_reporterecetas to "public" as "sistemas";
grant select on "sistemas".vis_programas to "public" as "sistemas";
grant select on "sistemas".vis_ordenescomida to "public" as "sistemas";
grant select on "sistemas".vis_detallelibroventas to "public" as "sistemas";
grant select on "sistemas".vis_consumosinvxi to "public" as "sistemas";
grant select on "sistemas".vis_libroventas to "public" as "sistemas";
grant select on "sistemas".vis_ventasxhoras to "public" as "sistemas";
grant select on "sistemas".vis_estadiscorte to "public" as "sistemas";
grant select on "sistemas".vis_estadisticaventas to "public" as "sistemas";

update statistics medium for table "informix".sysams (
     am_owner) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".sysblobs (
     colno) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".syscasts (
     argument_xid, result_xid) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".syschecks (
     seqno, type) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".syscolauth (
     colno, grantee, grantor) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".syscoldepend (
     colno) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".syscolumns (
     colno) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".sysconstraints (
     owner) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".sysdistrib (
     colno, seqno) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".sysfragments (
     evalpos, fragtype, indexname) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".sysindices (
     owner) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".syslangauth (
     grantee, grantor) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".sysobjstate (
     name, owner) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".sysopclasses (
     owner) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".sysprocauth (
     grantee, grantor) 
     resolution   2.00000   0.95000 ;
update statistics medium for table "informix".sysprocbody (
     datakey, seqno) 
     resolution   2.00000   0.95000 ;

 



