create view "informix".vbit (proc_id,cam_llaveval,mov_fecha,tab_id,tab_nom,cam_exp,cam_id,cam_des,cam_vanterior,cam_vactual,usu_nom) as
  select x3.proc_id ,x0.cam_llaveval ,x0.mov_fecha ,x2.tab_id
    ,x2.tab_des ,TRIM ( BOTH ' ' FROM x0.cam_exp ) ,x1.cam_id
    ,x1.cam_des ,x0.cam_vanterior ,x0.cam_vactual ,x0.usu_id
    from "informix".bbit x0 ,"informix".bcam x1 ,"informix".btab
    x2 ,"informix".bproc x3 where ((((x1.cam_id = x0.cam_id )
    AND (x2.tab_id = x1.tab_id ) ) AND (x3.proc_id = x2.proc_id
    ) ) AND NOT EXISTS (select x6.mov_id ,x6.cam_id ,x6.mov_fecha
    ,x6.usu_id ,x6.cam_exp ,x6.cam_vanterior ,x6.cam_vactual
    ,x6.cam_llaveval from "informix".bbit x6 where (((((((x6.cam_vanterior
    = 'Nada' ) AND (x0.cam_vactual = 'Borrado' ) ) AND (x6.cam_vactual
    = x0.cam_vanterior ) ) OR (((x0.cam_vanterior = 'Nada' ) AND
    (x6.cam_vactual = 'Borrado' ) ) AND (x6.cam_vanterior = x0.cam_vactual
    ) ) ) AND (x6.cam_id = x0.cam_id ) ) AND (x6.mov_fecha ::datetime
    year to minute = x0.mov_fecha ::datetime year to minute )
    ) AND (x6.cam_exp = x0.cam_exp ) ) ) )  union select x4.proc_id
    ,'Todos' ,x4.mov_fecha ::datetime year to second ,0 ,x5.proc_desc
    ,'Suceso en Proceso' ,0 ,' ' ,x4.cam_vanterior ::varchar(100)
    ,x4.cam_vactual ::varchar(100) ,x4.usu_id from "informix".bbitp
    x4 ,"informix".bproc x5 where (x5.proc_id = x4.proc_id ) ;
