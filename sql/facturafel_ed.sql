






{ TABLE "pruebas".facturafel_ed row size = 249 number of columns = 23 index size = 9 }

create table "pruebas".facturafel_ed 
  (
    fac_id integer,
    serie char(20),
    num_doc decimal(15,0),
    tipod char(2),
    fecha date,
    descrip_p char(70),
    codigo_p char(14),
    unidad_m char(3),
    cantidad decimal(5,0),
    precio_u decimal(14,2),
    precio_t decimal(14,2),
    descto_t decimal(14,2),
    descto_m decimal(14,2),
    precio_p decimal(14,2),
    precio_m decimal(14,2),
    total_imp decimal(14,2),
    ing_netos decimal(14,2),
    total_iva decimal(14,2),
    tipo char(15),
    base decimal(14,2),
    tasa decimal(9,2),
    monto decimal(14,2),
    categoria char(10)
  );

revoke all on "pruebas".facturafel_ed from "public" as "pruebas";


create index "pruebas".ix_feled_01 on "pruebas".facturafel_ed 
    (fac_id) using btree ;


