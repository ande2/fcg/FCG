






{ TABLE "pruebas".facturafel_e row size = 1558 number of columns = 64 index size = 260 }

create table "pruebas".facturafel_e 
  (
    serie char(20),
    num_doc decimal(15,0),
    tipod char(2),
    fecha date,
    fecha_em char(35),
    fecha_anul char(35),
    num_aut char(20),
    fecha_re char(15),
    rango_i decimal(12,0),
    rango_f decimal(12,0),
    from char(50),
    to char(50),
    cc char(50),
    formats char(10),
    tipo_doc char(12),
    estado_doc char(20),
    ant_serie varchar(20),
    ant_numdoc decimal(15,0),
    ant_fecemi varchar(35),
    ant_resoluc varchar(30),
    c_moneda char(3),
    tipo_cambio char(10),
    info_reg char(15),
    num_int decimal(20,0),
    nit_e char(12),
    nombre_c char(70),
    idioma_e char(2),
    nombre_e char(70),
    codigo_e char(3),
    dispositivo_e char(3),
    direccion_e char(70),
    departamento_e char(20),
    municipio_e char(20),
    pais_e char(2),
    codpos_e decimal(5,0),
    nit_r char(12),
    nombre_r char(70),
    idioma_r char(2),
    direccion_r char(70),
    departamento_r char(20),
    municipio_r char(20),
    pais_r char(2),
    codpos_r decimal(5,0),
    total_b decimal(10,2),
    total_d decimal(10,2),
    monto_d decimal(10,2),
    total_i decimal(10,2),
    ing_netosg decimal(10,2),
    total_iva1 decimal(10,2),
    tipo1 char(10),
    base1 decimal(10,2),
    tasa1 decimal(10,2),
    monto1 decimal(10,2),
    total_neto decimal(10,2),
    serie_e char(20),
    numdoc_e char(40),
    autorizacion char(200),
    estatus char(2),
    fac_id serial not null constraint "pruebas".n2092_117,
    p_descuento decimal(5,2),
    fel_msg integer,
    id_factura integer,
    total_en_letras char(250),
    tipo_pago char(2)
  );

revoke all on "pruebas".facturafel_e from "public" as "pruebas";


create unique index "pruebas".ix_fel_01 on "pruebas".facturafel_e 
    (fac_id) using btree ;
create index "pruebas".ix_fel_02 on "pruebas".facturafel_e (serie,
    num_doc,tipod,fecha) using btree ;
create index "pruebas".ix_fel_03 on "pruebas".facturafel_e (fecha,
    estatus,autorizacion) using btree ;


