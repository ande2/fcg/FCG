#!/usr/bin/bash
. /opt/informix/ifx_fcg.sh
. /opt/4js/gst320/rt/envgenero

BASEDIR=/app/FCG/ANDE.fcg
export BASEDIR

LANG=es_GT.utf8
export LANG

SPOOLDIR=$HOME/spl
export SPOOLDIR

#export FGLWEDEBUG=9
#export FGLSQLDEBUG=9
export FGLIMAGEPATH=./images
export LOGNAME=sistemas

cd /app/FCG/ANDE.fcg/bin

fglrun menu_dina.42r
